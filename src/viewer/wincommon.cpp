#include <algorithm>

#include "wincommon.h"
#include "event.h"
#include <SDL_keycode.h>
#include <SDL.h>

#include "util.h"
#include "window.h"
#include "event.h"
#include "guibase.h"

#include "mesh.h"
#include "renderer.h"
#include "glmx.h"

#include "camera.h"
#include "scene.h"
#include "framebuffer.h"

#include "uiconsole.h"
#include "innerwindow.h"

// ------------------------------------------------

const ScriptDef WinCommon::scriptdef("syswindow", ScriptTypes::SYSWINDOW, NULL, ScriptDef::PERSISTENT);


class WinCommonRecv : public IEventRecv
{
public:
    WinCommonRecv(WinCommon& w) : win(w)
    {
    }

    void mouseMoved(unsigned x, unsigned y, int rx, int ry, unsigned buttons)
    {
        Scriptable::CallEnv env(&win, "onMouseMove");
        if(env)
        {
            win.push(x);
            win.push(y);
            win.push(rx);
            win.push(ry);
            win.push(buttons);
            env.call(0);
        };
    }
    void mouseClick(unsigned button, unsigned state, unsigned x, unsigned y)
    {
        Scriptable::CallEnv env(&win, "onMouseClick");
        if(env)
        {
            win.push(button);
            win.push(state);
            win.push(x);
            win.push(y);
            env.call(0);
        };
    }
    void mouseWheel(int change)
    {
        Scriptable::CallEnv env(&win, "onMouseWheel");
        if(env)
        {
            win.push(change);
            env.call(0);
        };
    }
    void keyEvent(unsigned key, unsigned mod, unsigned state)
    {
        Scriptable::CallEnv env(&win, "onKeyEvent");
        if(env)
        {
            const char *keyname = SDL_GetScancodeName((SDL_Scancode)key);
            win.push(keyname);
            win.push(key);
            win.push(mod);
            win.push((bool)!!state); // make sure it's passed as a lua boolean type, and not as number
            env.call(0);
        }
    }
    void windowEvent(WindowEventType type) {}
    void windowResize(unsigned w, unsigned h)
    {
        Renderer *r = win.getRenderer();
        r->resize();

        Scriptable::CallEnv env(&win, "onResize");
        if(env)
        {
            win.push(w);
            win.push(h);
            env.call(0);
        }
    }

    WinCommon& win;
};

// -------------------------------------------------

WinCommon::WinCommon()
{
}

WinCommon::~WinCommon()
{
}

bool WinCommon::onCloseRequest()
{
    CallEnv env(this, "onCloseRequest");
    if(!env)
        return true; // no such method? -> close

    bool close = false;
    if(!env.call(close))
        return true; // error during call? -> close anyway

    return close;
}

void WinCommon::onClose()
{
    callOpt("onClose", 0);
    _deleteInnerWindows();
}

void WinCommon::onAfterClose()
{
    unbindScript();
}

void WinCommon::onUpdate(float dt)
{
    CallEnv env(this, "onUpdate");
    if(env)
    {
        push(dt);
        env.call(0);
    }

    OuterWindow::onUpdate(dt);
}

glm::uvec2 WinCommon::getViewport() const
{
    return render->getViewport();
}

void WinCommon::onOpen()
{
    addEventRecv(new WinCommonRecv(*this));
    addDrawHook(s_drawHook, this);
}

void WinCommon::s_drawHook(Window *w, void *ud)
{
    WinCommon *this_ = (WinCommon*)w;
    this_->drawHook();
}

void WinCommon::drawHook()
{
    callOpt("onRenderBegin", 0);
    callOpt("onRender", 0);
    drawUI();
    callOpt("onRenderEnd", 0);
}

void WinCommon::drawInnerWindows()
{
    OuterWindow::drawUI();
}

void WinCommon::onDrawUI()
{
    callOpt("onDrawUI", 0);
}

void WinCommon::drawUI()
{
    ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.2f, 0.2f, 0.2f, 0.5f));
    ImGui::PushStyleColor(ImGuiCol_ChildWindowBg, ImVec4(0.2f, 0.2f, 0.2f, 0.5f));

        // An invisible "inner root window" allows to draw widgets directly,
        // without having them show up in the default "Debug" window.
        ImGui::SetNextWindowPos(ImVec2(0, 0));
        ImGui::SetNextWindowSize(ImGui::GetIO().DisplaySize);
        ImGui::Begin("##rootbg", NULL, ImGui::GetIO().DisplaySize, 0,
            ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove
            | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoCollapse
            | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing
            | ImGuiWindowFlags_NoBringToFrontOnFocus);

            onDrawUI();

        ImGui::End();
    ImGui::PopStyleColor(2);
}

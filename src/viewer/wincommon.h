#pragma once

#include <vector>
#include "window.h"
#include "camera.h"
#include "outerwindow.h"
#include "luaobj.h"

class InnerWindow;

class WinCommon : public Window, public OuterWindow, public ScriptableT<WinCommon>
{
    friend class WinCommonRecv;

public:
    static const ScriptDef scriptdef;

    WinCommon();
    virtual ~WinCommon();

    glm::uvec2 getViewport() const;
    void drawUI();
    void drawInnerWindows();

private:

    static void s_drawHook(Window *w, void *ud);
    void drawHook();

protected:
    
    virtual void onOpen();
    virtual bool onCloseRequest();
    virtual void onClose();
    virtual void onAfterClose();
    virtual void onUpdate(float dt);
    virtual void onDrawUI();
};


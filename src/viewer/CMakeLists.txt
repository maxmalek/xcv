set(viewer_SRC
wincommon.cpp
wincommon.h
lua_syswin.cpp
lua_syswin.h
)

add_library(viewer ${viewer_SRC})
target_link_libraries(viewer shadergraph window scene base guibase algebra)

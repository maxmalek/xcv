#include "luainternal.h"
#include "wincommon.h"
#include "innerwindow.h"
#include "shadergrapheditor.h"
#include "renderer.h"
#include "longjobview.h"
#include "uiconsole.h"
#include <imgui/imgui_dock.h>
#include "lua_imgui.h"

template<typename T>
static T *fixinner(lua_State *L, T *iw)
{
    iw->scriptRegisterSelf(L);
    iw->makePersistent();
    return iw;
}

static ImGuiDockSlot getDockSlot(lua_State *L, int idx)
{
    const char *s = luaL_optstring(L, idx, "f");
    switch(*s)
    {
        case 'l': return ImGuiDockSlot_Left;
        case 'r': return ImGuiDockSlot_Right;
        case 't': return ImGuiDockSlot_Top;
        case 'b': return ImGuiDockSlot_Bottom;
        case 'T': return ImGuiDockSlot_Tab;
        case 'f': return ImGuiDockSlot_Float;
        case 0: case 'w': return ImGuiDockSlot_None;
        default:
            luaL_error(L, "Unhandled dock slot: %c", *s);
    }
    return ImGuiDockSlot_Float;
}

// (Title, identifier, sizeX, sizeY, posX, posY, dockSlot, dockSplitRatio)
template<typename T>
static T *makeinner(lua_State *L, int idx = 1)
{
    WinCommon *outer = luaGetObj<WinCommon>(L, idx);
    T *iw = new T(outer, luaL_checkstring(L, idx+1), lua_tostring(L, idx+2));
    glm::vec2 size, pos, outersize;
    {
        int w, h;
        outer->getSize(&w, &h);
        outersize = glm::vec2(w, h);
    }
    if(lua_isnumber(L, idx+3))
        size = lua_tovec2(L, idx+3);
    else
        size = outersize;
    iw->setInitialSize(size.x, size.y);

    if(lua_isnumber(L, idx+5))
        pos = lua_tovec2(L, idx+5);
    else
        pos = (outersize / 2.0f) - (size / 2.0f);
    iw->setInitialPos(pos.x, pos.y);

    iw->setDockSlot(getDockSlot(L, idx+7));
    iw->setDockSplitRatio(lua_optfloat(L, idx+8, 0.5f));

    iw->imguiFlags |= lua_imgui_getWindowFlags(L, idx+9);

    return fixinner<T>(L, iw);
}

struct LuaSysWindow : public LuaClassBind<WinCommon>
{
    static const luaL_Reg methods[];

    luaFunc(new)
    {
        luaReturnObj(new WinCommon);
    }

    luaFunc(open)
    {
        WinCommon *win = This(L);
        int w = (int)luaL_optinteger(L, 2, 0);
        int h = (int)luaL_optinteger(L, 3, 0);
        bool full = getBool(L, 4);
        int screen = (int)luaL_optinteger(L, 5, 0);
        luaReturnBool(win->open(w, h, full, screen));
    }

    luaFunc(requestClose) { luaReturnBool(This(L)->requestClose()); }
    luaFunc(destroy) { This(L)->close(); luaReturnNil(); }
    luaFunc(setTitle) { This(L)->setTitle(luaL_checkstring(L, 2)); luaReturnSelf(); }
    luaFunc(setSize) { This(L)->setSize(lua_checkint(L, 2), lua_checkint(L, 3)); luaReturnSelf(); }
    luaFunc(setPos) { This(L)->setPosition(lua_checkint(L, 2), lua_checkint(L, 3)); luaReturnSelf(); }
    luaFunc(setFullscreen) { This(L)->open(0, 0, getBool(L, 2)); luaReturnSelf(); }

    luaFunc(getViewport) { luaReturnT(This(L)->getViewport()); }

    luaFunc(newInner)        { luaReturnObj(makeinner<InnerWindow>(L)); }
    luaFunc(newJobsView)     { luaReturnObj(fixinner(L, new LongJobExecutorWindow(This(L), g_mainLongExec))); }
    luaFunc(newConsole)      { luaReturnObj(makeinner<UIConsole>(L)); }

    luaFunc(gpuinfo)
    {
        Renderer *r = This(L)->getRenderer();
        lua_pushstring(L, r->getGPUName().c_str());
        lua_pushinteger(L, r->getFreeVideoMemoryKB());
        lua_pushinteger(L, r->getTotalVideoMemoryKB());
        lua_pushinteger(L, r->getGPUTimeNS() / 1000); // microseconds
        return 4;
    }

    luaFunc(drawInnerWindows) { This(L)->drawInnerWindows(); luaReturnSelf(); }
    luaFunc(drawInnerWindowMenuItems)
    {
        InnerWindow *clicked = This(L)->_drawInnerWindowMenuItems();
        Scriptable *w = dynamic_cast<Scriptable*>(clicked);
        luaReturnObj(w);
    }

    luaFunc(placeMouse)
    {
        glm::ivec2 p = lua_toivec2(L, 2);
        This(L)->placeMouse(p.x, p.y);
        luaReturnSelf();
    }

    luaFunc(size)
    {
        glm::ivec2 sz;
        This(L)->getSize(&sz.x, &sz.y);
        luaReturnT(sz);
    }

    luaFunc(getFocusedInnerWindow)
    {
        InnerWindow *iw = This(L)->getFocusedInnerWindow();
        luaReturnObj(iw);
    }

    luaFunc(update)
    {
        float dt = lua_tofloat(L, 2);
        This(L)->update(dt);
        luaReturnSelf();
    }

    luaFunc(GetCurrent)
    {
        Window *w = Window::GetCurrent();
        WinCommon *wc = dynamic_cast<WinCommon*>(w);
        luaReturnObj(wc);
    }
};

const luaL_Reg LuaSysWindow::methods[] =
{
    luaRegisterNewDelete(LuaSysWindow),
    luaRegisterMethod(LuaSysWindow, open),
    luaRegisterMethod(LuaSysWindow, destroy),
    luaRegisterMethod(LuaSysWindow, requestClose),
    luaRegisterMethod(LuaSysWindow, setTitle),
    luaRegisterMethod(LuaSysWindow, setSize),
    luaRegisterMethod(LuaSysWindow, setPos),
    luaRegisterMethod(LuaSysWindow, setFullscreen),
    luaRegisterMethod(LuaSysWindow, getViewport),
    luaRegisterMethod(LuaSysWindow, newInner),
    luaRegisterMethod(LuaSysWindow, newJobsView),
    luaRegisterMethod(LuaSysWindow, newConsole),
    luaRegisterMethod(LuaSysWindow, gpuinfo),
    luaRegisterMethod(LuaSysWindow, drawInnerWindows),
    luaRegisterMethod(LuaSysWindow, drawInnerWindowMenuItems),
    luaRegisterMethod(LuaSysWindow, size),
    luaRegisterMethod(LuaSysWindow, getFocusedInnerWindow),
    luaRegisterMethod(LuaSysWindow, update),
    luaRegisterMethod(LuaSysWindow, GetCurrent)
};




luaFunc(getKeyState)
{
    const Window::KeyState ks = Window::GetKeyState(lua_toint(L, 1));
    lua_pushboolean(L, ks & Window::KDOWN);
    lua_pushboolean(L, ks & Window::KPRESSED);
    lua_pushboolean(L, ks & Window::KRELEASED);
    lua_pushboolean(L, ks & Window::KREPEAT);
    return 4;
}

luaFunc(getKeyName)
{
    if(const char *s = Window::GetKeyName(lua_checkint(L, 1)))
        luaReturnStr(s);
    luaReturnNil();
}

luaFunc(getKeyCode)
{
    luaReturnInt(Window::GetKeyCode(lua_checkrealstring(L, 1)));
}

static const luaL_Reg syswinlib[] =
{
    luaRegister(getKeyState),
    luaRegister(getKeyName),
    luaRegister(getKeyCode),
    {NULL, NULL}
};

int register_lua_syswin(lua_State *L)
{
    LuaStackCheck(L, 1);

    luaL_newlib(L, syswinlib);
    luaRegisterClassInLib<LuaSysWindow>(L, -1);
    //luaRegisterEnumInLib(L, gpulibenum, -1);
    return 1;
}

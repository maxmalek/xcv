#include "runtime.h"
#include "platform.h"
#include <SDL.h>
#include <SDL_main.h>
#include "texture.h"
#include "log.h"
#include "distance_cpu.h"
#include <vector>
#include <stdio.h>

static void benchdt(const char *fn, unsigned times, float threshold, bool mt)
{
    Blob3DInfo info;
    float *data = TextureAny::LoadFileAsMem<float>(fn, &info);
    if(!data)
    {
        logerror("Failed to load file: %s", fn);
        return;
    }
    info.type = B3D_FLOAT;

    std::vector<unsigned> timings;
    const size_t N = b3d_getNumElements(&info);
    std::vector<float> target(N);
    
     float * const p = &target[0];
    for(unsigned i = 0; i < times; ++i)
    {
        memcpy(p, data, N * sizeof(float)); // DT is in place -> must copy
        const unsigned starttime = SDL_GetTicks();
        if(mt)
            DistanceTransform::float3d_gt(p, info.w, info.h, info.d, threshold);
        else
            DistanceTransform::float3d_gt_singlethread(p, info.w, info.h, info.d, threshold);
        const unsigned endtime = SDL_GetTicks();
        const unsigned difftime = endtime - starttime;
        timings.push_back(difftime);
        log("[%u] Distance transform of (%ux%ux%u) image with threshold %f took %u ms", i, info.w, info.h, info.d, threshold, difftime);
    }

    double avg = 0;
    for(size_t i = 0; i < timings.size(); ++i)
        avg += timings[i];
    avg /= times;
    log("Average after %u runs: %f", times, avg);
    free(data);
}

int main(int argc, char **argv)
{
    if(argc < 2)
    {
        fprintf(stderr, "benchdt <file> [times=1] [threshold=0.5] [use all cores=1]");
        return 1;
    }
    const unsigned times = argc > 2 ? atoi(argv[2]) : 1;
    const float threshold = argc > 3 ? (float)atof(argv[3]) : 0.5f;
    const bool mt =  argc > 4 ? !!atoi(argv[4]) : true;

    unsigned flags = Runtime::WITH_GPU; // needed for image conversion
    if(mt)
        flags |= Runtime::WITH_MT;

    Runtime::Init(Runtime::Flags(flags));
    benchdt(argv[1], times, threshold, mt);
    return 0;
}

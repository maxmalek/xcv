#include "blob3dio.h"
#include "plugins/itk/imageio_itk.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "platform.h"

#include <clio/clio.h>
#include <stdarg.h>

// This file is intentionally not linked against SDL, so we can't use platform::getCPUCount() here.
// but we do have ITK, which also autodetects #cores.
#include <itkMultiThreader.h>


static void bail(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    fprintf(stderr, "\n");
    exit(1);
}

static void showhelp()
{
    printf("Usage: itk2blob3d [-options] infile [outfile]\n");
    printf("Mandatory options:\n");
    printf("-f --format <f>       Specify data format of output. Possible values:\n");
    for(unsigned i = B3D_FIRST_VALID_TYPE; i <= B3D_LAST_VALID_TYPE; ++i)
        printf("      %s / %s\n",  Blob3DNameSized[i], Blob3DNameCommon[i]);
    printf("-c --channels <c>     Specify number of channels/components\n"
           "                      the output should have.\n");
    printf("Optional:\n");
    printf("-r --rescale          If set, rescale output to full range of datatype.\n"
           "                      Only used if channels == 1, otherwise ignored.\n");
    printf("-d --dimensions <d>   Number of dimensions the output should have (1..3).\n"
           "                      Autodetect based on input if 0 or left away.\n");
    printf("-t --threads <t>      Use t threads for compression, if supported\n"
           "                      (0: auto, default)\n");
    printf("-l --level <l>        Use compression level l, if supported [0-22].\n"
           "                      (Fastest: 1; best: 22; default: 7; no compr: 0)\n");
    printf("-v --version <v>      Use file format version v. Newest if left away.\n");
    printf("\nExamples:\n"
           "./itk2blob3d -t u8 -c 4 image.png\n"
           "./itk2blob3d -t u16 -c 1 -r -d 3 ctscan.nii rescaled.b3d\n");

    exit(2);
}

static void helpcb(clio::ArgParser& parser)
{
    showhelp();
}

int main(int argc, char *argv[])
{
    if(argc <= 1)
        showhelp();

    bool rescale = false;
    unsigned channels = 0, dims = 0, threads = 0;
    int level = -1, version = -1;
    std::string typestr, infile, outfile;
    {
        clio::ArgParser parser("", "");
        parser.addFlag("h help");
        parser.addFlag("rescale r");
        parser.addStr("format f", "");
        parser.addInt("channels c", channels);
        parser.addInt("dimensions d", dims);
        parser.addInt("version v", version);
        parser.addInt("threads t", threads);
        parser.addInt("level l", level);

        parser.parse(argc, argv);

        if(parser.getFlag("help"))
            showhelp();

        rescale = parser.getFlag("rescale");
        typestr = parser.getStr("format");
        channels = parser.getInt("channels");
        dims = parser.getInt("dimensions");
        level = parser.getInt("level");
        threads = parser.getInt("threads");
        version = parser.getInt("version");


        std::vector<std::string> av = parser.getArgs();
        if(av.size())
            infile = av[0];
        if(av.size() > 1)
            outfile = av[1];

#ifdef _DEBUG
        puts("----------------------");
        parser.print();
        puts("----------------------");
#endif
    }
    if(infile.empty())
        bail("Must specity input file name (see --help).");
    if(typestr.empty())
        bail("Must specify output datatype (--type, see --help).");
    if(channels == 0)
        bail("Must specify number of channels/components (--channels, see --help).");

    const Blob3DType type = b3d_getTypeFromString(typestr.c_str());

    if(type == B3D_UNSPECIFIED)
        bail("Unknown data format: %s", typestr.c_str());

    platform::normalizePath((char*)infile.c_str());

    Blob3DInfo info;
    void *buf = NULL;

    IOResult res = loadImage_itk(infile.c_str(), &buf, &info, type, channels, dims, rescale ? ITKL_RESCALE : ITKL_NONE);

    if(res != IORESULT_OK)
        bail("Failed to open input file %s: %s", infile.c_str(), IOResultName[res]);

    if(!dims)
        printf("Detected %uD input. If this is incorrect, use --dimensions (see --help).\n", b3d_getDimensions(&info));

    if(outfile.empty())
    {
        const std::string::size_type dotpos = infile.rfind('.');
        const std::string::size_type slashpos = infile.rfind('/');
        std::string prefix;
        if(dotpos != std::string::npos)
        {
            // don't catch the '.' in  "~/.config/..." and such
            if(slashpos != std::string::npos && slashpos < dotpos)
                prefix = infile.substr(0, dotpos);
        }
        outfile = (prefix.empty() ? infile : prefix) + ".b3d";
        printf("Saving to %s\n", outfile.c_str());
    }

    if(!threads)
    {
        threads = itk::MultiThreader::GetGlobalDefaultNumberOfThreads(); // Use ITK to autodetect
#ifdef _DEBUG
        printf("Using %u threads\n", threads);
#endif
    }

    res = saveBlob3D(outfile.c_str(), buf, &info, version, threads, level);
    free(buf);

    if(res != IORESULT_OK)
        bail("Failed to save output file: %s\n", outfile.c_str(), IOResultName[res]);

#ifdef _DEBUG
    printf("Saved successfully\n");
#endif

    return 0;
}


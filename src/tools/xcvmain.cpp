#include "runtime.h"
#include "platform.h"
#include <SDL_main.h>

int main(int argc, char **argv)
{
    Runtime::Init();
    if(!Runtime::RunScript("main.lua"))
    {
        platform::msgbox("Failed to run main.lua, aborting");
        return 1;
    }
    while(Runtime::Update() & Runtime::WINDOWS_OPEN) {}
    Runtime::Shutdown();
    return 0;
}

// To be included by files that define new Lua API functions and classes

#pragma once

#include "api_lua.h"
#include <string>
#include <assert.h>
#include "util.h"
#include "luaobj_priv.h"
#include "log.h"
#include "glmx.h"
#include "macros.h"


// make sure this is NOT a macro! Must NOT evaluate x twice!
template<typename T>
inline static int _luaPushObjRet0(lua_State *L, T *x) // Returns nothing if x is NULL
{
    return x ? x->scriptReturnSelf(L) : 0;
}

template<typename T>
inline static int _luaPushObjRet1(lua_State *L, T *x) // Returns explicit nil if x is NULL
{
    if(x)
        return x->scriptReturnSelf(L);

    lua_pushnil(L);
    return 1;
}

#define luaReturnObj(x) return _luaPushObjRet0(L, x)
#define luaPushObj(x) (void)_luaPushObjRet1(L, x)


// like lua_touserdata(), but returns NULL for light userdata
inline void *lua_toheavyuserdata(lua_State *L, int idx)
{
    return lua_type(L, idx) == LUA_TUSERDATA ? lua_touserdata(L, idx) : NULL;
}

// glm vector types

#define GLMVEC(T, F)                                                    \
inline glm::T##2 lua_to##T##2(lua_State *L, int idx)                    \
{                                                                       \
    return glm::T##2(F(L, idx), F(L, idx+1));                           \
}                                                                       \
inline glm::T##3 lua_to##T##3(lua_State *L, int idx)                    \
{                                                                       \
    return glm::T##3(F(L, idx), F(L, idx+1), F(L, idx+2));              \
}                                                                       \
inline glm::T##4 lua_to##T##4(lua_State *L, int idx)                    \
{                                                                       \
    return glm::T##4(F(L, idx), F(L, idx+1), F(L, idx+2), F(L, idx+3)); \
}

#define GLMOPTVEC(T, F)                                                 \
inline glm::T##2 lua_opt##T##2(lua_State *L, int idx, glm::T##2 o)                   \
{                                                                       \
    return glm::T##2(F(L, idx, o.x), F(L, idx+1, o.y));                     \
}                                                                       \
inline glm::T##3 lua_opt##T##3(lua_State *L, int idx, glm::T##3 o)                   \
{                                                                       \
    return glm::T##3(F(L, idx, o.x), F(L, idx+1, o.y), F(L, idx+2, o.z));     \
}                                                                       \
inline glm::T##4 lua_opt##T##4(lua_State *L, int idx, glm::T##4 o)                   \
{                                                                       \
    return glm::T##4(F(L, idx, o.x), F(L, idx+1, o.y), F(L, idx+2, o.z), F(L, idx+3, o.w)); \
}

template<typename T>
int lua_pushT(lua_State *L, const T& v);

template<typename T>
int lua_pushT(lua_State *L, const T *v)
{
    luaReturnObj(v);
}

template<typename V>
inline int lua_pushvec(lua_State *L, const V& v)
{
    for(unsigned i = 0; i < V::components; ++i)
        lua_pushT(L, v[i]);
    return V::components;
}
template<typename V>
inline int lua_pushmat(lua_State *L, const V& v)
{
    for(unsigned i = 0; i < V::cols; ++i)
        lua_pushvec(L, v[i]); // columns
    return V::cols * V::rows;
}
template<typename T>
struct _LuaPushT {};
#define MKPUSH1(T, F) \
template<> struct _LuaPushT<T> { static int Push(lua_State *L, const T& v) { F(L, v); return 1; } };
#define MKPUSHV(T, F) \
template<> struct _LuaPushT<T> { static int Push(lua_State *L, const T& v) { return F(L, v); } };
#define MKPUSHGLMV(T) \
    MKPUSHV(T##2, lua_pushvec) \
    MKPUSHV(T##3, lua_pushvec) \
    MKPUSHV(T##4, lua_pushvec)
#define MKPUSHGLMM3(T) \
    MKPUSHV(T, lua_pushmat)
#define MKPUSHGLMM2(T)\
    MKPUSHGLMM3(T##2) \
    MKPUSHGLMM3(T##3) \
    MKPUSHGLMM3(T##4)
#define MKPUSHGLMM(T) \
    MKPUSHGLMM2(T##2x)\
    MKPUSHGLMM2(T##3x)\
    MKPUSHGLMM2(T##4x)


MKPUSH1(bool, lua_pushboolean)
MKPUSH1(int, lua_pushinteger)
MKPUSH1(unsigned, lua_pushinteger)
MKPUSH1(int64_t, lua_pushinteger)
MKPUSH1(uint64_t, lua_pushinteger)
MKPUSH1(float, lua_pushfloat)
MKPUSH1(double, lua_pushnumber)
MKPUSHGLMV(glm::vec)
MKPUSHGLMV(glm::ivec)
MKPUSHGLMV(glm::uvec)
MKPUSHGLMV(glm::i64vec)
MKPUSHGLMV(glm::u64vec)
MKPUSHGLMV(glm::bvec)
MKPUSHGLMV(glm::dvec)
MKPUSHGLMM(glm::mat)
MKPUSHGLMM(glm::dmat)
#undef MKPUSH1
#undef MKPUSHV
#undef MKPUSHGLMV
#undef MKPUSHGLMM
#undef MKPUSHGLMM2
#undef MKPUSHGLMM3

template<typename T>
inline int lua_pushT(lua_State *L, const T& v)
{
    return _LuaPushT<T>::Push(L, v);
}

#define luaReturnT(x) return lua_pushT(L, x);

GLMVEC(vec, lua_checkfloat)
GLMVEC(ivec, lua_checkint)
GLMVEC(uvec, lua_checkuint)
GLMVEC(i64vec, luaL_checkinteger)
GLMVEC(u64vec, luaL_checkinteger)
GLMVEC(dvec, luaL_checknumber)
GLMVEC(bvec, getBool) // TODO: this should be safer; right now it just casts anything to bool wildly
#undef GLMVEC
GLMOPTVEC(vec, lua_optfloat)
GLMOPTVEC(ivec, lua_optint)
GLMOPTVEC(uvec, lua_optuint)
GLMOPTVEC(i64vec, luaL_optinteger)
GLMOPTVEC(u64vec, luaL_optinteger)
GLMOPTVEC(dvec, luaL_optnumber)
// might want to have optbvecX functions?
#undef GLMOPTVEC


// glm matrix types (column-wise ctor, as defined by openGL and glm)
#define GLMMAT(T)                                                                                              \
inline glm::T##2 lua_to##T##2(lua_State *L, int idx)                                                           \
{                                                                                                              \
    return glm::T##2(lua_tovec2(L, idx), lua_tovec2(L, idx+2));                                                \
}                                                                                                              \
inline glm::T##3 lua_to##T##3(lua_State *L, int idx)                                                           \
{                                                                                                              \
    return glm::T##3(lua_tovec3(L, idx), lua_tovec3(L, idx+3), lua_tovec3(L, idx+6));                          \
}                                                                                                              \
inline glm::T##4 lua_to##T##4(lua_State *L, int idx)                                                           \
{                                                                                                              \
    return glm::T##4(lua_tovec4(L, idx), lua_tovec4(L, idx+4),lua_tovec4(L, idx+8), lua_tovec4(L, idx+12));    \
}                                                                                                              \
inline glm::T##3x2 lua_to##T##3x2(lua_State *L, int idx)                                                       \
{                                                                                                              \
    return glm::T##3x2(lua_tovec2(L, idx), lua_tovec2(L, idx+2), lua_tovec2(L, idx+4));                        \
}                                                                                                              \
inline glm::T##2x3 lua_to##T##2x3(lua_State *L, int idx)                                                       \
{                                                                                                              \
    return glm::T##2x3(lua_tovec3(L, idx), lua_tovec3(L, idx+3));                                              \
}                                                                                                              \
inline glm::T##4x2 lua_to##T##4x2(lua_State *L, int idx)                                                       \
{                                                                                                              \
    return glm::T##4x2(lua_tovec2(L, idx), lua_tovec2(L, idx+2), lua_tovec2(L, idx+4),  lua_tovec2(L, idx+6)); \
}                                                                                                              \
inline glm::T##2x4 lua_to##T##2x4(lua_State *L, int idx)                                                       \
{                                                                                                              \
    return glm::T##2x4(lua_tovec4(L, idx), lua_tovec4(L, idx+4));                                              \
}                                                                                                              \
inline glm::T##4x3 lua_to##T##4x3(lua_State *L, int idx)                                                       \
{                                                                                                              \
    return glm::T##4x3(lua_tovec3(L, idx), lua_tovec3(L, idx+3), lua_tovec3(L, idx+6),  lua_tovec3(L, idx+9)); \
}                                                                                                              \
inline glm::T##3x4 lua_to##T##3x4(lua_State *L, int idx)                                                       \
{                                                                                                              \
    return glm::T##3x4(lua_tovec4(L, idx), lua_tovec4(L, idx+4), lua_tovec4(L, idx+8));                        \
}

GLMMAT(mat)
GLMMAT(dmat)
#undef GLMMAT

// universal template getter for everything

template<typename T> struct _lua_getT {};
#define DEFGET(T, F) template<> struct _lua_getT<T> { static FORCEINLINE T get(lua_State *L, int idx) { return F(L, idx); } };
#define DEFGETV(T, F)  \
    DEFGET(T##2, F##2) \
    DEFGET(T##3, F##3) \
    DEFGET(T##4, F##4)
#define DEFGETM(T)                      \
    DEFGETV(glm::T, lua_to##T)          \
    DEFGET(glm::T##2x3, lua_to##T##2x3) \
    DEFGET(glm::T##2x4, lua_to##T##2x4) \
    DEFGET(glm::T##3x2, lua_to##T##3x2) \
    DEFGET(glm::T##3x4, lua_to##T##3x4) \
    DEFGET(glm::T##4x2, lua_to##T##4x2) \
    DEFGET(glm::T##4x3, lua_to##T##4x3)

DEFGET(float, lua_checkfloat)
DEFGET(int, lua_checkint)
DEFGET(unsigned, lua_checkuint)
DEFGET(int64_t, luaL_checkinteger)
DEFGET(uint64_t, luaL_checkinteger)
DEFGET(double, luaL_checknumber)
DEFGET(bool, getBool)
DEFGETV(glm::vec, lua_tovec)
DEFGETV(glm::dvec, lua_todvec)
DEFGETV(glm::ivec, lua_toivec)
DEFGETV(glm::uvec, lua_touvec)
DEFGETV(glm::i64vec, lua_toi64vec)
DEFGETV(glm::u64vec, lua_tou64vec)
DEFGETV(glm::bvec, lua_tobvec)
DEFGETM(mat)
DEFGETM(dmat)

#undef DEFGET
#undef DEFGETV
#undef DEFGETM

template<typename T> inline T lua_getT(lua_State *L, int idx)
{
    return lua_type(L, idx) != LUA_TLIGHTUSERDATA
        ? _lua_getT<T>::get(L, idx) // multiple values pushed on the stack
        : *static_cast<T*>(lua_touserdata(L, idx)); // UNSAFE: raw pointer
}

void luaPushVAErrorStr(lua_State *L, const char *fmt, va_list argp);

int luaErrorHandler(lua_State *L);
std::string luaGatherCallstack(lua_State *L, int level = 0);

int luaForwardPCall(lua_State *L, int idx, void (*f)(lua_State*, int) = NULL);
int luaForwardCall(lua_State *L, unsigned skip, unsigned extra);
int luaForwardCallOpt(lua_State *L, unsigned skip, unsigned extra);
void luaShowError(lua_State *L);


struct LuaEnum
{
    const char * const name;
    const lua_Integer val;
};

#define luaRegisterEnum(e) { #e, e }
#define luaRegisterClassEnum(cls, e) { #e, cls::e }

void luaRegisterEnumInLib(lua_State *L, const LuaEnum *reg, int idx);
LuaObjGlue *luaGetValidObj(lua_State *L, int idx);
bool luaIsObject(lua_State *L, int idx);

template<typename T>
int _luaReturnObjSafe(lua_State *L, T *obj, const char *fmt = NULL, ...)
{
    if(obj)
        return obj->scriptReturnSelf(L);

    if(fmt)
    {
        va_list argp;
        va_start(argp, fmt);
        luaPushVAErrorStr(L, fmt, argp);
        va_end(argp);
        lua_error(L);
    }
    else
        luaL_error(L, "%s object is NULL", T::scriptdef.classname);

    // not reached
    return 0;
}

template<typename T>
int _luaReturnObjOrErrMsg(lua_State *L, T *obj, const char *fmt = NULL, ...)
{
    if(obj)
        return obj->scriptReturnSelf(L);

    if(fmt)
    {
        lua_pushnil(L);
        va_list argp;
        va_start(argp, fmt);
        luaPushVAErrorStr(L, fmt, argp);
        va_end(argp);
        return 2;
    }

    return 0;
}

#define luaReturnObjSafe(...) return _luaReturnObjSafe(L, __VA_ARGS__)
#define luaReturnObjOrErrMsg(...) return _luaReturnObjOrErrMsg(L, __VA_ARGS__)

inline std::string getString(lua_State *L, int idx = 1)
{
    return lua_isstring(L, idx) ? lua_tostring(L, idx) : std::string();
}

// MUST return valid object of correct type, else error
template<typename T>
T *luaGetObj(lua_State *L, int idx = 1)
{
    return static_cast<T*>(luaGetObjCheckDef(L, idx, T::scriptdef));
}

// accepts nothing or nil, otherwise must be valid object of correct type
template<typename T>
T *luaGetObjOrNil(lua_State *L, int idx = 1)
{
    return lua_isnoneornil(L, idx) ? NULL : luaGetObj<T>(L, idx);
}

// returns NULL if value is not an object, or not of correct object type
template<typename T>
T *luaGetObjOpt(lua_State *L, int idx = 1)
{
    return static_cast<T*>(luaGetObjOptDef(L, idx, T::scriptdef));
}

template<typename T>
class LuaClassBind
{
public:
    typedef T class_type;

    inline static T *This(lua_State *L) { return luaGetObj<T>(L, 1); }

    static int l_delete(lua_State *L)
    {
        // If This() didn't error, glue can't be NULL
        LuaObjGlue *glue = (LuaObjGlue*)lua_touserdata(L, 1);
        glue->onGC(L);
        luaReturnNil();
    }
private:
    LuaClassBind(); // non-instaceable; serves only as namespace
};


void luaRegisterClassDefKeep(lua_State *L, const ScriptDef& def, const luaL_Reg *methods, size_t nmth);

// Keeps the class metatable on the stack
template<typename T>
void luaRegisterClassKeep(lua_State *L)
{
    typedef typename T::class_type oclass;
    luaRegisterClassDefKeep(L, oclass::scriptdef, T::methods, Countof(T::methods));
}

template<typename T>
void luaRegisterClass(lua_State *L)
{
    LuaStackCheck(L, 0);
    luaRegisterClassKeep<T>(L);
    lua_pop(L, 1);
}

// Registers class metatable in the table at valid stack index idx
template<typename T>
void luaRegisterClassInLib(lua_State *L, int idx)
{
    LuaStackCheck(L, 0);
    idx = lua_absindex(L, idx);
    typedef typename T::class_type oclass;
    luaRegisterClassKeep<T>(L);
    lua_setfield(L, idx,  oclass::scriptdef.classname);
}

Scriptable *luaGetObjCheckDef(lua_State *L, int idx, const ScriptDef& def);
Scriptable *luaGetObjOptDef(lua_State *L, int idx, const ScriptDef& def);

// MUST return valid object of correct type, else error
Scriptable *luaGetScriptable(lua_State *L, int idx = 1);

// accepts nothing or nil, otherwise must be valid object of correct type
Scriptable *luaGetScriptableOrNil(lua_State *L, int idx = 1);


// idx is stack index of table to iterate over
// for the callback, key is at -2, value at -1
template<typename F>
size_t luaForeach(lua_State *L, F& f, int idx)
{
    LuaStackCheck(L, 0);
    idx = lua_absindex(L, idx);
    lua_pushnil(L);
    const int key = lua_gettop(L);
    size_t c = 0;
    while(lua_next(L, idx) != 0)
    {
        f(L);
        assert(lua_gettop(L) >= key); // function must not pop the key
        lua_settop(L, key);
        ++c;
    }
    return c;
}
template<typename F>
size_t luaForeach(lua_State *L, F& f, int idx, void *ud)
{
    LuaStackCheck(L, 0);
    idx = lua_absindex(L, idx);
    lua_pushnil(L);
    const int key = lua_gettop(L);
    size_t c = 0;
    while(lua_next(L, idx) != 0)
    {
        f(L, ud);
        assert(lua_gettop(L) >= key); // function must not pop the key
        lua_settop(L, key);
        ++c;
    }
    return c;
}

template<typename F>
size_t luaForeachi(lua_State *L, F& f, int idx)
{
    LuaStackCheck(L, 0);
    idx = lua_absindex(L, idx);
    const size_t sz = lua_rawlen(L, idx);
    for(size_t i = 1; i <= sz; ++i)
    {
        lua_rawgeti(L, idx, i);
        {
            LuaStackCheck(L, 0);
            f(L);
        }
        lua_pop(L, 1);
    }
    return sz;
}
template<typename F>
size_t luaForeachi(lua_State *L, F& f, int idx, void *ud)
{
    idx = lua_absindex(L, idx);
    const size_t sz = lua_rawlen(L, idx);
    for(size_t i = 1; i <= sz; ++i)
    {
        lua_rawgeti(L, idx, i);
        {
            LuaStackCheck(L, 0);
            f(L, ud);
        }
        lua_pop(L, 1);
    }
    return sz;
}

#pragma once

#include "scriptedlongjob.h"
#include "longjob.h"
#include "luaobj.h"

class ScriptedLongJob : public ScriptableT<ScriptedLongJob>, public SequentialLongJob
{
public:
    static const ScriptDef scriptdef;
    virtual ~ScriptedLongJob();
    static ScriptedLongJob *New(const char *name, lua_State *L, int idx);

    virtual void drawUI();
    virtual glm::vec4 getPercDone() const;
    virtual void onBegin();
    virtual void onFinish();

private:
    ScriptedLongJob(const char *name);
    virtual LongJobResult _run();
    virtual void _cancel();
    void _kill();

    int coref;
    int nargs;
    float percdone;
};

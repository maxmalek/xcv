#pragma once

namespace ScriptTypes
{
    enum Enum
    {
        GPUBUFFER,
        TEXTURE,
        SHADERWRAPPER,
        COMPUTESHADER,
        SIMPLESHADER,
        INNERWINDOW,
        SYSWINDOW,
        IMNODEEDITOR,
        IMNODE,
        IMNODECONNECTION,
        SHADERGRAPHEDITORWINDOW,
        SHADERNODE,
        MEMORYEDITOR,
        MESH,
        RENDEROBJECT,
        SCENE,
        FRAMEBUFFER,
        MEMBLOCK,
        NUMVEC,
        CAMERA,
        LONGJOB,
        PLUGIN,
        UICONSOLE,
        GPUTIMER,
    };
}

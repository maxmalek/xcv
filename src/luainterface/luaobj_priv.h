#pragma once

#include <string>
#include "macros.h"
#include "luaobj.h"

struct LuaObjGlue
{
    LuaObjGlue(Scriptable *o);
    ~LuaObjGlue();
    void unbind();
    void onGC(lua_State *L);
    CountedPtr<Scriptable> obj; // always == object that holds the glue
    const ScriptDef * const objdef; // same as obj->def
    bool isgc; // is/was GC'd?
    std::string deletionCallstack;
#ifdef _DEBUG
    std::string creationCallstack;
#endif
};

// do not return
void NORETURN luaObjTypeError(lua_State *L, int idx,  const char *needtype);
void NORETURN luaObjDeletedError(lua_State *L, int idx,  const char *needtype);


#include "luainternal.h"
#include <sstream>
#include "platform.h"

void luaPushVAErrorStr(lua_State *L, const char *fmt, va_list argp)
{
    luaL_where(L, 1);
    lua_pushvfstring(L, fmt, argp);
    lua_concat(L, 2);
}

void luaRegisterEnumInLib(lua_State * L, const LuaEnum * reg, int idx)
{
    LuaStackCheck(L, 0);
    idx = lua_absindex(L, idx);
    for( ; reg->name; ++reg)
    {
        lua_pushinteger(L, reg->val);
        lua_setfield(L, idx, reg->name);
    }
}

// Returns NULL if not valid
// FIXME: This will very likely blow up with objects from other libs that are not castable to LuaObjGlue!
LuaObjGlue *luaGetValidObj(lua_State * L, int idx)
{
    LuaObjGlue *p;
    return (lua_type(L, 1) == LUA_TUSERDATA // filter out light userdata, those are not valid objects
        && ((p = (LuaObjGlue*)lua_touserdata(L, 1))) // NULL if not a userdata object
        && p->obj) // heavy userdata can be casted to this
        ? p : NULL;
}

// must be heavy userdata with a metatable attached
bool luaIsObject(lua_State *L, int idx)
{
    LuaStackCheck(L, 0);
    const int ty = lua_type(L, idx);
    bool ret = false;
    if(ty == LUA_TUSERDATA)
    {
        ret = !!lua_getmetatable(L, idx);
        if(ret)
            lua_pop(L, 1);
    }
    return ret;
}

void luaRegisterClassDefKeep(lua_State * L, const ScriptDef & def, const luaL_Reg *methods, size_t nmth)
{
    DEBUG_LOG("Register Lua class %s", def.classname);

    LuaStackCheck(L, 1);
    // []
    luaL_newmetatable(L, def.classname);
    // [t]
    lua_pushvalue(L, -1);
    // [t][t]
    lua_setfield(L, -2, "__index");  // t.__index = t
    // [t]
    for(unsigned i = 0; i < nmth; ++i)
    {
        const luaL_Reg *reg = methods + i;
        if(reg->func)
        {
            assert(reg->name);
            lua_pushcfunction(L, reg->func);
            lua_setfield(L, -2, reg->name);
        }
    }
    // [t]
    if(def.base)
    {
        int lty = luaL_getmetatable(L, def.base->classname);
        assert(lty == LUA_TTABLE && "Forgot to register base class?");
        // [t][mt]
        lua_pushvalue(L, -1);
        // [t][mt][mt]
        lua_setmetatable(L, -3);
        // [t][mt]
        lua_setfield(L, -2, "__baseclass");
        // [t]
    }
    // FIXME: else derive from generic object class, that has some methods?
    luaClassRegisterDefaultMethods(L);
}

Scriptable * luaGetObjCheckDef(lua_State * L, int idx, const ScriptDef & def)
{
    void *p = lua_toheavyuserdata(L, idx);
    LuaObjGlue *glue = static_cast<LuaObjGlue*>(p);
    if(!glue || !luaIsOfClass(glue->objdef, def.type))
        luaObjTypeError(L, idx, def.classname);
    if(!glue->obj)
        luaObjDeletedError(L, idx, def.classname);

    return glue->obj.content();
}

Scriptable * luaGetObjOptDef(lua_State * L, int idx, const ScriptDef & def)
{
    void *p = lua_toheavyuserdata(L, idx);
    LuaObjGlue *glue = static_cast<LuaObjGlue*>(p);
    if(!glue || !luaIsOfClass(glue->objdef, def.type))
        return NULL;
    if(!glue->obj)
        luaObjDeletedError(L, idx, glue->objdef->classname);

    return glue->obj.content();
}

Scriptable *luaGetScriptableOrNil(lua_State *L, int idx)
{
    return lua_isnoneornil(L, idx) ? NULL : luaGetScriptable(L, idx);
}

Scriptable * luaGetScriptable(lua_State * L, int idx)
{
    void *p = lua_toheavyuserdata(L, idx);
    LuaObjGlue *glue = static_cast<LuaObjGlue*>(p);
    if(!glue)
        luaObjTypeError(L, idx, "<any scriptable>");
    if(!glue->obj)
        luaObjDeletedError(L, idx, glue->objdef->classname);
    return glue->obj.content();
}

static std::string luaFormatStackInfo(lua_State *L, int level = 1)
{
    lua_Debug ar;
    std::ostringstream os;
    if (lua_getstack(L, level, &ar) && lua_getinfo(L, "Sln", &ar))
    {
        os << ar.short_src << ":" << ar.currentline
            << " ([" << ar.what << "] "  << ar.namewhat << " " << (ar.name ? ar.name : "(?)") << ")";
    }
    else
    {
        os << "???:0";
    }

    return os.str();
}

std::string luaGatherCallstack(lua_State *L, int level)
{
    lua_Debug dummy;
    std::ostringstream os;
    while(true)
    {
        int gotstack = lua_getstack(L, level, &dummy);
        if(!gotstack)
            break;
        os << luaFormatStackInfo(L, level) << "\n";
        ++level;
    }
    return os.str();
}



int luaForwardPCall(lua_State * L, int idx, void (*f)(lua_State*, int))
{
    idx = lua_absindex(L, idx);
    int top = lua_gettop(L);
    // keep additional params on the stack and just call the function as it is
    int ret = lua_pcall(L, top - idx, -1, 0); // minus extra args and function, no error handler
    if (ret != LUA_OK && lua_isrealstring(L, -1))
    {  /* error object is a string? */
        luaL_where(L, 1);  /* add extra info */
        lua_insert(L, -2);
        lua_concat(L, 2);
    }
    if(f) 
        f(L, ret); // Allow adding extra info or return values
    if(ret != LUA_OK)
        return lua_error(L);  /* propagate error */
    // return whatever the function returned
    return lua_gettop(L) - (idx - 1); // minus first param
}

// rotates 'extra' elements on top of the stack under the function (so that they will be returned before the stuff returned by the function)
// skips 'skip' elments before function to call, passes what remains on the stack as args to the function.
// intended to be called like: return luaForwardCall(...)
int luaForwardCall(lua_State * L, unsigned skip, unsigned extra)
{
    int top = lua_gettop(L);
    if(extra)
    {
        const int inspos = skip + 1;
        assert(inspos > 0);
        lua_rotate(L, inspos, (int)extra);
    }
    const int nargs = top - int(skip + 1 + extra); // subtract 1 more for the function
    if(nargs < 0)
        luaL_error(L, "forward callable missing");
    const int t = top - nargs - 1; // one below the function
    lua_call(L, nargs, -1); // omit self and function
    top = lua_gettop(L);
    int ret = top - t; // how many things the function returned...
    assert(ret >= 0);
    return ret + int(extra); // ... plus the extra stuff that was rotated under previously
}
int luaForwardCallOpt(lua_State * L, unsigned skip, unsigned extra)
{
    int top = lua_gettop(L);
    if(extra)
    {
        const int inspos = skip + 1;
        assert(inspos > 0);
        lua_rotate(L, inspos, (int)extra);
    }
    const int nargs = top - int(skip + 1 + extra); // subtract 1 more for the function
    if(nargs < 0 || (nargs == 0 && lua_isnoneornil(L, -1)))
        return int(extra);
    const int t = top - nargs - 1; // one below the function
    lua_call(L, nargs, -1); // omit self and function
    top = lua_gettop(L);
    int ret = top - t; // how many things the function returned...
    assert(ret >= 0);
    return ret + int(extra); // ... plus the extra stuff that was rotated under previously
}

// Expects error on top of stack
void luaShowError(lua_State * L)
{
    lua_getglobal(L, "onError");
    lua_insert(L, -2);
    if(lua_pcall(L, 1, 0, 0) == LUA_OK)
        return;

    // Default error handler
    const char *msg = lua_tostring(L, -1);
    luaL_traceback(L, L, msg, 1);
    const char *full = lua_tostring(L, -1);
    logerror("%s", full);
    platform::msgbox(full, "Unhandled Lua error");
}

static void printCallstack(lua_State *L, const char *errmsg = NULL, int level = 0)
{
    if(errmsg)
        logerror("Lua error: %s", errmsg);
    logerror("%s\n", luaGatherCallstack(L, level).c_str());
}

static void pushCallstack(lua_State *L, int level = 0)
{
    lua_pushstring(L, luaGatherCallstack(L, level).c_str());
}


int luaErrorHandler(lua_State *L)
{
    // [err]
    lua_pushliteral(L, "\n");
    // [\n][err]
    pushCallstack(L, 1);
    // [err][\n][stack]
    lua_concat(L, 3);
    // [err+stack]
    luaShowError(L);
    return 1;
}

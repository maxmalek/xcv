#include "luafuncs.h"
#include "luainternal.h"
#include "platform.h"
#include "natsort/strnatcmp.h"
#include "log.h"
#include "filesystem.h"
#include "scriptedlongjob.h"
#include "SDL.h"
#include "pluginapi.h"

// for global functions


// based on luaB_print from lbaselib.c
static int printx (LL::Enum level, lua_State *L) {
  int n = lua_gettop(L);  /* number of arguments */
  int i;
  lua_getglobal(L, "tostring");
  for (i=1; i<=n; i++) {
    const char *s;
    lua_pushvalue(L, -1);  /* function to be called */
    lua_pushvalue(L, i);   /* value to print */
    lua_call(L, 1, 1);
    s = lua_tostring(L, -1);  /* get result */
    if (s == NULL)
      return luaL_error(L, "'tostring' must return a string to 'print'");
    if (i>1) logx(level, 0, "\t");
    logx(level, 0, "%s", s);
    lua_pop(L, 1);  /* pop result */
  }
  logx(level, 1, "");
  return 0;
}

luaFunc(print)
{
    return printx(LL::NORMAL, L);
}

luaFunc(errprint)
{
    return printx(LL::ERR, L);
}

luaFunc(infoprint)
{
    return printx(LL::INFO, L);
}

luaFunc(debugprint)
{
    return printx(LL::DEBUG, L);
}

luaFunc(devprint)
{
    return printx(LL::DEBUG2, L);
}

luaFunc(msgbox)
{
    platform::msgbox(luaL_checkstring(L, 1), lua_tostring(L, 2));
    luaReturnNil();
}

luaFunc(msgboxYesNo)
{
    luaReturnBool(platform::msgboxYesNo(luaL_checkstring(L, 1), lua_tostring(L, 2)));
}

// FIXME: this function is wrong for heavy userdata not based on LuaObjGlue (e.g. added by 3rd party)
// Should add an additional metatable check or something
luaFunc(isValidObj)
{
    luaReturnBool(!!luaGetValidObj(L, 1));
}

// True exactly if this used to be a valid object but it has been deleted
luaFunc(isDeadObj)
{
    LuaObjGlue *p;
    return (lua_type(L, 1) == LUA_TUSERDATA) // filter out light userdata, those are not valid objects
        && ((p = (LuaObjGlue*)lua_touserdata(L, 1))) // NULL if not a userdata object
        && !p->obj; // ->obj is NULL if object was deleted
}

luaFunc(natless) // string.natless
{
    luaReturnBool(strnatcmp(luaL_checkstring(L, 1), luaL_checkstring(L, 2)) < 0);
}

luaFunc(wildcard)
{
    luaReturnBool(wildcardMatch(luaL_checkstring(L, 1), luaL_checkstring(L, 2)));
}

luaFunc(wildcardcase)
{
    luaReturnBool(wildcardMatchNoCase(luaL_checkstring(L, 1), luaL_checkstring(L, 2)));
}

luaFunc(files) // io.files
{
    std::vector<std::string> tmp;
    if(!GetFileList(luaL_checkstring(L, 1), tmp))
        luaReturnNil();
    lua_createtable(L, (int)tmp.size(), 0);
    for(size_t i = 0; i < tmp.size(); ++i)
    {
        lua_pushstring(L, tmp[i].c_str());
        lua_rawseti(L, -2, i+1);
    }
    return 1;
}

luaFunc(breakpoint)
{
    platform::breakpoint();
    luaReturnNil();
}

luaFunc(topointer)
{
    void *p = const_cast<void*>(lua_topointer(L, 1));
    lua_pushlightuserdataornil(L, p);
    return 1;
}

luaFunc(addressof)
{
    const void *p = lua_topointer(L, 1);
    lua_Integer i = (lua_Integer)(intptr_t)(p);
    luaReturnInt(i);
}

struct LuaLongJob : public LuaClassBind<ScriptedLongJob>
{
    static const luaL_Reg methods[];

    static ScriptedLongJob *Create(lua_State *L)
    {
        const char *name = luaL_checkstring(L, 1);
        return ScriptedLongJob::New(name, L, 2);
    }

    luaFunc(new)
    {
        luaReturnObj(Create(L));
    }

    luaFunc(status)
    {
        const char *s = "";
        switch(This(L)->getStatus())
        {
        case LONGJOB_CANCELLED: s = "cancelled"; break;
        case LONGJOB_DONE: s = "done"; break;
        case LONGJOB_FAILED: s = "failed"; break;
        case LONGJOB_RUNNING: s = "running"; break;
        case LONGJOB_NOT_STARTED: s = "initial"; break;
        }
        luaReturnStr(s);
    }

};
const luaL_Reg LuaLongJob::methods[] =
{
    luaRegisterNewDelete(LuaLongJob),
    luaRegister(status)
};

luaFunc(dispatchJob)
{
    ScriptedLongJob *j = LuaLongJob::Create(L);
    int ret = _luaPushObjRet0(L, j); // important: register first, THEN add to Q
    g_mainLongExec.add(j);
    return ret;
}

luaFunc(opendialog)
{
    const char *filters = luaL_optstring(L, 1, NULL);
    const char *path = luaL_optstring(L, 2, NULL);
    char *res = platform::openFileDialog(filters, path);
    lua_pushstringornil(L, res);
    free(res);
    return 1;
}

luaFunc(savedialog)
{
    const char *filters = luaL_optstring(L, 1, NULL);
    const char *path = luaL_optstring(L, 2, NULL);
    char *res = platform::saveFileDialog(filters, path);
    lua_pushstringornil(L, res);
    free(res);
    return 1;
}

luaFunc(exists)
{
    luaReturnBool(platform::fileExists(lua_checkrealstring(L, 1)));
}

luaFunc(openurl)
{
    const char *s = luaL_checkstring(L, 1);
    const bool ok = platform::openURL(s);
    luaReturnBool(ok);
}

luaFunc(opendefault)
{
    const char *s = luaL_checkstring(L, 1);
    const bool ok = platform::openDefaultAssoc(s);
    luaReturnBool(ok);
}

luaFunc(filesize)
{
    uint64_t sz = 0;
    if(!platform::fileExists(lua_checkrealstring(L, 1), &sz))
        luaReturnNil();

    luaReturnInt(sz);
}

luaFunc(getSupportedImageFormats)
{
    lua_pushstring(L, pluginapi::fileExtLoad.c_str());
    lua_pushstring(L, pluginapi::fileExtSave.c_str());
    return 2;
}


static const luaL_Reg functab[] =
{
    luaRegister(print),
    luaRegister(errprint),
    luaRegister(infoprint),
    luaRegister(debugprint),
    luaRegister(devprint),
    luaRegister(isValidObj),
    luaRegister(isDeadObj),
    luaRegister(dispatchJob),
    luaRegister(getSupportedImageFormats),
    {NULL, NULL}
};


static const luaL_Reg os_ext[] =
{
    luaRegister(msgbox),
    luaRegister(msgboxYesNo),
    luaRegister(savedialog),
    luaRegister(opendialog),
    luaRegister(openurl),
    luaRegister(opendefault),
    {NULL, NULL}
};

static const luaL_Reg string_ext[] =
{
    luaRegister(natless),
    luaRegister(wildcard),
    luaRegister(wildcardcase),
    {NULL, NULL}
};

static const luaL_Reg io_ext[] =
{
    luaRegister(files),
    luaRegister(exists),
    luaRegister(filesize),
    {NULL, NULL}
};

static const luaL_Reg debug_ext[] =
{
    luaRegister(breakpoint),
    luaRegister(topointer),
    luaRegister(addressof),
    {NULL, NULL}
};

void register_lua_funcs(lua_State *L)
{
    LuaStackCheck(L, 0);

    luaRegisterClass<LuaLongJob>(L);

    lua_pushglobaltable(L);
    luaL_setfuncs(L, functab, 0);
    lua_pop(L, 1);

    lua_getglobal(L, "string");
    luaL_setfuncs(L, string_ext, 0);
    lua_pop(L, 1);

    lua_getglobal(L, "io");
    luaL_setfuncs(L, io_ext, 0);
    lua_pop(L, 1);

    lua_getglobal(L, "debug");
    luaL_setfuncs(L, debug_ext, 0);
    lua_pop(L, 1);

    lua_getglobal(L, "os");
    luaL_setfuncs(L, os_ext, 0);
    lua_pop(L, 1);

    bool debug = false;
#ifdef _DEBUG
    debug = true;
#endif
    lua_pushboolean(L, debug);
    lua_setglobal(L, "_DEBUG");
}


#include "luaobj_priv.h"
#include "luastate.h"
#include "luainternal.h"

void LuaContext::_setglobal(const char * name)
{
    LuaStackCheck(_L, -1);

    // [value]
    lua_pushglobaltable(_L);
    // [value][G]
    lua_pushstring(_L, name);
    // [value][G][name]
    lua_rotate(_L, -3, -1);
    // [G][name][value]
    lua_rawset(_L, -3);
    // [G]
    lua_pop(_L, 1);
}

void LuaContext::_setfield(int idx, const char * name)
{
    lua_setfield(_L, idx, name);
}

void LuaContext::_pop(int n)
{
    lua_pop(_L, n);
}

bool LuaContext::_DoCall(lua_State *L, int nparams, int nrets, bool haserrh)
{
    return _DoCallEx(L, nparams, nrets, haserrh) > -1;
}

int LuaContext::_DoCallEx(lua_State * L, int nparams, int nrets, bool haserrh)
{
    const int oldtop = lua_gettop(L);
    const int base = oldtop - nparams - 1 - !!haserrh; // stack base without function, params, or error handler
    const int fidx = oldtop - nparams; // index of function to call
    assert(fidx > 0);
    const int errh = haserrh ? fidx - 1 : 0; // error handler is under function, if present
    assert(!haserrh || lua_isfunction(L, errh));
    const int r = lua_pcall(L, nparams, nrets, errh);
    if(r == LUA_OK)
    {
        if(haserrh)
            lua_remove(L, errh); // remove the error handler under the return values
        const int newtop = lua_gettop(L);
        assert(base <= newtop);
        const int actualrets = newtop - base;
        assert(actualrets >= 0);
        assert(nrets == -1 || nrets == actualrets);
        return actualrets;
    }
    lua_pop(L, 1 + !!haserrh); // pop error handler and error object
    return -1;
}

void LuaContext::_lookupglobal(const char * name)
{
    lua_getglobal(_L, name);
}

void LuaContext::_pushint(int v)
{
    lua_pushinteger(_L, v);
}

void LuaContext::_pushuint(unsigned v)
{
    lua_Unsigned u = v;
    lua_pushinteger(_L, u);
}

void LuaContext::_pushfloat(float v)
{
    lua_pushnumber(_L, (lua_Number)v);
}

void LuaContext::_pushdouble(double v)
{
    lua_pushnumber(_L, v);
}

void LuaContext::_pushbool(bool v)
{
    lua_pushboolean(_L, v);
}

void LuaContext::_pushstr(const char * v)
{
    lua_pushstring(_L, v);
}

void LuaContext::pushobj(Scriptable * v)
{
    if(v)
        v->scriptReturnSelf(_L);
    else
        pushnil();
}

void LuaContext::pushnil()
{
    lua_pushnil(_L);
}

void LuaContext::pushraw(void * p)
{
    lua_pushlightuserdata(_L, p);
}

int LuaContext::forEach(int idx, ForeachFunc0 f)
{
    return lua_istable(_L, idx)
        ? (int)luaForeach(_L, f, idx)
        : -1;
}
int LuaContext::forEach(int idx, ForeachFunc f, void * user)
{
    return lua_istable(_L, idx)
        ? (int)luaForeach(_L, f, idx, user)
        : -1;
}

int LuaContext::forEachi(int idx, ForeachFunc0 f)
{
    return lua_istable(_L, idx)
        ? (int)luaForeachi(_L, f, idx)
        : -1;
}
int LuaContext::forEachi(int idx, ForeachFunc f, void * user)
{
    return lua_istable(_L, idx)
        ? (int)luaForeachi(_L, f, idx, user)
        : -1;
}


// -----------------------------------------------------


static const char * const OBJ_TABLE_NAME = ".Objs";
static const char * const OBJ_PERSISTENT_TABLE_NAME = ".PObjs";
static const char * const LUA_OBJ_TABLE_NAME = ".LObjs";
static const char * const OBJ_FINALIZER_NAME = ".Obj.Finalizer";

inline static void getObjRegTable(lua_State *L, ScriptDef::Flags flags)
{
    const char *tab = (flags & ScriptDef::PERSISTENT) ? OBJ_PERSISTENT_TABLE_NAME : OBJ_TABLE_NAME;
    lua_getfield(L, LUA_REGISTRYINDEX, tab);
    assert(lua_type(L, -1) == LUA_TTABLE);
}

bool luaIsOfClass(const ScriptDef *objdef, unsigned classty)
{
    for( ; objdef; objdef = objdef->base)
        if(objdef->type == classty)
            return true;
    return false;
}

bool luaIsOfClassName(const ScriptDef *objdef, const char *name)
{
    for( ; objdef; objdef = objdef->base)
        if(!strcmp(objdef->classname, name))
            return true;
    return false;
}

// This function is registered twice; once as method, and once as standalone function
luaFunc(isa)
{
    bool res = false;
    if(LuaObjGlue *glue = (LuaObjGlue*)luaGetValidObj(L, 1))
        res = luaIsOfClassName(glue->objdef, luaL_checkstring(L, 2));
    luaReturnBool(res);
}

void luaClassRegisterDefaultMethods(lua_State * L)
{
    lua_pushcfunction(L, l_isa);
    lua_setfield(L, -2, "isa");
} 

// -----------------------------------------------------

void ScriptTypeBase::setScriptDef(Scriptable *obj, const ScriptDef& def)
{
    assert(obj);
    obj->setScriptDef(def);
}

// Prevents GC'ing the calling object by putting it in the global registry.
// Is re-entrant -- if makePersistent() is called N times, must call makeNotPersistent() N times to unstick.
void Scriptable::makePersistent()
{
    assert(_L);
    // Not necessary to do this when always persistent
    if(def->flags & ScriptDef::PERSISTENT)
        return;

    LuaStackCheck(_L, 0);
    pushSelf(_L);
    // [self]
    lua_pushvalue(_L, -1);
    // [self][self]
    lua_gettable(_L, LUA_REGISTRYINDEX);
    // [self][c]
    lua_Integer c = lua_tointeger(_L, -1); // 0 if it was nil, all good
    lua_pop(_L, 1);
    // [self]
    lua_pushinteger(_L, c + 1);
    // [self][c]
    lua_settable(_L, LUA_REGISTRYINDEX);
    // []
}

void Scriptable::makeNotPersistent()
{
    // Not necessary to do this when always persistent
    if(!_L || def->flags & ScriptDef::PERSISTENT)
        return;

    LuaStackCheck(_L, 0);
    pushSelf(_L);
    // [self]
    lua_pushvalue(_L, -1);
    // [self][self]
    lua_gettable(_L, LUA_REGISTRYINDEX);
    // [self][c]
    lua_Integer c = lua_tointeger(_L, -1);
    lua_pop(_L, 1);
    assert(c);
    --c;
    // [self]
    if(c > 0)
        lua_pushinteger(_L, c);
    else
        lua_pushnil(_L); // erase entry
    // [self][c]
    lua_settable(_L, LUA_REGISTRYINDEX);
    // []
}

Scriptable::Scriptable()
 : glue(0), luaref(LUA_NOREF), bound(0), def(NULL)
{
}

void Scriptable::setScriptDef(const ScriptDef& d)
{
    // can either set the first script type,
    // or make the script type more specialized.
    // In the latter case, must inherit from the previously set type.
    assert(!def || luaIsOfClass(&d, def->type));

    def = &d;
}


// leaves the lua userdata for this on the stack
void Scriptable::initScriptBindings(lua_State * ls, const ScriptDef *scriptdef)
{
    assert(this->def == scriptdef && "Different runtime type");
    DEBUG_LOG("Lua bind obj %p (%s) flags = %d", this, scriptdef->classname, scriptdef->flags);
    if(scriptdef->flags & ScriptDef::PERSISTENT)
    {
        assert(!bound && "Persistent object was re-bound");
    }
    ++bound;
    assert(!glue);
    assert(!_L);

    LuaStackCheck(ls, 1);

    _L = lua_newthread(ls); // do NOT use this further, for now. stay in the calling thread's context.
    // [L]
    lua_pushlightuserdata(ls, _L);
    // [L][pL]
    lua_insert(ls, -2);
    // [pL][L]
    lua_settable(ls, LUA_REGISTRYINDEX); // REG[pL] = L


    glue = new (lua_newuserdata(ls, sizeof(LuaObjGlue))) LuaObjGlue(this);
    // [glue]
    luaref = Register(ls, scriptdef->classname, scriptdef->flags);
    // [glue]
#ifdef _DEBUG
    glue->creationCallstack = luaGatherCallstack(ls); // original call stack; L is empty right now
#endif
    DEBUG_LOG(" -> glue = %p, refid = %d", glue, luaref);
}

void Scriptable::unbindScript()
{
    if(glue)
    {
        assert(glue->obj == this);
        glue->unbind(); // may delete this; don't access this->... after here
    }
}

void Scriptable::pushSelf(lua_State *ls)
{
    assert(glue);
    assert(!glue->isgc);
    LuaStackCheck(ls, 1);

    // []
    getObjRegTable(ls, def->flags);
    // [T]
    lua_rawgeti(ls, -1, luaref);
    assert(lua_type(ls, -1) == LUA_TUSERDATA);
    // [T][o]
    lua_replace(ls, -2);
    // [o]
}

Scriptable::~Scriptable()
{
    unbindScript();
}

const char *Scriptable::debug_getCreationCallstack() const
{
#ifdef _DEBUG
    if(glue)
        return glue->creationCallstack.c_str();
#endif
    return NULL;
}

bool Scriptable::call(const char * mth, int nrets)
{
    lookupMethod(mth);
    return doCall(0, nrets);
}

bool Scriptable::callOpt(const char * mth, int nrets)
{
    return lookupMethodOpt(mth) && doCall(0, nrets);
}

void Scriptable::_pushOrRegisterSelf(lua_State * ls, const ScriptDef *scriptdef)
{
    if(glue) // already bound?
        pushSelf(ls);
    else
        initScriptBindings(ls, scriptdef);
}

void Scriptable::_registerSelfNoPush(lua_State *ls, const ScriptDef *scriptdef)
{
    if(!glue)
    {
        initScriptBindings(ls, scriptdef);
        lua_pop(ls, 1);
    }
}

void Scriptable::lookupMethod(const char * mth)
{
    LuaStackCheck(_L, 3);
    assert(mth);
    // []
    lua_pushcfunction(_L, luaErrorHandler);
    // [eh]
    pushSelf(_L);
    // [eh][this]
    lua_getfield(_L, -1, mth);
    // [eh][this][f]
    lua_insert(_L, -2);
    // [eh][f][this]
}

bool Scriptable::lookupMethodOpt(const char * mth)
{
    if(lua_State *L = _L)
    {
#ifdef _DEBUG
        const int top = lua_gettop(_L);
#endif

        lua_pushcfunction(L, luaErrorHandler);
        // [eh]
        pushSelf(L);
        // [eh][this]

#ifdef _DEBUG
        // Make sure the object has a metatable attached; otherwise the lua_getfield() call will cause a panic
        lua_getmetatable(L, -1);
        assert(lua_istable(L, -1) && "Object has no metatable!");
        lua_pop(L, 1);
#endif

        lua_getfield(L, -1, mth);

        // [eh][this][f]
        if(!lua_isnoneornil(L, -1))
        {
            lua_insert(L, -2); 
            // [eh][f][this]
#ifdef _DEBUG
            assert(top+3 == lua_gettop(L));
#endif
            return true;
        }
#ifdef _DEBUG
        lua_getfield(L, -2, "__name");
        const char *name = lua_tostring(L, -1);
        //DEBUG_LOG("Method %s not found for object with __name = %s", mth, name);
        assert(name && "No __name field for object");
        lua_pop(L, 1);
#endif

        lua_pop(L, 3);

#ifdef _DEBUG
        assert(top == lua_gettop(_L));
#endif
    }

    return false;
}

bool Scriptable::doCall(int nparams, int nrets)
{
    return LuaState::_DoCall(_L, nparams + 1, nrets, true);
}

int Scriptable::doCallEx(int nparams, int nrets)
{
    return LuaState::_DoCallEx(_L, nparams + 1, nrets, true);
}

static const char * const s_metamethods[] =
{
    // Those are reserved to attach the class table
    //"__index",    // Reserved to redirect lookups into the class table
    //"__newindex", // Reserved to point writes into the object's (meta)table
    //"__gc",       // Obviously reserved
    "__len",
    "__tostring"
};

// registers object on top of the stack. leaves object on the stack.
int Scriptable::Register(lua_State * L, const char * classname, ScriptDef::Flags flags)
{
    LuaStackCheck(L, 0);

    assert(lua_isuserdata(L, -1));
    // [o]
    lua_createtable(L, 0, 8);
    // [o][t]
    lua_pushvalue(L, -1);
    // [o][t][t]
    lua_pushvalue(L, -1);
    // [o][t][t][t]
    lua_setfield(L, -1, "__index"); // t.__index = t
    // [o][t][t]
    lua_setfield(L, -1, "__newindex"); // t.__newindex = t
    // [o][t]
    lua_getfield(L, LUA_REGISTRYINDEX, OBJ_FINALIZER_NAME);
    // [o][t][f]
    lua_setfield(L, -2, "__gc"); // t.__gc = f
    // [o][t]
    if(classname)
    {
        lua_pushstring(L, classname);
        // [o][t][str]
        lua_setfield(L, -2, "__name");
        // [o][t]
        int lty = luaL_getmetatable(L, classname); // cls = REG[classname]
        // /!\ If this assert triggers -- was this class registered to Lua?
        assert(lty == LUA_TTABLE);
        // [o][t][cls]
        lua_pushvalue(L, -1);
        // [o][t][cls][cls]
        lua_setfield(L, -3, "__class");
        // [o][t][cls]
        for(unsigned i = 0; i < Countof(s_metamethods); ++i)
        {
            const char *mm = s_metamethods[i];
            lua_getfield(L, -1, mm);
            // [o][t][cls][__len]
            lua_setfield(L, -3, mm);
            // [o][t][cls]
        }
        lua_setmetatable(L, -2); // setmetatable(t, cls)
    }
    // [o][t]
    lua_setmetatable(L, -2); // setmetatable(o, t)
    // [o]
    getObjRegTable(L, flags);
    // [o][T]
    lua_pushvalue(L, -2);
    // [o][T][o]
    const int ref = luaL_ref(L, -2); // so that T[ref] == o
    // [o][T]
    lua_pop(L, 1);
    // [o]
    return ref;
}


// Might leave crap on the stack, beware!
static const char *fancyname(lua_State * L, int idx)
{
    if (luaL_getmetafield(L, idx, "__name") == LUA_TSTRING)
        return lua_tostring(L, -1);
    else if (lua_type(L, idx) == LUA_TLIGHTUSERDATA)
        return "light userdata";

    return luaL_typename(L, idx);
}

// Stub function that works as a finalizer for all custom object types.
// Calls an object's (possibly overloaded/overriden) delete() method.
luaFunc(genericFinalizer)
{
    // [obj]
    if(luaGetValidObj(L, 1)) // possible that this is a persistent object deleted manually earlier, and the Lua glue was lingering until now
    {
        LuaObjGlue *glue = (LuaObjGlue*)lua_touserdata(L, 1);
        DEBUG_LOG("__gc %s (obj = %p, glue = %p, ref = %u)", glue->objdef->classname, glue->obj.content(), glue, glue->obj ? glue->obj->_getLuaRefID() : 0);
        glue->isgc = true;
        // [obj]
        lua_getfield(L, 1, "delete"); // invokes __index metamethod
        // [obj][delete]
        if(!lua_isnoneornil(L, -1))
        {
            lua_insert(L, -2);
            // [delete][obj]
            lua_pushcfunction(L, luaErrorHandler);
            // [delete][obj][errh]
            lua_insert(L, -3);
            // [errh][delete][obj]
            lua_pcall(L, 1, 0, -3); // need a pcall to make sure the dtor below is called.
            // [errh]
        }

        // Dealloc for good before Lua does the free()
        // Also deletes the actual object if the glue held the last reference to it.
        glue->~LuaObjGlue();
    }
    luaReturnNil();
}

static luaL_Reg globalfuncs[] =
{
    luaRegister(isa),
    {NULL, NULL}
};

static void createTableWithMode(lua_State *L, const char *mode)
{
    LuaStackCheck(L, 1);

    lua_newtable(L);
    // [t]
    lua_pushstring(L, mode);
    // [t][mode]
    lua_setfield(L, -2, "__mode");
    // [t]
    lua_pushvalue(L, -1);
    // [t][t]
    lua_setmetatable(L, -1);
    // [t]
}

void Scriptable::RegisterGlobalObjTable(lua_State * L)
{
    LuaStackCheck(L, 0);
    // []
    createTableWithMode(L, "kv");
    // [t]
    lua_setfield(L, LUA_REGISTRYINDEX, OBJ_TABLE_NAME);
    // []
    // ----------------------------------------------
    lua_newtable(L);
    // [t]
    lua_setfield(L, LUA_REGISTRYINDEX, OBJ_PERSISTENT_TABLE_NAME);
    // []
    // ----------------------------------------------
    createTableWithMode(L, "k");
    // [t]
    lua_setfield(L, LUA_REGISTRYINDEX, LUA_OBJ_TABLE_NAME);
    // []
    // ----------------------------------------------

    lua_pushcfunction(L, l_genericFinalizer);
    lua_setfield(L, LUA_REGISTRYINDEX, OBJ_FINALIZER_NAME);


    // Additional functions
    lua_pushglobaltable(L);
    luaL_setfuncs(L, globalfuncs, 0);
    lua_pop(L, 1);
}

LuaObjGlue::LuaObjGlue(Scriptable * o)
 : obj(o), objdef(o->def), isgc(false)
{
}

LuaObjGlue::~LuaObjGlue()
{
    unbind();
}

void LuaObjGlue::unbind()
{
    if(obj)
    {
        assert(obj->glue == this);

        lua_State * const L = obj->_L;
        LuaStackCheck(L, 0);

        const int ref = obj->luaref;
        obj->luaref = LUA_NOREF;
        obj->_L = NULL;
        obj->glue = NULL;

        getObjRegTable(L, objdef->flags);
        // [reg]
        luaL_unref(L, -1, ref); // obj will be garbage collected at some point after this
        // [reg]
        lua_pop(L, 1);

        obj = NULL; // deletes the object if we are the last one referencing it

        lua_pushlightuserdata(L, L);
        // [pL]
        lua_pushnil(L);
        // [pL][nil]
        lua_settable(L, LUA_REGISTRYINDEX); // REG[pL] = nil (drops references to L)
        // (empty stack)
        // L will be garbage collected at some point after this
    }
}

void LuaObjGlue::onGC(lua_State * L)
{
    // when manually deleting an object, record a callstack dump
    // to be able to tell where exactly this object got deleted,
    // so the problem can be easily fixed
    if(!isgc)
        deletionCallstack = luaGatherCallstack(L);
    if(obj)
        obj->onGC();
    // Since this unsets the internal CountedPtr, this will delete the object if
    // Lua was the last to hold onto it
    unbind();
}

static int typeerror (lua_State *L, int arg, const char *tname)
{
    const char *typearg = fancyname(L, arg);
    const char *msg = lua_pushfstring(L, "%s expected, got %s", tname, typearg);
    return luaL_argerror(L, arg, msg);
}

void NORETURN luaObjTypeError(lua_State * L, int idx, const char *needtype)
{
    typeerror(L, idx, needtype);
    END_NORETURN_FUNC;
}

void NORETURN luaObjDeletedError(lua_State * L, int idx, const char *needtype)
{
    LuaObjGlue *glue = (LuaObjGlue*)lua_touserdata(L, idx);
    const char *msg = lua_pushfstring(L, "%s already deleted: Callstack at time of deletion:\n%s",
        needtype, glue->deletionCallstack.c_str());
    luaL_argerror(L, idx, msg);
    END_NORETURN_FUNC;
}



LuaCallPrep::LuaCallPrep(lua_State *L)
: L(L), _top(L ? lua_gettop(L) : -1)
{
}

LuaCallPrep::~LuaCallPrep()
{
    if(_top != -1)
        lua_settop(L, _top);
}

int LuaCallPrep::top()
{
    return lua_gettop(L) - _top - 1; // minus function
}

Scriptable::CallEnv::CallEnv(Scriptable *o, const char *mth)
// important: must be executed in this order. top must be set before lookupMethod() is called
 : LuaCallPrep(o->_L), obj(o), good(o->lookupMethodOpt(mth))
{
}

int Scriptable::CallEnv::nargs()
{
     return top() - 2; // minus self and error handler
}


bool Scriptable::CallEnv::call(int nrets)
{
    return good ? obj->doCall(nargs(), nrets) : false;
}

int Scriptable::CallEnv::callEx(int nrets)
{
    return good ? obj->doCallEx(nargs(), nrets) : -1;
}

bool Scriptable::CallEnv::call(bool& ret)
{
    if(callEx(1) >= 0)
    {
        ret = getBool(obj->_L, -1);
        return true;
    }
    return false;
}



_LuaStackCheck::_LuaStackCheck(lua_State *L, int n)
    : _L(L)
    , _top(L ? lua_gettop(L) : 0)
    , _offs(n)
{}

_LuaStackCheck::~_LuaStackCheck()
{
    if(_L)
    {
        int top = lua_gettop(_L);
        assert(top == _top+_offs);
        (void)(top);
    }
}


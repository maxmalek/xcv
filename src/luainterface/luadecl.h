#pragma once

#include "luaobjtypes.h"

struct ScriptDef
{
    enum Flags
    {
        NONE = 0, // normal object
        PERSISTENT = 0x0001, // stays registered even if all Lua references are lost (is stored safely in registry)
    };
    inline ScriptDef(const char *cls, unsigned ty, const ScriptDef *b = 0, Flags f = Flags::NONE)
        : type(ty), classname(cls), base(b), flags(f) {}

    const unsigned type;
    const char * const classname;
    const ScriptDef * const base;
    const Flags flags;
};

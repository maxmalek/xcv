#pragma once

#include "luaobj.h"

struct lua_State;

class Scriptable;
struct LuaObjGlue;


class LuaState : public LuaContext
{
    friend class Scriptable;
    friend struct LuaObjGlue;

public:
    LuaState();
    virtual ~LuaState();

    void gc();

    bool loadFile(const char *file);

    bool call(const char *func);
    bool call(const char *func, const char *p0);
    int callEx(const char *func, int nparams, int nrets = -1);

    template<typename T>
    void setGlobal(const char *name, CountedPtr<T>& thing)
    {
        push(thing.content());
        _setglobal(name);
    }
    template<typename T>
    void setGlobal(const char *name, const T& thing)
    {
        push(thing);
        _setglobal(name);
    }
    template<typename T>
    void setGlobal(const char *name, T& thing)
    {
        push(thing);
        _setglobal(name);
    }

    template<typename T>
    void registerObj(T *obj, const char *classname = 0)
    {
        registerObjDef(obj, T::scriptdef, classname);
    }
    void registerObjDef(Scriptable *obj, const ScriptDef& def, const char *classname = 0);

    /*class Ctor : protected LuaCallPrep
    {
    public:
        Ctor(LuaState& s, const char *classname, const ScriptDef& def, const char *fname = "new");
        Scriptable *call();
        operator bool() const { return good; }
    private:
        const ScriptDef& _def;
        int nargs();
        bool _lookupFunc(const char *classname, const char *fname);
        const bool good;
    };*/

protected:
    void lookupGlobalFunc(const char *func);
    // return whether call successful
    bool doCall(int nparams, int nrets = 0, bool haserrh = false);
    // returns how many items were returned by the call; -1 on error
    int doCallEx(int nparams, int nrets = -1, bool haserrh = false);
};

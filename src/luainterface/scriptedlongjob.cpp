#include "scriptedlongjob.h"
#include "luainternal.h"
#include "platform.h"


const ScriptDef ScriptedLongJob::scriptdef("longjob", ScriptTypes::LONGJOB, NULL);


ScriptedLongJob::ScriptedLongJob(const char *name)
: SequentialLongJob(name), coref(LUA_NOREF), percdone(0)
{
}

ScriptedLongJob::~ScriptedLongJob()
{
}

ScriptedLongJob * ScriptedLongJob::New(const char *name, lua_State * L, int idx)
{
    luaL_checktype(L, idx, LUA_TFUNCTION);

    lua_State *co = lua_newthread(L);
    if(!co)
        return NULL;

    const int ref = luaL_ref(L, LUA_REGISTRYINDEX); // pops co

    idx = lua_absindex(L, idx);
    int nparams = lua_gettop(L) - idx;
    lua_xmove(L, co, nparams + 1); // move function and params to coro

    ScriptedLongJob *j = new ScriptedLongJob(name);
    j->coref = ref;
    j->nargs = nparams;
    return j;
}

void ScriptedLongJob::drawUI()
{
    callOpt("drawUI", 0);
}

glm::vec4 ScriptedLongJob::getPercDone() const
{
    return glm::vec4(percdone, -1, -1, -1);
}

// Prevent GC'ing this object in case only C++ code keeps a reference, until the job is finished
void ScriptedLongJob::onBegin()
{
    makePersistent();
}

void ScriptedLongJob::onFinish()
{
    makeNotPersistent();
}

LongJobResult ScriptedLongJob::_run()
{
    if(coref == LUA_NOREF)
        return LONGJOB_FAILED;
    assert(_L);
    lua_rawgeti(_L, LUA_REGISTRYINDEX, coref);
    lua_State *co = lua_tothread(_L, -1);
    assert(co);
    lua_pop(_L, 1);
    const int result = lua_resume(co, _L, nargs);
    nargs = 0; // initial args were passed; no more args in future calls
    switch(result)
    {
        case LUA_OK:
            _kill();
            return LONGJOB_DONE;

        case LUA_YIELD:
            if(lua_isnumber(co, 1))
                percdone = lua_tofloat(co, 1);
            lua_settop(co, 0);
            return LONGJOB_RUNNING;
    }
    // --- something went wrong; error message is on top of stack ---
    luaShowError(co);
    _kill();
    return LONGJOB_FAILED;
}

void ScriptedLongJob::_cancel()
{
    callOpt("onCancel", 0);
    _kill();
}

void ScriptedLongJob::_kill()
{
    if(coref != LUA_NOREF)
    {
        luaL_unref(_L, LUA_REGISTRYINDEX, coref),
        coref = LUA_NOREF;
    }
}

#pragma once

#include "luadecl.h"
#include "refcounted.h"

// Lua
struct lua_State;

// Custom
class Scriptable;
class LuaContext;
class LuaState;
struct LuaObjGlue;

// Specialized below
template<typename T>
struct _LuaPusher {};


// thin wrapper around lua_State
class LuaContext
{
protected:
    inline LuaContext() : _L(0) {}
    ~LuaContext() {}

public:
    template<typename T>
    inline void push(const T& v)
    {
        _LuaPusher<T>::Push(*this, v);
    }
    void push(const char *v) { _pushstr(v); }
    void push(char *v) { _pushstr(v); }
    void pushobj(Scriptable *v);
    void pushnil();
    void pushraw(void *p);

    typedef void (*ForeachFunc0)(lua_State*);
    typedef void (*ForeachFunc)(lua_State*, void*);
    int forEach(int idx, ForeachFunc0 f);
    int forEach(int idx, ForeachFunc f, void *user);
    int forEachi(int idx, ForeachFunc0 f);
    int forEachi(int idx, ForeachFunc f, void *user);

    lua_State *_getInternalState() { return _L; }

public:
    void _pushint(int v);
    void _pushuint(unsigned v);
    void _pushfloat(float v);
    void _pushdouble(double v);
    void _pushbool(bool v);
    void _pushstr(const char *v);

protected:
    // stack manipulation
    void _lookupglobal(const char *name);
    void _setglobal(const char *name);
    void _setfield(int idx, const char *name);
    void _pop(int n = 1);

    static bool _DoCall(lua_State *L, int nparams, int nrets, bool haserrh = false);
    static int _DoCallEx(lua_State *L, int nparams, int nrets, bool haserrh = false);

    lua_State *_L;
};

template<> struct _LuaPusher<int> { static void Push(LuaContext& S, int v)
{ S._pushint(v); }};
template<> struct _LuaPusher<unsigned> { static void Push(LuaContext& S, unsigned v)
{ S._pushuint(v); }};
template<> struct _LuaPusher<float> { static void Push(LuaContext& S, float v)
{ S._pushfloat(v); }};
template<> struct _LuaPusher<double> { static void Push(LuaContext& S, double v)
{ S._pushdouble(v); }};
template<> struct _LuaPusher<bool> { static void Push(LuaContext& S, bool v)
{ S._pushbool(v); }};


class LuaCallPrep
{
protected:
    LuaCallPrep(lua_State *L); // L can be NULL
    ~LuaCallPrep();
    int top();
    lua_State * const L;
private:
    const int _top;
};

// This base class exists for two reasons:
// 1) Compiler can do empty base class optimization
// 2) is a friend of Scriptable and does the actual private call (because isn't templatized)
class ScriptTypeBase
{
protected:
    static void setScriptDef(Scriptable *obj, const ScriptDef& def);
};

// All classes that inherit from scriptable MUST also inherit from ScriptType<CLASS>. Can inherit from this multiple times.
// Like this:
// class A : public ScriptableT<FOO> {...}
// class B : public A, private ScriptType<A> // <-- Must do this else B will be seen as script type of A !
template<typename T>
class ScriptType : protected ScriptTypeBase
{
    friend class Scriptable;
protected:
    ScriptType()
    {
        // Dirty cast! We know that *this* inherits from Scriptable, but the cast is only possible because we know the actual type T.
        // Casting the pointer directly (this -> T) is possibly dangerous, therefore cast twice implicitly to make sure the intenion is clear and it does the right thing.
        T *self = (T*)this;
        Scriptable *obj = self;
        setScriptDef(obj, T::scriptdef);
    }
};

// All classes that should be Lua-bindable must derive form this class.
// Each actual bound class must have a 'static const ScriptDef scriptdef;' member to define the properties.
class Scriptable : public Refcounted<REFCOUNT_NORMAL>, public LuaContext
{
    friend class LuaState;
    friend struct LuaObjGlue;
    friend class ScriptTypeBase;
public:
    static const ScriptDef scriptdef;

    void unbindScript();

    bool call(const char *mth, int nrets);
    bool callOpt(const char *mth, int nrets);

    // Must be already registered to Lua before calling this!
    template<typename T>
    void setMember(const char *var, const T& x)
    {
        pushSelf(_L); // Will assert fail if not registered
        push(x);
        _setfield(-1, var);
        _pop(1);
    }

    int scriptReturnSelf(lua_State *ls)
    {
        _pushOrRegisterSelf(ls, def);
        return 1;
    }
    int scriptRegisterSelf(lua_State *ls)
    {
        _registerSelfNoPush(ls, def);
        return 0;
    }
    /*template<typename T>
    static int scriptReturnSelf(lua_State *ls, T *this_)
    {
        return this_->scriptReturnSelf<T>(ls);
    }
    template<typename T>
    static int scriptRegister(lua_State *ls, T *this_)
    {
        return this_->scriptReturnSelf<T>(ls);
    }*/

    inline bool hasScript() const { return !!glue; }
    void makePersistent();
    void makeNotPersistent();

    void lookupMethod(const char *mth);
    bool lookupMethodOpt(const char *mth);

    // Lua callstack where the object was created, if known. NULL otherwise or in release mode.
    const char *debug_getCreationCallstack() const;

    class CallEnv : protected LuaCallPrep
    {
    public:
        CallEnv(Scriptable *o, const char *mth);
        bool call(int nrets);
        int callEx(int nrets);
        bool call(bool& ret);
        operator bool() const { return good; }
    private:
        int nargs();
        Scriptable * const obj;
        const bool good;
    };

    int _getLuaRefID() const { return luaref; }

protected:
    Scriptable();
    virtual ~Scriptable();
    void initScriptBindings(lua_State *ls, const ScriptDef *scriptdef);

    void pushSelf(lua_State *ls); // only call when bindings are already initialized!

    LuaObjGlue *glue; // allocated in Lua; garbage collected

private:
    virtual void onGC() {}
    void _pushOrRegisterSelf(lua_State *ls, const ScriptDef *scriptdef);
    void _registerSelfNoPush(lua_State *ls, const ScriptDef *scriptdef);
    bool doCall(int nparams, int nrets = 0);
    int doCallEx(int nparams, int nrets = 0);
    void setScriptDef(const ScriptDef& d);
    static int Register(lua_State *L, const char *classname, ScriptDef::Flags flags);
    static void RegisterGlobalObjTable(lua_State *L);

    // how often en object's Lua binding was established. Ideally this is no more than 1.
    // Persistent objects will cause an assert fail if they are bound more than once during their life time.
    unsigned bound;

    unsigned luaref;
    const ScriptDef * def;
};

// Shortcut
template<typename T>
class ScriptableT : public Scriptable, private ScriptType<T>
{
protected:
    virtual ~ScriptableT() {};
};


bool luaIsOfClass(const ScriptDef *objdef, unsigned classty);
bool luaIsOfClassName(const ScriptDef *objdef, const char *name);
void luaClassRegisterDefaultMethods(lua_State *L);


// Records stack top in ctor. Dtor asserts that exactly n more elements are now on the stack.
// Works with negative n too. Ignore if L == NULL.
class _LuaStackCheck
{
public:
    _LuaStackCheck(lua_State *L, int n);
    ~_LuaStackCheck();
private:
    lua_State * const _L;
    const int _top;
    const int _offs;
};

#ifdef _DEBUG
#define LuaStackCheck(ctx, n) _LuaStackCheck _luastkchk(ctx, n)
#else
#define LuaStackCheck(ctx, n)
#endif

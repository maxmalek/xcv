#include "luastate.h"
#include "luafuncs.h"
#include "luainternal.h"
#include "log.h"
#include <assert.h>
#include "luaobj.h"
#include <vector>


static int panic(lua_State *L)
{
    const char *err = lua_tostring(L, -1);
    logerror("PANIC: error in Lua call: %s", err);
    assert(false);
    return 0;
}

// This is not a full-fledged allocator by far.
// But it does help to reduce allocation spam a bit.
struct LuaAllocHelper
{
    // These settings result in around 280k standby memory
    enum
    {
        BLOCKSIZE = 8,
        MAXBLOCKSIZE = 128,
        MAXENTRIES = 256
    };
    std::vector<void*> stores[MAXBLOCKSIZE/BLOCKSIZE];

    LuaAllocHelper()
    {
        for(size_t i = 0; i < Countof(stores); ++i)
            stores[i].reserve(MAXENTRIES);
    }

    ~LuaAllocHelper()
    {
        for(size_t i = 0; i < Countof(stores); ++i)
        {
            std::vector<void*>& v = stores[i];
            const size_t sz = v.size();
            for(size_t k = sz; k < sz; ++i)
                free(v[k]);
        }
    }

    void Free(void *p, size_t sz)
    {
        assert(p && sz);
        size_t idx = (sz-1) / BLOCKSIZE;
        if(idx < Countof(stores))
        {
            std::vector<void*>& v = stores[idx];
            if(v.size() < MAXENTRIES)
            {
                //printf("standby %p, sz = %u\n", p, sz);
                v.push_back(p);
                return;
            }
        }
        //printf("free %p, sz = %u\n", p, sz);
        free(p);
    }

    void *Alloc(size_t sz)
    {
        assert(sz);
        size_t idx = (sz-1) / BLOCKSIZE;
        if(idx < Countof(stores))
        {
            std::vector<void*>& v = stores[idx];
            if(!v.empty())
            {
                void *p = v.back();
                v.pop_back();
                //printf("Recycle %p, sz = %u\n", p, sz);
                return p;
            }
        }
        void *p = BlockRealloc(NULL, sz);
        //printf("malloc %p, sz = %u\n", p, sz);
        return p;
    }

    static void *BlockRealloc(void *p, size_t sz) // Rounds up to block size
    {
        sz = ((sz + BLOCKSIZE - 1) / BLOCKSIZE) * BLOCKSIZE;
        return realloc(p, sz);
    }
};

static void *allocHelper(void *ud, void *ptr, size_t osize, size_t nsize)
{
    LuaAllocHelper *ah = (LuaAllocHelper*)ud;
    void *ret = NULL;
    if(ptr)
    {
        if(!nsize)
            ah->Free(ptr, osize);
        else if(osize == nsize)
            ret = ptr;
        else
            ret = LuaAllocHelper::BlockRealloc(ptr, nsize);
    }
    else if(nsize)
        ret = ah->Alloc(nsize);

    return ret;
}

LuaState::LuaState()
{
    _L = lua_newstate(allocHelper, new LuaAllocHelper);
    assert(_L);
    lua_atpanic(_L, panic);
    luaL_openlibs(_L);
    register_lua_funcs(_L);
    Scriptable::RegisterGlobalObjTable(_L);
}

LuaState::~LuaState()
{
    void *ud = NULL;
    lua_getallocf(_L, &ud);
    LuaAllocHelper *ah = (LuaAllocHelper*)ud;
    lua_close(_L);
    delete ah;
}

void LuaState::gc()
{
    lua_gc(_L, LUA_GCCOLLECT, 0);
}

bool LuaState::loadFile(const char * file)
{
    return call("dofile", file);
}

void LuaState::registerObjDef(Scriptable * obj, const ScriptDef & def, const char *classname)
{
    if(!obj->glue)
    {
        // If the class is to be overridden, look up the right function first
        if(classname)
            lookupGlobalFunc("switchclass");
        // Register the object, leave it on the stack
        obj->initScriptBindings(_L, &def);
        if(classname)
        {
            lua_getfield(_L, LUA_REGISTRYINDEX, classname);
            assert(lua_type(_L, -1) == LUA_TTABLE);
            _DoCall(_L, 2, 0, true); // Pop everything
        }
        else
            lua_pop(_L, 1); // Pop leftover object
    }
}

void LuaState::lookupGlobalFunc(const char *func)
{
    lua_pushcfunction(_L, luaErrorHandler);
    lua_getglobal(_L, func);
}

bool LuaState::doCall(int nparams, int nrets, bool haserrh)
{
    return _DoCall(_L, nparams, nrets, haserrh);
}

int LuaState::doCallEx(int nparams, int nrets, bool haserrh)
{
    return _DoCallEx(_L, nparams, nrets, haserrh);
}

bool LuaState::call(const char * func)
{
    lookupGlobalFunc(func);
    return doCall(0, 0, true);
}

bool LuaState::call(const char * func, const char * p0)
{
    lookupGlobalFunc(func);
    lua_pushstring(_L, p0);
    return doCall(1, 0, true);
}

int LuaState::callEx(const char * func, int nparams, int nrets)
{
     lookupGlobalFunc(func);
     return doCallEx(nparams, nrets, true);
}

/*
LuaState::Ctor::Ctor(LuaState & s, const char *classname, const ScriptDef& def, const char * fname)
: LuaCallPrep(s.L), _def(def), good(_lookupFunc(classname, fname))
{
}

Scriptable * LuaState::Ctor::call()
{
    Scriptable *obj = NULL;
    if(LuaContext::_DoCallEx(L, nargs(), 1, true) > 0)
        obj = luaGetObjCheckDef(L, -1, _def);
    return obj;
}

int LuaState::Ctor::nargs()
{
    return top() - 1; // minus error handler
}

bool LuaState::Ctor::_lookupFunc(const char * classname, const char * fname)
{
    lua_getglobal(L, classname);
    if(!lua_istable(L, -1))
        return false;

    lua_pushcfunction(L, luaErrorHandler);
    lua_getfield(L, -1, fname);
}
*/


#pragma once

#ifdef WIN32
# define PLUGIN_API_CALLCONV __cdecl
#else
# define PLUGIN_API_CALLCONV __attribute__((__cdecl__))
#endif

#define compile_assert(x) do { switch(0) { case 0: case x:; } } while(0)

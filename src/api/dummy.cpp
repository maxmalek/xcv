// This file only exists to make this a valid compile target,
// and to perform some compile checks.

#include "api_plugin.h"
#include "api_macros.h"

static void _plugin_compile_checks()
{
    compile_assert(sizeof(ESizeofPluginApiDef) == sizeof(unsigned));
    compile_assert(sizeof(ESizeofPluginApiFuncs) == sizeof(unsigned));
    compile_assert(sizeof(EPluginApiReserved) == sizeof(unsigned));
}

// Definitions required for plugin functionality

// If you need support for 64bit image types, #include <stdint.h> before this file.
// (See api_types.h for more info)

#pragma once

#include "api_funcs.h"

enum EPluginApiVersion : unsigned int { PLUGIN_API_VERSION = 0 };
enum ESizeofPluginApiDef : unsigned int;
enum ESizeofPluginApiDesc : unsigned int;
enum ESizeofPluginApiFuncs : unsigned int;
enum ESizeofPluginImports : unsigned int;
enum EPluginApiDefMagic : unsigned int;
enum EPluginApiDescMagic : unsigned int;
enum EPluginApiFuncsMagic : unsigned int;


#ifdef _WIN32
#define EXPORT_API __declspec(dllexport)
#else
#define EXPORT_API __attribute__((visibility ("default")))
#endif


// Plugin must allocate the image data, write the pointer to pdata, and write to info.
typedef IOResult (*ImageLoadFileFunc)(const char *fn, void **pdata, Blob3DInfo *info);

// format is a lowercase string describing the target format of the data to save as (lowercase file extension). Return IORESULT_INVALID_FORMAT if unsupported
// (core will try other plugins if this one is unsuccessful)
typedef IOResult (*ImageSaveFileFunc)(const char *fn, const char *format, APITextureHandle tex);

// Called by the core on pointers returned via a plugin's loadImageFile() function.
// Must be defined when loadImageFile() is defined.
typedef void (*FreeImageDataFunc) (void *);

// Return static immutable string, e.g. "png;bmp;tga" to indicate file types supported for reading/writing
typedef const char * (*GetImageFormatsAvailLoadFunc)();
typedef const char * (*GetImageFormatsAvailSaveFunc)();

// Functions exported by a plugin
struct PluginAPIFuncs
{
    // Header
    ESizeofPluginApiFuncs structsize; // = sizeof(APIFuncs)
    EPluginApiFuncsMagic _magic;
    EPluginApiReserved _reserved;
    // Body (each function is optional and can be NULL, except as noted above)
    FreeImageDataFunc freeImageData; // must be present if loadImageFile() is present
    ImageLoadFileFunc loadImageFile;
    ImageSaveFileFunc saveImageFile;
    GetImageFormatsAvailLoadFunc getImageFormatsAvailLoad;
    GetImageFormatsAvailSaveFunc getImageFormatsAvailSave;
};

struct PluginAPIDesc
{
    // Header
    ESizeofPluginApiDesc structsize;
    EPluginApiDescMagic _magic;
    EPluginApiReserved _reserved;
    // Body
    int priority;
};

// This structure is filled by a plugin
struct PluginAPIDef
{
    // Header
    ESizeofPluginApiDef structsize; // = sizeof(PluginDef)
    EPluginApiDefMagic _magic;
    EPluginApiVersion version; // = PLUGIN_API_VERSION
    ESizeofPluginImports importsize; // = sizeof(PluginApiImportFuncs)
    EPluginApiReserved _reserved;
    // Body
    const PluginAPIDesc *desc; // optional, can be NULL
    const PluginAPIFuncs *funcs; // optional, can be NULL
};


enum ESizeofPluginApiDef : unsigned int { PLUGIN_DEF_SIZE = unsigned(sizeof(PluginAPIDef)) };
enum ESizeofPluginApiFuncs : unsigned int { PLUGIN_APIFUNCS_SIZE = unsigned(sizeof(PluginAPIFuncs)) };
enum ESizeofPluginApiDesc : unsigned int { PLUGIN_APIDESC_SIZE = unsigned(sizeof(PluginAPIFuncs))  };
enum ESizeofPluginImports : unsigned int { PLUGIN_IMPORTS_SIZE = unsigned(sizeof(PluginApiImportFuncs)), PLUGIN_NO_IMPORTS = 0 };

enum EPluginApiDescMagic : unsigned int { PLUGIN_APIDESC_MAGIC = 0x1337d35c };
enum EPluginApiFuncsMagic : unsigned int { PLUGIN_APIFUNCS_MAGIC = 0x1337ca11 };
enum EPluginApiDefMagic : unsigned int { PLUGIN_DEF_MAGIC = 0x13370def };

#define PluginAPIDesc_FillHeader \
    PLUGIN_APIDESC_SIZE, \
    PLUGIN_APIDESC_MAGIC, \
    PLUGIN_API_RESERVED

#define PluginAPIFuncs_FillHeader \
    PLUGIN_APIFUNCS_SIZE, \
    PLUGIN_APIFUNCS_MAGIC, \
    PLUGIN_API_RESERVED

#define PluginAPIDef_FillHeader \
    PLUGIN_DEF_SIZE, \
    PLUGIN_DEF_MAGIC, \
    PLUGIN_API_VERSION, \
    PLUGIN_IMPORTS_SIZE, \
    PLUGIN_API_RESERVED


// Main plugin entry points
typedef const PluginAPIDef * (PLUGIN_API_CALLCONV *QueryPluginInterfaceFunc)(EPluginApiVersion);
typedef const void (PLUGIN_API_CALLCONV *SetPluginImportsFunc)(const PluginApiImportFuncs *);

#define PLUGIN_EXPORT_FUNC extern "C" EXPORT_API const PluginAPIDef * PLUGIN_API_CALLCONV QueryPluginInterface(EPluginApiVersion version)

#define PLUGIN_IMPORT_FUNC extern "C" EXPORT_API void PLUGIN_API_CALLCONV SetPluginImports(const PluginApiImportFuncs *imports)

#define MAKE_PLUGIN_EXPORT(S) PLUGIN_EXPORT_FUNC { return &(S); }

#define MAKE_PLUGIN_IMPORT(api) \
    const PluginApiImportFuncs * api = NULL; \
    PLUGIN_IMPORT_FUNC { (api) = imports; }

#define PLUGIN_LUA_PACKAGE(package) extern "C" EXPORT_API int luaopen_ ## package (lua_State *L) // { ... }

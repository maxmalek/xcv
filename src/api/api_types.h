#pragma once

// If you need API 64 bit support, #include <stdint.h> before including this file.

enum Blob3DType
{
    B3D_UNSPECIFIED = -1,
    // unsigned
    B3D_U8,
    B3D_U16,
    B3D_U32,
    // signed
    B3D_S8,
    B3D_S16,
    B3D_S32,
    // float
    B3D_FLOAT,
    B3D_HALF,
    // large
    B3D_S64,
    B3D_U64,
    B3D_DOUBLE,

    // internal, inclusive
    B3D_FIRST_VALID_TYPE = B3D_U8,
    B3D_LAST_VALID_TYPE = B3D_DOUBLE
};

// Helper to get B3D_* type from C primitive type (e.g. Blob3DType ty = ToBlob3dType<float>::value)
// Not included: B3D_UNSPECIFIED, B3D_HALF.
template<typename T> struct ToBlob3dType {};
template<> struct ToBlob3dType<unsigned char>  { static const Blob3DType value = B3D_U8; };
template<> struct ToBlob3dType<unsigned short> { static const Blob3DType value = B3D_U16; };
template<> struct ToBlob3dType<unsigned int>   { static const Blob3DType value = B3D_U32; };
template<> struct ToBlob3dType<signed char>    { static const Blob3DType value = B3D_S8; };
template<> struct ToBlob3dType<signed short>   { static const Blob3DType value = B3D_S16; };
template<> struct ToBlob3dType<signed int>     { static const Blob3DType value = B3D_S32; };
template<> struct ToBlob3dType<float>          { static const Blob3DType value = B3D_FLOAT; };
template<> struct ToBlob3dType<double>         { static const Blob3DType value = B3D_DOUBLE; };

// Assume that if anything involving 64 bit ints is required, stdint.h is included BEFORE this file.
#ifdef INT64_MAX
template<> struct ToBlob3dType<int64_t>        { static const Blob3DType value = B3D_S64; };
#endif
#ifdef UINT64_MAX
template<> struct ToBlob3dType<uint64_t>       {static const Blob3DType value = B3D_U64; };
#endif

enum LogLevel
{
    LL_DEV,
    LL_DEBUG,
    LL_INFO,
    LL_NORMAL,
    LL_ERROR
};

enum IOResult
{
    IORESULT_OK,
    IORESULT_UNSPECIFIED,
    IORESULT_FILE_NOT_FOUND, // when fopen() for reading
    IORESULT_FAILED_TO_OPEN, // when fopen() for writing
    IORESULT_INVALID_FORMAT,
    IORESULT_WRITE_FAILED, // disk full?
    IORESULT_UNSUPPORTED,

    _IORESULT_pad32 = 0x7fffffff
};

static const char *IOResultName[] =
{
    "ok",
    "unspecified error",
    "file not found",
    "failed to open",
    "invalid format",
    "write failed",
    "unsupported",
};

struct Blob3DInfo
{
    union
    {
        struct { unsigned w, h, d; };
        unsigned dim[3];
    };
    unsigned channels;
    Blob3DType type;
    union
    {
        struct { float spx, spy, spz; };    // spacing / pixel size
        float sp[3];
    };
};

struct _APITextureHandle; // opaque type; plugins don't need to know details
typedef const _APITextureHandle * APITextureHandle;

enum EPluginApiReserved { PLUGIN_API_RESERVED = 0, _EPluginReserved_pad32 = 0x7fffffff };


// === Utility functions ===

inline static int b3d_getDimensions(const Blob3DInfo *info)
{
    if(info->w && !info->h && !info->d)
        return 1;
    if(info->w && info->h && !info->d)
        return 2;
    if(info->w && info->h && info->d)
        return 3;

    return -1; // error
}

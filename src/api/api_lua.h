// Lua API used internally and by plugins
// Include this when interfacing with Lua.

#pragma once

#ifdef _DEBUG
#define LUA_USE_APICHECK
#endif

#include <lua53/lua.h>
#include <lua53/lualib.h>
#include <lua53/lauxlib.h>

// TODO: Might also want to expose the internal Lua object system at some point


#define luaFunc(func)       static int l_##func(lua_State *L)
#define luaFn(func)         static int func(lua_State *L)
#define luaRegister(func)	{#func, l_##func}
#define luaRegisterMethod(cls, name) {#name, cls::l_##name}
#define luaRegisterNewDelete(cls) luaRegisterMethod(cls, new), luaRegisterMethod(cls, delete)
#define luaRegisterMethod2(name, func) {"" name, l_##func}
#define luaConstant(x) {#x, x}

#define luaReturnNil() { return 0; }
#define luaReturnNum(x) { lua_pushnumber(L, x); return 1; }
#define luaReturnSelf() { lua_settop(L, 1); return 1; } // This assumes 'self' is at bottom of stack, i.e. first arg
#define luaReturnNum(x) { lua_pushnumber(L, x); return 1; }
#define luaReturnInt(x) {lua_pushinteger(L, x); return 1; }
#define luaReturnBool(x) { lua_pushboolean(L, x); return 1; }
#define luaReturnStr(x) { lua_pushstring(L, x); return 1; }
#define luaReturnPtr(x) { lua_pushlightuserdataornil(L, x); return 1; }
#define luaReturnString(x) { lua_pushstring(L, x.c_str()); return 1; }
#define luaReturnVec2(x, y) { lua_pushnumber(L, x); lua_pushnumber(L, y); return 2; }
#define luaReturnVec3(x, y, z) { lua_pushnumber(L, x); lua_pushnumber(L, y); lua_pushnumber(L, z); return 3; }
#define luaReturnVec4(x, y, z, w) { lua_pushnumber(L, x); lua_pushnumber(L, y); lua_pushnumber(L, z); lua_pushnumber(L, w); return 4; }
#define luaReturnIvec2(x, y) { lua_pushinteger(L, x); lua_pushinteger(L, y); return 2; }
#define luaReturnIvec3(x, y, z) { lua_pushinteger(L, x); lua_pushinteger(L, y); lua_pushinteger(L, z); return 3; }
#define luaReturnIvec4(x, y, z, w) { lua_pushinteger(L, x); lua_pushinteger(L, y); lua_pushinteger(L, z); lua_pushinteger(L, w); return 4; }

// against warnings when casting to smaller types (since Lua operates with 64 bit ints and doubles internally)
#define lua_tofloat(L, x) (static_cast<float>(lua_tonumber(L, x)))
#define lua_toint(L, x) (static_cast<int>(lua_tointeger(L, x)))
#define lua_touint(L, x) (static_cast<unsigned>(lua_tointeger(L, x)))

#define lua_checkfloat(L, x) (static_cast<float>(luaL_checknumber(L, x)))
#define lua_checkint(L, x) (static_cast<int>(luaL_checkinteger(L, x)))
#define lua_checkuint(L, x) (static_cast<unsigned>(luaL_checkinteger(L, x)))

#define lua_optfloat(L, x, d) (static_cast<float>(luaL_optnumber(L, x, d)))
#define lua_optint(L, x, d) (static_cast<int>(luaL_optinteger(L, x, d)))
#define lua_optuint(L, x, d) (static_cast<unsigned>(luaL_optinteger(L, x, d)))

#define lua_pushfloat(L, f) lua_pushnumber(L, static_cast<lua_Number>(f))

inline void lua_pushlightuserdataornil(lua_State *L, void *p)
{
    if(p)
        lua_pushlightuserdata(L, p);
    else
        lua_pushnil(L);
}

inline void lua_pushstringornil(lua_State *L, const char *s)
{
    if(s)
        lua_pushstring(L, s);
    else
        lua_pushnil(L);
}

inline bool getBool(lua_State *L, int idx)
{
    switch(lua_type(L, idx))
    {
        case LUA_TNONE:
        case LUA_TNIL: return false;
        case LUA_TBOOLEAN: return lua_toboolean(L, idx) != 0;
        case LUA_TNUMBER: return lua_tonumber(L, idx) != 0;
        case LUA_TLIGHTUSERDATA:
        case LUA_TUSERDATA: return lua_touserdata(L, idx) != NULL;
        case LUA_TSTRING: return *lua_tostring(L, idx) != 0;

        default: return false;
    }
    return false;
}

inline void luaPushPointer(lua_State *L, void *x)
{
    if(x)
        lua_pushlightuserdata(L, x);
    else
        lua_pushnil(L);
}

inline const char *getCStrSafe(lua_State *L, int idx = 1)
{
    return lua_isstring(L, idx) ? lua_tostring(L, idx) : "";
}

inline const char *getCStr(lua_State *L, int idx = 1)
{
    return lua_isstring(L, idx) ? lua_tostring(L, idx) : NULL;
}

// See lapi.c: lua_isstring() for why this is necessary...
// Hint: lua_isstring() is also true for numbers. This ensures the thing is *really* a string.
inline static int lua_isrealstring(lua_State *L, int idx)
{
    int t = lua_type(L, idx);
    return (t == LUA_TSTRING);
}

inline static const char *lua_checkrealstring(lua_State *L, int idx)
{
    luaL_checktype(L, idx, LUA_TSTRING);
    return lua_tostring(L, idx);
}

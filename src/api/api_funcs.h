// === Functions defined in api_export, provided by the core binary to plugins ===

/* If you need any of the functions below in your plugin:
Retrieve via:
  MAKE_PLUGIN_IMPORT(api);
somewhere in the file scope of your main plugin file.
Then use:
  api->msgbox("hello world!", "title");

In case you do NOT need this, don't use MAKE_PLUGIN_IMPORT().
It might impact cross-version compatibility negatively depending on how the API evolves in future.
*/

#pragma once

#include <stddef.h>
#include "api_macros.h"
#include "api_types.h"

struct lua_State;

typedef void (PLUGIN_API_CALLCONV * APITextureGetDataCallback)(const void *texdata, size_t bytes, void *userdata);

// Functions exported by core, optionally usable by plugins
struct PluginApiImportFuncs
{
    void (PLUGIN_API_CALLCONV * msgbox)(const char *msg, const char *title); // Opens a message box. Function returns when the message box is closed.
    bool (PLUGIN_API_CALLCONV * msgbox_yesno)(const char *msg, const char *title); // Opens a message box, returns true when user clicked "Yes". Function returns when closed.

    void (PLUGIN_API_CALLCONV * error)(const char *fmt, ...); // Displays an error loudly (message box, red text, etc).
    void (PLUGIN_API_CALLCONV * log)(LogLevel lvl, const char *fmt, ...); // Log to console

    // Fills info struct
    void (PLUGIN_API_CALLCONV * texture_fill_info)(APITextureHandle tex, Blob3DInfo *info);

    // Return size of bytes required to store one mipmap level of a texture in the format described by info.
    // Miplevel is in OpenGL convention, starting at 0 for the largest mip level.
    // Miplevel 0 is always available; levels 1 and up are only available when the texture has mipmaps.
    size_t (PLUGIN_API_CALLCONV * texture_info_get_size_bytes)(const Blob3DInfo *info, unsigned miplevel);

    // Same as texture_info_get_size_bytes(), but accepts a texture instead of an info struct.
    size_t (PLUGIN_API_CALLCONV * texture_get_size_bytes)(APITextureHandle tex, unsigned miplevel);

    // Copy raw texture data into memory at dst. Performs conversion if necessary. Pass outtype = B3D_UNSPECIFIED and outchannels = 0 to never perform conversion.
    // Returns how many bytes were copied to dst, 0 if the conversion failed.
    size_t (PLUGIN_API_CALLCONV * texture_copy_data)(void *dst, size_t bufsize, APITextureHandle tex, Blob3DType outtype, unsigned outchannels, unsigned miplevel);

    // More efficient than api_texture_copy_data(). The pointer passed to the callback may live in GPU space and is only valid until the function returns.
    // The pointer may be NULL if the conversion failed; make sure to check it in the callback!
    void (PLUGIN_API_CALLCONV * texture_get_data_callback)(APITextureHandle ptex, APITextureGetDataCallback cb, Blob3DType outtype, unsigned outchannels, void * userdata, unsigned miplevel);

    // Returns global Lua state
    lua_State *(PLUGIN_API_CALLCONV * get_lua)();
};

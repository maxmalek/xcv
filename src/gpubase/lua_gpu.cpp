#include "luainternal.h"
#include "lua_glsl.h"
#include "log.h"

#include "gpubuffer.h"
#include "texture.h"
#include "shader.h"
#include "framebuffer.h"
#include "glsl.h"
#include "mesh.h"
#include "blendmode.h"
#include <ctype.h>
#include "renderer.h"
#include "lua_numvec.h"


static void checkchannels(lua_State *L, unsigned channels)
{
    if(!channels || channels > 4)
         luaL_error(L, "unsupported number of channels: %d (expected 1..4)", channels);
}

static void checkspace(lua_State *L, const GPUBufferRaw *buf, size_t sz, size_t offset)
{
    if(!buf->hasSpace(sz, offset))
        luaL_error(L, "buffer too small: must be at least %I+%I bytes, is %I bytes", sz, offset, buf->bytes());
}

static bool testgpuformat(lua_State *L, int idx, Blob3DType& ty, unsigned& channels, bool& integral)
{
    const char *s = lua_checkrealstring(L, idx);
    return gpuutil::parseFormat(s, &channels, &ty, &integral);
}

static void checkgpuformat(lua_State *L, int idx, Blob3DType& ty, unsigned& channels, bool& integral)
{
    if(!testgpuformat(L, idx, ty, channels, integral))
        luaL_error(L, "invalid/unhandled format: [%s]", lua_tostring(L, idx));
}

static const char * const SWIZZLE_CHARS = "01rgba";

static const char *validateSwizzle(lua_State *L, int idx)
{
    if(lua_isnoneornil(L, idx))
        return "";
    const char * const s = lua_checkrealstring(L, idx);
    if(!*s)
        return "";
    if(strlen(s) != 4)
        return NULL;
    for(unsigned i = 0; i < 4; ++i)
        if(!strchr(SWIZZLE_CHARS, s[i]))
            return NULL;
    return s;
}

static const char *checkSwizzle(lua_State *L, int idx)
{
    const char *s = validateSwizzle(L, idx);
    if(!s)
        luaL_error(L, "Invalid swizzle mask: %s", lua_tostring(L, idx));

    return s;
}


struct LuaGPUBuffer : public LuaClassBind<GPUBufferRaw>
{
    static const luaL_Reg methods[];

    luaFunc(new)
    {
        const void *buf;
        size_t sz;
        _getbytes(L, &buf, &sz, 1);
         GPUBufferRaw::Access a = _getAccess(lua_tostring(L, 2));
        luaReturnObj(GPUBufferRaw::New(buf, sz, a));
    }

    luaFunc(FromTexture)
    {
        TextureAny *tex = luaGetObj<TextureAny>(L, 1);
        GPUBufferRaw::Access a = _getAccess(lua_tostring(L, 2));
        luaReturnObj(GPUBufferRaw::FromTexture(*tex, a));
    }

    luaFunc(FromFile)
    {
        const char *fn = luaL_checkstring(L, 1);
        GPUBufferRaw::Access a = _getAccess(lua_tostring(L, 2));
        luaReturnObj(GPUBufferRaw::FromFile(fn, a));
    }


    luaFunc(glsltype) { luaReturnInt(GLSL::buffer); } // For compatibility with Texture


    // FIXME: This should be moved to its own function, like getRawMemory()
    static void _getbytes(lua_State *L, const void **pbuf, size_t *psz, int idx = 2)
    {
        const int ty = lua_type(L, idx);
        if(ty == LUA_TSTRING)
        {
            *pbuf = lua_tostring(L, idx);
            *psz = lua_rawlen(L, idx);
        }
        else if(ty == LUA_TNUMBER)
        {
            *pbuf = NULL;
            *psz = lua_tointeger(L, idx);
        }
        else if(ty == LUA_TUSERDATA)
        {
            if(luaIsObject(L, idx))
            {
                if(const TypedScriptableNumVec *v = luaGetObj<TypedScriptableNumVec>(L, idx))
                {
                    *psz = v->bytesize();
                    *pbuf = v->ptr();
                }
            }
            else
            {
                *psz = lua_rawlen(L, idx);
                *pbuf = lua_touserdata(L, idx);
            }
        }
        else
            luaL_error(L, "unable to deduce sized memory buffer from type %s", lua_typename(L, ty));
    }

    // convert string into GPUBufferRaw::Access. "" -> NONE.
    static GPUBufferRaw::Access _getAccess(const char *s)
    {
        unsigned f = GPUBufferRaw::NONE;
        if(s)
            while(char c = *s++)
                switch(c)
                {
                    case 'r': f |= GPUBufferRaw::MAP_READ; break;
                    case 'w': f |= GPUBufferRaw::MAP_WRITE; break;
                    case 'u': f |= GPUBufferRaw::UPDATE; break;
                    case 'p': f |= GPUBufferRaw::PERSISTENT; break;
                    case 'B': f |= GPUBufferRaw::BACKUP; break;
                }
        return (GPUBufferRaw::Access)f;
    }

    luaFunc(invalidate) { This(L)->invalidate(); luaReturnSelf(); }
    luaFunc(zerofill) { This(L)->zerofill(); luaReturnSelf(); }
    luaFunc(barrier)  { This(L)->barrier(); luaReturnSelf(); }
    luaFunc(rawsize)     { luaReturnInt(This(L)->bytes()) ; }
    // (buf, bytes, offset)
    luaFunc(uploadBytes)
    {
        const void *buf;
        size_t sz;
        _getbytes(L, &buf, &sz);
        GPUBufferRaw *b = This(L);
        size_t offs = lua_tointeger(L, 3);
        checkspace(L, b, sz, offs);
        b->uploadBytes(buf, sz, offs);
        luaReturnSelf();
    }

    // (tex, offset)
    luaFunc(importTexture)
    {
        GPUBufferRaw *buf = This(L);
        TextureAny *tex = luaGetObj<TextureAny>(L, 2);
        const size_t texsz = tex->getSizeBytes();
        const size_t offs = lua_tointeger(L, 3);
        checkspace(L, buf, texsz, offs);
        buf->importTexture(*tex, offs);
        luaReturnSelf();
    }

    luaFunc(readptr) { luaReturnPtr(const_cast<void*>(This(L)->gpuread)); }
    luaFunc(writeptr) { luaReturnPtr(This(L)->gpuwrite); }

    luaFunc(save)
    {
        luaReturnBool(This(L)->saveFile(luaL_checkstring(L, 2)));
    }

    luaFunc(insertFence)
    {
        This(L)->insertFenceSync();
        luaReturnNil();
    }
    luaFunc(waitFences)
    {
        This(L)->waitFences();
        luaReturnNil();
    }
};

const luaL_Reg LuaGPUBuffer::methods[] =
{
    luaRegisterNewDelete(LuaGPUBuffer),
    luaRegisterMethod(LuaGPUBuffer, FromTexture),
    luaRegisterMethod(LuaGPUBuffer, FromFile),
    luaRegisterMethod(LuaGPUBuffer, zerofill),
    luaRegisterMethod(LuaGPUBuffer, barrier),
    luaRegisterMethod(LuaGPUBuffer, invalidate),
    luaRegisterMethod(LuaGPUBuffer, uploadBytes),
    luaRegisterMethod(LuaGPUBuffer, readptr),
    luaRegisterMethod(LuaGPUBuffer, writeptr),
    luaRegisterMethod(LuaGPUBuffer, rawsize),
    luaRegisterMethod(LuaGPUBuffer, importTexture),
    luaRegisterMethod(LuaGPUBuffer, save),
    luaRegisterMethod(LuaGPUBuffer, insertFence),
    luaRegisterMethod(LuaGPUBuffer, waitFences),
    { "__len", LuaGPUBuffer::l_rawsize }
};

struct LuaTexture : public LuaClassBind<TextureAny>
{
    static const luaL_Reg methods[];

    // ("filename" [, type, flags]) or (x,y,z, typestring [, flags])
    luaFunc(new)
    {
        TextureAny *tex = NULL;
        if(lua_isrealstring(L, 1))
        {
            Blob3DType ty = (Blob3DType)luaL_optinteger(L, 2, B3D_UNSPECIFIED); // FIXME: this is meh
            TextureFlags flags = getflags(L, 3);
            tex = TextureAny::LoadFile(lua_tostring(L, 1), ty, flags, NULL);
            luaReturnObjOrErrMsg(tex, "Failed to load file as texture. Unknown format?");
        }

        // X size must be given, other 2 are optional
        glm::uvec3 sz(luaL_checkinteger(L, 1), lua_tointeger(L, 2), lua_tointeger(L, 3));
        TextureFlags flags = getflags(L, 5);
        Blob3DType ty;
        unsigned channels;
        bool integral;
        checkgpuformat(L, 4, ty, channels, integral);
        if(integral)
            flags = TextureFlags(flags | TEX_INTEGRAL);
        const char *swizzle = checkSwizzle(L, 6);
        tex = TextureAny::New(sz, ty, channels, flags, swizzle);
        luaReturnObjOrErrMsg(tex, "Failed to create texture");
    }

    // tex:upload(buf, type [, offset])
    luaFunc(upload) 
    {
        TextureAny *tex = This(L);
        GPUBufferRaw *buf = luaGetObj<GPUBufferRaw>(L, 2);

        Blob3DType ty = (Blob3DType)luaL_checkinteger(L, 3);
        const unsigned channels = lua_touint(L, 4);
        const size_t offset = lua_tointeger(L, 5);
        const size_t tb = tex->getSizeBytes();
        checkspace(L, buf, tb, offset);
        tex->upload(*buf, offset, ty, channels);
        luaReturnSelf();
    }

    luaFunc(save)
    {
        TextureAny *tex = This(L);
        const char *fn = lua_checkrealstring(L, 2);
        const char *fmt = lua_tostring(L, 3); // maybe NULL
        unsigned miplevel = (unsigned)luaL_optinteger(L, 4, 0);
        IOResult res = tex->saveFile(fn, fmt, miplevel);
        if(res == IORESULT_OK)
            luaReturnBool(true);

        lua_pushboolean(L, false);
        lua_pushstring(L, IOResultName[res]);
        return 2;
    }

    luaFunc(genMipmaps)
    {
        This(L)->generateMipmaps();
        luaReturnSelf();
    }

    // SIze in pixels
    // Returns 1, 2, or 3 numbers, depending on the number of dimensions
    luaFunc(size)
    {
        TextureAny *tex = This(L);
        glm::uvec3 sz = tex->size();
        switch(tex->textype())
        {
            case TEX_1D: luaReturnInt(sz.x);
            case TEX_2D: luaReturnIvec2(sz.x, sz.y);
            case TEX_3D: luaReturnIvec3(sz.x, sz.y, sz.z);
            default: assert(false);
        }
        // not reached
        return 0;
    }

    // Like size(), but returns always 3 numbers, with non-existing dimensions set to 0
    luaFunc(size3)
    {
        luaReturnT(This(L)->size3());
    }
    // Like size(), but returns always 3 numbers, with non-existing dimensions set to 1.
    // This is the internal size of the texture useful for multiplication, e.g.
    // local w,h,d = tex:internalsizee(); local npix = w*h*d;
    luaFunc(internalsize)
    {
        luaReturnT(This(L)->size());
    }

    luaFunc(rawsize)     { luaReturnInt(This(L)->getSizeBytes()) ; }

    luaFunc(getPixelSize) { luaReturnT(This(L)->pixelSize); }
    luaFunc(setPixelSize) { This(L)->pixelSize = lua_optvec3(L, 2, glm::vec3(1)); luaReturnSelf(); }
    luaFunc(displaysize) { luaReturnT(This(L)->displaysize()); }
    luaFunc(displaysizeRatio) { luaReturnT(This(L)->displaysizeRatio()); }
    luaFunc(displaysizeScaled) { luaReturnT(This(L)->displaysizeScaled()); }
    luaFunc(dimensions) { luaReturnInt(This(L)->dimensions()); }
    luaFunc(glsltype) { luaReturnInt(This(L)->glsltype); }
    luaFunc(channels) { luaReturnInt(This(L)->channels()); }
    luaFunc(datatype) { luaReturnInt(This(L)->datatype()); }
    luaFunc(swizzle) { luaReturnStr(This(L)->getSwizzleMask()); }
    luaFunc(invalidate) { This(L)->invalidate(); luaReturnSelf(); }
    luaFunc(zerofill) { This(L)->zerofill(); luaReturnSelf(); }
    luaFunc(barrier)  { This(L)->barrier(); luaReturnSelf(); }

    luaFunc(formatstr)
    {
        TextureAny *tex = This(L);
        luaReturnStr(gpuutil::getFormatString(tex->channels(), tex->datatype(), !!(tex->flags() & TEX_INTEGRAL) ).c_str());
    }

    // convert string into TextureAny::Access. "" -> READONLY.
    static TextureAny::Access _getAccess(const char *s)
    {
        static const TextureAny::Access lut[] = { TextureAny::READONLY, TextureAny::READONLY, TextureAny::WRITEONLY, TextureAny::READWRITE };
        int f = 0;
        if(s)
            while(char c = *s++)
                switch(c)
                {
                    case 'r': f |= 1; break;
                    case 'w': f |= 2; break;
                }
        return lut[f];
    }

    // convert string into TextureFlags. "" -> TEX_NONE.
    static TextureFlags _getFlags(const char *s)
    {
        unsigned f = TEX_NONE;
        if(s)
            while(char c = *s++)
                switch(c)
                {
                    case 'l': f |= TEX_LINEAR; break;
                    case 'm': f |= TEX_MIPMAP; break;
                    case 'i': f |= TEX_INTEGRAL; break;
                    case 'B': f |= TEX_KEEP_BACKUP; break;
                }
        return (TextureFlags)f;
    }

    luaFunc(texflags)
    {
        char buf[4];
        unsigned i = 0;
        TextureAny *tex = This(L);
        const TextureFlags f = tex->flags();
        if (f & TEX_MIPMAP)
            buf[i++] = 'm';
        if (f & TEX_LINEAR)
            buf[i++] = 'l';
        if (f & TEX_INTEGRAL)
            buf[i++] = 'i';
        if (f & TEX_KEEP_BACKUP)
            buf[i++] = 'B';
        buf[i++] = 0;
        luaReturnStr(buf);
    
    }

    static TextureFlags getflags(lua_State *L, int idx)
    {
        if(lua_isrealstring(L, idx))
        {
            const char *s = lua_tostring(L, idx);
            return _getFlags(s);
        }
        else if(!lua_isnoneornil(L, idx))
            luaL_error(L, "texture flags must be string, or nil for defaults");
        
        return TEX_LINEAR;
    }

    luaFunc(clearResidency)
    {
        This(L)->clearAllResidency();
        luaReturnSelf()
    }

    luaFunc(ParseFormat) // static
    {
        unsigned channels;
        Blob3DType ty;
        bool integral;
        const char *s = lua_checkrealstring(L, 1);
        bool valid = gpuutil::parseFormat(s, &channels, &ty, &integral);
        if(valid)
        {
            lua_pushinteger(L, channels);
            lua_pushinteger(L, ty);
            lua_pushboolean(L, integral);
            return 3;
        }
        luaReturnNil();
    }

    luaFunc(CheckSwizzle)
    {
        const char *s = validateSwizzle(L, 1);
        luaReturnBool(!!s);
    }
};

const luaL_Reg LuaTexture::methods[] =
{
    luaRegisterNewDelete(LuaTexture),
    luaRegisterMethod(LuaTexture, size),
    luaRegisterMethod(LuaTexture, size3),
    luaRegisterMethod(LuaTexture, save),
    luaRegisterMethod(LuaTexture, internalsize),
    luaRegisterMethod(LuaTexture, upload),
    luaRegisterMethod(LuaTexture, genMipmaps),
    luaRegisterMethod(LuaTexture, dimensions),
    luaRegisterMethod(LuaTexture, glsltype),
    luaRegisterMethod(LuaTexture, channels),
    luaRegisterMethod(LuaTexture, datatype),
    luaRegisterMethod(LuaTexture, formatstr),
    luaRegisterMethod(LuaTexture, texflags),
    luaRegisterMethod(LuaTexture, rawsize),
    luaRegisterMethod(LuaTexture, getPixelSize),
    luaRegisterMethod(LuaTexture, setPixelSize),
    luaRegisterMethod(LuaTexture, displaysize),
    luaRegisterMethod(LuaTexture, displaysizeRatio),
    luaRegisterMethod(LuaTexture, displaysizeScaled),
    luaRegisterMethod(LuaTexture, ParseFormat),
    luaRegisterMethod(LuaTexture, CheckSwizzle),
    luaRegisterMethod(LuaTexture, clearResidency),
    luaRegisterMethod(LuaTexture, invalidate),
    luaRegisterMethod(LuaTexture, zerofill),
    luaRegisterMethod(LuaTexture, barrier),
    luaRegisterMethod(LuaTexture, swizzle)
};

struct ShaderLoadCallbackData
{
    unsigned c;
    lua_State * const L;
    const unsigned nextIdx;
};

static void _shaderLoadErrorCallback(void *ud, const char *fmt, ...)
{
    ShaderLoadCallbackData *d = (ShaderLoadCallbackData*)ud;
    ++d->c;
    va_list ap;
    va_start(ap, fmt);
    lua_pushvfstring(d->L, fmt, ap);
    va_end(ap);
    lua_pushliteral(d->L, "\n");
    lua_concat(d->L, 2);
}

static std::string _shaderPatchCallback(const char *code, ShaderType::Enum type, void *ud)
{
    const ShaderLoadCallbackData *d = (ShaderLoadCallbackData*)ud;
    lua_State * const L = d->L;
    const int prevtop = lua_gettop(L);
    //                                             v-- this is d->nextIdx
    // [shader] [method] [parameters to method...]  ...

    lua_getglobal(L, "onCompileShader");
    // [whatever] ... [onCompileShader]
    if(lua_isnoneornil(L, -1))
        return code;

    // [whatever] ... [onCompileShader]
    lua_pushstring(L, code);
    // [whatever] ... [onCompileShader] [code]
    lua_pushstring(L, ShaderType::Names[type]);
    // [whatever] ... [onCompileShader] [code] [typename]

    for(int i = d->nextIdx; i <= prevtop; ++i)
        lua_pushvalue(L, i);

    const int nargs = prevtop - d->nextIdx + 1 + 2;  // +2 because 2 extra args were pushed before the rest

    // call as onCompileShader(code, typename, ...)
    lua_call(L, nargs, 1);
    if(lua_isnoneornil(L, -1))
        return code;

    std::string patched = luaL_checkstring(L, -1);
    lua_settop(L, prevtop);
    return patched;
}

template<typename T>
static int _setupShader(lua_State *L, T& sh, unsigned nextIdx)
{
    ShaderLoadCallbackData cbd { 0, L, nextIdx };
    ShaderLoadCallbacks cb { &cbd, _shaderLoadErrorCallback, _shaderPatchCallback };
    int top = lua_gettop(L);
    bool ok = sh.load(&cb);
    if(cbd.c) // any errors or warnings?
    {
        // [errors]
        luaL_where(L, 1);
        // [errors...][where]
        lua_insert(L, -((int)cbd.c + 1));
        // [where][errors...]
        lua_concat(L, cbd.c + 1);
        // [errstr]
        lua_pushboolean(L, ok);
        // [errstr][ok]
        lua_insert(L, -2);
        // [ok][errstr]
        return 2;
    }

    luaReturnBool(ok);
}

// Default case: Pass-through uniform
template<typename T>
struct _Setu
{
    static void set(lua_State *L, const ProgramUniform *u, int idx)
    {
        T val = lua_getT<T>(L, idx);
        u->setUnsafe(&val);
    }
};

// Special case for handles
template<>
struct _Setu<uint64_t>
{
    static void set(lua_State *L, const ProgramUniform *u, int idx)
    {
        uint64_t val = 0;
        if(lua_type(L, idx) == LUA_TUSERDATA)
        {
            TextureAny *tex = luaGetObjOrNil<TextureAny>(L, idx);
            // FIXME: Can't discern here whether to use an image handle or a sampler handle...
            // Could make the distinction based on the variable name, but that's just nasty.
            // So it's images only, for now.
            uint64_t handle = u->setImageHandle(tex, TextureAny::READWRITE);
            if(tex && !handle)
                luaL_argerror(L, idx, "Failed to get Image GPU handle");
        }
        else
        {
            uint64_t val = lua_tointeger(L, idx);
            u->setUnsafe(&val); // also clears bound handle, if any
        }
        
    }
};

template<typename T>
static void _setu(lua_State *L, const ProgramUniform *u, int idx)
{
    _Setu<T>::set(L, u, idx);
}

static bool _setuniform(lua_State *L, const ProgramUniform *u, int idx)
{
    // UNSAFE set -- if all we got is a raw pointer, pass it along and hope for the best
    if(lua_type(L, idx) == LUA_TLIGHTUSERDATA)
    {
        u->setUnsafe(lua_touserdata(L, idx));
        return true;
    }

#define SETU(as) _setu<as>(L, u, idx)
#define CASE(what, as) case GLSL::what: SETU(as); return true
    switch(u->getType())
    {
#define PRIMTYPE(c, glsl) CASE(glsl, c);
#define GLSLTYPE(name)    CASE(name, glm::name);
#define GLSLSAMPLER(name) CASE(name, unsigned);
#define GLSLIMAGE(name)   CASE(name, unsigned);
#define GLSLFAKETYPE(name)   break; // not settable as uniform
#include "glsl_gen.h"
    default:
        assert(false);
    }
#undef CASE
#undef SETU
    return false;
}

static bool _setuniform(lua_State *L, ShaderProgramWrapper& sh, const char *name, int idx)
{
    const ProgramUniform *u = sh.getProgram()->lookupUniform(name);
    return u ? _setuniform(L, u, idx) : false;
}

static void _setbuffer(lua_State *L, const ProgramBuffer *b, int idx)
{
    GPUBufferRaw *buf = luaGetObjOrNil<GPUBufferRaw>(L, idx);
    b->set(buf);
}

static bool _setbuffer(lua_State *L, ShaderProgramWrapper& sh, const char *name, int idx)
{
    if(const ProgramBuffer *b = sh.getProgram()->lookupBuffer(name))
    {
        _setbuffer(L, b, idx);
        return true;
    }
    return false;
}

static bool _setsampler(lua_State *L, ShaderProgramWrapper& sh, const char *name, int idx)
{
    return sh.setSampler(name, luaGetObjOrNil<TextureAny>(L, idx));
}

static bool _setimage(lua_State *L, ShaderProgramWrapper& sh, const char *name, int idx, int idxaccess)
{
    TextureAny::Access a = LuaTexture::_getAccess(lua_tostring(L, 4));
    return sh.setImage(name, luaGetObjOrNil<TextureAny>(L, idx), a);
}

static bool _setany(lua_State *L, ShaderProgramWrapper& sh, const char *name, int idx, int idxaccess)
{
    const ProgramVariable *v = sh.getProgram()->lookupAny(name);
    if(!v)
        return false;
    switch(v->getProgramVariableType())
    {
        case ProgramVariable::UNIFORM:
            return _setuniform(L, static_cast<const ProgramUniform*>(v), idx);
        case ProgramVariable::BUFFER:
            _setbuffer(L, static_cast<const ProgramBuffer*>(v), idx);
            return true;
        case ProgramVariable::ATTRIB:
            luaL_error(L, "Setting attributes is not yet implemented"); // FIXME / TODO
            return false;
        case ProgramVariable::IMAGE:
            static_cast<const ProgramImage*>(v)->set(
                luaGetObjOrNil<TextureAny>(L, idx),
                LuaTexture::_getAccess(lua_tostring(L, idxaccess))
            );
            return true;
        case ProgramVariable::SAMPLER:
            static_cast<const ProgramSampler*>(v)->set(
                luaGetObjOrNil<TextureAny>(L, idx)
            );
            return true;
    }
    luaL_error(L, "[internal error] Unhandled ProgramVariable::Type (%d)", v->getProgramVariableType());
    return false;
}

template<typename T>
static int _getu(lua_State *L, const ProgramUniform *u, bool rawptr)
{
    T *p = u->getpAs<T>();
    if(!rawptr)
        luaReturnT(*p);

    lua_pushlightuserdata(L, p);
    return 1;
}

static int _getuobj(lua_State *L, const ProgramUniform *u)
{
    if(Refcounted<REFCOUNT_NORMAL> *refobj = u->getObj())
    {
        if(Scriptable *so = dynamic_cast<Scriptable*>(refobj))
            luaReturnObj(so);
        else
        {
            logerror("_getuniform[%s %s]: Has Refcounted that is not Scriptable", GLSL::getName(u->getType()), u->getName());
            assert(false);
        }
    }
    return 0;
}

// pushes raw pointer as light userdata or a number of values on the stack
static int _getuniform(lua_State *L, const ProgramUniform *u, bool rawptr)
{
    if(int ret = _getuobj(L, u)) // Do we have an actual object bound? (For uint64 handles, mainly)
        return ret;

#define GETU(as) return _getu<as>(L, u, rawptr)
#define CASE(what, as) case GLSL::what: GETU(as);
    switch(u->getType())
    {
#define PRIMTYPE(c, glsl) CASE(glsl, c);
#define GLSLTYPE(name)    CASE(name, glm::name);
#define GLSLSAMPLER(name)    break; // not gettable as uniform
#define GLSLIMAGE(name)      break;
#define GLSLFAKETYPE(name)   break;
#include "glsl_gen.h"
    default:
        assert(false);
    }
#undef CASE
#undef SETU
    assert(false);
    luaReturnNil();
}

template<typename T>
static int _getprogramobjT(lua_State *L, const T *v)
{
    typedef typename T::ObjectType Obj;
    luaReturnObj(const_cast<Obj*>(v->get()));
}

static int _getany(lua_State *L, ShaderProgramWrapper& sh, const char *name, bool rawptr)
{
    const ProgramVariable *v = sh.getProgram()->lookupAny(name);
    if(!v)
        return false;
    switch(v->getProgramVariableType())
    {
        case ProgramVariable::UNIFORM:
            return _getuniform(L, static_cast<const ProgramUniform*>(v), rawptr);
        case ProgramVariable::BUFFER:
            return _getprogramobjT(L, static_cast<const ProgramBuffer*>(v));
        case ProgramVariable::ATTRIB:
            luaL_error(L, "Getting attributes is not yet implemented"); // FIXME / TODO
            return false;
        case ProgramVariable::IMAGE:
             return _getprogramobjT(L, static_cast<const ProgramImage*>(v));
        case ProgramVariable::SAMPLER:
             return _getprogramobjT(L, static_cast<const ProgramSampler*>(v));
    }
    luaL_error(L, "[internal error] Unhandled ProgramVariable::Type (%d)", v->getProgramVariableType());
    return false;
}

static int _getuniform(lua_State *L, ShaderProgramWrapper& sh, const char *name, bool rawptr)
{
    const ProgramUniform *u = sh.getProgram()->lookupUniform(name);
    return u ? _getuniform(L, u, rawptr) : 0;
}

static int _getbuffer(lua_State *L, ShaderProgramWrapper& sh, const char *name)
{
    const ProgramBuffer *u = sh.getProgram()->lookupBuffer(name);
    luaReturnObj(u ? u->get() : NULL);
}

static int _getsampler(lua_State *L, ShaderProgramWrapper& sh, const char *name)
{
    const ProgramSampler *u = sh.getProgram()->lookupSampler(name);
    luaReturnObj(u ? const_cast<TextureAny*>(u->get()) : NULL);
}
static int _getimage(lua_State *L, ShaderProgramWrapper& sh, const char *name)
{
    const ProgramImage *u = sh.getProgram()->lookupImage(name);
    luaReturnObj(u ? u->get() : NULL);
}


/*
// FIXME: allow assigning buffers to attribs (but this should be done on a per-object basis since it needs a VAO)
static int _setattrib(lua_State *L, ShaderProgramWrapper& sh, const char *name, int idx)
{
    bool res = false;
    if(const ProgramInput *inp = sh.getProgram()->lookupAttrib(name))
    {
        const GPUBufferRaw *buf = luaGetObjOpt<GPUBufferRaw>(L, idx); // possibly NULL but no problem
        inp->
        res = true;
    }
    luaReturnBool(res);
}
*/

typedef int (*ShaderPushFunc)(lua_State *L, const ProgramVariable& v);

static int _pushvartab(lua_State *L, const ProgramVariable& v)
{
    LuaStackCheck(L, 1);
    lua_createtable(L, 3, 0);
    lua_pushstring(L, v.getName());
    lua_rawseti(L, -2, 1);
    lua_pushinteger(L, v.getType());
    lua_rawseti(L, -2, 2);
    lua_pushstring(L, ProgramVariable::var_type_name[v.getProgramVariableType()]);
    lua_rawseti(L, -2, 3);
    return 1;
}

template<ShaderPushFunc func, typename V>
static void _pushalli(lua_State *L, const V& v, unsigned& c)
{
    LuaStackCheck(L, 0);
    for(size_t i = 0; i < v.size(); ++i, ++c)
        if(func(L, v[i]))
            lua_rawseti(L, -2, c);
}

// array of variables
template<ShaderPushFunc func>
static int _getvarsi(lua_State *L, const ShaderProgram& p)
{
    LuaStackCheck(L, 1);
    const ShaderProgram::Uniforms& u = p.getUniforms();
    const ShaderProgram::Buffers& b = p.getBuffers();
    const ShaderProgram::Inputs& a = p.getInputs();
    const ShaderProgram::Images& im = p.getImages();
    const ShaderProgram::Samplers& s = p.getSamplers();
    lua_createtable(L, (int)(u.size() + b.size() + a.size() + im.size() + s.size()), 0);
    unsigned c = 1;
    _pushalli<func>(L, u, c);
    _pushalli<func>(L, b, c);
    _pushalli<func>(L, a, c);
    _pushalli<func>(L, im, c);
    _pushalli<func>(L, s, c);
    return 1;
}

struct LuaShaderProgramWrapper : public LuaClassBind<ShaderProgramWrapper>
{
    static const luaL_Reg methods[];

    luaFunc(setuniform)   { luaReturnBool(_setuniform(L, *This(L), lua_checkrealstring(L, 2), 3));    }
    luaFunc(setbuffer)    { luaReturnBool(_setbuffer (L, *This(L), lua_checkrealstring(L, 2), 3));    }
    luaFunc(setsampler)   { luaReturnBool(_setsampler(L, *This(L), lua_checkrealstring(L, 2), 3));    }
    luaFunc(setimage)     { luaReturnBool(_setimage  (L, *This(L), lua_checkrealstring(L, 2), 3, 4)); }
    luaFunc(setany)       { luaReturnBool(_setany    (L, *This(L), lua_checkrealstring(L, 2), 3, 4)); }

    luaFunc(getuniform)   { return _getuniform(L, *This(L), lua_checkrealstring(L, 2), getBool(L, 3)); }
    luaFunc(getbuffer)    { return _getbuffer (L, *This(L), lua_checkrealstring(L, 2));                }
    luaFunc(getsampler)   { return _getsampler(L, *This(L), lua_checkrealstring(L, 2));                }
    luaFunc(getimage)     { return _getimage  (L, *This(L), lua_checkrealstring(L, 2));                }
    luaFunc(getany)       { return _getany    (L, *This(L), lua_checkrealstring(L, 2), getBool(L, 3)); }

    // Clears shader variables that refer to objects, allowing them to be GC'd faster
    luaFunc(clearRefs)    { This(L)->clearRefs(); luaReturnSelf(); }

    luaFunc(glsltype)
    {
        const char *name = lua_checkrealstring(L, 2);
        const ProgramUniform *v = This(L)->getProgram()->lookupUniform(name);
        if(v)
            luaReturnInt(v->getType());
        luaReturnNil();
    }

    luaFunc(ptr) // UNSAFE function! Always gets pointer to memory, even if it's unsafe to use it.
    {            // Use :getuniform(name, true) or :getany(name, true) to get a pointer if it's safe, and an object otherwise.
        const ProgramUniform *v = This(L)->getProgram()->lookupUniform(lua_checkrealstring(L, 2));
        luaPushPointer(L, v ? v->getp() : NULL);
        return 1;
    }

    luaFunc(rawsize)
    {
        if(const ProgramUniform *v = This(L)->getProgram()->lookupUniform(lua_checkrealstring(L, 2)))
        {
             luaReturnInt(GLSL::sizeOf(v->getType()));
        }
        luaReturnNil();
    }

    luaFunc(refresh)
    {
         if(const ProgramUniform *v = This(L)->getProgram()->lookupUniform(lua_checkrealstring(L, 2)))
         {
             v->refresh();
             luaReturnBool(true);
         }
         luaReturnBool(false);
    }
    
    // array of tables, each describing a variable
    luaFunc(getvars) { return _getvarsi<_pushvartab>(L, *(This(L)->getProgram())); }

    luaFunc(copyvars)
    {
        This(L)->copyVariables(*luaGetObj<ShaderProgramWrapper>(L, 2));
        luaReturnSelf();
    }
};

const luaL_Reg LuaShaderProgramWrapper::methods[] =
{
    // no ctor
    //luaRegisterMethod(LuaShaderProgramWrapper, delete),
    luaRegisterMethod(LuaShaderProgramWrapper, setuniform),
    luaRegisterMethod(LuaShaderProgramWrapper, setbuffer),
    luaRegisterMethod(LuaShaderProgramWrapper, setsampler),
    luaRegisterMethod(LuaShaderProgramWrapper, setimage),
    luaRegisterMethod(LuaShaderProgramWrapper, setany),
    luaRegisterMethod(LuaShaderProgramWrapper, getuniform),
    luaRegisterMethod(LuaShaderProgramWrapper, getbuffer),
    luaRegisterMethod(LuaShaderProgramWrapper, getsampler),
    luaRegisterMethod(LuaShaderProgramWrapper, getimage),
    luaRegisterMethod(LuaShaderProgramWrapper, getany),
    luaRegisterMethod(LuaShaderProgramWrapper, getvars),
    luaRegisterMethod(LuaShaderProgramWrapper, glsltype),
    luaRegisterMethod(LuaShaderProgramWrapper, ptr),
    luaRegisterMethod(LuaShaderProgramWrapper, rawsize),
    luaRegisterMethod(LuaShaderProgramWrapper, refresh),
    luaRegisterMethod(LuaShaderProgramWrapper, clearRefs),
    luaRegisterMethod(LuaShaderProgramWrapper, copyvars),
};


struct LuaComputeShader : public LuaClassBind<ComputeShader>
{
    static const luaL_Reg methods[];

    luaFunc(new)
    {
        luaReturnObj(new ComputeShader);
    }

    // sh:loadsrc(codeStr, defs, [, entrypoint]) -> bool
    luaFunc(loadstring)
    {
        ComputeShader *sh = This(L);
        const char *src = luaL_checkstring(L, 2);
        sh->setSource(src);
        return _setupShader(L, *sh, 3);
    }

    // sh:loadfile(codeStr, defs, [, entrypoint]) -> bool
    luaFunc(loadfile)
    {
        ComputeShader *sh = This(L);
        const char *fn = luaL_checkstring(L, 2);
        sh->setFile(fn);
        return _setupShader(L, *sh, 3);
    }

    luaFunc(dispatch)
    {
        This(L)->dispatchComputeSingle(
            (unsigned)luaL_checkinteger(L, 2),
            (unsigned)lua_tointeger(L, 3),
            (unsigned)lua_tointeger(L, 4)
        );
        luaReturnSelf();
    }

    luaFunc(multiDispatch)
    {
        glm::uvec3 g = lua_touvec3(L, 2);
        glm::uvec3 m = lua_touvec3(L, 5);
        if(!This(L)->dispatchComputeMulti(g, m))
            luaL_error(L, "multiDispatch failed -- is uniform '%s' used?", ComputeShader::MultiDispatchUniformName);
        luaReturnSelf();
    }

    luaFunc(dispatchForSize)
    {
        This(L)->dispatchComputeForSizeSingle(
            lua_checkuint(L, 2),
            lua_touint(L, 3),
            lua_touint(L, 4)
        );
        luaReturnSelf();
    }

    luaFunc(multiDispatchForSize)
    {
        glm::uvec3 size = lua_touvec3(L, 2);
        glm::uvec3 maxsize = lua_touvec3(L, 5);
        if(!This(L)->dispatchComputeForSizeMulti(size, maxsize))
            luaL_error(L, "multiDispatchForSize failed -- is uniform '%s' used?", ComputeShader::MultiDispatchUniformName);
        luaReturnSelf();
    }

    luaFunc(localsize)
    {
        const glm::uvec3 sz = This(L)->getLocalSize();
        luaReturnIvec3(sz.x, sz.y, sz.z);
    }

    luaFunc(groupsForSize)
    {
        const glm::uvec3 sz(
            lua_checkuint(L, 2),
            lua_touint(L, 3),
            lua_touint(L, 4)
        );
        luaReturnT(This(L)->getGroupsForSize(sz));
    }

    luaFunc(getInternalSource)
    {
        const char *src = This(L)->getInternalSource();
        lua_pushstringornil(L, src);
        return 1;
    }
};

const luaL_Reg LuaComputeShader::methods[] =
{
    luaRegisterNewDelete(LuaComputeShader),
    luaRegisterMethod(LuaComputeShader, loadstring),
    luaRegisterMethod(LuaComputeShader, loadfile),
    luaRegisterMethod(LuaComputeShader, dispatch),
    luaRegisterMethod(LuaComputeShader, multiDispatch),
    luaRegisterMethod(LuaComputeShader, dispatchForSize),
    luaRegisterMethod(LuaComputeShader, multiDispatchForSize),
    luaRegisterMethod(LuaComputeShader, localsize),
    luaRegisterMethod(LuaComputeShader, groupsForSize),
    luaRegisterMethod(LuaComputeShader, getInternalSource),
};

struct LuaSimpleShader : public LuaClassBind<SimpleShader>
{
    static const luaL_Reg methods[];

    luaFunc(new)
    {
        luaReturnObj(new SimpleShader);
    }

    // sh:loadstring(verStr, fragStr) -> bool, err
    luaFunc(loadstring)
    {
        SimpleShader *sh = This(L);
        const char *vert = luaL_checkstring(L, 2);
        const char *frag = luaL_checkstring(L, 3);
        sh->setSource(vert, frag);
        return _setupShader(L, *sh, 4);
    }

    // sh:loadfile(filename) -> bool, err
    luaFunc(loadfile)
    {
        SimpleShader *sh = This(L);
        const char *fn = luaL_checkstring(L, 2);
        sh->setFile(fn);
        return _setupShader(L, *sh, 3);
    }

    // FIXME: can enable as soon as shader binding is stacked
    /*luaFunc(render)
    {
        SimpleShaderBind bind(This(L));
        return luaForwardCall(L, 1);
    }*/

    luaFunc(bind)
    {
        This(L)->bind();
        luaReturnSelf();
    }

    luaFunc(getInternalSource)
    {
        const SimpleShader *sh = This(L);
        const char *v = sh->getInternalVertexSource();
        const char *f = sh->getInternalFragmentSource();
        lua_pushstringornil(L, v);
        lua_pushstringornil(L, f);
        return 2;
    }
};

const luaL_Reg LuaSimpleShader::methods[] =
{
    luaRegisterMethod(LuaSimpleShader, new), // inherited delete
    luaRegisterMethod(LuaSimpleShader, loadstring),
    luaRegisterMethod(LuaSimpleShader, loadfile),
    luaRegisterMethod(LuaSimpleShader, bind),
    luaRegisterMethod(LuaSimpleShader, getInternalSource),
    //luaRegisterMethod(LuaSimpleShader, render),

};

template<typename E, E TVAL, E FVAL>
static E getenumvalue(lua_State *L, int idx)
{
    switch(lua_type(L, idx))
    {
        case LUA_TNUMBER:
            return (E)lua_tointeger(L, idx);
        case LUA_TBOOLEAN:
            return lua_toboolean(L, idx) ? TVAL : FVAL;
    }
    luaL_argerror(L, idx, "expected enum value or bool");
    // not reached
    assert(false);
    return (E)0;
}

static VertexRenderMode getrendermode(lua_State *L, int idx)
{
    const char *s = lua_tostring(L, idx);
    if(s)
        switch(*s)
        {
            case 't': return VERTEX_RENDER_TRIANGLES;
            case 'l': return VERTEX_RENDER_LINES;
            case 'p': return VERTEX_RENDER_POINTS;
            case 's': return VERTEX_RENDER_LINE_STRIPS;
        }
    luaL_argerror(L, idx, "expected char (one of 'tlps')");
    return VERTEX_RENDER_TRIANGLES; // not reached
}

struct LuaMesh : public LuaClassBind<Mesh>
{
    static const luaL_Reg methods[];

    luaFunc(new)
    {
        luaReturnObj(new Mesh);
    }

    /*luaFunc(loadfile)
    {
        // TODO WRITE ME.
        // Do we really need a mesh loader? If so, which format(s)?
    }*/

    luaFunc(render)
    {
        Mesh *m = This(L);
        VertexRenderMode vr = getrendermode(L, 2);
        CullMethod cull = getenumvalue<CullMethod, CULL_BACK, CULL_NONE>(L, 3);
        DepthTest dtest = getenumvalue<DepthTest, DEPTH_TEST_ON, DEPTH_TEST_IGNORE>(L, 4);
        DepthWrite dw = getenumvalue<DepthWrite, DEPTH_WRITE_ON, DEPTH_WRITE_OFF>(L, 5);

        m->render(cull, dtest, vr, dw);
        luaReturnSelf();
    }


};

const luaL_Reg LuaMesh::methods[] =
{
    luaRegisterNewDelete(LuaMesh),
    //luaRegisterMethod(LuaMesh, loadfile),
    luaRegisterMethod(LuaMesh, render),
};

static void pushmesh(lua_State *L, MeshData *d)
{
    Mesh *m = new Mesh();
    m->load(*d);
    delete d;
    luaPushObj(m);
}

static void setmeshfield(lua_State *L, MeshData *d, int idx, const char *field)
{
    pushmesh(L, d);
    lua_setfield(L, idx, field);
}


struct LuaFrameBuffer : public LuaClassBind<FrameBuffer>
{
    static const luaL_Reg methods[];

    static unsigned getslot(lua_State *L, int idx)
    {
        unsigned slot = lua_touint(L, idx);
        if(slot < FrameBuffer::MAX_ATTACHMENTS)
            return slot;

        luaL_argerror(L, idx, "out of bounds slot");
        return 0;
    }

    // spec is a string that specifies fb attachments, space separated.
    // (Multiple spaces are skipped)
    static void _resize(lua_State *L, FrameBuffer *fb, glm::uvec2 sz, const char *spec)
    {
        if(!spec)
        {
            fb->init(sz, FrameBuffer::WITH_DEPTH_BUFFER, TEX_LINEAR);
            return;
        }

        TextureFlags texflags[FrameBuffer::MAX_ATTACHMENTS];
        unsigned tf = 0;
        unsigned fbf = FrameBuffer::NONE;
        unsigned n = 0;
        bool doit = false;
        for(const char *s = spec; ; ++s)
        {
            switch(*s)
            {
                // global framebuffer flags
                case 'd':  fbf |= FrameBuffer::WITH_DEPTH_TEXTURE; break;
                case 'D':  fbf |= FrameBuffer::WITH_DEPTH_BUFFER; break;

                // per-texture-attachment flags
                case 'l': tf |= TEX_LINEAR; doit = true; break;
                case 'm': tf |= TEX_MIPMAP; doit = true; break;
                case 'n': tf &= ~TEX_LINEAR; doit = true; break;
                case 'i': tf |= TEX_INTEGRAL; doit = true; break;

                case ' ':
                case 0:
                    if(doit)
                    {
                        if(n >= FrameBuffer::MAX_ATTACHMENTS)
                            luaL_error(L, "too many attachments specified");

                        texflags[n++] = (TextureFlags)tf;
                        doit = false;
                        tf = 0;
                    }
                    if(!*s)
                        goto done;
            }
        }
    done:
        fb->init(sz, (FrameBuffer::Flags)fbf, &texflags[0], n);
    }

    luaFunc(new)
    {
        FrameBuffer *fb = new FrameBuffer;
        if(lua_isnumber(L, 1) && lua_isnumber(L, 2))
            _resize(L, fb, lua_touvec2(L, 1), lua_tostring(L, 3));
        luaReturnObj(fb);
    }

    luaFunc(size)
    {
        luaReturnT(This(L)->getSize());
    }

    luaFunc(resize)
    {
        _resize(L, This(L), lua_touvec2(L, 2), lua_tostring(L, 4));
        luaReturnSelf();
    }

    luaFunc(capture)
    {
        FrameBufferCaptureScope cap(This(L));
        return luaForwardCall(L, 1, 0);
    }

    luaFunc(captureBlend)
    {
        FrameBufferCaptureScope cap(This(L));
        BlendMode::Enum blend = (BlendMode::Enum)lua_touint(L, 2);
        BlendMode::Scope blm(blend);
        return luaForwardCall(L, 2, 0);
    }

    luaFunc(texture)
    {
        luaReturnObj(This(L)->colorTex(getslot(L, 2)));
    }

    luaFunc(attach)
    {
        TextureAny *tex = luaGetObjOrNil<TextureAny>(L, 3);
        if(tex && tex->textype() != TEX_2D)
            luaL_argerror(L, 3, "must be 2D texture");
        if(!This(L)->attach(safe_reinterpret_cast<Texture2D*>(tex), getslot(L, 2)))
        {
            // Better to throw than return false silently
            luaL_error(L, "failed to attach texture");
        }
        luaReturnSelf();
    }

    luaFunc(clear)
    {
        This(L)->clear(lua_optvec4(L, 2, glm::vec4(0,0,0,0)));
        luaReturnSelf();
    }

    luaFunc(clearAll)
    {
        This(L)->clearAll(lua_optvec4(L, 2, glm::vec4(0,0,0,0)));
        luaReturnSelf();
    }

    luaFunc(clearColor)
    {
        This(L)->clearColor(lua_optvec4(L, 3, glm::vec4(0,0,0,0)), getslot(L, 2));
        luaReturnSelf();
    }

    luaFunc(clearDepth)
    {
        This(L)->clearDepth();
        luaReturnSelf();
    }

    luaFunc(CurrentSize) // static
    {
        glm::uvec2 s = FrameBuffer::GetCurrentViewport();
        luaReturnT(s);
    }

};
const luaL_Reg LuaFrameBuffer::methods[] =
{
    luaRegisterNewDelete(LuaFrameBuffer),
    luaRegisterMethod(LuaFrameBuffer, size),
    luaRegisterMethod(LuaFrameBuffer, resize),
    luaRegisterMethod(LuaFrameBuffer, attach),
    luaRegisterMethod(LuaFrameBuffer, capture),
    luaRegisterMethod(LuaFrameBuffer, captureBlend),
    luaRegisterMethod(LuaFrameBuffer, texture),
    luaRegisterMethod(LuaFrameBuffer, clear),
    luaRegisterMethod(LuaFrameBuffer, clearDepth),
    luaRegisterMethod(LuaFrameBuffer, clearAll),
    luaRegisterMethod(LuaFrameBuffer, clearColor),
    luaRegisterMethod(LuaFrameBuffer, CurrentSize),
};

struct ScriptGPUTimer : public GPUTimeSpan, public ScriptableT<ScriptGPUTimer>
{
    static const ScriptDef scriptdef;
};

const ScriptDef ScriptGPUTimer::scriptdef("gputimer", ScriptTypes::GPUTIMER);

struct GPUTimerRIAA
{
    const GPUTimeSpan& _gt;
    GPUTimerRIAA(lua_State *L, const GPUTimeSpan& gt) : _gt(gt)
    {
        if(gt.isCapturing())
            luaL_error(L, "Can't nest gputimer:call() of the same timer");
        gt.begin();
    }
    ~GPUTimerRIAA() { _gt.end(); }
};

struct LuaGPUTimer : public LuaClassBind<ScriptGPUTimer>
{
    static const luaL_Reg methods[];

    luaFunc(new)
    {
        luaReturnObj(new ScriptGPUTimer);
    }

    luaFunc(call)
    {
        GPUTimerRIAA t(L, *This(L));
        return luaForwardCall(L, 1, 0); // skip self
    }

    luaFunc(difftimeNS)
    {
        luaReturnInt(This(L)->getDiff());
    }

    luaFunc(difftimeMS)
    {
        luaReturnNum(This(L)->getDiff() / 1000000.0);
    }

    luaFunc(difftime)
    {
        luaReturnNum(This(L)->getDiff() / 1000000000.0);
    }

    luaFunc(capturing)
    {
        luaReturnBool(This(L)->isCapturing());
    }
};

const luaL_Reg LuaGPUTimer::methods[] =
{
    luaRegisterNewDelete(LuaGPUTimer),
    luaRegisterMethod(LuaGPUTimer, call),
    luaRegisterMethod(LuaGPUTimer, difftime),
    luaRegisterMethod(LuaGPUTimer, difftimeNS),
    luaRegisterMethod(LuaGPUTimer, difftimeMS),
    luaRegisterMethod(LuaGPUTimer, capturing),
};

luaFunc(finish)
{
    gpuutil::waitFinish();
    luaReturnNil();
}

luaFunc(barrier)
{
    gpuutil::barrier();
    luaReturnNil();
}

luaFunc(formatstr)
{
    const unsigned channels = lua_checkint(L, 1);
    checkchannels(L, channels);
    const GLSL::Type ty = lua_checkglsl(L, 2);
    const unsigned bits = lua_toint(L, 3); // optional
    const bool integral = getBool(L, 4);
    Blob3DType bty = gpuutil::fromGLSLType(ty, bits);
    if(bty == B3D_UNSPECIFIED)
        luaL_error(L, "%s is not a primitive type", GLSL::getName(ty));
    std::string fmt = gpuutil::getFormatString(channels, bty, integral);
    if(fmt.empty())
        luaReturnNil();
    luaReturnStr(fmt.c_str());
}

luaFunc(extensions)
{
    Renderer *r = Renderer::GetCurrentRenderer();
    if(r)
        luaReturnStr(r->getSupportedExtensions().c_str());
    
    luaReturnNil();
}

static const luaL_Reg gpulib[] =
{
    luaRegister(finish),
    luaRegister(barrier),
    luaRegister(formatstr),
    luaRegister(extensions),

    {NULL, NULL}
};

static const LuaEnum gpulibenum[] =
{
    luaRegisterEnum(CULL_NONE),
    luaRegisterEnum(CULL_FRONT),
    luaRegisterEnum(CULL_BACK),
    luaRegisterEnum(DEPTH_TEST_IGNORE),
    luaRegisterEnum(DEPTH_TEST_ON),
    luaRegisterEnum(DEPTH_WRITE_OFF),
    luaRegisterEnum(DEPTH_WRITE_ON),
    luaRegisterEnum(VERTEX_RENDER_TRIANGLES),
    luaRegisterEnum(VERTEX_RENDER_LINES),
    luaRegisterEnum(VERTEX_RENDER_POINTS),
    luaRegisterEnum(VERTEX_RENDER_LINE_STRIPS),
    { "BLEND_DONT_CHANGE", BlendMode::DONT_CHANGE },
    { "BLEND_REPLACE", BlendMode::REPLACE },
    { "BLEND_NORMAL", BlendMode::NORMAL },
    { "BLEND_ADD", BlendMode::ADD },
    { NULL, 0 },
};


int register_lua_gpu(lua_State *L)
{
    LuaStackCheck(L, 1);

    luaL_newlib(L, gpulib);
    luaRegisterClassInLib<LuaGPUBuffer>(L, -1);
    luaRegisterClassInLib<LuaTexture>(L, -1);
    luaRegisterClassInLib<LuaShaderProgramWrapper>(L, -1);
    luaRegisterClassInLib<LuaComputeShader>(L, -1);
    luaRegisterClassInLib<LuaSimpleShader>(L, -1);
    luaRegisterClassInLib<LuaMesh>(L, -1);
    luaRegisterClassInLib<LuaFrameBuffer>(L, -1);
    luaRegisterClassInLib<LuaGPUTimer>(L, -1);
    luaRegisterEnumInLib(L, gpulibenum, -1);

    lua_newtable(L);
    setmeshfield(L, MeshData::createQuad(), -2, "quad");
    setmeshfield(L, MeshData::createCube(), -2, "cube");
    lua_setfield(L, -2, "meshes");

    return 1;
}

#pragma once

#include <vector>
#include "luaobj.h"
#include "texture.h"
#include "glmx.h"
#include "deviceobject.h"

class FrameBufferCaptureScope;

class FrameBuffer : public DeviceObjectBase, public ScriptableT<FrameBuffer>
{
    friend class FrameBufferCaptureScope;
public:
    static const ScriptDef scriptdef;
    static const unsigned MAX_ATTACHMENTS = 8; // according to GL spec. (GL_MAX_DRAW_BUFFERS)

    enum Flags
    {
        NONE = 0,
        WITH_DEPTH_BUFFER = 0x01,
        WITH_DEPTH_TEXTURE = 0x02 // replaces WITH_DEPTH_BUFFER if set
    };

    FrameBuffer();
    ~FrameBuffer();
    bool init(glm::uvec2 dim, Flags flags, TextureFlags texflags = TEX_LINEAR); // init single target
    bool init(glm::uvec2 dim, Flags flags, const TextureFlags *ptexflags, unsigned num); // init multiple targets
    bool attach(Texture2D *tex, unsigned slot);
    Texture2D *colorTex(unsigned slot = 0);
    Texture2D *depthTex() const;
    const Texture2D* colorTex(unsigned slot = 0) const;
    glm::uvec2 getSize() const { return _dim; }
    void destroy();
    void clear(glm::vec4 col = glm::vec4(0)) const;
    void clearColor(glm::vec4 col, unsigned slot) const;
    void clearAll(glm::vec4 col = glm::vec4(0)) const;
    void clearDepth() const;

    static bool IsStackEmpty();
    static glm::uvec2 GetBaseViewport();
    static glm::uvec2 GetCurrentViewport();
    static void BindTop();
    static void CleanupRenderer(Renderer *r);

private:
    bool _initFB();
    void bind() const; // This is private on purpose, use FrameBufferCaptureScope instead

    static void Push(const FrameBuffer *fb);
    static void Pop();


    Tex2DRef _texs[MAX_ATTACHMENTS];
    Tex2DRef _dtex;
    unsigned _renderbuf;
    glm::uvec2 _dim;
    Flags _flags;

    struct PerStateData
    {
        unsigned fboId;
        bool rebind;
    };
    mutable PerStateHolder<unsigned, PerStateData> fbos;
    bool _inited;

    unsigned _getFBO() const;
    void _attachObjects(unsigned id) const;
    void _markOthersForRebind() const;

    // Inherited via DeviceObject
    virtual void onDeviceReset(bool init);
};

typedef CountedPtr<FrameBuffer> FrameBufRef;
typedef CountedPtr<const FrameBuffer> FrameBufCRef;

class FrameBufferCaptureScope
{
public:
    inline FrameBufferCaptureScope(const FrameBuffer& fb)
    {
        FrameBuffer::Push(&fb);
    }
    inline FrameBufferCaptureScope(const FrameBuffer *fb)
    {
        FrameBuffer::Push(fb);
    }
    inline FrameBufferCaptureScope(const FrameBufCRef& fb)
    {
        FrameBuffer::Push(fb.content());
    }
    inline FrameBufferCaptureScope(const FrameBufRef& fb)
    {
        FrameBuffer::Push(fb.content());
    }
    ~FrameBufferCaptureScope()
    {
        FrameBuffer::Pop();
    }
};

#pragma once

#include <glm/fwd.hpp>

namespace GLSL
{
    const unsigned MAX_TYPE_SIZE_BYTES = 16 * sizeof(double); // sizeof(dmat4)
    enum Type
    {
#define GLSLFAKETYPE(name) name,
#define PRIMTYPE(_, name) name,
#define GLSLTYPE(name) name,
#define GLSLSAMPLER(name) name,
#define GLSLIMAGE(name) name,
#include "glsl_gen.h"
        _MAXTYPE
    };

    const char *getName(Type ty);
    bool isSampler(Type ty);
    bool isImage(Type ty);
    bool isValid(unsigned ty);
    bool areTypesCompatible(Type a, Type b);
    bool areTypesMemoryCompatible(Type a, Type b);
    unsigned sizeOf(Type ty);
    unsigned scalarComponents(Type ty);

    template<typename T> struct TypeFor {};
    template<Type T> struct CTypeFor {};
    template<Type T> struct IsSampler { static const bool value = false; };
    template<Type T> struct IsImage { static const bool value = false; };

#define PRIMTYPE(ct, et) \
    template<> struct TypeFor<ct> { static const Type type = et; }; \
    template<> struct CTypeFor<et> { typedef ct type; };
#define GLSLTYPE(name) \
    template<> struct TypeFor<glm::name> { static const Type type = name; }; \
    template<> struct CTypeFor<name> { typedef glm::name type; };
#define GLSLSAMPLER(name) \
    template<> struct IsSampler<name> { static const bool value = true; }; \
    template<> struct CTypeFor<name> { typedef unsigned type; };
#define GLSLIMAGE(name) \
    template<> struct IsImage<name> { static const bool value = true; }; \
    template<> struct CTypeFor<name> { typedef unsigned type; };
#include "glsl_gen.h"

    template<typename T>
    struct IsCSampler { static const bool value = false; };
    template<>
    struct IsCSampler<unsigned> { static const bool value = true; };
    template<>
    struct IsCSampler<int>      { static const bool value = true; };

    template<typename T>
    struct IsCImage { static const bool value = false; };
    template<>
    struct IsCImage<unsigned> { static const bool value = true; };
    template<>
    struct IsCImage<int>      { static const bool value = true; };

    template<typename CTYPE, Type GLSLTYPE>
    struct Compatible
    {
        static const bool value
            = TypeFor<CTYPE>::type == GLSLTYPE
            || (IsCSampler<CTYPE>::value && IsSampler<GLSLTYPE>::value)
            || (IsCImage<CTYPE>::value && IsImage<GLSLTYPE>::value);
    };

    template<typename CTYPE>
    inline static bool isa(Type ty)
    {
        return TypeFor<CTYPE>::type == ty
            || (IsCSampler<CTYPE>::value && isSampler(ty))
            || (IsCImage<CTYPE>::value && isImage(ty));
    }

    class Typed
    {
    protected:
        Typed(Type ty) : glsltype(ty) {}
        virtual ~Typed() {} // virtual makes it polymorphic, needed for dynamic_cast
    public:
        const Type glsltype;
    };
}

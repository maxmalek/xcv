#pragma once

#include <stdint.h>
#include "glmx.h"
#include <string>
#include "gputimer.h"

struct SDL_Window;

class Renderer
{
public:

    enum Flags
    {
        NONE = 0,
        DEBUG = (1 << 0),
        VSYNC = (1 << 1),
    };

    static bool StaticInit();
    static void StaticShutdown();

    static Renderer *Create(SDL_Window *win, Renderer::Flags flags);
    static void Delete(Renderer *r);

    static void *GetCurrentContext();
    static unsigned GetCurrentGID();
    static Renderer *GetCurrentRenderer();

    static void ShowCompatFailReason(SDL_Window *win); // To be called when init() failed. Expected to explain why it failed, and what to do against it.

    static Renderer * const * const GetAllInstances();
    static size_t GetNumInstances();

    const void *getContext() const { return glctx; }
    unsigned getGlobalID() const { return _gid; }
    uint64_t getGPUTimeNS() const { return _gputimeNS; }

    bool init(Flags flags);
    inline bool isInited() const { return _inited; }
    void shutdown(); // clean exit but keep mostly initialized
    void killContext(); // unclean exit (clean up memory, but assume the context is dead and unusable)
    void resize();

    bool isCurrent() const;
    void makeCurrent();
    void makeCurrentUnsafe();
    void makeNotCurrent();
    void beginFrame();
    bool endFrame(); // returns true if everything is well, false if the context was reset
    void clear(const glm::vec4& col = glm::vec4(0,0,0,0));
    void clearDepth();
    void show();

    glm::uvec2 getViewport() const { return viewport; }

    unsigned getFreeVideoMemoryKB();
    unsigned getTotalVideoMemoryKB();

    const std::string& getGPUName() const { return _gpuname; }
    const std::string& getSupportedExtensions() const { return _supportedExt; }

    const void *getExtensionsCtx() const { return extctx; }

private:
    Renderer(SDL_Window *win);
    ~Renderer();

    void *privState;
    SDL_Window *window;
    void *glctx;
    void *apictx;
    void *extctx;
    std::string _gpuname;
    glm::uvec2 viewport;
    GPUTimeSpan _gputimer;
    uint64_t _gputimeNS;
    std::string _supportedExt;

    // global ID, unique among renderers
    // While a glctx pointer may have the same value twice during program execution,
    // There are no duplicated with this. Used for ownership tracking. Is never 0.
    const unsigned _gid;
    
    bool _inited;
    bool _failsafe;
};

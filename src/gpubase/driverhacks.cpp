#include "api_plugin.h"

// Always go for high performance
// via https://stackoverflow.com/questions/17458803/amd-equivalent-to-nvoptimusenablement
extern "C"
{
    EXPORT_API int NvOptimusEnablement = 0x00000001;
    EXPORT_API int AmdPowerXpressRequestHighPerformance = 1;
}

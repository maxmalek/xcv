#pragma once

#include <string>
#include "blob3ddef.h"
#include "glsl.h"

namespace gpuutil {

unsigned getB3dElemSize(Blob3DType ty);

// parses GLSL formats like rgba32f, rg8ui, etc
bool parseFormat(const char *s, unsigned *pchannels, Blob3DType *pty, bool *integral);
std::string getFormatString(unsigned c, Blob3DType ty, bool integral);
Blob3DType fromGLSLType(GLSL::Type glsltype, unsigned bits = 0);


// --- BACKEND SPECIFIC ---

void waitFinish();
void barrier();

}

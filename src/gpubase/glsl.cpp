#include "glsl.h"
#include "util.h"
#include "glmx.h"
#include <assert.h>

static const char *s_glsltypenames[] =
{
#define GLSLFAKETYPE(name) #name,
#define PRIMTYPE(name, _) #name,
#define GLSLTYPE(name) #name,
#define GLSLSAMPLER(name) #name,
#define GLSLIMAGE(name) #name,
#include "glsl_gen.h"
};

namespace GLSL {

const char *getName(Type ty)
{
    return ty < Countof(s_glsltypenames) ? s_glsltypenames[ty] : NULL;
}


bool isSampler(Type ty)
{
    switch(ty)
    {
#define GLSLSAMPLER(name) case name:
#include "glsl_gen.h"
        return true;
    default: ; // avoid warnings for unhandled cases
    }
    return false;
}

bool isImage(Type ty)
{
    switch(ty)
    {
#define GLSLIMAGE(name) case name:
#include "glsl_gen.h"
        return true;
    default: ; // avoid warnings for unhandled cases
    }
    return false;
}

bool isValid(unsigned ty)
{
    switch(ty)
    {
#define PRIMTYPE(cname, name) case name:
#define GLSLTYPE(name) case name:
#define GLSLSAMPLER(name) case name:
#define GLSLIMAGE(name) case name:
#define GLSLFAKETYPE(name) case name:
#include "glsl_gen.h"
        return true;

        default: ;
    }
    return false;
}

unsigned sizeOf(Type ty)
{
    switch(ty)
    {
#define PRIMTYPE(cname, name) case name: return sizeof(cname);
#define GLSLTYPE(name) case name: return sizeof(glm::name);
#define GLSLSAMPLER(name) case name: return sizeof(unsigned);
#define GLSLIMAGE(name) case name: return sizeof(unsigned);
#define GLSLFAKETYPE(name) case name: return 0;
#include "glsl_gen.h"

        default: ;
    }
    assert(false);
    return 0;
}

unsigned scalarComponents(Type ty)
{
    switch(ty)
    {
#define PRIMTYPE(cname, name) case name: return glmx::Info<cname>::scalar_components;
#define GLSLTYPE(name) case name: return glmx::Info<glm::name>::scalar_components;
#define GLSLSAMPLER(name) case name: return 1;
#define GLSLIMAGE(name) case name: return 1;
#define GLSLFAKETYPE(name) case name: return 1;
#include "glsl_gen.h"

        default: ;
    }
    assert(false);
    return 0;
}

struct GLSLTypeCompat
{
    Type a, b;
};

GLSLTypeCompat typecompat[] =
{
    { image1D, sampler1D },
    { image2D, sampler2D },
    { image3D, sampler3D },
    // Not 100% sure about these:
    { iimage1D, sampler1D },
    { iimage2D, sampler2D },
    { iimage3D, sampler3D },
    { uimage1D, sampler1D },
    { uimage2D, sampler2D },
    { uimage3D, sampler3D },
    { iimage1D, isampler1D },
    { iimage2D, isampler2D },
    { iimage3D, isampler3D },
    { uimage1D, usampler1D },
    { uimage2D, usampler2D },
    { uimage3D, usampler3D },
};

GLSLTypeCompat memcompat[] =
{
    { Int, Uint },
};

template<size_t N>
static bool checktypetab(const GLSLTypeCompat (&tab)[N], Type a, Type b)
{
    for(unsigned i = 0; i < N; ++i)
        if((a == tab[i].a && b == tab[i].b)
        || (a == tab[i].b && b == tab[i].a))
            return true;
    return false;
}

static bool _isHandle(Type a)
{
    return a == Uint || a == Uint64_t;
}

bool areTypesCompatible(Type a, Type b)
{
    if(a == b)
        return true;

    const bool ah = _isHandle(a);
    const bool bh = _isHandle(b);
    const bool atex = isImage(a) || isSampler(a);
    const bool btex = isImage(b) || isSampler(b);
    if((ah && btex) || (bh && atex))
        return true;

    return checktypetab(typecompat, a, b);
}

bool areTypesMemoryCompatible(Type a, Type b)
{
    if(a == b)
        return true;
    if(sizeOf(a) != sizeOf(b))
        return false;

    return checktypetab(memcompat, a, b);
}

} // end namespace GLSL

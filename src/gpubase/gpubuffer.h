#pragma once

#include <vector>
#include <stddef.h>
#include "macros.h"
#include "refcounted.h"
#include "glsl.h"
#include "luaobj.h"
#include "deviceobject.h"

class TextureAny;


class GPUBufferRaw : public DeviceObject, public ScriptableT<GPUBufferRaw>, public GLSL::Typed
{
public:
    static const ScriptDef scriptdef;

    enum Access
    {
        NONE  = 0x00,
        MAP_READ  = 0x01,
        MAP_WRITE = 0x02,
        UPDATE    = 0x04,
        MAP_RW    = MAP_READ | MAP_WRITE,
        PERSISTENT= 0x08,
        BACKUP    = 0x10,
    };

    static GPUBufferRaw *New(const void *data, size_t sz, Access a);
    static GPUBufferRaw *FromTexture(const TextureAny& tex, Access a);
    static GPUBufferRaw *FromFile(const char *fn, Access a);


    ~GPUBufferRaw();

    void unmap() const;
    const void *mapRead() const;
    void *mapWrite();
    void *mapRW();

    void zerofill();
    void importTexture(const TextureAny& tex, uint64_t offsetBytes);
    void uploadBytes(const void *ptr, uint64_t bytes, uint64_t offsetBytes);
    void downloadBytes(void *ptr, uint64_t bytes, uint64_t offsetBytes) const;
    void barrier();
    bool saveFile(const char *fn);

    // sync
    void insertFenceSync(); // issue a fence after writing to the buffer from a shader, and between reading from gpuread ptr.
    void waitFences(); // CPU waits for all prev. issued fences if necessary, but doesn't stall the rest of the pipeline
    void wait(); // stalls the pipeline. slow. avoid.

    // invalidate kills the contents and fences, and is the fastest thing to do if it needs to be refilled
    void invalidate();

    // Backup data on CPU memory
    void storeBackup() const;
    bool restoreBackup();
    bool deleteBackup() const;
    void _ensureBackup() const;

    inline uint64_t bytes() const { return _bufsize; }

    bool hasSpace(uint64_t sz, uint64_t offset = 0) const;

    // only when persistently mapped
    const void * const gpuread; // ... for reading
    void * const gpuwrite; // ... for writing

    const Access access;

protected:

    void _clearFences();

    const uint64_t _bufsize;
    std::vector<void*> _fences;

    inline GPUBufferRaw(unsigned id, uint64_t sz, Access a, const void *rp, void *wp)
        : GLSL::Typed(GLSL::buffer)
        , DeviceObject(id, 0)
        , gpuread(rp), gpuwrite(wp)
        , access(a), _bufsize(sz)
        , _backup(NULL)
    {
    }

private:

    mutable void *_backup;

    // non-copyable
    GPUBufferRaw(const GPUBufferRaw&);

    // Inherited via DeviceObject
    virtual void onDeviceReset(bool init);
};

template<typename T>
class GPUBuffer : public GPUBufferRaw
{
public:

    static GPUBuffer<T> *New(const T *ptr, uint64_t n, Access a)
    {
        compile_assert(sizeof(GPUBuffer<T>) == sizeof(GPUBufferRaw));
        const uint64_t bytes = n * sizeof(T);
        return static_cast<GPUBuffer<T>*>(GPUBufferRaw::New(ptr, bytes, a));
    }

    void upload(const T *ptr, uint64_t n, uint64_t offset)
    {
        uploadBytes(ptr, n * sizeof(T), offset * sizeof(T));
    }
    inline uint64_t size() const { return bytes() / sizeof(T); }

    const T *mapRead() { return (const T*)GPUBufferRaw::mapRead();  }
    T *mapWrite()      { return (      T*)GPUBufferRaw::mapWrite(); }
    T *mapRW()         { return (      T*)GPUBufferRaw::mapRW();    }

    typedef CountedPtr<GPUBuffer<T> > Ref;
};

typedef CountedPtr<GPUBufferRaw> GPUBufRef;

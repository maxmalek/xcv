#include "shader.h"
#include <sstream>
#include "renderer.h"
#include "log.h"
#include "gpuutil.h"

const ScriptDef ShaderProgramWrapper::scriptdef("programwrapper", ScriptTypes::SHADERWRAPPER);
const ScriptDef ComputeShader::scriptdef("computeshader", ScriptTypes::COMPUTESHADER, &ShaderProgramWrapper::scriptdef);
const ScriptDef SimpleShader::scriptdef("simpleshader", ScriptTypes::SIMPLESHADER, &ShaderProgramWrapper::scriptdef);

ShaderProgramWrapper::ShaderProgramWrapper(const ScriptDef& def)
: prog(new ShaderProgram)
{
}

void ShaderProgramWrapper::copyVariables(const ShaderProgramWrapper & from) const
{
    if(prog && from.prog)
        prog->copyVariables(*from.prog);
}

//  -----------------------------------------------------------------

const char * const ComputeShader::MultiDispatchUniformName = "multiDispatchId";

ComputeShader::ComputeShader()
: ShaderProgramWrapper(scriptdef)
, comp(new ShaderObject(ShaderType::COMPUTE))
{
}

ComputeShader::ComputeShader(const char *name)
: ShaderProgramWrapper(scriptdef)
, comp(new ShaderObject(ShaderType::COMPUTE))
{
    setFile(name);
}

void ComputeShader::setFile(const char *name)
{
    std::string f = name;
    f += ".comp";
    comp->setFileName(f.c_str());
    prog->attach(comp);
}

void ComputeShader::setSource(const char * src)
{
    comp->setCode(src);
    prog->attach(comp);
}

const char * ComputeShader::getFileName() const
{
    return comp->getFileName();
}

const char * ComputeShader::getInternalSource() const
{
    return comp->getInternalSource();
}

bool ComputeShader::load(const ShaderLoadCallbacks *callbacks, void *udcb)
{
    bool ok = comp->load(callbacks) && prog->link(callbacks);
    if(ok)
        prog->getComputeWorkGroupSize(glm::value_ptr(localSize));
    else
        localSize = glm::uvec3(0);
    return ok;
}

void ComputeShader::dispatchComputeSingle(glm::uvec3 g) const
{
    g = glm::max(g, glm::uvec3(1, 1, 1));
    if(const ProgramUniform *u = prog->lookupUniform(MultiDispatchUniformName))
        u->set(glm::uvec3(0));
    prog->dispatchComputeSingle(g.x, g.y, g.z);
}

bool ComputeShader::dispatchComputeMulti(glm::uvec3 g, glm::uvec3 n) const
{
    const ProgramUniform *u = prog->lookupUniform(MultiDispatchUniformName);
    if(!u)
        return false;

    g = glm::max(g, glm::uvec3(1, 1, 1));
    n = glm::max(n, glm::uvec3(1, 1, 1));

    prog->dispatchComputeMultiBegin();
    glm::uvec3 i;
    for(i.z = 0; i.z < n.z; ++i.z)
        for(i.y = 0; i.y < n.y; ++i.y)
            for(i.x = 0; i.x < n.x; ++i.x)
            {
                u->set(i);
                prog->_dispatchCompute(g.x, g.y, g.z);
            }
    prog->dispatchComputeMultiEnd();
    return true;
}

static glm::uvec3 groupsforsize(glm::uvec3 size, const glm::uvec3 local)
{
    size = glm::max(size, glm::uvec3(1, 1, 1));
    return (size + (local - 1u)) / local;
}

glm::uvec3 ComputeShader::getGroupsForSize(glm::uvec3 size) const
{
    return groupsforsize(size, localSize);
}

void ComputeShader::dispatchComputeForSizeSingle(glm::uvec3 size) const
{
    dispatchComputeSingle(getGroupsForSize(size));
}

bool ComputeShader::dispatchComputeForSizeMulti(glm::uvec3 size, glm::uvec3 maxsize) const
{
    if(maxsize == glm::uvec3(0,0,0) || glm::all(glm::lessThan(size, maxsize)))
    {
        dispatchComputeForSizeSingle(size);
        return true;
    }

    const glm::uvec3 totaldispatches = groupsforsize(size, glm::max(maxsize, glm::uvec3(1)));
    const glm::uvec3 usesize = glm::min(size, maxsize);
    const glm::uvec3 groupsPerDispatch = groupsforsize(usesize, localSize);
    
    return dispatchComputeMulti(groupsPerDispatch, totaldispatches);
}

// -----------------------------------------------------------------


SimpleShader::SimpleShader()
: ShaderProgramWrapper(scriptdef)
, vertex(new ShaderObject(ShaderType::VERTEX))
, fragment(new ShaderObject(ShaderType::FRAGMENT))
{
}

SimpleShader::SimpleShader(const char *name)
: ShaderProgramWrapper(scriptdef)
, vertex(new ShaderObject(ShaderType::VERTEX))
, fragment(new ShaderObject(ShaderType::FRAGMENT))
{
    setFile(name);
}

void SimpleShader::setSource(const char * vert, const char * frag)
{
    vertex->setCode(vert);
    fragment->setCode(frag);
    prog->attach(vertex);
    prog->attach(fragment);
}

void SimpleShader::setFile(const char *name)
{
    std::string n = name;
    vertex->setFileName((n + ".vert").c_str());
    fragment->setFileName((n + ".frag").c_str());
    prog->attach(vertex);
    prog->attach(fragment);
}

bool SimpleShader::load(const ShaderLoadCallbacks *callbacks, void *udcb)
{
    return vertex->load(callbacks)
        && fragment->load(callbacks)
        && prog->link(callbacks);
}

const char * SimpleShader::getInternalVertexSource() const
{
    return vertex->getInternalSource();
}

const char * SimpleShader::getInternalFragmentSource() const
{
    return fragment->getInternalSource();
}


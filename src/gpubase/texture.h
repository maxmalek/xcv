#pragma once

#include "refcounted.h"
#include "macros.h"
#include <assert.h>
#include "glmx.h"
#include "gpuutil.h"
#include "glsl.h"
#include "luaobj.h"
#include <stdint.h>
#include "perstateholder.h"
#include "deviceobject.h"


class GPUBufferRaw;
class Renderer;

enum TextureType
{
    TEX_ANY,
    TEX_1D, // conveniently 1, 2, 3
    TEX_2D,
    TEX_3D,
};

enum TextureFlags
{
    TEX_NONE          = 0,
    TEX_MIPMAP        = 0x0001,
    TEX_LINEAR        = 0x0002,
    TEX_KEEP_BACKUP   = 0x0004, // Automatically keeps a backup in CPU memory when a texture is uploaded from CPU
    TEX_INTEGRAL      = 0x0008,

    TEX_MIPMAP_LINEAR = TEX_MIPMAP | TEX_LINEAR,
};

// can be used as channel. All here must be < 0
enum TextureChannelSpecial
{
    TEXCH_IS_DEPTH = -1
};

namespace TexPriv
{
    static const GLSL::Type samplertypes[] = // same order as TextureType
    {
        GLSL::unknown,
        GLSL::sampler1D,
        GLSL::sampler2D,
        GLSL::sampler3D
    };
    unsigned textureTypeToTarget(TextureType);
    TextureType sizeToTexType(glm::uvec3 sz);

    template<TextureType TT> struct SizeType {};
    template<> struct SizeType<TEX_1D>
    {
        typedef unsigned type;
        inline static glm::uvec3 pad(type in) { return glm::uvec3(in, 0, 0); }
        inline static type get(glm::uvec3 in) { return in.x; }
    };
    template<> struct SizeType<TEX_2D>
    {
        typedef glm::uvec2 type;
        inline static glm::uvec3 pad(type in) { return glm::uvec3(in, 0); }
        inline static type get(glm::uvec3 in) { return type(in.x, in.y); }
    };
    template<> struct SizeType<TEX_3D>
    {
        typedef glm::uvec3 type;
        inline static type pad(type in) { return in; }
        inline static type get(type in) { return in; }
    };
    template<> struct SizeType<TEX_ANY>
    {
        typedef glm::uvec3 type;
        inline static type pad(type in) { return in; }
        inline static glm::uvec3 pad(unsigned in) { return glm::uvec3(in, 0, 0); }
        inline static glm::uvec3 pad(glm::uvec2 in) { return glm::uvec3(in, 0); }
        inline static type get(type in) { return in; }
    };

    glm::uvec3 fixSize(glm::uvec3, TextureType);

    template<TextureType ty> struct _TexTarget
    {
        static const TextureType type = ty;
        _TexTarget(TextureType) : _target(textureTypeToTarget(ty)) { compile_assert(ty != TEX_ANY); }
        static unsigned target() { return textureTypeToTarget(ty); } // important that this is static
    private:
        const unsigned _target; // IMPORTANT for ensuring that the memory layout is the same as _AnyTarget
    };

    struct _AnyTarget
    {
        static const TextureType type = TEX_ANY;
        _AnyTarget(TextureType ty) : _target(textureTypeToTarget(ty)) {}
        _AnyTarget(unsigned ta) : _target(ta) {}
        unsigned target() const { return _target; }
    private:
        const unsigned _target;
    };
};


class _TextureBase : public DeviceObject, public ScriptableT<_TextureBase>, public GLSL::Typed
{
public:
    static const ScriptDef scriptdef;
    typedef uint64_t Handle;

    enum Access
    {
        READONLY,
        WRITEONLY,
        READWRITE,
    };

    struct Residency
    {
        inline Residency() : mask(0), imgaccess(READONLY) {}
        unsigned mask;
        Access imgaccess;
    };
    typedef PerStateHolder<unsigned, Residency> ResidencyHolder;

    bool GLSLcompatibleWith(const GLSL::Typed& o) const { return GLSLcompatibleWith(o.glsltype); }
    bool GLSLcompatibleWith(GLSL::Type ty) const;

    inline glm::uvec3 size3() const { return _size0; } // unused dims = 0
    inline glm::uvec3 internalsize() const { return _size; } // unused dims = 1
    inline glm::vec3 displaysize() const { return pixelSize * glm::vec3(_size0); }
    glm::vec3 displaysizeRatio() const; // like displaysize(), but normalized so that the smallest size is 1.0. unused dims = 0.0
    glm::vec3 displaysizeScaled() const; // like size3(), but scaled so that pixelSize is taken into account

    inline Blob3DType datatype() const { return _datatype; }
    inline unsigned channels() const { return _channels; }
    inline unsigned internalformat() const { return _internalformat; }
    inline TextureFlags flags() const { return _flags; }
    size_t getSizeBytes(unsigned miplevel = 0) const;
    void fillInfo(Blob3DInfo& info) const;
    void barrier();
    Handle getImageHandle() const;
    Handle getTexHandle() const;
    void makeTexResident() const;
    void makeTexNotResident() const;
    void makeImageResident(Access a);
    void makeImageNotResident();
    void clearAllResidency();
    bool deleteBackup();
    void setSwizzleMask(const char *mask); // string length exactly 4, each char any of "rgba01"
    const char *getSwizzleMask() const { return &_swizzle[0]; }

    void adjustParams(Blob3DType& datatype, unsigned& channels) const; // if datatype == unspecified, set to tex's. if channels == 0, set to tex's. otherwise leave as is. (utility method)

    IOResult saveFile(const char *fn, const char *format = NULL, unsigned miplevel = 0) const;

    static uint64_t SizeInBytes(const Blob3DInfo& info, unsigned miplevel = 0);
    static unsigned GetInternalFormat(unsigned channels, Blob3DType datatype, TextureFlags flags); // 0 if invalid format
    static unsigned CalcMipLevel(TextureFlags flags, glm::uvec3 sz);
    static uint64_t SizeInBytes(glm::uvec3 sz, Blob3DType datatype, unsigned channels, unsigned miplevel = 0);
    static glm::uvec3 CalcMipLevelSize(glm::uvec3 sz, unsigned miplevel);

    static void CleanupRenderer(Renderer *r);

protected:
    explicit _TextureBase(unsigned id, TextureType ty, glm::uvec3 sz, Blob3DType datatype, unsigned channels, TextureFlags flags);
    virtual ~_TextureBase();
    
    //void _bind(unsigned target) const;
    void _bindUnit(unsigned target, unsigned unit) const;
    void _bindImageUnit(unsigned unit, Access a) const;
    static void _unbind(unsigned target);
    static void _unbindImage(unsigned target, unsigned unit);
    void _generateMipmaps(unsigned target) const;
    void _upload(unsigned target, const void *data, Blob3DType datatype, unsigned channels, unsigned miplevel = 0);
    void _upload(unsigned target, const GPUBufferRaw& buf, size_t offset, Blob3DType datatype, unsigned channels, unsigned miplevel = 0);
    void _download(unsigned target, void *dst, size_t bufsz, unsigned miplevel = 0) const;
    void _downloadAsync(unsigned target, GPUBufferRaw& buf, size_t offset, unsigned miplevel = 0) const;
    void _zerofill(unsigned target) const;
    void _invalidate(unsigned target) const;
    void _afterUpload(unsigned target, unsigned miplevel);

    void _storeBackup(unsigned target, unsigned miplevel) const;
    bool _restoreBackup(unsigned target);

    void _onDeviceReset(unsigned target, bool init);

    static unsigned _New(unsigned target, glm::uvec3 sz, Blob3DType datatype, unsigned channels, TextureFlags flags);
    unsigned _convert(unsigned target, Blob3DType datatype, unsigned channels, TextureFlags flags) const;

protected:
    const TextureType _ty;
    const glm::uvec3 _size, // unused dimensions are 1
                     _size0; // unused dimensions are 0
    const Blob3DType _datatype;
    const unsigned _channels;
    const unsigned _internalformat;
    const unsigned _miplevel;
    const TextureFlags _flags;
    mutable Handle _handleImg, _handleTex; // Handles for resident textures
    mutable ResidencyHolder _residency;
    char _swizzle[5]; // 0-terminated
public:
    glm::vec3 pixelSize;
private:
    _TextureBase(const _TextureBase&); // forbid copying
    _TextureBase& operator=(const _TextureBase&); // forbid assignment
    void _queryHandles() const;
    void _ensureHandles() const;
    mutable void *_backup;
    mutable unsigned _backupMiplevel;

    // Backend-specific device functions
    void _dev_init();
    void _dev_delete();
    void _dev_upload(unsigned target, unsigned miplevel, const void *data, Blob3DType datatype, unsigned channels);
    void _dev_upload(unsigned target, unsigned miplevel, const GPUBufferRaw & buf, size_t offset, Blob3DType datatype, unsigned channels);
    void _dev_download(unsigned target, unsigned miplevel, void *dst, size_t bufsz) const;
    void _dev_download(unsigned target, unsigned miplevel, GPUBufferRaw & buf, size_t offset) const;
    void _dev_setSwizzleMask(const char * mask);
};

class TextureAny;

template<typename Target>
class _Texture : public _TextureBase
{
public:
    static const TextureType staticType = Target::type;
private:
    typedef TexPriv::SizeType<staticType> SZT;
public:
    typedef typename SZT::type size_type;

    inline explicit _Texture(unsigned id, size_type sz, Blob3DType datatype, unsigned channels, TextureFlags flags, const char *swizzle, TextureType ty = Target::type)
        : _TextureBase(id, ty, SZT::pad(sz), datatype, channels, flags)
        , t(ty)
    {
        assert(ty != TEX_ANY && (ty == staticType || staticType == TEX_ANY));
        setSwizzleMask(swizzle);
    }
    inline unsigned target() const { return t.target(); }
    inline TextureType textype() const { return Target::type != TEX_ANY ? Target::type : _ty; }
    inline unsigned dimensions() const { return textype(); } // conveniently matches
    inline size_type size() const { return SZT::get(_size); } // vector has as many dimensions as the texture

    inline       TextureAny *asAny()       { return reinterpret_cast<      TextureAny*>(this); }
    inline const TextureAny *asAny() const { return reinterpret_cast<const TextureAny*>(this); }

    // Backend-specific
    inline void bind() const { _bind(target()); }
    inline void bindUnit(unsigned unit) const { _bindUnit(target(), unit); }
    inline void bindImageUnit(unsigned unit, Access a) const { _bindImageUnit(unit, a); }
    inline void unbind()
    {
        _unbind(target());
    }
    inline void unbindImage(unsigned unit)
    {
        _unbindImage(target(), unit);
    }
    inline void generateMipmaps() const { _generateMipmaps(target()); }
    inline void upload(const void *data, Blob3DType datatype, unsigned channels)
    {
        _upload(target(), data, datatype, channels);
    }
    inline void upload(const GPUBufferRaw& buf, size_t offset, Blob3DType datatype, unsigned channels)
    {
        _upload(target(), buf, offset, datatype, channels);
    }
    inline void download(void *data, size_t bufsz, unsigned miplevel = 0) const
    {
        _download(target(), data, bufsz, miplevel);
    }
    inline void downloadAsync(GPUBufferRaw& buf, size_t offset, unsigned miplevel = 0) const
    {
        _downloadAsync(target(), buf, offset, miplevel);
    }
    inline void zerofill()
    {
        _zerofill(target());
    }
    inline void invalidate()
    {
        _invalidate(target());
    }
    inline void storeBackup(unsigned miplevel = 0) const
    {
        _storeBackup(target(), miplevel);
    }
    inline bool restoreBackup()
    {
        _restoreBackup(target());
    }

    static _Texture<Target> *New(size_type sz, Blob3DType datatype, unsigned channels, TextureFlags flags, const char *swizzle)
    {
        const unsigned id = _New(Target::target(), SZT::pad(sz), datatype, channels, flags);
        return id ? new _Texture<Target>(id, sz, datatype, channels, flags, swizzle) : NULL;
    }

    static _Texture<Target> *LoadFile(const char *fn, Blob3DType datatype, TextureFlags flags, const char *swizzle, Blob3DInfo *outInfo);

    _Texture<Target> *convert(Blob3DType datatype, unsigned channels, TextureFlags flags, const char *swizzle) const
    {
        adjustParams(datatype, channels);
        const unsigned id = _convert(target(), datatype, channels, flags);
        if(!id)
            return NULL;
        _Texture<Target> *tex = new _Texture<Target>(id, _size0, datatype, channels, flags, swizzle);
        tex->_afterUpload(target(), 0);
        return tex;
    }

protected:
    virtual ~_Texture() {}

private:
    const Target t;
    _Texture(const _Texture&); // forbid copying
    _Texture& operator=(const _Texture&); // forbid assignment

    // Inherited via DeviceObject
    virtual void onDeviceReset(bool init) { _onDeviceReset(target(), init); }
};

typedef _Texture<TexPriv::_TexTarget<TEX_1D> > Texture1D;
typedef _Texture<TexPriv::_TexTarget<TEX_2D> > Texture2D;
typedef _Texture<TexPriv::_TexTarget<TEX_3D> > Texture3D;


class TextureAny : public _Texture<TexPriv::_AnyTarget>
{
public:
    typedef _Texture<TexPriv::_AnyTarget> Base;
    typedef TexPriv::SizeType<Base::staticType> _SZT;
    typedef typename _SZT::type size_type;

    explicit TextureAny(unsigned id, size_type sz, Blob3DType datatype, unsigned channels, TextureFlags flags, const char *swizzle, TextureType ty)
        : Base(id, sz, datatype, channels, flags, swizzle, ty) {}

    // Parameter 'info/outInfo' always contains the actual, untouched type of the memory that was read.
    // If the passed 'datatype' parameter is != B3D_UNSPECIFIED, convert so that the output is of that type (will NOT be recorded in 'outInfo'!)

    static TextureAny *LoadFile(const char *fn, Blob3DType datatype, TextureFlags flags, const char *swizzle, Blob3DInfo *outInfo = NULL);
    static void *LoadFileAsMem(const char *fn, Blob3DType datatype = B3D_UNSPECIFIED, Blob3DInfo *outInfo = NULL); // returned pointer must be free()'d

    template<typename T>
    static T *LoadFileAsMem(const char *fn, Blob3DInfo *outInfo = NULL)
    {
        const Blob3DType datatype = ToBlob3dType<T>::value;
        return (T*)LoadFileAsMem(fn, datatype, outInfo);
    }

    static TextureAny *NewFromMem(const void *data, const Blob3DInfo& info, Blob3DType datatype, TextureFlags flags, const char *swizzle); // convert to given datatype internally

    static TextureAny *New(size_type sz, Blob3DType datatype, unsigned channels, TextureFlags flags, const char *swizzle)
    {
        const glm::uvec3 szp = _SZT::pad(sz);
        const TextureType tt = TexPriv::sizeToTexType(szp);
        unsigned target = TexPriv::textureTypeToTarget(tt);
        if(tt == TEX_ANY)
            return NULL;
        const unsigned id = _New(target, szp, datatype, channels, flags);
        return id ? new TextureAny(id, sz, datatype, channels, flags, swizzle, tt) : NULL;
    }

    TextureAny *convert(Blob3DType datatype, unsigned channels, TextureFlags flags, const char *swizzle) const
    {
        adjustParams(datatype, channels);
        const unsigned id = _convert(target(), datatype, channels, flags);
        if(!id)
            return NULL;
        TextureAny *tex = new TextureAny(id, _size0, datatype, channels, flags, swizzle, textype());
        tex->_afterUpload(target(), 0);
        return tex;
    }

    // this ctor is EVIL and error-prone! better not to have it.
    /*template<typename T>
    TextureAny(const T& o) : Base(
        o.id(),
        o.id() ? _SZT::pad(o.size()) : _SZT::pad(typename T::size_type(0)),
        o.id() ? o.datatype() : B3D_UNSPECIFIED,
        o.id() ? o.channels() : 0,
        o.textype()
    ) {}*/

};

template<typename Target>
/* static */  _Texture<Target> *_Texture<Target>::LoadFile(const char *fn, Blob3DType datatype, TextureFlags flags, const char *swizzle, Blob3DInfo *outInfo)
{
    TextureAny *tex = TextureAny::LoadFile(fn, datatype, flags, swizzle, outInfo);
    if(staticType == tex->textype())
        return reinterpret_cast<_Texture<Target>*>(tex);

    delete tex;
    return NULL;
}

// These survive calls when pointer is NULL
typedef CountedPtr<Texture1D> Tex1DRef;
typedef CountedPtr<Texture2D> Tex2DRef;
typedef CountedPtr<Texture3D> Tex3DRef;

// Can't be NULL when called
typedef CountedPtr<TextureAny> TexAnyRef;
typedef CountedPtr<const TextureAny> TexAnyCRef;

namespace TexPriv
{
    template<typename T>
    struct Convert
    {
        // Since the memory layout of all texture classes is the same, we can just do cast wizardry
        typedef typename T::value_type TT;
        static const T& as(const TexAnyRef& in)
        {
            compile_assert(sizeof(T) == sizeof(in));
            compile_assert(sizeof(TT) == sizeof(typename TexAnyRef::value_type));
            assert(!in || TT::staticType == in->textype());
            return in.reinterpret<TT>();
        }
        static const TexAnyRef& any(const T& in)
        {
            compile_assert(sizeof(in) == sizeof(TexAnyRef));
            return in.template reinterpret<TextureAny>();
        }
    };
}


template<typename T>
inline const T& as(const TexAnyRef& in)
{
    return TexPriv::Convert<T>::as(in);
}

template<typename T>
inline const TexAnyRef& asAny(const T& in)
{
    return TexPriv::Convert<T>::any(in);
}

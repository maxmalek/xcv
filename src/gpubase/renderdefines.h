#pragma once

enum KnownAttribs
{
    KA_VERTEX,        // "in_vertex"
    KA_VERTEXNORMAL,  // "in_vertexnormal"
    KA_VERTEXUV,      // "in_vertexUV"
    KA_VERTEXCOLOR,   // "in_vertexColor"
    KA_USERIDX,       // "in_userIdx"

    KA_MAX
};

extern const char * const KnownAttribNames[KA_MAX];

enum CullMethod
{
    CULL_NONE,
    CULL_FRONT,
    CULL_BACK
};

enum DepthTest
{
    DEPTH_TEST_IGNORE,
    DEPTH_TEST_ON,
};

enum DepthWrite
{
    DEPTH_WRITE_OFF,
    DEPTH_WRITE_ON,
};

enum VertexRenderMode
{
    VERTEX_RENDER_TRIANGLES,
    VERTEX_RENDER_LINES,
    VERTEX_RENDER_POINTS,
    VERTEX_RENDER_LINE_STRIPS,

    VERTEX_RENDER_MAX,
};

/*
enum AccessType
{
    // usable for creation and mapping:
    ACCESS_NONE = 0x00,
    ACCESS_READ = 0x01,
    ACCESS_WRITE = 0x02,
    ACCESS_READ_WRITE = ACCESS_READ | ACCESS_WRITE,
    // only effective for creation
    ACCESS_UPDATE = 0x04,
    ACCESS_COHERENT = 0x08,
};
*/

#include "texture.h"
#include "util.h"
#include <assert.h>
#include "log.h"
#include "gpubuffer.h"
#include "blob3dio.h"
#include "macros.h"
#include "gpubuffer.h"
#include "platform.h"


const ScriptDef _TextureBase::scriptdef("texture", ScriptTypes::TEXTURE);

using namespace TexPriv;

static void UNUSED _compile_checks()
{
    // Make sure all texture types are binary compatible and can be reinterpret_cast'ed to and from TextureAny.
    // Can't check for member offsets this way, but this is as good as it gets.
    assert(sizeof(Texture1D) == sizeof(TextureAny));
    assert(sizeof(Texture2D) == sizeof(TextureAny));
    assert(sizeof(Texture3D) == sizeof(TextureAny));
}

TextureType TexPriv::sizeToTexType(glm::uvec3 sz)
{
    if(sz.x && !sz.y && !sz.z)
        return TEX_1D;
    else if(sz.x && sz.y && !sz.z)
        return TEX_2D;
    else if(sz.x && sz.y && sz.z)
        return TEX_3D;

    return TEX_ANY; // can't deduce texture type from size
}

glm::uvec3 TexPriv::fixSize(glm::uvec3 sz, TextureType tt)
{
    const bool allzero = !(sz.x | sz.y | sz.z);
    switch(tt)
    {
        case TEX_1D:
            assert(allzero || (sz.x != 0 && sz.y == 0 && sz.z == 0));
            return glm::uvec3(sz.x, 1, 1);

        case TEX_2D:
            assert(allzero || (sz.x != 0 && sz.y != 0 && sz.z == 0));
            return glm::uvec3(sz.x, sz.y, 1);

        case TEX_3D:
            assert(allzero || (sz.x != 0 && sz.y != 0 && sz.z != 0));
            return sz;

        case TEX_ANY:
            return sz;
    }
    assert(false);
    return glm::uvec3();
}

_TextureBase::_TextureBase(unsigned id, TextureType ty, glm::uvec3 sz, Blob3DType datatype, unsigned channels, TextureFlags flags)
    : GLSL::Typed(TexPriv::samplertypes[ty])
    , DeviceObject(id, 0) // primitive
    , _ty(ty)
    , _size(TexPriv::fixSize(sz, ty)), _size0(sz)
    , _datatype(datatype), _channels(channels)
    , _internalformat(GetInternalFormat(channels, datatype, flags))
    , _miplevel(CalcMipLevel(flags, sz))
    , _flags(flags), _handleImg(0), _handleTex(0)
    , pixelSize(1.0f), _backup(NULL), _backupMiplevel(-1)
{
    assert(datatype != B3D_UNSPECIFIED);
    assert(channels && channels <= 4);

    for(unsigned i = 0; i < 5; ++i)
        _swizzle[i] = 0;

    _dev_init();
}

_TextureBase::~_TextureBase()
{
    DEBUG_LOG("del tex %u (%ux%ux%u:%u)", _id, _size.x, _size.y, _size.z, _channels);
    deleteBackup();
    clearAllResidency();
    _dev_delete();
}

void _TextureBase::_afterUpload(unsigned target, unsigned miplevel)
{
    if(_flags & TEX_MIPMAP)
        _generateMipmaps(target);

    if(_flags & TEX_KEEP_BACKUP)
        _storeBackup(target, miplevel);
}

// data must be a normal client pointer
void _TextureBase::_upload(unsigned target, const void *data, Blob3DType datatype, unsigned channels, unsigned miplevel)
{
    _dev_upload(target, miplevel, data, datatype, channels);
    _afterUpload(target, miplevel);
}

void _TextureBase::_upload(unsigned target, const GPUBufferRaw & buf, size_t offset, Blob3DType datatype, unsigned channels, unsigned miplevel)
{
    assert(buf.hasSpace(getSizeBytes(miplevel), offset));

    _dev_upload(target, miplevel, buf, offset, datatype, channels);
    _afterUpload(target, miplevel);
}

uint64_t _TextureBase::SizeInBytes(glm::uvec3 sz, Blob3DType datatype, unsigned channels, unsigned miplevel)
{
    assert(channels);
    assert(datatype != B3D_UNSPECIFIED);
    sz = CalcMipLevelSize(sz, miplevel);
    return uint64_t(sz.x) * uint64_t(sz.y) * uint64_t(sz.z) * uint64_t(channels) * uint64_t(gpuutil::getB3dElemSize(datatype));
}

void _TextureBase::adjustParams(Blob3DType& datatype, unsigned& channels) const
{
    if(!channels)
        channels = _channels;
    if(datatype == B3D_UNSPECIFIED)
        datatype = _datatype;
}

void _TextureBase::_download(unsigned target, void *dst, size_t bufsz, unsigned miplevel) const
{
    const size_t bytes = SizeInBytes(internalsize(), _datatype, _channels, miplevel);
    assert(bufsz >= bytes);
    _dev_download(target, miplevel, dst, bufsz);
}

void _TextureBase::_downloadAsync(unsigned target, GPUBufferRaw & buf, size_t offset, unsigned miplevel) const
{
    const size_t bytes = SizeInBytes(internalsize(), _datatype, _channels, miplevel);
    assert(buf.hasSpace(bytes, offset));
    _dev_download(target, miplevel, buf, offset);
}

void _TextureBase::_storeBackup(unsigned target, unsigned miplevel) const
{
    const size_t bytes = getSizeBytes(miplevel);
    free(_backup);
    _backup = malloc(bytes);
    _dev_download(target, miplevel, _backup, bytes);
    _backupMiplevel = miplevel;
}

bool _TextureBase::_restoreBackup(unsigned target)
{
    if(!_backup)
        return false;

    _dev_upload(target, _backupMiplevel, _backup, _datatype, _channels);
    return true;
}

bool _TextureBase::deleteBackup()
{
    if(!_backup)
        return false;

    free(_backup);
    _backup = NULL;
    _backupMiplevel = -1;
    return true;
}

// Note: Calling glTextureParameteriv() is not allowed after getting bindless texture handles,
// so it's the safest to ONLY call this in the ctor!
void _TextureBase::setSwizzleMask(const char * mask)
{
    const bool nullmask = !mask || !*mask;
    assert(nullmask || strlen(mask) == 4);
    if(nullmask)
        mask = "rgba";

    _dev_setSwizzleMask(mask);

    for(unsigned i = 0; i < 4; ++i)
        _swizzle[i] = mask[i];
    _swizzle[4] = 0;
}

bool _TextureBase::GLSLcompatibleWith(GLSL::Type oty) const
{
    return GLSL::areTypesCompatible(glsltype, oty);
}

glm::vec3 _TextureBase::displaysizeRatio() const
{
    const glm::vec3 d = displaysize();
    float m = d.x;
    if(d.y && m < d.y)
        m = d.y;
    if(d.z && m < d.z)
        m = d.z;
    return d / m;
}

glm::vec3 _TextureBase::displaysizeScaled() const
{
    const glm::vec3 d = displaysize();
    float m = d.x;
    unsigned i = 0;
    if(d.y && m < d.y)
    {
        m = d.y;
        i = 1;
    }
    if(d.z && m < d.z)
    {
        m = d.z;
        i = 2;
    }
    glm::vec3 r = d / m;
    r *= float(_size[i]);
    return r;
}

size_t _TextureBase::getSizeBytes(unsigned miplevel) const
{
    return SizeInBytes(_size, _datatype, _channels, miplevel);
}

void _TextureBase::fillInfo(Blob3DInfo & info) const
{
    info.w = _size0.x;
    info.h = _size0.y;
    info.d = _size0.z;
    info.spx = pixelSize.x;
    info.spy = pixelSize.y;
    info.spz = pixelSize.z;
    info.type = datatype();
    info.channels = channels();
}

static unsigned _nextPow2(unsigned n)
{
    n--;
    n |= n >> 1;
    n |= n >> 2;
    n |= n >> 4;
    n |= n >> 8;
    n |= n >> 16;
    n++;
    return n;
}

glm::uvec3 _TextureBase::CalcMipLevelSize(glm::uvec3 sz, unsigned miplevel)
{
    assert(sz.x);
    assert(miplevel <= calcMipLevel3D(sz.x, sz.y, sz.z));
    while(miplevel--)
    {
        sz /= 2;
        sz = glm::max(sz, glm::uvec3(1, 1, 1));
        sz.x = _nextPow2(sz.x);
        sz.y = _nextPow2(sz.y);
        sz.z = _nextPow2(sz.z);
    }
    return sz;
}

unsigned _TextureBase::CalcMipLevel(TextureFlags flags, glm::uvec3 sz)
{
    // That works for any dimensionality
    return (flags & TEX_MIPMAP) ? calcMipLevel3D(sz.x, sz.y, sz.z) : 1;
}

void _TextureBase::_onDeviceReset(unsigned target, bool init)
{
    _handleImg = 0;
    _handleTex = 0;
    _residency.clear();
    _id = 0;

    if(init)
    {
        _id = _New(target, _size, _datatype, _channels, _flags);
        setSwizzleMask(&_swizzle[0]);
        _queryHandles();
        _restoreBackup(target); // if there is one
    }
}

static bool _checkImageInfo(const Blob3DInfo &info)
{
    return b3d_getDimensions(&info) > 0
        && info.spx >= 0
        && info.spy >= 0
        && info.spz >= 0
        && info.type >= B3D_FIRST_VALID_TYPE
        && info.type <= B3D_LAST_VALID_TYPE
        && info.channels > 0
        && info.channels <= 4;
}

static void _fixImageInfo(Blob3DInfo& info)
{
    if(info.spx == 0)
        info.spx = 1;
    if(info.spy == 0)
        info.spy = 1;
    if(info.spz == 0)
        info.spz = 1;
}

static void _initInfo(Blob3DInfo& info)
{
    info.w = info.h = info.d = info.channels = 0;
    info.type = B3D_UNSPECIFIED;
    info.spx = info.spy = info.spz = 0.0f;
}


template<typename TEX>
static TextureAny *_newTexture(const void *mem, Blob3DType datatype, TextureFlags flags, const Blob3DInfo& info, const char *swizzle)
{
    typedef typename TexPriv::SizeType<TEX::staticType> TexSizeType;
    const typename TexSizeType::type sz = TexSizeType::get(glm::uvec3(info.w, info.h, info.d));
    if(TEX *tex = TEX::New(sz, datatype, info.channels, flags, swizzle))
    {
        tex->upload(mem, info.type, info.channels);
        tex->pixelSize = glm::vec3(info.spx, info.spy, info.spz);
        return reinterpret_cast<TextureAny*>(tex);
    }
    return NULL;
}


// ----------------------------------------------------------------------------------------------------------


#include "pluginapi.h"

static void *loadTexMem(const char *fn, Blob3DInfo& info, FreeImageDataFunc& freefunc)
{
    _initInfo(info);

    void *mem = loadBlob3D(fn, &info);
    freefunc = free;
    if(!mem)
    {
        for(size_t i = 0; i < pluginapi::funcs.size(); ++i)
        {
            const PluginAPIFuncs *api = pluginapi::funcs[i];
            if(api->loadImageFile)
            {
                if(api->loadImageFile(fn, &mem, &info) == IORESULT_OK)
                {
                    if(_checkImageInfo(info)) // Make sure the info struct is filled properly
                    {
                        freefunc = api->freeImageData;
                        assert(freefunc); // checked at plugin load time
                        break;
                    }
                    else
                    {
                        logerror("Plugin [%s] supplied image with invalid info!", pluginapi::funcs[i]->pluginfile.c_str());
                        assert(false); // If this triggers, go fix your plugin! (See _checkImageInfo())
                        _initInfo(info); // restore initial state for next plugins
                    }
                }
            }
        }
    }

    if(mem)
        _fixImageInfo(info);

    return mem;
}

void *TextureAny::LoadFileAsMem(const char *fn, Blob3DType datatype, Blob3DInfo *outInfo)
{
    Blob3DInfo info;
    FreeImageDataFunc freefunc;
    void *mem = loadTexMem(fn, info, freefunc);
    if(!mem)
        return NULL;

    if(datatype == B3D_UNSPECIFIED)
        datatype = info.type;

    if(datatype == info.type)
    {
        // Plugin uses something that's not malloc()/free()? Make a copy.
        if(freefunc != free)
        {
            size_t sz = b3d_getSizeInBytes(&info);
            void *newmem = malloc(sz);
            if(!newmem)
                return NULL;
            memcpy(newmem, mem, sz);
            freefunc(mem);
            mem = newmem;
        }
    }
    else // Convert to given datatype? Use GPU.
    {
        TexAnyRef tex = NewFromMem(mem, info, datatype, TEX_NONE, NULL);
        const size_t memsz = b3d_getSizeInBytes(&info);
        const size_t texsz = tex->getSizeBytes();
        if(memsz < texsz || freefunc != free) // If we can re-use the memory without re-allocating, do it
        {
            freefunc(mem);
            mem = malloc(texsz);
            if(!mem)
                return NULL;
        }
        tex->download(mem, texsz, 0);
    }

   if(outInfo)
        *outInfo = info;

    return mem;
}

TextureAny * TextureAny::LoadFile(const char * fn, Blob3DType datatype, TextureFlags flags, const char *swizzle, Blob3DInfo *outInfo)
{
    Blob3DInfo info;
    FreeImageDataFunc freefunc;
    void *mem = loadTexMem(fn, info, freefunc);
    if(!mem)
        return NULL;

    if(datatype == B3D_UNSPECIFIED)
        datatype = info.type;

    TextureAny *ret = NewFromMem(mem, info, datatype, flags, swizzle);
    freefunc(mem);

   if(outInfo)
        *outInfo = info;

    return ret;
}

TextureAny * TextureAny::NewFromMem(const void *mem, const Blob3DInfo& info, Blob3DType datatype, TextureFlags flags, const char *swizzle)
{
    if(!mem)
        return NULL;

    // If no swizzle mask was passed, apply best guess as to how to interpret this texture
    if(!swizzle)
    {
        switch(info.channels)
        {
            case 1: swizzle = "rrr1"; break; // Single greyscale channel
            case 2: swizzle = "rrrg"; break; // Luminance + alpha
            case 3: swizzle = "rgb1"; break; // RGB
            case 4: swizzle = "rgba"; break; // RGB + alpha
        }
    }

    TextureAny *ret = NULL;

    switch(b3d_getDimensions(&info))
    {
        case 1:
            ret = _newTexture<Texture1D>(mem, datatype, flags, info, swizzle);
            break;
        case 2:
            ret = _newTexture<Texture2D>(mem, datatype, flags, info, swizzle);
            break;
        case 3:
            ret = _newTexture<Texture3D>(mem, datatype, flags, info, swizzle);
            break;
        default:
            assert(false);
    }

    return ret;
}

IOResult _TextureBase::saveFile(const char * fn, const char * format, unsigned miplevel) const
{
    // If no format given, try to guess from the file name
    if(!format)
    {
        format = findFileExtension(fn);
        if(!format)
            logerror("No file extension given -- saving as .b3d file.");
    }

    const TextureAny *self = static_cast<const TextureAny*>(this);
    IOResult ret = IORESULT_UNSUPPORTED;

    if(format && strcmp(format, "b3d"))
    {
        for(size_t i = 0; i < pluginapi::funcs.size(); ++i)
        {
            const PluginAPIFuncs *api = pluginapi::funcs[i];
            if(api->saveImageFile)
            {
                IOResult res = api->saveImageFile(fn, format, reinterpret_cast<APITextureHandle>(self));
                switch(res)
                {
                    case IORESULT_OK:
                        return IORESULT_OK;
                    case IORESULT_UNSUPPORTED:
                        break;
                    default:
                        ret = res;
                }
            }
        }
    }

    // last resort: save as b3d
    if(ret  == IORESULT_UNSUPPORTED)
    {
        size_t sz = getSizeBytes(miplevel);
        GPUBufRef buf = GPUBufferRaw::New(NULL, sz, GPUBufferRaw::MAP_READ);
        self->downloadAsync(*buf, 0, miplevel); // Leave everything as-is
        buf->wait();
        const void *p = buf->mapRead();
        Blob3DInfo info;
        self->fillInfo(info);
        unsigned cpus = platform::getCPUCount();
        ret = saveBlob3D(fn, p, &info, -1, cpus);
    }

    return ret;
}

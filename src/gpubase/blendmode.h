#pragma once

namespace BlendMode {

enum Enum
{
    REPLACE = -1, // ignore prev. alpha, overwrite everything
    DONT_CHANGE = 0, // ignore; no change [important that this is 0 !!]
    NORMAL = 1, // normal overdraw
    ADD = 2, // additive blending

    _MAX,
};

void Push(Enum mode);
void Pop();
void Init(Enum mode);

struct Scope
{
    inline Scope(Enum m) { Push(m); }
    inline ~Scope() { Pop(); }
};

void CleanupRenderer(unsigned gid);


} // end namespace BlendMode
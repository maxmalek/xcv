#include "gpubuffer.h"
#include "texture.h"
#include "platform.h"
#include "fileio.h"

const ScriptDef GPUBufferRaw::scriptdef("gpubuf", ScriptTypes::GPUBUFFER);

void GPUBufferRaw::_ensureBackup() const
{
    if(!_backup) // size is always constant; re-use existing buffer if possible
        _backup = malloc(_bufsize);
}

bool GPUBufferRaw::deleteBackup() const
{
    if(!_backup)
        return false;

    free(_backup);
    _backup = NULL;
    return true;
}

bool GPUBufferRaw::hasSpace(size_t sz, size_t offset) const
{
    return sz + offset <= _bufsize;
}

void GPUBufferRaw::storeBackup() const
{
    _ensureBackup();
    downloadBytes(_backup, _bufsize, 0);
}

GPUBufferRaw * GPUBufferRaw::FromTexture(const TextureAny & tex, Access a)
{
    const uint64_t bytes = tex.getSizeBytes();
    GPUBufferRaw *buf = New(NULL, bytes, a);
    buf->importTexture(tex, 0);
    return buf;
}

GPUBufferRaw * GPUBufferRaw::FromFile(const char * fn, Access a)
{
    const uint64_t fsz = platform::fileSize(fn);
    GPUBufferRaw *buf;
    if(a & MAP_WRITE)
    {
        buf = New(NULL, fsz, a);
        if(!buf)
            return NULL;
        void *p = buf->mapWrite();
        uint64_t rd = 0;
        if(fileio::loadInto(fn, p, fsz, &rd) && rd == fsz)
        {
            ((char*)p)[fsz] = 0; // Make sure it's null-terminated
            buf->unmap();
        }
        else
        {
            delete buf;
            buf = NULL;
        }
    }
    else
    {
        uint64_t rd = 0;
        void *mem = fileio::loadBuf(fn, &rd); // always adds a terminating \0
        buf = New(mem, rd, a);
        free(mem);
    }
    return buf;
}

bool GPUBufferRaw::saveFile(const char *fn)
{
    FILE *f = fopen(fn, "wb");
    if(!f)
        return false;
    const uint64_t sz = bytes();
    uint64_t wd = 0;
    if(access & MAP_READ)
    {
        const void *p = mapRead();
        wd = fwrite(p, 1, sz, f);
    }
    else
    {
        if(void *p = malloc(sz))
        {
            downloadBytes(p, sz, 0);
            wd = fwrite(p, 1, sz, f);
            free(p);
        }
    }
    fclose(f);
    return wd == sz;
}

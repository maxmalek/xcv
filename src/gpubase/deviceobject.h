#pragma once

#include "glsl.h"

class DeviceObjectBase
{
protected:
    static void HandleDeviceReset(bool init);
    void performDeviceReset(bool init);
    DeviceObjectBase(unsigned priority); // objects with lower priority get reset first. Pass 0 for primitives that have no DeviceObject children
    virtual ~DeviceObjectBase();

private:
    virtual void onDeviceReset(bool init) = 0;
    size_t _devObjIdx;
    unsigned _ireset;
    const unsigned _priority;

    static inline bool sortOrder(const DeviceObjectBase *a, const DeviceObjectBase *b) { return a->_priority < b->_priority; }

    DeviceObjectBase(const DeviceObjectBase&); // forbid copying
    DeviceObjectBase& operator=(const DeviceObjectBase&); // forbid assignment
};

class DeviceObject : private DeviceObjectBase
{
public:
    static void HandleDeviceReset(bool init) { DeviceObjectBase::HandleDeviceReset(init); }
    void performDeviceReset(bool init) { DeviceObjectBase::performDeviceReset(init); }

    // Conveninence method that returns the internal GL id. Returns 0 when called on a NULL pointer.
    unsigned id() const;

protected:
    DeviceObject(unsigned id, unsigned priority); // objects with lower priority get reset first. Pass 0 for primitives that have no DeviceObject children

    unsigned _id;
};

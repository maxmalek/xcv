#include "deviceobject.h"
#include "macros.h"
#include <vector>
#include <algorithm>
#include "log.h"

typedef std::vector<DeviceObjectBase*> Storage;
static Storage s_objs, s_newObjs;
static unsigned s_nResets = 0;
static bool s_resetting = false;


void DeviceObjectBase::HandleDeviceReset(bool init)
{
    DEBUG_LOG("DeviceObject::HandleDeviceReset(init = %u): %u objects to reset...", (unsigned)init, (unsigned)s_objs.size());
    assert(s_resetting == init);
    
    if(!init)
        s_resetting = true;

    ++s_nResets;

    {
        Storage cur = s_objs;
        std::sort(cur.begin(), cur.end(), sortOrder);

        for(size_t i = 0; i < cur.size(); ++i)
            cur[i]->performDeviceReset(init);
    }

    if(init)
    {
        DEBUG_LOG("%u new device objetcs were created during reset", (unsigned)s_newObjs.size());
        for(size_t i = 0; i < s_newObjs.size(); ++i)
        {
            DeviceObjectBase *o = s_newObjs[i];
            assert(o->_devObjIdx == unsigned(-1));
            o->_devObjIdx = s_objs.size();
            s_objs.push_back(o);
        }
        s_newObjs.clear();
        s_resetting = false;
    }
    else
    {
        // Just clearing state without init should not produce new objects
        assert(s_newObjs.empty());
    }
}

DeviceObjectBase::DeviceObjectBase(unsigned priority)
: _devObjIdx(!s_resetting ? s_objs.size() : unsigned(-1)), _ireset(0), _priority(priority)
{
    if(!s_resetting)
        s_objs.push_back(this);
    else
        s_newObjs.push_back(this);
}

DeviceObjectBase::~DeviceObjectBase()
{
    assert(s_objs[_devObjIdx] == this);
    DeviceObjectBase *last = s_objs.back();
    s_objs[_devObjIdx] = last;
    last->_devObjIdx = _devObjIdx;
    s_objs.pop_back();
}

void DeviceObjectBase::performDeviceReset(bool init)
{
    if(_ireset != s_nResets)
    {
        DEBUG_LOG("Reset device object %p", this);
        _ireset = s_nResets;
        onDeviceReset(init);
    }
}

DeviceObject::DeviceObject(unsigned id, unsigned priority)
    : DeviceObjectBase(priority)
    , _id(id)
{
}



// Starting from GCC 6, value range propagation assumes that 'this' can't be NULL. Which is dumbshit.
// Doing the check here is *far* more convenient than adding NULL checks in front of every access everywhere.
// See https://gcc.gnu.org/gcc-6/changes.html
#if defined(__GNUC__) || defined (__clang__)
#pragma GCC optimize("no-delete-null-pointer-checks")
#endif
unsigned DeviceObject::id() const
{
    // HACK: 'this' isn't exactly 0 when ((DeviceObject*)NULL) is dereferenced because we're derived from DeviceObjectBase.
    // Also note: DeviceObject must be the very first class derived from, else this will blow up anyway if NULL!
    const DeviceObjectBase *base = this;
    return (uintptr_t(base) > sizeof(DeviceObjectBase)) ? _id : 0;
}

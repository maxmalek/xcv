#pragma once

#include <vector>

// Unsorted container intended for only a few entries
template<typename K, typename T>
struct PerStateHolder
{
    struct Pair { K k; T val; };

    const T& getdef(const K& k, const T& def)
    {
        T *e = getp(k);
        return e ? *e : def;
    }

    T *getp(const K& k)
    {
        for(size_t i = 0; i < _v.size(); ++i)
            if(_v[i].k == k)
                return &_v[i].val;
        return NULL;
    }

    T& operator[](const K& k)
    {
        if(T *e = getp(k))
            return *e;
        return add(k, T());
    }

    void remove(const K& k)
    {
         for(size_t i = 0; i < _v.size(); ++i)
            if(_v[i].k == k)
            {
                _v[i] = _v.back(); // pops off properly if last
                _v.pop_back();
                return;
            }
    }

    T& add(const K& k, const T& val)
    {
        Pair p = { k, val };
        _v.push_back(p);
        return _v.back().val;
    }

    void clear() { _v.clear(); }
    size_t size() const { return _v.size(); }

    std::vector<Pair> _v;
};

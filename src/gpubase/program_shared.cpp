#include "program.h"
#include <string.h>
#include <algorithm>
#include "fileio.h"
#include "log.h"
#include <assert.h>
#include "util.h"


const char* ProgramVariable::var_type_name[] = { "uniform", "buffer", "attrib", "sampler", "image" };

#define errorcb(str, ...) do { if(callbacks && callbacks->error) callbacks->error(callbacks->user, str, __VA_ARGS__); else logerror(str, __VA_ARGS__); } while(0)


static void _check()
{
    compile_assert(Countof(ProgramVariable::var_type_name) == ProgramVariable::_MAXTYPE);
    compile_assert(Countof(ShaderType::Names) == ShaderType::_MAXTYPE);
}

template <class ForwardIterator, class T, class Compare>
static ForwardIterator custom_lower_bound(ForwardIterator first, ForwardIterator last, const T& val, Compare comp)
{
    ForwardIterator it;
    typename std::iterator_traits<ForwardIterator>::difference_type count, step;
    count = std::distance(first,last);
    while(count > 0)
    {
        it = first;
        step = count/2;
        std::advance (it,step);
        if (comp(*it, val))
        {
            first= ++it;
            count -= step+1;
        }
        else
            count = step;
    }
    return first;
}

bool ProgramVariable::lessname(const ProgramVariable& a, const char *bname)
{
    return strcmp(a.name, bname) < 0;
}

bool ProgramVariable::lessnameptr(const ProgramVariable * a, const char *bname)
{
    return lessname(*a, bname);
}

bool ProgramVariable::lessptr(const ProgramVariable * a, const ProgramVariable * b)
{
    return lessname(*a, &b->name[0]);
}

bool ProgramVariable::operator<(const ProgramVariable& b) const
{
    return lessname(*this, &b.name[0]);
}

bool ProgramVariable::operator==(const char *n) const
{
    return !strcmp(name, n);
}

bool ProgramVariable::accepts(GLSL::Type ty) const
{
    const GLSL::Type myt = getType();
    switch(_vartype)
    {
        case UNIFORM:
        case ATTRIB:
            return ty == myt;
        case BUFFER:
            assert(myt == GLSL::buffer);
            return ty == GLSL::buffer;
        case SAMPLER:
            assert(GLSL::isSampler(myt));
            return ty == myt || GLSL::isSampler(ty) || GLSL::isImage(ty);
        case IMAGE:
            assert(GLSL::isImage(myt));
            return ty == myt || GLSL::isSampler(ty) || GLSL::isImage(ty);
        default: ;
    }
    assert(false);
    return false;
}

// ------------------------------------------------------------------------

void ProgramUniform::setUnsafe(const void *data) const
{
    holder = NULL;
    _setUnsafe(data);
}

void ProgramUniform::_setUnsafe(const void *data) const
{
    // copy incoming data so that getp() can return the last thing set
    memcpy(&_buf[0], data, GLSL::sizeOf(glsltype));

    uploadRaw(data);
}

uint64_t ProgramUniform::setImageHandle(TextureAny *tex, TextureAny::Access a) const
{
    assert(glsltype == GLSL::Uint64_t);
    uint64_t handle = 0;
    if(tex)
    {
        tex->makeImageResident(a);
        handle = tex->getImageHandle();
    }
    holder = tex;
    _setUnsafe(&handle);
    return handle;
}

uint64_t ProgramUniform::setSamplerHandle(TextureAny *tex) const
{
    assert(glsltype == GLSL::Uint64_t);
    uint64_t handle = 0;
    if(tex)
    {
        tex->makeTexResident();
        handle = tex->getTexHandle();
    }
    holder = tex;
    _setUnsafe(&handle);
    return handle;
}

void ProgramUniform::clearHandle() const
{
    if(!!holder)
    {
        assert(glsltype == GLSL::Uint64_t);
        holder = NULL;
        uint64_t handle = 0;
        _setUnsafe(&handle);
    }
}

// ------------------------------------------------------------------------

ShaderObject::ShaderObject(ShaderType::Enum ty)
: DeviceObject(0, 0)
, _type(ty)
, _fromfile(false)
, _counter(0)
{
}

const char *ShaderObject::getFileName() const
{
    return _fromfile ? _source.c_str() : NULL;
}

void ShaderObject::setFileName(const char * filename)
{
    _fromfile = true;
    _source = filename;
}

void ShaderObject::setCode(const char *code)
{
    _source = code;
    _fromfile = false;
}

bool ShaderObject::load(const ShaderLoadCallbacks *callbacks)
{
    bool ret;
    if(_fromfile)
    {
        char *code = (char*)fileio::loadBuf(_source.c_str(), NULL);
        if(!code)
        {
            errorcb("Shader file not found: %s", _source.c_str());
            return false;
        }
        ret =_loadCode(code, callbacks);
        free(code);
    }
    else
        ret =_loadCode(_source.c_str(), callbacks);

    return ret;
}

// ----------------------------------------------------------

ShaderProgram::ShaderProgram()
: DeviceObject(0, 1) // has other DeviceObject children
, _counter(0)
{
    _clearLinkCache();
}

void ShaderProgram::_clearLinkCache()
{
    memset(_cachedIDs, 0, sizeof(_cachedIDs));
    memset(_cachedCounters, 0, sizeof(_cachedCounters));
}

void ShaderProgram::_updateLinkCache()
{
    for(unsigned i = 0; i < ShaderType::_MAXTYPE; ++i)
    {
        unsigned id = 0;
        unsigned counter = 0;
        if(const ShaderObject *psh = _shaders[i].content())
        {
            id = psh->id();
            counter = psh->_getCounter();
        }
        _cachedCounters[i] = counter;
        _cachedIDs[i] = id;
    }
}

void ShaderProgram::attach(const ShaderObjRef& ref)
{
    _shaders[ref->getType()] = ref;
}

void ShaderProgram::detach(ShaderType::Enum ty)
{
    _shaders[ty] = NULL;
}

bool ShaderProgram::_mustRelink() const
{
    for(unsigned i = 0; i < ShaderType::_MAXTYPE; ++i)
    {
        unsigned id = 0;
        unsigned counter = 0;
        if(const ShaderObject *psh = _shaders[i].content())
        {
            id = psh->id();
            counter = psh->_getCounter();
        }
        if(_cachedIDs[i] != id || _cachedCounters[i] != counter)
            return true;
    }
    return false;
}

bool ShaderProgram::link(const ShaderLoadCallbacks *callbacks)
{
    if(!_mustRelink())
        return true;

    if(!_link(callbacks))
        return false;

    // all good, update cache
    _updateLinkCache();

    return _queryInterface();
}

template<typename T>
static void s_append(ShaderProgram::AllVarsPtrs& all, const std::vector<T>& v)
{
    for(size_t i = 0; i < v.size(); ++i)
        all.push_back(&v[i]);
}

template<typename V>
static void s_autobind(V& vars)
{
    typedef typename V::value_type Var;
    int maxbind = -1;
    for(size_t i = 0; i < vars.size(); ++i)
    {
        const Var& u = vars[i];
        const int b = u.getBinding();
        maxbind = maxbind < b ? b : maxbind;
    }
    ++maxbind;

    std::vector<char> used(maxbind, 0);
    std::vector<size_t> fix;

    for(size_t i = 0; i < vars.size(); ++i)
    {
        const Var& u = vars[i];
        const int b = u.getBinding();
        if(b >= 0)
        {
            if(used[b])
                fix.push_back(i);
            else
                used[b] = 1;
        }
    }

    // Auto-assign binding points that were not assigned explicitly
    unsigned bindloc = 0;
    for(size_t i = 0; i < fix.size(); ++i)
    {
        while(bindloc < (unsigned)used.size() && used[bindloc])
            ++bindloc;
        Var& u = vars[fix[i]];
        assert(u.getBinding() == 0); // shader compiler must reject shader that specifies same binding location more than once
        u._setBinding(bindloc);
        ++bindloc;
    }
}

bool ShaderProgram::_queryInterface()
{
    vv.allvars.clear();
    _queryUniforms();

    {
        const unsigned n = getNumBuffers();
        vv.buffers.resize(n);
        for(unsigned i = 0; i < n; ++i)
            vv.buffers[i].query(_id, i);
    }

    {
        const unsigned n = getNumAttribs();
        vv.inputs.resize(n);
        for(unsigned i = 0; i < n; ++i)
            vv.inputs[i].query(_id, i);
    }

    std::sort(vv.uniforms.begin(), vv.uniforms.end());
    std::sort(vv.buffers.begin(), vv.buffers.end());
    std::sort(vv.inputs.begin(), vv.inputs.end());

    // Now that the variables are all in their proper place in memory, get pointers and sort.
    s_append(vv.allvars, vv.uniforms);
    s_append(vv.allvars, vv.buffers);
    s_append(vv.allvars, vv.inputs);
    s_append(vv.allvars, vv.samplers);
    s_append(vv.allvars, vv.images);
    std::sort(vv.allvars.begin(), vv.allvars.end(), ProgramVariable::lessptr);

    // Auto-distribute initial binding locations if not set
    s_autobind(vv.samplers);
    s_autobind(vv.images);
    s_autobind(vv.buffers);

    logdebug("Program %u -- %u Uniforms, %u buffers, %u samplers, %u images, %u attribs:", id(),
        (unsigned)vv.uniforms.size(), (unsigned)vv.buffers.size(), (unsigned)vv.samplers.size(), (unsigned)vv.images.size(), (unsigned)vv.inputs.size());
    for(size_t i = 0; i < vv.uniforms.size(); ++i)
    {
        const ProgramUniform& u = vv.uniforms[i];
        logdebug("  u [%u, %d] %s %s", u.getLoc(), u.getBinding(), GLSL::getName(u.getType()), u.getName());
    }
    for(size_t i = 0; i < vv.buffers.size(); ++i)
    {
        const ProgramBuffer& b = vv.buffers[i];
        logdebug("  b [%d, %d] %s", b._getBlockIdx(), b.getBinding(), b.getName());
    }
    for(size_t i = 0; i < vv.samplers.size(); ++i)
    {
        const ProgramSampler& b = vv.samplers[i];
        logdebug("  s [%d] %s", b.getBinding(), b.getName());
    }
    for(size_t i = 0; i < vv.images.size(); ++i)
    {
        const ProgramImage& b = vv.images[i];
        logdebug("  i [%d] %s", b.getBinding(), b.getName());
    }
    for(size_t i = 0; i < vv.inputs.size(); ++i)
    {
        const ProgramInput& b = vv.inputs[i];
        logdebug("  a [%d] %s", b.getBinding(), b.getName());
    }

    return true;
}

template<typename T>
static const T *_lookup(const char *name, const std::vector<T>& V)
{
    // binary search
    typename std::vector<T>::const_iterator it = custom_lower_bound(V.begin(), V.end(), name, T::lessname);
    // because lower_bound returns the first element that compares less, it might not be the correct one
    if(it == V.end() || strcmp(it->getName(), name))
        return NULL;
    return &*it;
}

const ProgramUniform *ShaderProgram::lookupUniform(const char *name) const
{
    return _lookup(name, vv.uniforms);
}

const ProgramBuffer *ShaderProgram::lookupBuffer(const char *name) const
{
    return _lookup(name, vv.buffers);
}

const ProgramInput *ShaderProgram::lookupAttrib(const char *name) const
{
    return _lookup(name, vv.inputs);
}

const ProgramSampler * ShaderProgram::lookupSampler(const char *name) const
{
    return _lookup(name, vv.samplers);
}

const ProgramImage * ShaderProgram::lookupImage(const char *name) const
{
    return _lookup(name, vv.images);
}

const ProgramVariable * ShaderProgram::lookupAny(const char *name) const
{
    // binary search
    AllVarsPtrs::const_iterator it = custom_lower_bound(vv.allvars.begin(), vv.allvars.end(), name, ProgramVariable::lessnameptr);
    // because lower_bound returns the first element that compares less, it might not be the correct one
    if(it == vv.allvars.end() || strcmp((*it)->getName(), name))
        return NULL;
    return *it;
}

void ShaderProgram::clearRefs()
{
    vv.clearRefs();
}

bool ShaderProgram::setUniformUnsafe(const char *name, const void *data) const
{
    if(id())
    {
        if(const ProgramUniform *u = lookupUniform(name))
        {
            u->setUnsafe(data);
            return true;
        }
    }
    return false;
}

bool ShaderProgram::setUniform(const char * name, const void * data, GLSL::Type ty) const
{
    if(id())
    {
        if(const ProgramUniform *u = lookupUniform(name))
        {
            const bool ok = u->accepts(ty);
            assert(ok);
            if(ok)
                u->setUnsafe(data);
            return ok;
        }
    }
    return false;
}

void ShaderProgram::setUniformsUnsafe(const ShaderVar *sv, size_t n) const
{
    if(id())
        for(size_t i = 0; i < n; ++i)
            if(const ProgramUniform *u = lookupUniform(sv[i].name))
                u->setUnsafe(sv[i].data);
}

bool ShaderProgram::setBuffer(const char *name, GPUBufRef& buf) const
{
    if(id())
    {
        if(const ProgramBuffer *u = lookupBuffer(name))
        {
            u->set(buf);
            return true;
        }
        assert(false);
    }
    return false;
}

void ShaderProgram::setBuffers(const BufferVar *sh, size_t n) const
{
    if(id())
    {
        for(size_t i = 0; i < n; ++i)
            if(const ProgramBuffer *u = lookupBuffer(sh[i].name))
                u->set(sh[i].buf);
            else
                assert(false); // FIXME: want to return false instead
    }
}

bool ShaderProgram::setSampler(const char * name, const TextureAny *tex) const
{
    if(id())
        if(const ProgramSampler *samp = lookupSampler(name))
        {
            if(!tex || samp->accepts(tex->glsltype))
            {
                samp->set(tex);
                return true;
            }
            else
                logerror("ShaderProgram::setSampler(): Type mismatch [want %s, passed %s]",
                    GLSL::getName(samp->getType()), GLSL::getName(tex->glsltype));
        }
    return false;
}

bool ShaderProgram::setImage(const char * name, TextureAny *tex,  _TextureBase::Access a) const
{
    if(id())
    {
        if(const ProgramImage *img = lookupImage(name))
        {
            if(!tex || img->accepts(tex->glsltype))
            {
                img->set(tex, a);
                return true;
            }
            else
                logerror("ShaderProgram::setImage(): Type mismatch [want %s, passed %s]",
                   GLSL::getName(img->getType()),  GLSL::getName(tex->glsltype));
        }
    }
    return false;
}

void ShaderProgram::setUniforms(const SafeShaderVar * sv, size_t n) const
{
    if(id())
        for(size_t i = 0; i < n; ++i)
            if(const ProgramUniform *u = lookupUniform(sv[i].name))
            {
                const bool ok = u->accepts(sv[i].data.glsltype);
                assert(ok);
                if(ok)
                    u->setUnsafe(sv[i].data.ptr);
            }
}

// --------------------------------------------------------------

template<typename T>
static bool cancopy(const T *to, const T *from)
{
    return from && to->getType() == from->getType();
}

bool ProgramUniform::copyfrom(const ProgramUniform *from) const
{
    if(!cancopy(this, from))
        return false;

    holder = from->holder;
    _setUnsafe(from->getp());
    return true;
}

bool ProgramBuffer::copyfrom(const ProgramBuffer *from) const
{
    if(!cancopy(this, from))
        return false;

    set(from->bufref);
    return true;
}

bool ProgramImage::copyfrom(const ProgramImage *from) const
{
    if(!cancopy(this, from))
        return false;

    set(from->texref, from->_a);
    return true;
}

bool ProgramSampler::copyfrom(const ProgramSampler *from) const
{
    if(!cancopy(this, from))
        return false;

    set(from->texref);
    return true;
}

void ShaderProgram::copyVariables(const ShaderProgram& from)
{
    const ShaderProgram::Uniforms& u = getUniforms();
    const ShaderProgram::Buffers& b = getBuffers();
    //const ShaderProgram::Inputs& a = getInputs();
    const ShaderProgram::Images& im = getImages();
    const ShaderProgram::Samplers& s = getSamplers();

    for(size_t i = 0; i < u.size(); ++i)
        u[i].copyfrom(from.lookupUniform(u[i].getName()));
    for(size_t i = 0; i < b.size(); ++i)
        b[i].copyfrom(from.lookupBuffer(b[i].getName()));
    for(size_t i = 0; i < im.size(); ++i)
        im[i].copyfrom(from.lookupImage(im[i].getName()));
    for(size_t i = 0; i < s.size(); ++i)
        s[i].copyfrom(from.lookupSampler(s[i].getName()));
}

void ShaderProgram::Variables::clearRefs()
{
    for(size_t i = 0; i < uniforms.size(); ++i)
        uniforms[i].clearHandle();
    for(size_t i = 0; i < buffers.size(); ++i)
        buffers[i].set(NULL);
    for(size_t i = 0; i < images.size(); ++i)
        images[i].set(NULL, TextureAny::READONLY);
    for(size_t i = 0; i < samplers.size(); ++i)
        samplers[i].set(NULL);
}

void ShaderProgram::Variables::refresh()
{
    for(size_t i = 0; i < uniforms.size(); ++i)
        uniforms[i].refresh();
    for(size_t i = 0; i < buffers.size(); ++i)
        buffers[i].refresh();
    for(size_t i = 0; i < images.size(); ++i)
        images[i].refresh();
    for(size_t i = 0; i < samplers.size(); ++i)
        samplers[i].refresh();
}

void ShaderProgram::Variables::_setprogid(unsigned id)
{
    for(size_t i = 0; i < uniforms.size(); ++i)
        uniforms[i].program = id;
    for(size_t i = 0; i < buffers.size(); ++i)
        buffers[i].program = id;
    for(size_t i = 0; i < images.size(); ++i)
        images[i].program = id;
    for(size_t i = 0; i < samplers.size(); ++i)
        samplers[i].program = id;
}

void ShaderProgram::refreshVariables()
{
    vv.refresh();
}

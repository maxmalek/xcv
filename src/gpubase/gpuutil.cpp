#include "gpuutil.h"
#include <assert.h>
#include <string.h>
#include "texture.h"
#include "util.h"
#include <sstream>

unsigned gpuutil::getB3dElemSize(Blob3DType ty)
{
    assert(ty <= B3D_LAST_VALID_TYPE);
    return Blob3DSize[ty];
}

// Format accodring to https://www.khronos.org/registry/OpenGL/extensions/ARB/ARB_shader_image_load_store.txt, Table X.2
// Does not handle all edge cases; rather, provides a convenient method to specify the format as a string
// (The underlying GL implementation may or may not accept the channel and type combination)
bool gpuutil::parseFormat(const char * s, unsigned * pchannels, Blob3DType * pty, bool * pintegral)
{
    struct Part
    {
        char ch[4+1+1];
        char num[2+1];
        char suffix[2+1];
    } part;
    memset(&part, 0, sizeof(part));

    const char *p = s;
    int k = 0;

    while(*p && strchr("rgba", *p) && k < 4)
        part.ch[k++] = *p++;
    k = 0;
    while(*p && isdigit(*p) && k < 2)
        part.num[k++] = *p++;
    k = 0;
    while(*p && strchr("fui", *p) && k < 2)
        part.suffix[k++] = *p++;

    if(*p)
        return false; // wrong format, string must end here

    unsigned ci = 0;
    for( ; ci < 4 && part.ch[ci] && part.ch[ci] == "rgba\0"[ci]; ++ci)
        ;
    // ci is now one past end
    if(!ci || part.ch[ci])
        return false; // not prefix of "rgba" or unrecognized stuff behind it

    const unsigned channels = ci;
    const unsigned bits = atoi(part.num);
    if(!(bits == 8 || bits == 16 || bits == 32))
        return false; // wrong number of bits

    const unsigned bytes = bits / 8;

    Blob3DType ty = B3D_UNSPECIFIED;
    bool integral = !!strchr(part.suffix, 'i');
    if(!strcmp(part.suffix, "f"))
    {
        switch(bytes)
        {
            // no 8-bit float
            case 2: ty = B3D_HALF; break;
            case 4: ty = B3D_FLOAT; break;
            default: return false;
        }
    }
    else if(!strcmp(part.suffix, "i"))
    {
        switch(bytes)
        {
            case 1: ty = B3D_S8; break;
            case 2: ty = B3D_S16; break;
            case 4: ty = B3D_S32; break;
            default: return false;
        }
    }
    else if(!part.suffix[0] || !strcmp(part.suffix, "ui"))
    {
        switch(bytes)
        {
            case 1: ty = B3D_U8; break;
            case 2: ty = B3D_U16; break;
            case 4: ty = B3D_U32; break;
            default: return false;
        }
    }

    if(ty == B3D_UNSPECIFIED)
        return false;

    if(pchannels)
        *pchannels = channels;
    if(pty)
        *pty = ty;
    if(pintegral)
        *pintegral = integral;
    return true;
}

std::string gpuutil::getFormatString(unsigned c, Blob3DType ty, bool integral)
{
    assert(c && c <= 4);

    std::ostringstream os;
    for(unsigned i = 0; i < c; ++i)
        os << "rgba"[i];

    if(integral)
        switch(ty)
        {
            case B3D_U8: os << "8ui"; break;
            case B3D_U16: os << "16ui"; break;
            case B3D_U32: os << "32ui"; break;
            case B3D_S8: os << "8i"; break;
            case B3D_S16: os << "16i"; break;
            case B3D_S32: os << "32i"; break;
            default: return "";
        }
    else
        switch(ty)
        {
            case B3D_U8: os << "8"; break;
            case B3D_U16: os << "16"; break;
            case B3D_U32: os << "32"; break; // not supported by OpenGL
            case B3D_FLOAT: os << "32f"; break;
            case B3D_HALF: os << "16f"; break;
            // No signed non-integral types
            default: return "";
        }
#ifdef _DEBUG
    unsigned cc;
    Blob3DType bty;
    bool intgr;
    bool ok = parseFormat(os.str().c_str(), &cc, &bty, &intgr);
    assert(ok && c == cc && ty == bty && intgr == integral);
#endif

    return os.str();
}

Blob3DType gpuutil::fromGLSLType(GLSL::Type glsltype, unsigned bits)
{
    switch(glsltype)
    {
        case GLSL::Uint:
            switch(bits)
            {
                case 0: case 32: return B3D_U32;
                case 16: return B3D_U16;
                case 8: return B3D_U8;
                case 64: return B3D_U64;
            }
            break;

        case GLSL::Int:
            switch(bits)
            {
                case 0: case 32: return B3D_S32;
                case 16: return B3D_S16;
                case 8: return B3D_S8;
                case 64: return B3D_S64;
            }
            break;

        case GLSL::Float:
            switch(bits)
            {
                case 0: case 32: return B3D_FLOAT;
                case 16: return B3D_HALF;
                case 64: return B3D_DOUBLE;
            }
            break;
    }
    return B3D_UNSPECIFIED;
}

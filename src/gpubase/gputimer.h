#pragma once

#include <stdint.h>
#include "deviceobject.h"

class _GPUQueryObject : public DeviceObject
{
public:
    enum _noinit { noinit };
protected:
    _GPUQueryObject();
public:
    virtual ~_GPUQueryObject();
    virtual void init() = 0;
    void deinit();
    void forget();
    bool wasInit() const;
    bool completed() const; // [non-blocking] check if timer result is available

    uint64_t getValue() const; // [blocking] waits until timer result is there and returns it
    bool getValueAsync(uint64_t *ns) const 
    {
        if(completed())
        {
            *ns = getValue();
            return true;
        }
        return false;
    }
private:
    // Inherited via DeviceObject
    virtual void onDeviceReset(bool init);
};

class GPUTimer : public _GPUQueryObject
{
public:
    GPUTimer();
    GPUTimer(_noinit);
    void init();
    void capture() const;
};

class GPUTimeSpan : public GPUTimer
{
public:
    GPUTimeSpan();
    GPUTimeSpan(_noinit);
    void begin() const; // start recording
    void end() const;   // end recording
    uint64_t getDiff() const;
    bool isCapturing() const { return _capturing; }

    void deinit() { _capturing = false; GPUTimer::deinit(); }
    void forget() { _capturing = false; GPUTimer::forget(); }
    bool wasInit() const { return GPUTimer::wasInit(); }
    void init();
private:
    mutable uint64_t _starttime;
    mutable bool _capturing;
};

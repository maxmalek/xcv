#pragma once

#include "program.h"
#include "glmx.h"
#include "luaobj.h"

class ShaderProgramWrapper : public ScriptableT<ShaderProgramWrapper>
{
public:
    static const ScriptDef scriptdef;

    virtual ~ShaderProgramWrapper() {}

    void bind() const { prog->bind(); }
    void unbind() const { prog->unbind(); }

    const ProgramRef& getProgram() { return prog; }

    template<unsigned N>
    void setUniforms(const SafeShaderVar (&sv)[N]) const { prog->setUniforms(sv); }
    template<typename T>
    inline bool setUniform(const char *name, const T& v) const { return prog->setUniform(name, v); }

    template<unsigned N>
    void setBuffers(const BufferVar (&sv)[N]) const { prog->setBuffers(sv); }

    bool setSampler(const char *name, const TextureAny * tex) const { return prog->setSampler(name, tex); }
    bool setImage(const char *name, TextureAny *tex, _TextureBase::Access a) const { return prog->setImage(name, tex, a); }

    void clearRefs() const { prog->clearRefs(); }

    void copyVariables(const ShaderProgramWrapper& from) const;

protected:
    ShaderProgramWrapper(const ScriptDef& def);

    std::string basename;
    ProgramRef prog;
};

class ComputeShader : public ShaderProgramWrapper, private ScriptType<ComputeShader>
{
public:
    static const ScriptDef scriptdef;
    static const char * const MultiDispatchUniformName;

    ComputeShader();
    ComputeShader(const char *name);
    // omit file extension, is automatically added as ".comp"
    void setFile(const char *name);
    void setSource(const char *src);
    const char *getFileName() const;
    const char *getInternalSource() const;
    virtual ~ComputeShader() {}

    bool load(const ShaderLoadCallbacks *callbacks = NULL, void *udcb = NULL);
    void dispatchComputeSingle(unsigned x, unsigned y = 1, unsigned z = 1) const { dispatchComputeSingle(glm::uvec3(x,y,z)); }
    void dispatchComputeSingle(glm::uvec3 g) const;
    glm::uvec3 getLocalSize() const { return localSize; }
    glm::uvec3 getGroupsForSize(glm::uvec3 size) const; // how many groups will be launched
    const ShaderProgram::Uniforms& getUniforms() const { return prog->getUniforms(); }
    const ShaderProgram::Buffers& getBuffers() const { return prog->getBuffers(); }

    bool dispatchComputeMulti(glm::uvec3 g, glm::uvec3 n) const;

    // Assuming each thread works on one output element,
    // launch enough blocks to work on the given input size.
    void dispatchComputeForSizeSingle(glm::uvec3 size) const;
    void dispatchComputeForSizeSingle(unsigned x, unsigned y = 1, unsigned z = 1) const { dispatchComputeForSizeSingle(glm::uvec3(x,y,z)); }

    // Same as dispatchForSize(), but does multiple dispatches if size exceeds maxsize
    bool dispatchComputeForSizeMulti(glm::uvec3 size, glm::uvec3 maxsize) const;

private:
    ShaderObjRef comp;
    glm::uvec3 localSize;
};

typedef CountedPtr<ComputeShader> ComputeShaderRef;


class SimpleShader : public ShaderProgramWrapper, private ScriptType<SimpleShader>
{
public:
    static const ScriptDef scriptdef;

    SimpleShader();
    SimpleShader(const char *name);
    virtual ~SimpleShader() {}

    void setSource(const char *vert, const char *frag);
    // omit file extension, is automatically added as ".vert" and ".frag"
    void setFile(const char *name);
    bool load(const ShaderLoadCallbacks *callbacks = NULL, void *udcb = NULL);
    const char *getInternalVertexSource() const;
    const char *getInternalFragmentSource() const;

private:
    ShaderObjRef vertex, fragment;
};

typedef CountedPtr<SimpleShader> SimpleShaderRef;

template<typename T>
class ShaderBind
{
public:
    ShaderBind(const T& p) : _sh(p) { p->bind(); }
    ~ShaderBind() { _sh->unbind(); }
    inline const T& bound() const { return _sh; }

private:
    T _sh;
    typedef typename T::is_counted_ptr _check; // compile-time check

};

typedef ShaderBind<SimpleShaderRef> SimpleShaderBind;


#include "luainternal.h"
#include "glsl.h"

GLSL::Type lua_checkglsl(lua_State *L, int idx)
{
    unsigned ty = lua_checkint(L, idx);
    if(GLSL::isValid(ty))
        return GLSL::Type(ty);

    luaL_error(L, "Invalid GLSL type: %d", ty);
    return GLSL::unknown; // not reached
}

#define gg(i) lua_checkglsl(L, i)

// return name or nil
luaFunc(nameof)
{
    // Don't use lua_getglsl() here. Return nil if invalid instead.
    luaReturnStr(GLSL::getName((GLSL::Type)luaL_checkinteger(L, 1)));
}

luaFunc(sizeof)
{
    luaReturnInt(GLSL::sizeOf(gg(1)));
}

luaFunc(compatible)
{
    luaReturnBool(GLSL::areTypesCompatible(gg(1), gg(2)));
}

luaFunc(isSampler)
{
    luaReturnBool(GLSL::isSampler(gg(1)));
}

luaFunc(isImage)
{
    luaReturnBool(GLSL::isImage(gg(1)));
}

luaFunc(scalarComponents)
{
    luaReturnInt(GLSL::scalarComponents(gg(1)));
}

static const luaL_Reg glsllib[] =
{
    luaRegister(nameof),
    luaRegister(sizeof),
    luaRegister(compatible),
    luaRegister(isSampler),
    luaRegister(isImage),
    luaRegister(scalarComponents),
    {NULL, NULL}
};

static const LuaEnum glsllibenum[] =
{
#define PRIMTYPE(cname, glslname) { #cname, GLSL::glslname },
#define GLSLTYPE(name) luaRegisterClassEnum(GLSL, name),
#define GLSLSAMPLER(name) luaRegisterClassEnum(GLSL, name),
#define GLSLIMAGE(name) luaRegisterClassEnum(GLSL, name),
#define GLSLFAKETYPE(name) luaRegisterClassEnum(GLSL, name),
#include "glsl_gen.h"
    { NULL, 0 },
};


int register_lua_glsl(lua_State *L)
{
    LuaStackCheck(L, 1);

    luaL_newlib(L, glsllib);
    luaRegisterEnumInLib(L, glsllibenum, -1);
    return 1;
}

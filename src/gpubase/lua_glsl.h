#pragma once
#include "glsl.h"
struct lua_State;
int register_lua_glsl(lua_State *L);
GLSL::Type lua_checkglsl(lua_State *L, int idx);

#include "mesh.h"
#include "glapi.h"
#include "util.h"
#include <limits>
#include "macros.h"
#include "renderer.h"
#include "log.h"


const ScriptDef Mesh::scriptdef("mesh", ScriptTypes::MESH);


// glMultiDrawArraysIndirect() buffer as described in the GL spec 
struct DrawArraysIndirectCommand
{
    unsigned  count;
    unsigned  instanceCount;
    unsigned  first;
    unsigned  baseInstance;
};

struct DrawElementsIndirectCommand
{
    unsigned  count;
    unsigned  instanceCount;
    unsigned  firstIndex;
    unsigned  baseVertex;
    unsigned  baseInstance;
};

static const unsigned s_vertexRenderEnumToGL[] =
{
    GL_TRIANGLES,
    GL_LINES,
    GL_POINTS,
    GL_LINE_STRIP
};

static void _check()
{
    compile_assert(Countof(s_vertexRenderEnumToGL) == VERTEX_RENDER_MAX);
}


template<bool intData = false>
static void vaobind(unsigned vao, unsigned vbo, KnownAttribs attr, unsigned elems, GLenum type, unsigned stride, size_t offset)
{
    glVertexArrayVertexBuffer(vao, attr, vbo, offset, stride);
    if(intData)
        glVertexArrayAttribIFormat(vao, attr, elems, type, 0);
    else
        glVertexArrayAttribFormat(vao, attr, elems, type, GL_FALSE, 0);
    glEnableVertexArrayAttrib(vao, attr);
}

template<typename C, bool intData = false>
static void autobind(unsigned vao, unsigned vbo, KnownAttribs attr, GLenum type, size_t offset)
{
    typedef typename C::value_type ELEM;
    const unsigned sz = sizeof(ELEM);
    const unsigned c = ELEM::components;
    vaobind(vao, vbo, attr, c, type, sz, offset);
}

static unsigned bytesPerVert(size_t n)
{
    // Hardware doesn't support 8-bit index size natively,
    // so we use either u16 or u32.
    return (n <= std::numeric_limits<unsigned short>::max())
        ? sizeof(unsigned short)
        : sizeof(unsigned int);
}

// -------------------------------------------------------------------------


Mesh::Mesh()
: DeviceObjectBase(1)
{
    clear();
}

Mesh::~Mesh()
{
    clear();
}

void Mesh::load(const MeshData& m)
{
    clear();

    // number of elements
    n.vert    = m.v.size();
    n.col     = m.colors.size();
    n.norm    = m.vn.size();
    n.uv      = m.uv.size();
    n.useridx = m.userIdx.size();
    n.idx     = m.f.size();
    n.multi   = m.multi.size();

    const unsigned idxSz = bytesPerVert(n.vert);

    // byte sizes
    b.vert    = ByteSize(m.v);
    b.col     = ByteSize(m.colors);
    b.norm    = ByteSize(m.vn);
    b.uv      = ByteSize(m.uv);
    b.useridx = ByteSize(m.userIdx);
    b.idx     = m.f.size() * idxSz; // may shrink indices from 32 to 16 bits if number of vertices is < 0xffff
    b.multi   = m.multi.size() * (n.idx ? sizeof(DrawElementsIndirectCommand) : sizeof(DrawArraysIndirectCommand));

    // starting byte offsets in buffer
    o.vert    = 0;
    o.col     = o.vert    + b.vert;
    o.norm    = o.col     + b.col;
    o.uv      = o.norm    + b.norm;
    o.useridx = o.uv      + b.uv;
    o.idx     = o.useridx + b.useridx;
    o.multi   = o.idx     + b.idx;
    const size_t total = o.multi + b.multi;

    // buffer data layout is same as in struct Offs in mesh.h
    vbo = GPUBufferRaw::New(NULL, total, GPUBufferRaw::MAP_WRITE);
    const unsigned bufid = vbo->id();

    char * const p = (char*)vbo->mapWrite();
    
    if(b.vert)
        memcpy(p + o.vert, &m.v[0], b.vert);
    if(b.col)
        memcpy(p + o.col, &m.colors[0], b.col);
    if(b.norm)
        memcpy(p + o.norm, &m.vn[0], b.norm);
    if(b.uv)
        memcpy(p + o.uv, &m.uv[0], b.uv);
    if(b.useridx)
        memcpy(p + o.useridx, &m.userIdx[0], b.useridx);
    if(b.idx)
    {
        if(idxSz == sizeof(MeshData::Indices::value_type)) // copy u32 verbatim?
        {
            memcpy(p + o.idx, &m.f[0], b.idx);
            iboType = GL_UNSIGNED_INT;
        }
        else // shrink type to u16
        {
            unsigned short *w = (unsigned short*)(p + o.idx);
            for(size_t i = 0; i < m.f.size(); ++i)
                *w++ = static_cast<unsigned short>(m.f[i]);
            iboType = GL_UNSIGNED_SHORT;
        }
    }
    if(n.multi)
    {
        if(n.idx)
        {
            DrawElementsIndirectCommand *w = (DrawElementsIndirectCommand*)(p + o.multi);
            for(size_t i = 0; i < n.multi; ++i, ++w)
            {
                w->count = m.multi[i].count;
                w->instanceCount = 1;
                w->firstIndex = m.multi[i].start;
                w->baseVertex = 0;
                w->baseInstance = 0;
            }
        }
        else
        {
            DrawArraysIndirectCommand *w = (DrawArraysIndirectCommand*)(p + o.multi);
            for(size_t i = 0; i < n.multi; ++i, ++w)
            {
                w->count = m.multi[i].count;
                w->instanceCount = 1;
                w->first = m.multi[i].start;
                w->baseInstance = 0;
            }
        }
    }

    vbo->unmap();
    vbo->storeBackup(); // LAZY: this is ugly and lame as it does a GPU round-trip
}

void Mesh::clear()
{
    for(unsigned i = 0; i < vaos.size(); ++i)
        glDeleteVertexArrays(1, &vaos._v[i].val);
    vaos.clear();
    memset(&n, 0, sizeof(n));
    memset(&o, 0, sizeof(o));
    memset(&b, 0, sizeof(b));
    vbo = NULL;
}

void Mesh::render(CullMethod cull, DepthTest dtest, VertexRenderMode vr, DepthWrite dw) const
{
    // As per the openGL spec, VAOs can't be shared between different GL contexts.
    // Therefore delay VAO creation until they are actually needed.
    const unsigned vao = _getVAO();

    switch(cull)
    {
        case CULL_NONE:
            glDisable(GL_CULL_FACE);
            break;

        case CULL_FRONT:
            glEnable(GL_CULL_FACE);
            glCullFace(GL_FRONT);
            break;

        case CULL_BACK:
            glEnable(GL_CULL_FACE);
            glCullFace(GL_BACK);
            break;
    }

    if(dtest)
        glDepthFunc(GL_LESS);
    else
        glDepthFunc(GL_ALWAYS);

    glDepthMask(!!dw);

    glBindVertexArray(vao);

    const unsigned primMode = s_vertexRenderEnumToGL[vr];
    const unsigned bufid = vbo->id();

    if(n.multi)
    {
        glBindBufferRange(GL_DRAW_INDIRECT_BUFFER, 0, bufid, o.multi, b.multi);
        if(n.idx)
        {
            glBindBufferRange(GL_ELEMENT_ARRAY_BUFFER, 0, bufid, o.idx, b.idx);
            glMultiDrawElementsIndirect(primMode, iboType, NULL, (unsigned)n.multi, 0);
            glBindBufferRange(GL_ELEMENT_ARRAY_BUFFER, 0, 0, 0, 0);
        }
        else
        {
            glMultiDrawArraysIndirect(primMode, NULL, (unsigned)n.multi, 0);
        }
        glBindBufferRange(GL_DRAW_INDIRECT_BUFFER, 0, 0, 0, 0);
    }
    else
    {
        if (n.idx)
        {
            glBindBufferRange(GL_ELEMENT_ARRAY_BUFFER, 0, bufid, o.idx, b.idx);
            glDrawElements(primMode, (unsigned)n.idx, iboType, NULL);
            glBindBufferRange(GL_ELEMENT_ARRAY_BUFFER, 0, 0, 0, 0);
        }
        else
        {
            glDrawArrays(primMode, 0, (unsigned)n.vert);
        }
    }

    glBindVertexArray(0);
}

unsigned Mesh::_getVAO() const
{
    const unsigned gid = Renderer::GetCurrentGID();
    unsigned vao = vaos.getdef(gid, 0);
    if(vao)
        return vao;

    vao = _createVAO();
    vaos.add(gid, vao);
    return vao;
}

unsigned Mesh::_createVAO() const
{
    unsigned vao = 0;
    glCreateVertexArrays(1, &vao);

    const unsigned bufid = vbo->id();
    DEBUG_LOG("Create VAO %u for Mesh %p (VBO %u)", vao, this, bufid);
    
    if(b.vert)
        autobind<MeshData::Vertices>(vao, bufid, KA_VERTEX, GL_FLOAT, o.vert);
    if(b.col)
        autobind<MeshData::Vertices>(vao, bufid, KA_VERTEXCOLOR, GL_FLOAT, o.col);
    if(b.norm)
        autobind<MeshData::Normals>(vao, bufid, KA_VERTEXNORMAL, GL_FLOAT, o.norm);
    if(b.uv)
        autobind<MeshData::TexCoords>(vao, bufid, KA_VERTEXUV, GL_FLOAT, o.uv);
    if(b.useridx)
    {
        const unsigned sz = sizeof(MeshData::UserIndices::value_type);
        vaobind<true>(vao, bufid, KA_USERIDX, 1, GL_INT, sz, o.useridx);
    }

    return vao;
}

void Mesh::onDeviceReset(bool init)
{
    if(!!vbo)
        vbo->performDeviceReset(init);
    vaos.clear();
}

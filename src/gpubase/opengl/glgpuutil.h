#include "gpuutil.h"

// OpenGL-specific utility

namespace glgpuutil {

Blob3DType getB3DType(unsigned glty);
unsigned getGLFormatForChannels(unsigned channels);
unsigned getNumChannels(unsigned glfmt);
unsigned getGLInternalFormat(unsigned c, Blob3DType ty, unsigned texflags); // Can also pass any of TextureChannelSpecial (See texutre.h) as channel
unsigned getGLType(Blob3DType ty);

}

#include "glgpuutil.h"
#include "glapi.h"
#include <assert.h>
#include "texture.h"
#include "macros.h"
#include "util.h"


// ----- For gpuutil.h ------

void gpuutil::waitFinish()
{
    glFinish();
}

void gpuutil::barrier()
{
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
}



// ------ GL backend specific ----------

// Indexed by (#channels - 1)
static const unsigned gl_u8[] = { GL_R8, GL_RG8, GL_RGB8, GL_RGBA8 };
static const unsigned gl_u16[] = { GL_R16, GL_RG16, GL_RGB16, GL_RGBA16 };
//static const unsigned gl_u16[] = { GL_R32, GL_RG32, GL_RGB32, GL_RGBA32 }; // no such thing
static const unsigned gl_float[] = { GL_R32F, GL_RG32F, GL_RGB32F, GL_RGBA32F };
static const unsigned gl_half[] = { GL_R16F, GL_RG16F, GL_RGB16F, GL_RGBA16F };

static const unsigned gl_integral_i8[] = { GL_R8I, GL_RG8I, GL_RGB8I, GL_RGBA8I };
static const unsigned gl_integral_i16[] = { GL_R16I, GL_RG16I, GL_RGB16I, GL_RGBA16I };
static const unsigned gl_integral_i32[] = { GL_R32I, GL_RG32I, GL_RGB32I, GL_RGBA32I };
static const unsigned gl_integral_u8[] = { GL_R8UI, GL_RG8UI, GL_RGB8UI, GL_RGBA8UI };
static const unsigned gl_integral_u16[] = { GL_R16UI, GL_RG16UI, GL_RGB16UI, GL_RGBA16UI };
static const unsigned gl_integral_u32[] = { GL_R32UI, GL_RG32UI, GL_RGB32UI, GL_RGBA32UI };

static const unsigned gl_format[] = { GL_RED, GL_RG, GL_RGB, GL_RGBA };

// Indexed by Blob3DType
static const unsigned gl_type[] =
{
    GL_UNSIGNED_BYTE,      // B3D_U8
    GL_UNSIGNED_SHORT,     // B3D_U16
    GL_UNSIGNED_INT,       // B3D_U32
    GL_BYTE,               // B3D_S8
    GL_SHORT,              // B3D_S16
    GL_INT,                // B3D_S32
    GL_FLOAT,              // B3D_FLOAT
    GL_HALF_FLOAT,         // B3D_HALF
    GL_INT64_ARB,          // B3D_S64
    GL_UNSIGNED_INT64_ARB, // B3D_U64
    GL_DOUBLE              // B3D_DOUBLE
};

unsigned glgpuutil::getGLType(Blob3DType ty)
{
    compile_assert(Countof(gl_type) == B3D_LAST_VALID_TYPE+1);
    assert(ty >= B3D_FIRST_VALID_TYPE);
    assert(ty <= B3D_LAST_VALID_TYPE);
    return gl_type[ty];
}

Blob3DType glgpuutil::getB3DType(unsigned glty)
{
    switch (glty)
    {
    case GL_UNSIGNED_BYTE:
    case GL_R8: case GL_RG8: case GL_RGB8: case GL_RGBA8:
    case GL_R8UI: case GL_RG8UI: case GL_RGB8UI: case GL_RGBA8UI:
        return B3D_U8;
    case GL_BYTE:
    case GL_R8I: case GL_RG8I: case GL_RGB8I: case GL_RGBA8I:
        return B3D_S8;
    case GL_UNSIGNED_SHORT:
    case GL_R16: case GL_RG16: case GL_RGB16: case GL_RGBA16:
    case GL_R16UI: case GL_RG16UI: case GL_RGB16UI: case GL_RGBA16UI:
        return B3D_U16;
    case GL_SHORT:
    case GL_R16I: case GL_RG16I: case GL_RGB16I: case GL_RGBA16I:
        return B3D_S16;
    case GL_FLOAT:
    case GL_R32F: case GL_RG32F: case GL_RGB32F: case GL_RGBA32F:
        return B3D_FLOAT;
    case GL_HALF_FLOAT:
    case GL_R16F: case GL_RG16F: case GL_RGB16F: case GL_RGBA16F:
        return B3D_HALF;
    case GL_INT:
    case GL_R32I: case GL_RG32I: case GL_RGB32I: case GL_RGBA32I:
        return B3D_S32;
    case GL_UNSIGNED_INT:
    case GL_R32UI: case GL_RG32UI: case GL_RGB32UI: case GL_RGBA32UI:
        return B3D_U32;
    }
    assert(false);
    return Blob3DType(-1);
}

unsigned glgpuutil::getGLFormatForChannels(unsigned channels)
{
    assert(channels);
    return gl_format[channels - 1];
}

unsigned glgpuutil::getNumChannels(unsigned glfmt)
{
    switch(glfmt)
    {
    case GL_RED: case GL_GREEN: case GL_BLUE: case GL_R8: case GL_R16: case GL_R32F: case GL_R16F: // normalized
    case GL_R8I: case GL_R8UI: case GL_R16I: case GL_R16UI: case GL_R32I: case GL_R32UI: // integral
        return 1;
    case GL_RG: case GL_RG8: case GL_RG16: case GL_RG32F: case GL_RG16F: // normalized
    case GL_RG8I: case GL_RG8UI: case GL_RG16I: case GL_RG16UI: case GL_RG32I: case GL_RG32UI: // integral
        return 2;
    case GL_RGB: case GL_RGB8: case GL_RGB16: case GL_RGB32F: case GL_RGB16F: // normalized
    case GL_RGB8I: case GL_RGB8UI: case GL_RGB16I: case GL_RGB16UI: case GL_RGB32I: case GL_RGB32UI: // integral
        return 3;
    case GL_RGBA: case GL_RGBA8: case GL_RGBA16: case GL_RGBA32F: case GL_RGBA16F: // normalized
    case GL_RGBA8I: case GL_RGBA8UI: case GL_RGBA16I: case GL_RGBA16UI: case GL_RGBA32I: case GL_RGBA32UI: // integral
        return 4;
    }
    assert(false);
    return 0;
}

unsigned glgpuutil::getGLInternalFormat(unsigned c, Blob3DType ty, unsigned texflags)
{
    if(c == TEXCH_IS_DEPTH)
    {
        switch(ty)
        {
            case B3D_UNSPECIFIED: return GL_DEPTH_COMPONENT;
            case B3D_U16:   return GL_DEPTH_COMPONENT16;
            //case B3D_U32:   return GL_DEPTH_COMPONENT32;
            case B3D_FLOAT: return GL_DEPTH_COMPONENT32F;
            default:
                assert(false);
                return -1;
        }
    }
    // empty?
    if(!c || ty == B3D_UNSPECIFIED)
        return 0;

     --c;
    if(c < 4u)
    {
        if(texflags & TEX_INTEGRAL)
            switch (ty)
            {
                case B3D_U8: return gl_integral_u8[c];
                case B3D_U16: return gl_integral_u16[c];
                case B3D_U32: return gl_integral_u32[c];
                case B3D_S8: return gl_integral_i8[c];
                case B3D_S16: return gl_integral_i16[c];
                case B3D_S32: return gl_integral_i32[c];
                default: // float or large types not supported
                    assert(false);
            }
        else
            switch (ty)
            {
                case B3D_U8: return gl_u8[c];
                case B3D_U16: return gl_u16[c];
                //case B3D_U32: return gl_u32[c]; // no such thing
                case B3D_FLOAT: return gl_float[c];
                case B3D_HALF: return gl_half[c];
            }
    }

    return 0;
}

#include "glapi.h"
#include <imgui/imgui.h>
#include <map>
#include <string.h>

#include "log.h"
#include "macros.h"
#include "util.h"
#include "texture.h"

static void *       g_currentGLContext;
static int          g_Prog = 0;
static unsigned     s_VboHandle = 0, s_ElementsHandle = 0;
Tex2DRef            s_imgui_FontTexture = 0;
static void *       s_vboPtr = 0;
static void *       s_elemPtr = 0;

static unsigned     g_UniformLocationTex = 0, g_UniformLocationProjMtx = 0;
static unsigned     g_AttribLocationPosition = 0, g_AttribLocationUV = 0, g_AttribLocationColor = 0;

#define TEX_UNIT 0

typedef std::vector<CountedCPtrAny> RefCache;

// VAOs can't be shared across different GL contexts, so we use this kludge to get around it
struct ContextVars
{
    ContextVars()
    {
        const unsigned vbo = s_VboHandle;
        unsigned vao = 0;
        glCreateVertexArrays(1, &vao);
        assert(vao);
#define OFFSETOF(TYPE, ELEMENT) ((size_t)&(((TYPE *)0)->ELEMENT))
        glVertexArrayVertexBuffer(vao, g_AttribLocationPosition, vbo, OFFSETOF(ImDrawVert, pos), sizeof(ImDrawVert));
        glVertexArrayVertexBuffer(vao, g_AttribLocationUV, vbo, OFFSETOF(ImDrawVert, uv ), sizeof(ImDrawVert));
        glVertexArrayVertexBuffer(vao, g_AttribLocationColor, vbo, OFFSETOF(ImDrawVert, col), sizeof(ImDrawVert));
        glVertexArrayAttribFormat(vao, g_AttribLocationPosition, 2, GL_FLOAT, GL_FALSE, 0);
        glVertexArrayAttribFormat(vao, g_AttribLocationUV, 2, GL_FLOAT, GL_FALSE, 0);
        glVertexArrayAttribFormat(vao, g_AttribLocationColor, 4, GL_UNSIGNED_BYTE, GL_TRUE, 0);
#undef OFFSETOF
        glEnableVertexArrayAttrib(vao, g_AttribLocationPosition);
        glEnableVertexArrayAttrib(vao, g_AttribLocationUV);
        glEnableVertexArrayAttrib(vao, g_AttribLocationColor);
        vaoHandle = vao;
    }

    ~ContextVars()
    {
        if(vaoHandle)
            glDeleteVertexArrays(1, &vaoHandle);
    }
    unsigned vaoHandle;
    RefCache refcache; // textures in flight, cleared after every frame
};

typedef std::map<ImGuiContext*, ContextVars> ContextMap;
static ContextMap s_imgui_ctxMap;


static unsigned getVAO()
{
    return s_imgui_ctxMap[ImGui::GetCurrentContext()].vaoHandle;
}

static RefCache& getRefCache()
{
    return s_imgui_ctxMap[ImGui::GetCurrentContext()].refcache;
}

void  ImGui_Impl_CacheReference(const Refcounted<REFCOUNT_NORMAL> *obj)
{
    getRefCache().push_back(obj);
}

enum
{
    cIndexType = sizeof(ImDrawIdx) == sizeof(unsigned short) ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT
};

// This is the main rendering function that you have to implement and provide to ImGui (via setting up 'RenderDrawListsFn' in the ImGuiIO structure)
// If text or lines are blurry when integrating ImGui in your engine:
// - in your Render function, try translating your projection matrix by (0.5f,0.5f) or (0.375f,0.375f)
void ImGui_Impl_GL45_RenderDrawLists(ImDrawData* draw_data)
{
    // Setup render state: alpha-blending enabled, no face culling, no depth testing, scissor enabled
    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_SCISSOR_TEST);

    // Handle cases of screen coordinates != from framebuffer coordinates (e.g. retina displays)
    ImGuiIO& io = ImGui::GetIO();
    float fb_height = io.DisplaySize.y * io.DisplayFramebufferScale.y;
    draw_data->ScaleClipRects(io.DisplayFramebufferScale);

    // Setup viewport, orthographic projection matrix
    glViewport(0, 0, (GLsizei)io.DisplaySize.x, (GLsizei)io.DisplaySize.y);
    const float ortho_projection[4][4] =
    {
        { 2.0f/io.DisplaySize.x, 0.0f,                   0.0f, 0.0f },
        { 0.0f,                  2.0f/-io.DisplaySize.y, 0.0f, 0.0f },
        { 0.0f,                  0.0f,                  -1.0f, 0.0f },
        {-1.0f,                  1.0f,                   0.0f, 1.0f },
    };
    glUseProgram(g_Prog);
    glUniformMatrix4fv(g_UniformLocationProjMtx, 1, GL_FALSE, &ortho_projection[0][0]);

    const unsigned vao = getVAO();
    assert(vao);
    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, s_VboHandle);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, s_ElementsHandle);

    TextureAny *lastTex = NULL;

    for (int n = 0; n < draw_data->CmdListsCount; n++)
    {
        const ImDrawList* cmd_list = draw_data->CmdLists[n];
        const ImDrawIdx* idx_buffer_offset = 0;

        glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)cmd_list->VtxBuffer.size() * sizeof(ImDrawVert), (GLvoid*)&cmd_list->VtxBuffer.front(), GL_STREAM_DRAW);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizeiptr)cmd_list->IdxBuffer.size() * sizeof(ImDrawIdx), (GLvoid*)&cmd_list->IdxBuffer.front(), GL_STREAM_DRAW);

        for (const ImDrawCmd* pcmd = cmd_list->CmdBuffer.begin(); pcmd != cmd_list->CmdBuffer.end(); pcmd++)
        {
            // Assuming the texture was either added via ImGui::ToTexID() [see guibase.h] or is s_imgui_FontTexture
            TextureAny *tex = (TextureAny*)pcmd->TextureId;
            if(tex != lastTex)
            {
                lastTex = tex;
                glBindTextureUnit(TEX_UNIT, tex->id());
            }

            if (pcmd->UserCallback)
            {
                pcmd->UserCallback(cmd_list, pcmd);
            }
            else
            {
                glScissor((int)pcmd->ClipRect.x, (int)(fb_height - pcmd->ClipRect.w), (int)(pcmd->ClipRect.z - pcmd->ClipRect.x), (int)(pcmd->ClipRect.w - pcmd->ClipRect.y));
                glDrawElements(GL_TRIANGLES, (GLsizei)pcmd->ElemCount, cIndexType, idx_buffer_offset);
            }
            idx_buffer_offset += pcmd->ElemCount;
        }
    }
    glBindTextureUnit(TEX_UNIT, 0);
    glInvalidateBufferData(s_VboHandle);
    glInvalidateBufferData(s_ElementsHandle);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glUseProgram(0);

    glDisable(GL_SCISSOR_TEST);

    getRefCache().clear();
}

void ImGui_Impl_GL45_CreateFontsTexture()
{
    ImGuiIO& io = ImGui::GetIO();

    // Build texture atlas
    unsigned char* pixels;
    int width, height;

    io.Fonts->GetTexDataAsAlpha8(&pixels, &width, &height);
    
    Texture2D *tex = Texture2D::New(glm::uvec2(width, height), B3D_U8, 1, TEX_KEEP_BACKUP, "111r");
    tex->upload(pixels, B3D_U8, 1);

    io.Fonts->TexID = (ImTextureID)tex;

    s_imgui_FontTexture = tex;

    // Cleanup (don't clear the input data if you want to append new fonts later)
    io.Fonts->ClearInputData();
    io.Fonts->ClearTexData();
}

static const GLchar *vertex_shader =
    "#version 330\n"
    "uniform mat4 ProjMtx;\n"
    "in vec2 Position;\n"
    "in vec2 UV;\n"
    "in vec4 Color;\n"
    "out vec2 Frag_UV;\n"
    "out vec4 Frag_Color;\n"
    "void main()\n"
    "{\n"
    "	Frag_UV = UV;\n"
    "	Frag_Color = Color;\n"
    "	gl_Position = ProjMtx * vec4(Position.xy, 0.0, 1.0);\n"
    "}\n";

static const GLchar* fragment_shader =
    "#version 330\n"
    "uniform sampler2D Tex;\n"
    "in vec2 Frag_UV;\n"
    "in vec4 Frag_Color;\n"
    "out vec4 Out_Color;\n"
    "void main()\n"
    "{\n"
    "	Out_Color = Frag_Color * texture(Tex, Frag_UV.st);\n"
    "}\n";


bool ImGui_Impl_GL45_CreateDeviceObjects()
{
    if(s_imgui_FontTexture)
        return true;

    logdebug2("ImGui_Impl_GL45_CreateDeviceObjects()");
    {
        const unsigned vs = glCreateShader(GL_VERTEX_SHADER);
        const unsigned fs = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(vs, 1, &vertex_shader, 0);
        glShaderSource(fs, 1, &fragment_shader, 0);
        glCompileShader(vs);
        glCompileShader(fs);

        char info[2048];
        int ok = 0;
        // -- vertex shader --
        info[0] = 0;
        glGetShaderInfoLog(vs, sizeof(info), NULL, info);
        if(info[0])
            logerror("ImGui vertex shader log:\n%s", info);
        glGetShaderiv(vs, GL_COMPILE_STATUS, &ok);
        RELEASE_ASSERT(ok, "vertex shader failed");
        // -- frament shader --
        info[0] = 0;
         glGetShaderInfoLog(fs, sizeof(info), NULL, info);
        if(info[0])
            logerror("ImGui fragment shader log:\n%s", info);
        glGetShaderiv(fs, GL_COMPILE_STATUS, &ok);
        RELEASE_ASSERT(ok, "fragment shader failed");

        // -- attach --
        const unsigned proc = glCreateProgram();
        glAttachShader(proc, vs);
        glAttachShader(proc, fs);

        // -- link --
        glLinkProgram(proc);

        info[0] = 0;
        glGetProgramInfoLog(proc, sizeof(info), NULL, info);
        if(info[0])
            logerror("ImGui program link log:\n%s", info);
        glGetProgramiv(proc, GL_LINK_STATUS, &ok);
        RELEASE_ASSERT(ok, "link failed");

        glDeleteShader(vs);
        glDeleteShader(fs);
        g_Prog = proc;

        // -- query uniforms --
        g_UniformLocationTex = glGetUniformLocation(proc, "Tex");
        g_UniformLocationProjMtx = glGetUniformLocation(proc, "ProjMtx");
        g_AttribLocationPosition = glGetAttribLocation(proc, "Position");
        g_AttribLocationUV = glGetAttribLocation(proc, "UV");
        g_AttribLocationColor = glGetAttribLocation(proc, "Color");

        glProgramUniform1i(proc, g_UniformLocationTex, TEX_UNIT);
    }

    glCreateBuffers(1, &s_VboHandle);
    glCreateBuffers(1, &s_ElementsHandle);
    RELEASE_ASSERT(s_VboHandle && s_ElementsHandle, "Failed to create buffers");

    ImGui_Impl_GL45_CreateFontsTexture();
    logdebug2("ImGui_Impl_GL45_CreateDeviceObjects() done");

    return true;
}

void ImGui_Impl_GL45_InvalidateContext(ImGuiContext *ctx)
{
    s_imgui_ctxMap.erase(ctx);
}

void ImGui_Impl_GL45_InvalidateDeviceObjects()
{
    if (s_imgui_FontTexture)
    {
        s_imgui_FontTexture = NULL;
        ImGui::GetIO().Fonts->TexID = 0;
    }
}

void ImGui_Impl_GL45_ForgetDeviceObjects()
{
    s_VboHandle = s_ElementsHandle = 0;
    s_vboPtr = 0;
    s_elemPtr = 0;
    g_Prog = 0;
    s_imgui_FontTexture = NULL;
    ImGui::GetIO().Fonts->TexID = 0;

    // Zero out all handles before clearing the map, to avoid trying to delete stuff that's already gone
    for(ContextMap::iterator it = s_imgui_ctxMap.begin(); it != s_imgui_ctxMap.end(); ++it)
        it->second.vaoHandle = 0;
    s_imgui_ctxMap.clear();
}

void ImGui_Impl_GL45_Shutdown()
{
    if(s_VboHandle)
        glDeleteBuffers(1, &s_VboHandle);
    if(s_ElementsHandle)
        glDeleteBuffers(1, &s_ElementsHandle);

    s_imgui_ctxMap.clear();

    if(g_Prog)
        glDeleteProgram(g_Prog);

    ImGui_Impl_GL45_ForgetDeviceObjects();
    ImGui::Shutdown();
}

#include "blendmode.h"
#include "glapi.h"
#include "perstateholder.h"
#include <vector>
#include "macros.h"
#include <assert.h>
#include "renderer.h"

namespace BlendMode {

typedef std::vector<Enum> BlendStack;

static PerStateHolder<unsigned, BlendStack> s_perstate;


static void apply(Enum mode)
{
    unsigned f1, f2;
    switch(mode)
    {
        case REPLACE:
            glDisable(GL_BLEND);
            return;
        case NORMAL:
            f1 = GL_SRC_ALPHA;
            f2 = GL_ONE_MINUS_SRC_ALPHA;
            break;
        case ADD:
            f1 = GL_SRC_ALPHA;
            f2 = GL_ONE;
            break;
        default:
            assert(false);
        case DONT_CHANGE:
            return;
    }

    glEnable(GL_BLEND);
    glBlendFunc(f1, f2);
}

// apply the topmost blend mode that is not DONT_CHANGE
void applytop(const BlendStack& bs)
{
    size_t i = bs.size();
    assert(i);
    while(i--)
        if(bs[i] != DONT_CHANGE)
        {
            apply(bs[i]);
            return;
        }
    // bottom of stack is anything but DONT_CHANGE -> should never be reached
    assert(false);
}

void Push(Enum mode)
{
    BlendStack &bs = s_perstate[Renderer::GetCurrentGID()];
    assert(bs.size() || mode != DONT_CHANGE); // make sure first mode is not DONT_CHANGE
    bs.push_back(mode);
    apply(mode);
}

void Pop()
{
    BlendStack &bs = s_perstate[Renderer::GetCurrentGID()];
    bs.pop_back();
    applytop(bs);
}

void Init(Enum mode)
{
    BlendStack &bs = s_perstate[Renderer::GetCurrentGID()];
    assert(bs.size() <= 1);
    bs.clear();
    Push(mode);
}

void CleanupRenderer(unsigned gid)
{
    s_perstate.remove(gid);
}


} // end namespace BlendMode

#include <assert.h>
#include <stdint.h>
#include "gpubuffer.h"
#include "glapi.h"
#include "log.h"
#include "texture.h"
#include "glgpuutil.h"

static unsigned glaccess(GPUBufferRaw::Access a)
{
    unsigned bits = 0;
    if(a & GPUBufferRaw::MAP_READ)
        bits |= GL_MAP_READ_BIT;
    if(a & GPUBufferRaw::MAP_WRITE)
        bits |= GL_MAP_WRITE_BIT;
    if(a & GPUBufferRaw::UPDATE)
        bits |= GL_DYNAMIC_STORAGE_BIT;
    if(a & GPUBufferRaw::PERSISTENT)
        bits |= GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT;
    return bits;
}

static GLuint _makeBuf(const void *data, uint64_t sz, GPUBufferRaw::Access a, const void **pr, void **pw)
{
    assert(pw && pr);
    if(!sz)
        return 0;
    unsigned id = 0;
    glCreateBuffers(1, &id);
    DEBUG_LOG("GPUBufferRaw: Alloc buffer %u, size %u bytes, access = %u", id, (unsigned)sz, a);
    assert(id);
    if(!id)
        return 0;

    const unsigned createFlags = glaccess(a);
    glNamedBufferStorage(id, sz, data, createFlags);

    void *rp = NULL, *wp = NULL;
    if(a & GPUBufferRaw::PERSISTENT)
    {
        const unsigned mapFlags = (createFlags | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT)
            & ~(GL_DYNAMIC_STORAGE_BIT);
        void *p = glMapNamedBufferRange(id, 0, sz, mapFlags);
        if(a & GPUBufferRaw::MAP_READ)
            rp = p;
        if(a & GPUBufferRaw::MAP_WRITE)
            wp = p;
    }
    *pw = wp;
    *pr = rp;
    return id;
}

GPUBufferRaw *GPUBufferRaw::New(const void *data, uint64_t sz, Access a)
{
    const void *rp;
    void *wp;
    unsigned id = _makeBuf(data, sz, a, &rp, &wp);
    GPUBufferRaw *buf = NULL;
    if(id)
    {
        buf = new GPUBufferRaw(id, sz, a, rp, wp);
        if(data && a & BACKUP)
        {
            buf->_ensureBackup();
            memcpy(buf->_backup, data, sz);
        }
    }
    return buf;
}

GPUBufferRaw::~GPUBufferRaw()
{
    DEBUG_LOG("GPUBufferRaw: delete buf %u, size %u bytes", _id, unsigned(_bufsize));
    glDeleteBuffers(1, &_id);
    _clearFences();
}

void GPUBufferRaw::zerofill()
{
    glClearNamedBufferData(_id, GL_R8, GL_RED_INTEGER, GL_UNSIGNED_BYTE, NULL);
    glMemoryBarrier(GL_BUFFER_UPDATE_BARRIER_BIT);
}

// If persistently mapped, do not EVER unmap the buffer until it's deleted.

const void * GPUBufferRaw::mapRead() const
{
    return gpuread ? gpuread : glMapNamedBufferRange(_id, 0, _bufsize, GL_MAP_READ_BIT);
}

void * GPUBufferRaw::mapWrite()
{
    return gpuwrite ? gpuwrite : glMapNamedBufferRange(_id, 0, _bufsize, GL_MAP_WRITE_BIT);
}

void * GPUBufferRaw::mapRW()
{
    assert(gpuwrite == gpuread);
    return gpuwrite ? gpuwrite : glMapNamedBufferRange(_id, 0, _bufsize, GL_MAP_READ_BIT | GL_MAP_WRITE_BIT);
}

void GPUBufferRaw::unmap() const
{
    if(!gpuread && !gpuwrite)
        glUnmapNamedBuffer(_id);
}

void GPUBufferRaw::uploadBytes(const void * ptr, uint64_t bytes, uint64_t offsetBytes)
{
    DEBUG_LOG("GPUBufferRaw[%u]: Upload %u bytes, ptr = %p, offset = %u", _id, unsigned(bytes), ptr, unsigned(offsetBytes));
    assert(hasSpace(bytes, offsetBytes));
    glNamedBufferSubData(_id, offsetBytes, bytes, ptr);
    if(access & BACKUP)
    {
        _ensureBackup();
        memcpy(((char*)_backup) + offsetBytes, ptr, bytes);
    }
    //glMemoryBarrier(GL_BUFFER_UPDATE_BARRIER_BIT);
}

void GPUBufferRaw::downloadBytes(void * ptr, uint64_t bytes, uint64_t offsetBytes) const
{
    DEBUG_LOG("GPUBufferRaw[%u]: Download %u bytes, ptr = %p, offset = %u", _id, unsigned(bytes), ptr, unsigned(offsetBytes));
    assert(hasSpace(bytes, offsetBytes));
    glGetNamedBufferSubData(_id, offsetBytes, bytes, ptr);
}

void GPUBufferRaw::barrier()
{
    glMemoryBarrier(GL_BUFFER_UPDATE_BARRIER_BIT | GL_SHADER_STORAGE_BARRIER_BIT);
}

void GPUBufferRaw::insertFenceSync()
{
    _fences.push_back(glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0));
}

void GPUBufferRaw::waitFences()
{
    if(const size_t N = _fences.size())
    {
        // only needed when not mapped coherently
        //glMemoryBarrier(GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT);

        for(size_t i = 0; i < N; ++i)
        {
            // via https://github.com/nvMcJohn/apitest/blob/master/src/framework/bufferlock.cpp
            unsigned waitflags = 0;
            unsigned res = glClientWaitSync((GLsync)_fences[i], waitflags, GL_TIMEOUT_IGNORED);
            switch(res)
            {
                case GL_ALREADY_SIGNALED:
                case GL_CONDITION_SATISFIED:
                    break; // all good, next fence
                case GL_WAIT_FAILED:
                    glFinish(); // oh crap. better to flush the whole pipeline and hope things will be alright
                    goto end;   // it's going to be costly but the only way to be safe.
                default:
                    // Have to flush if the first wait failed
                    waitflags |= GL_SYNC_FLUSH_COMMANDS_BIT;
            }
        }
    }

end:
    _clearFences();
}

void GPUBufferRaw::wait()
{
    glMemoryBarrier(GL_ALL_BARRIER_BITS); // could be finer-grained, but issuing a glFinish() is going to cause a huge stall anyway.
    glFinish();
    _clearFences();
}

void GPUBufferRaw::invalidate()
{
    glInvalidateBufferSubData(_id, 0, _bufsize);
    _clearFences();
}

bool GPUBufferRaw::restoreBackup()
{
    if(!_backup)
        return false;
    void *p;
    if(access & UPDATE)
        uploadBytes(_backup, _bufsize, 0);
    else if(access & MAP_WRITE && (p = mapWrite()))
    {
        memcpy(p, _backup, _bufsize);
        // Buffer is mapped coherently, so these are not needed
        //glFlushMappedNamedBufferRange(_bufid, 0, _bufsize);
        //glMemoryBarrier(GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT);
        unmap();
    }
    else // Can't update directly, use temp buffer and do GPU->GPU copy
    {
        GLuint b = 0;
        glCreateBuffers(1, &b);
        glNamedBufferStorage(b, _bufsize, _backup, 0);
        glCopyNamedBufferSubData(b, _id, 0, 0, _bufsize);
        glDeleteBuffers(1, &b);
    }
    return true;
}

void GPUBufferRaw::_clearFences()
{
    if(const size_t N = _fences.size())
    {
        for(size_t i = 0; i < N; ++i)
            glDeleteSync((GLsync)_fences[i]);
        _fences.clear();
    }
}

void GPUBufferRaw::onDeviceReset(bool init)
{
    _fences.clear(); // don't do any cleanups
    const void *rp;
    void *wp;

    // HACK: those usually const things must be changed :<
    *const_cast<unsigned*>(&_id) = 0;
    *const_cast<void**>(&gpuread) = NULL;
    *const_cast<void**>(&gpuwrite) = NULL;

    if(init)
    {
        unsigned id =  _makeBuf(_backup, _bufsize, access, &rp, &wp); // also restores backup, if any
        *const_cast<unsigned*>(&_id) = id;
        *const_cast<const void**>(&gpuread) = rp;
        *const_cast<void**>(&gpuwrite) = wp;
    }
}

static unsigned bytesfortype(unsigned type)
{
    switch(type)
    {
        case GL_BYTE: return 1;
        case GL_SHORT: case GL_HALF_FLOAT: return 2;
        case GL_INT: case GL_UNSIGNED_INT: case GL_FLOAT: return 4;
        case GL_DOUBLE: case GL_INT64_ARB: case GL_UNSIGNED_INT64_ARB: return 8;
    }
    assert(false);
    return -1;
}

static unsigned nforfmt(unsigned fmt)
{
    switch (fmt)
    {
        case GL_RED: return 1;
        case GL_RG: return 2;
        case GL_RGB: return 3;
        case GL_RGBA: return 4;
    }
    assert(false);
    return -1;
}

void GPUBufferRaw::importTexture(const TextureAny & tex, uint64_t offsetBytes)
{
    const uint64_t bytes = tex.getSizeBytes();
    assert(hasSpace(bytes, offsetBytes));
    const unsigned format = glgpuutil::getGLFormatForChannels(tex.channels());
    const unsigned type = glgpuutil::getGLType(tex.datatype());
    glMemoryBarrier(GL_PIXEL_BUFFER_BARRIER_BIT);
    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glBindBuffer(GL_PIXEL_PACK_BUFFER, _id);

    // highest positive value for GLsizei -- not sure if it would cope with an unsigned value,
    // but since we're doing block transfers anyway keep it safe.
    const uint64_t blocksize = INT32_MAX;
    uint64_t offset = offsetBytes;
    uint64_t remain = bytes;
    for(uint64_t b = 0; b < bytes; b += blocksize, offset += blocksize, remain -= blocksize)
    {
        GLsizei actualbytes = (GLsizei)(remain < blocksize ? remain : blocksize);
        glGetTextureImage(tex.id(), 0, format, type, actualbytes, (void*)(uintptr_t)offset);
    }
    glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);

    // HACK: attempt to fix crash / sync problem.
    // Apparently the GL context gets lost when a tex->buf transfer operation is active and the texture is deleted
    // <"4.5.0 NVIDIA 372.70" on Win 8.1> 
    // This doesn't fully fix the problem, but seems to make it a lot less likely to appear
    //insertFenceSync();
    //waitFences();
}

// OpenGL 4.5 backend for texture functionality

#include "texture.h"
#include "glapi.h"
#include "util.h"
#include <assert.h>
#include "log.h"
#include "gpubuffer.h"
#include "renderer.h"
#include "blob3dio.h"
#include "glgpuutil.h"

using namespace TexPriv;

unsigned TexPriv::textureTypeToTarget(TextureType ty)
{
    switch(ty)
    {
        case TEX_1D: return GL_TEXTURE_1D;
        case TEX_2D: return GL_TEXTURE_2D;
        case TEX_3D: return GL_TEXTURE_3D;
        default: ;
    }
    assert(false);
    return 0;
}

static const unsigned _glaccess[] =
{
    GL_READ_ONLY,
    GL_WRITE_ONLY,
    GL_READ_WRITE
};


THREADLOCAL std::vector<_TextureBase*> s_allTextures;

void _TextureBase::_dev_init()
{
    s_allTextures.push_back(this);
}

void _TextureBase::_dev_delete()
{
    std::vector<_TextureBase*>& alltex = s_allTextures;
    for(size_t i = 0; i < alltex.size(); ++i)
        if(alltex[i] == this)
        {
            alltex[i] = alltex.back();
            alltex.pop_back();
            break;
        }

    glDeleteTextures(1, &_id);
    _id = 0;
}

// With GL 4.5 functions around there's really no use to have this anymore
/*void _TextureBase::_bind(unsigned target) const
{
    glBindTexture(target, id());
}*/

void _TextureBase::_bindUnit(unsigned target, unsigned unit) const
{
    //DEBUG_LOG("Bind tex %u to sampler unit %u", id(), unit);
    
    // pre-4.5
    //assert(unit < 32);
    //glActiveTexture(GL_TEXTURE0 + unit);
    //_bind(target);

    glBindTextureUnit(unit, id());
}

void _TextureBase::_bindImageUnit(unsigned unit, Access a) const
{
    //DEBUG_LOG("Bind tex %u to IMAGE unit %u, access %u", id(), unit, a);

    if(id())
    {
        glBindImageTexture(unit, id(), 0, GL_TRUE, 0, _glaccess[a], _internalformat);
    }
}

void _TextureBase::_unbind(unsigned target)
{
    glBindTexture(target, 0);
}

void _TextureBase::_unbindImage(unsigned target, unsigned unit)
{
    glBindImageTexture(unit, 0, 0, GL_TRUE, 0, GL_READ_ONLY, GL_RGBA32F);
}

void _TextureBase::_generateMipmaps(unsigned target) const
{
    //glGenerateTextureMipmapEXT(id(), target);
    glGenerateTextureMipmap(id());
}

static void s_dev_upload(unsigned target, unsigned id, glm::uvec3 size, unsigned miplevel, const void *data, Blob3DType datatype, unsigned channels)
{
    const unsigned efmt = glgpuutil::getGLFormatForChannels(channels);
    const unsigned etype = glgpuutil::getGLType(datatype);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    switch(target)
    {
        case GL_TEXTURE_1D:
            glTextureSubImage1D(id, miplevel, 0, size.x, efmt, etype, data);
            break;
        case GL_TEXTURE_2D:
            glTextureSubImage2D(id, miplevel, 0, 0, size.x, size.y, efmt, etype, data);
            break;
        case GL_TEXTURE_3D:
            glTextureSubImage3D(id, miplevel, 0, 0, 0, size.x, size.y, size.z, efmt, etype, data);
            break;
        default:
            assert(false);
            return;
    }

    glMemoryBarrier(GL_TEXTURE_UPDATE_BARRIER_BIT);
}

// data may be a normal pointer, or an offset into a bound pixel pack buffer
void _TextureBase::_dev_upload(unsigned target, unsigned miplevel, const void *data, Blob3DType datatype, unsigned channels)
{
    s_dev_upload(target, _id, _size, miplevel, data, datatype, channels);
}

void _TextureBase::_dev_upload(unsigned target, unsigned miplevel, const GPUBufferRaw & buf, size_t offset, Blob3DType datatype, unsigned channels)
{
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, buf.id());
    _dev_upload(target, miplevel, (void*)(uintptr_t)(offset), datatype, channels);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
    glMemoryBarrier(GL_PIXEL_BUFFER_BARRIER_BIT);
}

void _TextureBase::_dev_download(unsigned target, unsigned miplevel, void *dst, size_t bufsz) const
{
    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    const unsigned efmt = glgpuutil::getGLFormatForChannels(_channels);
    const unsigned etype = glgpuutil::getGLType(_datatype);
    glMemoryBarrier(GL_TEXTURE_UPDATE_BARRIER_BIT);
    glGetTextureImage(_id, miplevel, efmt, etype, (GLsizei)bufsz, dst);
}

void _TextureBase::_dev_download(unsigned target, unsigned miplevel, GPUBufferRaw & buf, size_t offset) const
{
    const size_t avail = buf.bytes() - offset;
    glBindBuffer(GL_PIXEL_PACK_BUFFER, buf.id());
    _dev_download(target, miplevel, (void*)(uintptr_t)(offset), avail);
    glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
    glMemoryBarrier(GL_PIXEL_BUFFER_BARRIER_BIT | GL_BUFFER_UPDATE_BARRIER_BIT);
}

void _TextureBase::_queryHandles() const
{
    assert(!_handleTex && !_handleImg);
    if(ENABLE_ARB_bindless_texture && !_handleTex)
    {
        // According to ARB_bindless_texture spec, layered == true is not allowed for 1D/2D textures.
        // But to get a handle to a full 3D texture, layered must be true.
        const GLboolean layered = _ty == TEX_3D ? GL_TRUE : GL_FALSE;

        _handleImg = glGetImageHandleARB(_id, 0, layered, 0, _internalformat);
        _handleTex = glGetTextureHandleARB(_id);
        DEBUG_LOG("bindless tex %u handles: [tex = " I64FMTX ", img = " I64FMTX "]", _id, _handleTex, _handleImg);
        assert(_handleTex && _handleImg);
    }
}

void _TextureBase::_ensureHandles() const
{
    assert(ENABLE_ARB_bindless_texture);
    if(!_handleTex)
    {
        assert(!_handleImg);
        _queryHandles();
    }
}

uint64_t _TextureBase::SizeInBytes(const Blob3DInfo & info, unsigned miplevel)
{
    return SizeInBytes(glm::uvec3(info.w, info.h, info.d), info.type, info.channels, miplevel);
}

unsigned _TextureBase::GetInternalFormat(unsigned channels, Blob3DType datatype, TextureFlags flags)
{
    // OPENGL HACK: 3-channel images seem to cause more harm than good.
    // For example bindless textures fail to obtain image handles, image store fails, and so on.
    // So we better use 4 components internally instead of the requested 3.
    // Higher-level logic ensures that the swizzle mask is set correctly ("rgb1" is still the default for 3 channels).
    if(channels == 3)
        channels = 4;

    return glgpuutil::getGLInternalFormat(channels, datatype, flags);
}

void _TextureBase::_zerofill(unsigned target) const
{
    for(unsigned i = 0; i < _miplevel; ++i)
        glClearTexImage(_id, i, _internalformat, GL_UNSIGNED_BYTE, NULL);
}

void _TextureBase::_invalidate(unsigned target) const
{
    for(unsigned i = 0; i < _miplevel; ++i)
        glInvalidateTexImage(_id, i);
}

// Note: Calling glTextureParameteriv() is not allowed after getting bindless texture handles,
// so it's the safest to ONLY call this in the ctor!
void _TextureBase::_dev_setSwizzleMask(const char * mask)
{
    GLint swiz[4];

    for(unsigned i = 0; i < 4; ++i)
    {
        unsigned w = GL_ZERO;
        switch(mask[i])
        {
            case 'r': w = GL_RED; break;
            case 'g': w = GL_GREEN; break;
            case 'b': w = GL_BLUE; break;
            case 'a': w = GL_ALPHA; break;
            case '0': w = GL_ZERO; break;
            case '1': w = GL_ONE; break;
            default: assert(false); // Totally fine to fall through in release mode
        }
        swiz[i] = w;
    }
    glTextureParameteriv(_id, GL_TEXTURE_SWIZZLE_RGBA, &swiz[0]);
}

void _TextureBase::makeTexResident() const
{
    if(ENABLE_ARB_bindless_texture)
    {
        unsigned& mask = _residency[Renderer::GetCurrentGID()].mask;
        if(!(mask & 1))
        {
            mask |= 1;
            _ensureHandles();
            glMakeTextureHandleResidentARB(_handleTex);
        }
    }
}

void _TextureBase::makeTexNotResident() const
{
    if(_handleTex) // HAS_GL_bindless_texture
    {
        Residency *pr = _residency.getp(Renderer::GetCurrentGID());
        if(pr && pr->mask & 1)
        {
            pr->mask &= ~1;
            glMakeTextureHandleNonResidentARB(_handleTex);
        }
    }
}

void _TextureBase::makeImageResident(Access a)
{
    if(ENABLE_ARB_bindless_texture)
    {
        Residency& residency = _residency[Renderer::GetCurrentGID()];
        _ensureHandles();
        if(!(residency.mask & 2))
        {
            residency.mask |= 2;
    up:
            glMakeImageHandleResidentARB(_handleImg, _glaccess[a]);
            residency.imgaccess = a;
            return;
        }
        else if(residency.imgaccess != a) // resident, but maybe not the right access?
        {
            glMakeImageHandleNonResidentARB(_handleImg);
            goto up;
        }
    }
}

void _TextureBase::makeImageNotResident()
{
    if(_handleImg) // HAS_GL_bindless_texture
    {
        Residency *pr = _residency.getp(Renderer::GetCurrentGID());
        if(pr && pr->mask & 2)
        {
            pr->mask &= ~2;
            glMakeImageHandleNonResidentARB(_handleImg);
        }
    }
}

// Ugly as hell, but what can you do, when residency is per-GL-state and you need to clear all of them...
void _TextureBase::clearAllResidency()
{
    if(_handleTex) // HAS_GL_bindless_texture
    {
        Renderer *cur = Renderer::GetCurrentRenderer();
        assert(cur);
        const size_t n = Renderer::GetNumInstances();
        assert(n);
        Renderer * const * const ra = Renderer::GetAllInstances();
        for(size_t i = 0; i < n; ++i)
        {
            Renderer *r = ra[i];
            if(Residency *pr = _residency.getp(r->getGlobalID()))
            {
                r->makeCurrent(); // if this context is in standby, all residencies must have been cleared (otherwise this would assert fail)

                if(pr->mask & 1)
                    glMakeTextureHandleNonResidentARB(_handleTex);
                if(pr->mask & 2)
                    glMakeImageHandleNonResidentARB(_handleImg);
                pr->mask &= ~(1 | 2);
            }
        }
        cur->makeCurrentUnsafe();
    }
    _residency.clear();
}


// Make all textures non-resident in the passed context
void _TextureBase::CleanupRenderer(Renderer *r)
{
    const GLExtensionContext *ext = (const GLExtensionContext*)r->getExtensionsCtx();
    if(!ext || !ext->HAS_ARB_bindless_texture)
        return;

    Renderer *oldcur = Renderer::GetCurrentRenderer();
    logdebug2("_TextureBase::CleanupRenderer[glctx = %p]: Clear residency of %u textures...", r->getContext(), (unsigned)s_allTextures.size());
    r->makeCurrent();
    const unsigned gid = r->getGlobalID();

    for(size_t i = 0; i < s_allTextures.size(); ++i)
    {
        _TextureBase * const tex = s_allTextures[i];
        if(tex->_handleTex)
        {
            if(Residency *pr = tex->_residency.getp(gid))
            {
                if(pr->mask & 1)
                    glMakeTextureHandleNonResidentARB(tex->_handleTex);
                if(pr->mask & 2)
                    glMakeImageHandleNonResidentARB(tex->_handleImg);
                tex->_residency.remove(gid);
            }
        }
    }
    if(oldcur && !oldcur->isCurrent())
        oldcur->makeCurrentUnsafe();
}

void _TextureBase::barrier()
{
    glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT | GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
}

_TextureBase::Handle _TextureBase::getImageHandle() const
{
    if(ENABLE_ARB_bindless_texture)
    {
        if(Handle h = _handleImg)
            return h;
        _queryHandles();
        return _handleImg;
    }
    return 0;
}

_TextureBase::Handle _TextureBase::getTexHandle() const
{
    if(ENABLE_ARB_bindless_texture)
    {
        if(Handle h = _handleTex)
            return h;
        _queryHandles();
        return _handleTex;
    }
    return 0;
}

static unsigned setupTex(unsigned target, bool mipmap, bool linear)
{
    unsigned id = 0;
    glCreateTextures(target, 1, &id);
    assert(id);
    if(!id)
        return 0;
    const int minfilter = mipmap ? (linear ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST_MIPMAP_NEAREST) : (linear ? GL_LINEAR : GL_NEAREST);
    const int magfilter = linear ? GL_LINEAR : GL_NEAREST;
    glTextureParameteri(id, GL_TEXTURE_MIN_FILTER, minfilter);
    glTextureParameteri(id, GL_TEXTURE_MAG_FILTER, magfilter);
    glTextureParameteri(id, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTextureParameteri(id, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    return id;
}

unsigned _TextureBase::_New(unsigned target, glm::uvec3 sz, Blob3DType datatype, unsigned channels, TextureFlags flags)
{
    assert(sz.x);

    const unsigned miplevel = CalcMipLevel(flags, sz);
    const bool linear = !!(flags & TEX_LINEAR);
    const bool mipmap = !!(flags & TEX_MIPMAP);
    const unsigned id = setupTex(target, mipmap, linear);
    if(!id)
        return 0;

    const unsigned internalformat = GetInternalFormat(channels, datatype, flags);
    if(!internalformat)
        return 0;

    switch(target)
    {
    case GL_TEXTURE_1D:
        glTextureStorage1D(id, miplevel, internalformat, sz.x);
        break;
    case GL_TEXTURE_2D:
        glTextureStorage2D(id, miplevel, internalformat, sz.x, sz.y);
        break;
    case GL_TEXTURE_3D:
        glTextureStorage3D(id, miplevel, internalformat, sz.x, sz.y, sz.z);
        break;
    default:
        assert(false);
    }
    DEBUG_LOG("setup tex %u (%ux%ux%u:%u) mip=%u linear=%u",
        id, sz.x, sz.y, sz.z, channels, unsigned(mipmap), unsigned(linear));
    return id;
}

unsigned _TextureBase::_convert(unsigned target, Blob3DType datatype, unsigned channels, TextureFlags flags) const
{
    assert(datatype != B3D_UNSPECIFIED);
    assert(channels);

    // FIXME: this is horrible, but at least we're not doing a GPU->CPU->GPU round-trip
    // This would better be solved using compute shaders and doing explicit conversion
    // (For some reason a pixel download operation does not perform conversion -- only uploads do. So we must download and re-upload.)
    GPUBufRef tmp = GPUBufferRaw::FromTexture(*static_cast<const TextureAny*>(this), GPUBufferRaw::NONE);

    const unsigned id = _New(target, _size0, datatype, channels, flags);
    if(!id)
        return 0;

    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, tmp->id());
    glMemoryBarrier(GL_PIXEL_BUFFER_BARRIER_BIT);
    s_dev_upload(target, id, _size0, 0, NULL, _datatype, _channels);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);

    // _afterUpload() is called via parent function (see texture.h)

    return id;
}

#include "framebuffer.h"
#include "glapi.h"
#include "renderer.h"
#include "log.h"
#include "util.h"

const ScriptDef FrameBuffer::scriptdef("framebuffer", ScriptTypes::FRAMEBUFFER);

static THREADLOCAL std::vector<FrameBufCRef> s_fbstack;
static THREADLOCAL PerStateHolder<unsigned, const FrameBuffer* >s_bound;
static THREADLOCAL std::vector<FrameBuffer*> s_allFramebuffers;

static void fberror(const FrameBuffer *fb, const char *what)
{
    logerror("Attempt to %s empty / uninitialized framebuffer", what);
    if(const char *stack = fb->debug_getCreationCallstack())
        logerror("Lua callstack at creation time:\n%s", stack);
}

static bool isBound(const FrameBuffer *fb)
{
    const unsigned gid = Renderer::GetCurrentGID();
    return s_bound.getdef(gid, NULL) == fb;
}

inline static glm::uvec2 fbsize(const FrameBuffer *fb)
{
    return fb ? fb->getSize() : Renderer::GetCurrentRenderer()->getViewport();
}

inline static const FrameBuffer *top()
{
    return s_fbstack.size() ? s_fbstack.back().content() : NULL;
}

static bool goodslot(unsigned slot)
{
    return slot < FrameBuffer::MAX_ATTACHMENTS;
}

FrameBuffer::FrameBuffer()
: DeviceObjectBase(1) // has Texture (aka DeviceObject(0)) as children
, _renderbuf(0)
, _inited(false)
{
    s_allFramebuffers.push_back(this);
}

FrameBuffer::~FrameBuffer()
{
    logdebug2("FrameBuffer::~FrameBuffer(): Clear for %u contexts...", (unsigned)fbos.size());

    destroy();

    Renderer *cur = Renderer::GetCurrentRenderer();
    assert(cur);
    const size_t n = Renderer::GetNumInstances();
    assert(n);
    Renderer * const * const ra = Renderer::GetAllInstances();
    for(size_t i = 0; i < n; ++i)
    {
        Renderer *r = ra[i];
        if(const PerStateData *sd = fbos.getp(r->getGlobalID()))
        {
            r->makeCurrent();
            glDeleteFramebuffers(1, &sd->fboId);
        }
    }
    cur->makeCurrentUnsafe();

    std::vector<FrameBuffer*>& allfb = s_allFramebuffers;
    for(size_t i = 0; i < allfb.size(); ++i)
        if(allfb[i] == this)
        {
            allfb[i] = allfb.back();
            allfb.pop_back();
            break;
        }
}

void FrameBuffer::CleanupRenderer(Renderer *r)
{
    Renderer *oldcur = Renderer::GetCurrentRenderer();
    logdebug2("FrameBuffer::CleanupRenderer[glctx = %p]: Clear %u FBOs...", r->getContext(), (unsigned)s_allFramebuffers.size());
    r->makeCurrent();
    const unsigned gid = r->getGlobalID();
    s_bound.remove(gid);

    for(size_t i = 0; i < s_allFramebuffers.size(); ++i)
    {
        FrameBuffer *fb = s_allFramebuffers[i];
        if(PerStateData *sd = fb->fbos.getp(gid))
        {
            glDeleteFramebuffers(1, &sd->fboId);
            fb->fbos.remove(gid);
        }
    }
    if(oldcur && !oldcur->isCurrent())
        oldcur->makeCurrentUnsafe();
}

unsigned FrameBuffer::_getFBO() const
{
    const unsigned gid = Renderer::GetCurrentGID();
    if(PerStateData *sd = fbos.getp(gid))
    {
        const unsigned id = sd->fboId;
        if(sd->rebind)
        {
            sd->rebind = false;
            _attachObjects(id);
        }
        return id;
    }

    unsigned id = 0;
    glCreateFramebuffers(1, &id);
    assert(id);
    if(!id)
        return 0;
    const PerStateData dat { id, false };
    fbos.add(gid, dat);
    _attachObjects(id);
    return id;
}

// linear mapping, so that attachment I is always drawbuffer I.
static void updateDrawBuffers(unsigned fb, const Tex2DRef *texs)
{
    const unsigned N = FrameBuffer::MAX_ATTACHMENTS;
    unsigned drawbufs[N];
    for(unsigned i = 0; i < N; ++i)
    {
        drawbufs[i] = texs[i] ? GL_COLOR_ATTACHMENT0 + i : GL_NONE;
    }
    glNamedFramebufferDrawBuffers(fb, N, &drawbufs[0]);
    assert(glCheckNamedFramebufferStatus(fb, GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
}

// attach objects to current GL state
void FrameBuffer::_attachObjects(unsigned id) const
{
    assert(_inited);
    glNamedFramebufferTexture(id, GL_DEPTH_ATTACHMENT, _dtex->id(), 0);
    glNamedFramebufferRenderbuffer(id, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _renderbuf);

    DEBUG_LOG("Attach framebuffer %u objects [renderbuffer %u, dtex %u] (%ux%u)", id, _renderbuf, _dtex->id(), _dim.x, _dim.y);

    for(unsigned i = 0; i < Countof(_texs); ++i)
        if(Texture2D *tex = _texs[i].content())
            glNamedFramebufferTexture(id, GL_COLOR_ATTACHMENT0 + i, tex->id(), 0);

    updateDrawBuffers(id, &_texs[0]);
}

void FrameBuffer::_markOthersForRebind() const
{
    const unsigned gid = Renderer::GetCurrentGID();
    for(size_t i = 0; i < fbos.size(); ++i)
        if(fbos._v[i].k != gid)
            fbos._v[i].val.rebind = true;
}

void FrameBuffer::destroy()
{
    DEBUG_LOG("Destroy framebuffer [renderbuffer %u] (%ux%u)", _renderbuf, _dim.x, _dim.y);
    assert(!isBound(this));

    // Don't do glDeleteFramebuffers() here. Keep the FBO alive until the dtor.

    _dim.x = _dim.y = 0;
    _dtex = NULL;
    for(unsigned i = 0; i < MAX_ATTACHMENTS; ++i)
        _texs[i] = NULL;
    if(_renderbuf)
    {
        glDeleteRenderbuffers(1, &_renderbuf);
        _renderbuf = 0;
    }

    // Clear out the FBO, but don't delete it.
    // Make sure other contexts will also clear it out if the FBO is bound there
    const unsigned gid = Renderer::GetCurrentGID();
    if(const PerStateData *sd = fbos.getp(gid))
        _attachObjects(sd->fboId);
    _markOthersForRebind();
    _inited = false;
}

// clear first attachment and depth buffer
void FrameBuffer::clear(glm::vec4 col) const
{
     if(!_inited)
        fberror(this, "clear");
    const unsigned id = _getFBO();
    assert(id);
    clearDepth();
    glClearNamedFramebufferfv(id, GL_COLOR, 0, glm::value_ptr(col));
}

void FrameBuffer::clearColor(glm::vec4 col, unsigned slot) const
{
    if(!_inited)
        fberror(this, "clearColor");
    const unsigned id = _getFBO();
    assert(id);
    assert(goodslot(slot));
    glClearNamedFramebufferfv(id, GL_COLOR,  slot, glm::value_ptr(col));
}

void FrameBuffer::clearAll(glm::vec4 col) const
{
    if(!_inited)
        fberror(this, "clearAll");
    const unsigned id = _getFBO();
    assert(id);
    clearDepth();
    for(unsigned i = 0; i < MAX_ATTACHMENTS; ++i)
        if(_texs[i])
            glClearNamedFramebufferfv(id, GL_COLOR, i, glm::value_ptr(col));
}

void FrameBuffer::clearDepth() const
{
    if(!_inited)
        fberror(this, "clearDepth");
    const unsigned id = _getFBO();
    assert(id);

    // There is no stencil buffer. AMD CodeXL complains about this.
    //glClearNamedFramebufferfi(_framebuf, GL_DEPTH_STENCIL, 0, 1.0f, 0);
    const float depth = 1;
    glClearNamedFramebufferfv(id, GL_DEPTH, 0, &depth);
}

bool FrameBuffer::IsStackEmpty()
{
    return s_fbstack.empty();
}

glm::uvec2 FrameBuffer::GetBaseViewport()
{
    return fbsize(NULL);
}

glm::uvec2 FrameBuffer::GetCurrentViewport()
{
    return fbsize(top());
}

void FrameBuffer::Push(const FrameBuffer *fb)
{
     s_fbstack.push_back(fb);
     BindTop();
}

// (It would be more efficient to just keep the framebuffer bound and swap texture targets in and out...)
void FrameBuffer::BindTop()
{
    //glMemoryBarrier(GL_FRAMEBUFFER_BARRIER_BIT);
    const FrameBuffer *fb = top();
    if(fb)
        fb->bind();
    else // If top is NULL, use bottom entry (-> window framebuffer)
    {
        const unsigned gid = Renderer::GetCurrentGID();
        s_bound[gid] =  NULL;
        glm::uvec2 vp = GetBaseViewport();
        //DEBUG_LOG("FB render to window (%ux%u)", vp.x, vp.y);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glViewport(0, 0, vp.x, vp.y);
    }
}

void FrameBuffer::Pop()
{
    s_fbstack.pop_back();
    BindTop();
    glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT | GL_FRAMEBUFFER_BARRIER_BIT);
}

bool FrameBuffer::init(glm::uvec2 dim, Flags flags,  TextureFlags texflags)
{
    return init(dim, flags, &texflags, 1);
}

bool FrameBuffer::init(glm::uvec2 dim, Flags flags, const TextureFlags *ptexflags, unsigned num)
{
    assert(!isBound(this));
    assert(num < MAX_ATTACHMENTS);
    if(num >= MAX_ATTACHMENTS)
    {
        logerror("FrameBuffer: Too many attachments (%u of %u allowed)", num, MAX_ATTACHMENTS);
        return false;
    }

    destroy(); // also sets _dim = (0, 0);

    if(!dim.x || !dim.y)
    {
        logerror("FrameBuffer: Attempt to create zero-sized framebuffer");
        return false;
    }

    _dim = dim;

    if(flags & WITH_DEPTH_TEXTURE)
    {
        // No depth buffer necessary if there's a depth texture already
        flags = Flags(flags & ~WITH_DEPTH_BUFFER);

        unsigned dtexid = 0;
        glCreateTextures(GL_TEXTURE_2D, 1, &dtexid);
        assert(dtexid);
        glTextureParameteri(dtexid, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTextureParameteri(dtexid, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        // FIXME: This is a bit strange. When i wrote this part,
        // trying to get the bindless texture handles for a depth component texture
        // would fail (and cause a GL error).
        // Couldn't find anything about this in the spec.
        /*unsigned channels = TEXCH_IS_DEPTH;
        glTextureStorage2D(dtexid, 1, GL_DEPTH_COMPONENT32F, dim.x, dim.y);*/
        
        // The alternative: Use single-channel float texture as depth attachment.
        // This seems to work...
        unsigned channels = 1;
        glTextureStorage2D(dtexid, 1, GL_R32F, _dim.x, _dim.y);

        _dtex = new Texture2D(dtexid, dim, B3D_FLOAT, channels, TEX_LINEAR, NULL);
    }
    else
        _dtex = NULL;

    _flags = flags;

    DEBUG_LOG("Setup framebuffer with %u target textures", num);

    // Create color textures
    for(unsigned i = 0; i < num; ++i)
    {
        _texs[i] = Texture2D::New(dim, B3D_FLOAT, 4, ptexflags[i], NULL);
        DEBUG_LOG("FB Tex[%u] uses tex id %u", i, _texs[i]->id());
    }

    const bool ok = _initFB();
    if(!ok)
        destroy();
    return ok;
}

bool FrameBuffer::_initFB()
{
    _inited = true;
    const unsigned id = _getFBO();
    _attachObjects(id);

    const int status = glCheckNamedFramebufferStatus(id, GL_FRAMEBUFFER);
    const bool ok = status == GL_FRAMEBUFFER_COMPLETE;
    assert(ok);
    if(!ok)
        destroy();

    return ok;
}

bool FrameBuffer::attach(Texture2D * tex, unsigned slot)
{

    assert(goodslot(slot));

    unsigned texid = 0;
    if(tex)
    {
        glm::uvec2 d = tex->size();
        if(d.x < _dim.x || d.y < _dim.y) // too small?
            return false;
        texid = tex->id();
    }

    const unsigned fbo = _getFBO();
    assert(fbo);

    _texs[slot] = tex;
    glNamedFramebufferTexture(fbo, GL_COLOR_ATTACHMENT0 + slot, texid, 0);
    updateDrawBuffers(fbo, &_texs[0]);
    _markOthersForRebind();
    return true;
}

Texture2D * FrameBuffer::colorTex(unsigned slot)
{
    assert(goodslot(slot));
    assert(!isBound(this));
    return _texs[slot].content();
}

Texture2D * FrameBuffer::depthTex() const
{
    assert(!isBound(this));
    return _dtex.content();
}

const Texture2D * FrameBuffer::colorTex(unsigned slot) const
{
    assert(goodslot(slot));
    assert(!isBound(this));
    return _texs[slot].content();
}

void FrameBuffer::bind() const
{
    if(!_inited)
        fberror(this, "bind");
    const unsigned id = _getFBO();
    assert(id);
    const unsigned gid = Renderer::GetCurrentGID();
    if(s_bound.getdef(gid, NULL) != this)
    {
        s_bound[gid] = this;

        //DEBUG_LOG("FB %u bind (%ux%u) [stack=%u]", _id, _dim.x, _dim.y, (unsigned)s_fbstack.size());
        glViewport(0, 0, _dim.x, _dim.y);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, id);
    }
}

void FrameBuffer::onDeviceReset(bool init)
{
    assert(!isBound(this));
    assert(s_fbstack.empty());

    _renderbuf = 0;
    fbos.clear();

    // Reset all textures first...
    for(unsigned i = 0; i < Countof(_texs); ++i)
        if(Texture2D *tex = _texs[i].content())
            tex->performDeviceReset(init);
    if(_dtex)
        _dtex->performDeviceReset(init);

    // ... then re-create the rest of those device objects
    if(init)
    {
        bool ok = _initFB();
        assert(ok);
        if(!ok)
            destroy();
    }
}

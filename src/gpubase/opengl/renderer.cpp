#include <SDL.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "renderer.h"
#include "glapi.h"
#include "mesh.h"
#include "fileio.h"
#include <vector>
#include <algorithm>
#include <limits>
#include <sstream>
#include "util.h"
#include "log.h"
#include "shader.h"
#include "framebuffer.h"
#include <SDL_atomic.h>
#include "blendmode.h"
#include "platform.h"

static THREADLOCAL void *s_curCtx = NULL;
static SDL_atomic_t s_counter = { 0 };
static THREADLOCAL unsigned s_curGID = 0;
static THREADLOCAL Renderer *s_current = 0;

static std::vector<Renderer*> s_instances;
static std::vector<Renderer*> s_standby;


static const SDL_GLcontextFlag DEFAULT_GL_ATTRIB_CONTEXT_FLAGS = SDL_GLcontextFlag(
    SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG // Don't support legacy crap
  | SDL_GL_CONTEXT_ROBUST_ACCESS_FLAG      // Safer memory accesses
  | SDL_GL_CONTEXT_RESET_ISOLATION_FLAG    // If the context crashes, maybe don't take out the whole system
#ifdef _DEBUG
  | SDL_GL_CONTEXT_DEBUG_FLAG // Always create debug contexts in debug mode
#endif
);

static int setAttrib(SDL_GLattr attr, int val, const char *str)
{
    int ret = SDL_GL_SetAttribute(attr, val);
    if(ret < 0)
        logerror("SDL_GL_SetAttribute[%s] = %d  -- Failed", str, val);
    else
        DEBUG_LOG("SDL_GL_SetAttribute[%s] = %d", str, val);
    return ret;
}
#define GLATTR(attr, val) setAttrib(attr, val, #attr)

static void setDefaultGLAttribs(bool failsafe)
{
    if(failsafe)
    {
        SDL_GL_ResetAttributes();
    }
    else
    {
        GLATTR(SDL_GL_ACCELERATED_VISUAL, 1);
        GLATTR(SDL_GL_DOUBLEBUFFER, 1);
        GLATTR(SDL_GL_DEPTH_SIZE, 24);
        GLATTR(SDL_GL_STENCIL_SIZE, 8);
        GLATTR(SDL_GL_CONTEXT_FLAGS, DEFAULT_GL_ATTRIB_CONTEXT_FLAGS);

        // Don't flush the GL context on MakeCurrent()
        #if SDL_VERSION_ATLEAST(2,0,4)
            if(GLATTR(SDL_GL_CONTEXT_RELEASE_BEHAVIOR, SDL_GL_CONTEXT_RELEASE_BEHAVIOR_NONE) < 0)
        #endif
                logerror("Warning: SDL_GL_CONTEXT_RELEASE_BEHAVIOR not supported or not set");

        // Don't just crash when the GPU segfaults
        #if SDL_VERSION_ATLEAST(2,0,6)
            if(GLATTR(SDL_GL_CONTEXT_RESET_NOTIFICATION, SDL_GL_CONTEXT_RESET_LOSE_CONTEXT) < 0)
        #endif
                logerror("Warning: SDL_GL_CONTEXT_RESET_NOTIFICATION not supported or not set");
    }

    // Min. GL 4.5
    GLATTR(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    GLATTR(SDL_GL_CONTEXT_MINOR_VERSION, 5);

    // Always use core profile -- don't want any deprecated functionality
    GLATTR(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    // Share objects between GL contexts
    GLATTR(SDL_GL_SHARE_WITH_CURRENT_CONTEXT, 1);
}

void Renderer::ShowCompatFailReason(SDL_Window *win)
{
    DEBUG_LOG("ShowCompatFailReason()");

    assert(win);

    struct GLVersion
    {
        unsigned char major, minor;
    } static const tryver[] =
    {
        { 4, 5 },
        { 4, 4 }, { 4, 3 }, { 4, 2 }, { 4, 1 }, { 4, 0 }, // GL4
        { 3, 3 }, { 3, 2 }, { 3, 1 }, { 3, 0 }, // GL3
        { 2, 0 }, //  legacy
        { 0, 0 }
    };

    setDefaultGLAttribs(false);
    GLATTR(SDL_GL_SHARE_WITH_CURRENT_CONTEXT, 0); // Minimize the risk of something going wrong

    std::ostringstream ss;
    std::string sver, svendor, srenderer;
    const GLVersion *v;


    SDL_GLContext ctx = NULL;

    for(v = &tryver[0]; v->major; ++v)
    {
        GLATTR(SDL_GL_CONTEXT_MAJOR_VERSION, v->major);
        GLATTR(SDL_GL_CONTEXT_MINOR_VERSION, v->minor);

        ctx = SDL_GL_CreateContext(win);
        if(ctx)
            break;
    }

    if(!ctx)
    {
        // One last try with platform default attribs
        SDL_GL_ResetAttributes();
        ctx = SDL_GL_CreateContext(win);
    }

    if(ctx)
    {
        GLApiSymbols sym;
        GLExtensionContext glext;
        OpenGLAPI::LoadSymbolsNoError(&sym, &glext);
        if(sym.glGetString)
        {
            sver = (const char*)sym.glGetString(GL_VERSION);
            svendor = (const char*)sym.glGetString(GL_VENDOR);
            srenderer = (const char*)sym.glGetString(GL_RENDERER);
        }
    }

    ss << "This program requires OpenGL 4.5.\n";

    if(v->major == 4 && v->minor == 5)
        ss << "Your system supports OpenGL " << unsigned(v->major) << '.' <<  unsigned(v->minor) << ", which should work just fine.\nBut something failed. This message should never be shown.";
    else if(v->major == 4)
    {
        ss << "Your system supports OpenGL " << unsigned(v->major) << '.' <<  unsigned(v->minor) << ", which is not quite enough.\nTry to get updated graphics drivers.";
        #ifdef __APPLE__ // They get what they deserve.
        if(v->minor == 1)
            ss << "\nNote that Apple apparently stopped supporting OpenGL at version 4.1.\nGet a non-Apple operating system that supports modern OpenGL and try again.";
        #endif
    }
    else if(v->major)
        ss << "Your system supports OpenGL " << unsigned(v->major) << '.' <<  unsigned(v->minor) << ", which is too old.\nGet new hardware.";
    else if(!v->major)
    {
        if(ctx)
            ss << "Your system could create an unspecified OpenGL context with default parameters, but nothing else.\nDriver problem?";
        else
            ss << "Your system seems not OpenGL-capable at all.\nDo you have a graphics driver installed?";
    }

    if(svendor.length())
    {
        ss << "\n\nGL Driver reported:"
           << "\nVersion:  " << sver
           << "\nVendor:   " << svendor
           << "\nRenderer: " << srenderer;
    }

    logerrorbox("%s", ss.str().c_str());

    if(ctx)
        SDL_GL_DeleteContext(ctx);
}

bool Renderer::StaticInit()
{
    s_instances.reserve(32);

    if (SDL_GL_LoadLibrary(NULL) != 0)
    {
        logerrorbox("Failed to load GL library:\n%s", SDL_GetError());
        SDL_GL_UnloadLibrary();
        return false;
    }

    setDefaultGLAttribs(false);
    return true;
}

void Renderer::StaticShutdown()
{
    // Actually delete all the contexts
    while(!s_instances.empty())
        delete s_instances[0];

    s_standby.clear();
    OpenGLAPI::ClearSymbols();
    SDL_GL_UnloadLibrary();
    //SDL_AtomicSet(&s_counter, 0); // want to keep this always increasing and never reset
    s_curCtx = NULL;
    s_curGID = 0;
    s_current = NULL;
}

Renderer * Renderer::Create(SDL_Window * win, Renderer::Flags flags)
{
    Renderer *r;
    if(s_standby.size())
    {
        r = s_standby.back();
        logdebug2("Renderer::Create(): Using one from standby [glctx = %p]", r->glctx);
        s_standby.pop_back();
        r->window = win;
    }
    else
    {
        logdebug2("Renderer::Create(): Alloc new context");
        r = new Renderer(win);
    }

    if(!r->init(flags))
    {
        Delete(r);
        r = NULL;
    }
    return r;
}

// Don't actually delete contexts when no longer needed.
// Instead, keep them around until new contexts are requested, and re-use old ones.
// This works around problems on recent (~Aug 2017) AMD driver issues
// where deleting a context may cause a crash in the GL driver when there are other active contexts.
// FIXME: This. ^
void Renderer::Delete(Renderer * r)
{
    assert(std::find(s_standby.begin(), s_standby.end(), r) == s_standby.end());

    if(r)
    {
        logdebug("Moving renderer [glctx = %p] to standby", r->glctx);
        r->shutdown();
        s_standby.push_back(r);
    }
}

static const char *getDebugTypeStr(unsigned e)
{
    switch(e)
    {
        case GL_DEBUG_TYPE_ERROR: return "error";
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: return "deprecated";
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: return "undef. behavior";
        case GL_DEBUG_TYPE_PORTABILITY: return "portability";
        case GL_DEBUG_TYPE_PERFORMANCE: return "performance";
        case GL_DEBUG_TYPE_OTHER: return "other";
    }
    return "unknown";
}

static void APIENTRY debugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam)
{
    const char *msgty = getDebugTypeStr(type);
    switch(severity)
    {
        case GL_DEBUG_SEVERITY_MEDIUM:
        case GL_DEBUG_SEVERITY_LOW:
            logdebug("GL[%s]: %s", msgty, message);
            break;
        case GL_DEBUG_SEVERITY_HIGH:
            logerror("GL[%s]: %s", msgty, message);
            break;
        case GL_DEBUG_SEVERITY_NOTIFICATION:
            //DEBUG_LOG("(GL[%u]: %s)", severity, message);
            break;
        default:
            assert(false);
    }
    if(source == GL_DEBUG_SOURCE_APPLICATION && type == GL_DEBUG_TYPE_OTHER && id == 0xbbbb)
        platform::breakpoint();
}

// --------------------------------------------------------------------------------------

Renderer::Renderer(SDL_Window *win)
: privState(NULL)
, window(win)
, glctx(NULL)
, apictx(NULL)
, extctx(NULL)
, _gid(1 + SDL_AtomicAdd(&s_counter, 1)) // SDL_AtomicAdd() returns prev. value; want to start at 1 so that_gid is never 0
, _gputimer(GPUTimer::noinit)
, _gputimeNS(0)
, _inited(false)
, _failsafe(false)
{
    s_instances.push_back(this);
}

Renderer::~Renderer()
{
    shutdown();

    for(size_t i = 0; i < s_instances.size(); ++i)
        if(s_instances[i] == this)
        {
            s_instances[i] = s_instances.back();
            s_instances.pop_back();
            break;
        }

    killContext();
}

void Renderer::shutdown()
{
    if(!_inited)
        return;

    BlendMode::CleanupRenderer(_gid);
    _TextureBase::CleanupRenderer(this);
    FrameBuffer::CleanupRenderer(this);
    _gputimer.deinit();
    logdebug("Shutdown renderer, glctx = %p", glctx);
    makeNotCurrent();
    _inited = false;
}

void * Renderer::GetCurrentContext()
{
    return s_curCtx;
}

unsigned Renderer::GetCurrentGID()
{
    return s_curGID;
}

Renderer * Renderer::GetCurrentRenderer()
{
    return s_current;
}

Renderer * const * const Renderer::GetAllInstances()
{
    assert(s_instances.size());
    return &s_instances[0];
}

size_t Renderer::GetNumInstances()
{
    return s_instances.size();
}

static void _PrintGLVal(const char *prefix, unsigned which)
{
    GLint x = -1; 
    glGetIntegerv(which, &x);
    logdebug("%s: %u", prefix, x);
}
#define GLVAL(x) _PrintGLVal(#x, x)

static bool s_failsafe = false;

static SDL_GLContext createCtx(Renderer::Flags flags, SDL_Window *window, bool &failsafe)
{
    // Set the debug flag if a debug context is requested
    unsigned ctxflags = DEFAULT_GL_ATTRIB_CONTEXT_FLAGS;
    if(flags & Renderer::DEBUG)
        ctxflags |= SDL_GL_CONTEXT_DEBUG_FLAG;
    GLATTR(SDL_GL_CONTEXT_FLAGS, ctxflags);

    SDL_GLContext glctx = SDL_GL_CreateContext(window);

    if(!glctx)
    {
        logerror("Failed to create GL context, switching to failsafe mode");
        setDefaultGLAttribs(true);
        glctx = SDL_GL_CreateContext(window);
        s_failsafe = true;
    }
    if(!glctx)
    {
        logerror("Failed to create GL context:\n%s", SDL_GetError());
        return NULL;
    }
    failsafe = s_failsafe;
    return glctx;
}

bool Renderer::init(Flags flags)
{
    assert(!_inited);

    Renderer *oldcur = GetCurrentRenderer();

    // Windows:
    // Need to create a GL context BEFORE getting the symbols,
    // otherwise all we'll get is an old OpenGL 1.1 context
    if(!glctx)
    {
       
        glctx = createCtx(flags, window, _failsafe);
        logdebug("Renderer[%u]::init(), glctx = %p", _gid, glctx);
    }

    if(!apictx)
    {
        // Before getting the symbols, make it current.
        // According to spec, the retrieved symbols are valid for this context ONLY!
        // When another context is made current, all global function pointers need to changed to match that context.
        SDL_GL_MakeCurrent(window, glctx);

        GLExtensionContext *ext = (GLExtensionContext*)(extctx ? extctx : (extctx = new GLExtensionContext));

        GLApiSymbols *api = new GLApiSymbols;
        if(!OpenGLAPI::LoadSymbols(api, ext, _failsafe))
        {
            delete api;
            SDL_GL_DeleteContext(glctx);
            glctx = NULL;
            return false;
        }
        apictx = api;

        std::ostringstream os;
        #define GL_PTR(pty, fn)
        #define GL_GROUP_PTR(g, pty, fn)
        #define GL_GROUP(g)                 if(ext->HAS_ ## g) os << "GL_" #g " ";
        #include "glstubs.h"
        _supportedExt = os.str();
    }

    makeCurrentUnsafe(); // This applies the just loaded symbols

    // enable vsync to burn less CPU
    if(flags & VSYNC)
        if(SDL_GL_SetSwapInterval(-1) == -1)
            SDL_GL_SetSwapInterval(1);

    // Debug callbacks are likely only enabled in debug contexts, but let's set the function in all cases.
    // Sync mode is bad for performance, but if it's a debug context, it's vital to diagnose where an error happened. So that one is on-request.
    if(flags & DEBUG)
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    else
        glDisable(GL_DEBUG_OUTPUT_SYNCHRONOUS);

    glDebugMessageCallback(debugCallback, NULL);

    GLVAL(GL_MAX_COMPUTE_SHARED_MEMORY_SIZE);
    GLVAL(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS);
    
    // Actual robustness needs a few things:
    // * GL_ARB_robust_buffer_access_behavior
    // * GL_ARB_robustness
    // * Any of:
    //  ** WGL_ARB_create_context_robustness
    //  ** GLX_ARB_create_context_robustness
    // SDL already handles the WGL/GLX part, but we still need to check that the context was really created with robustness flags.
    bool robust = ((GLExtensionContext*)extctx)->HAS_ARB_robust_buffer_access_behavior;
    {
        GLint x = 0;
        glGetIntegerv(GL_RESET_NOTIFICATION_STRATEGY, &x);
        const char *pretty = NULL;
        switch(x)
        {
            case GL_LOSE_CONTEXT_ON_RESET:
                pretty = "Lose context on reset";
                break;
            case GL_NO_RESET_NOTIFICATION:
                pretty = "No notification";
                robust = false;
                break;
            default:
                robust = false;
        }
        if(!pretty)
            logerror("Unknown GL reset notification strategy: 0x%X", x);
        else
            logdebug("GL reset notification strategy: %s", pretty);
    }

    {
        GLint x = 0;
        glGetIntegerv(GL_CONTEXT_FLAGS, &x);
        if(x & GL_CONTEXT_FLAG_ROBUST_ACCESS_BIT)
            logdebug("GL_CONTEXT_FLAG_ROBUST_ACCESS_BIT is set");
        else
        {
            logerror("GL_CONTEXT_FLAG_ROBUST_ACCESS_BIT is not set");
            robust = false;
        }

        logdebug("GL_CONTEXT_FLAG_DEBUG_BIT: %u", !!(x & GL_CONTEXT_FLAG_DEBUG_BIT));
        logdebug("GL_CONTEXT_FLAG_FORWARD_COMPATIBLE_BIT: %u", !!(x & GL_CONTEXT_FLAG_FORWARD_COMPATIBLE_BIT));
    }

    if(!robust)
        logerror("Warning: GL access is not robust; may crash on GPU error");

    for(unsigned i = 0; i < 3; ++i)
    {
        int v;
        glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, i, &v);
        logdebug("GL_MAX_COMPUTE_WORK_GROUP_COUNT[%u] = %u", i, v);
    }

    const GLubyte *gpu = glGetString(GL_RENDERER);
    _gpuname = gpu ? (const char *)gpu : "Unknown";

    BlendMode::Init(BlendMode::NORMAL); // Pushes initial bottom-of-stack element, never popped

    if(!_gputimer.wasInit())
        _gputimer.init();

    logdebug("OpenGL Renderer init done!");

    if(oldcur)
        oldcur->makeCurrent();

    _inited = true;

    return true;
}

void Renderer::killContext()
{
    logdebug("Renderer[%u]::killContext(), glctx = %p", _gid, glctx);
    makeNotCurrent();
    if(apictx)
    {
        delete (GLApiSymbols*)apictx;
        apictx = NULL;
    }
    if(extctx)
    {
        delete (GLExtensionContext*)extctx;
        extctx = NULL;
    }
    if(glctx)
    {
        SDL_GL_DeleteContext(glctx);
        glctx = NULL;
    }

    _gputimer.forget();
    _inited = false;
}

unsigned int Renderer::getFreeVideoMemoryKB()
{
    GLint meminfo[4];
    memset(meminfo, 0, 4 * sizeof(GLint));

    const GLExtensionContext *ext = (GLExtensionContext*)extctx;

    if(ext->HAS_NVX_gpu_memory_info)
        glGetIntegerv(GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX, meminfo); // in KB
    else if(ext->HAS_ATI_meminfo)
        glGetIntegerv(GL_TEXTURE_FREE_MEMORY_ATI, meminfo); // in KB, // [0] = total memory free in the pool

    return meminfo[0];
}

unsigned int Renderer::getTotalVideoMemoryKB()
{
    GLint meminfo[4];
    memset(meminfo, 0, 4 * sizeof(GLint));

    const GLExtensionContext *ext = (GLExtensionContext*)extctx;

    if(ext->HAS_NVX_gpu_memory_info)
        glGetIntegerv(GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX , meminfo); // in KB
    else if(ext->HAS_ATI_meminfo)
    {
        // FIXME: ATI?
        // doesn't have such a thing, apparently
        //glGetIntegerv(GL_TEXTURE_FREE_MEMORY_ATI, meminfo); // in KB, // [0] = total memory free in the pool
    }

    return meminfo[0];
}

bool Renderer::isCurrent() const
{
    const bool iscur = _gid == s_curGID; // This is intentionally not [s_current != this]
    // Ensure consistency
    assert(iscur == (s_current == this));
    assert(iscur == (s_curCtx == glctx));
    return iscur;
}

void Renderer::makeCurrent()
{
    assert(_inited); // Make sure this isn't after shutdown()
    if(!isCurrent())
        makeCurrentUnsafe();
}

void Renderer::makeCurrentUnsafe()
{
    s_current = this;
    s_curCtx = glctx;
    s_curGID = _gid;
    SDL_GL_MakeCurrent(window, glctx);
    OpenGLAPI::ApplySymbols((GLApiSymbols*)apictx, (GLExtensionContext*)extctx);
}

void Renderer::makeNotCurrent()
{
    if(_gid == s_curGID) // intentionally no other checks if current, in case the context was reset
    {
        s_curGID = 0;
        s_current = NULL;
        s_curCtx = NULL;
        OpenGLAPI::ClearSymbols();
        SDL_GL_MakeCurrent(window, NULL);
    }
}

static void _checkFrameClean()
{
#ifdef _DEBUG
    assert(FrameBuffer::IsStackEmpty());
    int fbo = 0;
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &fbo);
    assert(fbo == 0);
#endif
}

void Renderer::beginFrame()
{
    assert(s_current == this);
    _checkFrameClean();

    _gputimer.begin();

    glViewport(0, 0, viewport.x, viewport.y);

    glClearColor(0, 0, 0, 0);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);

    glEnable(GL_PROGRAM_POINT_SIZE);
    glPointSize(3.0f);
}


bool Renderer::endFrame()
{
    assert(s_current == this);
    _gputimer.end();

    unsigned presumedstatus = GL_NO_ERROR;
    for(unsigned err, c = 0; (err = glGetError()) != GL_NO_ERROR; ++c)
    {
        switch(err)
        {
            case GL_NO_ERROR:
                break;
            case GL_CONTEXT_LOST:
                presumedstatus = GL_UNKNOWN_CONTEXT_RESET;
                logerror("glGetError returned GL_CONTEXT_LOST");
                goto next; // hard error, continue below
            default:
                logerror("[%u] glGetError returned code %u", c, err);
                // not a hard error
        }
    }

next:

    unsigned status = glGetGraphicsResetStatus();
    if(status == GL_NO_ERROR && presumedstatus == GL_NO_ERROR)
    {
        _checkFrameClean();
        return true; // all good!
    }
    else if(status == GL_NO_ERROR)
        status = presumedstatus; // eh?

    // --- Context is dead below here ----

    const char *reason = NULL;
    switch(status)
    {
        case GL_GUILTY_CONTEXT_RESET: reason = "guilty"; break;
        case GL_INNOCENT_CONTEXT_RESET: reason = "innocent"; break;
        case GL_UNKNOWN_CONTEXT_RESET: reason = "unknown"; break;
    }
    logerror("Renderer[%u]: GL context [%p] reset! (Status: %s)", _gid, glctx, reason);

#ifdef _DEBUG
    // This GL context is pretty dead. Calling any GL function means something doesn't drop state properly.
    // So for the sake of debugging, it's better to crash than risk unclean behavior.
    OpenGLAPI::ClearSymbols();
#endif

    return false;
}

void Renderer::clear(const glm::vec4& col)
{
    glClearColor(col.r, col.g, col.b, col.a);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Renderer::clearDepth()
{
    glClear(GL_DEPTH_BUFFER_BIT);
}

void Renderer::show()
{
    glFinish();
    _gputimeNS = _gputimer.getDiff();
    SDL_GL_SwapWindow(window);
}

void Renderer::resize()
{
    int w, h;
    SDL_GL_GetDrawableSize(window, &w, &h);
    viewport.x = w;
    viewport.y = h;
    logdebug("Renderer: Resize viewport to (%dx%d)", w, h);
}

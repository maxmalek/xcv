// Autogenerated via glsltypes.lua

#ifdef GLSLFAKETYPE
GLSLFAKETYPE(unknown)
GLSLFAKETYPE(buffer)
#undef GLSLFAKETYPE
#endif

#ifdef PRIMMAP
PRIMMAP(GL_FLOAT, float, Float)
PRIMMAP(GL_DOUBLE, double, Double)
PRIMMAP(GL_INT, int, Int)
PRIMMAP(GL_UNSIGNED_INT, unsigned, Uint)
PRIMMAP(GL_BOOL, bool, Bool)
PRIMMAP(GL_INT64_ARB, int64_t, Int64_t)
PRIMMAP(GL_UNSIGNED_INT64_ARB, uint64_t, Uint64_t)
#undef PRIMMAP
#endif

#ifdef GLSLMAP
GLSLMAP(GL_FLOAT_VEC2, vec2)
GLSLMAP(GL_FLOAT_VEC3, vec3)
GLSLMAP(GL_FLOAT_VEC4, vec4)
GLSLMAP(GL_DOUBLE_VEC2, dvec2)
GLSLMAP(GL_DOUBLE_VEC3, dvec3)
GLSLMAP(GL_DOUBLE_VEC4, dvec4)
GLSLMAP(GL_INT_VEC2, ivec2)
GLSLMAP(GL_INT_VEC3, ivec3)
GLSLMAP(GL_INT_VEC4, ivec4)
GLSLMAP(GL_UNSIGNED_INT_VEC2, uvec2)
GLSLMAP(GL_UNSIGNED_INT_VEC3, uvec3)
GLSLMAP(GL_UNSIGNED_INT_VEC4, uvec4)
GLSLMAP(GL_BOOL_VEC2, bvec2)
GLSLMAP(GL_BOOL_VEC3, bvec3)
GLSLMAP(GL_BOOL_VEC4, bvec4)
GLSLMAP(GL_FLOAT_MAT2, mat2)
GLSLMAP(GL_FLOAT_MAT3, mat3)
GLSLMAP(GL_FLOAT_MAT4, mat4)
GLSLMAP(GL_FLOAT_MAT2x3, mat2x3)
GLSLMAP(GL_FLOAT_MAT2x4, mat2x4)
GLSLMAP(GL_FLOAT_MAT3x2, mat3x2)
GLSLMAP(GL_FLOAT_MAT3x4, mat3x4)
GLSLMAP(GL_FLOAT_MAT4x2, mat4x2)
GLSLMAP(GL_FLOAT_MAT4x3, mat4x3)
GLSLMAP(GL_DOUBLE_MAT2, dmat2)
GLSLMAP(GL_DOUBLE_MAT3, dmat3)
GLSLMAP(GL_DOUBLE_MAT4, dmat4)
GLSLMAP(GL_DOUBLE_MAT2x3, dmat2x3)
GLSLMAP(GL_DOUBLE_MAT2x4, dmat2x4)
GLSLMAP(GL_DOUBLE_MAT3x2, dmat3x2)
GLSLMAP(GL_DOUBLE_MAT3x4, dmat3x4)
GLSLMAP(GL_DOUBLE_MAT4x2, dmat4x2)
GLSLMAP(GL_DOUBLE_MAT4x3, dmat4x3)
GLSLMAP(GL_SAMPLER_1D, sampler1D)
GLSLMAP(GL_SAMPLER_2D, sampler2D)
GLSLMAP(GL_SAMPLER_3D, sampler3D)
GLSLMAP(GL_SAMPLER_CUBE, samplerCube)
GLSLMAP(GL_SAMPLER_1D_SHADOW, sampler1DShadow)
GLSLMAP(GL_SAMPLER_2D_SHADOW, sampler2DShadow)
GLSLMAP(GL_SAMPLER_1D_ARRAY, sampler1DArray)
GLSLMAP(GL_SAMPLER_2D_ARRAY, sampler2DArray)
GLSLMAP(GL_SAMPLER_1D_ARRAY_SHADOW, sampler1DArrayShadow)
GLSLMAP(GL_SAMPLER_2D_ARRAY_SHADOW, sampler2DArrayShadow)
GLSLMAP(GL_SAMPLER_2D_MULTISAMPLE, sampler2DMS)
GLSLMAP(GL_SAMPLER_2D_MULTISAMPLE_ARRAY, sampler2DMSArray)
GLSLMAP(GL_SAMPLER_CUBE_SHADOW, samplerCubeShadow)
GLSLMAP(GL_SAMPLER_BUFFER, samplerBuffer)
GLSLMAP(GL_SAMPLER_2D_RECT, sampler2DRect)
GLSLMAP(GL_SAMPLER_2D_RECT_SHADOW, sampler2DRectShadow)
GLSLMAP(GL_INT_SAMPLER_1D, isampler1D)
GLSLMAP(GL_INT_SAMPLER_2D, isampler2D)
GLSLMAP(GL_INT_SAMPLER_3D, isampler3D)
GLSLMAP(GL_INT_SAMPLER_CUBE, isamplerCube)
GLSLMAP(GL_INT_SAMPLER_1D_ARRAY, isampler1DArray)
GLSLMAP(GL_INT_SAMPLER_2D_ARRAY, isampler2DArray)
GLSLMAP(GL_INT_SAMPLER_2D_MULTISAMPLE, isampler2DMS)
GLSLMAP(GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY, isampler2DMSArray)
GLSLMAP(GL_INT_SAMPLER_BUFFER, isamplerBuffer)
GLSLMAP(GL_INT_SAMPLER_2D_RECT, isampler2DRect)
GLSLMAP(GL_UNSIGNED_INT_SAMPLER_1D, usampler1D)
GLSLMAP(GL_UNSIGNED_INT_SAMPLER_2D, usampler2D)
GLSLMAP(GL_UNSIGNED_INT_SAMPLER_3D, usampler3D)
GLSLMAP(GL_UNSIGNED_INT_SAMPLER_CUBE, usamplerCube)
GLSLMAP(GL_UNSIGNED_INT_SAMPLER_1D_ARRAY, usampler1DArray)
GLSLMAP(GL_UNSIGNED_INT_SAMPLER_2D_ARRAY, usampler2DArray)
GLSLMAP(GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE, usampler2DMS)
GLSLMAP(GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY, usampler2DMSArray)
GLSLMAP(GL_UNSIGNED_INT_SAMPLER_BUFFER, usamplerBuffer)
GLSLMAP(GL_UNSIGNED_INT_SAMPLER_2D_RECT, usampler2DRect)
GLSLMAP(GL_IMAGE_1D, image1D)
GLSLMAP(GL_IMAGE_2D, image2D)
GLSLMAP(GL_IMAGE_3D, image3D)
GLSLMAP(GL_IMAGE_CUBE, imageCube)
GLSLMAP(GL_IMAGE_1D_ARRAY, image1DArray)
GLSLMAP(GL_IMAGE_2D_ARRAY, image2DArray)
GLSLMAP(GL_IMAGE_2D_MULTISAMPLE, image2DMS)
GLSLMAP(GL_IMAGE_2D_MULTISAMPLE_ARRAY, image2DMSArray)
GLSLMAP(GL_IMAGE_BUFFER, imageBuffer)
GLSLMAP(GL_IMAGE_2D_RECT, image2DRect)
GLSLMAP(GL_INT_IMAGE_1D, iimage1D)
GLSLMAP(GL_INT_IMAGE_2D, iimage2D)
GLSLMAP(GL_INT_IMAGE_3D, iimage3D)
GLSLMAP(GL_INT_IMAGE_CUBE, iimageCube)
GLSLMAP(GL_INT_IMAGE_1D_ARRAY, iimage1DArray)
GLSLMAP(GL_INT_IMAGE_2D_ARRAY, iimage2DArray)
GLSLMAP(GL_INT_IMAGE_2D_MULTISAMPLE, iimage2DMS)
GLSLMAP(GL_INT_IMAGE_2D_MULTISAMPLE_ARRAY, iimage2DMSArray)
GLSLMAP(GL_INT_IMAGE_BUFFER, iimageBuffer)
GLSLMAP(GL_INT_IMAGE_2D_RECT, iimage2DRect)
GLSLMAP(GL_UNSIGNED_INT_IMAGE_1D, uimage1D)
GLSLMAP(GL_UNSIGNED_INT_IMAGE_2D, uimage2D)
GLSLMAP(GL_UNSIGNED_INT_IMAGE_3D, uimage3D)
GLSLMAP(GL_UNSIGNED_INT_IMAGE_CUBE, uimageCube)
GLSLMAP(GL_UNSIGNED_INT_IMAGE_1D_ARRAY, uimage1DArray)
GLSLMAP(GL_UNSIGNED_INT_IMAGE_2D_ARRAY, uimage2DArray)
GLSLMAP(GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE, uimage2DMS)
GLSLMAP(GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_ARRAY, uimage2DMSArray)
GLSLMAP(GL_UNSIGNED_INT_IMAGE_BUFFER, uimageBuffer)
GLSLMAP(GL_UNSIGNED_INT_IMAGE_2D_RECT, uimage2DRect)
GLSLMAP(GL_INT64_VEC2_ARB, i64vec2)
GLSLMAP(GL_INT64_VEC3_ARB, i64vec3)
GLSLMAP(GL_INT64_VEC4_ARB, i64vec4)
GLSLMAP(GL_UNSIGNED_INT64_VEC2_ARB, u64vec2)
GLSLMAP(GL_UNSIGNED_INT64_VEC3_ARB, u64vec3)
GLSLMAP(GL_UNSIGNED_INT64_VEC4_ARB, u64vec4)
#undef GLSLMAP
#endif


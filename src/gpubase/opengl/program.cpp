#include "program.h"
#include "glapi.h"
#include "util.h"
#include "macros.h"
#include "log.h"
#include <assert.h>
#include "renderdefines.h"

// (formatstring, ...)
#define errorcb(...) do { if(callbacks && callbacks->error) callbacks->error(callbacks->user, __VA_ARGS__); else logerror(__VA_ARGS__); } while(0)

enum CompileError
{
    C_GOOD = 0,
    C_WARNING = 1,
    C_ERROR = 2,
};

unsigned s_proctype2gltype[ShaderType::_MAXTYPE] =
{
    GL_VERTEX_SHADER,
    GL_FRAGMENT_SHADER,
    GL_COMPUTE_SHADER
};

unsigned s_proctype2glbits[ShaderType::_MAXTYPE] =
{
    GL_VERTEX_SHADER_BIT,
    GL_FRAGMENT_SHADER_BIT,
    GL_COMPUTE_SHADER_BIT
};

static GLSL::Type gettype(unsigned gltype)
{
    switch(gltype)
    {
        case 0: return GLSL::unknown;
#define PRIMMAP(impl, _, ret) case impl: return GLSL::ret;
#define GLSLMAP(impl, ret) case impl: return GLSL::ret;
#include "glslmap.h"
    }
    assert(false);
    return GLSL::Type(-1);
}

static bool check64()
{
    if(ENABLE_ARB_gpu_shader_int64)
        return true;
    logerror("Warning: GPU does not support 64 bit uniforms!");
    return false;
}

bool ProgramUniform::query(unsigned program_, unsigned index)
{
    assert(!(GLSL::isSampler(glsltype) || GLSL::isImage(glsltype)));

    program = program_;
    int size;
    glGetActiveUniform(program_, index, sizeof(name), NULL, &size, &impltype, &name[0]);
    location = glGetUniformLocation(program_, &name[0]);
    if(location == unsigned(-1))
    {
        glsltype = GLSL::unknown;
        return false;
    }
    glsltype = gettype(impltype);

    // get initial value
    switch(impltype)
    {
        case GL_FLOAT:
        case GL_FLOAT_VEC2:
        case GL_FLOAT_VEC3:
        case GL_FLOAT_VEC4:
        case GL_FLOAT_MAT2:
        case GL_FLOAT_MAT2x3:
        case GL_FLOAT_MAT2x4:
        case GL_FLOAT_MAT3:
        case GL_FLOAT_MAT3x2:
        case GL_FLOAT_MAT3x4:
        case GL_FLOAT_MAT4:
        case GL_FLOAT_MAT4x2:
        case GL_FLOAT_MAT4x3:
            glGetUniformfv(program, location, getpAs<float>());
            break;

        case GL_INT:
        case GL_INT_VEC2:
        case GL_INT_VEC3:
        case GL_INT_VEC4:
            glGetUniformiv(program, location, getpAs<int>());
            break;

        case GL_UNSIGNED_INT: 
        case GL_UNSIGNED_INT_VEC2:
        case GL_UNSIGNED_INT_VEC3:
        case GL_UNSIGNED_INT_VEC4:
            glGetUniformuiv(program, location, getpAs<unsigned>());
            break;

        // -- gpu_shader_int64 START --
        case GL_INT64_ARB:
        case GL_INT64_VEC2_ARB:
        case GL_INT64_VEC3_ARB:
        case GL_INT64_VEC4_ARB:
            if(check64())
                glGetUniformi64vARB(program, location, getpAs<int64_t>());
            else
                *getpAs<int64_t>() = 0;
            break;

        case GL_UNSIGNED_INT64_ARB: 
        case GL_UNSIGNED_INT64_VEC2_ARB:
        case GL_UNSIGNED_INT64_VEC3_ARB:
        case GL_UNSIGNED_INT64_VEC4_ARB:
            if(check64())
                glGetUniformui64vARB(program, location, getpAs<uint64_t>());
            else
                *getpAs<uint64_t>() = 0;
            break;
        // -- gpu_shader_int64 END --

        case GL_BOOL:
        case GL_BOOL_VEC2:
        case GL_BOOL_VEC3:
        case GL_BOOL_VEC4:
        {
            int booldata[4] = { 0, 0, 0, 0 };
            glGetUniformiv(program, location, &booldata[0]);
            bool *pb = getpAs<bool>();
            for(unsigned i = 0; i < 4; ++i)
                pb[i] = !!booldata[i];
            break;
        }

        case GL_DOUBLE:
        case GL_DOUBLE_VEC2:
        case GL_DOUBLE_VEC3:
        case GL_DOUBLE_VEC4:
        case GL_DOUBLE_MAT2:
        case GL_DOUBLE_MAT2x3:
        case GL_DOUBLE_MAT2x4:
        case GL_DOUBLE_MAT3:
        case GL_DOUBLE_MAT3x2:
        case GL_DOUBLE_MAT3x4:
        case GL_DOUBLE_MAT4:
        case GL_DOUBLE_MAT4x2:
        case GL_DOUBLE_MAT4x3:
            glGetUniformdv(program, location, getpAs<double>());

        default:
            assert(false);
            memset(_buf, 0, sizeof(_buf));
    }

    return true;
}

void ProgramUniform::uploadRaw(const void *data) const
{
    assert(data);
    if(binding >= 0) // opaque type?
    {
        // update binding point
        binding = *(int*)data;
        assert(binding >= 0);
    }
    const unsigned loc = location;
    const unsigned proc = program;
    assert(proc);
    switch(impltype)
    {
        case GL_FLOAT:          glProgramUniform1fv(proc, loc, 1, (GLfloat*)data); break;
        case GL_FLOAT_VEC2:     glProgramUniform2fv(proc, loc, 1, (GLfloat*)data); break;
        case GL_FLOAT_VEC3:     glProgramUniform3fv(proc, loc, 1, (GLfloat*)data); break;
        case GL_FLOAT_VEC4:     glProgramUniform4fv(proc, loc, 1, (GLfloat*)data); break;
        case GL_INT:            glProgramUniform1iv(proc, loc, 1, (GLint*)data); break;
        case GL_INT_VEC2:       glProgramUniform2iv(proc, loc, 1, (GLint*)data); break;
        case GL_INT_VEC3:       glProgramUniform3iv(proc, loc, 1, (GLint*)data); break;
        case GL_INT_VEC4:       glProgramUniform4iv(proc, loc, 1, (GLint*)data); break;
        case GL_UNSIGNED_INT:     glProgramUniform1uiv(proc, loc, 1, (GLuint*)data); break;
        case GL_UNSIGNED_INT_VEC2:glProgramUniform2uiv(proc, loc, 1, (GLuint*)data); break;
        case GL_UNSIGNED_INT_VEC3:glProgramUniform3uiv(proc, loc, 1, (GLuint*)data); break;
        case GL_UNSIGNED_INT_VEC4:glProgramUniform4uiv(proc, loc, 1, (GLuint*)data); break;
        case GL_FLOAT_MAT2:     glProgramUniformMatrix2fv  (proc, loc, 1, GL_FALSE, (GLfloat*)data); break;
        case GL_FLOAT_MAT2x3:   glProgramUniformMatrix2x3fv(proc, loc, 1, GL_FALSE, (GLfloat*)data); break;
        case GL_FLOAT_MAT2x4:   glProgramUniformMatrix2x4fv(proc, loc, 1, GL_FALSE, (GLfloat*)data); break;
        case GL_FLOAT_MAT3:     glProgramUniformMatrix3fv  (proc, loc, 1, GL_FALSE, (GLfloat*)data); break;
        case GL_FLOAT_MAT3x2:   glProgramUniformMatrix3x2fv(proc, loc, 1, GL_FALSE, (GLfloat*)data); break;
        case GL_FLOAT_MAT3x4:   glProgramUniformMatrix3x4fv(proc, loc, 1, GL_FALSE, (GLfloat*)data); break;
        case GL_FLOAT_MAT4:     glProgramUniformMatrix4fv  (proc, loc, 1, GL_FALSE, (GLfloat*)data); break;
        case GL_FLOAT_MAT4x2:   glProgramUniformMatrix4x2fv(proc, loc, 1, GL_FALSE, (GLfloat*)data); break;
        case GL_FLOAT_MAT4x3:   glProgramUniformMatrix4x3fv(proc, loc, 1, GL_FALSE, (GLfloat*)data); break;
        // 64 BIT START
        case GL_INT64_ARB:              if(check64()) glProgramUniform1i64vARB(proc, loc, 1, (GLint64*)data); break;
        case GL_INT64_VEC2_ARB:         if(check64()) glProgramUniform2i64vARB(proc, loc, 1, (GLint64*)data); break;
        case GL_INT64_VEC3_ARB:         if(check64()) glProgramUniform3i64vARB(proc, loc, 1, (GLint64*)data); break;
        case GL_INT64_VEC4_ARB:         if(check64()) glProgramUniform4i64vARB(proc, loc, 1, (GLint64*)data); break;
        case GL_UNSIGNED_INT64_ARB:     if(check64()) glProgramUniform1ui64vARB(proc, loc, 1, (GLuint64*)data); break;
        case GL_UNSIGNED_INT64_VEC2_ARB:if(check64()) glProgramUniform2ui64vARB(proc, loc, 1, (GLuint64*)data); break;
        case GL_UNSIGNED_INT64_VEC3_ARB:if(check64()) glProgramUniform3ui64vARB(proc, loc, 1, (GLuint64*)data); break;
        case GL_UNSIGNED_INT64_VEC4_ARB:if(check64()) glProgramUniform4ui64vARB(proc, loc, 1, (GLuint64*)data); break;
        // 64 BIT END
        case GL_DOUBLE:          glProgramUniform1dv(proc, loc, 1, (GLdouble*)data); break;
        case GL_DOUBLE_VEC2:     glProgramUniform2dv(proc, loc, 1, (GLdouble*)data); break;
        case GL_DOUBLE_VEC3:     glProgramUniform3dv(proc, loc, 1, (GLdouble*)data); break;
        case GL_DOUBLE_VEC4:     glProgramUniform4dv(proc, loc, 1, (GLdouble*)data); break;
        case GL_DOUBLE_MAT2:     glProgramUniformMatrix2dv  (proc, loc, 1, GL_FALSE, (GLdouble*)data); break;
        case GL_DOUBLE_MAT2x3:   glProgramUniformMatrix2x3dv(proc, loc, 1, GL_FALSE, (GLdouble*)data); break;
        case GL_DOUBLE_MAT2x4:   glProgramUniformMatrix2x4dv(proc, loc, 1, GL_FALSE, (GLdouble*)data); break;
        case GL_DOUBLE_MAT3:     glProgramUniformMatrix3dv  (proc, loc, 1, GL_FALSE, (GLdouble*)data); break;
        case GL_DOUBLE_MAT3x2:   glProgramUniformMatrix3x2dv(proc, loc, 1, GL_FALSE, (GLdouble*)data); break;
        case GL_DOUBLE_MAT3x4:   glProgramUniformMatrix3x4dv(proc, loc, 1, GL_FALSE, (GLdouble*)data); break;
        case GL_DOUBLE_MAT4:     glProgramUniformMatrix4dv  (proc, loc, 1, GL_FALSE, (GLdouble*)data); break;
        case GL_DOUBLE_MAT4x2:   glProgramUniformMatrix4x2dv(proc, loc, 1, GL_FALSE, (GLdouble*)data); break;
        case GL_DOUBLE_MAT4x3:   glProgramUniformMatrix4x3dv(proc, loc, 1, GL_FALSE, (GLdouble*)data); break;
        // bools are a bit annoying... since their size in glsl is 32 bits
#define BDAT(i) (!!((char*)data)[i])
        case GL_BOOL:
            glProgramUniform1i(proc, loc, BDAT(0));
            break;
        case GL_BOOL_VEC2:
            glProgramUniform2i(proc, loc, BDAT(0), BDAT(1));
            break;
        case GL_BOOL_VEC3:
            glProgramUniform3i(proc, loc, BDAT(0), BDAT(1), BDAT(2));
            break;
        case GL_BOOL_VEC4:
            glProgramUniform4i(proc, loc, BDAT(0), BDAT(1), BDAT(2), BDAT(3));
            break;

            // FIXME: extend this list as necessary
        default:
            logerror("ProgramUniform::uploadRaw(): Unsupported GLSL type: %s", GLSL::getName(glsltype));
            assert(false);
    }
}

// -----------------------------------------------------------------------

bool _ProgramTexture::query(unsigned program_, unsigned index)
{
    program = program_;
    int size;
    glGetActiveUniform(program_, index, sizeof(name), NULL, &size, &impltype, &name[0]);
    location = glGetUniformLocation(program_, &name[0]);
    if(location == unsigned(-1))
    {
        glsltype = GLSL::unknown;
        return false;
    }
    glsltype = gettype(impltype);
    assert(GLSL::isImage(glsltype) || GLSL::isSampler(glsltype));

    glGetUniformiv(program, location, &binding);
    assert(binding >= 0);

    return true;
}

void _ProgramTexture::_setBinding(unsigned unit)
{
    binding = unit;
    glProgramUniform1i(program, location, unit);
}

// -----------------------------------------------------------------------

void ProgramSampler::set(const TextureAny * tex) const
{
    texref = tex;
    if(ENABLE_ARB_bindless_texture)
    {
        Handle h = 0;
        if(tex)
        {
            h = tex->getTexHandle();
        }
        if(h != _h)
        {
            glProgramUniformHandleui64ARB(program, location, h);
            _h = h;
        }
    }
}

void ProgramSampler::bind() const
{
    if(!texref)
        return;
    if(ENABLE_ARB_bindless_texture)
    {
        texref->makeTexResident();
    }
    else
    {
        //DEBUG_LOG("ProgramSampler::bind() \"%s\"", name);
        assert(binding >= 0);
        texref->bindUnit(binding);
    }
}

void ProgramSampler::unbind() const
{
    if(!texref)
        return;
    if(ENABLE_ARB_bindless_texture)
    {
        texref->makeTexNotResident();
    }
    else
    {
        glBindTextures(binding, 1, NULL);
        /*if(!!texref)
            texref->unbindImage(binding);*/
    }
}

void ProgramSampler::refresh()
{
    _h = -1; // force update
    set(texref.content());
}


// -----------------------------------------------------------------------

void ProgramImage::set(TextureAny * tex, _TextureBase::Access a) const
{
    texref = tex;
    _a = a;
    if(ENABLE_ARB_bindless_texture)
    {
        Handle h = 0;
        if(tex)
        {
            h = tex->getImageHandle();
            _a = a;
        }
        if(h != _h)
        {
            glProgramUniformHandleui64ARB(program, location, h);
            _h = h;
        }
    }
}

void ProgramImage::bind() const
{
    if(!texref)
        return;
    if(ENABLE_ARB_bindless_texture)
    {
        texref->makeImageResident(_a);
    }
    else
    {
        //DEBUG_LOG("ProgramImage::bind() \"%s\"", name);
        assert(binding >= 0);
        texref->bindImageUnit(binding, _a);
    }
}

void ProgramImage::unbind() const
{
    if(!texref)
        return;
    if(ENABLE_ARB_bindless_texture)
    {
        texref->makeImageNotResident();
    }
    else
    {
        glBindImageTexture(binding, 0, 0, GL_TRUE, 0, GL_READ_ONLY, GL_RGBA32F);
        /*if(!!texref)
            texref->unbind();*/
    }
}

void ProgramImage::refresh()
{
    _h = -1; // force update
    set(texref.content(), _a);
}



// -----------------------------------------------------------------------

void ProgramBuffer::query(unsigned program, unsigned index)
{
    static const unsigned prop[] = {GL_BUFFER_BINDING};
    static const unsigned N = Countof(prop);
    int values[N];
    glGetProgramResourceiv(program, GL_SHADER_STORAGE_BLOCK, index, N, prop, N, NULL, values);
    glGetProgramResourceName(program, GL_SHADER_STORAGE_BLOCK, index, sizeof(name), NULL, &name[0]);
    binding = values[0];


    glsltype = GLSL::buffer;

    this->program = program;
    this->blockidx = index;
}

void ProgramBuffer::bind() const
{
    if(!!bufref)
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, binding, bufref->id());
}

void ProgramBuffer::unbind() const
{
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, binding, 0);
}

void ProgramBuffer::_setBinding(unsigned unit)
{
    glShaderStorageBlockBinding(program, blockidx, unit);
    binding = unit;
}

// -----------------------------------------------------------------------


void ProgramInput::query(unsigned program, unsigned index)
{
    static const unsigned prop[] = {GL_TYPE, GL_LOCATION, };
    static const unsigned N = Countof(prop);
    int values[N];
    glGetProgramResourceiv(program, GL_PROGRAM_INPUT, index, N, prop, N, NULL, values);
    glGetProgramResourceName(program, GL_PROGRAM_INPUT, index, sizeof(name), NULL, &name[0]);
    glsltype = gettype(values[0]);
    binding = values[1];
}

// TODO: allow binding inputs dynamically?

// -----------------------------------------------------------------------

static CompileError checkshader(unsigned id, unsigned status, const ShaderLoadCallbacks *callbacks)
{
    char info[2048];
    info[0] = 0;
    glGetShaderInfoLog(id, 2048, NULL, info);
    CompileError ret = C_GOOD;
    if(info[0])
    {
        errorcb("%s", info);
        ret = C_WARNING;
    }
    GLint ok;
    glGetShaderiv(id, status, &ok);
    return ok ? ret : C_ERROR;
}

ShaderObject::~ShaderObject()
{
    if(_id)
        glDeleteShader(_id);
}

bool ShaderObject::_loadCode(const char *code, const ShaderLoadCallbacks *callbacks)
{
    compile_assert(ShaderType::_MAXTYPE == Countof(s_proctype2gltype));

    unsigned id;
    std::string finalsrc;
    {
        const char *strp = code;
        if(callbacks && callbacks->patch)
        {
            finalsrc = callbacks->patch(code, _type, callbacks->user);
            strp = finalsrc.c_str();
        }

        id = glCreateShader(s_proctype2gltype[_type]);
        glShaderSource(id, 1, &strp, NULL);
    }

    glCompileShader(id);

    CompileError ce = checkshader(id, GL_COMPILE_STATUS, callbacks);
    if(ce != C_GOOD)
        if(const char *fn = getFileName())
            errorcb("... shader source file: %s", fn);
    if(ce == C_ERROR)
    {
        glDeleteShader(id);
        return false;
    }

    // --- All good ---
    if(_id)
        glDeleteShader(_id);
    _id = id;
    _finalsource.swap(finalsrc);
    ++_counter;
    return true;
}

const char * ShaderObject::getInternalSource() const
{
    return _finalsource.c_str();
}

void ShaderObject::onDeviceReset(bool init)
{
    _id = 0;
    _counter = 0;

    if(init)
    {
        _id = glCreateShader(s_proctype2gltype[_type]);
        const char * psrc = _finalsource.c_str();
        glShaderSource(_id, 1, &psrc, NULL);
        glCompileShader(_id);
        ++_counter;
    }
}

// -----------------------------------------------------------------------

static CompileError checkprogram(unsigned proc, unsigned status, const ShaderLoadCallbacks *callbacks)
{
    char info[2048];
    info[0] = 0;
    glGetProgramInfoLog(proc, 2048, NULL, info);
    CompileError ret = C_GOOD;
    if(info[0])
    {
        errorcb(info);
        ret = C_WARNING;
    }
    GLint ok;
    glGetProgramiv(proc, status, &ok);
    return ok ? ret : C_ERROR;
}

ShaderProgram::~ShaderProgram()
{
    if(_id)
        glDeleteProgram(_id);
}

void ShaderProgram::_queryUniforms()
{
    vv.uniforms.clear();
    vv.images.clear();
    vv.samplers.clear();
    const unsigned N = getNumUniforms();
    if(!N)
        return;

    std::vector<unsigned> indices(N);
    std::vector<int> impltypes(N);
    for(unsigned i = 0; i < N; ++i)
        indices[i] = i;
    glGetActiveUniformsiv(_id, N, &indices[0], GL_UNIFORM_TYPE, &impltypes[0]);

    for(unsigned i = 0; i < N; ++i)
    {
        const GLSL::Type ty = gettype(impltypes[i]);
        if(GLSL::isSampler(ty))
        {
            vv.samplers.resize(vv.samplers.size() + 1);
            vv.samplers.back().query(_id, i);
        }
        else if(GLSL::isImage(ty))
        {
            vv.images.resize(vv.images.size() + 1);
            vv.images.back().query(_id, i);
        }
        else // Any other type is just a generic uniform
        {
            vv.uniforms.resize(vv.uniforms.size() + 1);
            vv.uniforms.back().query(_id, i);
        }
    }
}

bool ShaderProgram::_link(const ShaderLoadCallbacks * callbacks)
{
    const unsigned proc = glCreateProgram();

    for(unsigned i = 0; i < ShaderType::_MAXTYPE; ++i)
    if(unsigned id = _shaders[i]->id())
        glAttachShader(proc, id);

    for(unsigned i = 0; i < KA_MAX; ++i)
        glBindAttribLocation(proc, i, KnownAttribNames[i]);

    glLinkProgram(proc);
    CompileError ce = checkprogram(proc, GL_LINK_STATUS, callbacks);

    if(ce != C_GOOD)
    {
        errorcb("... when linking the following shaders:");
        for(unsigned i = 0; i < ShaderType::_MAXTYPE; ++i)
            if(ShaderObject *sh = _shaders[i].content())
            {
                if(const char *fn = sh->getFileName())
                    errorcb("File: %s", fn);
                else
                    errorcb("(Generated source code)");
            }
    }

    if(ce == C_ERROR)
    {
        glDeleteProgram(proc);
        return false;
    }

    if(_id)
        glDeleteProgram(_id);
    _id = proc;
    ++_counter;
    return true;
}

template<typename V>
static void bindall(const V& v)
{
    const size_t N = v.size();
    for(size_t i = 0; i < N; ++i)
        v[i].bind();
}

template<typename V>
static void unbindall(const V& v)
{
    const size_t N = v.size();
    for(size_t i = 0; i < N; ++i)
        v[i].unbind();
}

void ShaderProgram::bind() const
{
    const int id = this->id();
    glUseProgram(id);

    if(id)
    {
        bindall(vv.buffers);
        bindall(vv.images);
        bindall(vv.samplers);
    }
}

void ShaderProgram::unbind() const
{
    glUseProgram(0);

    if(id())
    {
        unbindall(vv.buffers);
        unbindall(vv.images);
        unbindall(vv.samplers);
    }
}

// expected that caller caches this somewhere
void ShaderProgram::getComputeWorkGroupSize(unsigned * pOut) const
{
    glGetProgramiv(id(), GL_COMPUTE_WORK_GROUP_SIZE, (int*)pOut);
}

void ShaderProgram::dispatchComputeSingle(unsigned x, unsigned y, unsigned z) const
{
    dispatchComputeMultiBegin();
    _dispatchCompute(x, y, z);
    dispatchComputeMultiEnd();
}

void ShaderProgram::dispatchComputeMultiBegin() const
{
    assert(_shaders[ShaderType::COMPUTE]);
    bind();
}

void ShaderProgram::_dispatchCompute(unsigned x, unsigned y, unsigned z) const
{
    DEBUG_LOG("ShaderProgram[%u]::dispatchCompute(%u, %u, %u)", id(), x, y, z);
    glDispatchCompute(x, y, z);

    // HACK: Don't spam the compute queue too much
    // Seems to make certain drivers (nvidia? Why is it always nvidia?!) more stable
    // Note: Tried to batch a number of dispatches together, still prone to crashing with heavy workloads.
    // For some reason, gently poking the driver to please finish working whenever it deems appropriate fixes the issue.
    // (This could also be a glFinish(), but in case the driver is capable of executing more than one compute block at a time
    // it can do so when glFlush() is used. With glFinish(), work is completely serialized, and also even less prone to blowing up.)
    glFlush();
}

void ShaderProgram::dispatchComputeMultiEnd() const
{
    unbind();
    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT | GL_SHADER_IMAGE_ACCESS_BARRIER_BIT | GL_TEXTURE_FETCH_BARRIER_BIT | GL_BUFFER_UPDATE_BARRIER_BIT);
}



unsigned ShaderProgram::getNumUniforms() const
{
    int n = 0;
    //glGetProgramiv(_proc, GL_ACTIVE_UNIFORMS, &n);
    glGetProgramInterfaceiv(_id, GL_UNIFORM, GL_ACTIVE_RESOURCES, &n);
    return n;
}

unsigned ShaderProgram::getNumBuffers() const
{
    int n = 0;
    glGetProgramInterfaceiv(_id, GL_SHADER_STORAGE_BLOCK, GL_ACTIVE_RESOURCES, &n);
    return n;
}

unsigned ShaderProgram::getNumAttribs() const
{
    int n = 0;
    //glGetProgramiv(_proc, GL_ACTIVE_ATTRIBUTES, &n);
    glGetProgramInterfaceiv(_id, GL_PROGRAM_INPUT, GL_ACTIVE_RESOURCES, &n);
    return n;
}

void ShaderProgram::onDeviceReset(bool init)
{
    _id = 0;
    _counter = 0;
    _clearLinkCache();
    vv._setprogid(0);
    // Keep uniform data etc intact so they can still be retrieved

    if(init)
    {
        _link(NULL);
        vv._setprogid(_id);
    }
}

// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
/*
ProgramPipeline::ProgramPipeline()
{
    glGenProgramPipelines(1, &_pipeline);
    memset(_cachedIDs, 0, sizeof(_cachedIDs));
    memset(_cachedCounters, 0, sizeof(_cachedCounters));
}

ProgramPipeline::~ProgramPipeline()
{
    glDeleteProgramPipelines(1, &_pipeline);
}

void ProgramPipeline::bind()
{
    const unsigned pipeline = id();
    if(pipeline)
    {
        compile_assert(ShaderProgram::_MAXTYPE == Countof(s_proctype2glbits));
        for(unsigned i = 0; i < ShaderProgram::_MAXTYPE; ++i)
        {
            const unsigned id = _progs[i]->id();
            const unsigned counter = id ? _progs[i]->_getCounter() : 0;
            if(_cachedIDs[i] != id || _cachedCounters[i] != counter)
            {
                glUseProgramStages(pipeline, s_proctype2glbits[i], id);
                _cachedIDs[i] = id;
                _cachedCounters[i] = counter;
            }
        }
        glUseProgram(0);
    }

    glBindProgramPipeline(pipeline);
}
*/

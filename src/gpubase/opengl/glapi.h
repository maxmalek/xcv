#ifndef OPENGL_API_H
#define OPENGL_API_H

// Prevent glcorearb.h from pulling in windows.h and the pollution it entails
#if defined(_WIN32) && !defined(APIENTRY)
#define APIENTRY __stdcall
#endif

#if !defined(APIENTRY)
#define APIENTRY
#endif

#include "glcorearb.h" // From https://khronos.org/registry/OpenGL/api/GL/glcorearb.h
#include "glmyext.h"

// In case windows.h was included and vomited all over the place...
#ifdef WINAPI
#error "Windows.h was included! Sadness!"
#endif

#define GL_PTR(pty, fn)                  extern pty fn;
#define GL_GROUP_PTR(g, pty, fn)         GL_PTR(pty, fn)
#define GL_GROUP(g)                      extern bool ENABLE_ ## g;
#include "glstubs.h"

struct GLApiSymbols
{
#define GL_PTR(pty, fn)                  pty fn;
#define GL_GROUP_PTR(g, pty, fn)         GL_PTR(pty, fn)
#define GL_GROUP(g)
#include "glstubs.h"
};

struct GLExtensionContext
{
#define GL_PTR(pty, fn)
#define GL_GROUP_PTR(g, pty, fn)
#define GL_GROUP(g)                      bool HAS_ ## g;
#include "glstubs.h"
};

void glBreakpoint(const char *s);

namespace OpenGLAPI
{
    bool LoadSymbols(GLApiSymbols *syms, GLExtensionContext *ext, bool failsafe);
    void LoadSymbolsNoError(GLApiSymbols *syms, GLExtensionContext *ext);
    void ApplySymbols(const GLApiSymbols *syms, const GLExtensionContext *ext);
    void ClearSymbols();
};

#endif

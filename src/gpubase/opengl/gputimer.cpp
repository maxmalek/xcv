#include "gputimer.h"
#include "glapi.h"
#include <assert.h>

_GPUQueryObject::_GPUQueryObject()
: DeviceObject(0, 0)
{
}

_GPUQueryObject::~_GPUQueryObject()
{
    deinit();
}

void _GPUQueryObject::onDeviceReset(bool init_)
{
    forget();
    if(init_)
        init();
}

void _GPUQueryObject::forget()
{
    _id = 0;
}

bool _GPUQueryObject::wasInit() const
{
    return !!_id;
}

void _GPUQueryObject::deinit()
{
    if(_id)
    {
        glDeleteQueries(1, &_id);
        _id = 0;
    }
}

bool _GPUQueryObject::completed() const
{
    assert(_id);
    int avail = 0; 
    glGetQueryObjectiv(_id, GL_QUERY_RESULT_AVAILABLE, &avail);
    return !!avail;
}

uint64_t _GPUQueryObject::getValue() const
{
    assert(_id);
    uint64_t ns = 0;
    glGetQueryObjectui64v(_id, GL_QUERY_RESULT, &ns);
    return ns;
}

GPUTimer::GPUTimer()
{
    init();
}

GPUTimer::GPUTimer(_noinit)
{
}

void GPUTimer::init()
{
    assert(!_id);
    glCreateQueries(GL_TIMESTAMP, 1, &_id);
    assert(_id);
}

void GPUTimer::capture() const
{
    assert(_id);
    glQueryCounter(_id, GL_TIMESTAMP);
}


GPUTimeSpan::GPUTimeSpan() : GPUTimer(), _capturing(false)
{
}

GPUTimeSpan::GPUTimeSpan(_noinit) : GPUTimer(noinit), _capturing(false)
{
}

void GPUTimeSpan::begin() const
{
    assert(!_capturing);
    capture();
    _capturing = true;
}

void GPUTimeSpan::end() const
{
    assert(_capturing);
    _starttime = getValue();
    capture();
    _capturing = false;
}

uint64_t GPUTimeSpan::getDiff() const
{
    assert(!_capturing);
    uint64_t endtime = getValue();
    return endtime - _starttime;
}

void GPUTimeSpan::init()
{
    assert(!_capturing);
    GPUTimer::init();
}

#include <SDL.h>
#include "util.h"
#include "glapi.h"
#include "platform.h"
#include <string>
#include "log.h"


#define GL_PTR(pty, fn) pty fn = NULL;
#define GL_GROUP_PTR(g, pty, fn) GL_PTR(pty, fn)
#define GL_GROUP(g) bool ENABLE_ ## g;
#include "glstubs.h"

static bool hasext(const char *e)
{
    const bool has = !!SDL_GL_ExtensionSupported(e);
    DEBUG_LOG("Has GL Extension [%s] = %u", e, (unsigned)has);
    return has;
}

static bool enableExt(const char *e, bool failsafe)
{
    if(!failsafe)
        return true;

    static const char *greylist[] =
    {
        "ARB_bindless_texture",
        NULL
    };

    for(const char **t = &greylist[0]; *t; ++t)
        if(!strcmp(e, *t))
        {
            logdebug("Failsafe: Disabling GL extension [%s]", e);
            return false;
        }

    return true;
}

template<typename T> bool getsym(const char *sym, T **ptr)
{
    return !!(*ptr = (T*)SDL_GL_GetProcAddress(sym));
}

template<typename T> bool reqsym(const char *sym, T **ptr)
{
    if(!getsym(sym, ptr))
    {
        logerror("Failed to get required GL sym: %s", sym);
        return false;
    }
    //DEBUG_LOG("%s -> %p\n", sym, *ptr);
    return true;
}

template<typename T> void groupsym(const char *sym, const char *extname, bool& group, T **ptr)
{
    if(!group)
        *ptr = NULL;
    bool ok = getsym(sym, ptr);
    if(!ok)
    {
        group = false;
        logdebug("Failed to get optional GL sym: %s, disabling extension [%s]", sym, extname);
    }
}

// Populate symbol table
static bool lookup_all_glsyms(GLApiSymbols *syms, GLExtensionContext *ext, bool failsafe)
{
    bool ok = true;
#define GL_PTR(_, fn)                  ok = reqsym(#fn, &syms->fn) && ok;
#define GL_GROUP(g)                    ext->HAS_ ## g = hasext("GL_" #g) && enableExt(#g, failsafe);
#define GL_GROUP_PTR(g, _, fn)         groupsym(#fn, #g, ext->HAS_ ## g, &syms->fn);
#include "glstubs.h"
    return ok;
}

// Copy symbol table to the global pointers
static void apply_all_glsyms(const GLApiSymbols *syms, const GLExtensionContext *ext)
{
#define GL_PTR(_, fn) fn = syms->fn;
#define GL_GROUP_PTR(g, pty, fn) GL_PTR(pty, fn)
#define GL_GROUP(g) ENABLE_ ## g  = ext->HAS_ ## g;
#include "glstubs.h"
}

void glBreakpoint(const char *s)
{
    glDebugMessageInsert(GL_DEBUG_SOURCE_APPLICATION, GL_DEBUG_TYPE_OTHER, 0xbbbb, GL_DEBUG_SEVERITY_HIGH, -1, s);
}

namespace OpenGLAPI {

void LoadSymbolsNoError(GLApiSymbols *syms, GLExtensionContext *ext)
{
    lookup_all_glsyms(syms, ext, false);
}

bool LoadSymbols(GLApiSymbols *syms, GLExtensionContext *ext, bool failsafe)
{
    DEBUG_LOG("OpenGLAPI::LoadSymbols() [sizeof(GLApiSymbols) = %u]", (unsigned)sizeof(GLApiSymbols));
    bool ok = lookup_all_glsyms(syms, ext, failsafe);

    const char *ver = (const char*)syms->glGetString(GL_VERSION);
    const char *ven = (const char*)syms->glGetString(GL_VENDOR);

    static bool s_shownOnce = false;
    if(!s_shownOnce)
    {
        s_shownOnce = true;

        loginfo("GL version:   %s", ver);
        loginfo("GL vendor:    %s", ven);
        const char *ren = (const char*)syms->glGetString(GL_RENDERER);
        loginfo("GL renderer:  %s", ren);
        const char *glsl = (const char*)syms->glGetString(GL_SHADING_LANGUAGE_VERSION);
        loginfo("GLSL version: %s", glsl);

        #define GL_PTR(_, fn)
        #define GL_GROUP_PTR(g, pty, fn)
        #define GL_GROUP(g) logdebug("GL extension: %s = %s", #g, ext->HAS_ ## g ? "enabled" : "unsupported");
        #include "glstubs.h"
    }

    if(!ok)
    {
        std::string msg = "Failed to obtain required OpenGL symbols.\nGL driver [";
        msg += ven;
        msg += "] reports version [";
        msg += ver;
        msg += "], need at least 4.5.";
        logerrorbox("%s", msg.c_str());
    }
    return ok;
}

void ApplySymbols(const GLApiSymbols *syms, const GLExtensionContext *ext)
{
    apply_all_glsyms(syms, ext);
}


void ClearSymbols()
{
    DEBUG_LOG("OpenGLAPI::ClearSymbols()");
    #define GL_PTR(pty, fn) fn = NULL;
    #define GL_GROUP_PTR(g, pty, fn) GL_PTR(pty, fn)
    #define GL_GROUP(g) ENABLE_ ## g = false;
    #include "glstubs.h"
}

}; // end namespace OpenGLAPI

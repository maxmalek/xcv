#pragma once

#include "meshdata.h"
#include "gpubuffer.h"
#include "renderdefines.h"
#include "perstateholder.h"
#include "deviceobject.h"

class Mesh : protected DeviceObjectBase, public ScriptableT<Mesh>
{
public:
    static const ScriptDef scriptdef;

    Mesh();
    virtual ~Mesh();
    void load(const MeshData&);
    void clear();
    void render(CullMethod cull, DepthTest dtest, VertexRenderMode vr, DepthWrite dw) const;

private:
    GPUBufRef vbo;
    unsigned iboType;

    struct Offs
    {
        size_t vert, col, norm, uv, useridx, idx, multi; // also memory order in vbo
    } n, o, b; // number, offset, bytes

    mutable PerStateHolder<unsigned, unsigned> vaos;

    unsigned _createVAO() const;
    unsigned _getVAO() const;


    // Inherited via DeviceObject
    virtual void onDeviceReset(bool init);

};

typedef CountedPtr<Mesh> MeshRef;

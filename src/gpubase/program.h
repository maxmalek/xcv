#pragma once

#include <vector>
#include <string>
#include "refcounted.h"
#include "glsl.h"
#include "texture.h"
#include "gpubuffer.h"
#include "deviceobject.h"


class ShaderDefines;
class ShaderProgram;

namespace ShaderType
{
    enum Enum
    {
        VERTEX,
        FRAGMENT,
        COMPUTE,
        _MAXTYPE
    };

    static const char * const Names[] =
    {
        "vert",
        "frag",
        "comp"
    };
}

struct ShaderLoadCallbacks
{
    typedef void (*Error)(void *ud, const char *msg, ...);
    typedef std::string (*PatchCode)(const char *code, ShaderType::Enum type, void *ud);
    void *user;
    Error error;
    PatchCode patch;
};


class ProgramVariable
{
    friend class ShaderProgram;
public:
    typedef _TextureBase::Handle Handle;
    enum Type
    {
        UNIFORM,
        BUFFER,
        ATTRIB,

        // Artifical uniform types -> better type safety
        SAMPLER, // GLSL::sampler*
        IMAGE, // GLSL::image*

        _MAXTYPE
    };
    static const char* var_type_name[];

    inline const char *getName() const { return &name[0]; }
    inline int getBinding() const { return binding; }
    GLSL::Type getType() const { return glsltype; };
    Type getProgramVariableType() const { return _vartype; }

    static bool lessname(const ProgramVariable& a, const char *bname);
    static bool lessnameptr(const ProgramVariable *a, const char *bname);
    static bool lessptr(const ProgramVariable *a, const ProgramVariable *b);
    bool operator<(const ProgramVariable& b) const;
    inline bool operator==(const char *n) const;
    bool accepts(GLSL::Type) const;
protected:
    ProgramVariable(Type t) : _vartype(t), glsltype(GLSL::unknown), program(0), binding(-1) { name[0] = 0; }
    Type _vartype;
    unsigned program; // set in each derived class' query()
    mutable int binding;
    GLSL::Type glsltype;
    char name[32];
};


class ProgramBuffer : public ProgramVariable
{
    friend class ShaderProgram;
public:
    typedef GPUBufferRaw ObjectType;

    inline ProgramBuffer() : ProgramVariable(BUFFER), blockidx(-1) {}

    void set(GPUBufRef& b) const { bufref = b; }
    void set(GPUBufferRaw *b) const { bufref = b; }
    GPUBufferRaw *get() const { return bufref.content(); }

    void refresh() { _setBinding(binding); }

    bool copyfrom(const ProgramBuffer *from) const; // can be NULL; returns true if copied

    // backend-specific
    void query(unsigned program, unsigned index);

    void _setBinding(unsigned b); // for autobind
    void bind() const;
    void unbind() const;

    inline unsigned _getBlockIdx() const { return blockidx; }
private:
    mutable GPUBufRef bufref;
    unsigned blockidx;
};

class ProgramInput : public ProgramVariable
{
public:
    inline ProgramInput() : ProgramVariable(ATTRIB) {}

    // backend-specific
    void query(unsigned program, unsigned index);
};

class ProgramUniform : public ProgramVariable
{
public:
    // shared / impl agnostic
    inline ProgramUniform() : ProgramVariable(UNIFORM), location(-1), impltype(-1) {}
    unsigned getLoc() const { return location; }

    inline void *getp() const { return &_buf[0]; }
    template<typename T> inline T *getpAs() const { return reinterpret_cast<T*>(getp()); }
    void setUnsafe(const void *data) const;

    uint64_t setImageHandle(TextureAny *tex, TextureAny::Access a) const;
    uint64_t setSamplerHandle(TextureAny *tex) const;
    void clearHandle() const;
    Refcounted<REFCOUNT_NORMAL> *getObj() const { return holder.content(); }

    bool copyfrom(const ProgramUniform *from) const; // can be NULL; returns true if copied

    // backend-specific
    bool query(unsigned program, unsigned index);
    void uploadRaw(const void *data) const;

    template<typename T>
    void set(const T& v) const
    {
        assert(accepts(GLSL::TypeFor<T>::type));
        setUnsafe(&v);
    }
    void refresh() const
    {
        uploadRaw((const void*)&_buf[0]);
    }
private:
    void _setUnsafe(const void *data) const;
    unsigned location;
    unsigned impltype; // backend-specific
    mutable char _buf[GLSL::MAX_TYPE_SIZE_BYTES];
    mutable CountedPtrAny holder;
};

// Internal class for ProgramSampler and ProgramImage
class _ProgramTexture : public ProgramVariable
{
protected:
    inline _ProgramTexture(Type ty) : ProgramVariable(ty), location(-1), impltype(-1), _h(0) {}
    void _refresh() { _setBinding(binding); }
public:
    unsigned getLoc() const { return location; }

     // backend-specific
    bool query(unsigned program, unsigned index);

    void _setBinding(unsigned unit); // for autobind

protected:
    unsigned location;
    unsigned impltype; // backend-specific
    mutable _TextureBase::Handle _h;
};

class ProgramImage : public _ProgramTexture
{
public:
    typedef TextureAny ObjectType;
    inline ProgramImage() : _ProgramTexture(IMAGE), _a(_TextureBase::READONLY) {}
    void set(TexAnyRef& tex, _TextureBase::Access a) const { set(tex.content(), a); }
    void set(TextureAny *tex, _TextureBase::Access a) const;
    TextureAny *get() const { return texref.content(); }
    bool copyfrom(const ProgramImage *from) const; // can be NULL; returns true if copied
    void bind() const;
    void unbind() const;
    void refresh();
private:
    mutable _TextureBase::Access _a;
    mutable TexAnyRef texref;
};

class ProgramSampler : public _ProgramTexture
{
public:
    typedef TextureAny ObjectType;
    inline ProgramSampler() : _ProgramTexture(SAMPLER) {}
    void set(const TexAnyRef& tex) const { set(tex.content()); }
    void set(const TexAnyCRef& tex) const { set(tex.content()); }
    void set(const TextureAny *tex) const;
    const TextureAny *get() const { return texref.content(); }
    bool copyfrom(const ProgramSampler *from) const; // can be NULL; returns true if copied
    void bind() const;
    void unbind() const;
    void refresh();
private:
    mutable TexAnyCRef texref;
};


class ShaderObject : public DeviceObject, public Refcounted<REFCOUNT_NORMAL>
{
public:
    void setCode(const char *code);
    void setFileName(const char *filename);
    bool load(const ShaderLoadCallbacks *callbacks = NULL);
    unsigned _getCounter() const { return _counter; }
    unsigned getType() const { return _type; }
    const char *getFileName() const;
    const char *getInternalSource() const;

    // backend-specific
    ShaderObject(ShaderType::Enum ty);
    virtual ~ShaderObject();
    bool _loadCode(const char *code, const ShaderLoadCallbacks *callbacks);

private:
    const ShaderType::Enum _type;
    unsigned _counter;
    bool _fromfile;
    std::string _source; // file name or code string
    std::string _finalsource; // processed source string as passed into the shader compiler. only set if shader compiled.

    // Inherited via DeviceObject
    virtual void onDeviceReset(bool init);
};

typedef CountedPtr<ShaderObject> ShaderObjRef;

struct ShaderVar
{
    const char * const name;
    const void * const data;
};

struct BufferVar
{
    const char * const name;
    GPUBufRef& buf;
};

struct SafeDataDef
{
    const GLSL::Type glsltype;
    const void * const ptr;
};

template<typename T>
inline static SafeDataDef as_uniform(const T& v)
{
    SafeDataDef def = { GLSL::TypeFor<T>::type, &v };
    return def;
}

struct SafeShaderVar
{
    const char *name;
    SafeDataDef data;
};

template<typename T>
inline static SafeShaderVar as_uniform(const char *name, const T& v)
{
    SafeDataDef def = { GLSL::TypeFor<T>::type, &v };
    SafeShaderVar sv = { name, def };
    return sv;
}

#define AS_UNIFORM(v) as_uniform(#v, v)


class ShaderProgram : public DeviceObject, public Refcounted<REFCOUNT_NORMAL>
{
public:
    // shared / impl agnostic
    ShaderProgram();
    void attach(const ShaderObjRef& ref);
    void detach(ShaderType::Enum ty);
    bool link(const ShaderLoadCallbacks *callbacks = NULL);
    unsigned _getCounter() const { return _counter; }

    template<typename T>
    inline bool setUniform(const char *name, const T& v) const
    {
        return setUniform(name, &v, GLSL::TypeFor<T>::type);
    }

    bool setUniformUnsafe(const char *name, const void *data) const; // can be called on NULL pointer
    void setUniformsUnsafe(const ShaderVar *sv, size_t n) const; // can be called on NULL pointer
    template <size_t N> void setUniformsUnsafe(const ShaderVar (&sv)[N]) const { setUniformsUnsafe(&sv[0], N); }

    bool setUniform(const char *name, const void *data, GLSL::Type ty) const; // can be called on NULL pointer
    void setUniforms(const SafeShaderVar *sv, size_t n) const; // can be called on NULL pointer
    template <size_t N> void setUniforms(const SafeShaderVar (&sv)[N]) const { setUniforms(&sv[0], N); }

    bool setBuffer(const char *name, GPUBufRef& buf) const; // can be called on NULL pointer
    void setBuffers(const BufferVar *sv, size_t n) const; // can be called on NULL pointer
    template <size_t N> void setBuffers(const BufferVar (&sv)[N]) const { setBuffers(&sv[0], N); }

    bool setSampler(const char *name,const  TextureAny * tex) const;
    bool setImage(const char *name, TextureAny *tex, _TextureBase::Access a) const;

    typedef std::vector<ProgramUniform> Uniforms;
    typedef std::vector<ProgramBuffer> Buffers;
    typedef std::vector<ProgramInput> Inputs;
    typedef std::vector<ProgramSampler> Samplers;
    typedef std::vector<ProgramImage> Images;
    typedef std::vector<const ProgramVariable*> AllVarsPtrs;

    const Uniforms& getUniforms() const { return vv.uniforms; }
    const Buffers& getBuffers() const { return vv.buffers; }
    const Inputs& getInputs() const { return vv.inputs; }
    const Images& getImages() const { return vv.images; }
    const Samplers& getSamplers() const { return vv.samplers; }
    const AllVarsPtrs& getAllVars() const { return vv.allvars; }

    const ProgramUniform *lookupUniform(const char *) const;
    const ProgramBuffer *lookupBuffer(const char *) const;
    const ProgramInput *lookupAttrib(const char *) const;
    const ProgramSampler *lookupSampler(const char *) const;
    const ProgramImage *lookupImage(const char *) const;

    const ProgramVariable *lookupAny(const char *) const;

    void clearRefs();
    void copyVariables(const ShaderProgram& from);
    void refreshVariables();

    // backend
    virtual ~ShaderProgram();
    void bind() const; // can be called on NULL pointer
    void unbind() const;
    void getComputeWorkGroupSize(unsigned *pOut) const;
    void dispatchComputeSingle(unsigned x, unsigned y = 1, unsigned z = 1) const;

    void dispatchComputeMultiBegin() const;
    void _dispatchCompute(unsigned x, unsigned y = 1, unsigned z = 1) const;
    void dispatchComputeMultiEnd() const;

private:
    bool _mustRelink() const;
    bool _queryInterface();
    void _updateLinkCache();
    void _clearLinkCache();

    // backend-specific
    void _queryUniforms();
    bool _link(const ShaderLoadCallbacks *callbacks);
    unsigned getNumUniforms() const;
    unsigned getNumBuffers() const;
    unsigned getNumAttribs() const;

    unsigned _counter;

    struct Variables
    {
        Uniforms uniforms;
        Buffers buffers;
        Inputs inputs;
        Samplers samplers;
        Images images;
        AllVarsPtrs allvars;

        void refresh();
        void _setprogid(unsigned id);
        void clearRefs();
    } vv;

    ShaderObjRef _shaders[ShaderType::_MAXTYPE];
    unsigned _cachedIDs[ShaderType::_MAXTYPE];
    unsigned _cachedCounters[ShaderType::_MAXTYPE];

    // Inherited via DeviceObject
    virtual void onDeviceReset(bool init);
};

typedef CountedPtr<ShaderProgram> ProgramRef;


/*
class ProgramPipeline : public DeviceObject, public Refcounted<REFCOUNT_NORMAL>
{
public:
    // shared / impl agnostic
    inline void set(const ProgramRef& ref) { _progs[ref->getType()] = ref; }
    inline ProgramRef& get(ShaderProgram::Type ty) { return _progs[ty]; }

    // backend-specific
    ProgramPipeline();
    virtual ~ProgramPipeline();
    void bind(); // can be called on NULL pointer

private:
    ProgramRef _progs[ShaderProgram::_MAXTYPE];
    unsigned _cachedIDs[ShaderProgram::_MAXTYPE];
    unsigned _cachedCounters[ShaderProgram::_MAXTYPE];
};
*/

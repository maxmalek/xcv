#include "renderdefines.h"
#include "macros.h"
#include "util.h"

const char * const KnownAttribNames[KA_MAX] =
{
    "in_vertex",
    "in_vertexNormal",
    "in_vertexUV",
    "in_vertexColor",
    "in_userIdx",
};

static void _check()
{
    compile_assert(Countof(KnownAttribNames) == KA_MAX);
}

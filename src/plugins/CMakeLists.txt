if(NOT BUILD_PLUGINDIR)
    set(BUILD_PLUGINDIR "${BUILD_BINDIR}/plugins/" CACHE PATH "Where to place plugin binaries after compiling" FORCE)
endif()

set(PLUGINDIR ${CMAKE_CURRENT_SOURCE_DIR})
set(PLUGIN_INCLUDES
    pluginapi.h
)

find_package(ITK)
OPTION(BUILD_PLUGIN_ITK "Build ITK plugin (requires ITK)" ${ITK_FOUND})
if(BUILD_PLUGIN_ITK)
    add_subdirectory(itk)
endif()

OPTION(BUILD_PLUGIN_STB_IMAGE "Build stb_image plugin (Loads and writes various image types)" TRUE)
if(BUILD_PLUGIN_STB_IMAGE)
    add_subdirectory(stb_image)
endif()

OPTION(BUILD_PLUGIN_BSPLINE "Build bspline plugin" FALSE)
if(BUILD_PLUGIN_BSPLINE)
    add_subdirectory(bsplinecore)
endif()

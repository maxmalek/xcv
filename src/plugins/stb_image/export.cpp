#include "api_plugin.h"
#include "api_funcs.h"
#include "api_types.h"
#include <stdlib.h>

#define STB_IMAGE_IMPLEMENTATION
#define STBI_FAILURE_USERMSG
#include "stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#define STB_IMAGE_WRITE_STATIC
#include "stb_image_write.h"

MAKE_PLUGIN_IMPORT(api);

static const char *FMT_LOAD = "png;bmp;tga;hdr;jpg;jpeg;psd;pnm;ppm;pg;pic;gif";
static const char *FMT_SAVE = "png;bmp;tga;hdr";

static void freeImageData(void *p)
{
    free(p);
}

static IOResult loadImageFile(const char *fn, void **pdata, Blob3DInfo *info)
{
    void *mem = NULL;
    int x, y, comp;
    if(stbi_is_hdr(fn))
        if( (mem = stbi_loadf(fn, &x, &y, &comp, 0)) )
            info->type = B3D_FLOAT;
    if(!mem)
        if( (mem = stbi_load(fn, &x, &y, &comp, 0)) )
            info->type = B3D_U8;
    if(!mem)
        return IORESULT_UNSUPPORTED;

    *pdata = mem;
    info->w = x;
    info->h = y;
    info->channels = comp;

    return IORESULT_OK;
}

struct SaveData
{
    const char *fn;
    IOResult result;
    Blob3DInfo info;
};

static void save2d_cb_png(const void *texdata, size_t bytes, void *userdata)
{
    SaveData *sav = (SaveData*)userdata;
    int res = stbi_write_png(sav->fn, sav->info.w, sav->info.h, sav->info.channels, texdata, 0);
    sav->result = res ? IORESULT_OK : IORESULT_UNSPECIFIED;
}

static void save2d_cb_bmp(const void *texdata, size_t bytes, void *userdata)
{
    SaveData *sav = (SaveData*)userdata;
    int res = stbi_write_bmp(sav->fn, sav->info.w, sav->info.h, sav->info.channels, texdata);
    sav->result = res ? IORESULT_OK : IORESULT_UNSPECIFIED;
}

static void save2d_cb_tga(const void *texdata, size_t bytes, void *userdata)
{
    SaveData *sav = (SaveData*)userdata;
    int res = stbi_write_tga(sav->fn, sav->info.w, sav->info.h, sav->info.channels, texdata);
    sav->result = res ? IORESULT_OK : IORESULT_UNSPECIFIED;
}

static void save2d_cb_hdr(const void *texdata, size_t bytes, void *userdata)
{
    SaveData *sav = (SaveData*)userdata;
    int res = stbi_write_hdr(sav->fn, sav->info.w, sav->info.h, sav->info.channels, (const float*)texdata);
    sav->result = res ? IORESULT_OK : IORESULT_UNSPECIFIED;
}

struct SaveAs
{
    const char *name;
    Blob3DType type;
    void (*callback)(const void *texdata, size_t bytes, void *userdata);
};

static void save2d(APITextureHandle tex, SaveData& sav, const SaveAs& fmt)
{
    api->texture_get_data_callback(tex, fmt.callback, fmt.type, 0, &sav, 0); // leave channels as is
}

static const SaveAs formats[] =
{
    { "png", B3D_U8, save2d_cb_png },
    { "bmp", B3D_U8, save2d_cb_bmp },
    { "tga", B3D_U8, save2d_cb_tga },
    { "hdr", B3D_FLOAT, save2d_cb_hdr },
};
static const unsigned NUM_FORMATS = 4;

static IOResult saveImageFile(const char *fn, const char *format, APITextureHandle tex)
{
    SaveData sav;
    sav.fn = fn;
    sav.result = IORESULT_UNSUPPORTED;
    api->texture_fill_info(tex, &sav.info);

    for(unsigned i = 0; i < NUM_FORMATS; ++i)
        if(!strcmp(formats[i].name, format))
        {
            save2d(tex, sav, formats[i]);
            break;
        }

    return sav.result;
}

static const char *getImageFormatsAvailLoad()
{
    return FMT_LOAD;
}

static const char *getImageFormatsAvailSave()
{
    return FMT_SAVE;
}

static const PluginAPIFuncs funcs =
{
    PluginAPIFuncs_FillHeader,
    freeImageData,
    loadImageFile,
    saveImageFile,
    getImageFormatsAvailLoad,
    getImageFormatsAvailSave,
};

static const PluginAPIDesc desc =
{
    PluginAPIDesc_FillHeader,
    -2 // priority
};

static const PluginAPIDef plugin =
{
    PluginAPIDef_FillHeader,
    &desc,
    &funcs
};


MAKE_PLUGIN_EXPORT(plugin)

#pragma once

#include <stdlib.h>
#include <assert.h>

enum KnotMode
{
    KNOTS_NONE,
    KNOTS_UNIFORM,
    // TODO: more modes (edge length, averaging, etc)
};

inline static unsigned getNumKnotsForPoints(unsigned DEG, unsigned points)
{
    return points + DEG + 1;
}

static size_t totalNumKnots(unsigned DEG, const unsigned *pNumPoints, unsigned ndim)
{
    unsigned total = 0;
    for(unsigned i = 0; i < ndim; ++i)
        total += getNumKnotsForPoints(DEG, pNumPoints[i]);
    return total;
}




template<typename F = float>
struct bsplinecore_setup {

static bool generateKnotsUniform(unsigned DEG, F *knots, unsigned psz)
{
    const unsigned ksz = getNumKnotsForPoints(DEG, psz);
    const unsigned n = psz - 1;
    const unsigned h = n - DEG;
    const F div = F(int(h + 1));

    if(n < DEG)
        return 0;

    for(unsigned j = 1; j <= h; ++j)
        knots[j+DEG] = F(int(j)) / div;

    // smooth end point interpolation
    for(unsigned i = 0; i < DEG+1; ++i)
        knots[i] = F(0);
    for(unsigned i = ksz - DEG - 1; i < ksz; ++i)
        knots[i] = F(1);

    return true;
}

static size_t memorySizeForKnots(unsigned DEG, const unsigned *pNumPoints, unsigned ndim)
{
    return totalNumKnots(DEG, pNumPoints, ndim) * sizeof(F);
}

static F *setupKnotsMultiDim(void *mem, size_t memsize, unsigned DEG, const unsigned *pNumPoints, unsigned ndim, KnotMode mode, F **pknots = NULL)
{
    const size_t reqsize = memorySizeForKnots(DEG, pNumPoints, ndim);
    if(memsize < reqsize)
        return NULL;

    F * const knots = (F*)mem;
    F *k = knots;
    for(unsigned i = 0; i < ndim; ++i)
    {
        unsigned np = pNumPoints[i];
        switch(mode)
        {
            case KNOTS_NONE:
                break;
            case KNOTS_UNIFORM:
                generateKnotsUniform(DEG, k, np);
                break;
        }
        if(pknots)
            pknots[i] = k;
        k += getNumKnotsForPoints(DEG, np);
    }
    return knots;
}

static F *mallocKnotsMultiDim(unsigned DEG, const unsigned *pNumPoints, unsigned ndim, KnotMode mode, F **pknots, size_t *pTotal, size_t *pMemSize)
{
    const size_t total = totalNumKnots(DEG, pNumPoints, ndim);
    const size_t memsize = total * sizeof(F);
    if(pTotal)
        *pTotal = total;
    if(pMemSize)
        *pMemSize = memsize;
    void * const mem = malloc(memsize);
    return setupKnotsMultiDim(mem, memsize, DEG, pNumPoints, ndim, mode);
}

inline static unsigned findKnotIndex(const F *knots, unsigned ksz, F t, unsigned deg)
{
    assert(F(0) <= t && t <= F(1));
    if(t > F(0))
    {
        unsigned good = 0;
        unsigned c = ksz;
        while(c)
        {
            unsigned step = c / 2u;
            unsigned i = good + step;
            if(knots[i] < t)
            {
                good = i + 1;
                c -= step + 1;
            }
            else
                c = step;
        }
        return good - 1;
    }

    return deg;
}


}; // end struct bsplinecore_setup


// --------------------------------------------------------------------


template<unsigned DEG, typename F = float>
struct bsplinecore {

enum { K = DEG+1 };
typedef F NonzeroBasis[K];

template<typename T>
static inline T clamp(T val, T lo, T hi)
{
    return lo < val ? (val < hi ? val : hi) : lo;
}

inline static unsigned getNumKnotsForPoints(unsigned points)
{
    return points + K;
}

inline static bool generateKnotsUniform(F *knots, unsigned psz)
{
    return bsplinecore_setup<F>::generateKnotsUniform(DEG, knots, psz);
}

inline static F *mallocKnotsMultiDim(const unsigned *pNumPoints, unsigned ndim, KnotMode mode = KNOTS_UNIFORM, F **pknots = NULL, unsigned *pTotal = NULL)
{
    return bsplinecore_setup<F>::mallocKnotsMultiDim(DEG, pNumPoints, ndim, mode,  pknots, pTotal);
}


inline static unsigned findKnotIndex(const F *knots, unsigned ksz, F t)
{
    return bsplinecore_setup<F>::findKnotIndex(knots, ksz, t, DEG);
}

// returns start index, so that coeffs[0..DEG] affect points[cstart..cstart+DEG];
static unsigned calcBasisGivenIdx_t01(const F *knots, unsigned idx, F t, NonzeroBasis& basis)
{
    basis[DEG] = F(1);
    const float k1 = knots[idx+1];
    const float k2 = k1 - t;
    const float k3 = t - knots[idx];

    for(unsigned d = 1; d < K; ++d)
    {
        F x = F(1) / (k1 - knots[idx-d+1]);
        const F m = k2 * x;

        basis[K-d-1] = m * basis[K-d];

        // loop could also be written as:
        // for(unsigned i = idx-d+1; i < idx; ++i)
        // But this way the iteration count is easier to deduce at compile time,
        // which may help the compiler with loop unrolling
        for(unsigned j = 1; j < d; ++j)
        {
            const unsigned i = idx - d + j;
            const unsigned ci = DEG - d + j;
            const F k1 = knots[i+d+1];
            const F y = F(1) / (k1 - knots[i+1]);
            const F a = (t - knots[i]) * x;
            const F b = (k1 - t) * y;
            x = y;
            basis[ci] = a * basis[ci] + b * basis[ci+1];
        }
        basis[DEG] *= (k3 * x);
    }

    return idx-DEG;
}

// returns start index, so that coeffs[0..DEG] affect points[cstart..cstart+DEG];
static unsigned calcBasis_t01(const F *knots, unsigned psz, F t, NonzeroBasis& basis)
{
    const unsigned ksz = getNumKnotsForPoints(psz);
    const unsigned idx = findKnotIndex(knots, ksz, t);
    return calcBasisGivenIdx_t01(knots, idx, t, basis);
}

/*static void calcManyIndexes_t01(const F *knots, unsigned psz, const F *ta, unsigned *indexes)
{
    const unsigned ksz = getNumKnotsForPoints(psz);
    for(unsigned i = 0; i < psz; ++i)
        indexes[i] = findKnotIndex(knots, ksz, ta[i]);  // TODO: can be optimized: ta is sorted. do linear array scan instead of binary search!
}*/

static unsigned calcBasis(const F *knots, unsigned psz, F t, NonzeroBasis& basis)
{
    return calcBasis_t01(knots, psz, clamp(t, F(0), F(1)), basis);
}

static void calcBasisArray(const F *knots, unsigned psz, const F *ta, unsigned *begins, NonzeroBasis *bases, unsigned sz)
{
    for(unsigned i = 0; i < sz; ++i)
        begins[i] = calcBasis(knots, psz, ta[i], bases[i]); // TODO: can be optimized: ta is sorted. do linear array scan instead of binary search!
}

template<typename P>
static P calcPointFromBasis(const P *points, const NonzeroBasis& basis, unsigned begin)
{
    points += begin;
    P out = points[0] * basis[0];
    for(unsigned i = 1; i < K; ++i) // coeffs[0..DEG] correspond to points[begin..begin+DEG]
        out += points[i] * basis[i];
    return out;
}

template<typename P>
static P calcPointFromExpandedBasis(const P *points, const F *basis, unsigned begin)
{
    return calcPointFromBasis(points, static_cast<NonzeroBasis>(basis + begin), begin);
}

static void expandCopyBasis(F *dst, const NonzeroBasis& src, unsigned begin)
{
    dst += begin;
    for(unsigned i = 0; i < K; ++i)
        dst[i] = src[i];
}

// (t) -> whatever
template<typename P>
static P calcPoint(const P *points, unsigned psz, const F *knots, const F t)
{
    NonzeroBasis basis;
    unsigned begin = calcBasis(knots, psz, t, basis);
    return calcPointFromBasis(points, basis, begin);
}

// (t_0, ... t_D) -> whatever
template<typename P, unsigned D>
struct MultiDimPointCalc
{
    static P calc(const P *points, const unsigned *dimskip, const NonzeroBasis *bases, const unsigned *begins)
    {
        const unsigned begin = begins[D];
        const unsigned dskip = dimskip[D];
        unsigned skipAccu = dskip * begin;
        P work[K];

        for(unsigned i = 0; i < K; ++i, skipAccu += dskip)
        {
            work[i] = MultiDimPointCalc<P, D-1>::calc(points + skipAccu, dimskip, bases, begins);
        }

        return calcPointFromBasis(&work[0], bases[D], 0);
    }
};
template<typename P>
struct MultiDimPointCalc<P, 0>
{
    static P calc(const P *points, const unsigned *dimskip, const NonzeroBasis *bases, const unsigned *begins)
    {
        (void)dimskip; // unused
        return calcPointFromBasis(points, bases[0], begins[0]);
    }
};

static void _setupMultidimLookups(const unsigned DIM, NonzeroBasis *bases, unsigned *begins, unsigned *dimskip, const unsigned *pNumPoints, const F *knots, const F *pt)
{
    unsigned knotlenAccu = 0;
    unsigned dimskipAccu = 1;
    for(unsigned i = 0; i < DIM; ++i)
    {
        dimskip[i] = i ? dimskipAccu : 0;
        dimskipAccu *= pNumPoints[i];

        const unsigned knotlen = getNumKnotsForPoints(pNumPoints[i]);
        begins[i] = calcBasis(knots + knotlenAccu, pNumPoints[i], pt[i], bases[i]);
        knotlenAccu += knotlen;
    }
}

template<unsigned DIM, typename P>
static P calcPointNDUnsafe(const P *points, const unsigned *pNumPoints, const F *knots, const F *pt)
{
    NonzeroBasis bases[DIM];
    unsigned dimskip[DIM];
    unsigned begins[DIM];
    _setupMultidimLookups(DIM, bases, begins, dimskip, pNumPoints, knots, pt);

    return MultiDimPointCalc<P, DIM-1>::calc(points, dimskip, bases, begins);
}

template<unsigned DIM, typename P>
inline static P calcPointND(const P *points, const unsigned (&pNumPoints)[DIM], const F *knots, const F (&pt)[DIM])
{
    return calcPointNDUnsafe<DIM, P>(points, &pNumPoints[0], knots, &pt[0]);
}


}; // end struct bspline


#pragma once

#include <stddef.h>
#include <vector>
#include "glmx.h"

class BSplineSolverPipeline
{
public:
    BSplineSolverPipeline();
    ~BSplineSolverPipeline();

    struct Arrays
    {
        float *pAxis[3];
        size_t sz; // #floats in each array
        glm::uvec3 dim;
    };

    void importTextures(const unsigned *pTex, size_t n);
    void calculate3DControlPoints();


private:
    std::vector<Arrays> varrays;
};
#if 0

#include "solverpipeline.h"
#include "blob3d.h"
#include "parallel_for.h"
#include <assert.h>
#include "soashuffle.h"
#include "bsplinesolve.h"

using namespace jobq;

BSplineSolverPipeline::BSplineSolverPipeline()
{
}

BSplineSolverPipeline::~BSplineSolverPipeline()
{
}

struct ConvertData
{
    void *raw;
    BSplineSolverPipeline::Arrays *dst;
    Blob3DInfo info;
};

void jf_rawToSOA(Job, const void *ud)
{
    const ConvertData *cv = (const ConvertData*)ud;
    Blob3DInfo info = cv->info;
    typedef soa::Shuffler<float, 3, 1> Shuf;
    size_t nTuples = info.w * info.h * info.d;
    float *mem = (float*)malloc(nTuples * sizeof(Shuf::value_type) * Shuf::tuple_size);
    Shuf::aos2soa(mem, (float*)cv->raw, nTuples);
    cv->dst->pAxis[0] = mem;
    cv->dst->pAxis[1] = mem + nTuples;
    cv->dst->pAxis[2] = mem + 2*nTuples;
    cv->dst->sz = nTuples;
    free(cv->raw);
}

void BSplineSolverPipeline::importTextures(const unsigned * pTex, size_t n)
{
    varrays.resize(n);
    Job master = CreateJob(NULL);
    for(unsigned i = 0; i < n; ++i)
    {
        ConvertData cv;
        cv.raw = blob3d::downloadTexture(&cv.info, NULL, pTex[i], B3D_FLOAT);
        cv.dst = &varrays[i];
        assert(cv.info.channels == 4);
        assert(cv.info.type == B3D_FLOAT);
        Job ch = CreateJob(master, jf_rawToSOA, cv);
        Run(ch);
    }
    Run(master);
    Wait(master);
}

struct SingleElementData1D
{
    float *p;
    size_t n;
};

void jf_calcctrl_singleElement_1D(Job parent, const void *ud)
{
    assert(false); // WRITE ME
}

struct SingleElementData3D
{
    float *p;
    size_t sz;
    glm::uvec3 dim;
};

// process one cube of X, Y, or Z values
void jf_calcCtrl_singleElement_3D(Job parent, const void *ud)
{
    assert(false); // WRITE ME
}

void jf_calc3dctrl_xyz_3D(Job parent, const void *ud)
{
    const BSplineSolverPipeline::Arrays *a = (const BSplineSolverPipeline::Arrays*)ud;
    for(unsigned i = 0; i < 3; ++i)
    {
        SingleElementData3D ax;
        ax.p = a->pAxis[i];
        ax.sz = a->sz;
        ax.dim = a->dim;
        Job j = CreateJob(parent, jf_calcCtrl_singleElement_3D, ax);
        Run(j);
    }
}

void BSplineSolverPipeline::calculate3DControlPoints()
{
    Job master = CreateJob(NULL);
    for(size_t i = 0; i < varrays.size(); ++i)
    {
        Job j = CreateJob(master, jf_calc3dctrl_xyz_3D, varrays[i]);
        Run(j);
    }

    Run(master);
    Wait(master);
}

#endif

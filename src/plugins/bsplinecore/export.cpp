#include "api_plugin.h"

MAKE_PLUGIN_IMPORT(api);


static const PluginAPIDef plugin =
{
    PluginAPIDef_FillHeader,
    NULL,
    NULL
};


MAKE_PLUGIN_EXPORT(plugin)


int lua_register_bsplinecore(lua_State *L);

PLUGIN_LUA_PACKAGE(bsplinecore)
{
    return lua_register_bsplinecore(L);
}

#pragma once

#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <assert.h>
#include <memory.h>

template<typename T>
class BSpline
{
public:

	BSpline(int degree)
		: _degree(degree), _maxknot(0) // min knot value assumed to be always 0
	{
		assert(degree > 0, "Empty spline??");
	}

	void clear()
	{
		_maxknot = 0;
		_knots.clear();
		points.clear();
	}

	void addPoint(const T& p)
	{
		points.push_back(p);
	}

	// add points first, then call this
	bool init()
	{
		bool good = false;
		if(_knots.empty())
		{
			good = generateKnotVectorUniform();
			_maxknot = 1;
		}
		else
		{
			_knots.resize(getNumKnots());
			// constrain the end points for smooth interpolation
			// this is somewhat hackish and takes some control away from the knots
			// already added, but at least it's safe.
			// If in doubt, just add more knots.
			for(int i = 0; i <= _degree; ++i)
				_knots[i] = 0;
			for(size_t i = _knots.size() - _degree - 1; i < _knots.size(); ++i)
				_knots[i] = _maxknot;
			good = true;
		}
		return good;
	}

	// Note: user is responsible for adding knots in sorted order
	void addKnot(float t, const T& p)
	{
		// If this is the first knot, repeat multiple times
		// for smooth end interpolation
		if(_knots.empty())
			_knots.resize(_degree, 0);
		else
			_knots.push_back(t);

		_maxknot = std::max(t, _maxknot);
		points.push_back(p);
	}

	// deBoor algorithm on shrinking workmem
	T operator() (float t)
	{
		t = clamp(0.0f, t, _maxknot);
		int r = findKnotIndex(t);
		if(r < _degree)
			r = _degree;
		const int k = _degree + 1;
		T *work = (T*)alloca(k * sizeof(T));
		{
			const T *ptr = &points[r - _degree];
			// Work mem is not initialized, let's just plow over it so that no operator= will be called
			// and ruin the day due to invalid internal pointers of objects that don't exist.
			// Dtors won't be called either when the stack frame exits, so this should HOPEFULLY be fine for any type T passed in.
			std::uninitialized_copy(ptr, ptr + k, work);
		}
		unsigned int worksize = k;
		const float *kp = &_knots[0];
		while(worksize > 1)
		{
			const int j = k - worksize + 1; // iteration number, starting with 1, going up to k
			const int tmp = r-k+1+j;
			for(size_t w = 0; w < worksize-1; ++w)
			{
				const int i = w + tmp;
				const float ki = kp[i];
				const float a = (t-ki) / (kp[i+k-j] - ki);
				assert(a >= 0 && a <= 1, "a out of range");
				work[w] = (work[w] * (1.f - a)) + (work[w+1] * a);
			}
			--worksize;
		}
		T ret = work[0];
		return ret;
	}

	inline size_t size() const { return points.size(); }

protected:

	unsigned getNumKnots()
	{
		return points.size() + _degree + 1;
	}

	bool generateKnotVectorUniform()
	{
		const int n = points.size() - 1;
		const int p = _degree;
		if(n < p)
			return false;

		_knots.resize(getNumKnots());
#ifdef _DEBUG
		std::fill(_knots.begin(), _knots.end(), 999.f);
#endif
		for(int i = 0; i < p+1; ++i)
			_knots[i] = 0.0f;

		const float div = float(n - p + 1);
		for(int j = 1; j <= n - p; ++j)
			_knots[j+p] = j / div;
		for(size_t i = _knots.size() - p - 1; i < _knots.size(); ++i)
			_knots[i] = 1.0f;

		return true;
	}

	inline int findKnotIndex(float t)
	{
		// find index, so that t is in [_knots[k], _knots[k+1])
		std::vector<float>::iterator ik = std::lower_bound(_knots.begin(), _knots.end(), t);
		if(ik == _knots.end())
			return _knots.size() - 1; // last valid index
		return ik - _knots.begin() - (*ik > 0); // ik points to first element not less than u, go one back if it's bigger, and we have k.
	}

	int _degree;
	float _maxknot;
	std::vector<float> _knots;
	std::vector<T> points;
};

#include "api_lua.h"
#include "bsplinecore.h"
#include <stdlib.h>
#include <vector>
#include <string.h>

typedef bsplinecore_setup<float> bscore;


static KnotMode getKnotMode(lua_State *L, int idx)
{
    if(lua_isnoneornil(L, idx))
        return KNOTS_UNIFORM;
    const char * const s = lua_checkrealstring(L, idx);
    if(!s || !*s || !strcmp(s, "uniform"))
        return KNOTS_UNIFORM;

    if(!strcmp(s, "none"))
        return KNOTS_NONE;

    luaL_error(L, "Unknown KnotMode: %s", s);
    return KNOTS_NONE; // Unreachable
}

// dim, mode, [# of points for each dimension]
luaFunc(createKnots)
{
    const unsigned deg = lua_checkuint(L, 1);
    const KnotMode km = getKnotMode(L, 2);
    std::vector<unsigned> pdims;
    const int top = lua_gettop(L);
    for(int i = 3; i <= top; ++i) // remaining params are number of wanted points in that dimension
    {
        const unsigned n = lua_checkuint(L, i);
        if(!n)
            luaL_error(L, "0 is not allowed");
        pdims.push_back(n);
    }
    if(pdims.empty())
        luaL_error(L, "Need at least 1 dimension");

    const unsigned ndim = (unsigned)pdims.size();
    const size_t sz = bscore::memorySizeForKnots(deg, &pdims[0], ndim);
    void *p = lua_newuserdata(L, sz); // push memory
    bscore::setupKnotsMultiDim(p, sz, deg,  &pdims[0], ndim, km);
    lua_pushinteger(L, sz); // push size in bytes
    return 2; // udata, size
}


static const luaL_Reg bsplinefuncs[] =
{
    // TODO: register funcs here
    luaRegister(createKnots),
    {NULL, NULL},
};

int lua_register_bsplinecore(lua_State *L)
{
    luaL_newlib(L, bsplinefuncs);
    return 1;
}

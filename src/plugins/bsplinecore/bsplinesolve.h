#pragma once

#include <Eigen/Sparse>
#include <Eigen/Dense>
#include <Eigen/SparseQR>

#include "bsplinecore.h"

template<unsigned DEG, typename F = float>
struct bsplinesolve {

typedef bsplinecore<DEG, F> B;
enum { K = B::K };

typedef Eigen::SparseMatrix<F> FMtx;
typedef Eigen::Matrix<F, 1, Eigen::Dynamic> FVec;
typedef Eigen::SparseQR<FMtx, Eigen::COLAMDOrdering<int> > FSparseSolver; // what

struct ApproximationCoeffs
{
    unsigned ncp; // total given points that should lie on the curve
    unsigned napprox; // number of control poins to calculate
    FMtx N;
    FMtx M;
    FSparseSolver solver;
};

struct InterpolationCoeffs
{
    unsigned ncp;
    FMtx N;
    FSparseSolver solver;
};

static void _computeBasisMatrix(FMtx& N, const F *ts, const F *knots, unsigned ncp, unsigned napprox)
{
    N = FMtx(ncp, napprox);
    N.reserve(Eigen::VectorXi::Constant(ncp, K)); // ncp rows; K non-zero entries per row

    for(unsigned i = 0; i < ncp; ++i)
    {
        typename B::NonzeroBasis basis;
        const unsigned begin = B::calcBasis(knots, ncp, ts[i], basis);
        for(unsigned k = 0; k < K; ++k)
            if(basis[k])
                N.insert(i, k+begin) = basis[k]; // insert(row, col)
    }

    N.makeCompressed();
}

// expects knots for ncp control points
// Coefficients change only when changing dimension.
// No need to recalculate the matrices everytime when only point positions change,
// as long as the values in ts stay constant.
static void computeCoeffs(const F *ts, const F *knots, unsigned ncp, InterpolationCoeffs& ico)
{
    _computeBasisMatrix(ico.N, ts, knots, ncp, ncp);
    ico.ncp = ncp;
    ico.solver.compute(ico.N);
}

// expects knots for napprox control points
// Coefficients change only when changing dimension.
// No need to recalculate the matrices everytime when only point positions change,
// as long as the values in ts stay constant.
static void computeCoeffs(const F *ts, const F *knots, unsigned ncp, unsigned napprox, ApproximationCoeffs& aco)
{
    _computeBasisMatrix(aco.N, ts, knots, ncp, napprox);
    aco.ncp = ncp;
    aco.napprox = napprox;

    // Taking only the center part skips the end points, since the curve already goes through them
    const FMtx Ncenter = aco.N.block(1, 1, aco.N.rows() - 2, aco.N.cols() - 2);
    aco.M = Ncenter.transpose() * Ncenter; // least squares or whatever this does
    aco.M.makeCompressed();
    aco.solver.compute(aco.M);
    assert(aco.solver.info() == Eigen::Success); // FIXME: can this fail? What to do if it does?
}


static void computeCoeffsND(const unsigned DIM, const F *ts, const F *knots, const unsigned *pncp, InterpolationCoeffs *pico)
{
    for(unsigned d = 0; d < DIM; ++d)
    {
        const unsigned ncp = pncp[d];
        computeCoeffs(ts, knots, ncp, pico[d]);
        ts += ncp;
        knots += B::getNumKnotsForPoints(ncp);
    }
}

static void computeCoeffsND(const unsigned DIM, const F *ts, const F *knots, const unsigned *pncp, const unsigned *pnapprox, ApproximationCoeffs *paco)
{
    for(unsigned d = 0; d < DIM; ++d)
    {
        const unsigned ncp = pncp[d];
        computeCoeffs(ts, knots, ncp, pnapprox[d], paco[d]);
        ts += ncp;
        knots += B::getNumKnotsForPoints(ncp);
    }
}




template<typename T, typename TVEC>
struct _ScalarAccessor
{
    // assume TVEC == T
    enum { components = 1 };
    static inline const T& get(const TVEC& v, unsigned dim)
    {
        (void)dim;
        return v;
    }
    static inline void set(TVEC& v, const T& x, unsigned dim)
    {
        (void)dim;
        v = x;
    }
};

// TODO: should change the processing to SOA layout instead of AOS... would also get rid of this accessor template crap
template<typename ACC, typename PV, typename P>
static P *_solveForAllDimensionsAOS(const FSparseSolver& solver, const PV& pb, P *dst, unsigned stride)
{
    typedef Eigen::Matrix<F, Eigen::Dynamic, 1> FRVec; // row vector
    const unsigned sz = (unsigned)pb.size();
    FRVec b;
    b.resize(sz);

    // This temp copying is ugly, but otherwise things are not type compatible...
    for(unsigned d = 0; d < ACC::components; ++d)
    {
        // copy out one dimension
        for(unsigned i = 0; i < sz; ++i)
            b[i] = ACC::get(pb[i], d);

        FRVec sol = solver.solve(b);
        assert(solver.info() == Eigen::Success);

        // merge directly into output
        for(unsigned i = 0; i < sz; ++i)
        {
            ACC::set(*dst, sol[i], d);
            dst += stride;
        }
    }
    return dst;
}

template<typename P, typename ACC = _ScalarAccessor<P, P> >
static void computeControlPoints(P *controlpoints, const P *curvepoints, const InterpolationCoeffs& ico, unsigned stride = 1)
{
    typedef Eigen::Matrix<P, Eigen::Dynamic, 1> PRVec; // row vector

    P *dst = _solveForAllDimensionsAOS<ACC, PRVec, P>(ico.solver, PRVec::Map(curvepoints, ico.ncp), controlpoints, stride);

    // sol[0] and sol[z] already hold the same values as controlpoints[0] and controlpoints[z], respectively,
    // but may be numerically slightly less accurate since they went through the matrix solver.
    // The explicit copy solves this.
    controlpoints[0] = curvepoints[0];
    *(dst-stride) = curvepoints[ico.ncp - 1];
}

template<typename P, typename ACC = _ScalarAccessor<P, P> >
static void computeControlPoints(P *controlpoints, const P *curvepoints, const ApproximationCoeffs& aco, unsigned stride = 1)
{
    typedef Eigen::Matrix<P, Eigen::Dynamic, 1> PRVec; // row vector

    const unsigned h = aco.napprox - 1;
    const unsigned n = aco.ncp - 1;

    PRVec Q(h - 1);
    for(unsigned i = 1; i < h; ++i)
    {
        P tmp = P(0);
        for(unsigned k = 1; k < n; ++k)
            tmp += aco.N.coeff(k, i) * (curvepoints[k] - aco.N.coeff(k, 0) * curvepoints[0] - aco.N.coeff(k, h) * curvepoints[n]);
        Q[i-1] = tmp;
    }

    P *dst = _solveForAllDimensionsAOS<ACC, PRVec, P>(aco.solver, Q, controlpoints + 1, stride);

    // End points are known to be interpolated exactly by the curve, and were NOT written by the solver,
    // since here all points EXCEPT the first and last one were calculated
    controlpoints[0] = curvepoints[0];
    *dst = curvepoints[n]; // FIXME: check that this is correct
}

#include <vector>


template<unsigned DIM, typename P, typename ACC = _ScalarAccessor<P, P> >
static void computeControlPointsNDUnsafe(P * const controlpoints, const P *const curvepoints, const InterpolationCoeffs * const pico)
{
    unsigned workpoints = pico[0].ncp;
    for(unsigned i = 1; i < DIM; ++i)
        workpoints *= pico[i].ncp;

    P * const work = DIM > 1 ? (P*)malloc(workpoints * sizeof(P)) : NULL;

    // Swap dst and src buffers in every iteration.
    // Because the src pointer is the original data in the first iteration, it must NOT become dst.
    // So we use a lookup table instead and use the entries in alternating order.
    // Since controlpoints is of sufficient size, it can be used as temporary buffer,
    // so only one needs to be allocated here.
    // -> Write pointers in such a way that the last iteration will write to controlpoints.
    P *pingpong[2];
    pingpong[DIM & 1] = work;
    pingpong[(DIM+1) & 1] = controlpoints;

    const P *src = curvepoints;
    
    for(unsigned d = 0; d < DIM; ++d)
    {
        P * const dst = pingpong[d & 1];
        assert(dst);
        const unsigned entriesPerRow = pico[d].ncp;
        const P *srcptr = src;
        const P * const endptr = src + workpoints;
        // Make sure that the last iteration writes into controlpoints
        assert(d != (DIM-1) || (dst == controlpoints && src != controlpoints));
        for(unsigned y = 0; srcptr < endptr; ++y, srcptr += entriesPerRow)
        {
            // entriesPerRow stride so that the computed points will be placed column-wise into dst
            // the next iteration (if there is any) will read the points row-wise, which means the memory layout is exactly as required.
            // (This corresponds to an implicit matrix transpose)
            computeControlPoints(dst + y, srcptr, pico[d], entriesPerRow); 
        }
        src = dst;
    }

    free(work);

}



};


#pragma once

#include "bsplinesolve.h"
#include <vector>

template<typename P, unsigned DEG, unsigned IDIM, typename F = float>
class BSpline
{
protected:
    typedef bsplinesolve<DEG, F> BS;
    typedef typename BS::B B;
public:
    typedef unsigned Dimensions[IDIM];
    typedef F Param[IDIM];
    static inline const Param& asParam(const F *t) { return reinterpret_cast<const Param&>(*t); }
    BSpline()
        : _totalPoints(0), knots(0)
    {
    }
    ~BSpline()
    {
        free(knots);
    }

    std::vector<P> controlpoints;
    Dimensions nPoints;

    
    inline P operator() (const Param& ts)
    {
        assert(controlpoints.size() == _totalPoints);
        return B::calcPointND(&controlpoints[0], nPoints, knots, ts);
    }

    void generateKnots(KnotMode mode = KNOTS_UNIFORM)
    {
        free(knots);
        knots = B::mallocKnotsMultiDim(&nPoints[0], IDIM, mode);
        unsigned tp = 1;
        for(unsigned i = 0; i < IDIM; ++i)
            tp *= nPoints[i];
        _totalPoints = tp;
    }

protected:

    size_t _totalPoints;
    F *knots;
};



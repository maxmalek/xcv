#pragma once

#include <xmmintrin.h>
#include "glmx.h"

// _MM_SHUFFLE values via http://wurstcaptures.untergrund.net/assembler_tricks.html#shufps

inline __m128 mm_mix(__m128 v1, __m128 v2, __m128 a, __m128 oneMinusA)
{
    return _mm_add_ps(
        _mm_mul_ps(v1, oneMinusA),
        _mm_mul_ps(v2, a));
}

#define mm_expand(v, i) _mm_shuffle_ps((v), (v), _MM_SHUFFLE(i,i,i,i))


inline glm::vec3 calcPointInternal_Cubic_DenseVec3(const float *knots, unsigned ksz, unsigned k, const float t, unsigned idx, float *work)
{
    glm::vec4 ret4;

    const unsigned cacheOffs = idx - (k - 1);
    const unsigned knotOffset = cacheOffs + 1;

    const float *kcache = &knots[ksz + cacheOffs];

    // Loads
    __m128 cache1 = _mm_loadu_ps(kcache);
    __m128 cache2 = _mm_loadu_ps(kcache + 4);
    __m128 cache3 = _mm_loadu_ps(kcache + 3);

    __m128 m0 = _mm_load_ps(work);
    __m128 m1 = _mm_load_ps(work + 4);
    __m128 m2 = _mm_load_ps(work + 8);

    // constants
    const __m128 kk = _mm_loadu_ps(knots + knotOffset);
    const __m128 tt = _mm_set1_ps(t);
    const __m128 one = _mm_set1_ps(1.0f);

    // Setup
    __m128 a1 = _mm_mul_ps(_mm_sub_ps(tt, kk), cache1);
    __m128 a2 = _mm_mul_ps(_mm_sub_ps(tt, _mm_shuffle_ps(kk, kk, _MM_SHUFFLE(2,1,0,0))), cache2);
    __m128 a3 = _mm_mul_ps(_mm_sub_ps(tt, _mm_shuffle_ps(kk, kk, _MM_SHUFFLE(1,0,0,0))), cache3);
    __m128 oneMinusA1 = _mm_sub_ps(one, a1);
    __m128 oneMinusA2 = _mm_sub_ps(one, a2);
    __m128 oneMinusA3 = _mm_sub_ps(one, a3);

    // Loaded values:
    //        m0        m1        m2
    //    x,y,z,x,   y,z,x,y,   z,x,y,z
    // Shuffle into individial vectors:
    // from: (x,y,z),(x,    y,z),(x,y,    z),(x,y,z)
    // to:    x,y,z,    x,y,z  ,   x,y,z,     x,y,z
    //         v0        v1         v2         v3
    __m128 v0 = m0;
    __m128 v1 = _mm_shuffle_ps(m0, m1, _MM_SHUFFLE(0,0,2,1));
           v1 = _mm_shuffle_ps(v1, v1, _MM_SHUFFLE(3,1,0,0));
    __m128 v2 = _mm_shuffle_ps(m1, m2, _MM_SHUFFLE(1,0,3,3));
    __m128 v3 = _mm_shuffle_ps(m2, m2, _MM_SHUFFLE(2,1,0,0));

    // deBoor scheme, first iteration
    __m128 v0_1 = mm_mix(v0, v1, mm_expand(a1, 3), mm_expand(oneMinusA1, 3));
    __m128 v1_1 = mm_mix(v1, v2, mm_expand(a1, 2), mm_expand(oneMinusA1, 2));
    __m128 v2_1 = mm_mix(v2, v3, mm_expand(a1, 1), mm_expand(oneMinusA1, 1));

    // second iteration
    __m128 v0_2 = mm_mix(v0_1, v1_1, mm_expand(a1, 3), mm_expand(oneMinusA2, 3));
    __m128 v1_2 = mm_mix(v1_1, v2_1, mm_expand(a1, 2), mm_expand(oneMinusA2, 2));

    // last iteration
    __m128 v0_3 = mm_mix(v0_1, v1_1, mm_expand(a1, 2), mm_expand(oneMinusA3, 2));

    // Final store
    _mm_store_ps(glm::value_ptr(ret4), v0_3);

    return glm::vec3(ret4);
}



/*
a0 = (t - knots[tmp + 0]) * kcache[cacheOffs + 0];
a1 = (t - knots[tmp + 1]) * kcache[cacheOffs + 1];
a2 = (t - knots[tmp + 2]) * kcache[cacheOffs + 2];
work[0] = (work[0] * (K(1) - a0)) + (work[1] * a0);
work[1] = (work[1] * (K(1) - a1)) + (work[2] * a1);
work[2] = (work[2] * (K(1) - a2)) + (work[3] * a2);
tmp += 1; // drop knot[0]

a0 = (t - knots[tmp + 0]) * kcache[cacheOffs + 0];
a1 = (t - knots[tmp + 1]) * kcache[cacheOffs + 1];
work[0] = (work[0] * (K(1) - a0)) + (work[1] * a0);
work[1] = (work[1] * (K(1) - a1)) + (work[2] * a1);
tmp += 1; // drop knot[0]

a0 = (t - knots[tmp + 0]) * kcache[cacheOffs + 0];
work[0] = (work[0] * (K(1) - a0)) + (work[1] * a1);
*/






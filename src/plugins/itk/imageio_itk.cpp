#ifdef _MSC_VER
#pragma warning(disable:4244)
#endif

#include "pstdint.h"
#include "api_types.h"
#include "imageio_itk.h"

#include <stdlib.h>

#include <itkImage.h>
#include <itkImageFileReader.h>
#include <itkImageRegionIterator.h>

#include <itkRGBAPixel.h>
#include <itkRGBPixel.h>
#include <itkImageFileWriter.h>
#include <itkCastImageFilter.h>

#include <itkRescaleIntensityImageFilter.h>
#include "itkDefaultConvertPixelTraits.h"

#include <stdlib.h>
#include <limits>

template<typename T, unsigned N> struct NComp {};
template<typename T> struct NComp<T, 1> { enum { Components = 1 }; typedef T BasicType; typedef T PixelType; };
template<typename T> struct NComp<T, 2> { enum { Components = 2 }; typedef T BasicType; typedef itk::FixedArray<T, 2> PixelType; };
template<typename T> struct NComp<T, 3> { enum { Components = 3 }; typedef T BasicType; typedef itk::RGBPixel<T> PixelType; };
template<typename T> struct NComp<T, 4> { enum { Components = 4 }; typedef T BasicType; typedef itk::RGBAPixel<T> PixelType; };

template<typename T> struct Rescale
{
    inline static T minval() { return std::numeric_limits<T>::min(); }
    inline static T maxval() { return std::numeric_limits<T>::max(); }
};
template<> struct Rescale<float>
{
    inline static float minval() { return 0.0f; }
    inline static float maxval() { return 1.0f; }
};
template<> struct Rescale<double>
{
    inline static float minval() { return 0.0; }
    inline static float maxval() { return 1.0; }
};

template<typename T, unsigned COMP, unsigned DIM>
struct ImageRW
{
    typedef typename NComp<T, COMP>::PixelType PixelType;
    typedef itk::Image<PixelType, DIM> ImageType;
    typedef typename ImageType::Pointer ImagePtr;
    typedef itk::ImageFileWriter<ImageType> WriterType;
    typedef itk::ImageFileReader<ImageType> ReaderType;
    typedef typename ImageType::SpacingType SpacingType;

    static ImagePtr Save(const void *mem, const Blob3DInfo *info)
    {
        itk::ImageRegion<DIM> outRegion;
        SpacingType spacing;
        for(unsigned i = 0; i < DIM; ++i)
        {
            outRegion.SetSize(i, info->dim[i]);
            spacing[i] = info->sp[i];
        }
        ImagePtr outImg = ImageType::New();
        outImg->SetSpacing(spacing);
        outImg->SetRegions(outRegion);
        outImg->Allocate();

        // TODO: Is there a way to do this without copying?
        const PixelType *rptr = (const PixelType*)mem;
        itk::ImageRegionIterator<ImageType> it(outImg, outRegion);
        for( ; !it.IsAtEnd(); ++it)
            it.Value() = *rptr++;

        return outImg;
    }

    static IOResult Load(ImagePtr in, void **mem, Blob3DInfo *info)
    {
        const typename ImageType::RegionType dim = in->GetLargestPossibleRegion();
        const typename ImageType::SpacingType spacing = in->GetSpacing();
        size_t bytes = sizeof(PixelType);

        for(unsigned i = 0; i < DIM; ++i)
        {
            info->sp[i] = spacing[i];
            const size_t sz = dim.GetSize(i);
            assert(sz);
            info->dim[i] = (unsigned)sz;
            bytes *= sz;
        }

        PixelType * const ptr = (PixelType*)malloc(bytes);
        if(!ptr)
            return IORESULT_UNSPECIFIED;
        PixelType *wptr = ptr;

        itk::ImageRegionConstIterator<ImageType> it(in, dim);

        for( ; !it.IsAtEnd(); ++it)
            *wptr++ = it.Value();

        *mem = ptr;
        info->channels = COMP;
        info->type = ToBlob3dType<T>::value;
        return IORESULT_OK;
    }
};

// Throws on error
template<typename IMAGE>
static typename IMAGE::Pointer loadItkImage(const char *fn)
{
    typedef itk::ImageFileReader<IMAGE> ReaderType;
    typename ReaderType::Pointer reader = ReaderType::New();
    reader->SetFileName(fn);
    reader->Update();
    return reader->GetOutput();
}

template<typename T, unsigned COMP, unsigned DIM> struct RescaleHandler
{
    typedef ImageRW<T, COMP, DIM> Loader;
    typedef typename Loader::ImageType ImageType;
    typedef typename ImageType::Pointer ImagePtr;
    static ImagePtr Compute(ImagePtr img)
    {
        std::cout << "ERROR: Rescale not available for multi-component images\n";
        return img;
    }
};

template<typename T, unsigned DIM> struct RescaleHandler<T, 1, DIM>
{
    typedef ImageRW<T, 1, DIM> Loader;
    typedef typename Loader::ImageType ImageType;
    typedef typename ImageType::Pointer ImagePtr;
    static ImagePtr Compute(ImagePtr img)
    {
        typedef itk::RescaleIntensityImageFilter<ImageType, ImageType> RescaleFilterType;
        typename RescaleFilterType::Pointer rescaleFilter = RescaleFilterType::New();
        rescaleFilter->SetInput(img);
        rescaleFilter->SetOutputMinimum(Rescale<T>::minval());
        rescaleFilter->SetOutputMaximum(Rescale<T>::maxval());
        rescaleFilter->Update();
        return rescaleFilter->GetOutput();
    }
};

template<typename T, unsigned COMP, unsigned DIM>
static IOResult load(const char *fn, void **mem, Blob3DInfo *info, ITKLoadFlags lf)
{
    typedef ImageRW<T, COMP, DIM> Loader;
    typedef typename Loader::ImageType ImageType;

    typename ImageType::Pointer img = loadItkImage<ImageType>(fn);

    if(lf & ITKL_RESCALE)
        img = RescaleHandler<T, COMP, DIM>::Compute(img);

    return Loader::Load(img, mem, info);
}

template<typename T, unsigned COMP>
static IOResult load_1(const char *fn, void **mem, Blob3DInfo *info, unsigned dim, ITKLoadFlags lf)
{
    switch(dim)
    {
        case 1: return load<T, COMP, 1>(fn, mem, info, lf);
        case 2: return load<T, COMP, 2>(fn, mem, info, lf);
        case 3: return load<T, COMP, 3>(fn, mem, info, lf);
    }
    return IORESULT_INVALID_FORMAT;
}

template<typename T>
static IOResult load_2(const char *fn, void **mem, Blob3DInfo *info, unsigned loadchannels, unsigned dim, ITKLoadFlags lf)
{
    switch(loadchannels)
    {
        case 1: return load_1<T, 1>(fn, mem, info, dim, lf);
        case 2: return load_1<T, 2>(fn, mem, info, dim, lf);
        case 3: return load_1<T, 3>(fn, mem, info, dim, lf);
        case 4: return load_1<T, 4>(fn, mem, info, dim, lf);
    }
    return IORESULT_INVALID_FORMAT;
}

static IOResult load_3(const char *fn, void **mem, Blob3DInfo *info, Blob3DType loadtype, unsigned loadchannels, unsigned dim, ITKLoadFlags lf)
{
#define CVT(ty, cty) case ty: return load_2<cty>(fn, mem, info, loadchannels, dim, lf)
    switch(loadtype)
    {
        CVT(B3D_U8, uint8_t);
        CVT(B3D_U16, uint16_t);
        CVT(B3D_U32, uint32_t);
        CVT(B3D_S8, int8_t);
        CVT(B3D_S16, int16_t);
        CVT(B3D_S32, int32_t);
        CVT(B3D_FLOAT, float);
        CVT(B3D_S64, int64_t);
        CVT(B3D_U64, uint64_t);
        CVT(B3D_DOUBLE, double);
        default: ; // unsupported
    }
    return IORESULT_INVALID_FORMAT;
#undef CVT
}

// Does by far not cover all possible scenarios/image type combinations that could be loaded.
// Since ITK is (intentionally?!) missing functionality to query image properties
// such as underlying data type and number of image color components BEFORE actually
// loading and converting it at the same time.
// So we support the most common formats for now.
// Loading a monochrome image without alpha will take 4x the space compared to what would be necessary,
// but that's the only downside.
IOResult loadImage_itk(const char *fn, void **mem, Blob3DInfo *info, Blob3DType loadtype, unsigned loadchannels, unsigned dim, ITKLoadFlags lf)
{
    // Pick hopefully good enough default params suitable for most things,
    // because at this point we can't know anything about the file to be loaded, but ITK wants to know everything at compile time...
    if(loadtype == B3D_UNSPECIFIED)
        loadtype = B3D_U16; // Enough precision for medical images?
    if(!loadchannels)
        loadchannels = 4; // can't know any better; support colors+alpha whenever possible

    IOResult res = load_3(fn, mem, info, loadtype, loadchannels, dim ? dim : 3, lf);

    // MEGA HACK: because of ITK's dumb auto-conversion.
    // If the number of dimensions wasn't specified, try to guess.
    if(res == IORESULT_OK && !dim)
    {
        if(info->d == 1) // 3D -> 2D
            info->d = 0;

        if(!info->d && info->h == 1) // 2D -> 1D
            info->h = 0;
    }
    return res;
}


template<typename T, unsigned COMP, unsigned DIM>
struct ImageSave
{
    typedef typename NComp<T, COMP>::PixelType PixelType;
    typedef itk::Image<PixelType, DIM> ImageType;
    typedef itk::ImageFileWriter<ImageType> WriterType;
    typedef typename ImageType::SpacingType SpacingType;

    static typename ImageType::Pointer Save(const void *mem, const Blob3DInfo *info)
    {
        itk::ImageRegion<DIM> outRegion;
        SpacingType spacing;
        for(unsigned i = 0; i < DIM; ++i)
        {
            outRegion.SetSize(i, info->dim[i]);
            spacing[i] = info->sp[i];
        }
        typename ImageType::Pointer outImg = ImageType::New();
        outImg->SetSpacing(spacing);
        outImg->SetRegions(outRegion);
        outImg->Allocate();

        // TODO: Is there a way to do this without copying?
        const PixelType *rptr = (const PixelType*)mem;
        itk::ImageRegionIterator<ImageType> it(outImg, outRegion);
        for( ; !it.IsAtEnd(); ++it)
            it.Value() = *rptr++;

        return outImg;
    }
};

template<typename IMAGE>
static IOResult writeImage_T_itk(const char *fn, typename IMAGE::Pointer img)
{
    typedef itk::ImageFileWriter<IMAGE> WriterTypeOut;

    typename WriterTypeOut::Pointer writer = WriterTypeOut::New();
    writer->SetFileName(fn);
    writer->SetInput(img);
    writer->Update(); // throws on failure
    return IORESULT_OK;
}

template<typename T, unsigned COMP, unsigned DIM>
static IOResult convertAndWrite(const char *fn, const void *mem, const Blob3DInfo *info)
{
    typedef ImageRW<T, COMP, DIM> Saver;
    typedef typename Saver::ImageType ImageType;
    typename ImageType::Pointer img = Saver::Save(mem, info);
    return writeImage_T_itk<ImageType>(fn, img);
}

template<typename T, unsigned COMP>
static IOResult convertAndWrite_1(const char *fn, const void *mem, const Blob3DInfo *info)
{
    const unsigned dim = b3d_getDimensions(info);
    switch(dim)
    {
        case 1: return convertAndWrite<T, COMP, 1>(fn, mem, info);
        case 2: return convertAndWrite<T, COMP, 2>(fn, mem, info);
        case 3: return convertAndWrite<T, COMP, 3>(fn, mem, info);
    }
    return IORESULT_INVALID_FORMAT;
}

template<typename T>
static IOResult convertAndWrite_2(const char *fn, const void *mem, const Blob3DInfo *info)
{
    switch(info->channels)
    {
        case 1: return convertAndWrite_1<T, 1>(fn, mem, info);
        case 2: return convertAndWrite_1<T, 2>(fn, mem, info);
        case 3: return convertAndWrite_1<T, 3>(fn, mem, info);
        case 4: return convertAndWrite_1<T, 4>(fn, mem, info);
    }
    return IORESULT_INVALID_FORMAT;
}

static IOResult convertAndWrite_3(const char *fn, const void *mem, const Blob3DInfo *info)
{
#define CVT(ty, cty) case ty: return convertAndWrite_2<cty>(fn, mem, info)
    switch(info->type)
    {
        CVT(B3D_U8, uint8_t);
        CVT(B3D_U16, uint16_t);
        CVT(B3D_U32, uint32_t);
        CVT(B3D_S8, int8_t);
        CVT(B3D_S16, int16_t);
        CVT(B3D_S32, int32_t);
        CVT(B3D_FLOAT, float);
        CVT(B3D_S64, int64_t);
        CVT(B3D_U64, uint64_t);
        CVT(B3D_DOUBLE, double);
        default: ; // unsupported
    }
    return IORESULT_INVALID_FORMAT;
#undef CVT
}

IOResult writeImage_itk(const char *fn, const void *mem, const Blob3DInfo *info)
{
    return convertAndWrite_3(fn, mem, info);
}



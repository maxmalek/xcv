#include "api_plugin.h"
#include "api_types.h"
#include <string.h>
#include <stdlib.h>

#include "imageio_itk.h"
#include <itkMacro.h>
#include <sstream>

MAKE_PLUGIN_IMPORT(api);

// Rough guess via https://itk.org/Wiki/ITK/File_Formats
static const char *FMT_SUPPORT = "bmp;dicom;dcm;gdcm;ge;gipl;ipl;jpg;jpeg;mhd;nhdr;nrrd;png;tiff;tif;vtk;img;hdr;nii";


static void freeImageData(void *p)
{
    free(p);
}


static IOResult loadImageFile(const char *fn, void **pdata, Blob3DInfo *info)
{
    try
    {
        return loadImage_itk(fn, pdata, info, B3D_UNSPECIFIED, 0, 0, ITKL_NONE);
    }
    catch(itk::ExceptionObject& ex)
    {
        std::ostringstream os;
        os << ex;
        api->log(LL_ERROR, "%s", os.str().c_str());
    }
    return IORESULT_UNSPECIFIED;
}

struct SaveData
{
    Blob3DInfo info;
    Blob3DType outtype;
    const char *fn;
    IOResult result;
};

void saveImageCallback(const void *texdata, size_t bytes, void *userdata)
{
    SaveData& sav = *(SaveData*)userdata;
    Blob3DInfo outinfo = sav.info;
    outinfo.type = sav.outtype;
    sav.result = IORESULT_UNSPECIFIED;
    try
    {
        sav.result = writeImage_itk(sav.fn, texdata, &outinfo);
    }
    catch(itk::ExceptionObject& ex)
    {
        std::ostringstream os;
        os << ex;
        api->error("%s", os.str().c_str());
    }
}

static IOResult saveImageFile(const char *fn, const char *format, APITextureHandle tex)
{
    SaveData sav;
    api->texture_fill_info(tex, &sav.info);
    sav.outtype = sav.info.type;
    sav.fn = fn;
    switch(sav.outtype)
    {
        case B3D_HALF: sav.outtype = B3D_FLOAT; break;
        default: ;
    }
    api->texture_get_data_callback(tex, saveImageCallback, sav.outtype, 0, &sav, 0);
    return sav.result;
}

static const char *getImageFormatsAvailLoad()
{
    return FMT_SUPPORT;
}

static const char *getImageFormatsAvailSave()
{
    return FMT_SUPPORT;
}

static const PluginAPIFuncs funcs =
{
    PluginAPIFuncs_FillHeader,
    freeImageData,
    loadImageFile,
    saveImageFile,
    getImageFormatsAvailLoad,
    getImageFormatsAvailSave,
};

static const PluginAPIDesc desc =
{
    PluginAPIDesc_FillHeader,
    -10 // priority
};

static const PluginAPIDef plugin =
{
    PluginAPIDef_FillHeader,
    &desc,
    &funcs
};


MAKE_PLUGIN_EXPORT(plugin)

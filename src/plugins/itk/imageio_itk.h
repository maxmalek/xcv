#pragma once
struct Blob3DInfo;


// bitmask
enum ITKLoadFlags
{
    ITKL_NONE = 0,
    ITKL_RESCALE = (1 << 0)
};

// throws itk::ExceptionObject on failure
IOResult writeImage_itk(const char *fn, const void *data, const Blob3DInfo *info);
IOResult loadImage_itk(const char *fn, void **data, Blob3DInfo *info, Blob3DType loadtype, unsigned loadchannels, unsigned dim, ITKLoadFlags flags);


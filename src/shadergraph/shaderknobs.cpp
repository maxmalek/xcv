#include "shaderknobs.h"
#include "program.h"
#include <imgui/imgui.h>



bool ShaderKnob::drawUI(const ProgramUniform *u)
{
    const char *name = u->getName();
    const char *label = name;

    bool refresh = false;
    int idx = 0;
    switch(u->getType())
    {
        case GLSL::Float:
            refresh = ImGui::DragFloat(label, u->getpAs<float>(), 0.01f);
            break;
        case GLSL::vec2:
            refresh = ImGui::DragFloat2(label, u->getpAs<float>(), 0.01f);
            break;
        case GLSL::vec3:
            refresh = ImGui::DragFloat3(label, u->getpAs<float>(), 0.01f);
            break;
        case GLSL::vec4:
            refresh = ImGui::DragFloat4(label, u->getpAs<float>(), 0.01f);
            break;

        case GLSL::Int:
            refresh = ImGui::DragInt(label, u->getpAs<int>(), 0.2f);
            break;
        case GLSL::ivec2:
            refresh = ImGui::DragInt2(label, u->getpAs<int>(), 0.2f);
            break;
        case GLSL::ivec3:
            refresh = ImGui::DragInt3(label, u->getpAs<int>(), 0.2f);
            break;
        case GLSL::ivec4:
            refresh = ImGui::DragInt4(label, u->getpAs<int>(), 0.2f);
            break;

        case GLSL::Uint:
            refresh = ImGui::DragInt(label, u->getpAs<int>(), 0.2f, 0, 0x7fffffff);
            break;
        case GLSL::uvec2:
            refresh = ImGui::DragInt2(label, u->getpAs<int>(), 0.2f, 0, 0x7fffffff);
            break;
        case GLSL::uvec3:
            refresh = ImGui::DragInt3(label, u->getpAs<int>(), 0.2f, 0, 0x7fffffff);
            break;
        case GLSL::uvec4:
            refresh = ImGui::DragInt4(label, u->getpAs<int>(), 0.2f, 0, 0x7fffffff);
            break;

        // this block is all fall-through
        case GLSL::bvec4:
            refresh = ImGui::Checkbox("", u->getpAs<bool>() + idx);
            ImGui::SameLine();
            ++idx;
        case GLSL::bvec3:
            refresh = ImGui::Checkbox("", u->getpAs<bool>() + idx);
            ImGui::SameLine();
            ++idx;
        case GLSL::bvec2:
            refresh = ImGui::Checkbox("", u->getpAs<bool>() + idx);
            ImGui::SameLine();
            ++idx;
         case GLSL::Bool:
            refresh = ImGui::Checkbox(label, u->getpAs<bool>() + idx);
            break;

        default:
            ImGui::TextColored(ImColor(255,0,0,255), "Unhandled type %s", GLSL::getName(u->getType()));
            ImGui::SameLine();
            ImGui::TextUnformatted(name);
    }

    if(refresh)
        u->refresh();

    return refresh;
}

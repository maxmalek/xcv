#include "shadernode.h"
#include <imgui/imgui.h>
#include <sstream>
#include "glsl.h"
#include "shaderknobs.h"
#include "shadergrapheditor.h"


const ScriptDef ShaderNodeView::scriptdef("node", ScriptTypes::SHADERNODE, &NodeView::scriptdef, ScriptDef::PERSISTENT);


ShaderNodeView::ShaderNodeView(ShaderGraphEditor *holder, const char *name_)
    : NodeView(holder)
{
    if(name_)
        name = name_;
}

/*bool ShaderNodeView::load()
{
   return callOpt("load");
}*/

ShaderNodeView::~ShaderNodeView()
{
}

void ShaderNodeView::onDestroy()
{
    // FIXME: make sure this is ALWAYS called even if the Lua glue has been GC'd.
    // Not sure right now if this is really the case.
    callOpt("onDestroy", 0);
}

/*void ShaderNodeView::update()
{
    callOpt("update");
}*/

void ShaderNodeView::drawUI()
{
    callOpt("drawUI", 0);
}

void ShaderNodeView::setStatus(ShaderNodeStatus::Enum v)
{
    switch(v)
    {
        default:
        case ShaderNodeStatus::ERROR:
            bodyColor = glm::vec4(1.0f, 0.2f, 0.0f, 1.0f);
            break;
        case ShaderNodeStatus::GOOD:
            bodyColor = NodeView::DEFAULT_BODY_COLOR;
            break;
    }
}


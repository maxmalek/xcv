#pragma once

#include "glsl.h"

class ProgramUniform;

namespace ShaderKnob
{
    bool drawUI(const ProgramUniform *u);
};

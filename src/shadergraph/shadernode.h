#pragma once

#include "nodeeditor.h"

class ShaderGraphEditor;


namespace ShaderNodeStatus
{
    // first 2 are for cast from bool
    enum Enum
    {
        ERROR,
        GOOD,
    };
}


class ShaderNodeView : public NodeView, private ScriptType<ShaderNodeView>
{
public:
    static const ScriptDef scriptdef;

    ShaderNodeView(ShaderGraphEditor *holder, const char *name = 0);
    //virtual bool load();
    // called whenever data is requested. gets output from children, sets own output
    //virtual void update();
    virtual void drawUI();

    // graphical helpers
    void setStatus(ShaderNodeStatus::Enum);

protected:
    virtual ~ShaderNodeView();
    virtual void onDestroy();
};

#include "lua_shadergraph.h"
#include "luainternal.h"
#include "shader.h"
#include "shadergrapheditor.h"
#include "shadernode.h"
#include <imgui/imgui.h>
#include "shaderknobs.h"


struct LuaShaderNode : public LuaClassBind<ShaderNodeView>
{
    static const luaL_Reg methods[];
};

const luaL_Reg LuaShaderNode::methods[] =
{
    {NULL, NULL},
};



struct LuaShaderGraphEditor : public LuaClassBind<ShaderGraphEditor>
{
    static const luaL_Reg methods[];

    luaFunc(new)
    {
        luaReturnObj(new ShaderGraphEditor);
    }

    luaFunc(newNode)
    {
        luaReturnObj(This(L)->newNode());
    }

    static void _addnode(lua_State *L, void *p)
    {
        std::vector<std::string>& v = *(std::vector<std::string>*)p;
        v.push_back(luaL_checkstring(L, -1));
    }
};

const luaL_Reg LuaShaderGraphEditor::methods[] =
{
    luaRegisterMethod(LuaShaderGraphEditor, new),
    luaRegisterMethod(LuaShaderGraphEditor, newNode),
};



luaFunc(autoDrawControls)
{
    ShaderProgramWrapper *sh = luaGetObj<ShaderProgramWrapper>(L);
    const ShaderProgram *p = sh->getProgram().content();
    const int n = lua_gettop(L);
    bool changed = false;
    for(int i = 2; i <= n; ++i)
    {
        const char *s = luaL_checkstring(L, i);
        if(const ProgramUniform *u = p->lookupUniform(s))
        {
            changed = ShaderKnob::drawUI(u) || changed;
        }
    }
    luaReturnBool(changed);
}


static const luaL_Reg sglib[] =
{
    luaRegister(autoDrawControls),
    {NULL, NULL}
};

int register_lua_shadergraph(lua_State *L)
{
    LuaStackCheck(L, 1);

    luaL_newlib(L, sglib);
    luaRegisterClassInLib<LuaShaderNode>(L, -1);
    luaRegisterClassInLib<LuaShaderGraphEditor>(L, -1);
    //luaRegisterEnumInLib(L, gpulibenum, -1);
    return 1;
}

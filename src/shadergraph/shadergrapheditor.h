#pragma once

#include "nodeeditor.h"
#include "shadernode.h"

class ShaderGraphEditor : public NodeEditorView, private ScriptType<ShaderGraphEditor>
{
public:
    static const ScriptDef scriptdef;

    ShaderGraphEditor();
    virtual ~ShaderGraphEditor();
    ShaderNodeView *newNode(); // create and add

protected:
    virtual NodeView *createNode();
};


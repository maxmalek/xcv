#include "shadergrapheditor.h"
#include <algorithm>
#include "shadernode.h"
#include <imgui/imgui.h>
#include "luainternal.h"
#include "glsl.h"

const ScriptDef ShaderGraphEditor::scriptdef("editor", ScriptTypes::SHADERGRAPHEDITORWINDOW, &NodeEditorView::scriptdef);


ShaderGraphEditor::ShaderGraphEditor()
{
}

ShaderGraphEditor::~ShaderGraphEditor()
{
}

ShaderNodeView * ShaderGraphEditor::newNode()
{
    NodeView *n = _createNode();
    assert(dynamic_cast<ShaderNodeView*>(n));
    return static_cast<ShaderNodeView*>(n);
}

NodeView * ShaderGraphEditor::createNode()
{
    return new ShaderNodeView(this);
}

#include "plugin.h"
#include "pluginapi.h"
#include "stdlib.h"
#include <SDL_loadso.h>
#include <string>
#include <algorithm>
#include "log.h"
#include <assert.h>

const char * const Plugin::Ext =
#ifdef _WIN32
    "dll"
#elif __linux__
    "so"
#elif __MACOSX__
    "dylib"
#else
#  error Must specify dynamic library extension for platform
#endif
;


const ScriptDef Plugin::scriptdef("pluginclass", ScriptTypes::PLUGIN, NULL, ScriptDef::PERSISTENT);

Plugin::List Plugin::s_loaded;
Plugin::List Plugin::s_loadedSorted;

static bool _pluginSortOrder(const Plugin *a, const Plugin *b)
{
    return a->getPriority() > b->getPriority();
}


Plugin * Plugin::Load(const char * name)
{
    logdebug2("Loading plugin %s", name);

    const char *internalname = strchr(name, '/');
    if(internalname)
        ++internalname;
    else
        internalname = name;
    if(!*internalname)
        return NULL; // eh?

    for(size_t i = 0; i < s_loaded.size(); ++i)
        if(!strcmp(s_loaded[i]->getName(), internalname))
            return s_loaded[i];


    void *p = SDL_LoadObject(name);
    if(!p)
    {
        // Try again with added platform extension (dll, so, dylib, ...)
        const size_t len = strlen(name);
        std::string fn;
        fn.reserve(len + 8);

        fn += name;
        fn += '.';
        fn += Ext;
        p = SDL_LoadObject(fn.c_str());

        if(!p)
            return NULL;
    }

    logdebug2("Plugin[%s]: Loaded handle %p", internalname, p);

    Plugin *pl = new Plugin(name, internalname, p);
    pl->query(); // Even if the initial query fails there might be other things loaded on demand later

    s_loaded.push_back(pl);
    s_loadedSorted.push_back(pl);
    std::sort(s_loadedSorted.begin(), s_loadedSorted.end(), _pluginSortOrder);

    return pl;
}

Plugin::Plugin(const char *filename, const char *name, void *handle)
    : _handle(handle), _name(name), _filename(filename), _def(NULL), _localf(NULL), _priority(0), _imgload(NULL), _imgsave(NULL)
{
}

Plugin::~Plugin()
{
    unload();
    if(_handle)
    {
        logdebug2("~Plugin[%s]: UnloadObject handle %p", _name.c_str(), _handle);
        SDL_UnloadObject(_handle);
    }
}

template<typename V, typename F>
static void _clearPtr(V& v, const F& f)
{
    typename V::iterator iend = std::remove(v.begin(), v.end(), f);
    v.resize(std::distance(v.begin(), iend));
}

static bool _sanityCheck(const PluginAPIFuncs *f, const char *name)
{
    if(f->_magic != PLUGIN_APIFUNCS_MAGIC)
    {
        logerror("Plugin %s: Magic is %08X, should be %08X", name, f->_magic, PLUGIN_APIFUNCS_MAGIC);
        return false;
    }

    if(f->loadImageFile && !f->freeImageData)
        return false;

    return true;
}

#define SUBSTRUCT(name) offsetof(PluginAPIDef, name) < def->structsize ? def->name : NULL
#define ASSIGN(var, structname, thestruct, member) do { if(offsetof(structname, member) + sizeof(structname::member) <= thestruct->structsize) { (var) = thestruct->member; } } while(0)



// Return true if plugin functions were directly exported
bool Plugin::query()
{
    QueryPluginInterfaceFunc qf = (QueryPluginInterfaceFunc)SDL_LoadFunction(_handle, "QueryPluginInterface");
    if(!qf)
        return false;

    assert(pluginapi::exports);
    const PluginAPIDef *def = qf(PLUGIN_API_VERSION);

    if(!def || !def->structsize)
        return false;
    if(def->_magic != PLUGIN_DEF_MAGIC)
    {
        logerror("Plugin %s: Magic is %08X, should be %08X", _name.c_str(), def->_magic, PLUGIN_DEF_MAGIC);
        return false;
    }

    // TODO: ADD AS NECESSARY: Version checks
    int prio = 0;
    if(const PluginAPIDesc *desc = SUBSTRUCT(desc))
    {
        if(desc->_magic == PLUGIN_APIDESC_MAGIC)
        {
            ASSIGN(prio, PluginAPIDesc, desc, priority);
        }
        else
            logerror("Plugin %s: ApiDesc magic is %08X, should be %08X", _name.c_str(), desc->_magic, PLUGIN_APIDESC_MAGIC);
    }

    _priority = prio;

    // Check imports
    // If the plugin doesn't expose this, it doesn't need any of the exported core API (and is effectively much more compatible)
    SetPluginImportsFunc sif = (SetPluginImportsFunc)SDL_LoadFunction(_handle, "SetPluginImports");
    if(sif)
    {
        if(def->importsize > PLUGIN_IMPORTS_SIZE)
            logerror("Skipping plugin %s: Expects a larger imports table (it was probably compiled for a newer version)", _name.c_str());
        else
            sif(pluginapi::exports);
    }


    // Copy function pointer struct locally;
    // This is so that extra entries in shorter structs are zero'd out, and longer structs won't cause a problem
    pluginapi::FuncsPrio *localf = NULL;
    if(const PluginAPIFuncs *f = SUBSTRUCT(funcs))
    {
        if(!_sanityCheck(f, _name.c_str()))
            return false;

        localf = new pluginapi::FuncsPrio;
        if(!localf)
            return false;

        {
            PluginAPIFuncs *pf = localf; // this offsets the pointer. important!
            memset(pf, 0, sizeof(PluginAPIFuncs));
            memcpy(pf, f, std::min<size_t>(f->structsize, sizeof(PluginAPIFuncs)));
        }
        localf->priority = prio;
        localf->pluginfile = _filename;

        pluginapi::funcs.push_back(localf);
        std::sort(pluginapi::funcs.begin(), pluginapi::funcs.end(), pluginapi::FuncsPrio::Sort);

        const char *s;
        bool fix = false;
        if(localf->getImageFormatsAvailLoad && (s = localf->getImageFormatsAvailLoad()) && *s)
        {
            _imgload = s;
            fix = true;
            pluginapi::fileExtLoad += pluginapi::SEP;
            pluginapi::fileExtLoad += s;
        }
        if(localf->getImageFormatsAvailSave && (s = localf->getImageFormatsAvailSave()) && *s)
        {
            _imgsave = s;
            fix = true;
            pluginapi::fileExtSave += pluginapi::SEP;
            pluginapi::fileExtSave += s;
        }
        if(fix)
            pluginapi::_fixupExt();
    }
    
    _localf = localf;
    _def = def;

    return true;
}

template<typename V, typename E>
static void _removeFromVec(V& v, const E& e)
{
    typename V::iterator iend = std::remove(v.begin(), v.end(), e);
    v.resize(std::distance(v.begin(), iend));
}


void Plugin::unload()
{
    if(_localf)
    {
        _removeFromVec(pluginapi::funcs, _localf);
        delete _localf;
        _localf = NULL;
    }

    _removeFromVec(s_loaded, this);
    _removeFromVec(s_loadedSorted, this);

    // Fix file extension list
    {
        const char *s;
        bool fix = false;
        if(_imgload)
        {
            pluginapi::fileExtLoad.clear();
            fix = true;
            for(size_t i = 0; i < s_loaded.size(); ++i)
                if( (s = s_loaded[i]->_imgload) )
                {
                    fix = true;
                    pluginapi::fileExtLoad += s;
                }
        }
        if(_imgsave)
        {
            pluginapi::fileExtSave.clear();
            fix = true;
            for(size_t i = 0; i < s_loaded.size(); ++i)
                if( (s = s_loaded[i]->_imgsave) )
                {
                    fix = true;
                    pluginapi::fileExtSave += s;
                }
        }
        if(fix)
            pluginapi::_fixupExt();
    }

    _imgload = NULL;
    _imgsave = NULL;
}

void * Plugin::getFunction(const char * fn) const
{
    return SDL_LoadFunction(_handle, fn);
}


#pragma once

// This overrides the define in api_funcs.h
#define IMPORT_API static

#include "api_funcs.h"

extern const PluginApiImportFuncs * plugin_export_api;

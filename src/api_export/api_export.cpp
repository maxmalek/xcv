#include "api_export_priv.h"

#include "texture.h"
#include "gpubuffer.h"
#include "platform.h"
#include "luainternal.h"
#include "pluginapi.h"
#include "log.h"


IMPORT_API void api_msgbox(const char *msg, const char *title)
{
    platform::msgbox(msg, title);
}

IMPORT_API bool api_msgbox_yesno(const char *msg, const char *title)
{
    return platform::msgboxYesNo(msg, title);
}

IMPORT_API void api_error(const char *fmt, ...)
{
    va_list va;
    va_start(va, fmt);
    vlogx(LL::ERR_MSGBOX, 1, fmt, va);
    va_end(va);
}

static LL::Enum translateLL(LogLevel lvl)
{
    switch(lvl)
    {
        case LL_ERROR: return LL::ERR;
        case LL_NORMAL: return LL::NORMAL;
        case LL_INFO: return LL::INFO;
        case LL_DEBUG: return LL::DEBUG;
        case LL_DEV: return LL::DEBUG2;
    }
    assert(false);
    return LL::NORMAL;
}

IMPORT_API void api_log(LogLevel lvl, const char *fmt, ...)
{
    va_list va;
    va_start(va, fmt);
    vlogx(translateLL(lvl), 1, fmt, va);
    va_end(va);
}

IMPORT_API void api_texture_fill_info(APITextureHandle ptex, Blob3DInfo *info)
{
    const TextureAny *tex = (TextureAny*)ptex;
    tex->fillInfo(*info);
}

IMPORT_API unsigned api_texture_get_dimensions(APITextureHandle ptex)
{
    const TextureAny *tex = (TextureAny*)ptex;
    return tex->dimensions();
}

IMPORT_API size_t api_texture_info_get_size_bytes(const Blob3DInfo * info, unsigned miplevel)
{
    return TextureAny::SizeInBytes(*info, miplevel);
}

IMPORT_API size_t api_texture_get_size_bytes(APITextureHandle ptex, unsigned miplevel)
{
    const TextureAny *tex = (TextureAny*)ptex;
    return tex->getSizeBytes(miplevel);
}

IMPORT_API size_t api_texture_copy_data(void * dst, size_t bufsize, APITextureHandle ptex, Blob3DType outtype, unsigned outchannels, unsigned miplevel)
{
    const TextureAny *tex = (TextureAny*)ptex;
    tex->adjustParams(outtype, outchannels);

    TexAnyCRef usetex;
    if(outtype == tex->datatype() && outchannels == tex->channels())
        usetex = tex;
    else
        usetex = (TextureAny*)tex->convert(outtype, outchannels, TEX_NONE, NULL);

    if(!usetex)
        return 0;

    usetex->download(dst, bufsize, miplevel);
    return TextureAny::SizeInBytes(usetex->internalsize(), outtype, outchannels, miplevel);
}

IMPORT_API void api_texture_get_data_callback(APITextureHandle ptex, APITextureGetDataCallback cb, Blob3DType outtype, unsigned outchannels, void * userdata, unsigned miplevel)
{
    const TextureAny *tex = (const TextureAny*)ptex;
    tex->adjustParams(outtype, outchannels);
    const size_t sz  = TextureAny::SizeInBytes(tex->internalsize(), outtype, outchannels, miplevel);

    const void *p = NULL;
    GPUBufRef buf;
    {
        TexAnyCRef usetex;
        if(outtype == tex->datatype() && outchannels == tex->channels())
            usetex = tex;
        else
            usetex = (TextureAny*)tex->convert(outtype, outchannels, TEX_NONE, tex->getSwizzleMask());
        if(usetex)
        {
            buf = GPUBufferRaw::FromTexture(*usetex, GPUBufferRaw::MAP_READ);
            usetex = NULL;
            p = buf->mapRead();
        }
    }

    cb(p, sz, userdata);
}

IMPORT_API lua_State *api_get_lua()
{
    assert(pluginapi::lua);
    return pluginapi::lua;
}


static const PluginApiImportFuncs plugin_exports[] =
{
    api_msgbox,
    api_msgbox_yesno,
    api_error,
    api_log,
    api_texture_fill_info,
    api_texture_info_get_size_bytes,
    api_texture_get_size_bytes,
    api_texture_copy_data,
    api_texture_get_data_callback,
    api_get_lua,

    // Terminator, not part of the struct decl
    NULL
};

/* extern */ const PluginApiImportFuncs *plugin_export_api = plugin_exports;

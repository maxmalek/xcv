// Internal representation of a plugin

#pragma once

#include "api_plugin.h"
#include <vector>
#include <string>
#include "luaobj.h"

struct PluginAPIDef;
struct PluginAPIFuncs;

class Plugin : public ScriptableT<Plugin>
{
public:
    static const ScriptDef scriptdef;
    typedef std::vector<Plugin*> List;

    static Plugin *Load(const char *name); // name without extension
    static const char * const Ext;

    void unload();
    const char *getName() const { return _name.c_str(); }
    const char *getFileName() const { return _filename.c_str(); }
    int getPriority() const { return _priority; }
    void *getFunction(const char *fn) const;

    static const List& GetLoaded() { return s_loaded; }
    static const List& GetLoadedSorted() { return s_loadedSorted; }
    ~Plugin();

private:
    Plugin(const char *filename, const char *name, void *handle);

    bool query();
    void unregister();

    std::string _name, _filename;
    void *_handle;
    const PluginAPIDef *_def;
    PluginAPIFuncs *_localf;
    int _priority;
    const char *_imgload, *_imgsave;

    static List s_loaded;
    static List s_loadedSorted;
};


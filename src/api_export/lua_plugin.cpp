#include "lua_plugin.h"
#include "luainternal.h"
#include "plugin.h"


struct LuaPlugin : public LuaClassBind<Plugin>
{
    static const luaL_Reg methods[];

    luaFunc(unload)
    {
        Plugin *p = This(L);
        p->unload();
        assert(p->refcount() == 1); // Next line will cause this to drop to 0 and delete it
        p->unbindScript();
        luaReturnNil();
    }
    luaFunc(name)
    {
        luaReturnStr(This(L)->getName());
    }

};

const luaL_Reg LuaPlugin::methods[] =
{
    luaRegisterMethod(LuaPlugin, unload),
    luaRegisterMethod(LuaPlugin, name),
};

luaFunc(load)
{
    const char *fn = luaL_checkstring(L, 1);
    Plugin *p = Plugin::Load(fn);
    luaReturnObj(p);
}

luaFunc(isloaded)
{
    const char *name = lua_checkrealstring(L, 1);
    bool ret = false;
    const Plugin::List& pl = Plugin::GetLoaded();
    for(size_t i = 0; i < pl.size(); ++i)
    {
        Plugin *p = pl[i];
        if(!strcmp(p->getName(), name) || !strcmp(p->getFileName(), name))
        {
            ret = true;
            break;
        }
    }
    luaReturnBool(ret);
}

luaFunc(listloaded)
{
    const bool sorted = getBool(L, 1);
    const Plugin::List& pl = sorted ? Plugin::GetLoadedSorted() : Plugin::GetLoaded();
    lua_createtable(L, 0, (int)pl.size());
    for(size_t i = 0; i < pl.size(); ++i)
    {
        luaPushObj(pl[i]);
        lua_rawseti(L, -2, i+1);
    }
    return 1;
}

// Searcher for require()
luaFunc(_searcher)
{
    luaL_checkstring(L, 1);
    lua_settop(L, 1);
    lua_pushliteral(L, "luaopen_");
    lua_pushvalue(L, 1);
    lua_concat(L, 2);
    const char *fn = luaL_checkstring(L, -1);

    const Plugin::List& pl = Plugin::GetLoadedSorted();
    for(size_t i = 0; i < pl.size(); ++i)
        if(const Plugin *p = pl[i])
            if(void *f = p->getFunction(fn))
            {
                logdebug2("Found '%s' in '%s'", fn, p->getFileName());
                lua_pop(L, 1);
                lua_pushcfunction(L, (lua_CFunction)f);
                lua_pushstring(L, p->getFileName());
                return 2;
            }
    lua_settop(L, 0);
    return 0;
}


static const luaL_Reg pluginlib[] =
{
    luaRegister(load),
    luaRegister(isloaded),
    luaRegister(listloaded),
    luaRegister(_searcher),
    {NULL, NULL}
};

int register_lua_plugin(lua_State *L)
{
    LuaStackCheck(L, 1);

    luaL_newlib(L, pluginlib);
    lua_pushstring(L, Plugin::Ext);
    lua_setfield(L, -2, "ext");

    luaRegisterClass<LuaPlugin>(L);

    return 1;
}

// QUick zero-impact self text during startup
// Ensures that some peculiarities that can't be checked at compile time work correctly

#include "selftest.h"
#include "log.h"
#include "texture.h"
#include "gpubuffer.h"
#include "program.h"
#include "framebuffer.h"
#include "luainterface.h"
#include "luainternal.h"
#include "pluginapi.h"

template<typename T> void nullderef()
{
    CountedPtr<T> ref;
    // Must not crash, see DeviceObject::id()
    volatile unsigned id = ((T*)NULL)->id();
    id = ref->id();
    (void)id;
}

static volatile unsigned dtorFail = 0;

struct DtorTest
{
    DtorTest() { ++dtorFail; }
    ~DtorTest() { --dtorFail; }
};

static int failfunc(lua_State *L)
{
    DtorTest test; // on-stack object must be destroyed properly
    throw DtorTest(); // thrown object must be destroyed too
    return 0;
}

static void checkLuaExceptions()
{
    lua_State *L = luaL_newstate();
    lua_pushcfunction(L, failfunc);
    int res = lua_pcall(L, 0, 0, 0);
    RELEASE_ASSERT(res != LUA_OK, "Selftest: Did not cause a Lua error when an exception was thrown");
    RELEASE_ASSERT(dtorFail == 0, "Selftest: Lua did not properly catch a C++ exception; check compiler settings");
    lua_close(L);
}

void selftest::perform()
{
    logdebug2("Performing startup self-test... If this crashes, something was mis-compiled.");
    nullderef<Texture2D>(); // 1D, 2D, 3D are the same
    nullderef<GPUBufferRaw>();
    nullderef<GPUBuffer<float> >();
    nullderef<ShaderProgram>();
    nullderef<ShaderObject>();

    // Check that Lua properly handles C++ exceptions. This requires Lua to be compiled as C++ code.
    checkLuaExceptions();

    RELEASE_ASSERT(pluginapi::exports, "Plugin exports were not compiled in properly?!");

    logdebug2("Self-test done, all good.");
}

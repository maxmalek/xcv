#include "runtime.h"
#include <stdlib.h>
#include <SDL.h>
#include "log.h"
#include "jobq.h"
#include "macros.h"
#include "ui.h"
#include "luainterface.h"
#include <assert.h>
#include <imgui/imgui.h>
#include "timer.h"
#include "pluginapi.h"
#include "platform.h"
#include "selftest.h"
#include "api_export_priv.h"

// If this is defined, spawn the runtime in a different thread.
// This allows to kill the new "quasi-main" thread in case of problems,
// which makes all thread locals go away and might help to reset the graphics driver
// more throughly. Sadly, this didn't help to work around problems caused by the
// Nvidia 372.70 driver last time i tested this.
// So the extra thread is apparently of no use for now and can stay disabled.
// (Plus there was a deadlock problem when exit() is called)

//#define USE_EXTRA_THREAD




typedef void (*ThFunc)(void*);

#ifdef USE_EXTRA_THREAD
#include "threading.h"
class RtThread;
#endif

namespace Runtime
{
    static Flags s_flags = NONE;
    static bool inited = false;
    static bool once = false;
    static LuaInterface *lua = NULL;
    static FrameTimer ftimer;

    static ThFunc thnext;
#ifdef USE_EXTRA_THREAD
    static RtThread *th;
    static Waitable *thsync;
    static ThFunc thrun;
    static void *thparam;
    static bool thquit = false;
#endif

    static void check(bool ok, const char *what)
    {
        if(!ok)
        {
            logerror("Runtime: Failed to init %s", what);
            exit(1);
        }
    }

    static void _InitScripting();

#ifdef USE_EXTRA_THREAD
    static void _InitThread();
    static void _EnsureThread();
#endif

    static void th_Init(void*);
    static void th_Update(void*);
    static void th_RunScript(void*);
    static void th_Shutdown(void*);
    static void runThreadWait(ThFunc f, void *param = NULL);
}

#ifdef USE_EXTRA_THREAD
class RtThread : public Thread
{
    // Inherited via Thread
    virtual void run()
    {
        MTGuard lock(Runtime::thsync);
        while(!Runtime::thquit)
        {
            if(Runtime::thrun)
            {
                do
                {
                    ThFunc f = Runtime::thrun;
                    void *p = Runtime::thparam;
                    Runtime::thrun = NULL;
                    Runtime::thparam = NULL;
                    f(p); // this may set thrun & thparam to something new
                }
                while(Runtime::thrun);

                Runtime::thsync->broadcast();
                if(Runtime::thquit)
                    break;
            }
            Runtime::thsync->wait();
        }
    }
};

static void Runtime::_EnsureThread()
{
    if(!th)
    {
        DEBUG_LOG("Runtime::th is NULL, start new thread");
        _InitThread();
    }
}

static void Runtime::_InitThread()
{
    assert(!th);
    thquit = false;
    th = new RtThread;
    th->launch();
}
#endif

static void Runtime::runThreadWait(ThFunc f, void *param)
{
#ifdef USE_EXTRA_THREAD
    _EnsureThread();
#endif

    // Run any leftover job, if there is one
    if(thnext)
    {
        DEBUG_LOG("Runtime: There is a leftover job");
        ThFunc f = thnext;
        thnext = NULL;
        runThreadWait(f);
        DEBUG_LOG("Runtime: Leftover job done");
    }

#ifdef USE_EXTRA_THREAD
    {
        MTGuard lock(thsync);
        thrun = f;
        thparam = param;
        thsync->broadcast();
        while(thrun)
            thsync->wait();
    }

    if(thquit)
    {
        thsync->broadcast();
        delete th;
        th = NULL;
    }
#else
    f(param);
#endif
}

static void Runtime::th_Init(void*)
{
    if(s_flags & WITH_GPU)
        check(ui::Init(), "UI/GPU support");

    if(s_flags & WITH_LUA)
        _InitScripting();

    inited = true;
}

void Runtime::Init(Flags flags)
{
    if(inited)
    {
        logerror("Runtime already inited");
        return;
    }

    pluginapi::exports = plugin_export_api;

    selftest::perform();

    if(!once)
    {
        atexit(Runtime::Shutdown);
        once = true;
    }

    s_flags = flags;

    logdebug("Runtime init");
    SDL_SetHint("SDL_HINT_TIMER_RESOLUTION", "0");
    check(SDL_Init(0) == 0, "SDL");
    if(s_flags & WITH_MT)
    {
        int cache = SDL_GetCPUCacheLineSize();
        int cpus =  platform::getCPUCount();
        loginfo("CPU cores detected: %d", cpus);
        logdebug2("CPU cache line size: %d", cache);
        if(cache > ARCH_CACHE_LINE_SIZE)
            logerror("[performance warning] CPU cache line size: %u (compiled for %u)", cache, ARCH_CACHE_LINE_SIZE);
        jobq::StartWorkers(cpus - 1);
    }

#ifdef USE_EXTRA_THREAD
    if(!thsync)
        thsync = new Waitable;
#endif

    runThreadWait(th_Init);
}

Runtime::UpdateResult Runtime::Update()
{
    UpdateResult res;
    runThreadWait(th_Update, &res);
    return res;
}

static void _rebootUI(void*)
{
    ui::_RebootSubsystem();
}

void Runtime::th_Update(void *param)
{
#ifdef _DEBUG
    // Ensure that no leftovers accumulate on the Lua stack
    _LuaStackCheck stkchk(lua ? lua->_getInternalState() : NULL, 0);
#endif

    const ui::UpdateResultFlags uflags = ui::Update(ftimer.getdt());

    unsigned res = 0;
    if(uflags & ui::WINDOWS_OPEN)
        res |= WINDOWS_OPEN;
    if(uflags & ui::NEED_REBOOT)
    {
#ifdef USE_EXTRA_THREAD
        DEBUG_LOG("Runtime: UI update returned ui::NEED_REBOOT, exiting thread...");
        thquit = true;
#endif
        thnext = _rebootUI; // run this next frame
    }

    UpdateResult *pres = (UpdateResult*)param;
    *pres = (UpdateResult)res;
}

static void Runtime::_InitScripting()
{
    if(!lua)
    {
        lua = new LuaInterface;
        char buf[32];
        sprintf(buf, "%u.%u.%u", VER_MAJOR, VER_MINOR, VER_PATCHLEVEL);
        lua->setGlobal("XCV_VERSION", &buf[0]);
        pluginapi::lua = lua->_getInternalState();
    }

    if(!lua->loadFile("init.lua")) // FIXME: move this elsewhere?
        logerror("Failed to exec init.lua");
}

struct RunScriptPass
{
    const char *fn;
    bool good;
};

LuaInterface *Runtime::GetLuaInterface()
{
    return lua;
}

bool Runtime::RunScript(const char * fn)
{
    assert(lua);
    RunScriptPass pass = { fn, false };
    runThreadWait(th_RunScript, &pass);
    return pass.good;
}

static void Runtime::th_RunScript(void *param)
{
    RunScriptPass *pass = (RunScriptPass*)param;
    pass->good = lua->loadFile(pass->fn);
}

void Runtime::Shutdown()
{
    if(!inited)
        return;

    if(lua)
        lua->call("onQuit");

    runThreadWait(th_Shutdown);

    if(s_flags & WITH_MT)
        jobq::StopWorkers();

#ifdef USE_EXTRA_THREAD
    delete thsync;
    thsync = NULL;
#endif

    SDL_Quit();
    inited = false;
}

static void Runtime::th_Shutdown(void*)
{
    ui::CloseAllWindows();

    // Delete this AFTER windows are closed, but BEFORE the rest of the UI subsystem goes down
    delete lua;
    lua = NULL;
    pluginapi::lua = NULL;

    if(s_flags & WITH_GPU)
        ui::Shutdown();

#ifdef USE_EXTRA_THREAD
    thquit = true;
#endif
}

#include "ui.h"
#include "renderer.h"
#include "window.h"
#include <SDL.h>
#include <imgui/imgui.h>
#include "imgui_impl_sdl.h"
#include <stdio.h>
#include "log.h"
#include "uiconsole.h"
#include "longjobview.h"
#include "deviceobject.h"
#include "platform.h"

static void Msgbox_Log_callback(int level, const char *message, size_t len, void*)
{
    if(level == LL::ERR_MSGBOX)
        platform::msgbox(message, "Error");
}

namespace ui
{

SDL_Window *s_win = NULL;
Renderer *s_renderer = NULL;

static const int sdlSubsystems = SDL_INIT_VIDEO | SDL_INIT_EVENTS;

static bool InitRenderer()
{
    if(!Renderer::StaticInit())
        return false;

    if(!Window::StaticInit())
        return false;

    if(!InitStaticWindow())
        return false;

    if(!InitStaticRenderer())
        return false;

    return true;
}

bool Init()
{
    if(SDL_Init(sdlSubsystems))
    {
        logerror("ui:SDL_Init() failed:\n%s", SDL_GetError());
        return false;
    }

    if(SDL_VideoInit(NULL) < 0)
    {
        logerror("ui: SDL_VideoInit() failed:\n%s", SDL_GetError());
        return false;
    }

    const unsigned N = SDL_GetNumVideoDrivers();
    logdebug2("Available video drivers (%u):", N);
    for(unsigned i = 0; i < N; ++i)
        logdebug2(" [%u]: %s", i, SDL_GetVideoDriver(i));

    loginfo("ui: Using video driver [%s]", SDL_GetCurrentVideoDriver());

#ifndef _DEBUG
    ImGui::GetIO().IniFilename = NULL;
#endif

    log_addLogCallback(UIConsole::Log_callback, NULL);
    log_addLogCallback(Msgbox_Log_callback, NULL);

    return InitRenderer();
}

bool InitStaticWindow()
{
    s_win = SDL_CreateWindow("", 0, 0, 0, 0, SDL_WINDOW_HIDDEN | SDL_WINDOW_OPENGL);
    if(!s_win)
    {
        logerror("InitStaticWindow: Failed to create window:\n%s", SDL_GetError());
        return false;
    }
    return true;
}

bool InitStaticRenderer()
{
    // FIXME: this will likely not work under linux if there is no X session.
    // -> Need a way to create a GL context without a window?
    assert(s_win);

    Window::SetStaticRenderer(NULL);
    s_renderer = Renderer::Create(s_win, Renderer::DEBUG);
    if(s_renderer)
    {
        Window::SetStaticRenderer(s_renderer);
        return true;
    }

    logerrorbox("InitStaticRenderer: Failed to init renderer");
    Renderer::ShowCompatFailReason(s_win);
    SDL_DestroyWindow(s_win);
    s_win = NULL;
    return false;
}

// To be called on device or driver reset -- attempt to recover
void _KillSubsystem()
{
    DEBUG_LOG("ui::_KillSubsystem() begin");

    ImGui_Impl_GL45_ForgetDeviceObjects();
    DeviceObject::HandleDeviceReset(false); // first, clear out everything...

    DEBUG_LOG("Shutdown static renderer");

    Window::SetStaticRenderer(NULL);
    Renderer::Delete(s_renderer);
    s_renderer = NULL;
    if(s_win)
    {
        SDL_DestroyWindow(s_win);
        s_win = NULL;
    }

    DEBUG_LOG("Shutdown all window renderers");

    Window::TempCloseAll();

    Renderer::StaticShutdown();

    SDL_QuitSubSystem(SDL_INIT_VIDEO);
    assert(!SDL_WasInit(SDL_INIT_VIDEO));
   DEBUG_LOG("ui::_KillSubsystem() done");
}

bool _RebootSubsystem()
{
    DEBUG_LOG("ui::_RebootSubsystem() begin");
    if(SDL_InitSubSystem(SDL_INIT_VIDEO))
    {
        logerror("Failed to bring SDL video subsystem back up!");
        return false;
    }

    if(!InitRenderer())
    {
        logerror("Failed to bring static renderer back up!");
        return false;
    }

    DEBUG_LOG("Static renderer is back up, re-init objects...");

    DeviceObject::HandleDeviceReset(true); // ... then re-init

    DEBUG_LOG("Recreating window renderers...");

    const unsigned failed = Window::TempReopenAll();
    if(failed)
        return false;

    DEBUG_LOG("ui::_RebootSubsystem() successful!");
    return true;
}


UpdateResultFlags Update(float dt)
{
    Window::HandleEvents();
    g_mainLongExec.update();
    unsigned res = 0;
    if(!Window::UpdateAll(dt))
    {
        res |= NEED_REBOOT;
        _KillSubsystem();
    }
    if(Window::IsAnyOpen())
        res |= WINDOWS_OPEN;
    return (UpdateResultFlags)res;
}

void Shutdown()
{
    if(s_renderer)
        s_renderer->makeCurrent();

    log_removeLogCallback(UIConsole::Log_callback);

    Window::CloseAllNow();

    ImGui_Impl_GL45_Shutdown();

    Window::SetStaticRenderer(NULL);
    Renderer::Delete(s_renderer);
    if(s_win)
    {
        SDL_DestroyWindow(s_win);
        s_win = NULL;
    }

    Renderer::StaticShutdown();
    SDL_VideoQuit();
    SDL_QuitSubSystem(sdlSubsystems);
}

void CloseAllWindows()
{
    Window::CloseAllNow();
    if(s_renderer)
        s_renderer->makeCurrent();
}

} // end namespace ui

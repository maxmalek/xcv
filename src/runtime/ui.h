#pragma once

struct SDL_Window;

namespace ui
{
    enum UpdateResultFlags
    {
        WINDOWS_OPEN = 0x0001,
        NEED_REBOOT = 0x0002,
    };
    bool Init();
    bool InitStaticRenderer();
    bool InitStaticWindow();
    UpdateResultFlags Update(float dt);
    void Shutdown();
    void CloseAllWindows();

    void _KillSubsystem();
    bool _RebootSubsystem();
}



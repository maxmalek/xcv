// Full-fledged Lua interface with all extension functions

#pragma once

#include "luastate.h"


class LuaInterface : public LuaState
{
public:
    LuaInterface();
    virtual ~LuaInterface();
};

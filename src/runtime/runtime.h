#pragma once

class LuaInterface;

namespace Runtime
{
    enum Version
    {
        VER_MAJOR = 0,
        VER_MINOR = 1,
        VER_PATCHLEVEL = 0
    };
    enum Flags
    {
        NONE = 0,
        WITH_GPU        = 0x00000001,
        WITH_MT         = 0x00000002,
        WITH_LUA        = 0x00000004,

        ALL             = WITH_GPU | WITH_MT | WITH_LUA
    };
    enum UpdateResult
    {
        WINDOWS_OPEN    = 0x01,
    };
    void Init(Flags flags = ALL);
    UpdateResult Update();
    bool RunScript(const char *fn);
    LuaInterface *GetLuaInterface();
    void Shutdown();
};


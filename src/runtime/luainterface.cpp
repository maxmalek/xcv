#include "luainterface.h"
#include "luainternal.h"

// extensions
#include "lua_gpu.h"
#include "lua_glsl.h"
#include "lua_gui.h"
#include "lua_syswin.h"
#include "lua_shadergraph.h"
#include "lua_imgui.h"
#include "lua_scene.h"
#include "lua_numvec.h"
#include "lua_glm.h"
#include "lua_plugin.h"

// own headers
#include <string>
#include "platform.h"

struct LuaRegEx
{
    const char *name;
    lua_CFunction func;
    int unpackglobal;
};

static const LuaRegEx extlibs[] =
{
    {"gpu", register_lua_gpu, 0},
    {"glsl", register_lua_glsl, 0},
    {"gui", register_lua_gui, 0},
    {"sys", register_lua_syswin, 0},
    {"shadergraph", register_lua_shadergraph, 0},
    {"imgui", register_lua_imgui, 0},
    {"render", register_lua_scene, 0},
    {"numvec", register_lua_numvec, 1},
    {"glm", register_lua_glm, 0},
    {"plugin", register_lua_plugin, 0},
    {NULL, NULL}
};

static void unpackglobal(lua_State *L)
{
    // ...[_G][key][value]
    lua_pushvalue(L, -2);
    lua_pushvalue(L, -2);
    // ...[_G][key][value][key][value]
    lua_settable(L, -5); // _G[key] = value
    // ...[_G][key][value]
}

static void register_all_lua_ext(lua_State *L)
{
    const LuaRegEx *lib;
    for (lib = extlibs; lib->func; lib++)
    {
        luaL_requiref(L, lib->name, lib->func, 1);
        if(lib->unpackglobal)
        {
            LuaStackCheck(L, 0);
            lua_pushglobaltable(L);
            luaForeach(L, unpackglobal, -2);
            lua_pop(L, 1);
        }
        lua_pop(L, 1);
    }
}

static int panic(lua_State *L)
{
    const char *err = lua_tostring(L, -1);
    logerror("PANIC: error in Lua call %s", err);
    std::string s = "PANIC: error in Lua call ";
    s += err;
    platform::msgbox(s.c_str(), "Lua panic handler");
    assert(false);
    return 0;
}

LuaInterface::LuaInterface() : LuaState()
{
    register_all_lua_ext(_L);
    lua_atpanic(_L, panic);
}

LuaInterface::~LuaInterface()
{
}

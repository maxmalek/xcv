#pragma once

#include <stdint.h>

namespace fileio {

void *loadBuf(const char *fn, uint64_t *readsize); // always adds a terminating \0 not counted in *readsize
void *loadInto(const char *fn, void *buf, uint64_t bufsize, uint64_t *readsize); // returns buf if successful

} // end namespace fileio

#pragma once

#include "blob3ddef.h"

void *loadBlob3D(const char *fn, Blob3DInfo *info); // returned memory must be free()'d

// cpus: > 1: multithreaded compression
// level: -1: default; 0: don't compress; 1-22: use that level
IOResult saveBlob3D(const char *fn, const void *data, const Blob3DInfo *info, int version = -1, unsigned cpus = 0, int level = -1);

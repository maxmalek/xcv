#include "blob3dio.h"
#include <stdlib.h>
#include <stdio.h>
#include <varint.h>
#include "pstdint.h"
#include <assert.h>
#include <string.h>
#include "util.h"
#include "myendian.h"

// #include <zstd.h> // included by the file below, which also enables the MT stuff when statically linked
#include <zstd/compress/zstdmt_compress.h>



typedef unsigned char u8;

static const char * const MAGIC = "b3dV";

typedef void* (*loader_fn)(FILE *fh, Blob3DInfo *info, uint64_t *psz, bool swapEndian);

struct Header
{
    char magic[4];
    u8 version;
    u8 type;
    u8 channels;
    u8 endian;
};


// does not check things checkheader() already checks for
static bool checkinfo(const Blob3DInfo& info)
{
    // check axis/dimension sanity
    // Allowed cases (where an axis exists if its size is not 0)
    //  - X only: 1D texture
    //  - X, Y: 2D texture
    //  - X, Y, Z: 3D texture
    unsigned b = 0;
    b |= !!info.w;
    b |= !!info.h << 1;
    b |= !!info.d << 2;
    switch(b)
    {
        case 1:
        case 3:
        case 7:
            break; // all good
        default:
            return false;
    }

    return true;
}

static void *mallocinfo(const Blob3DInfo *info, uint64_t *psz)
{
    const uint64_t sz = b3d_getSizeInBytes(info);
    void *mem = sz ? malloc(sz) : NULL;
    if(mem)
        *psz = sz;
    return mem;
}

static void *readbody(FILE *fh, const Blob3DInfo *info, uint64_t *psz)
{
    void *mem = mallocinfo(info, psz);
    if(!mem || fread(mem, *psz, 1, fh) != 1)
    {
        free(mem);
        mem = NULL;
    }
    return mem;
}

static void *readbody_and_uncompress(FILE *fh, size_t packedsize, const Blob3DInfo *info, uint64_t *psz)
{
    if(!packedsize)
        return readbody(fh, info, psz);
    else if(packedsize == -1)
        return NULL; // incomplete; field wasn't overwritten when saving

    void *dst = mallocinfo(info, psz); // writes psz
    size_t const buffInSize = ZSTD_DStreamInSize();
    void * const buffIn  = malloc(buffInSize);
    if(!dst)
        return NULL;
    if(!buffIn)
    {
        free(dst);
        return NULL;
    }

    if(ZSTD_DStream* const dstream = ZSTD_createDStream())
    {
        size_t remain = packedsize;
        size_t const initResult = ZSTD_initDStream(dstream);
        if (!ZSTD_isError(initResult))
        {
            size_t read, toRead = buffInSize;
            ZSTD_outBuffer output = { dst, *psz, 0 };
            while(remain)
            {
                if(toRead > remain)
                    toRead = remain;
                read = fread(buffIn, 1, toRead, fh);
                const bool incomplete = read != toRead; // end of data? must be last block if so
                remain -= read;
                ZSTD_inBuffer input = { buffIn, read, 0 };
                while (input.pos < input.size)
                {
                    toRead = ZSTD_decompressStream(dstream, &output, &input);
                    if (ZSTD_isError(toRead))
                        goto fail;
                }

                if(incomplete && remain) // must be done with data if file ended
                {
                    fail:
                    free(dst);
                    dst = NULL;
                    break;
                }
            }
        }
        ZSTD_freeDStream(dstream);
    }
    free(buffIn);
    return dst;
}

static void *load_v0(FILE *fh, Blob3DInfo *info, uint64_t *psz, bool swapEndian)
{
    u8 buf[32];
    if(fread(buf, sizeof(buf), 1, fh) != 1)
        return NULL;

    u8 *p = &buf[0];
    p += vint::decode(&info->w, p, 8);
    p += vint::decode(&info->h, p, 8);
    p += vint::decode(&info->d, p, 8);

    return checkinfo(*info) ? readbody(fh, info, psz) : NULL;
}

static void *load_v1(FILE *fh, Blob3DInfo *info, uint64_t *psz, bool swapEndian)
{
    u8 buf[3 * sizeof(unsigned) + 3 * sizeof(float)];
    if(fread(buf, sizeof(buf), 1, fh) != 1)
        return NULL;

    if(swapEndian)
        endian::memswap((u8*)&buf[0], sizeof(buf), 4);

    unsigned *pu = (unsigned*)&buf[0];
    float *pf = (float*)&buf[3 * sizeof(unsigned)];

    info->w = *pu++;
    info->h = *pu++;
    info->d = *pu++;
    info->spx = *pf++;
    info->spy = *pf++;
    info->spz = *pf++;
    
    return checkinfo(*info) ? readbody(fh, info, psz) : NULL;
}

static void *load_v2(FILE *fh, Blob3DInfo *info, uint64_t *psz, bool swapEndian)
{
    static const size_t s4 = 3 * sizeof(unsigned) + 3 * sizeof(float);
    static const size_t s8 = sizeof(uint64_t);
    u8 buf[s4 + s8];
    if(fread(buf, sizeof(buf), 1, fh) != 1)
        return NULL;

    if(swapEndian)
    {
        endian::memswap((u8*)&buf[0], s4, 4);
        endian::memswap((u8*)&buf[s4], s8, 8);
    }

    unsigned *pu = (unsigned*)&buf[0];
    float *pf = (float*)&buf[3 * sizeof(unsigned)];

    info->w = *pu++;
    info->h = *pu++;
    info->d = *pu++;
    info->spx = *pf++;
    info->spy = *pf++;
    info->spz = *pf++;

    uint64_t *pu64 = (uint64_t*)&buf[s4];
    size_t packedsize = *pu64++;
    
    return checkinfo(*info) ? readbody_and_uncompress(fh, packedsize, info, psz) : NULL;
}

static const loader_fn loaders[] = { load_v0, load_v1, load_v2 };

static bool checkheader(const Header& h)
{
    if(memcmp(h.magic, MAGIC, sizeof(h.magic)))
        return false;

    if(h.version >= Countof(loaders))
        return false;

    if(!h.channels || h.channels > 4)
        return false;

    if(h.type < B3D_FIRST_VALID_TYPE || h.type > B3D_LAST_VALID_TYPE)
        return false;

    if(h.endian > 1) // either 0 or 1
        return false;

    return true;
}

// TODO: should add a function that calls a callback for a file that may then proceed to load the actual data
// this allows mem mapping a gpu buffer, unnpack directly into that, and async-transfer that into a texture

void * loadBlob3D(const char * fn, Blob3DInfo *info)
{
    FILE *fh = fopen(fn, "rb");
    if(!fh)
        return NULL;

    Header h;
    if(fread(&h, sizeof(h), 1, fh) != 1 || !checkheader(h))
    {
        fclose(fh);
        return NULL;
    }

    info->channels = h.channels;
    info->type = (Blob3DType)h.type;
    info->spx = info->spy = info->spz = 1.0f;

    bool swapEndian = endian::isLittle() != h.endian;
    uint64_t sz = 0;
    void *mem = loaders[h.version](fh, info, &sz, swapEndian);
    fclose(fh);
    if(swapEndian && mem)
        endian::memswap((u8*)mem, sz, Blob3DSize[h.type]);
    return mem;
}


// ----------------------------------------------------------------------------------------
// ------ SAVING --------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------

static const int DEFAULT_COMPRESS_LEVEL_ZSTD = 7; // between fast and normal

struct SaveOptions
{
    SaveOptions()
        : cpus(0)
        , compressionlevel(DEFAULT_COMPRESS_LEVEL_ZSTD)
    {}

    unsigned cpus;
    unsigned compressionlevel;
};

static bool fwrite_compress(FILE *fh, const void *data, size_t datasize, const SaveOptions& opts, uint64_t *pcompressed)
{
    if(!opts.compressionlevel)
        return fwrite(data, 1, datasize, fh) == datasize;

    size_t const buffOutSize = ZSTD_DStreamOutSize();
    void*  const buffOut = malloc(buffOutSize);
    if(!buffOut)
        return false;

    bool good = false;
    uint64_t totalcompressed = 0;

#ifdef ZSTD_STATIC_LINKING_ONLY
    if(opts.cpus > 1)
    {
        if(ZSTDMT_CCtx *cctx = ZSTDMT_createCCtx(opts.cpus))
        {
            size_t const initResult = ZSTDMT_initCStream(cctx, opts.compressionlevel);
            if ( (good = !ZSTD_isError(initResult)) )
            {
                size_t toRead = ZSTD_DStreamInSize();
                size_t remain = datasize;
                const char *p = (const char *)data;
                while (remain)
                {
                    assert(toRead);
                    ZSTD_inBuffer input = { p, toRead, 0 };
                    p += toRead;
                    if(remain > toRead)
                        remain -= toRead;
                    else
                    {
                        input.size = remain;
                        remain = 0;
                    }

                    ZSTD_outBuffer output = { buffOut, buffOutSize, 0 };
                    toRead = ZSTDMT_compressStream(cctx, &output, &input);
                    if (ZSTD_isError(toRead))
                    {
                        good = false;
                        break;
                    }
                    if(output.pos) // possibly 0 if threads are still processing and no result is there yet
                    {
                        if(fwrite(buffOut, output.pos, 1, fh) != 1)
                        {
                            good = false;
                            break;
                        }
                        totalcompressed += output.pos;
                    }
                }
                if(good)
                {
                    while(true)
                    {
                        ZSTD_outBuffer output = { buffOut, buffOutSize, 0 };
                        size_t const remainingToFlush = ZSTDMT_endStream(cctx, &output);   /* close frame */
                        if(ZSTD_isError(remainingToFlush))
                        {
                            good = false;
                            break;
                        }
                        totalcompressed += output.pos;
                        if(output.pos)
                        {
                            if(fwrite(buffOut, output.pos, 1, fh) != 1)
                            {
                                good = false;
                                break;
                            }
                        }
                        if(!remainingToFlush)
                            break;
                    }
                }
            }
            ZSTDMT_freeCCtx(cctx);
        }
    }
    else // single-core version
#endif
    {
        if(ZSTD_CStream* const cstream = ZSTD_createCStream())
        {
            size_t const initResult = ZSTD_initCStream(cstream, opts.compressionlevel);
            if ( (good = !ZSTD_isError(initResult)) )
            {
                size_t toRead = initResult;
                ZSTD_inBuffer input = { data, datasize, 0 };
                while (input.pos < input.size)
                {
                    ZSTD_outBuffer output = { buffOut, buffOutSize, 0 };
                    toRead = ZSTD_compressStream(cstream, &output, &input);
                    if (ZSTD_isError(toRead) || fwrite(buffOut, output.pos, 1, fh) != 1)
                    {
                        good = false;
                        break;
                    }
                    totalcompressed += output.pos;
                }
                if(good)
                {
                    ZSTD_outBuffer output = { buffOut, buffOutSize, 0 };
                    size_t const remainingToFlush = ZSTD_endStream(cstream, &output);   /* close frame */
                    totalcompressed += output.pos;
                    if (remainingToFlush || fwrite(buffOut, output.pos, 1, fh) != 1)
                    {
                        good = false;
                    }
                }
            }
            ZSTD_freeCStream(cstream);
        }
    }
    
    free(buffOut);
    *pcompressed = totalcompressed;
    return good;
}

static bool save_v0(FILE *fh, const void *data, const Blob3DInfo *info, const SaveOptions&)
{
    u8 buf[32];
    memset(buf, 0, sizeof(buf));
    u8 *p = &buf[0];
    p += vint::encode(p, info->w, 8);
    p += vint::encode(p, info->h, 8);
    p += vint::encode(p, info->d, 8);

    uint64_t bytes = b3d_getSizeInBytes(info);
    return fwrite(buf, sizeof(buf), 1, fh) == 1
        && fwrite(data, bytes, 1, fh) == 1;
}

static bool save_v1(FILE *fh, const void *data, const Blob3DInfo *info, const SaveOptions&)
{
    u8 buf[3 * sizeof(unsigned) + 3 * sizeof(float)];
    memset(buf, 0, sizeof(buf));
    unsigned *pu = (unsigned*)&buf[0];
    float *pf = (float*)&buf[3 * sizeof(unsigned)];
    *pu++ = info->w;
    *pu++ = info->h;
    *pu++ = info->d;
    *pf++ = info->spx;
    *pf++ = info->spy;
    *pf++ = info->spz;
    uint64_t bytes = b3d_getSizeInBytes(info);
    return fwrite(buf, sizeof(buf), 1, fh) == 1
        && fwrite(data, bytes, 1, fh) == 1;
}

static bool save_v2(FILE *fh, const void *data, const Blob3DInfo *info, const SaveOptions& opts)
{
    static const size_t s4 = 3 * sizeof(unsigned) + 3 * sizeof(float);
    static const size_t s8 = sizeof(uint64_t);
    u8 buf[s4 + s8];
    memset(buf, 0, sizeof(buf));
    unsigned *pu = (unsigned*)&buf[0];
    float *pf = (float*)&buf[3 * sizeof(unsigned)];
    *pu++ = info->w;
    *pu++ = info->h;
    *pu++ = info->d;
    *pf++ = info->spx;
    *pf++ = info->spy;
    *pf++ = info->spz;
    unsigned *pu64 = (unsigned*)&buf[s4];
    *pu64++ = -1; // placeholder

    const uint64_t bytes = b3d_getSizeInBytes(info);
    uint64_t packedsize = -1;
    fpos_t hpos;
    return fgetpos(fh, &hpos) == 0 // store where v2 header starts
        && fwrite(buf, sizeof(buf), 1, fh) == 1 // write v2 header
        && fwrite_compress(fh, data, bytes, opts, &packedsize) // compress data into file
        && fsetpos(fh, &hpos) == 0 // go back to start of v2 header
        && fseek(fh, s4, SEEK_CUR) == 0 // seek to packed size field
        && fwrite(&packedsize, s8, 1, fh) == 1; // overwrite packed size field with actual value
}

typedef bool (*writer_fn)(FILE *fh, const void *data, const Blob3DInfo *info, const SaveOptions& opts);
static const writer_fn writers[] = { save_v0, save_v1, save_v2 };

IOResult saveBlob3D(const char * fn, const void * data, const Blob3DInfo *info, int version /* = -1 */, unsigned cpus /* = 0 */, int level /* = -1 */)
{
    assert(data);

    // Newest version if not specified
    if(version < 0)
        version = Countof(writers) - 1;
    else if(version >= Countof(writers))
        return IORESULT_INVALID_FORMAT;

    Header h;
    for(unsigned i = 0; i < 4; ++i)
        h.magic[i] = MAGIC[i];
    h.version = version;
    h.endian = endian::isLittle();
    h.channels = (u8)info->channels;
    h.type = (u8)info->type;

    FILE *fh = fopen(fn, "wb");
    if(!fh)
        return IORESULT_FAILED_TO_OPEN;

    SaveOptions opts;
    opts.cpus = cpus;
    if(level >= 0)
        opts.compressionlevel = (unsigned)level;

    bool success = fwrite(&h, sizeof(h), 1, fh) == 1
                && writers[version](fh, data, info, opts);
    fclose(fh);
    if(!success)
        remove(fn);
    return success ? IORESULT_OK : IORESULT_WRITE_FAILED;
}

#include "fileio.h"
#include <stdlib.h>
#include <stdio.h>
#include "platform.h"

namespace fileio {

void * loadInto(const char * fn, void * buf, uint64_t bufsize, uint64_t *readsize)
{
    FILE *f = fopen(fn, "rb");
    if(!f)
        return NULL;

    uint64_t rd = fread(buf, 1, bufsize, f);
    fclose(f);
    if(readsize)
        *readsize = rd;

    ((char*)buf)[rd] = 0; // Null terminate in all cases, esp. for text files
    return buf;
}

void *loadBuf(const char *fn, uint64_t *readsize)
{
    const uint64_t fsz = platform::fileSize(fn);
    void *buf = malloc(fsz+1);
    if(!buf)
        return NULL;

    uint64_t rd = 0;
    if(!loadInto(fn, buf, fsz, &rd) || rd != fsz)
    {
        free(buf);
        return NULL;
    }
    if(readsize)
        *readsize = rd;
    
    return buf;
}


} // end namespace fileio

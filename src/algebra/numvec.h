#pragma once

#include <stdlib.h>
#include "parallel_for.h"
#include <cmath>
#include "glmx.h"
#include "util.h"

namespace NumVecOp {
enum Type // (order is not important)
{
    // Arith
    OP_Add,
    OP_Sub,
    OP_Mul,
    OP_Div,
    OP_Mod,
    // Bitwise
    OP_BAnd,
    OP_BOr,
    OP_BXor,
    // Compare
    OP_Max,
    OP_Min,
    // Matrix
    OP_MScale,
    OP_Mtranslate,
    OP_Mrotate,
};

// Definitely want to avoid virtual calls in single operators...
#define MKOP(name, op, reductionstart) \
    struct name { \
      static const Type value = OP_##name; \
      template<typename A, typename B> struct Typed { \
        static void apply(A& a, const B& b) { a op##= b; } \
      }; \
      template<typename T> struct ReductionStart { \
        inline static T getValue() { return T(reductionstart); } \
      }; \
    };
#define MKF(name, f, reductionstart) \
    struct name { \
      static const Type value = OP_##name; \
      template<typename A, typename B> struct Typed { \
        static void apply(A& a, const B& b) { a = f(a, b); } \
      }; \
      template<typename T> struct ReductionStart { \
        inline static T getValue() { return T(reductionstart); } \
      }; \
    };

template<typename T, bool hasinf>
struct _LimitSelector;

template<typename T>
struct _LimitSelector<T, true>
{
    static_assert(std::numeric_limits<T>::is_specialized, "T not specialized");
    inline static T getMin() { return -std::numeric_limits<T>::infinity(); }
    inline static T getMax() { return std::numeric_limits<T>::infinity(); }
};
template<typename T>
struct _LimitSelector<T, false>
{
    static_assert(std::numeric_limits<T>::is_specialized, "T not specialized");
    inline static T getMin() { return std::numeric_limits<T>::lowest(); }
    inline static T getMax() { return std::numeric_limits<T>::max(); }
};

template <typename T>
struct has_value_type
{
    typedef char yes[1];
    typedef char no[2];

    template <typename C> static yes& test(typename C::value_type*);
    template <typename> static no& test(...);

    enum { value = sizeof(test<T>(0)) == sizeof(yes) };
};

template<typename T, bool hasVT>
struct _LimitTypeSelector;

template<typename T>
struct _LimitTypeSelector<T, true>
{
    typedef typename T::value_type type;
};
template<typename T>
struct _LimitTypeSelector<T, false>
{
    typedef T type;
};

template<typename T>
struct LimitTypeSelector
{
    typedef typename _LimitTypeSelector<T, has_value_type<T>::value>::type type;
};

template<typename T>
struct LimitSelector
{
    typedef typename LimitTypeSelector<T>::type TT;
    static_assert(std::numeric_limits<TT>::is_specialized, "T not specialized");
    typedef _LimitSelector<TT, std::numeric_limits<TT>::has_infinity> W;
    inline static TT getMin() { return W::getMin(); }
    inline static TT getMax() { return W::getMax(); }
};

MKOP(Add, +, 0)
MKOP(Sub, -, 0)
MKOP(Mul, *, 1)
MKOP(Div, /, 1)
MKOP(Mod, %, 1)
MKOP(BAnd, &, (std::numeric_limits<T>::max()) )
MKOP(BOr, |, 0)
MKOP(BXor, ^, 0)

MKF(Min, glm::min, (LimitSelector<T>::getMax()) )
MKF(Max, glm::max, (LimitSelector<T>::getMin()) )
MKF(MScale, glm::scale, 1) // identity matrix
MKF(Mtranslate, glm::translate, 0) // zero matrix
#undef MKOP
#undef MKF

struct FMod {
    enum { value = OP_Mod };
    template<typename A, typename B> struct Typed
    {
        static void apply(A& a, const B& b) { a = std::fmod<A>(a, b); }
    };
    template<typename T> struct ReductionStart
    {
        inline static T getValue() { return T(1); }
    };
};

struct Mrotate {
    enum { value = OP_Mrotate };
    template<typename A, typename B> struct Typed
    {
        static void apply(A& a, const B& b)
        {
            const glm::tvec3<typename B::value_type, B::prec> axis(b.x, b.y, b.z);
            a = glm::rotate(a, b.w, axis);
        }
    };
    template<typename T> struct ReductionStart
    {
        inline static T getValue() { return T(); }
    };
};



// So we pull them into strategies, which operate on a block of data.
template<typename T1, typename T2>
struct Strategy
{
    typedef T1 t1;
    typedef T2 t2;
};

template<typename T1, typename T2, typename OP>
struct ModuloStrategy : public Strategy<T1, T2>
{
    typedef typename OP::template Typed<T1, T2> op;
    static inline void apply(T1 *my, const T2 *o, size_t sz, size_t sz2, size_t offs)
    {
        for(size_t i = 0; i < sz; ++i)
            op::apply(my[i], o[(i + offs) % sz2]);
    }
};

template<typename T1, typename T2, typename OP>
struct FastStrategy : public Strategy<T1, T2>
{
    typedef typename OP::template Typed<T1, T2> op;
    static inline void apply(T1 *my, const T2 *o, size_t sz, size_t sz2, size_t offs)
    {
        assert(offs == 0); // HMMM?
        for(size_t i = 0; i < sz; ++i)
            op::apply(my[i], o[i]);
    }
};

template<typename T1, typename T2, typename OP>
struct ScalarStrategy : public Strategy<T1, T2>
{
    typedef typename OP::template Typed<T1, T2> op;
    static inline void apply(T1 *my, const T2 *o, size_t sz, size_t sz2, size_t offs)
    {
        const T2 scalar = *o;
        for(size_t i = 0; i < sz; ++i)
            op::apply(my[i], scalar);
    }
};

// Virtual call strategy if the actual operator is not known at compile time
// This means one virtual call per block, but the actual hot loop is vcall free
template<typename T1, typename T2>
struct DynamicStrategy : public Strategy<T1, T2>
{
    virtual void apply(T1 *my, const T2 *o, size_t sz, size_t sz2, size_t offs) const = 0;
};

template<typename BASE>
struct VirtualStrategy : public DynamicStrategy<typename BASE::t1, typename BASE::t2>, protected BASE
{
    VirtualStrategy() {}
    typedef typename BASE::t1 t1;
    typedef typename BASE::t2 t2;
    typedef typename BASE::op op;

    // Inherited from DynamicStrategy
    virtual void apply(t1 *my, const t2 *o, size_t sz, size_t sz2, size_t offs) const
    {
        BASE::apply(my, o, sz, sz2, offs);
    }
};



} // end namespace NumVecOp

struct NumVecBase
{
    enum _ViewOrCopy { VIEW, COPY };
    static const size_t MINBLOCKSIZE = 512;
};


// Fixed-size vector for numeric types.
// Can unfortunately not use std::vector internally, since std::vector<bool> is special
// and breaks operator[]. It's also the only type where, given N elements,
// the vector does not end up being N * sizeof(T) bytes in size.
template<typename T>
class NumVec : public NumVecBase
{
public:
    typedef T value_type;

private:
    template<typename STRAT>
    struct Pass
    {
        const size_t total;
        const typename STRAT::t2 * const p;
        const STRAT& strat;
    };

public:

    NumVec(size_t n, const T& initval = T())
        : _v(_init(_alloc(n), n, initval)), _sz(n), _own(true)
    {
    }

    // View into raw memory, or copy of raw memory
    NumVec(void *p, size_t n, _ViewOrCopy how)
        : _v(how == VIEW ? safe_reinterpret_cast<T*>(p) : _rawinitcopy(_alloc(n), p, n)), _sz(n), _own(how == COPY)
    {
    }

private:

    static T *_alloc(size_t n)
    {
        return static_cast<T*>(n ? malloc(n * sizeof(T)) : NULL);
    }

    static T *_init(T *p, size_t n, const T& initval)
    {
        for(size_t i = 0; i < n; ++i)
           new (p + i) T(initval);
        return p;
    }

    static T *_rawinitcopy(T *dst, const void *p, size_t n)
    {
        const T *pp = safe_reinterpret_cast<const T*>(p);
        for(size_t i = 0; i < n; ++i)
           new (dst + i) T(pp[i]);
        return dst;
    }

    static T *_initcopy(T *dst, const NumVec<T>& src)
    {
        const size_t n = src.size();
        for(size_t i = 0; i < n; ++i)
           new (dst + i) T(src[i]);
        return dst;
    }

    void _dealloc()
    {
        if(_own)
        {
            const size_t n = size();
            for(size_t i = 0; i < n; ++i)
                _v[i].~T();
            free(_v);
            _sz = 0;
        }
    }
public:

    NumVec(const NumVec<T>& o)
        : _v(_initcopy(_alloc(o.size()), o)), _sz(o._sz), _own(true)
    {}

    ~NumVec()
    {
        _dealloc();
    }

    T& operator=(const NumVec<T>& o)
    {
        if(this != &o)
        {
            _dealloc();
            _v = _initcopy(_alloc(o.size()), o);
            _sz = o._sz;
            _own = true;
        }
        return *this;
    }

    inline       T& operator[](const size_t i)       { return _v[i]; }
    inline const T& operator[](const size_t i) const { return _v[i]; }
    inline size_t size() const { return _sz; }

    template<typename OP, typename V>
    void applyVec(const V& o)
    {
        typedef typename V::value_type t2;
        typedef NumVecOp::FastStrategy<T, t2, OP> SFast;
        typedef NumVecOp::ScalarStrategy<T, t2, OP> SMod;
        const size_t mysz = _sz;
        const size_t othersz = o.size();

        if(mysz && othersz)
        {
            if(mysz >= othersz) // fast path
            {
                SFast dummy;
                Pass<SFast> u = { othersz, &o[0], dummy};
                para::ForEach(&_v[0], u, mysz, _apply_strategy, 0, MINBLOCKSIZE);
            }
            else // need to take slow path with modulo ops
            {
                SMod dummy;
                Pass<SMod> u = { othersz, &o[0], dummy};
                para::ForEach(&_v[0], u, mysz, _apply_strategy, 0, MINBLOCKSIZE);
            }
        }
    }

    template<typename OP, typename T2>
    void applyScalar(const T2& o)
    {
        typedef NumVecOp::ScalarStrategy<T, T2, OP> SScalar;
        if(_sz)
        {
            SScalar dummy;
            Pass<SScalar> u = { 1, &o, dummy };
            para::ForEach(&_v[0], u, _sz, _apply_strategy, 0, MINBLOCKSIZE);
        }
    }

    // This is useful when the operator is not known at compile time
    template<typename STRAT>
    void applyStrategy(const STRAT& strat, const typename STRAT::t2 *v, size_t sz2)
    {
        Pass<STRAT> u = { sz2, v, strat };
        para::ForEach(&_v[0], _sz, u, _apply_strategy, 0, MINBLOCKSIZE);
    }

    // Reduce numvec into a single value, given a starting value
    template<typename OP, typename S>
    S reduce(S val = OP::template ReductionStart<S>::getValue()) const
    {
        typedef typename OP::template Typed<S, T> TT;
        const size_t sz = _sz;
        for(size_t i = 0; i < sz; ++i)
            TT::apply(val, _v[i]);
        return val;
    }

private:


    template<typename STRAT>
    static void _apply_strategy(T *my, size_t sz, size_t abspos, const Pass<STRAT>& u)
    {
        u.strat.apply(my, u.p, sz, u.total, abspos);
    }


    T *_v;
    size_t _sz;
    bool _own;
};

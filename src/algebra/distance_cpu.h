#pragma once

#include <stddef.h>

namespace DistanceTransform {

void float3d_gt(float *data, size_t w, size_t h, size_t d, float solidIfGreaterThan);
void float3d_gt_singlethread(float *data, size_t w, size_t h, size_t d, float solidIfGreaterThan);

}

// Based on the distance transform described in http://cs.brown.edu/~pff/dt/

#include "distance_cpu.h"
#include "parallel_for.h"
#include "utilnd.h"

#include <vector>
#include <math.h>
#include <stdlib.h>
#include <assert.h>
#include <algorithm>

#ifdef _DEBUG
#include <stdio.h>
#endif

template<typename T> size_t wrkSize(size_t n)
{
    return n * sizeof(size_t) // v
        + (n + 1) * sizeof(T) // z
        + n * sizeof(T); // in
}

template<typename T>
static void *allocWrk(size_t n)
{
    return malloc(wrkSize<T>(n));
}

template<typename T>
static void dt1d_linear(T * const out, void * const wrk, size_t n, const T inf)
{
    const T * const in = (const T*)wrk;
    size_t * const v = (size_t*)(((T*)wrk) + n);
    T * const z = (T*)(v + n);

    v[0] = 0;
    z[0] = -inf;
    z[1] = inf;

    {
        unsigned k = 0;
        for(size_t q = 1; q < n; ++q)
        {
            const T tmp = in[q] + T(size_t(q*q));
            T s;
            while(true)
            {
                const size_t vk = v[k];
                s = (tmp - (in[vk] + float(vk*vk))) / float(size_t(2*q - 2*vk));
                if(s > z[k])
                    break;
                --k;
            }
            ++k;
            v[k] = q;
            z[k] = s;
            z[k+1] = inf;
        }
    }

    {
        unsigned k = 0;
        for(size_t q = 0; q < n; ++q)
        {
            while(z[k+1] < q)
                ++k;
            const size_t vk = v[k];
            const size_t tmp = q - vk;
            const T d = T(tmp*tmp) + in[vk];
            out[q] = d;
        }
    }
}

template<typename T, unsigned N>
static void data2wrk(const T *data, void * const wrk, const size_t (&pos)[N], const size_t (&sz)[N], unsigned axis)
{
    assert(pos[axis] == 0);
    size_t idx = utilND::index(pos, sz);
    size_t adv = utilND::distanceToNextOnAxis<size_t,N>(axis, sz);
    const size_t lim = sz[axis];
    T *dst = (T*)wrk;
#ifdef _DEBUG
    size_t checkpos[N];
    for(unsigned i = 0; i < N; ++i)
        checkpos[i] = pos[i];
#endif
    for(size_t i = 0; i < lim; ++i, idx += adv)
    {
#ifdef _DEBUG
        size_t check = utilND::index(checkpos, sz);
        assert(idx == check);
        checkpos[axis]++;
#endif
        *dst++ = data[idx];
    }
}

template<typename T, unsigned N>
static void wrk2data(T *data, void * const wrk, size_t (&pos)[N], const size_t (&sz)[N], unsigned axis)
{
    assert(pos[axis] == 0);
    size_t idx = utilND::index(pos, sz);
    size_t adv = utilND::distanceToNextOnAxis<size_t,N>(axis, sz);
    const size_t lim = sz[axis];
    T *src = (T*)wrk;

    for(size_t i = 0; i < lim; ++i, idx += adv)
    {
#ifdef _DEBUG
        size_t check = utilND::index(pos, sz);
        assert(idx == check);
        pos[axis]++;
#endif
        data[idx] = *src++;
    }
#ifdef _DEBUG
    pos[axis] = 0;
#endif
}

// Start with last axis and then go back
template<typename T, unsigned N, unsigned BASELEVEL>
struct DoAxisRec
{
    static inline void doit(T *data, unsigned targetAxis, void *wrk, size_t (&pos)[N], const size_t (&sz)[N], const T inf)
    {
        const unsigned curAxis = (targetAxis + BASELEVEL) % N;
        assert(curAxis != targetAxis);
        const size_t lim = sz[curAxis];
        for(unsigned i = 0; i < lim; ++i)
        {
            pos[curAxis] = i;
            DoAxisRec<T, N, BASELEVEL-1>::doit(data, targetAxis, wrk, pos, sz, inf);
        }
    }
};

template<typename T, unsigned N>
struct DoAxisRec<T, N, 0u>
{
    static inline void doit(T *data, unsigned targetAxis, void *wrk, size_t (&pos)[N], const size_t (&sz)[N], const T inf)
    {
        pos[targetAxis] = 0;
        data2wrk<T,N>(data, wrk, pos, sz, targetAxis);
        dt1d_linear<T>((T*)wrk, wrk, sz[targetAxis], inf);
        wrk2data<T,N>(data, wrk, pos, sz, targetAxis);
    }
};

template<typename T, unsigned N>
static void doAxisRec(size_t startpos, T *data, unsigned targetAxis, void *wrk, const size_t (&sz)[N], const T inf)
{
    size_t pos[N];
    for(unsigned i = 0; i < N; ++i)
        pos[i] = 0;
    const unsigned startAxis = (targetAxis + N - 1) % N;
    pos[startAxis] = startpos;

    DoAxisRec<T, N, N-1>::doit(data, targetAxis, wrk, pos, sz, inf);
}

template<typename T>
struct OuterAxisData
{
    T *const data;
    unsigned targetAxis;
    const size_t * const psz;
    const T inf;
};

template<typename T, unsigned N>
static void pf_doOuterAxis(char *fake, size_t iter, size_t begin, const OuterAxisData<T>& ax)
{
    typedef const size_t (carray)[N];

    carray& sz = *reinterpret_cast<carray*>(ax.psz);

#ifdef _DEBUG
    printf("BEGIN: outer axis for %u is %u, [%u .. %u]\n", ax.targetAxis, (ax.targetAxis + N - 1) % N, (unsigned)begin, unsigned(begin+iter));
#endif

    void *wrk = allocWrk<T>(sz[ax.targetAxis]);

    for(size_t i = 0; i < iter; ++i)
        doAxisRec<T, N>(begin + i, ax.data, ax.targetAxis, wrk, sz, ax.inf);
    
    free(wrk);

#ifdef _DEBUG
    printf("END  : outer axis for %u is %u, [%u .. %u]\n", ax.targetAxis, (ax.targetAxis + N - 1) % N, (unsigned)begin, unsigned(begin+iter));
#endif
}

template<typename T>
struct CmpGreater
{
    CmpGreater(T ref, T solidval, T nonsolidval) : _ref(ref), _solid(solidval), _nonsolid(nonsolidval) {}
    inline T select(T check) const
    {
        return check > _ref ? _solid : _nonsolid;
    }
    const T _ref, _solid, _nonsolid;
};


template<typename T, typename CMP>
static void pf_prep(T *data, size_t sz, size_t, const CMP& cmp)
{
    for(size_t i = 0; i < sz; ++i)
        data[i] = cmp.select(data[i]);
}

template<typename T>
static void pf_post(T *data, size_t sz, size_t, const float& mul)
{
    const float m = mul;
    for(size_t i = 0; i < sz; ++i)
        data[i] = m * std::sqrt(data[i]);
}

template<unsigned N>
static float getNormalizationMultiplier(const size_t (&n)[N])
{
    uint64_t accu = 0;
    for(unsigned i = 0; i < N; ++i)
        accu += n[i] * n[i];
    float mul = float(1.0 / sqrt((double)accu)); // CHECK THIS: hope the float cast isn't THAT bad for accuracy... have to see for larger models
    return mul;
}

template<typename T, typename CMP, unsigned N>
static void transform(T * const data, const size_t (&n)[N], const CMP& cmp, const T inf)
{
    size_t sz = 1;
    for(unsigned i = 0; i < N; ++i)
        sz *= n[i];

     const unsigned TH = jobq::GetAvailThreads();

    const jobq::Job first = para::CreateJob(data, sz, cmp, pf_prep<T, CMP>, para::CountSplitter(sz / TH));
    jobq::Job last = first;

    for(unsigned i = 0; i < N; ++i)
    {
        jobq::Job prev = last;
        const OuterAxisData<T> dat { data, i, &n[0], inf };
        last = para::CreateJob((char*)NULL, n[i], dat, pf_doOuterAxis<T, N>, para::CountSplitter(n[i] / TH));
        jobq::AddContinuation(prev, last);
    }
    {
        // Get max. N-dim. vector length and normalize by that
        const float mul = getNormalizationMultiplier(n); // CHECK THIS: hope the float cast isn't THAT bad for accuracy... have to see for larger models

        jobq::Job prev = last;
        last = para::CreateJob(data, sz, mul, pf_post<T>, para::CountSplitter(sz / TH));
        jobq::AddContinuation(prev, last);
    }

    Run(first);
    Wait(last);
}

template<typename T, typename CMP, unsigned N>
static void transform_singlethread(T * const data, const size_t (&n)[N], const CMP& cmp, const T inf)
{
    size_t sz = 1;
    for(unsigned i = 0; i < N; ++i)
        sz *= n[i];

    pf_prep<T, CMP>(data, sz, 0, cmp);

    for(unsigned i = 0; i < N; ++i)
    {
        const OuterAxisData<T> dat { data, i, &n[0], inf };
        pf_doOuterAxis<T, N>(NULL, n[i], 0, dat);
    }
    {
        // Get max. N-dim. vector length and normalize by that
        const float mul = getNormalizationMultiplier(n); // CHECK THIS: hope the float cast isn't THAT bad for accuracy... have to see for larger models
        pf_post<T>(data, sz, 0, mul);
    }
}


void DistanceTransform::float3d_gt(float *data, size_t w, size_t h, size_t d, float solidIfGreaterThan)
{
    const float inf = 1e20f;
    const CmpGreater<float> cmp(solidIfGreaterThan, 0.0f, inf);
    const size_t n[] = {w, h, d};
    transform(data, n, cmp, inf);
}

void DistanceTransform::float3d_gt_singlethread(float * data, size_t w, size_t h, size_t d, float solidIfGreaterThan)
{
    const float inf = 1e20f;
    const CmpGreater<float> cmp(solidIfGreaterThan, 0.0f, inf);
    const size_t n[] = {w, h, d};
    transform_singlethread(data, n, cmp, inf);
}

#include "lua_numvec.h"
#include "luainternal.h"
#include "numvec.h"
#include "glmx.h"
#include "glsl.h"

const ScriptDef TypedScriptableNumVec::scriptdef("numvec", ScriptTypes::NUMVEC);

// Various hacks to get pointer into storage and size of underlying container
// without knowing the underlying type.
// This is ONLY possible because TypedScriptableNumVec is always also a SNumVec<T>.
// just need to make proper static casts that take care of offsetting 'this' properly.
SNumVec<char>& TypedScriptableNumVec::_asvec()
{
    return static_cast<SNumVec<char>&>(*this);
}
const SNumVec<char>& TypedScriptableNumVec::_asvec() const
{
    return static_cast<const SNumVec<char>&>(*this);
}


char *TypedScriptableNumVec::ptr()
{
    return &_asvec()[0];
}
char *TypedScriptableNumVec::ptr(size_t offset)
{
    return ptr() + (offset * bytesPerElem);
}
const char *TypedScriptableNumVec::ptr() const
{
    return &_asvec()[0];
}
const char *TypedScriptableNumVec::ptr(size_t offset) const
{
    return ptr() + (offset * bytesPerElem);
}
unsigned TypedScriptableNumVec::components() const
{
    return GLSL::scalarComponents(glsltype);
}
size_t TypedScriptableNumVec::elems() const
{
    return _asvec().size();
}

size_t TypedScriptableNumVec::bytesize() const
{
    return elems() * bytesPerElem;
}

bool TypedScriptableNumVec::checkindex(size_t n, size_t off)
{
    return n + off < elems();
}

bool TypedScriptableNumVec::checkbytes(size_t bytes, size_t off)
{
    return bytes + off <= bytesize();
}

void TypedScriptableNumVec::rawload(const void * src, size_t bytes, size_t off)
{
    assert(checkbytes(bytes, off));
    memcpy(((char*)ptr()) + off, src, bytes);
}

template<typename TO, typename FROM> SNumVec<TO> *pvcast(const SNumVec<FROM> *v)
{
    return static_cast<SNumVec<TO>*>(v);
}

// kinda hacky, but since the strategies don't contain any members, this is fine.
// All that matters is the virtual apply() method being set correctly.
template<typename OP, typename T1, typename T2>
inline static const NumVecOp::DynamicStrategy<T1, T2>& getStrategy(size_t sz1, size_t sz2)
{
    typedef NumVecOp::ScalarStrategy<T1, T2, OP> SScalar;
    typedef NumVecOp::FastStrategy<T1, T2, OP> SFast;
    typedef NumVecOp::ModuloStrategy<T1, T2, OP> SMod;
    static const NumVecOp::VirtualStrategy<SScalar> scalar;
    static const NumVecOp::VirtualStrategy<SFast> fast;
    static const NumVecOp::VirtualStrategy<SMod> mod;
    if(sz2 == 1)
        return scalar;
    if(sz1 < sz2)
        return fast;
    return mod;
}

template<typename OP, typename T1, typename T2>
static void calc(NumVec<T1>& v1, const T2 *v2p, size_t sz2)
{
    v1.applyStrategy(getStrategy<OP, T1, T2>(v1.size(), sz2), v2p, sz2);
}

template<typename OP, typename T1, typename T2>
inline static void calc0(TypedScriptableNumVec& v1, const TypedScriptableNumVec& v2)
{
    assert(v1.glsltype == GLSL::TypeFor<T1>::type);
    assert(v2.glsltype == GLSL::TypeFor<T2>::type);
    // The cast to SNumVec instead of NumVec is intentional.
    // Up-cast to the shared type first, then the proper down-cast is done automatically.
    SNumVec<T1>& s1 = static_cast<SNumVec<T1>&>(v1);
    const SNumVec<T2>& s2 = static_cast<const SNumVec<T2>&>(v2);
    assert(s2.size());
    calc<OP, T1, T2>(s1, &s2[0], s2.size());
}

template<typename OP, typename S, typename VT>
static S reduce(const NumVec<VT>& v, S val)
{
    return v.template reduce<OP, S>(val);
}

template<typename OP, typename S, typename VT>
inline static int reduce0Push(lua_State *L, const TypedScriptableNumVec& v, const TypedScriptableNumVec& scalar)
{
    assert(v.glsltype == GLSL::TypeFor<VT>::type);
    assert(scalar.glsltype == GLSL::TypeFor<S>::type);
    const SNumVec<VT>& vt = static_cast<const SNumVec<VT>&>(v);
    const SNumVec<S>& st = static_cast<const SNumVec<S>&>(scalar);
    luaReturnT((reduce<OP, S, VT>(vt, st[0])));
}


#define DEFCALC(OP, T1, T2) \
    DEFCALCC(NumVecOp::OP, T1, T2)

#define DEFCMP(T) \
    DEFCALC(Min, T, T) \
    DEFCALC(Max, T, T)

#define DEFARITH(T1, T2) \
    DEFCALC(Add, T1, T2) \
    DEFCALC(Sub, T1, T2) \
    DEFMUL(T1, T2)

#define DEFMUL(T1, T2) \
    DEFCALC(Mul, T1, T2) \
    DEFCALC(Div, T1, T2)

#define DEFBIT(T1, T2) \
    DEFCALC(BAnd, T1, T2) \
    DEFCALC(BOr, T1, T2) \
    DEFCALC(BXor, T1, T2)

#define IFTYPE(T1, T2) \
    if(ty1 == GLSL::TypeFor<T1>::type && ty2 == GLSL::TypeFor<T2>::type) switch(op)

#define MULTYPE(T1, T2) \
    IFTYPE(T1, T2) { DEFMUL(T1, T2) }

#define MATTYPE(T) \
    MULTYPE(T, T::value_type) \
    IFTYPE(T, T) { \
        DEFARITH(T, T) \
    }

#define IVECTYPE(T) \
    MULTYPE(T, T::value_type) \
    IFTYPE(T, T) { \
        DEFARITH(T, T) \
        DEFCMP(T) \
        DEFCALC(Mod, T, T) \
    }

#define VECTYPE(T) \
    MULTYPE(T, T::value_type) \
    IFTYPE(T, T) { \
        DEFARITH(T, T) \
        DEFCMP(T) \
    }
#define INTTYPE2(T1, T2) \
    IFTYPE(T1, T2) { \
        DEFARITH(T1, T2) \
        DEFBIT(T1, T2) \
        DEFCALC(Mod, T1, T2) \
    }

#define INTTYPE(T) \
    IFTYPE(T, T) { \
        DEFARITH(T, T) \
        DEFBIT(T, T) \
        DEFCALC(Mod, T, T) \
        DEFCMP(T) \
    }


// roughly sorted in decreasing order of expected usage likelyhood
#define HUGE_TYPE_SWITCH \
{                                                     \
    IFTYPE(float, float)                              \
    {                                                 \
        DEFARITH(float, float)                        \
        DEFCMP(float)                                 \
        DEFCALC(FMod, float, float)                   \
    }                                                 \
    INTTYPE(int)                                      \
    INTTYPE(unsigned)                                 \
    INTTYPE2(int, unsigned)                           \
    INTTYPE2(unsigned, int)                           \
    VECTYPE(glm::vec2)                                \
    VECTYPE(glm::vec3)                                \
    VECTYPE(glm::vec4)                                \
    MATTYPE(glm::mat2)                                \
    MATTYPE(glm::mat3)                                \
    MATTYPE(glm::mat4)                                \
    IVECTYPE(glm::ivec2)                              \
    IVECTYPE(glm::ivec3)                              \
    IVECTYPE(glm::ivec4)                              \
    IVECTYPE(glm::uvec2)                              \
    IVECTYPE(glm::uvec3)                              \
    IVECTYPE(glm::uvec4)                              \
    IFTYPE(glm::mat4, glm::vec3)                      \
    {                                                 \
        DEFCALC(MScale, glm::mat4, glm::vec3)         \
        DEFCALC(Mtranslate, glm::mat4, glm::vec3)     \
    }                                                 \
    IFTYPE(glm::mat4, glm::vec4)                      \
    {                                                 \
        DEFCALC(Mrotate, glm::mat4, glm::vec4)        \
    }                                                 \
    IFTYPE(double, double)                            \
    {                                                 \
        DEFARITH(float, float)                        \
        DEFCMP(float)                                 \
        DEFCALC(FMod, float, float)                   \
    }                                                 \
    VECTYPE(glm::dvec2)                               \
    VECTYPE(glm::dvec3)                               \
    VECTYPE(glm::dvec4)                               \
    MATTYPE(glm::dmat2)                               \
    MATTYPE(glm::dmat3)                               \
    MATTYPE(glm::dmat4)                               \
    IFTYPE(glm::dmat4, glm::dvec3)                    \
    {                                                 \
        DEFCALC(MScale, glm::dmat4, glm::dvec3)       \
        DEFCALC(Mtranslate, glm::mat4, glm::vec3)     \
    }                                                 \
    IFTYPE(glm::dmat4, glm::dvec4)                    \
    {                                                 \
        DEFCALC(Mrotate, glm::dmat4, glm::dvec4)      \
    }                                                 \
}

#define DEFCALCC(OPCLASS, T1, T2) \
    case OPCLASS::value: calc0<OPCLASS, T1, T2>(v1, v2); return;


static void calcDispatch(lua_State *L, TypedScriptableNumVec& v1, const TypedScriptableNumVec& v2, NumVecOp::Type op)
{
    const GLSL::Type ty1 = v1.glsltype;
    const GLSL::Type ty2 = v2.glsltype;
    HUGE_TYPE_SWITCH
    luaL_error(L, "Unhandled arith between types %s, %s", GLSL::getName(ty1), GLSL::getName(ty2));
}

#undef DEFCALCC

#define DEFCALCC(OPCLASS, T1, T2) \
    case OPCLASS::value: return reduce0Push<OPCLASS, T1, T2>(L, v, scalar);

static int reduceDispatchPush(lua_State *L, const TypedScriptableNumVec& v, const TypedScriptableNumVec& scalar,  NumVecOp::Type op)
{
    const GLSL::Type ty1 = v.glsltype;
    const GLSL::Type ty2 = scalar.glsltype;
    HUGE_TYPE_SWITCH
    luaL_error(L, "Unhandled reduction between types %s, %s", GLSL::getName(ty1), GLSL::getName(ty2));

    // unreachable
    assert(false);
    return 0;
}

#undef DEFCALCC


inline GLSL::Type getglsltype(lua_State *L, int idx)
{
    const unsigned t = lua_checkuint(L, idx);
    if(t < GLSL::_MAXTYPE)
        return (GLSL::Type)t;
    luaL_argerror(L, 1, "invalid GLSL type");
    return GLSL::unknown; // unreachable
}

static void badtype(lua_State *L, int idx)
{
    luaL_argerror(L, idx, "unhandled/unsupported GLSL type");
}

// Used multiple times further down...
#define ALLMETHODS \
    A(add, Add) \
    A(sub, Sub) \
    A(mul, Mul) \
    A(div, Div) \
    A(mod, Mod) \
    A(band, BAnd) \
    A(bor, BOr) \
    A(bxor, BXor) \
    A(min, Min) \
    A(max, Max) \
    A(mscale, MScale) \
    A(mtranslate, Mtranslate) \
    A(mrotate, Mrotate)


struct LuaNumVec : public LuaClassBind<TypedScriptableNumVec>
{
    static const luaL_Reg methods[];

    template<typename T>
    static SNumVec<T> *newT(size_t n, const T& initval = T(0))
    {
        return new SNumVec<T>(n, initval);
    }

    // returned vector is zero initialized
    static TypedScriptableNumVec *newGLSL(lua_State *L, size_t n, GLSL::Type ty)
    {
        switch(ty)
        {
#define PRIMTYPE(cty, glty) case GLSL::glty: return newT<cty>(n); break;
#define GLSLTYPE(glty) case GLSL::glty: return newT<glm::glty>(n); break;
#include "glsl_gen.h"
            default: badtype(L, 1);
        }
        luaReturnNil();
    }

    luaFunc(new)
    {
        const GLSL::Type ty = getglsltype(L, 1);
        const lua_Integer n = luaL_checkinteger(L, 2);
        if(n <= 0)
            luaL_argerror(L, 2, "should be > 0");
        luaReturnObj(newGLSL(L, n, ty));
    }

    template<typename T>
    static int newRawViewT(lua_State *L, void *p, size_t bytes)
    {
        luaReturnObj(new SNumVec<T>(p, bytes, SNumVec<T>::VIEW, SNumVec<T>::BYTES));
    }

    luaFunc(newRawView)
    {
        const unsigned ty = getglsltype(L, 1);
        luaL_checktype(L, 2, LUA_TLIGHTUSERDATA);
        void *p = lua_touserdata(L, 2);
        size_t bytes = luaL_checkinteger(L, 3);
        switch(ty)
        {
#define PRIMTYPE(cty, glty) case GLSL::glty: return newRawViewT<cty>(L, p, bytes); break;
#define GLSLTYPE(glty) case GLSL::glty: return newRawViewT<glm::glty>(L, p, bytes); break;
#include "glsl_gen.h"
            default: badtype(L, 1);
        }
        luaReturnNil();
    }

    template<typename T>
    static int viewAsT(lua_State *L, TypedScriptableNumVec& v)
    {
        return newRawViewT<T>(L, v.ptr(), v.bytesize());
    }

    luaFunc(viewAs)
    {
        TypedScriptableNumVec *v = This(L);
        const unsigned ty = lua_checkint(L, 2);
        switch(ty)
        {
#define PRIMTYPE(cty, glty) case GLSL::glty: return viewAsT<cty>(L, *v); break;
#define GLSLTYPE(glty) case GLSL::glty: return viewAsT<glm::glty>(L, *v); break;
#include "glsl_gen.h"
            default: badtype(L, 1);
        }
        luaReturnNil();
    }

    template<typename T, typename OP>
    static TypedScriptableNumVec *_newGLSLReductionStart_3()
    {
        typedef typename OP::template ReductionStart<T> R;
        return newT<T>(1, R::getValue());
    }

    template<typename T>
    static TypedScriptableNumVec *_newGLSLReductionStart_2(lua_State *L, NumVecOp::Type op)
    {
        switch(op)
        {
#define A(mth, opx) case NumVecOp::OP_##opx: return _newGLSLReductionStart_3<T, NumVecOp::opx>();
            ALLMETHODS
#undef A
        }
        luaL_error(L, "bad operator: %d", op);
        return NULL;
    }

    static TypedScriptableNumVec *newGLSLReductionStart(lua_State *L, GLSL::Type ty, NumVecOp::Type op)
    {
        switch(ty)
        {
#define PRIMTYPE(cty, glty) case GLSL::glty: return _newGLSLReductionStart_2<cty>(L, op); break;
#define GLSLTYPE(glty) case GLSL::glty: return _newGLSLReductionStart_2<glm::glty>(L, op); break;
#include "glsl_gen.h"
            default: badtype(L, 1);
        }
        luaReturnNil();
    }

    // reduce(op, [startvals...])
    // where startval can be a numvec (of which the first element is taken) or lua values
    // op is one of numvec.Add, .Sub, etc. (see NumVecOp::Type for reference)
    // If no starting value is provided, use a suitable one (1 for Mul, 0 for Add, and so on. A "neutral element" to be specific.)
    luaFunc(reduce)
    {
        TypedScriptableNumVec *v = This(L);
        const NumVecOp::Type op = (NumVecOp::Type)lua_checkuint(L, 2);
        CountedPtr<const TypedScriptableNumVec> scalar;

        if(luaIsObject(L, 2))
            scalar = luaGetObj<TypedScriptableNumVec>(L, 2); // caller has provided starting value as some numvec<T>
        else
        {
            const int nparams = lua_gettop(L) - 2; // self & op
            scalar = nparams
                ? packscalars(L, 2, nparams, v->glsltype)    // caller has provided starting value as scalars
                : newGLSLReductionStart(L, v->glsltype, op); // automatically determine a suitable starting value
        }

        return reduceDispatchPush(L, *v, *scalar, op);
    }

    template<typename T>
    static void arithrawPtrT_UNSAFE(lua_State *L, NumVecOp::Type op, TypedScriptableNumVec *self, void *p, size_t nelem)
    {
        const SNumVec<T> v(p, nelem, SNumVec<T>::VIEW, SNumVec<T>::ELEMENTS); // construct in-place temp vector while avoiding malloc()
        calcDispatch(L, *self, v, op);
    }

    static void arithraw_UNSAFE(lua_State *L, NumVecOp::Type op, TypedScriptableNumVec *self, GLSL::Type ty, void *p, size_t nelem)
    {
        switch(ty)
        {
#define PRIMTYPE(cty, glty) case GLSL::glty: arithrawPtrT_UNSAFE<cty>(L, op, self, p, nelem); break;
#define GLSLTYPE(glty) case GLSL::glty: arithrawPtrT_UNSAFE<glm::glty>(L, op, self, p, nelem); break;
#include "glsl_gen.h"
            default: assert(false); // unreachable: can't create numvec with unhandled glsl type
        }
    }

    template<typename T>
    static void arithrawT(lua_State *L, NumVecOp::Type op, TypedScriptableNumVec *self)
    {
        // 1: self, 2: glsltype, 3+: scalars
        T x = lua_getT<T>(L, 3); // build single element out of scalars
        arithrawPtrT_UNSAFE<T>(L, op, self, &x, 1); // forward single elem as raw ptr
    }

    static void arithscalar(lua_State *L, NumVecOp::Type op, TypedScriptableNumVec *self, GLSL::Type ty)
    {
        switch(ty)
        {
#define PRIMTYPE(cty, glty) case GLSL::glty: arithrawT<cty>(L, op, self); break;
#define GLSLTYPE(glty) case GLSL::glty: arithrawT<glm::glty>(L, op, self); break;
#include "glsl_gen.h"
        default: luaL_error(L, "GLSL type %d (%s) does not support arithmetics", ty, GLSL::getName(ty));
        }
    }

    static int arith(unsigned op_, lua_State *L)
    {
        const NumVecOp::Type op = (NumVecOp::Type)op_;
        TypedScriptableNumVec *self = This(L);
        switch(lua_type(L, 2))
        {
            // (numvec)
            case LUA_TUSERDATA:
            {
                const TypedScriptableNumVec *v2 = luaGetObj<TypedScriptableNumVec>(L, 2);
                calcDispatch(L, *self, *v2, op);
                break;
            }
            // (rawptr, type, nelem)
            case LUA_TLIGHTUSERDATA:
            {
                void *p = lua_touserdata(L, 2);
                const GLSL::Type ty = getglsltype(L, 3);
                size_t nelem = luaL_checkinteger(L, 4);
                arithraw_UNSAFE(L, op, self, ty, p, nelem);
                break;
            }
            // (type, [scalar values]...)
            case LUA_TNUMBER:
            {
                const GLSL::Type ty = getglsltype(L, 2);
                arithscalar(L, op, self, ty);
                break;
            }
            default:
                luaL_argerror(L, 2, "need numvec, raw ptr, or scalar(s)");
        }
        luaReturnSelf();
    }


#define A(mth, op) luaFunc(mth) { return arith(NumVecOp::op::value, L); }
    ALLMETHODS
#undef A


    luaFunc(glsltype)
    {
        luaReturnInt(This(L)->glsltype);
    }

    template<typename T>
    static T *getelemptr(lua_State *L, SNumVec<T>& v, lua_Integer i)
    {
        assert(v.glsltype == GLSL::TypeFor<T>::type);
        --i; // C++ -> Lua index
        if(i < lua_Integer(v.size()))
            return &v[size_t(i)];
        luaL_argerror(L, 2, "index out of range");
        return NULL;
    }

    // return single element as integral values on the Lua stack. Lua-indexed.
    template<typename T>
    static int unpackT(lua_State *L, TypedScriptableNumVec& v, lua_Integer where)
    {
        SNumVec<T>& s = static_cast<SNumVec<T>&>(v);
        T *p = getelemptr(L, s, where);
        return lua_pushT<T>(L, *p);
    }

    // write single element given by integral values on the Lua stack. Lua-indexed.
    template<typename T>
    static void setT(lua_State *L, TypedScriptableNumVec& v, lua_Integer where, int idx)
    {
        SNumVec<T>& s = static_cast<SNumVec<T>&>(v);
        size_t i = where - 1;
        if(i < s.size())
            s[i] = lua_getT<T>(L, idx);
        else
            luaL_argerror(L, 2, "index out of range");
    }

    template<typename T>
    static TypedScriptableNumVec *packT(lua_State *L, int idx, int nparams)
    {
        const unsigned comp = glmx::Info<T>::scalar_components;
        const unsigned nelem = (nparams + (comp - 1)) / comp;
        SNumVec<T> *v = new SNumVec<T>(nelem);
        for(unsigned i = 0; i < nelem; ++i, idx += comp)
            (*v)[i] = lua_getT<T>(L, idx);
        return v;
    }

    luaFunc(unpack)
    {
        TypedScriptableNumVec& v = *This(L);
        const lua_Integer where = luaL_optinteger(L, 2, 1);
        switch(v.glsltype)
        {
#define PRIMTYPE(cty, glty) case GLSL::glty: return unpackT<cty>(L, v, where); break;
#define GLSLTYPE(glty) case GLSL::glty: return unpackT<glm::glty>(L, v, where); break;
#include "glsl_gen.h"
            default: badtype(L, 1);
        }
        luaReturnNil();
    }

    // static
    luaFunc(UnpackRaw)
    {
        luaL_checktype(L, 1, LUA_TLIGHTUSERDATA);
        const void *p = lua_touserdata(L, 1);
        GLSL::Type ty = getglsltype(L, 2);
        switch(ty)
        {
#define PRIMTYPE(cty, glty) case GLSL::glty: return lua_pushT(L, *static_cast<const cty*>(p)); break;
#define GLSLTYPE(glty) case GLSL::glty: return lua_pushT(L, *static_cast<const glm::glty*>(p)); break;
#include "glsl_gen.h"
            default: badtype(L, 1);
        }
        luaReturnNil(); // unreachable
    }

    luaFunc(set)
    {
        TypedScriptableNumVec& v = *This(L);
        const lua_Integer where = luaL_optinteger(L, 2, 1);
        switch(v.glsltype)
        {
#define PRIMTYPE(cty, glty) case GLSL::glty: setT<cty>(L, v, where, 3); break;
#define GLSLTYPE(glty) case GLSL::glty: setT<glm::glty>(L, v, where, 3); break;
#include "glsl_gen.h"
            default: badtype(L, 1);
        }
        luaReturnSelf();
    }

    static TypedScriptableNumVec *packscalars(lua_State *L, int idx, int nparams, GLSL::Type ty)
    {
        switch(ty)
        {
#define PRIMTYPE(cty, glty) case GLSL::glty: return packT<cty>(L, 2, nparams); break;
#define GLSLTYPE(glty) case GLSL::glty: return packT<glm::glty>(L, 2, nparams); break;
#include "glsl_gen.h"
            default: badtype(L, 1);
        }
        return NULL;
    }

    // Pointer to single-element, Lua-indexed
    /*luaFunc(__index)
    {
        TypedScriptableNumVec *v = This(L);
        lua_Integer where = luaL_checkinteger(L, 2);
        if(where <= 0)
            luaL_argerror(L, 2, "index must be >= 1");
        size_t i = where - 1;
        if(i < v->elems())
        {
            luaPushPointer(L, v->ptr(i));
            return 1;
        }
        luaL_argerror(L, 2, "index out of bounds");
        luaReturnNil();
    }*/

    // TODO: multi-set

    // pack passed scalars (or raw pointer) into a single-element numvec
    luaFunc(Pack)
    {
        GLSL::Type ty = getglsltype(L, 1);
        const int nparams = lua_gettop(L) - 1;
        luaReturnObj(packscalars(L, 2, nparams, ty));
    }

    luaFunc(ptr)
    {
        TypedScriptableNumVec *v = This(L);
        size_t off = lua_tointeger(L, 2);
        if(off < v->elems())
        {
            luaPushPointer(L, v->ptr(off));
            return 1;
        }
        luaL_argerror(L, 2, "pointer offset out of range");
        luaReturnNil();
    }

    luaFunc(rawptr)
    {
        size_t off = lua_tointeger(L, 2);
        TypedScriptableNumVec *v = This(L);
        if(off < v->bytesize())
        {
            void *p = ((char*)v->ptr()) + off;
            luaPushPointer(L, p);
            return 1;
        }
        luaL_argerror(L, 2, "pointer offset out of range");
        luaReturnNil();
    }

    luaFunc(len)
    {
        luaReturnInt(This(L)->elems());
    }

    luaFunc(rawsize)
    {
        luaReturnInt(This(L)->bytesize());
    }

    luaFunc(bytesPerElem)
    {
        luaReturnInt(This(L)->bytesPerElem);
    }

    luaFunc(compPerElem)
    {
        luaReturnInt(This(L)->elems());
    }

    static int doRawload(lua_State *L, TypedScriptableNumVec *self, void *src, size_t bytes, size_t off)
    {
        if(!self->checkbytes(bytes, off))
            luaL_error(L, "out of bounds load");
        self->rawload(src, bytes, off);
        luaReturnSelf();
    }

    // FIXME: support more offsets and such. same semantics as load().
    luaFunc(rawload)
    {
        TypedScriptableNumVec *self = This(L);
        // Must be checked first
        if(lua_islightuserdata(L, 2))
        {
            lua_Integer n = luaL_optinteger(L, 3, self->bytesize());
            lua_Integer off = lua_tointeger(L, 4);
            return doRawload(L, self, lua_touserdata(L, 2), n, off);
        }

        // FIXME: might just call into load() instead?
        if(lua_type(L, 2) == LUA_TUSERDATA)
        {
            TypedScriptableNumVec *v = luaGetObj<TypedScriptableNumVec>(L, 2);
            lua_Integer n = lua_tointeger(L, 3);
            lua_Integer off = lua_tointeger(L, 4);
            if(!n)
                n = glm::min(v->bytesize(), self->bytesize()) - off;
            return doRawload(L, self, v->ptr(), n, off);
        }

        luaL_argerror(L, 2, "unable to load from this type");
        luaReturnNil();
    }

    luaFunc(load)
    {
        TypedScriptableNumVec *self = This(L);
        const TypedScriptableNumVec *other = luaGetObj<TypedScriptableNumVec>(L, 2);
        if(!GLSL::areTypesMemoryCompatible(self->glsltype, other->glsltype))
            luaL_error(L, "incompatible value types: self: %s, other: %s",
                GLSL::getName(self->glsltype), GLSL::getName(other->glsltype));

        const lua_Integer otherOffs = lua_tointeger(L, 4);
        const lua_Integer selfOffs = lua_tointeger(L, 5);
        const lua_Integer otherCopyable = other->elems() - otherOffs;
        const lua_Integer selfCopyable = self->elems() - selfOffs;
        if(otherCopyable < 0)
            luaL_argerror(L, 4, "out-of-range offset");
        if(selfCopyable < 0)
            luaL_argerror(L, 5, "out-of-range offset");

        const lua_Integer maxCopyable = selfCopyable < otherCopyable ? selfCopyable : otherCopyable;
        const lua_Integer n = luaL_optinteger(L, 3, maxCopyable);
        if(n > maxCopyable)
            luaL_argerror(L, 3, "max. copyable size exceeded");

        const size_t bytes = n * self->bytesPerElem;
        self->rawload(other->ptr() + otherOffs * other->bytesPerElem, bytes, selfOffs * self->bytesPerElem);

        luaReturnInt(n);
    }

    luaFunc(clone)
    {
        luaReturnObj(This(L)->clone());
    }

    template<typename T>
    static void exportT(lua_State *L, TypedScriptableNumVec& v, int tabidx)
    {
        SNumVec<T>& s = static_cast<SNumVec<T>&>(v);
        const size_t N = v.elems();
        lua_Integer pos = 1; // start at index 1 (Lua index)
        tabidx = lua_absindex(L, tabidx);
        assert(lua_type(L, tabidx) == LUA_TTABLE);
        for(size_t i = 0; i < N; ++i)
        {
            const T& val = s[i];
            const int ncomp = lua_pushT<T>(L, val);
            for(int i = ncomp - 1; i >= 0; --i)
                lua_rawseti(L, tabidx, pos + i); // Set values backwards since lua_rawseti() pops from top of stack
            pos += ncomp;
        }
    }

    luaFunc(export)
    {
        TypedScriptableNumVec *self = This(L);
        const size_t ncomp = GLSL::scalarComponents(self->glsltype);
        lua_createtable(L, (int)(self->elems() * ncomp), 3);
        lua_pushstring(L, GLSL::getName(self->glsltype));
        lua_setfield(L, -2, "type"); // GLSL Type, as string! (for compatibility -- the numeric values are internal only)
        lua_pushinteger(L, self->elems());
        lua_setfield(L, -2, "n");    // number of elements
        lua_pushinteger(L, ncomp);
        lua_setfield(L, -2, "components");

        switch(self->glsltype)
        {
#define PRIMTYPE(cty, glty) case GLSL::glty: exportT<cty>(L, *self, -1); break;
#define GLSLTYPE(glty) case GLSL::glty: exportT<glm::glty>(L, *self, -1); break;
#include "glsl_gen.h"
            default: badtype(L, 1);
        }

        return 1;
    }
};


const luaL_Reg LuaNumVec::methods[] =
{
    luaRegisterNewDelete(LuaNumVec),
    luaRegisterMethod(LuaNumVec, newRawView),

    luaRegisterMethod(LuaNumVec, Pack),
    luaRegisterMethod(LuaNumVec, UnpackRaw),

#define A(mth, op) luaRegisterMethod(LuaNumVec, mth),
    ALLMETHODS
#undef A

    luaRegisterMethod(LuaNumVec, viewAs),
    luaRegisterMethod(LuaNumVec, clone),
    luaRegisterMethod(LuaNumVec, unpack),
    luaRegisterMethod(LuaNumVec, ptr),
    luaRegisterMethod(LuaNumVec, rawptr),
    luaRegisterMethod(LuaNumVec, len),
    luaRegisterMethod(LuaNumVec, rawsize),
    luaRegisterMethod(LuaNumVec, bytesPerElem),
    luaRegisterMethod(LuaNumVec, compPerElem),
    luaRegisterMethod(LuaNumVec, rawload),
    luaRegisterMethod(LuaNumVec, load),
    luaRegisterMethod(LuaNumVec, set),
    luaRegisterMethod(LuaNumVec, export),
    luaRegisterMethod(LuaNumVec, reduce),

    // metamethods
    //luaRegisterMethod(LuaNumVec, __index),
    //{ "__newindex", LuaNumVec::l_set },
    { "__len", LuaNumVec::l_len },
};

luaFunc(israwptr)
{
    luaReturnBool(lua_type(L, 1) == LUA_TLIGHTUSERDATA);
}

static const LuaEnum numvecenum[] =
{
#define A(mth, op) { #op, NumVecOp::OP_##op }, // Use capitalized operator names
    ALLMETHODS
#undef A
    {NULL, NULL}
};

static const luaL_Reg numveclib[] =
{
    {NULL, NULL}
};




// This function is registered globally
int register_lua_numvec(lua_State *L)
{
    LuaStackCheck(L, 1);

    luaL_newlib(L, numveclib);
    luaRegisterClassKeep<LuaNumVec>(L);
    luaRegisterEnumInLib(L, numvecenum, -1);
    lua_setfield(L, -2,  TypedScriptableNumVec::scriptdef.classname);
    

    return 1;
}

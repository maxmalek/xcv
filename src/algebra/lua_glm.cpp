#include "lua_glm.h"
#include "luainternal.h"
#include "glmx.h"
#include <memory.h>
#include "pmalloca.h"


#define F(i) lua_checkfloat(L, i)

luaFunc(ortho)
{
    // left, right, bottom, top
    luaReturnT(glm::ortho(F(1), F(2), F(3), F(4)));
}

luaFunc(lookat)
{
    glm::vec3 eye = lua_tovec3(L, 1);
    glm::vec3 at = lua_tovec3(L, 4);
    glm::vec3 up = lua_optvec3(L, 7, glm::vec3(0, -1, 0));
    luaReturnT(glm::lookAt(eye, at ,up));
}

luaFunc(normalize)
{

    const int top = lua_gettop(L);
    if(!top)
        return 0;
    float sqsum = 0;
    float *buf = (float*)_malloca(top * sizeof(float));
    ASSUME(buf);
    for(int i = 0; i < top; ++i)
    {
        const float f = lua_tofloat(L, i+1); // Lua index
        buf[i] = f;
        sqsum += f*f;
    }
    lua_settop(L, 0); // *replace* values on the stack (avoid stack overflow with a lot of params)
    const float m = sqsum ? 1.0f / glm::sqrt(sqsum) : 0.0f;
    for(int i = 0; i < top; ++i)
        lua_pushfloat(L, buf[i] * m);
    _freea(buf);
    return top;
}



static const luaL_Reg glmlib[] =
{
    luaRegister(ortho),
    luaRegister(lookat),
    luaRegister(normalize),
    {NULL,  NULL}
};


int register_lua_glm(lua_State *L)
{
    LuaStackCheck(L, 1);

    luaL_newlib(L, glmlib);

    return 1;
}

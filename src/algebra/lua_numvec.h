#pragma once

#include "luaobj.h"
#include "glsl.h"
#include "numvec.h"

int register_lua_numvec(lua_State *L);


template<typename T> class SNumVec;

// Don't want the actual element type appear in the Lua object;
// it's supposed to be dynamic on the Lua side.
// The GLSL type is all that's needed to cast back to the static type.
class TypedScriptableNumVec : public ScriptableT<TypedScriptableNumVec>
{
public:
    static const ScriptDef scriptdef;
    virtual ~TypedScriptableNumVec() {}
    const GLSL::Type glsltype;
    const unsigned bytesPerElem;
    char *ptr();
    char *ptr(size_t offset);
    const char *ptr() const;
    const char *ptr(size_t offset) const;
    size_t elems() const;
    size_t bytesize() const;
    unsigned components() const;
    bool checkindex(size_t n, size_t off = 0);
    bool checkbytes(size_t bytes, size_t off = 0);
    void rawload(const void *src, size_t bytes, size_t off = 0);

    virtual TypedScriptableNumVec *clone() const = 0;


protected:
    TypedScriptableNumVec(GLSL::Type ty, unsigned esz) : glsltype(ty), bytesPerElem(esz) {}
    SNumVec<char>& _asvec();
    const SNumVec<char>& _asvec() const;

private:
    TypedScriptableNumVec(const TypedScriptableNumVec&); // link error
};


// This holds the static type.
template<typename T>
class SNumVec : public TypedScriptableNumVec, public NumVec<T>
{
public:
    typedef NumVec<T> Base;

    SNumVec(size_t n, const T& initval = T())
        : Base(n, initval)
        , TypedScriptableNumVec(GLSL::TypeFor<T>::type, sizeof(T))
    {}

    enum _IsBytes { BYTES };
    enum _IsElems { ELEMENTS };

    // Tagged constructors to make it absolutely imposssible to mess up the size,
    // having it scream BYTES or ELEMENTS in ALL CAPS is intentional.
    SNumVec(void *p, size_t bytes, NumVecBase::_ViewOrCopy how, _IsBytes)
        : Base(p, bytes / sizeof(T), how)
        , TypedScriptableNumVec(GLSL::TypeFor<T>::type, sizeof(T))
    {}

    SNumVec(void *p, size_t n, NumVecBase::_ViewOrCopy how, _IsElems)
        : Base(p, n, how)
        , TypedScriptableNumVec(GLSL::TypeFor<T>::type, sizeof(T))
    {}

    // copy ctor
    SNumVec(const SNumVec<T>& o)
        : Base(o)
        , TypedScriptableNumVec(o.glsltype, o.bytesPerElem)
    {
    }

    virtual ~SNumVec() {}

    virtual TypedScriptableNumVec *clone() const
    {
        return new SNumVec<T>(*this);
    }
};


#include "lua_imgui.h"
#include "luainternal.h"
#include "guibase.h"
#include "util.h"
#include "texture.h"
#include "pmalloca.h"
#include <algorithm>


#define S(i) luaL_checkstring(L, i)
#define SS(i) lua_checkrealstring(L, i)
#define So(i, d) luaL_optstring(L, i, d)
#define V2o(i, d) lua_optvec2(L, i, d)
#define V2(i) lua_tovec2(L, i)
#define V3(i) lua_tovec3(L, i)
#define V4(i) lua_tovec4(L, i)
#define F(i) lua_checkfloat(L, i)
#define F0(i) lua_optfloat(L, i, 0.0f)
#define F1(i) lua_optfloat(L, i, 1.0f)

inline bool maybebool(lua_State *L, int idx, bool def)
{
    if(lua_isnone(L, idx))
        return def;
    return getBool(L, idx);
}

#define deftrue(idx) maybebool(L, idx, true)

static ImTextureID tex2id(lua_State *L, const TextureAny *tex)
{
    ImTextureID id = 0;
    if(tex)
    {
        switch(tex->textype())
        {
            case TEX_1D:
            case TEX_2D:
                break; // ok
            default:
                luaL_error(L, "expected 1D/2D texture");
        }
        id = ImGui::ToTexID(tex);
    }
    return id;
}

ImGuiWindowFlags lua_imgui_getWindowFlags(lua_State *L, int idx)
{
    ImGuiWindowFlags flags = 0;
    if(const char *s = lua_tostring(L, idx))
        for( ; *s; ++s)
            switch(*s)
            {
                case 'T': flags |= ImGuiWindowFlags_NoTitleBar; break;
                case 'R': flags |= ImGuiWindowFlags_NoResize; break;
                case 'O': flags |= ImGuiWindowFlags_NoMove; break;
                case 'S': flags |= ImGuiWindowFlags_NoScrollbar; break;
                case 'M': flags |= ImGuiWindowFlags_NoScrollWithMouse; break;
                case 'C': flags |= ImGuiWindowFlags_NoCollapse; break;
                case 'a': flags |= ImGuiWindowFlags_AlwaysAutoResize; break;
                case 'b': flags |= ImGuiWindowFlags_ShowBorders; break;
                case 'h': flags |= ImGuiWindowFlags_HorizontalScrollbar; break;
                case 'H': flags |= ImGuiWindowFlags_AlwaysHorizontalScrollbar; break;
                case 'V': flags |= ImGuiWindowFlags_AlwaysVerticalScrollbar; break;
                case 't': flags |= ImGuiWindowFlags_NoSavedSettings; break;
            }

    return flags;
}




luaFunc(Button) { luaReturnBool(ImGui::Button(S(1), V2o(2, glm::vec2(0,0)))); }
luaFunc(InvisibleButton) { luaReturnBool(ImGui::InvisibleButton(S(1), V2o(2, glm::vec2(0,0)))); }
luaFunc(SmallButton) { luaReturnBool(ImGui::SmallButton(S(1))); }
luaFunc(Selectable) { luaReturnBool(ImGui::Selectable(S(1))); }
luaFunc(Text) { ImGui::TextUnformatted(S(1)); luaReturnNil(); }
luaFunc(TextDisabled)
{
    const char *s = luaL_checkstring(L, 1);
    ImGui::PushStyleColor(ImGuiCol_Text, ImGui::GetStyle().Colors[ImGuiCol_TextDisabled]);
    ImGui::TextUnformatted(s);
    ImGui::PopStyleColor();
    luaReturnNil();
}
luaFunc(TextColor)
{
    // Avoid using ImGui::TextColored() because that is printf()-style
    const char *s = luaL_checkstring(L, 1);
    glm::vec3 color = lua_tovec3(L, 2);
    float a = lua_optfloat(L, 5, 1); // default opaque
    ImGui::PushStyleColor(ImGuiCol_Text, glm::vec4(color, a));
    ImGui::TextUnformatted(s);
    ImGui::PopStyleColor();
    luaReturnNil();
}
luaFunc(Checkbox)
{
    bool b = getBool(L, 2);
    bool changed = ImGui::Checkbox(S(1), &b);
    return lua_pushT(L, glm::bvec2(changed, b));
}

// FIXME: MSVC 2015 eats this code, but not so sure about gcc/clang. Fix if necessary.
template<typename T, typename S, int e, typename callf>
static inline int sliderT(lua_State *L, callf func)
{
    const int o = 2 + e; // offset for getting the other args.
    const char *label = S(1);
    T v = lua_getT<T>(L, 2);
    const float minval = F0(o);
    const float maxval = F1(o+1);
    const char *fmt = So(o+2, "%.3f");
    const float exponent = F1(o+3);
    S *ptr = safe_reinterpret_cast<S*>(&v);
    bool mv = func(label, ptr, minval, maxval, fmt, exponent);
    lua_pushboolean(L, mv);
    return lua_pushT(L, v) + 1;
}
template<typename T, int e, typename callf>
static inline int sliderIntT(lua_State *L, callf func)
{
    const int o = 2 + e; // offset for getting the other args.
    const char *label = S(1);
    T v = lua_getT<T>(L, 2);
    const int minval = lua_checkint(L, o);
    const int maxval = lua_checkint(L, o+1);
    int *ptr = safe_reinterpret_cast<int*>(&v);
    bool mv = func(label, ptr, minval, maxval, "%.0f");
    lua_pushboolean(L, mv);
    return lua_pushT(L, v) + 1;
}
luaFunc(Slider)
{
    return sliderT<float, float, 1>(L, ImGui::SliderFloat);
}
luaFunc(Slider2)
{
    return sliderT<glm::vec2, float, 2>(L, ImGui::SliderFloat2);
}
luaFunc(Slider3)
{
    return sliderT<glm::vec3, float, 3>(L, ImGui::SliderFloat3);
}
luaFunc(Slider4)
{
    return sliderT<glm::vec4, float, 4>(L, ImGui::SliderFloat4);
}
luaFunc(SliderInt)
{
    return sliderIntT<int, 1>(L, ImGui::SliderInt);
}
template<typename T, typename S, int e, typename callf>
static inline int dragT(lua_State *L, callf func)
{
    const int o = 3 + e; // offset for getting the other args.
    const char *label = S(1);
    T v = lua_getT<T>(L, 2);
    const float speed = F1(3);
    const float minval = F0(o);
    const float maxval = F0(o+1);
    const char *fmt = So(o+2, "%.3f");
    const float exponent = F1(o+3);
    S *ptr = safe_reinterpret_cast<S*>(&v);
    bool mv = func(label, ptr, speed, minval, maxval, fmt, exponent);
    lua_pushboolean(L, mv);
    return lua_pushT(L, v) + 1;
}
luaFunc(DragFloat)
{
    return dragT<float, float, 1>(L, ImGui::DragFloat);
}
luaFunc(DragFloat2)
{
    return dragT<glm::vec2, float, 2>(L, ImGui::DragFloat2);
}
luaFunc(DragFloat3)
{
    return dragT<glm::vec3, float, 3>(L, ImGui::DragFloat3);
}
luaFunc(DragFloat4)
{
    return dragT<glm::vec4, float, 4>(L, ImGui::DragFloat4);
}



luaFunc(OpenPopup)
{
    ImGui::OpenPopup(S(1));
    luaReturnNil();
}
struct PopupRAII
{
    ~PopupRAII() { ImGui::EndPopup(); }
};

luaFunc(PopupBody)
{
    if(ImGui::BeginPopup(S(1)))
    {
        PopupRAII pop;
        return luaForwardCall(L, 1, 0);
    }
    luaReturnNil();
}

luaFunc(CloseCurrentPopup)
{
    ImGui::CloseCurrentPopup();
    luaReturnNil();
}

luaFunc(PopupContextItem)
{
    const char *id = luaL_checkstring(L, 1);
    int button = lua_optint(L, 2, 1);
    if(ImGui::BeginPopupContextItem(id, button))
    {
        PopupRAII pop;
        return luaForwardCall(L, 2, 0);
    }
    luaReturnNil();
}

luaFunc(Image)
{
    const TextureAny *tex = luaGetObjOrNil<TextureAny>(L);
    glm::vec2 sz = lua_optvec2(L, 2,  ImGui::GetContentRegionAvail());
    glm::vec2 uv0 = lua_optvec2(L, 4, glm::vec2(0,0));
    glm::vec2 uv1 = lua_optvec2(L, 6, glm::vec2(1,1));
    glm::vec4 bordercol = lua_optvec4(L, 8, glm::vec4(0,0,0,0));
    ImTextureID id = tex2id(L, tex);
    ImGui::Image(id, sz, uv0, uv1, ImVec4(1,1,1,1), bordercol);
    luaReturnNil();
}

luaFunc(ImageButton)
{
    const TextureAny *tex = luaGetObjOrNil<TextureAny>(L);
    glm::vec2 sz = lua_optvec2(L, 2,  ImGui::GetContentRegionAvail());
    glm::vec2 uv0 = lua_optvec2(L, 4, glm::vec2(0,0));
    glm::vec2 uv1 = lua_optvec2(L, 6, glm::vec2(1,1));
    int framepadding = lua_optint(L, 8, -1);
    glm::vec4 bgcol = lua_optvec4(L, 9, glm::vec4(0,0,0,0));
    ImTextureID id = tex2id(L, tex);
    bool clicked = ImGui::ImageButton(id, sz, uv0, uv1, framepadding, bgcol, ImVec4(1,1,1,1));
    luaReturnBool(clicked);
}

luaFunc(SameLine)
{
    ImGui::SameLine();
    luaReturnNil();
}

luaFunc(IsMouseHoveringAnyWindow)
{
    luaReturnBool(ImGui::IsMouseHoveringAnyWindow());
}

luaFunc(IsMouseHoveringRootWindow)
{
    luaReturnBool(ImGui::IsMouseHoveringRootWindow());
}

luaFunc(IsAnyItemActive)
{
    luaReturnBool(ImGui::IsAnyItemActive());
}

struct GroupRIAA
{
    GroupRIAA() { ImGui::BeginGroup(); }
    ~GroupRIAA() { ImGui::EndGroup(); }
};

struct PushIDRIAA
{
    PushIDRIAA(lua_State *L)
    {
        if(const void *p = lua_topointer(L, 1))
            ImGui::PushID(p);
        else if(lua_isrealstring(L, 1))
            ImGui::PushID(lua_tostring(L, 1));
        else
            ImGui::PushID(lua_toint(L, 1));
    }
    ~PushIDRIAA() { ImGui::PopID(); }
};

struct ChildRIAA
{
    ChildRIAA(lua_State *L)
    {
        glm::vec2 size = lua_optvec2(L, 2, glm::vec2(0));
        bool border = getBool(L, 4);
        ImGuiWindowFlags flags = lua_imgui_getWindowFlags(L, 5); // FIXME
        if(lua_isnumber(L, 1))
            ImGui::BeginChild(lua_touint(L, 1), size, border, flags);
        else
            ImGui::BeginChild(luaL_checkstring(L, 1), size, border, flags);
    }
    ~ChildRIAA() { ImGui::EndChild(); }
};

luaFunc(Group)
{
    GroupRIAA g;
    return luaForwardCall(L, 0, 0);
}

luaFunc(SubID)
{
    PushIDRIAA p(L);
    return luaForwardCall(L, 1, 0);
}

luaFunc(Child)
{
    ChildRIAA c(L);
    return luaForwardCall(L, 5, 0);
}


struct MainMenuBarRIAA
{
    MainMenuBarRIAA() : opened(ImGui::BeginMainMenuBar()) {}
    ~MainMenuBarRIAA() { if(opened) ImGui::EndMainMenuBar(); }
    const bool opened;
    inline operator bool() const { return opened; }
};

luaFunc(MainMenuBar)
{
    MainMenuBarRIAA m;
    if(m)
        return luaForwardCall(L, 0, 0);
    luaReturnNil();
}

struct MenuBarRIAA
{
    MenuBarRIAA() : opened(ImGui::BeginMenuBar()) {}
    ~MenuBarRIAA() { if(opened) ImGui::EndMenuBar(); }
    const bool opened;
    inline operator bool() const { return opened; }
};


luaFunc(MenuBar)
{
    MenuBarRIAA m;
    if(m)
        return luaForwardCall(L, 0, 0);
    luaReturnNil();
}

struct MenuRIAA
{
    MenuRIAA(const char *s, bool enabled) : opened(ImGui::BeginMenu(s, enabled)) {}
    ~MenuRIAA() { if(opened) ImGui::EndMenu(); }
    const bool opened;
    inline operator bool() const { return opened; }
};

luaFunc(Menu)
{
    MenuRIAA m(S(1), getBool(L, 2));
    lua_pushboolean(L, m.opened);
    return !m ? 1 : luaForwardCall(L, 2, 1);
}

luaFunc(MenuItem)
{
    bool selected = getBool(L, 2);
    bool open = ImGui::MenuItem(S(1), lua_tostring(L, 3), selected, deftrue(4));
    luaReturnBool(open);
}

luaFunc(ProgressBar)
{
    ImGui::ProgressBar(F(1), lua_optvec2(L, 2, glm::vec2(-1, 0)), lua_tostring(L, 4));
    luaReturnNil();
}

luaFunc(Dummy)
{
    ImGui::Dummy(lua_optvec2(L, 1, glm::vec2(0)));
    luaReturnNil();
}

luaFunc(GetContentRegionAvail)
{
    luaReturnT(glm::vec2(ImGui::GetContentRegionAvail()));
}

luaFunc(Separator)
{
    ImGui::Separator();
    luaReturnNil();
}

luaFunc(CalcTextSize)
{
    const float wrap = lua_optfloat(L, 2, -1.0f);
    glm::vec2 sz = ImGui::CalcTextSize(S(1), NULL, false, wrap);
    luaReturnT(sz);
}

luaFunc(IsItemHovered)
{
    luaReturnBool(ImGui::IsItemHovered());
}

luaFunc(IsItemClicked)
{
    luaReturnBool(ImGui::IsItemClicked());
}

luaFunc(IsItemActive)
{
    luaReturnBool(ImGui::IsItemActive());
}

struct TooltipRIAA
{
    TooltipRIAA() { ImGui::BeginTooltip(); }
    ~TooltipRIAA() { ImGui::EndTooltip(); }
};

luaFunc(Tooltip)
{
    if(lua_isrealstring(L, 1))
    {
        ImGui::SetTooltip("%s", S(1));
        luaReturnNil();
    }

    TooltipRIAA tt;
    return luaForwardCall(L, 0, 0);
}


// pops top off the stack first, leaves string on the stack
static bool comboTableGetter(void* data, int idx, const char** out_text)
{
    lua_State *L = (lua_State*)data;
    LuaStackCheck(L, 0);
    lua_pop(L, 1);
    assert(lua_type(L, -1) == LUA_TTABLE);
    lua_rawgeti(L, -1, idx + 1);
    const char *s = luaL_tolstring(L, -1, NULL);
    lua_replace(L, -2);
    *out_text = s;
    // can't pop the string immediately, because then the GC might eat it
    return true;
}

// Returns clicked, index, element
luaFunc(Combo)
{
    const char *label = luaL_checkstring(L, 1);
    int sel = lua_toint(L, 2) - 1; // Lua -> C index
    if(sel < 0)
        sel = 0;
    bool res = false;
    int height = lua_toint(L, 4);
    if(height <= 0)
        height = -1;
    switch(lua_type(L, 3))
    {
        case LUA_TSTRING:
        {
            const char *s = lua_tostring(L, 3);
            res = ImGui::Combo(label, &sel, s, height);
            assert(sel >= 0);
            for(int i = 0; i < sel; ++i)
                s += strlen(s) + 1;
            lua_pushstring(L, s); // leave element on the stack
            break;
        }
        case LUA_TTABLE:
        {
            int len = (int)luaL_len(L, 3);
            lua_pushvalue(L, 3); // put table op top
            lua_pushnil(L); // sentinel
            res = ImGui::Combo(label, &sel, comboTableGetter, L, len, height);
            assert(sel >= 0);
            lua_rawgeti(L, 3, sel + 1); // leave element on the stack
            break;
        }
        default:
            luaL_error(L, "expected table or string with null-terminated parts");
    }
    lua_pushboolean(L, res);
    lua_pushinteger(L, sel + 1); // Lua index
    lua_pushvalue(L, -3);
    return 3;
}

static ImGuiInputTextFlags getInputTextFlags(lua_State *L, int idx)
{
    unsigned flags = 0;
    if(const char *s = lua_tostring(L, idx))
        for( ; *s; ++s)
            switch(*s)
            {
                case 'd': flags |= ImGuiInputTextFlags_CharsDecimal; break;
                case 's': flags |= ImGuiInputTextFlags_AutoSelectAll; break;
                case 't': flags |= ImGuiInputTextFlags_AllowTabInput; break;
                case 'r': flags |= ImGuiInputTextFlags_ReadOnly; break;
                case 'p': flags |= ImGuiInputTextFlags_Password; break;
                case 'e': flags |= ImGuiInputTextFlags_EnterReturnsTrue; break;
            }

    return flags;
}

/*static void inputCallback(ImGuiTextEditCallbackData *data)
{
}*/

luaFunc(InputText)
{
    const char *label = luaL_checkstring(L, 1);
    size_t len = 0;
    const char *text = lua_tolstring(L, 2, &len);
    size_t maxlen = lua_tointeger(L, 3);
    if(maxlen <= 1)
        maxlen = len + 128;
    const unsigned flags = getInputTextFlags(L, 4);

    const size_t copylen = std::min(len, maxlen);
    char * const buf = (char*)_malloca(maxlen+1);
    ASSUME(buf);
    memcpy(buf, text, copylen);
    buf[copylen] = 0;

    const bool changed = ImGui::InputText(label, buf, maxlen+1, flags);
    lua_pushboolean(L, changed);
    if(changed)
        lua_pushstring(L, buf); // This makes a copy
    else
    {
        lua_replace(L, 1);
        lua_settop(L, 2); // Keep original string on the stack; don't copy anything
    }

    _freea(buf);

    return 2;
}

static int multiLineCB(ImGuiTextEditCallbackData *data)
{
    glm::vec2& scroll = *(glm::vec2*)data->UserData;
    scroll.x = ImGui::GetScrollX();
    scroll.y = ImGui::GetScrollY();
    return 0;
}

// Returns (changed, text)
luaFunc(InputTextMultiline)
{
    const char *label = luaL_checkstring(L, 1);
    size_t len = 0;
    const char *text = lua_tolstring(L, 2, &len);
    size_t maxlen = lua_tointeger(L, 3);
    if(maxlen == 0)
        maxlen = len + 2048;
    const unsigned flags = getInputTextFlags(L, 4);
    glm::vec2 size = lua_optvec2(L, 5, glm::vec2(0,0));

    const size_t copylen = std::min(len, maxlen);
    char * const buf = (char*)_malloca(maxlen+1);
    ASSUME(buf);
    memcpy(buf, text, copylen);
    buf[copylen] = 0;

    glm::vec2 scroll;
    const bool changed = ImGui::InputTextMultiline(label, buf, maxlen+1, size, flags | ImGuiInputTextFlags_CallbackAlways, multiLineCB, &scroll);
    lua_pushboolean(L, changed);
    if(changed)
        lua_pushstring(L, buf); // This makes a copy
    else
    {
        lua_replace(L, 1);
        lua_settop(L, 2); // Keep original text on the stack; don't copy anything
    }

    lua_pushfloat(L, scroll.x);
    lua_pushfloat(L, scroll.y);

    _freea(buf);

    return 4;
}

struct ItemWidthRIAA
{
    ItemWidthRIAA(float w) { ImGui::PushItemWidth(w); }
    ~ItemWidthRIAA() { ImGui::PopItemWidth(); }
};

luaFunc(ItemWidth)
{
    ItemWidthRIAA wp(lua_checkfloat(L, 1));
    return luaForwardCall(L, 1, 0);
}

luaFunc(IsMouseButton)
{
    int button = lua_toint(L, 1);
    luaReturnBool(ImGui::IsMouseDown(button));
}

luaFunc(IsMouseClicked)
{
    int button = lua_toint(L, 1);
    luaReturnBool(ImGui::IsMouseClicked(button));
}

luaFunc(GetCursorPos)
{
    glm::vec2 pos = ImGui::GetCursorPos();
    luaReturnT(pos);
}

luaFunc(SetCursorPos)
{
    glm::vec2 pos = lua_tovec2(L, 1);
    ImGui::SetCursorPos(pos);
    luaReturnNil();
}

luaFunc(GetCursorScreenPos)
{
    glm::vec2 pos = ImGui::GetCursorScreenPos();
    luaReturnT(pos);
}

luaFunc(SetCursorScreenPos)
{
    glm::vec2 pos = lua_tovec2(L, 1);
    ImGui::SetCursorScreenPos(pos);
    luaReturnNil();
}



luaFunc(GetMousePos)
{
    glm::vec2 m = ImGui::GetMousePos();
    luaReturnT(m);
}

luaFunc(GetMouseDragDelta)
{
    int button = (int)lua_tointeger(L, 1);
    float lockTh = lua_tofloat(L, 2);
    glm::vec2 m = ImGui::GetMouseDragDelta(button, lockTh);
    luaReturnT(m);
}

luaFunc(ResetMouseDragDelta)
{
    int button = (int)lua_tointeger(L, 1);
    ImGui::ResetMouseDragDelta(button);
    luaReturnNil();
}

luaFunc(GetMouseWheelDelta)
{
    luaReturnT(ImGui::GetIO().MouseWheel);
}

luaFunc(GetMouseMoveDelta)
{
    glm::vec2 m = ImGui::GetIO().MouseDelta;
    luaReturnT(m);
}


static float histoGetVal(void *p, int idx)
{
    lua_State *L = (lua_State*)p;
    lua_rawgeti(L, 2, idx + 1); // to Lua index
    float v = lua_tofloat(L, -1);
    lua_pop(L, 1);
    return v;
}
luaFunc(PlotHistogram)
{
    const char *label = S(1);
    lua_Integer N = luaL_len(L, 2); // expect table
    int offset = lua_toint(L, 3);
    const char *overlay = lua_tostring(L, 4); // maybe NULL
    float minscale = lua_optfloat(L, 5, FLT_MAX);
    float maxscale = lua_optfloat(L, 6, FLT_MAX);
    glm::vec2 graphsize = lua_optvec2(L, 7, glm::vec2(0));
    ImGui::PlotHistogram(label, histoGetVal, L, int(N), offset, overlay, minscale, maxscale, graphsize);
    luaReturnNil();
}

luaFunc(ColorButton)
{
    glm::vec3 col = lua_tovec3(L, 1);
    float a = lua_optfloat(L, 4, 1.0f);
    glm::vec4 c(col, a);
    bool small = getBool(L, 5);
    bool border = deftrue(6);
    bool p = ImGui::ColorButton(c, small, border);

    // draw border around it, manually. Sigh, why is there no proper API for this...
    if(border)
    {
        glm::vec2 ul = ImGui::GetItemRectMin();
        glm::vec2 br = ImGui::GetItemRectMax();
        ImU32 icol = ImColor(glm::vec4(col, 1.0));
        ImGui::GetWindowDrawList()->AddRect(ul+glm::vec2(1), br-glm::vec2(1), icol, 0, 0, 2);
        ImGui::GetWindowDrawList()->AddRect(ul-glm::vec2(1), br+glm::vec2(1), 0xffffffff, 0, 0, 1);
    }

    luaReturnBool(p);
}

luaFunc(ColorEdit3)
{
    const char *label = S(1);
    glm::vec3 c = lua_optvec3(L, 2, glm::vec3(0));
    bool ch = ImGui::ColorEdit3(label, &c[0]);
    lua_pushboolean(L, ch);
    lua_pushvec(L, c);
    return 4;
}

luaFunc(ColorEdit4)
{
    const char *label = S(1);
    glm::vec4 c = lua_optvec4(L, 2, glm::vec4(0));
    bool ch = ImGui::ColorEdit4(label, &c[0]);
    lua_pushboolean(L, ch);
    lua_pushvec(L, c);
    return 5;
}

luaFunc(GetWindowSize)
{
    glm::vec2 wsz = ImGui::GetWindowSize();
    luaReturnT(wsz);
}

luaFunc(GetWindowPos)
{
    glm::vec2 wp = ImGui::GetWindowPos();
    luaReturnT(wp);
}

luaFunc(IsWindowFocused)
{
    luaReturnBool(ImGui::IsWindowFocused());
}

// Lower bits store the ID, high byte stores property type
enum PropertyMask
{
    PROPMASK_COLOR = 0x00000000,
    PROPMASK_STYLE = 0x01000000,

    PROPMASK_MASK  = 0xff000000,
};
struct PropertyCount
{
    PropertyCount() : ncol(0), nstyle(0) {}
    ~PropertyCount()
    {
        if(ncol)
            ImGui::PopStyleColor(ncol);
        if(nstyle)
            ImGui::PopStyleVar(nstyle);
    }
    unsigned ncol;
    unsigned nstyle;
};
template<typename V>
static void getvelems(lua_State *L, int arg, V& v, const V& def = V())
{
    LuaStackCheck(L, 0);
    arg = lua_absindex(L, arg);
    for(unsigned i = 0; i < V::components; ++i) // Lua index
    {
        lua_rawgeti(L, arg, i+1);
        v[i] = lua_optfloat(L, -1, def[i]);
    }
    lua_pop(L, V::components);
}
static const glm::vec4 defaultColor(0,0,0,1);
static void _applyStyle(lua_State *L, void *ud)
{
    PropertyCount *pc = (PropertyCount*)ud;
    unsigned key = lua_toint(L, -2);
    switch(key & PROPMASK_MASK)
    {
        case PROPMASK_COLOR:
        {
            ++pc->ncol;
            glm::vec4 color;
            if(lua_type(L, -1) == LUA_TTABLE)
                getvelems(L, -1, color, defaultColor);
            else
                color = ImVec4(ImColor(lua_touint(L, -2)));
            ImGui::PushStyleColor(ImGuiCol(key & ~PROPMASK_MASK), color);
        }
        break;

        case PROPMASK_STYLE:
        {
            ++pc->nstyle;
            glm::vec2 param(0);
            if(lua_type(L, -1) == LUA_TTABLE)
                getvelems(L, -1, param);
            else
                param.x = lua_tofloat(L, -2);
            ImGui::PushStyleVar(ImGuiStyleVar(key & ~PROPMASK_MASK), param);
        }
    }
}
luaFunc(ApplyStyle)
{
    luaL_checktype(L, 1, LUA_TTABLE);
    PropertyCount pc;
    luaForeach(L, _applyStyle, 1, &pc);
    return luaForwardCall(L, 1, 0);
}

luaFunc(SetKeyboardFocusHere)
{
    ImGui::SetKeyboardFocusHere(lua_toint(L, 1));
    luaReturnNil();
}

luaFunc(GetDisplaySize)
{
    glm::vec2 sz = ImGui::GetIO().DisplaySize;
    luaReturnT(sz);
}

luaFunc(RootDock)
{
    glm::vec2 pos = lua_tovec2(L, 1);
    glm::vec2 size = lua_tovec2(L, 3);
    ImGui::RootDock(pos, size);
    luaReturnNil();
}

static ImGuiTreeNodeFlags getTreeNodeFlags(lua_State *L, int idx)
{
    const char *s = luaL_optstring(L, idx, "");
    ImGuiTreeNodeFlags f = 0;
    while(true)
        switch(*s++)
        {
            case 's': f |= ImGuiTreeNodeFlags_Selected; break;
            case 'f': f |= ImGuiTreeNodeFlags_Framed; break;
            case 'v': f |= ImGuiTreeNodeFlags_AllowOverlapMode; break;
            //case 'p': f |= ImGuiTreeNodeFlags_NoTreePushOnOpen; break; // dangerous?
            case 'L': f |= ImGuiTreeNodeFlags_NoAutoOpenOnLog; break;
            case 'o': f |= ImGuiTreeNodeFlags_DefaultOpen; break;
            case 'd': f |= ImGuiTreeNodeFlags_OpenOnDoubleClick; break;
            case 'a': f |= ImGuiTreeNodeFlags_OpenOnArrow; break;
            case 'l': f |= ImGuiTreeNodeFlags_Leaf; break;
            case 'b': f |= ImGuiTreeNodeFlags_Bullet; break;
            case 0: return f;
        }
}

struct TreePopRAII
{
    ~TreePopRAII() { ImGui::TreePop(); }
};

luaFunc(TreeNode)
{
    const char *label = luaL_checkstring(L, 1);
    const ImGuiTreeNodeFlags flags = getTreeNodeFlags(L, 2);
    bool expand = ImGui::TreeNodeEx(label, flags);
    lua_pushboolean(L, expand);
    if(!expand)
        return 1;
    TreePopRAII pop;
    return luaForwardCallOpt(L, 2, 1);
}

luaFunc(CollapsingHeader)
{
    const char *label = luaL_checkstring(L, 1);
    bool open = true;
    bool *p_open = getBool(L, 2) ? &open : NULL;
    ImGuiTreeNodeFlags flags = getTreeNodeFlags(L, 3);
    bool vis = ImGui::CollapsingHeader(label, p_open, flags);
    lua_pushboolean(L, vis);
    lua_pushboolean(L, open);
    return !vis ? 2 : luaForwardCallOpt(L, 3, 2);
}

luaFunc(GetTextLineHeight)
{
    luaReturnNum(ImGui::GetTextLineHeight());
}

struct TextWrapRAII
{
    TextWrapRAII(float x) { ImGui::PushTextWrapPos(x); }
    ~TextWrapRAII() { ImGui::PopTextWrapPos(); }
};

luaFunc(TextWrapWidth) // relative
{
    TextWrapRAII wr(ImGui::GetCursorPos().x + lua_checkfloat(L, 1));
    return luaForwardCall(L, 1, 0);
}
luaFunc(TextWrapPos) // absolute
{
    TextWrapRAII wr(lua_checkfloat(L, 1));
    return luaForwardCall(L, 1, 0);
}

luaFunc(DrawLine)
{
    glm::vec2 lstart = lua_tovec2(L, 1);
    glm::vec2 lend = lua_tovec2(L, 3);
    unsigned color = lua_touint(L, 5);
    float thickness = lua_tofloat(L, 6);
    ImDrawList* draw_list = ImGui::GetWindowDrawList();
    draw_list->AddLine(lstart, lend, color, thickness);
    luaReturnNil();
}

luaFunc(GetItemRect)
{
    glm::vec2 ul = ImGui::GetItemRectMin();
    glm::vec2 br = ImGui::GetItemRectMax();
    luaReturnVec4(ul.x, ul.y, br.x, br.y);
}

luaFunc(ShowHelpMarker)
{
    luaReturnBool(ImGui::ShowHelpMarker(S(1))); // true when hovered
}

luaFunc(Bullet)
{
    ImGui::Bullet();
    luaReturnNil();
}

/*
struct WindowRIAA
{
    WindowRIAA(bool open) : _open(open) {}
    ~WindowRIAA() { if(_open) ImGui::End(); }
    const bool _open;
}

luaFunc(SubWindow)
{
    WindowRIAA w(ImGui::Begin(luaL_checkstring(L, 1),
}
*/

/*luaFunc(w)
{
    ImGui::PushItemWidth
}*/



static const luaL_Reg imguilib[] =
{
    luaRegister(Button),
    luaRegister(InvisibleButton),
    luaRegister(SmallButton),
    luaRegister(Selectable),
    luaRegister(Text),
    luaRegister(TextColor),
    luaRegister(TextDisabled),
    luaRegister(OpenPopup),
    luaRegister(PopupBody),
    luaRegister(CloseCurrentPopup),
    luaRegister(PopupContextItem),
    luaRegister(Checkbox),
    luaRegister(Slider),
    luaRegister(Slider2),
    luaRegister(Slider3),
    luaRegister(Slider4),
    luaRegister(DragFloat),
    luaRegister(DragFloat2),
    luaRegister(DragFloat3),
    luaRegister(DragFloat4),
    luaRegister(SliderInt),
    luaRegister(Image),
    luaRegister(ImageButton),
    luaRegister(SameLine),
    luaRegister(IsMouseHoveringAnyWindow),
    luaRegister(IsMouseHoveringRootWindow),
    luaRegister(IsAnyItemActive),
    luaRegister(Child),
    luaRegister(Group),
    luaRegister(SubID),
    luaRegister(MainMenuBar),
    luaRegister(MenuBar),
    luaRegister(Menu),
    luaRegister(MenuItem),
    luaRegister(ProgressBar),
    luaRegister(Dummy),
    luaRegister(GetContentRegionAvail),
    luaRegister(Separator),
    luaRegister(CalcTextSize),
    luaRegister(IsItemHovered),
    luaRegister(IsItemClicked),
    luaRegister(IsItemActive),
    luaRegister(Tooltip),
    luaRegister(Combo),
    luaRegister(InputText),
    luaRegister(InputTextMultiline),
    luaRegister(ItemWidth),
    luaRegister(IsMouseButton),
    luaRegister(IsMouseClicked),
    luaRegister(GetMousePos),
    luaRegister(GetCursorPos),
    luaRegister(SetCursorPos),
    luaRegister(GetCursorScreenPos),
    luaRegister(SetCursorScreenPos),
    luaRegister(GetMouseDragDelta),
    luaRegister(ResetMouseDragDelta),
    luaRegister(GetMouseWheelDelta),
    luaRegister(GetMouseMoveDelta),
    luaRegister(PlotHistogram),
    luaRegister(ColorButton),
    luaRegister(ColorEdit3),
    luaRegister(ColorEdit4),
    luaRegister(GetWindowSize),
    luaRegister(GetWindowPos),
    luaRegister(IsWindowFocused),
    luaRegister(ApplyStyle),
    luaRegister(SetKeyboardFocusHere),
    luaRegister(RootDock),
    luaRegister(GetDisplaySize),
    luaRegister(TreeNode),
    luaRegister(CollapsingHeader),
    luaRegister(GetTextLineHeight),
    luaRegister(TextWrapWidth),
    luaRegister(TextWrapPos),
    luaRegister(DrawLine),
    luaRegister(GetItemRect),
    luaRegister(ShowHelpMarker),
    luaRegister(Bullet),
    {NULL, NULL}
};

#define COL(e) { "Col_" #e, PROPMASK_COLOR | ImGuiCol_##e }
#define STY(e) { "Style_" #e, PROPMASK_STYLE | ImGuiStyleVar_##e }

static const LuaEnum imguilibenum[] =
{
    // From imgui.h ...
    COL(Text),
    COL(TextDisabled),
    COL(WindowBg),
    COL(ChildWindowBg),         // Background of child windows
    COL(PopupBg),               // Background of popups), menus), tooltips windows
    COL(Border),
    COL(BorderShadow),
    COL(FrameBg),               // Background of checkbox), radio button), plot), slider), text input
    COL(FrameBgHovered),
    COL(FrameBgActive),
    COL(TitleBg),
    COL(TitleBgCollapsed),
    COL(TitleBgActive),
    COL(MenuBarBg),
    COL(ScrollbarBg),
    COL(ScrollbarGrab),
    COL(ScrollbarGrabHovered),
    COL(ScrollbarGrabActive),
    COL(ComboBg),
    COL(CheckMark),
    COL(SliderGrab),
    COL(SliderGrabActive),
    COL(Button),
    COL(ButtonHovered),
    COL(ButtonActive),
    COL(Header),
    COL(HeaderHovered),
    COL(HeaderActive),
    COL(Column),
    COL(ColumnHovered),
    COL(ColumnActive),
    COL(ResizeGrip),
    COL(ResizeGripHovered),
    COL(ResizeGripActive),
    COL(CloseButton),
    COL(CloseButtonHovered),
    COL(CloseButtonActive),
    COL(PlotLines),
    COL(PlotLinesHovered),
    COL(PlotHistogram),
    COL(PlotHistogramHovered),
    COL(TextSelectedBg),
    COL(ModalWindowDarkening),
    STY(Alpha),               // float
    STY(WindowPadding),       // ImVec2
    STY(WindowRounding),      // float
    STY(WindowMinSize),       // ImVec2
    STY(ChildWindowRounding), // float
    STY(FramePadding),        // ImVec2
    STY(FrameRounding),       // float
    STY(ItemSpacing),         // ImVec2
    STY(ItemInnerSpacing),    // ImVec2
    STY(IndentSpacing),       // float
    STY(GrabMinSize),         // float

    { NULL, 0 },
};

#undef STY
#undef COL


int register_lua_imgui(lua_State *L)
{
    LuaStackCheck(L, 1);

    luaL_newlib(L, imguilib);
    luaRegisterEnumInLib(L, imguilibenum, -1);
    return 1;
}

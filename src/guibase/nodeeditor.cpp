#include "nodeeditor.h"
#include <math.h>
#include <float.h>
#include <string.h>
#include <algorithm>
#include "guibase.h"
#include "util.h"
#include "macros.h"
#include <imgui/imgui_internal.h>
#include <SDL_keycode.h>

const ScriptDef NodeEditorView::scriptdef("imnodeeditor", ScriptTypes::IMNODEEDITOR);
const ScriptDef NodeView::scriptdef("imnode", ScriptTypes::IMNODE, NULL, ScriptDef::PERSISTENT);
const ScriptDef Connection::scriptdef("imnodeconnection", ScriptTypes::IMNODECONNECTION, NULL, ScriptDef::PERSISTENT);

// Based on https://gist.github.com/emoon/b8ff4b4ce4f1b43e79f2


const float NODE_SLOT_SELECT_RADIUS = 9.0f;
const float NODE_SLOT_DRAW_RADIUS = 6.0f;
const glm::vec2 NODE_WINDOW_PADDING(4.0f, 4.0f);
const unsigned CONN_LINE_SEGMENTS = 32;

template<SDL_Keycode k>
static bool isKey()
{
    compile_assert((k & ~SDLK_SCANCODE_MASK) < Countof(ImGuiIO::KeysDown));
    return ImGui::GetIO().KeysDown[k & ~SDLK_SCANCODE_MASK];
}

static bool isCtrlPressed()
{
    return isKey<SDLK_LCTRL>() || isKey<SDLK_RCTRL>();
}

static bool isTouchingLine(glm::vec2 lineStart, glm::vec2 lineEnd, glm::vec2 point, float thickness, glm::vec2 *pClosest)
{
    const glm::vec2 dir = lineEnd - lineStart;
    const glm::vec2 diff = point - lineStart;
    const float t = glm::clamp(glm::dot(diff, dir) / glm::dot(dir, dir), 0.0f, 1.0f);
    const glm::vec2 closest = lineStart + t * dir;
    glm::vec2 d = point - closest;
    float distsqr = glm::dot(d, d);
    if(pClosest)
        *pClosest = closest;
    return distsqr <= thickness*thickness;
}



// ------ Copied out of ImGui and slightly modified ------------------------------------

struct CurveSegment
{
    CurveSegment()
        : start(glm::uninitialize), end(glm::uninitialize) {}
    CurveSegment(glm::vec2 a, glm::vec2 b, ImU32 c)
        : start(a), end(b), color(c) {}
    glm::vec2 start, end;
    ImU32 color;
};

static CurveSegment makeCurveSegment(const ImVec2& prev, const ImVec2& p1, const ImVec2& p2, const ImVec2& p3, const ImVec2& p4, float t, ImU32 color)
{
    float u = 1.0f - t;
    float w1 = u*u*u;
    float w2 = (3*u)*(u*t);
    float w3 = (3*u)*(t*t);
    float w4 = (t*t)*t;
    ImVec2 cur = ImVec2(w1*p1.x + w2*p2.x + w3*p3.x + w4*p4.x, w1*p1.y + w2*p2.y + w3*p3.y + w4*p4.y);
    return CurveSegment(prev, cur, color);
}

static void PathBezierCurveSegments(CurveSegment *segs, const ImVec2& p1, const ImVec2& p2, const ImVec2& p3, const ImVec2& p4, int num_segments, const ImU32 *colors, unsigned ncolors, unsigned curcolor, float shift)
{
    assert(num_segments);
    const float t_step = 1.0f / (float)num_segments;
    const float shiftstep = shift * t_step;

    ImVec2 prev = p1;
    for (int i_step = 1; i_step <= num_segments; i_step++)
    {
        curcolor %= ncolors;
        float t = -shiftstep + t_step * i_step;
        prev = (*segs++ = makeCurveSegment(prev, p1, p2, p3, p4, t, colors[curcolor++])).end;
    }
    curcolor %= ncolors;
    *segs++ = makeCurveSegment(prev, p1, p2, p3, p4, 1.0f, colors[curcolor++]);
}


static void drawCurveSimple(ImDrawList *drawList, glm::vec2 p1, glm::vec2 p2, int STEPS, ImU32 color, float thickness = 3)
{
    glm::vec2 cp1 = (p1 + glm::vec2(p2.x, p1.y)) * 0.5f;
    glm::vec2 cp2 = (p2 + glm::vec2(p1.x, p2.y)) * 0.5f;
    drawList->AddBezierCurve(p1, cp1, cp2, p2, color, thickness, STEPS);
}

// segs needs space for STEPS+1 entries
static void generateCurveSegments(CurveSegment *segs, glm::vec2 p1, glm::vec2 p2, int STEPS, const ImU32 *colors, unsigned ncolors, unsigned curcolor, float thickness, float shift)
{
    glm::vec2 cp1 = (p1 + glm::vec2(p2.x, p1.y)) * 0.5f;
    glm::vec2 cp2 = (p2 + glm::vec2(p1.x, p2.y)) * 0.5f;
    PathBezierCurveSegments(segs, p1, cp1, cp2, p2, STEPS, colors, ncolors, curcolor, shift);
}

static void drawCurveSegments(ImDrawList *drawList, const CurveSegment *segs, unsigned n, glm::vec2 offset, float thickness)
{
    for(unsigned i = 0; i < n; ++i, ++segs)
        drawList->AddLine(offset + segs->start, offset + segs->end, segs->color, thickness);
}

static void drawCurveSegmentsColor(ImDrawList *drawList, const CurveSegment *segs, unsigned n, glm::vec2 offset, float thickness, ImU32 color)
{
    for(unsigned i = 0; i < n; ++i, ++segs)
        drawList->AddLine(offset + segs->start, offset + segs->end, color, thickness);
}

static bool collideCurveSegmentsPoint(const CurveSegment *segs, unsigned n, glm::vec2 testpoint, float thickness, glm::vec2 *pColl)
{
    for(unsigned i = 0; i < n; ++i, ++segs)
        if(isTouchingLine(segs->start, segs->end, testpoint, thickness, pColl))
            return true;
    return false;
}

static bool doLinesIntersect(glm::vec2 astart, glm::vec2 aend, glm::vec2 bstart, glm::vec2 bend, glm::vec2 *pOut = 0)
{
    const float denom = ((bend.y - bstart.y)*(aend.x - astart.x)) -
        ((bend.x - bstart.x)*(aend.y - astart.y));

    const float nume_a = ((bend.x - bstart.x)*(astart.y - bstart.y)) -
        ((bend.y - bstart.y)*(astart.x - bstart.x));

    const float nume_b = ((aend.x - astart.x)*(astart.y - bstart.y)) -
        ((aend.y - astart.y)*(astart.x - bstart.x));

    if(denom == 0.0f)
    {
        if(nume_a == 0.0f && nume_b == 0.0f)
        {
            if(pOut)
                *pOut = astart + (aend * 0.5f);
            return (astart == bstart && aend == bend) || (aend == bstart && bend == astart); // coincident?
        }
        return false; // parallel
    }

    const float ua = nume_a / denom;
    const float ub = nume_b / denom;

    if(ua >= 0.0f && ua <= 1.0f && ub >= 0.0f && ub <= 1.0f)
    {
        if(pOut)
            *pOut = astart + ua*(aend - astart);
        return true; // intersecting
    }

    return false;
}

static bool overlap1d(float Amin, float Amax, float Bmin, float Bmax)
{
    Amin = glm::max(Amin, Bmin);
    Amax = glm::min(Amax, Bmax);
    return Amax > Amin;
}

static bool doBoxesOverlap(glm::vec2 abox1, glm::vec2 abox2, glm::vec2 bbox1, glm::vec2 bbox2)
{
    assert(abox1.x <= abox2.x && abox1.y <= abox2.y);
    assert(bbox1.x <= bbox2.x && bbox1.y <= bbox2.y);

    return overlap1d(abox1.x, abox2.x, bbox1.x, bbox2.x)
        && overlap1d(abox1.y, abox2.y, bbox1.y, bbox2.y);
}

static bool isPointInBox(glm::vec2 p, glm::vec2 box1, glm::vec2 box2)
{
    return box1.x <= p.x && p.x < box2.x
        && box1.y <= p.y && p.y < box2.y;
}

static bool doBoxAndLineIntersect( glm::vec2 box1, glm::vec2 box2, glm::vec2 linestart, glm::vec2 lineend)
{
    return isPointInBox(linestart, box1, box2)
        || isPointInBox(lineend, box1, box2)
        || doLinesIntersect(linestart, lineend, box1, glm::vec2(box1.x, box2.y))
        || doLinesIntersect(linestart, lineend, box1, glm::vec2(box2.x, box1.y))
        || doLinesIntersect(linestart, lineend, box2, glm::vec2(box1.x, box2.y))
        || doLinesIntersect(linestart, lineend, box2, glm::vec2(box2.x, box1.y));
}

static bool isAnyCurveSegmentInBox(glm::vec2 bounds1, glm::vec2 bounds2, CurveSegment *segs, unsigned n, glm::vec2 box1, glm::vec2 box2)
{
    if(!doBoxesOverlap(box1, box2, bounds1, bounds2))
        return false;
    for(unsigned i = 0; i < n; ++i, ++segs)
        if(doBoxAndLineIntersect(box1, box2, segs->start, segs->end))
            return true;
    return false;
}

static void getUpperLeftLowerRight(glm::vec2 a, glm::vec2 b, glm::vec2 * ul, glm::vec2 * br)
{
    *ul = glm::vec2(glm::min(a.x, b.x), glm::min(a.y, b.y));
    *br = glm::vec2(glm::max(a.x, b.x), glm::max(a.y, b.y));
}


// -------------------------------------------------------

glm::vec2 ConnectorBase::getPos(glm::vec2 offset) const
{
    return pos + node->pos + offset;
}

void OutputConnector::unlinkAll()
{
    while(connections.size())
        connections[0]->unlink();
}

Connection *OutputConnector::link(InputConnector *to)
{
    for(size_t i = 0; i < connections.size(); ++i)
    {
        Connection *con = connections[i].content();
        assert(con->source == this);
        if(con->target == to)
            return con;
    }

    to->unlink();

    Connection *con = new Connection(this, to);
    connections.push_back(con);
    to->connection = con;

    node->onLinkToTarget(con, slotName(), to->slotName());
    return con;
}

void OutputConnector::unlink(InputConnector *to)
{
    for(size_t i = 0; i < connections.size(); ++i)
    {
        if(connections[i]->target == to)
        {
            CountedPtr<Connection> con = connections[i];
            std::swap(connections[i], connections.back());
            connections.pop_back();
            con->unlink();
            break;
        }
    }
}


Connection *InputConnector::link(OutputConnector *from)
{
    return from->link(this);
}

void InputConnector::unlink()
{
    if(connection)
    {
        CountedPtr<Connection> con = connection;
        con->source->node->onUnlinkFromTarget(con.content(), con->source->slotName(), slotName());
        con->onUnlink();
        connection = NULL;
        assert(con->target == this);
        con->source->unlink(this);

        con->onDestroy();
        con->unbindScript();
    }
}

Connection::Connection(OutputConnector *src, InputConnector *trg)
    : status(ANIMATED), selected(0), source(src), color(src->desc.color), overrideColor(false), target(trg)
{
    assert(src->kind == CK_OUTPUT);
    assert(trg->kind == CK_INPUT);

    updateStatus();
}

void Connection::updateColor()
{
    color = source->desc.color;
}

// called BEFORE unlinking happens
void Connection::unlink()
{
    target->unlink();
}

void Connection::onUnlink()
{
    callOpt("onUnlink", 0);
}

void Connection::onDestroy()
{
    callOpt("onDestroy", 0);
}

void Connection::setSelected(bool on)
{
    // This takes care that a connection that is in the process of being selected is correctly handled (selected == 2 and in beingselconns[])
    if(on)
    {
        if(selected == 2)
        {
            NodeEditorView *holder = source->node->getHolder();
            holder->_unselectCon(this);
        }
        selected = true;
    }
    else if(isSelected())
        selected = false;
}

void Connection::setstatus(Status s, bool on)
{
    unsigned char oldstatus = status;
    status ^= (-char(on) ^ oldstatus) & s;

    if((status ^ oldstatus) & ACTIVE)
    {
        CallEnv env(this, "onChangeActive");
        if(env)
        {
            push(active());
            env.call(0);
        }
    }
}

NodeView * Connection::getSourceNode()
{
    return source->node;
}

NodeView * Connection::getTargetNode()
{
    return target->node;
}

const char * Connection::getSourceConnName() const
{
    return source->desc.name.c_str();
}

const char * Connection::getTargetConnName() const
{
    return target->desc.name.c_str();
}

bool Connection::check() const
{
    NodeEditorView *holder = source->node->getHolder();
    return holder ? holder->_canConnect(source, target) : false;
}

bool Connection::updateStatus()
{
    bool ok = check();
    setstatus(ACTIVE, ok);
    return ok;
}

template<typename T>
static void _setupConnectors(NodeView *src, std::map<std::string, T*>& connectors, const ConnectorDesc *connectorDescs, size_t num)
{
    typedef std::map<std::string, T*> M;

    // resize to fit, drop lost cons, alloc new
    for(typename M::iterator it = connectors.begin(); it != connectors.end(); )
    {
        bool found = false;
        for(size_t i = 0; i < num; ++i)
            if(it->first == connectorDescs[i].name)
            {
                found = true;
                break;
            }
        if(found)
            ++it;
        else
        {
            delete it->second;
            it = connectors.erase(it);
        }
    }
    for (size_t i = 0; i < num; ++i)
    {
        const ConnectorDesc& desc = connectorDescs[i];
        if(T *con = connectors[desc.name])
            con->desc = desc;
        else
            connectors[desc.name] = new T(desc, src);
    }
}

static glm::vec4 highlight(glm::vec4 v)
{
    return v * 1.2f + 0.1f;
}

enum DragState
{
    DragState_Default,
    DragState_Hover,
    DragState_Dragging,
};


static bool isConnectorHovered(const ConnectorBase *c, glm::vec2 offset)
{
    glm::vec2 mousePos = ImGui::GetIO().MousePos;
    glm::vec2 conPos = offset + c->pos;

    float xd = mousePos.x - conPos.x;
    float yd = mousePos.y - conPos.y;

    return ((xd * xd) + (yd *yd)) < (NODE_SLOT_SELECT_RADIUS * NODE_SLOT_SELECT_RADIUS);
}

ConnectorBase *NodeEditorView::_getHoverCon(glm::vec2 offset)
{
    if(!active)
        return NULL;

    for(size_t n = 0; n < allnodes.size(); ++n)
        if(const NodeView *node = allnodes[n].content())
        {
            glm::vec2 nodePos = node->pos + offset;

            for(NodeView::InputConMap::const_iterator it = node->inputs.begin(); it != node->inputs.end(); ++it)
                if (isConnectorHovered(it->second, nodePos))
                    return it->second;

            for(NodeView::OutputConMap::const_iterator it = node->outputs.begin(); it != node->outputs.end(); ++it)
                if (isConnectorHovered(it->second, nodePos))
                    return it->second;
        }
    return NULL;
}

size_t NodeEditorView::getNumNodes() const
{
    size_t c = 0;
    for(size_t i = 0; i < allnodes.size(); ++i)
       c += !!allnodes[i];
    return c;
}

void NodeEditorView::sortNodeZ()
{
    nodeZsort.clear();
    for(size_t i = 0; i < allnodes.size(); ++i)
        if(NodeView *node = allnodes[i].content())
            nodeZsort.push_back(node);
    if(nodeZsort.empty())
        return;
    std::sort(nodeZsort.begin(), nodeZsort.end(), NodeView::IsInFrontOf);
    int z = (int)nodeZsort.size();
    for(size_t i = 0; i < nodeZsort.size(); ++i)
        nodeZsort[i]->_zorder = --z;
    assert(nodeZsort.back()->_zorder == 0);
}

// ---------------------------------------------------------------------

static Connection *_linkConnectors(ConnectorBase *a, ConnectorBase *b)
{
    InputConnector *input = NULL;
    OutputConnector *output = NULL;
    switch(a->kind)
    {
        case CK_INPUT: input = (InputConnector*)a; break;
        case CK_OUTPUT: output = (OutputConnector*)a; break;
    }
    switch(b->kind)
    {
        case CK_INPUT: input = (InputConnector*)b; break;
        case CK_OUTPUT: output = (OutputConnector*)b; break;
    }
    return input && output ? output->link(input) : NULL;
}

void NodeEditorView::_handleDragging(glm::vec2 offset)
{
    if(!active)
        return;

    glm::vec2 pos;
    ConnectorBase* con = _getHoverCon(offset);
    if(con)
        pos = con->getPos(offset);

    /*if(con && ImGui::IsImGuiKeyPressed(ImGuiKey_Backspace))
    {
        con->unlink();
        _dragState = DragState_Default;
        return;
    }*/

    switch (_dragState)
    {
        case DragState_Default:
        {
            _dragNode.start = con;
            _dragNode.startpos = pos;

            if (con)
            {
                _dragState = DragState_Hover;
                return;
            }
        }
        break;

        case DragState_Hover:
        {
            // Make sure we are still hovering the same node
            if (!con || con != _dragNode.start)
            {
                _dragNode.start = NULL;
                _dragState = DragState_Default;
                return;
            }

            if (ImGui::IsMouseClicked(0) && _dragNode.start)
            {
                _dragState = DragState_Dragging;

                // if already connected, break the connection first
                /*if(con->kind == CK_)
                {
                    _dragNode.startpos = con->endpoint->getPos(offset);
                    _dragNode.start = con->endpoint;
                    con->unlink();
                }*/
            }
        }
        break;

        case DragState_Dragging:
        {
            ImDrawList* drawList = ImGui::GetWindowDrawList();

            const bool canconn = _canConnect(_dragNode.start, con);
            {
                glm::vec4 color(glm::uninitialize);
                if(!con || _dragNode.start == con)
                    color = glm::vec4(0.7f);
                else if(canconn)
                    color = glm::vec4(0.0f, 0.9f, 0.3f, 1.0f);
                else
                    color = glm::vec4(0.9f, 0.0f, 0.0f, 0.8f);

                drawCurveSimple(drawList, _dragNode.startpos, ImGui::GetIO().MousePos, 0, ImColor(color));
            }

            if (!ImGui::IsMouseDown(0))
            {
                if (canconn || (con && ImGui::GetIO().KeyCtrl)) // hold ctrl when connecting to force
                    _linkConnectors(con, _dragNode.start);
                _dragNode.start = NULL;
                _dragState = DragState_Default;
            }
        }
        break;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

NodeEditorView::NodeState NodeEditorView::_drawNode(ImDrawList* drawList, glm::vec2 offset, NodeView* node)
{
    const bool wasactive = ImGui::IsAnyItemActive();

    ImGui::PushID(node->_id);
    glm::vec2 node_rect_min = offset + node->pos;

    drawList->ChannelsSplit(2);

    // Display node contents first
    drawList->ChannelsSetCurrent(1); // Foreground
    bool node_widgets_active = false;

    // Draw title in center

    glm::vec2 textSize = ImGui::CalcTextSize(node->name.c_str());
    const float lineSize = textSize.y + 5.0f;

    glm::vec2 pos = node_rect_min + NODE_WINDOW_PADDING;
    pos.x = node_rect_min.x + (node->size.x / 2) - textSize.x / 2;

    const glm::vec2 headerPos = pos; // remember that for later
    glm::vec2 newsz;
    {
        glm::vec2 screenpos = node_rect_min + NODE_WINDOW_PADDING + glm::vec2(0, lineSize);
        ImGui::SetCursorScreenPos(screenpos);

        ImGui::BeginGroup();
        {
            node->drawUI();

            // get this *before* closing the group
            if(!wasactive && node_active_used == -1 && ImGui::IsAnyItemActive()) // any control in group used?
            {
                node_widgets_active = true;
                node_active_used = node->_id;
            }
            newsz = ImGui::GetCursorScreenPos();
        }
        ImGui::EndGroup();

        glm::vec2 span = glm::vec2(ImGui::GetItemRectMax()) - glm::vec2(ImGui::GetItemRectMin());
        glm::vec2 diff = glm::max(newsz - screenpos, span);
        if(glm::any(glm::lessThan(node->customContentSize, diff)))
        {
            node->customContentSize = glm::max(node->customContentSize, diff);
            node->updateContent();
        }
    }

    // Save the size of what we have emitted and whether any of the widgets are being used
    glm::vec2 node_rect_max = node_rect_min + node->size;
    //ImGui::PushClipRect(node_rect_min, node_rect_max, true);

    // Display node box
    drawList->ChannelsSetCurrent(0);
    ImGui::SetCursorScreenPos(node_rect_min);
    //ImGui::InvisibleButton("node", node->size);
    //const bool hovered = ImGui::IsItemHovered();
    const bool hovered = active && ImGui::IsMouseHoveringRect(node_rect_min, node_rect_max, true);
    if (hovered)
        open_context_menu |= ImGui::IsMouseClicked(1);

    const bool isUsingAnyConnector = active && !!_dragNode.start;
    const bool isUsingMyConnector = active && _dragNode.start && _dragNode.start->node == node;
    const bool hilight = active && (hovered || isUsingMyConnector);
    const bool node_moving_active = active && !isUsingAnyConnector && hovered && ImGui::IsMouseDown(0);

    glm::vec4 bgcol = hilight
        ? highlight(node->bodyColor)
        : node->bodyColor;

    glm::vec4 titlecol = hilight
        ? highlight(node->titleColor)
        : node->titleColor;

    if(node->selected)
        drawList->AddRect(node_rect_min, node_rect_max, ImColor(255, 255, 0), 4.0f, 15, 4.0f);

    drawList->AddRectFilled(node_rect_min, node_rect_max, ImColor(bgcol), 4.0f);

    glm::vec2 titleArea = node_rect_max;
    titleArea.y = node_rect_min.y + lineSize;

    // Draw text bg area
    drawList->AddRectFilled(node_rect_min + glm::vec2(1,1), titleArea, ImColor(titlecol), 4.0f);
    drawList->AddRect(node_rect_min, node_rect_max, ImColor(node->borderColor), 4.0f);

    const char *hoverslot = NULL;
    const char *hovertype;
    for(NodeView::InputConMap::const_iterator it = node->inputs.begin(); it != node->inputs.end(); ++it)
    {
        InputConnector *con = it->second;
        glm::vec2 textoffs(2.0f, 0.0f);
        if(_drawConnector(con, drawList, node_rect_min, textoffs))
        {
            hoverslot = it->first.c_str();
            hovertype = "in";
        }
    }

    for(NodeView::OutputConMap::const_iterator it = node->outputs.begin(); it != node->outputs.end(); ++it)
    {
        OutputConnector *con = it->second;
        glm::vec2 txs = ImGui::CalcTextSize(con->desc.name.c_str());
        glm::vec2 textoffs(-(2*NODE_SLOT_DRAW_RADIUS + 4.0f + txs.x), 0.0f);
        if(_drawConnector(con, drawList, node_rect_min, textoffs))
        {
            hoverslot = it->first.c_str();
            hovertype = "out";
        }
    }

    ImGui::SetCursorScreenPos(headerPos);
    ImGui::TextUnformatted(node->name.c_str());

    NodeState ns = hovered && !isUsingAnyConnector ? N_HOVERED : N_NOTHING;

    // don't drag this node when using its widgets or connector
    if(node_moving_active && !node_widgets_active && !isUsingAnyConnector)
    {
        if(ImGui::IsMouseClicked(0))
            ns = N_SELECTED;
        else if(ImGui::IsMouseDragging(0))
            ns = N_DRAGGED;
    }
    if(node_widgets_active)
        ns = N_INUSE;

    //ImGui::PopClipRect();

    if(hovered)
        node_top_hovered = node->_id;

    if(hoverslot)
    {
        CallEnv env(node, "onConnectorHovered");
        if(env)
        {
            node->push(hovertype);
            node->push(hoverslot);
            env.call(0);
        }
    }

    ImGui::PopID();

    drawList->ChannelsMerge();
    return ns;
}

bool NodeEditorView::_drawConnector(ConnectorBase *con, ImDrawList *drawlist, glm::vec2 node_rect_min, glm::vec2 textoffs)
{
    glm::vec2 connpos = node_rect_min + con->pos;
    glm::vec2 textpos = connpos + textoffs;
    textpos.x += NODE_SLOT_DRAW_RADIUS;
    textpos.y -= NODE_SLOT_DRAW_RADIUS;
    ImGui::SetCursorScreenPos(textpos);
    ImGui::TextUnformatted(con->desc.name.c_str());
    const bool textHovered = active && ImGui::IsItemHovered();
    const bool conHovered = active && isConnectorHovered(con, node_rect_min);

    glm::vec4 conColor = conHovered
        ? highlight(con->desc.color)
        : con->desc.color;

    drawlist->AddCircleFilled(connpos, NODE_SLOT_DRAW_RADIUS, ImColor(conColor));
    if(con->isInUse())
    {
        const glm::vec4 ringcol = glm::vec4(1.0f - glm::vec3(conColor), 1.0f);
        drawlist->AddCircle(connpos, NODE_SLOT_DRAW_RADIUS, ImColor(ringcol), 12, 2.0f);
    }

    return textHovered || conHovered;
}

bool NodeEditorView::_canConnect(ConnectorBase * a, ConnectorBase * b) const
{
    if(a && b && a->node != b->node)
    {
        if(a->kind == CK_INPUT && b->kind == CK_OUTPUT)
            return canConnect(*b->node, b->desc, *a->node, a->desc);
        else if(b->kind == CK_INPUT && a->kind == CK_OUTPUT)
            return canConnect(*a->node, a->desc, *b->node, b->desc);
    }
    return false;
}

void NodeEditorView::_removeNodeByID(int id)
{
    if(id < 0)
        return;

    if(NodeView *n = allnodes[id].content())
    {
        _prepareNodeDeletion(n);
        allnodes[id] = NULL;
        sortNodeZ();
    }
    
    if(id == node_hovered_in_scene)
        node_hovered_in_scene = -1;
    if(id == node_active_context_menu)
        node_active_context_menu = -1;
}

template<typename T>
static void s_removeFrom(std::vector<T*>& v, T *c)
{
    for(size_t i = 0; i < v.size(); ++i)
        if(v[i] == c)
        {
            v[i] = v.back();
            v.pop_back();
            break;
        }
}

void NodeEditorView::_prepareNodeDeletion(NodeView *n)
{
    unselectNode(n);
    n->_holder = NULL;
}


void NodeEditorView::_deleteSelection()
{
    // Must break conns + clear vector first; as some may get deleted with their nodes, which would lead to dangling pointers
    {
        std::vector<CountedPtr<Connection> > tmp;
        for(size_t i = 0; i < allnodes.size(); ++i)
            if(NodeView *node = allnodes[i].content())
                for(NodeView::InputConMap::const_iterator it = node->inputs.begin(); it != node->inputs.end(); ++it)
                    if(Connection *c = it->second->connection.content())
                        if(c->isSelected())
                            tmp.push_back(c);
        for(size_t i = 0; i < tmp.size(); ++i)
            tmp[i]->unlink();
    }

    std::vector<NodeView*> tmp; // beingselected is modified during iteration
    tmp.swap(beingselected);
    for(size_t i = 0; i < tmp.size(); ++i)
        tmp[i]->destroy();

    tmp.clear(); // selnodes is modified during iteration
    tmp.swap(selnodes);
    for(size_t i = 0; i < tmp.size(); ++i)
        tmp[i]->destroy();
    tmp.clear();
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const glm::vec4 NodeView::DEFAULT_BODY_COLOR = glm::vec4(0.15f, 0.15f, 0.15f, 1.0f);


void NodeEditorView::_drawConnectionLines(ImDrawList* drawList, glm::vec2 offset)
{
    const int colidx = timeAccu;
    const int hoverid = active ? node_hovered_in_scene : -1;
    const glm::vec2 mousepos = getMousePosInPanel();
    ImU32 col[8];
    const float step = 0.6f / Countof(col);
    for (size_t i = 0; i < allnodes.size(); ++i)
        if(const NodeView* node = allnodes[i].content())
            for(NodeView::InputConMap::const_iterator it = node->inputs.begin(); it != node->inputs.end(); ++it)
            {
                // Connection goes from srcnode ---> node
                InputConnector * const incon = it->second;
                Connection * const con = incon->connection.content();
                if(!con)
                    continue;
                OutputConnector* outcon = con->source;
                NodeView *srcnode = con->getSourceNode();
                const glm::vec4 basecol = con->overrideColor ? con->colorOV : con->color;
                const bool ishover = hoverid == node->_id || hoverid == srcnode->_id;
                float a = 1.0f;
                float thickness = 3;
                if(ishover)
                {
                    a += 0.3f;
                    thickness += 3;
                }
                glm::vec2 from = srcnode->pos + outcon->pos;
                glm::vec2 to = node->pos + incon->pos;

                CurveSegment segments[CONN_LINE_SEGMENTS+1];

                const bool anim = _animateLines && con->active() && con->animated();
                const ImU32 fixcol = ImColor(basecol);

                if(anim)
                {
                    for(int i = 0; i < Countof(col); ++i, a -= step)
                        col[i] = ImColor(basecol * a);
                    generateCurveSegments(segments, to, from, CONN_LINE_SEGMENTS, col, Countof(col), colidx, thickness, timeRemain);
                }
                else
                    generateCurveSegments(segments, to, from, CONN_LINE_SEGMENTS, &fixcol, 1, colidx, thickness, 0);

                bool touch = active && collideCurveSegmentsPoint(segments, Countof(segments), mousepos, thickness + 2, NULL);
                if(!touch && selectionActive)
                {
                    glm::vec2 p1, p2, xfrom, xto;
                    getPanelSelectionBounds(&p1, &p2);
                    getUpperLeftLowerRight(from, to, &xfrom, &xto);
                    touch = isAnyCurveSegmentInBox(xfrom, xto, segments, Countof(segments), p1, p2);
                }
                if(touch)
                {
                    if(!con->selected)
                    {
                        con->selected = 2;
                        beingselconns.push_back(con);
                    }
                }
                else if(con->selected == 2)
                {
                    s_removeFrom(beingselconns, con);
                    con->selected = 0;
                }

                if(con->selected)
                    drawCurveSegmentsColor(drawList, segments, Countof(segments), offset, thickness + 2, ImColor(1.0f, 1.0f, 0.0f, 1.0f));

                if(anim)
                    drawCurveSegments(drawList, segments, Countof(segments), offset, thickness);
                else
                    drawCurveSegmentsColor(drawList, segments, Countof(segments), offset, thickness, fixcol);
            }
}

bool NodeView::IsInFrontOf(const NodeView * a, const NodeView * b)
{
    return a->_zorder > b->_zorder;
}

NodeView::NodeView(NodeEditorView *holder)
: user(NULL)
, titleColor(0.2f, 0.2f, 0.3f, 1.0f)
, bodyColor(DEFAULT_BODY_COLOR)
, borderColor(0.4f, 0.4f, 0.4f, 1.0f)
, _holder(holder)
, _zorder(-1)
, selected(false)
, _id(-1)
{
}

NodeView::~NodeView()
{
    _unlinkAll();
    unbindScript();
}

void NodeView::destroy()
{
    onDestroy();
    _unlinkAll();
    unbindScript();
    if(_holder)
        _holder->removeNode(this);
}

template<typename M>
static typename M::mapped_type lookupOrNull(const M& m, const typename M::key_type& k)
{
    typename M::const_iterator it = m.find(k);
    return it != m.end() ? it->second : NULL;
}


Connection *NodeView::connectToTarget(const char *srcslot, NodeView *target, const char * targetslot, bool force)
{
    OutputConnector *csrc = lookupOrNull(outputs, srcslot);
    InputConnector *ctrg = lookupOrNull(target->inputs, targetslot);
    if(!csrc || !ctrg)
        return NULL;

    // returns existing connection if already linked
    return csrc->link(ctrg);
}

Connection * NodeView::connectToSource(const char *targetslot, NodeView * source, const char *srcslot, bool force)
{
    OutputConnector *csrc = lookupOrNull(source->outputs, srcslot);
    InputConnector *ctrg = lookupOrNull(inputs, targetslot);
    if(!csrc || !ctrg)
        return NULL;

    // returns existing connection if already linked
    return csrc->link(ctrg);
}

void NodeView::setSelected(bool sel)
{
    if(_holder)
    {
        if(sel)
            _holder->selectNode(this);
        else
            _holder->unselectNode(this);
    }
}

Connection * NodeView::getInputConnection(const char *slot)
{
    InputConnector *con = inputs[slot];
    return con ? con->connection.content() : NULL;
}

const std::vector<CountedPtr<Connection> > * NodeView::getOutputConnections(const char *slot)
{
    OutputConnector *oc = outputs[slot];
    return oc ? &oc->connections : NULL;
}

void NodeView::drawUI()
{
    callOpt("drawUI", 0);
}

void NodeView::onLinkToTarget(Connection *con, const char *myslot, const char *otherslot)
{
    assert(this != con->getTargetNode());
    CallEnv env(this, "onLinkToTarget");
    if(env)
    {
        pushobj(con);
        pushobj(con->getTargetNode());
        push(myslot);
        push(otherslot);
        env.call(0);
    }
}

void NodeView::onUnlinkFromTarget(Connection *con, const char *myslot, const char *otherslot)
{
    NodeView *target = con->getTargetNode();
    assert(target);
    assert(this != target);

    // This may happen on shutdown, where objects are deleted in random order.
    // Would lead to an assertion fail, but since we're shutting down,
    // cleaning up properly right here isn't so important as it's done implicitly anyways.
    if(!target->hasScript() || !con->hasScript())
        return;

    CallEnv env(this, "onUnlinkFromTarget");
    if(env)
    {
        pushobj(con);
        pushobj(target);
        push(myslot);
        push(otherslot);
        env.call(0);
    }
}

bool NodeView::overlapsWith(glm::vec2 p1, glm::vec2 p2) // upper left + bottom right of rect
{
    return doBoxesOverlap(pos, pos + size, p1, p2);
}

void NodeView::clearAllSelections()
{
    selected = false;
    for(InputConMap::iterator it = inputs.begin(); it != inputs.end(); ++it)
        if(Connection *con = it->second->connection.content())
            con->selected = 0;
    for(OutputConMap::iterator it = outputs.begin(); it != outputs.end(); ++it)
    {
        OutputConnector *oc = it->second;
        for(size_t j = 0; j < oc->connections.size(); ++j)
            if(Connection *con = oc->connections[j].content())
                con->selected = 0;
    }
}

void NodeView::updateContent()
{
    glm::vec2 titleSize = ImGui::CalcTextSize(name.c_str());

    titleSize.y *= 1.5;

    if(size_t n = inDesc.size())
        _setupConnectors(this, inputs, &inDesc[0], n);

    if(size_t n = outDesc.size())
        _setupConnectors(this, outputs, &outDesc[0], n);

    // Calculate the size needed for the inputs
    glm::vec2 inputTextSize;
    const float textOffsY = titleSize.y + customContentSize.y + 2.0f;
    for(InputConMap::iterator it = inputs.begin(); it != inputs.end(); ++it)
    {
        InputConnector *c = it->second;
        const glm::vec2 textSize = ImGui::CalcTextSize(c->desc.name.c_str());
        c->pos = ImVec2(0.0f, textOffsY + inputTextSize.y + (textSize.y *0.5f));

        inputTextSize.x = glm::max(textSize.x, inputTextSize.x);
        inputTextSize.y += textSize.y;
    }

    inputTextSize.x += 25.0f; // space

    // Calculate for the outputs
    glm::vec2 outputTextSize;
    for(OutputConMap::iterator it = outputs.begin(); it != outputs.end(); ++it)
    {
        OutputConnector *c = it->second;
        const glm::vec2 textSize = ImGui::CalcTextSize(c->desc.name.c_str());
        outputTextSize.x = std::max(outputTextSize.x, textSize.x);
        outputTextSize.y += textSize.y;
    }
     // where to start drawing outputs
    glm::vec2 offs(
        inputTextSize.x + customContentSize.x,
        textOffsY
    );

    size.x = glm::max(glm::max(titleSize.x, 40.0f), glm::max(customContentSize.x, outputTextSize.x + inputTextSize.x));
    size.y = glm::max(inputTextSize.y, outputTextSize.y) + offs.y;
    size += glm::vec2(NODE_SLOT_DRAW_RADIUS, 3.0f);

    // set the positions for the output nodes when we know where the place them

    for(OutputConMap::iterator it = outputs.begin(); it != outputs.end(); ++it)
    {
        OutputConnector *c = it->second;
        const glm::vec2 textSize = ImGui::CalcTextSize(c->desc.name.c_str());

        c->pos = glm::vec2(size.x, offs.y + (textSize.y * 0.5f));
        offs.y += textSize.y;
    }
}

void NodeView::moveToFront()
{
    _zorder = -1;
    _holder->sortNodeZ();
    assert(_zorder == 0);
}

void NodeView::_unlinkAll()
{
    for(InputConMap::iterator it = inputs.begin(); it != inputs.end(); ++it)
        it->second->unlinkAll();
    for(OutputConMap::iterator it = outputs.begin(); it != outputs.end(); ++it)
        it->second->unlinkAll();
}

// -------------------------------------------------------

NodeEditorView::NodeEditorView()
    : active(false)
    , _dragState(DragState_Default)
    , timeAccu(0)
    , timeRemain(0)
    , open_context_menu(false)
    , is_context_menu_open(false)
    , _animateLines(true)
    , selectionActive(false)
    , node_active_context_menu(-1)
    , node_last_selected(-1)
{
}

NodeEditorView::~NodeEditorView()
{
}

NodeView * NodeEditorView::createNode()
{
    return new NodeView(this);
}

void NodeEditorView::_drawBG(ImDrawList * drawList)
{
    const float GRIDSIZE = 200.0f;
    const ImU32 color = ImColor(glm::vec4(0.6f, 0.6f, 0.6f, 0.3f));
    const glm::vec2 region(ImGui::GetContentRegionAvail());
    const glm::vec2 end = _panelPos + region + GRIDSIZE;
    const glm::vec2 shift = glm::abs(glm::mod(_panelPos + scrolling, GRIDSIZE));
    const glm::vec2 start = shift - GRIDSIZE;
    for(unsigned axis = 0; axis < 2; ++axis)
    {
        glm::vec2 i = start;
        for( ; i[axis] < end[axis]; i[axis] += GRIDSIZE)
        {
            glm::vec2 e(end);
            e[axis] = i[axis];
            drawList->AddLine(i, e, color);
        }
    }
}

void NodeEditorView::drawPanel()
{
    //active = ImGui::IsRootWindowOrAnyChildFocused();

    ImGui::PushID(this);

    _panelPos = glm::vec2(ImGui::GetCursorScreenPos());
    const glm::vec2 panelSize = ImGui::GetContentRegionAvail();
    _panelSize = panelSize;
    const glm::vec2 drawPos =_panelPos + scrolling;

    node_hovered_in_list = -1;
    node_hovered_in_scene = -1;
    node_active_used = -1;
    node_top_hovered = -1;

    ImGui::SameLine();
    ImGui::BeginGroup();

    // Create our child canvas
    ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, glm::vec2(1,1));
    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, glm::vec2(0,0));
    ImGui::PushStyleColor(ImGuiCol_ChildWindowBg, ImColor(40,40,40,200));
    ImGui::BeginChild("scrolling_region", glm::vec2(0,0), true, ImGuiWindowFlags_NoScrollbar|ImGuiWindowFlags_NoMove);

    mouseInPanelArea = ImGui::IsWindowHovered() && ImGui::IsMouseHoveringRect(_panelPos, _panelPos + panelSize);

    ImGui::PushItemWidth(120.0f);

    ImDrawList* draw_list = ImGui::GetWindowDrawList();

    _drawBG(draw_list);

    const NodeState ns = _drawNodes(draw_list, drawPos);

    _handleDragging(drawPos);

    if(ns == N_NOTHING || selectionActive)
        _handleSelectionRect();

    _drawConnectionLines(draw_list, drawPos);

    draw_list->ChannelsMerge();

    _handleContextMenu();
    _handleScrolling();

    _drawSelectionRect();

    ImGui::PopItemWidth();
    ImGui::EndChild();
    ImGui::PopStyleColor();
    ImGui::PopStyleVar(2);
    ImGui::EndGroup();

    if(active && ImGui::IsImGuiKeyPressed(ImGuiKey_Delete))
        _deleteSelection();

    ImGui::PopID();

    active = ImGui::IsWindowOrAnyChildFocused();
}

bool NodeEditorView::_addNode(NodeView * node)
{
    // Make sure we're not adding a foreign node
    assert(node->getHolder() == this);
    if(node->getHolder() != this)
        return false;

    for(size_t i = 0; i < allnodes.size(); ++i)
        if(allnodes[i] == node)
            return false;

    size_t index = 0;
    for(size_t i = 0; i < allnodes.size(); ++i)
        if(!allnodes[i])
        {
            index = i + 1;
            break;
        }
    if(index)
    {
        node->_id = unsigned(index - 1);
        allnodes[index - 1] = node;
    }
    else
    {
        node->_id = unsigned(allnodes.size());
        allnodes.push_back(node);
    }
    sortNodeZ();

    node->updateContent();
    return true;
}

void NodeEditorView::removeNode(NodeView * node)
{
    if(!node)
        return;
    assert(allnodes[node->_id] == node);
    _removeNodeByID(node->_id);
}


NodeView * NodeEditorView::_createNodeAt(glm::vec2 pos)
{
    NodeView *node = _createNode();
    if(node)
        node->pos = pos;
    return node;
}

NodeView * NodeEditorView::_createNodeAtCursor()
{
    return _createNodeAt(_savedCursorPos);
}

bool NodeEditorView::canConnect(const NodeView& source, const ConnectorDesc& sourceCon, const NodeView& target, const ConnectorDesc& targetCon) const
{
    NodeEditorView *self = const_cast<NodeEditorView*>(this);
    CallEnv env(self, "canConnect");
    if(env)
    {
        self->pushobj(const_cast<NodeView*>(&source));
        self->push(sourceCon.name.c_str());
        self->pushobj(const_cast<NodeView*>(&target));
        self->push(targetCon.name.c_str());
        bool yes = false;
        return env.call(yes) && yes; // call must not fail and return true
    }
    // Default if no such function
    return &source != &target && sourceCon.type == targetCon.type;
}

NodeView * NodeEditorView::_createNode()
{
    NodeView *node = createNode();
    if(node && !_addNode(node))
    {
        delete node;
        node = NULL;
    }
    return node;
}

NodeEditorView::NodeState NodeEditorView::_drawNodes(ImDrawList *dl, glm::vec2 offset)
{
    ImGui::PushID("nodes");
    NodeView *node = NULL;
    NodeState nsmax = N_NOTHING;
    bool move = true;
    for(size_t i = 0; i < nodeZsort.size(); ++i)
    {
        NodeState ns = _drawNode(dl, offset, nodeZsort[i]);
        if(nsmax <= ns)
        {
            node = nodeZsort[i];
            nsmax = ns;
        }
    }

    if(!active)
    {
        ImGui::PopID();
        return N_NOTHING;
    }

    switch(nsmax)
    {
    case N_NOTHING:
        node = NULL;
        break;
    case N_SELECTED:
    case N_HOVERED:
    case N_DRAGGED:
    case N_INUSE:
        node_hovered_in_scene = node->_id;
        break;
    }

    const bool addsel = selnodes.empty() || isCtrlPressed();

    const bool mbdown = ImGui::IsMouseDown(0);

    if(node && mbdown && (nsmax == N_SELECTED || nsmax == N_INUSE))
    {
        if(nsmax == N_SELECTED)
        {
            node_last_selected = node->_id;

            if(addsel)
            {
                if(node->selected)
                    unselectNode(node);
                else
                    selectNode(node);
            }
            else
            {
                if(!node->selected)
                {
                    clearAllSelections();
                    selectNode(node);
                }
            }
        }

        if(node->_zorder != 0)
            node->moveToFront();
    }

    // Drag selected nodes
    if(node && nsmax >= N_SELECTED && !selectionActive)
    {
        //if(node_top_hovered != -1 && node->_id != node_top_hovered)
        //    ImGui::SetHoveredID(0);

        if(node_active_used != -1 && node_active_used != node->_id)
        {
            ImGui::SetActiveID(0, ImGui::GetCurrentWindow()); // make sure controls of nodes below this one are not accidentally used while overdrawn
            node_active_used = -1;
        }

        move = node->selected && node_active_used == -1;
    }

    if(move && mbdown && node_last_selected != -1 && !ImGui::IsAnyItemActive()) // FIXME: Conditional jump or move depends on uninitialised value(s)
    {
        const glm::vec2 mv = ImGui::GetIO().MouseDelta;
        for(size_t i = 0; i < selnodes.size(); ++i)
            selnodes[i]->pos += mv;
    }

    if(!mbdown)
        node_last_selected = -1;

    ImGui::PopID();
    return nsmax;
}

void NodeEditorView::_drawSelectionRect()
{
    if(!selectionActive)
        return;

    ImDrawList *drawlist = ImGui::GetWindowDrawList();
    glm::vec2 p1 = panelPosToScreenPos(_selectionStart);
    glm::vec2 p2 = getMousePosOnScreen();
    drawlist->AddRectFilled(p1, p2, ImColor(0.7f, 0.0f, 0.0f, 0.3f), 0, 0);
    drawlist->AddRect(p1, p2, ImColor(0.7f, 0.0f, 0.0f, 0.8f), 0, 0);
}

void NodeEditorView::_handleScrolling()
{
    // dragging middle mouse
    if (active && mouseInPanelArea && !ImGui::IsAnyItemActive()
        && (    ImGui::IsMouseDragging(2, 0.0f)
             //|| (node_selected == -1 && _dragState == DragState_Default && ImGui::IsMouseDragging(0, 0.0f))
        )
    ){
        scrolling += glm::vec2(ImGui::GetIO().MouseDelta);
    }
}

void NodeEditorView::_handleSelectionRect()
{
    bool wasSelectionActive = false;
    if(mouseInPanelArea && !ImGui::IsAnyItemActive() && _dragState == DragState_Default
        && (ImGui::IsMouseClicked(0))
    ){
        if(!selectionActive)
        {
            _selectionStart = getMousePosInPanel();

            // Append to current selection when Ctrl is held
            if(!isCtrlPressed())
                clearAllSelections();
        }
        selectionActive = true;
    }
    else if(!ImGui::IsMouseDown(0))
    {
        wasSelectionActive = selectionActive;
        selectionActive = false;
    }

    if(selectionActive)
    {
        glm::vec2 p1, p2;
        getPanelSelectionBounds(&p1, &p2);

        // Unselect nodes that are in the process of being selected but that are no longer touching the selection rect
        for(size_t i = 0; i < beingselected.size(); )
        {
            NodeView *node = beingselected[i];
            assert(node->selected);
            if(node->overlapsWith(p1, p2))
                ++i;
            else
            {
                node->selected = false;
                beingselected[i] = beingselected.back();
                beingselected.pop_back();
            }
        }

        // Add nodes touching the selection rect to nodes currently being selected
        for(size_t i = 0; i < allnodes.size(); ++i)
            if(NodeView *node = allnodes[i].content())
                if(!node->selected && node->overlapsWith(p1, p2))
                {
                    node->selected = true;
                    beingselected.push_back(node);
                }
    }
    else if(wasSelectionActive) // When releasing selection rect, make somewhat selected nodes fully selected
    {
        for(size_t i = 0; i < beingselected.size(); ++i)
        {
            NodeView *node = beingselected[i];
            node->selected = false;
            selectNode(node); // add to actual list of really selected nodes
        }
        beingselected.clear();

        for(size_t i = 0; i < beingselconns.size(); ++i)
        {
            assert(beingselconns[i]->selected == 2);
            beingselconns[i]->selected = 1;
        }
        beingselconns.clear();
    }
}

void NodeEditorView::_callContextMenuFunc(const char *fn)
{
    CallEnv env(this, fn);
    if(env)
    {
        if(node_active_context_menu != -1)
            pushobj(getNodeByID(node_active_context_menu));
        env.call(0);
    }
}

void NodeEditorView::_handleContextMenu()
{
    // Open context menu
    if (mouseInPanelArea && !ImGui::IsAnyItemHovered() && ImGui::IsMouseClicked(1))
    {
        open_context_menu = true;
    }
    if (open_context_menu)
    {
        open_context_menu = false;
        node_active_context_menu = node_hovered_in_scene;
        ImGui::OpenPopup("context_menu");
        _callContextMenuFunc("onOpenContextMenu");
    }

    // Draw context menu
    const bool wasopen = is_context_menu_open;
    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, glm::vec2(8,8));
    if ( (is_context_menu_open = ImGui::BeginPopup("context_menu")) )
    {
        _callContextMenuFunc("onDrawContextMenu");
        ImGui::EndPopup();
    }
    else // Popup not open
    {
        if(wasopen)
            _callContextMenuFunc("onCloseContextMenu");

        node_active_context_menu = -1;
        _savedCursorPos = getMousePosInPanel();
    }

    ImGui::PopStyleVar();
}

void NodeEditorView::updateAnimation(float dt)
{
    //if(active)
    {
        float intpart;
        timeRemain = modff(timeRemain + dt, &intpart);
        timeAccu += (int)intpart;
    }
}

void NodeEditorView::selectNode(NodeView * n)
{
    if(!n->selected)
    {
        n->selected = true;
        selnodes.push_back(n);
    }
}

void NodeEditorView::unselectNode(NodeView * n)
{
    if(n->selected)
    {
        n->selected = false;
        std::vector<NodeView*>::iterator iend = std::remove(selnodes.begin(), selnodes.end(), n);
        selnodes.resize(std::distance(selnodes.begin(), iend));

        iend = std::remove(beingselected.begin(), beingselected.end(), n);
        beingselected.resize(std::distance(beingselected.begin(), iend));
    }
}

void NodeEditorView::_unselectCon(Connection * con)
{
    con->selected = 0;
    s_removeFrom(beingselconns, con);
}

void NodeEditorView::clearAllSelections()
{
    beingselconns.clear();
    beingselected.clear();
    selnodes.clear();
    for(size_t i = 0; i < allnodes.size(); ++i)
        if(NodeView *node = allnodes[i].content())
            node->clearAllSelections();
}

void NodeEditorView::removeAllNodes()
{
    clearAllSelections();
    for(size_t i = 0; i < allnodes.size(); ++i)
        if(NodeView *node = allnodes[i].content())
            node->destroy();
}

glm::vec2 NodeEditorView::getMousePosInPanel()
{
    return screenPosToPanelPos(getMousePosOnScreen());
}

glm::vec2 NodeEditorView::getMousePosOnScreen()
{
    return glm::vec2(ImGui::GetIO().MousePos);
}

glm::vec2 NodeEditorView::panelPosToScreenPos(glm::vec2 p)
{
    return p + scrolling + _panelPos;
}

void NodeEditorView::getPanelSelectionBounds(glm::vec2 * ul, glm::vec2 * br)
{
    getUpperLeftLowerRight(_selectionStart, getMousePosInPanel(), ul, br);
}

glm::vec2 NodeEditorView::screenPosToPanelPos(glm::vec2 p)
{
    return p - scrolling - _panelPos;
}

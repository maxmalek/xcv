#include "luainternal.h"
#include "innerwindow.h"
#include "outerwindow.h"
#include "nodeeditor.h"
#include "memeditor.h"
#include "uiconsole.h"
#include <algorithm>
#include <imgui/imgui.h>
#include "platform.h"


struct LuaInnerWindow : public LuaClassBind<InnerWindow>
{
    static const luaL_Reg methods[];

    luaFunc(destroy) { This(L)->destroy(); return l_delete(L); }
    luaFunc(hide) { This(L)->uivisible = false; luaReturnSelf(); }
    luaFunc(show) { This(L)->uivisible = true; luaReturnSelf(); }
    luaFunc(detach) { This(L)->deleteOnClose = true; luaReturnSelf(); }
    luaFunc(visible) { luaReturnBool(This(L)->uivisible); }
    luaFunc(setVisible) { This(L)->uivisible = getBool(L, 2); luaReturnSelf(); }
    luaFunc(setTitle) { This(L)->setTitle(luaL_checkstring(L, 2)); luaReturnSelf(); }
    luaFunc(setShowMenuBar) { This(L)->setShowMenuBar(getBool(L, 2)); luaReturnSelf(); }
    luaFunc(size) { glm::vec2 sz; This(L)->getSize(&sz.x, &sz.y); luaReturnT(sz); }
    luaFunc(setSize) { glm::vec2 sz = lua_tovec2(L, 2); This(L)->setSize(sz.x, sz.y); luaReturnSelf(); }
    luaFunc(getPos) { glm::vec2 sz; This(L)->getPos(&sz.x, &sz.y); luaReturnT(sz); }
    luaFunc(setPos) { glm::vec2 p = lua_tovec2(L, 2); This(L)->setPos(p.x, p.y); luaReturnSelf(); }

    luaFunc(getParent)
    {
        OuterWindow *o = This(L)->getOuterWindow();
        Scriptable *sc = dynamic_cast<Scriptable*>(o);
        luaReturnObj(sc); // possibly nil
    }
};

const luaL_Reg LuaInnerWindow::methods[] =
{
    luaRegisterMethod(LuaInnerWindow, delete),
    luaRegisterMethod(LuaInnerWindow, destroy),
    luaRegisterMethod(LuaInnerWindow, hide),
    luaRegisterMethod(LuaInnerWindow, show),
    luaRegisterMethod(LuaInnerWindow, detach),
    luaRegisterMethod(LuaInnerWindow, visible),
    luaRegisterMethod(LuaInnerWindow, setVisible),
    luaRegisterMethod(LuaInnerWindow, setTitle),
    luaRegisterMethod(LuaInnerWindow, getParent),
    luaRegisterMethod(LuaInnerWindow, setShowMenuBar),
    luaRegisterMethod(LuaInnerWindow, size),
    { "getSize", LuaInnerWindow::l_size }, // alias for more consistent naming
    luaRegisterMethod(LuaInnerWindow, setSize),
    luaRegisterMethod(LuaInnerWindow, getPos),
    luaRegisterMethod(LuaInnerWindow, setPos)
};

struct LuaNodeView : public LuaClassBind<NodeView>
{
    static const luaL_Reg methods[];

    luaFunc(destroy)
    {
        This(L)->destroy();
        return l_delete(L);
    }

    luaFunc(setName)
    {
        This(L)->name = luaL_checkstring(L, 2);
        luaReturnSelf();
    }

    luaFunc(getName) { luaReturnString(This(L)->name); }
    luaFunc(getInternalID) { luaReturnInt(This(L)->getInternalID()); }

    static void _addDesc(lua_State *L, void *p)
    {
        // ...[key][value]
        std::vector<ConnectorDesc>& v = *(std::vector<ConnectorDesc>*)p;
        ConnectorDesc d;
        d.name = lua_checkrealstring(L, -2);
        // push all the things on the stack
        lua_getfield(L, -1, "detail");
        lua_getfield(L, -2, "color");
        lua_getfield(L, -3, "type");
        // pop off in reverse order
        d.type = lua_checkint(L, -1);
        d.color = (ImVec4)ImColor(lua_optuint(L, -2, 0xffffffff));
        d.detail = luaL_optstring(L, -3, d.name.c_str());
        v.push_back(d);
    }

    static void _pushDesc(lua_State *L, const ConnectorDesc& d)
    {
        LuaStackCheck(L, 1);
        lua_newtable(L);
        if(d.detail.length())
        {
            lua_pushstring(L, d.detail.c_str());
            lua_setfield(L, -2, "detail");
        }
        lua_pushinteger(L, (unsigned)ImColor(d.color));
        lua_setfield(L, -2, "color");
        lua_pushinteger(L, d.type);
        lua_setfield(L, -2, "type");
    }

    static void _setconndesc(lua_State *L, int idx, NodeView *n, std::vector<ConnectorDesc>& v)
    {
        v.clear();
        if(!lua_isnoneornil(L, idx))
            luaForeach(L, _addDesc, idx, &v);
        std::sort(v.begin(), v.end());
    }

    luaFunc(setConnectors)
    {
        NodeView *n = This(L);
        _setconndesc(L, 2, n, n->inDesc);
        _setconndesc(L, 3, n, n->outDesc);
        n->updateContent();
        luaReturnSelf();
    }

    static void _pushAllDesc(lua_State *L, const std::vector<ConnectorDesc>& v)
    {
        LuaStackCheck(L, 1);
        lua_createtable(L, (int)v.size(), 0);
        for(size_t i = 0; i < v.size(); ++i)
        {
            _pushDesc(L, v[i]);
            lua_setfield(L, -2, v[i].name.c_str());
        }
    }

    luaFunc(getConnectors)
    {
        NodeView *n = This(L);
        LuaStackCheck(L, 2);
        _pushAllDesc(L, n->inDesc);
        _pushAllDesc(L, n->outDesc);
        return 2;
    }

    luaFunc(getNumConnectors)
    {
        NodeView *n = This(L);
        lua_pushinteger(L, n->inDesc.size());
        lua_pushinteger(L, n->outDesc.size());
        return 2;
    }

    static void pushConInfoTable(lua_State *L, Connection *con)
    {
        LuaStackCheck(L, 1);
        lua_createtable(L, 0, 6);

        luaPushObj(con->getSourceNode());
        lua_setfield(L, -2, "sourcenode");

        luaPushObj(con->getTargetNode());
        lua_setfield(L, -2, "targetnode");

        lua_pushstring(L, con->getSourceConnName());
        lua_setfield(L, -2, "sourceslot");

        lua_pushstring(L, con->getTargetConnName());
        lua_setfield(L, -2, "targetslot");

        luaPushObj(con);
        lua_setfield(L, -2, "con");

        lua_pushboolean(L, con->active());
        lua_setfield(L, -2, "active");
    }

    luaFunc(getInputSources)
    {
        NodeView *n = This(L);
        const bool all = getBool(L, 2); // consider inactive connections?
        LuaStackCheck(L, 1);
        const NodeView::InputConMap& incons = n->getInputConnectors();
        lua_createtable(L, 0, (int)incons.size());
        for(NodeView::InputConMap::const_iterator it = incons.begin(); it != incons.end(); ++it)
        {
            Connection *con = it->second->connection.content();
            if(!con && all)
                lua_pushboolean(L, false);
            else if(con && (all || con->active()))
                pushConInfoTable(L, con);
            else
                continue;
            lua_setfield(L, -2, it->first.c_str());
        }
        return 1;
    }

    // returns { [node] = <connection to node is active>, [node2] = <active>, ... }
    luaFunc(getInputSourceNodesK)
    {
        NodeView *n = This(L);
        const bool all = getBool(L, 2); // include nodes behind inactive connections?
        LuaStackCheck(L, 1);
        lua_newtable(L);
        unsigned k = 0;
        const NodeView::InputConMap& incons = n->getInputConnectors();
        for(NodeView::InputConMap::const_iterator it = incons.begin(); it != incons.end(); ++it)
            if(Connection *con = it->second->connection.content())
                if(con && (all || con->active()))
                {
                    luaPushObj(con->getSourceNode());
                    lua_pushboolean(L, con->active());
                    lua_rawset(L, -3);
                }
        return 1;
    }

    luaFunc(getOutputTargets)
    {
        NodeView *n = This(L);
        const bool all = getBool(L, 2); // consider inactive connections?
        LuaStackCheck(L, 1);
        lua_newtable(L);
        const NodeView::OutputConMap& outcons = n->getOutputConnectors();
        for(NodeView::OutputConMap::const_iterator it = outcons.begin(); it != outcons.end(); ++it)
        {
            OutputConnector *ocon = it->second;
            assert(ocon);
            const std::vector<CountedPtr<Connection> >& oconns = ocon->connections;
            if(oconns.size())
            {
                lua_createtable(L, (int)oconns.size(), 0);
                unsigned k = 0;
                for(size_t i = 0; i < oconns.size(); ++i)
                {
                    Connection *con = oconns[i].content();
                    if(all || con->active())
                    {
                        pushConInfoTable(L, con);
                        lua_rawseti(L, -2, ++k);
                    }
                }
            }
            else if(all)
                lua_pushboolean(L, false);
            else
                continue;

             lua_setfield(L, -2, it->first.c_str());
        }
        return 1;
    }

    luaFunc(getIncomingConnection)
    {
        luaReturnObj(This(L)->getInputConnection(lua_checkrealstring(L, 2)));
    }
    luaFunc(getOutgoingConnections)
    {
        NodeView *n = This(L);
        const std::vector<CountedPtr<Connection> > *poutcons = n->getOutputConnections(lua_checkrealstring(L, 2));
        if(!poutcons)
            luaReturnNil();

        const std::vector<CountedPtr<Connection> >& outcons = *poutcons;
        lua_createtable(L, (int)outcons.size(), 0);
        unsigned k = 0;
        for(size_t i = 0; i < outcons.size(); ++i)
            if(Connection *con = outcons[i].content())
            {
                luaPushObj(con);
                lua_rawseti(L, -2, ++k); // Lua index
            }
        return 1;
    }

    luaFunc(getAllConnections)
    {
        NodeView *node = This(L);
        {
            const NodeView::InputConMap& incon = node->getInputConnectors();
            lua_createtable(L, (int)incon.size(), 0);
            unsigned k = 0;
            for(NodeView::InputConMap::const_iterator it = incon.begin(); it != incon.end(); ++it)
            {
                const InputConnector *ic = it->second;
                if(Connection *c = ic->connection.content())
                {
                    luaPushObj(c);
                    lua_rawseti(L, -2, ++k); // Lua index
                }
            }
        }
        {
            const NodeView::OutputConMap& outcon = node->getOutputConnectors();
            lua_createtable(L, (int)outcon.size(), 0);
            unsigned k = 0;
            for(NodeView::OutputConMap::const_iterator it = outcon.begin(); it != outcon.end(); ++it)
            {
                const OutputConnector *oc = it->second;
                const OutputConnector::Conns& conns = oc->connections;
                for(size_t j = 0; j < conns.size(); ++j)
                    if(Connection *con = conns[j].content())
                    {
                        luaPushObj(con);
                        lua_rawseti(L, -2, ++k); // Lua index
                    }
            }
        }
        return 2;
    }

    luaFunc(getPos)
    {
        glm::vec2 pos = This(L)->pos;
        luaReturnVec2(pos.x, pos.y);
    }

    luaFunc(setPos)
    {
        This(L)->pos = lua_tovec2(L, 2);
        luaReturnSelf();
    }

    luaFunc(getSize)
    {
        luaReturnT(This(L)->getSize());
    }

    luaFunc(getHolder)
    {
        luaReturnObj(This(L)->getHolder());
    }

    luaFunc(setTitleColor)
    {
        This(L)->titleColor = lua_tovec4(L, 2);
        luaReturnSelf();
    }

    luaFunc(setBodyColor)
    {
        This(L)->bodyColor = lua_tovec4(L, 2);
        luaReturnSelf();
    }

    luaFunc(setBorderColor)
    {
        This(L)->borderColor = lua_tovec4(L, 2);
        luaReturnSelf();
    }

    luaFunc(connectToTarget)
    {
        const char *srcslot = lua_checkrealstring(L, 2);
        NodeView *target = luaGetObj<NodeView>(L, 3);
        const char *targetslot = lua_checkrealstring(L, 4);
        bool force = getBool(L, 5);
        Connection *con = This(L)->connectToTarget(srcslot, target, targetslot, force);
        luaReturnObj(con);
    }

    luaFunc(connectToSource)
    {
        const char *targetslot =lua_checkrealstring(L, 2);
        NodeView *source = luaGetObj<NodeView>(L, 3);
        const char *srcslot = lua_checkrealstring(L, 4);
        bool force = getBool(L, 5);
        Connection *con = This(L)->connectToTarget(targetslot, source, srcslot, force);
        luaReturnObj(con);
    }

    luaFunc(isSelected)
    {
        luaReturnBool(This(L)->isSelected());
    }

    luaFunc(setSelected)
    {
        This(L)->setSelected(getBool(L, 2));
        luaReturnSelf();
    }

};

const luaL_Reg LuaNodeView::methods[] =
{
    // no ctor, inherited dtor
    luaRegisterMethod(LuaNodeView, destroy),
    luaRegisterMethod(LuaNodeView, setName),
    luaRegisterMethod(LuaNodeView, getName),
    luaRegisterMethod(LuaNodeView, setConnectors),
    luaRegisterMethod(LuaNodeView, getConnectors),
    luaRegisterMethod(LuaNodeView, getPos),
    luaRegisterMethod(LuaNodeView, setPos),
    luaRegisterMethod(LuaNodeView, getSize),
    luaRegisterMethod(LuaNodeView, getInternalID),
    luaRegisterMethod(LuaNodeView, getHolder),
    luaRegisterMethod(LuaNodeView, getInputSources),
    luaRegisterMethod(LuaNodeView, getInputSourceNodesK),
    luaRegisterMethod(LuaNodeView, getOutputTargets),
    luaRegisterMethod(LuaNodeView, getIncomingConnection),
    luaRegisterMethod(LuaNodeView, getOutgoingConnections),
    luaRegisterMethod(LuaNodeView, getAllConnections),
    luaRegisterMethod(LuaNodeView, setTitleColor),
    luaRegisterMethod(LuaNodeView, setBodyColor),
    luaRegisterMethod(LuaNodeView, setBorderColor),
    luaRegisterMethod(LuaNodeView, connectToTarget),
    luaRegisterMethod(LuaNodeView, connectToSource),
    luaRegisterMethod(LuaNodeView, isSelected),
    luaRegisterMethod(LuaNodeView, setSelected)
};

struct LuaConnectionView : public LuaClassBind<Connection>
{
    static const luaL_Reg methods[];

    luaFunc(isActive) { luaReturnBool(This(L)->active()); }
    luaFunc(isAnimated) { luaReturnBool(This(L)->animated()); }
    luaFunc(setActive)   { This(L)->setstatus(Connection::ACTIVE,   getBool(L, 1)); luaReturnSelf(); }
    luaFunc(setAnimated) { This(L)->setstatus(Connection::ANIMATED, getBool(L, 1)); luaReturnSelf(); }
    luaFunc(getNodes) { Connection *con = This(L); luaPushObj(con->getSourceNode()); luaPushObj(con->getTargetNode()); return 2; }
    luaFunc(unlink) { This(L)->unlink(); luaReturnNil(); }
    luaFunc(check) { luaReturnBool(This(L)->check()); }
    luaFunc(updateStatus) { luaReturnBool(This(L)->updateStatus()); }
    luaFunc(updateColor) { This(L)->updateColor(); luaReturnSelf(); }
    luaFunc(setColor) { This(L)->color = lua_tovec4(L, 2); luaReturnSelf(); }
    luaFunc(getColor) { luaReturnT(This(L)->color); luaReturnSelf();  }
    luaFunc(setOverrideColor) { Connection *con  = This(L); con->overrideColor = getBool(L, 2); con->colorOV = lua_optvec4(L, 3, glm::vec4(0,0,0,1)); luaReturnSelf();  }
    luaFunc(getOverrideColor) { luaReturnT(This(L)->colorOV); luaReturnSelf();  }

    luaFunc(getConnectorNames)
    {
        Connection *con = This(L);
        lua_pushstring(L, con->getSourceConnName());
        lua_pushstring(L, con->getTargetConnName());
        return 2;
    }
    luaFunc(setSelected)
    {
        This(L)->setSelected(getBool(L, 2));
        luaReturnSelf();
    }
    luaFunc(isSelected)
    {
        luaReturnBool(This(L)->isSelected());
    }
};

const luaL_Reg LuaConnectionView::methods[] =
{
    // no ctor, inherited dtor
    luaRegisterMethod(LuaConnectionView, isActive),
    luaRegisterMethod(LuaConnectionView, isAnimated),
    luaRegisterMethod(LuaConnectionView, setActive),
    luaRegisterMethod(LuaConnectionView, setAnimated),
    luaRegisterMethod(LuaConnectionView, getNodes),
    luaRegisterMethod(LuaConnectionView, unlink),
    luaRegisterMethod(LuaConnectionView, check),
    luaRegisterMethod(LuaConnectionView, updateStatus),
    luaRegisterMethod(LuaConnectionView, updateColor),
    luaRegisterMethod(LuaConnectionView, setColor),
    luaRegisterMethod(LuaConnectionView, getColor),
    luaRegisterMethod(LuaConnectionView, setOverrideColor),
    luaRegisterMethod(LuaConnectionView, getOverrideColor),
    luaRegisterMethod(LuaConnectionView, getConnectorNames),
    luaRegisterMethod(LuaConnectionView, setSelected),
    luaRegisterMethod(LuaConnectionView, isSelected)
};

struct LuaNodeEditorView : public LuaClassBind<NodeEditorView>
{
    static const luaL_Reg methods[];

    luaFunc(new) { luaReturnObj(new NodeEditorView); }
    luaFunc(getMaxNodes) { luaReturnInt(This(L)->getMaxNodes()); }
    luaFunc(getNumNodes) { luaReturnInt(This(L)->getNumNodes()); }
    luaFunc(removeAllNodes) { This(L)->removeAllNodes(); luaReturnSelf(); }

    luaFunc(drawPanel)
    {
        This(L)->drawPanel(); // TODO: w/h
        luaReturnSelf();
    }

    luaFunc(getNode)
    {
        NodeEditorView *self = This(L);
        unsigned idx = lua_checkuint(L, 2) - 1; // Lua index
        NodeView *n = idx < self->getMaxNodes()
            ? self->getNodeByID(idx)
            : NULL;
        luaReturnObj(n);
    }

    luaFunc(getAllNodes)
    {
        LuaStackCheck(L, 1);
        NodeEditorView *self = This(L);
        lua_newtable(L);
        const size_t N = self->getMaxNodes();
        unsigned idx = 0;
        for(size_t i = 0; i < N; ++i)
            if(NodeView *n = self->getNodeByID(unsigned(i)))
            {
                luaPushObj(n);
                lua_rawseti(L, -2, ++idx); // Lua index
            }
        return 1;
    }

    luaFunc(getSelectedNodes)
    {
        const std::vector<NodeView*>& v = This(L)->getSelectedNodes();
        lua_createtable(L, (int)v.size(), 0);
        for(size_t i = 0; i < v.size(); ++i)
        {
            assert(v[i]->isSelected());
            luaPushObj(v[i]);
            lua_rawseti(L, -2, i+1); // Lua index
        }
        return 1;
    }
    luaFunc(clearSelection)
    {
        This(L)->clearAllSelections();
        luaReturnNil();
    }

    luaFunc(updateAnimation)
    {
        This(L)->updateAnimation(lua_tofloat(L, 2));
        luaReturnSelf();
    }
    luaFunc(getLocalCursorPos)
    {
        luaReturnT(This(L)->getLocalCursorPos());
    }
    luaFunc(getScrollPos) { luaReturnT(This(L)->getScrollPos()); }
    luaFunc(setScrollPos) { This(L)->setScrollPos(lua_tovec2(L, 2)); luaReturnSelf(); }
    luaFunc(getPanelSize) { luaReturnT(This(L)->getPanelSize()); }
};

const luaL_Reg LuaNodeEditorView::methods[] =
{
    // no ctor
    luaRegisterMethod(LuaNodeEditorView, delete),
    luaRegisterMethod(LuaNodeEditorView, removeAllNodes),
    luaRegisterMethod(LuaNodeEditorView, getMaxNodes),
    luaRegisterMethod(LuaNodeEditorView, getNumNodes),
    luaRegisterMethod(LuaNodeEditorView, getNode),
    luaRegisterMethod(LuaNodeEditorView, getAllNodes),
    luaRegisterMethod(LuaNodeEditorView, getSelectedNodes),
    luaRegisterMethod(LuaNodeEditorView, drawPanel),
    luaRegisterMethod(LuaNodeEditorView, updateAnimation),
    luaRegisterMethod(LuaNodeEditorView, getLocalCursorPos),
    luaRegisterMethod(LuaNodeEditorView, getScrollPos),
    luaRegisterMethod(LuaNodeEditorView, setScrollPos),
    luaRegisterMethod(LuaNodeEditorView, getPanelSize),
    luaRegisterMethod(LuaNodeEditorView, clearSelection)
};



struct LuaMemoryEditor : public LuaClassBind<MemoryEditor>
{
    static const luaL_Reg methods[];

    luaFunc(new)
    {
        luaReturnObj(new MemoryEditor);
    }

    luaFunc(unsafeptr)
    {
        MemoryEditor *me = This(L);
        if(!lua_isnoneornil(L, 2))
        {
            luaL_checktype(L, 2, LUA_TLIGHTUSERDATA);
            me->mem_data = (unsigned char*)lua_touserdata(L, 2);
        }
        if(!lua_isnoneornil(L, 4))
        {
            luaL_checktype(L, 4, LUA_TLIGHTUSERDATA);
            me->mem_write = (unsigned char*)lua_touserdata(L, 4);
        }
        if(me->mem_data)
        {
            size_t sz = luaL_checkinteger(L, 3); // do this first; so we don't change the pointer if this errors out
            me->mem_size = sz;
        }
        else
            me->mem_size = 0;
        luaReturnSelf();
    }

    luaFunc(drawUI) { luaReturnBool(This(L)->drawUI()); }
    luaFunc(drawControls) { This(L)->drawControls(); luaReturnSelf(); }
    luaFunc(drawMenu) { This(L)->drawMenu(); luaReturnSelf(); }
    luaFunc(setType) { This(L)->setTypeStr(lua_tostring(L, 2)); luaReturnSelf(); }
    luaFunc(setColumns) { This(L)->Columns = lua_touint(L, 2); luaReturnSelf(); }
    luaFunc(setHex)  { This(L)->AsHex = getBool(L, 2); luaReturnSelf(); }
    luaFunc(setAllowEdits) { This(L)->AllowEdits = getBool(L, 2); luaReturnSelf(); }
    luaFunc(setEndian) { This(L)->Endian = lua_touint(L, 2); luaReturnSelf(); }
};

const luaL_Reg LuaMemoryEditor::methods[] =
{
    luaRegisterMethod(LuaMemoryEditor, new),
    luaRegisterMethod(LuaMemoryEditor, unsafeptr),
    luaRegisterMethod(LuaMemoryEditor, drawUI),
    luaRegisterMethod(LuaMemoryEditor, drawMenu),
    luaRegisterMethod(LuaMemoryEditor, drawControls),
    luaRegisterMethod(LuaMemoryEditor, setType),
    luaRegisterMethod(LuaMemoryEditor, setColumns),
    luaRegisterMethod(LuaMemoryEditor, setHex),
    luaRegisterMethod(LuaMemoryEditor, setAllowEdits),
    luaRegisterMethod(LuaMemoryEditor, setEndian),
};

struct LuaUIConsole : public LuaClassBind<UIConsole>
{
    static const luaL_Reg methods[];

    luaFunc(setFilter) { This(L)->setFilter(lua_tostring(L, 2)); luaReturnSelf(); } // can be NULL
    luaFunc(getFilter) { luaReturnStr(This(L)->getFilter()); }
    luaFunc(setLogLevel) { This(L)->setLogLevel(lua_checkint(L, 2)); luaReturnSelf(); }
    luaFunc(getLogLevel) { luaReturnInt(This(L)->getLogLevel()); }
    luaFunc(setAutoScroll) { This(L)->setAutoScroll(getBool(L, 2)); luaReturnSelf(); }
    luaFunc(getAutoScroll) { luaReturnBool(This(L)->getAutoScroll()); }
    luaFunc(useGlobalLog) { This(L)->useGlobalLog(getBool(L, 2), getBool(L, 3)); luaReturnSelf(); }
    luaFunc(scrollToBottom) { This(L)->scrollToBottom(); luaReturnSelf(); }
    luaFunc(clear) { This(L)->clear(); luaReturnSelf(); }

    static int _doprint(lua_State *L, bool nl)
    {
        const char *s = luaL_checkstring(L, 2);
        const int level = lua_toint(L, 3);
        const glm::vec4 col = lua_optvec4(L, 4, glm::vec4(1));
        This(L)->add(s, level, col, false);
        luaReturnSelf();
    }

    luaFunc(add) { return _doprint(L, false); }
    luaFunc(addln) { return _doprint(L, true); }
};

const luaL_Reg LuaUIConsole::methods[] =
{
    // no ctor, delete inherited
    luaRegisterMethod(LuaUIConsole, setFilter),
    luaRegisterMethod(LuaUIConsole, getFilter),
    luaRegisterMethod(LuaUIConsole, setLogLevel),
    luaRegisterMethod(LuaUIConsole, getLogLevel),
    luaRegisterMethod(LuaUIConsole, setAutoScroll),
    luaRegisterMethod(LuaUIConsole, getAutoScroll),
    luaRegisterMethod(LuaUIConsole, useGlobalLog),
    luaRegisterMethod(LuaUIConsole, scrollToBottom),
    luaRegisterMethod(LuaUIConsole, add),
    luaRegisterMethod(LuaUIConsole, addln),
    luaRegisterMethod(LuaUIConsole, clear),
};

luaFunc(setClipboardText)
{
    luaReturnBool(platform::setClipboardText(lua_tostring(L, 1))); // can be NULL
}

luaFunc(getClipboardText)
{
    char *clip = platform::getClipboardText();
    lua_pushstringornil(L, clip);
    free(clip);
    return 1;
}

static const luaL_Reg guilib[] =
{
    luaRegister(setClipboardText),
    luaRegister(getClipboardText),
    {NULL, NULL}
};

int register_lua_gui(lua_State *L)
{
    LuaStackCheck(L, 1);

    luaL_newlib(L, guilib);
    luaRegisterClassInLib<LuaInnerWindow>(L, -1);
    luaRegisterClassInLib<LuaNodeEditorView>(L, -1);
    luaRegisterClassInLib<LuaNodeView>(L, -1);
    luaRegisterClassInLib<LuaConnectionView>(L, -1);
    luaRegisterClassInLib<LuaMemoryEditor>(L, -1);
    luaRegisterClassInLib<LuaUIConsole>(L, -1);

    //luaRegisterEnumInLib(L, guilibenum, -1);
    return 1;
}

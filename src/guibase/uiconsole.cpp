#include "uiconsole.h"
#include "guibase.h"
#include <stdio.h>
#include "util.h"
#include "macros.h"
#include "log.h"

#include <vector>
#include <algorithm>

const ScriptDef UIConsole::scriptdef("uiconsole", ScriptTypes::UICONSOLE, &InnerWindow::scriptdef);

static std::vector<UIConsole*> s_cons;
UIConsole::LineList UIConsole::s_globalLog;

UIConsole::UIConsole(OuterWindow *w, const char *title, const char *ident, const unsigned maxlines)
: InnerWindow(w, title, ident)
, _maxlines(255)
, _loglevel(0)
, _scrollToBottom(false)
, _autoscroll(true)
{
    clear();

    s_cons.push_back(this);
}

UIConsole::~UIConsole()
{
    std::vector<UIConsole*>::iterator it = std::remove(s_cons.begin(), s_cons.end(), this);
    s_cons.resize(std::distance(s_cons.begin(), it));
}

void UIConsole::_trim()
{
    while(_lines.size() > _maxlines)
        _lines.pop_front();
}

void UIConsole::clear()
{
    _lines.clear();
    newline();
}

void UIConsole::newline()
{
    _lines.push_back(Line("", 0, glm::vec4(1,1,1,1)));
    _trim();
}

void UIConsole::setFilter(const char * wildcard)
{
    _filter.clear();
    _useFilter.clear();
    if(wildcard)
    {
        _filter = wildcard;
        _useFilter += '*';
        _useFilter += wildcard;
        _useFilter += '*';
    }
}

void UIConsole::add(const char *s, int lvl, const glm::vec4& c, bool nl)
{
    _lines.back().str += s;
    _lines.back().color = c;
    _lines.back().level = lvl;
    if(nl)
        newline();
    if(_autoscroll)
        _scrollToBottom = true;
}

void UIConsole::add(const std::string& s, int lvl, const glm::vec4& c, bool nl)
{
    add(s.c_str(), lvl, c, nl);
}

void UIConsole::add(va_list va, const char *fmt, int lvl, const glm::vec4& c, bool nl)
{
    char buf[1024];
    vsnprintf(buf, Countof(buf), fmt, va);
    buf[Countof(buf)-1] = 0;
    add(&buf[0], lvl, c, nl);
}

void UIConsole::fmt(const char *fmt, ...)
{
    va_list va;
    va_start(va, fmt);
    add(va, fmt, LL::NORMAL);
    va_end(va);
}

void UIConsole::fmtError(const char *fmt, ...)
{
    va_list va;
    va_start(va, fmt);
    add(va, fmt, LL::ERR, glm::vec4(1,0,0,1));
    va_end(va);
}

bool UIConsole::_showline(const Line& line, int loglevel, const char *wildcard)
{
    return line.level <= loglevel && (!wildcard || wildcardMatchNoCase(line.str.c_str(), wildcard));
}

void UIConsole::drawContent()
{
    ImGui::BeginChild("ScrollingRegion", ImVec2(0,-ImGui::GetItemsLineHeightWithSpacing()), false, ImGuiWindowFlags_HorizontalScrollbar);
    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(4,1)); // Tighten spacing
    const char *flt = _useFilter.empty() ? NULL : _useFilter.c_str();
    const int lvl = _loglevel;
    for(LineList::const_iterator it = _lines.begin(); it != _lines.end(); ++it)
    {
        if(_showline(*it, lvl, flt))
        {
            ImGui::PushStyleColor(ImGuiCol_Text, it->color);
            ImGui::TextUnformatted(it->str.c_str());
            ImGui::PopStyleColor();
        }
    }

    ImGui::PopStyleVar();

    if (_scrollToBottom)
    {
        ImGui::SetScrollHere();
        _scrollToBottom = false;
    }
    ImGui::EndChild();

    InnerWindow::drawContent();
}


// ------------------------------

static const glm::vec4 colors[] =
{
    glm::vec4(1,0.2f,0.0f,1),       // ERR_MSGBOX
    glm::vec4(1,0.3f,0.1f,1),       // ERR
    glm::vec4(1, 1, 1, 1),          // NORMAL
    glm::vec4(0.5f, 1, 1, 1),       // INFO
    glm::vec4(0.5f, 0.5f, 1, 1),    // DEBUG
    glm::vec4(0.8f, 0.4f, 0.8f, 1), // DEBUG2
};

void UIConsole::useGlobalLog(bool on, bool init)
{
    _useGlobalLog = on;
    if(init)
    {
        if(_lines.size() && _lines.back().str == "")
            _lines.pop_back();
        const char *flt = _useFilter.empty() ? NULL : _useFilter.c_str();
        const int lvl = _loglevel;
        for(LineList::const_iterator it = s_globalLog.begin(); it != s_globalLog.end(); ++it)
        {
            if(_showline(*it, lvl, flt))
                _lines.push_back(*it);
        }
        _lines.push_back(Line("", 0, glm::vec4()));
    }
}

void UIConsole::_appendLine(UIConsole *con, LineList& L, int level, const char *message, size_t len)
{
    if(!L.size())
        L.push_back(Line("", 0, glm::vec4()));
    Line& last = L.back();
    last.str += message;
    last.level = level;
    last.color = colors[level - LL::_MIN];

    if(message[len-1] == '\n')
    {
        L.push_back(Line("", 0, glm::vec4()));
        if(con && con->_autoscroll)
            con->_scrollToBottom = true;
    }
    while(L.size() > 1024)
        L.pop_front();
}

void UIConsole::Log_callback(int level, const char *message, size_t len, void*)
{
    compile_assert(Countof(colors) == LL::_TOTAL);
    if(level < LL::_MIN || level > LL::_MAX)
        level = LL::ERR;

    _appendLine(NULL, s_globalLog, level, message, len);

    for(size_t i = 0; i < s_cons.size(); ++i)
    {
        UIConsole *con = s_cons[i];
        if(con->_useGlobalLog && level <= con->_loglevel)
        {
            LineList& L = con->_lines;
            _appendLine(con, L, level, message, len);
        }
    }
}

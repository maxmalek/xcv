#pragma once

#include <list>
#include <string>
#include <glmx.h>
#include <stdarg.h>
#include "innerwindow.h"

class UIConsole : public InnerWindow, private ScriptType<UIConsole>
{
public:
    static const ScriptDef scriptdef;

    UIConsole(OuterWindow *w, const char *title, const char *ident, unsigned maxlines = 255);
    virtual ~UIConsole();

    virtual void drawContent();
    void add(const char *s, int lvl, const glm::vec4& c = glm::vec4(1,1,1,1), bool nl = true);
    void add(const std::string& s, int lvl, const glm::vec4& c = glm::vec4(1,1,1,1), bool nl = true);
    void add(va_list va, const char *fmt, int lvl, const glm::vec4& c = glm::vec4(1,1,1,1), bool nl = true);
    void fmt(const char *s, ...);
    void fmtError(const char *s, ...);
    void clear();
    void newline();

    void setMaxLines(unsigned n) { _maxlines = n; _trim(); }
    unsigned getMaxLines() const { return _maxlines; }
    void setLogLevel(int lvl) { _loglevel = lvl; }
    int getLogLevel() const { return _loglevel; }
    void setFilter(const char *wildcard); // NULL is ok
    const char *getFilter() const { return _filter.c_str(); }
    void setAutoScroll(bool on) { _autoscroll = on; }
    bool getAutoScroll() const { return _autoscroll; }
    void useGlobalLog(bool on, bool init);
    void scrollToBottom() { _scrollToBottom = true; }

    static void Log_callback(int level, const char *message, size_t len, void*);

protected:
    void _trim();
    struct Line
    {
        Line(const char * s, int lvl, const glm::vec4& c) : level(lvl), str(s), color(c) {}
        int level;
        std::string str;
        glm::vec4 color;
    };
    unsigned _maxlines;
    int _loglevel;
    std::string _filter, _useFilter;
    bool _useGlobalLog;
    typedef std::list<Line> LineList;
    LineList _lines;
    bool _scrollToBottom;
    bool _autoscroll;

    static LineList s_globalLog;
    static void _appendLine(UIConsole *con, LineList& L, int level, const char *message, size_t len);
    static bool _showline(const Line& line, int loglevel, const char *wildcard);
};

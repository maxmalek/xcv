#pragma once

#include <string>
#include "luaobj.h"

class WinCommon;
class OuterWindow;

class InnerWindow : public ScriptableT<InnerWindow>
{
public:
    static const ScriptDef scriptdef;

    InnerWindow(OuterWindow *w, const char *name, const char *ident = 0);

    virtual void drawUI();
    virtual void drawContent();
    virtual void onUpdate(float dt);
    virtual void onResize(float w, float h);

    void destroy(); // close and delete
    void removeFromOuter();

    void setShowMenuBar(bool on);

    bool uivisible;
    bool deleteOnClose;
    bool inWindowList;

    int imguiFlags;

    void setTitle(const char *title);
    const std::string& getTitle() const { return _wintitle; }
    OuterWindow *getOuterWindow() { return _outerwin; }
    void getSize(float *w, float *h);
    void getPos(float *w, float *h);
    void setSize(float w, float h);
    void setPos(float x, float y);
    void setInitialPos(float x, float y);
    void setInitialSize(float w, float h);
    void setDockSplitRatio(float ratio);
    void setDockSlot(unsigned slot); // ImGuiDockSlot

protected:
    enum TodoFlags
    {
        TDF_NONE = 0,
        TDF_SETPOS = 0x01,
        TDF_SETPOS_INIT = 0x02,
        TDF_SETSIZE = 0x04,
        TDF_SETSIZE_INIT = 0x08,
        TDF_SETDOCK_FLOATSIZE = 0x10,
        TDF_SETDOCK_SPLIT = 0x20,
        TDF_SETDOCK_SLOT = 0x40,
        TDF_SETDOCK_FLOATPOS = 0x80
    };

    virtual ~InnerWindow();

    float posX, posY; // posiiton
    float winW, winH; // size
    float dockSplitRatio;
    unsigned dockSlot;
    OuterWindow *_outerwin;
    std::string _wintitle, _usetitle;
    const std::string _winident;
    unsigned todo;

    virtual void onWinClose();

private:
    bool shown;
    bool _drawing;
    void _updateSize(float w, float h);
    void _destroy();
    InnerWindow(const InnerWindow&); // non-copyable
    void _updateTitle();
};


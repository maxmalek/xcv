// Modified version; Original comment below:

// Mini memory editor for ImGui (to embed in your game/tools)
// v0.10
//
// You can adjust the keyboard repeat delay/rate in ImGuiIO.
// The code assume a mono-space font for simplicity! If you don't use the default font,
// use ImGui::PushFont()/PopFont() to switch to a mono-space font before caling this.
//
// Usage:
//   static MemoryEditor memory_editor;                                                     // save your state somewhere
//   memory_editor.Draw("Memory Editor", mem_block, mem_block_size, (size_t)mem_block);     // run
//
// TODO: better resizing policy (ImGui doesn't have flexible window resizing constraints yet)

#include "memeditor.h"
#include "imgui/imgui.h"
#include "pstdint.h"
#include <stdio.h>
#include "blob3ddef.h"
#include <assert.h>
#include "myendian.h"
#include "util.h"
#include "macros.h"
#include "fp16.h"

const ScriptDef MemoryEditor::scriptdef("memoryeditor", ScriptTypes::MEMORYEDITOR, &InnerWindow::scriptdef);

#define PRINT_BUFFER_SIZE 32

enum
{
    DATATYPE_ASCII = B3D_LAST_VALID_TYPE + 1,

    LAST_DATATYPE = DATATYPE_ASCII,
    MAX_DATATYPES = LAST_DATATYPE + 1 // one past end
};

typedef int (*PrintDispatch) (char *buf, const char *fmt, bool swap, const void *mem);

template<typename T>
static int snprintfT(char *buf, const char *fmt, bool swap, const void *mem)
{
    T x = *(T*)mem;
    if(swap)
        x = endian::swap<T>(x);
    return snprintf(buf, PRINT_BUFFER_SIZE, fmt, x);
}

static const PrintDispatch B3DPrintDispatch[] =
{
    &snprintfT<uint8_t>,
    &snprintfT<uint16_t>,
    &snprintfT<uint32_t>,
    &snprintfT<int8_t>,
    &snprintfT<int16_t>,
    &snprintfT<int32_t>,
    &snprintfT<float>,
    NULL, // half-float
    &snprintfT<int64_t>,
    &snprintfT<uint64_t>,
    &snprintfT<double>,
    NULL, // ascii text
};

static const char *B3DPrintFormatDec[] =
{
    "%" PRINTF_UINT8_DEC_WIDTH                        "u ", // B3D_U8
    "%" PRINTF_UINT16_DEC_WIDTH PRINTF_INT16_MODIFIER "u ", // B3D_U16
    "%" PRINTF_UINT32_DEC_WIDTH PRINTF_INT32_MODIFIER "u ", // B3D_U32
    "% 4"                        "d ", // B3D_S8
    "% 6" PRINTF_INT16_MODIFIER "d ", // B3D_S16
    "% 11" PRINTF_INT32_MODIFIER "d ", // B3D_S32
    "% 14.8g ",                                              // B3D_FLOAT
    NULL,                                                   // B3D_HALF // special case
    "% 20" PRINTF_INT64_MODIFIER "d ", // B3D_S64
    "%" PRINTF_UINT64_DEC_WIDTH PRINTF_INT64_MODIFIER "u ", // B3D_U64
    "% 16.8g ",                                             // B3D_DOUBLE

    NULL, // ascii text
};

static const char *B3DPrintFormatHex[] =
{
    "%0" PRINTF_UINT8_HEX_WIDTH                        "X ", // B3D_U8
    "%0" PRINTF_UINT16_HEX_WIDTH PRINTF_INT16_MODIFIER "X ", // B3D_U16
    "%0" PRINTF_UINT32_HEX_WIDTH PRINTF_INT32_MODIFIER "X ", // B3D_U32
    "%0" PRINTF_INT8_HEX_WIDTH                        "X ", // B3D_S8
    "%0" PRINTF_INT16_HEX_WIDTH PRINTF_INT16_MODIFIER "X ", // B3D_S16
    "%0" PRINTF_INT32_HEX_WIDTH PRINTF_INT32_MODIFIER "X ", // B3D_S32
    "% 14.8g ",                                              // B3D_FLOAT
    NULL,                                                   // B3D_HALF // special case
    "%0" PRINTF_INT64_HEX_WIDTH PRINTF_INT64_MODIFIER "X ", // B3D_S64
    "%0" PRINTF_UINT64_HEX_WIDTH PRINTF_INT64_MODIFIER "X ", // B3D_U64
    "% 16.8g",                                               // B3D_DOUBLE

    NULL // ascii text
};

static Blob3DType toB3DType(int typeidx)
{
    if(typeidx == DATATYPE_ASCII)
        return B3D_UNSPECIFIED;

    const Blob3DType ty = (Blob3DType)(typeidx);
    assert(ty >= B3D_FIRST_VALID_TYPE && ty <= B3D_LAST_VALID_TYPE);
    return ty;
}

static const char *getFormat(Blob3DType ty, bool hex)
{
    compile_assert(Countof(B3DPrintFormatHex) == MAX_DATATYPES);
    compile_assert(Countof(B3DPrintFormatDec) == MAX_DATATYPES);

    assert(ty != B3D_HALF);
    assert(ty != B3D_UNSPECIFIED);

    const char **fmt = hex ? B3DPrintFormatHex : B3DPrintFormatDec;
    return fmt[ty];
}

static int printmem(char *buf, Blob3DType ty, bool hex, bool swap, const void *mem)
{
    compile_assert(Countof(B3DPrintDispatch) == MAX_DATATYPES);

    switch(ty)
    {
        case B3D_UNSPECIFIED: // Normal ASCII text
        {
            const unsigned char c = *(const unsigned char *)mem;
            return sprintf(buf, "%c", (c >= 32 && c < 128) ? c : '.');
        }

        case B3D_HALF:
        {
            fp16::FP16 half = *(const fp16::FP16*)mem;
            const float flt = fp16::half_to_float(half).f;
            const char *fmt = getFormat(B3D_FLOAT, hex);
            return snprintfT<float>(buf, fmt, swap, &flt);
        }

        default:
            ;
    }

    const char *fmt = getFormat(ty, hex);
    return B3DPrintDispatch[ty](buf, fmt, swap, mem);
}

static const char *datatypeName(int which)
{
    switch(which)
    {
        case DATATYPE_ASCII:
            return "ASCII Text";
        default:
            return Blob3DNameCommon[toB3DType(which)];
    }
    assert(false);
    return "<error>";
}

static bool  _datatypeComboCallback(void* self_, int which, const char** p)
{
    //MemoryEditor *self = (MemoryEditor*)self_;
    *p = datatypeName(which);
    return true;
}

MemoryEditor::MemoryEditor()
{
    typeidx = DATATYPE_ASCII;
    AsHex = false;
    Endian = 0; // 0 : native, 1: little, 2: big
    Columns = 16;
    DataEditingAddr = -1;
    DataEditingTakeFocus = false;
    DataInput[0] = 0;
    AddrInput[0] = 0;
    AllowEdits = true;
    mem_data = 0;
    mem_write = 0;
    mem_size = 0;
    base_display_addr = 0;
}

bool MemoryEditor::drawUI()
{
    if(!mem_data)
        return false;

    const Blob3DType datatype = toB3DType(typeidx);
    const unsigned elemSize = datatype == B3D_UNSPECIFIED ? 1 : Blob3DSize[datatype];
    const bool swapEndian = Endian && (2 - endian::isLittle() != Endian);

    char printbuf[PRINT_BUFFER_SIZE];

    ImGui::BeginChild("##scrolling", ImVec2(0, -ImGui::GetItemsLineHeightWithSpacing()));

    ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0,0));
    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0,0));

    int addr_digits_count = 0;
    for (size_t n = base_display_addr + mem_size - 1; n > 0; n >>= 4)
        addr_digits_count++;

    float glyph_width = ImGui::CalcTextSize("F").x;
    float cell_width = glyph_width * 3; // "FF " we include trailing space in the width to easily catch clicks everywhere

    int effectiveColumns = Columns - (Columns % elemSize);
    if(effectiveColumns < (int)elemSize)
        effectiveColumns = elemSize;

    const int effectiveElems = effectiveColumns / elemSize;

    float line_height = ImGui::GetTextLineHeight();
    int bytes_per_line = (int)((mem_size + effectiveColumns-1) / effectiveColumns);
    int line_total_count = bytes_per_line + (elemSize - 1) / elemSize;
    ImGuiListClipper clipper(line_total_count, line_height);
    int visible_start_addr = clipper.DisplayStart * effectiveColumns;
    int visible_end_addr = clipper.DisplayEnd * effectiveColumns;

    bool data_next = false;

    if (!AllowEdits || DataEditingAddr >= mem_size)
        DataEditingAddr = -1;

    int data_editing_addr_backup = DataEditingAddr;
    if (DataEditingAddr != -1)
    {
        if (ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_UpArrow)) && DataEditingAddr >= effectiveColumns)                   { DataEditingAddr -= effectiveColumns; DataEditingTakeFocus = true; }
        else if (ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_DownArrow)) && DataEditingAddr < mem_size - effectiveColumns)  { DataEditingAddr += effectiveColumns; DataEditingTakeFocus = true; }
        else if (ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_LeftArrow)) && DataEditingAddr > 0)                { DataEditingAddr -= 1; DataEditingTakeFocus = true; }
        else if (ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_RightArrow)) && DataEditingAddr < mem_size - 1)    { DataEditingAddr += 1; DataEditingTakeFocus = true; }
    }
    if ((DataEditingAddr / effectiveColumns) != (data_editing_addr_backup / effectiveColumns))
    {
        // Track cursor movements
        float scroll_offset = ((DataEditingAddr / effectiveColumns) - (data_editing_addr_backup / effectiveColumns)) * line_height;
        bool scroll_desired = (scroll_offset < 0.0f && DataEditingAddr < visible_start_addr + effectiveColumns*2) || (scroll_offset > 0.0f && DataEditingAddr > visible_end_addr - effectiveColumns*2);
        if (scroll_desired)
            ImGui::SetScrollY(ImGui::GetScrollY() + scroll_offset);
    }

    bool draw_separator = true;
    for (int line_i = clipper.DisplayStart; line_i < clipper.DisplayEnd; line_i++) // display only visible items
    {
        int addr = line_i * effectiveColumns;
        ImGui::Text("%0*" PRINTF_INT64_MODIFIER "X: ", addr_digits_count, base_display_addr+addr);
        ImGui::SameLine();

        // Draw Hexadecimal
        float line_start_x = ImGui::GetCursorPosX();
        float incrspacing = 0;

        for (int n = 0; n < effectiveColumns && addr < mem_size; ++n, ++addr)
        {
            if(elemSize > 1 && n && n % elemSize == 0)
                incrspacing += glyph_width;

            ImGui::SameLine(line_start_x + cell_width * n + incrspacing);

            if (DataEditingAddr == addr)
            {
                // Display text input on current byte
                ImGui::PushID(addr);
                struct FuncHolder
                {
                    // FIXME: We should have a way to retrieve the text edit cursor position more easily in the API, this is rather tedious.
                    static int Callback(ImGuiTextEditCallbackData* data)
                    {
                        int* p_cursor_pos = (int*)data->UserData;
                        if (!data->HasSelection())
                            *p_cursor_pos = data->CursorPos;
                        return 0;
                    }
                };
                int cursor_pos = -1;
                bool data_write = false;
                if (DataEditingTakeFocus)
                {
                    ImGui::SetKeyboardFocusHere();
                    sprintf(AddrInput, "%0*X", addr_digits_count, (unsigned)(base_display_addr+addr));
                    sprintf(DataInput, "%02X", mem_data[addr]);
                }
                ImGui::PushItemWidth(ImGui::CalcTextSize("FF").x);
                ImGuiInputTextFlags flags = ImGuiInputTextFlags_CharsHexadecimal|ImGuiInputTextFlags_EnterReturnsTrue|ImGuiInputTextFlags_AutoSelectAll|ImGuiInputTextFlags_NoHorizontalScroll|ImGuiInputTextFlags_AlwaysInsertMode|ImGuiInputTextFlags_CallbackAlways;
                if (ImGui::InputText("##data", DataInput, 32, flags, FuncHolder::Callback, &cursor_pos))
                    data_write = data_next = true;
                else if (!DataEditingTakeFocus && !ImGui::IsItemActive())
                    DataEditingAddr = -1;
                DataEditingTakeFocus = false;
                ImGui::PopItemWidth();
                if (cursor_pos >= 2)
                    data_write = data_next = true;
                if (data_write && mem_write)
                {
                    int data;
                    if (sscanf(DataInput, "%X", &data) == 1)
                        mem_write[addr] = (unsigned char)data;
                }
                ImGui::PopID();
            }
            else
            {
                ImGui::Text("%02X ", mem_data[addr]);
                if (AllowEdits && ImGui::IsItemHovered() && ImGui::IsMouseClicked(0))
                {
                    DataEditingTakeFocus = true;
                    DataEditingAddr = addr;
                }
            }
        }

        ImGui::SameLine(line_start_x + cell_width * effectiveColumns + glyph_width * 2 + (elemSize > 1 ? ((effectiveElems-1) * glyph_width) : 0.0f));

        if (draw_separator)
        {
            ImVec2 screen_pos = ImGui::GetCursorScreenPos();
            ImGui::GetWindowDrawList()->AddLine(ImVec2(screen_pos.x - glyph_width, screen_pos.y - 9999), ImVec2(screen_pos.x - glyph_width, screen_pos.y + 9999), ImColor(ImGui::GetStyle().Colors[ImGuiCol_Border]));
            draw_separator = false;
        }

        // Draw interpreted value
        addr = line_i * effectiveColumns;
        for (int n = 0; n < effectiveElems && addr+(elemSize-1) < mem_size; n++, addr += elemSize)
        {
            if (n > 0) ImGui::SameLine();
            printmem(printbuf, datatype, AsHex, swapEndian, mem_data+addr);
            ImGui::TextUnformatted(printbuf);
        }
    }
    clipper.End();
    ImGui::PopStyleVar(2);

    ImGui::EndChild();

    if (data_next && DataEditingAddr < mem_size)
    {
        DataEditingAddr = DataEditingAddr + 1;
        DataEditingTakeFocus = true;
    }
    return true;
}

void MemoryEditor::drawControls()
{
    int addr_digits_count = 0;
    for (size_t n = base_display_addr + mem_size - 1; n > 0; n >>= 4)
        addr_digits_count++;

    float glyph_width = ImGui::CalcTextSize("F").x;
    float cell_width = glyph_width * 3; // "FF " we include trailing space in the width to easily catch clicks everywhere


    ImGui::AlignFirstTextHeightToWidgets();
    ImGui::PushItemWidth(60);
    ImGui::PushAllowKeyboardFocus(false);
    int cols_backup = Columns;
    if (ImGui::DragInt("##cols", &Columns, 0.2f, 4, 64, "%.0f Bytes"))
    {
        ImVec2 new_window_size = ImGui::GetWindowSize();
        new_window_size.x += (Columns - cols_backup) * (cell_width + glyph_width);
        ImGui::SetWindowSize(new_window_size);
    }
    ImGui::PopAllowKeyboardFocus();
    ImGui::PopItemWidth();
    ImGui::SameLine();
    ImGui::Text("Range %0*" PRINTF_INT64_MODIFIER  "X..%0*" PRINTF_INT64_MODIFIER "X",
                addr_digits_count, base_display_addr, addr_digits_count, base_display_addr+mem_size-1);
    ImGui::SameLine();
    ImGui::PushItemWidth(70);
    if (ImGui::InputText("##addr", AddrInput, 32, ImGuiInputTextFlags_CharsHexadecimal | ImGuiInputTextFlags_EnterReturnsTrue))
    {
        int goto_addr;
        if (sscanf(AddrInput, "%X", &goto_addr) == 1)
        {
            goto_addr -= (int)base_display_addr;
            if (goto_addr >= 0 && goto_addr < mem_size)
            {
                ImGui::BeginChild("##scrolling");
                ImGui::SetScrollFromPosY(ImGui::GetCursorStartPos().y + (goto_addr / Columns) * ImGui::GetTextLineHeight());
                ImGui::EndChild();
                DataEditingAddr = goto_addr;
                DataEditingTakeFocus = true;
            }
        }
    }
    ImGui::PopItemWidth();

    ImGui::SameLine();
    ImGui::Text("Type");
    ImGui::PushItemWidth(120);
    ImGui::SameLine();
    ImGui::ComboImproved("##datatype", &typeidx, _datatypeComboCallback, this, MAX_DATATYPES, MAX_DATATYPES);
    ImGui::PopItemWidth();

    ImGui::SameLine();
    ImGui::Text("Mode");
    ImGui::PushItemWidth(50);
    ImGui::SameLine();
    int hex = AsHex;
    ImGui::Combo("##dechex", &hex, "Dec\0Hex");
    AsHex = !!hex;
    ImGui::PopItemWidth();

    ImGui::SameLine();
    ImGui::Text("Endian");
    ImGui::PushItemWidth(100);
    ImGui::SameLine();
    ImGui::Combo("##endian", &Endian, "Native\0Little\0Big");
    ImGui::PopItemWidth();
}

static const char * const endianNames[]
{
    "Native Endian",
    "Little Endian",
    "Big Endian"
};

void MemoryEditor::drawMenu()
{
    if(ImGui::BeginMenu("Type"))
    {
        for(int i = 0; i < MAX_DATATYPES; ++i)
        {
            bool sel = i == typeidx;
            if(ImGui::MenuItem(datatypeName(i), "", &sel))
                typeidx = i;
        }
        ImGui::EndMenu();
    }
    ImGui::Separator();

    bool bdec = !AsHex, bhex = !!AsHex;
    if(ImGui::MenuItem("Dec", "", &bdec))
        AsHex = false;

    if(ImGui::MenuItem("Hex", "", &bhex))
        AsHex = true;

    ImGui::Separator();

    for(int i = 0; i < Countof(endianNames); ++i)
    {
        bool sel = i == Endian;
        if(ImGui::MenuItem(endianNames[i], "", &sel))
            Endian = i;
    }
}

void MemoryEditor::setTypeStr(const char * typestr)
{
    typeidx = b3d_getTypeFromString(typestr);
}

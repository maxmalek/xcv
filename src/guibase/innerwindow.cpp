#include "innerwindow.h"
#include "outerwindow.h"
#include <imgui/imgui.h>
#include <imgui/imgui_dock.h>

const ScriptDef InnerWindow::scriptdef("imwindow", ScriptTypes::INNERWINDOW, NULL);


InnerWindow::InnerWindow(OuterWindow * w, const char *title, const char *ident)
: uivisible(true), deleteOnClose(false), inWindowList(true)
, posX(0), posY(0), winW(0), winH(0)
, dockSplitRatio(0), dockSlot(ImGuiDockSlot_None)
, imguiFlags(0), _outerwin(w), _wintitle(title), _winident(ident && *ident ? ident : title)
, todo(TDF_NONE), shown(false), _drawing(false)
{
    _updateTitle();
    w->_addInnerWindow(this);

    // Can't do this there; dynamic_cast in the ctor is a bad idea, since this->scriptdef points to InnerWindow::scriptdef right now,
    // and if this object is registered right here it will have the wrong type in Lua.
    /*if(Scriptable *sc = dynamic_cast<Scriptable*>(w))
    {
        scriptRegisterSelf(sc->_getInternalState());
        makePersistent();
    }*/
}

InnerWindow::~InnerWindow()
{
    assert(!_drawing);
    removeFromOuter();
}

void InnerWindow::setTitle(const char *title)
{
    _wintitle = title;
    _updateTitle();
}

void InnerWindow::getSize(float * w, float * h)
{
    if(w)
        *w = winW;
    if(h)
        *h = winH;
}

void InnerWindow::getPos(float * x, float * y)
{
    if(x)
        *x = posX;
    if(y)
        *y = posY;
}

void InnerWindow::setInitialPos(float x, float y)
{
    if(!shown)
    {
        posX = x;
        posY = y;
        todo |= TDF_SETPOS_INIT;
    }
}

void InnerWindow::setInitialSize(float w, float h)
{
    if(!shown)
    {
        winW = w;
        winH = h;
        todo |= TDF_SETSIZE_INIT;
    }
}

void InnerWindow::setDockSplitRatio(float ratio)
{
    if(!shown)
    {
        dockSplitRatio = ratio;
        todo |= TDF_SETDOCK_SPLIT;
    }
}

void InnerWindow::setDockSlot(unsigned slot)
{
    if(!shown)
    {
        dockSlot = slot;
        todo |= TDF_SETDOCK_SLOT;
    }
}

void InnerWindow::setSize(float w, float h)
{
    _updateSize(w, h);
    todo |= TDF_SETSIZE;
}

void InnerWindow::setPos(float x, float y)
{
    posX = x;
    posY = y;
    todo |= TDF_SETPOS;
}


void InnerWindow::_updateTitle()
{
    _usetitle = _wintitle + "###" + _winident;
}

void InnerWindow::drawUI()
{
    CountedPtr<InnerWindow> selfref = this; // This is to avoid this window going out of scope while being drawn and otherwise unreferenced

    if(uivisible)
    {
        _drawing = true;
        if(todo)
        {
            if(todo & TDF_SETPOS_INIT)
            {
                ImGui::SetNextWindowPos(ImVec2(posX, posY), ImGuiSetCond_FirstUseEver);
                ImGui::SetNextDockFloatingPos(ImVec2(posX, posY));
            }
            if(todo & TDF_SETSIZE_INIT)
            {
                ImGui::SetNextWindowSize(ImVec2(winW, winH), ImGuiSetCond_FirstUseEver);
                ImGui::SetNextDockFloatingSize(ImVec2(winW, winH));
            }
            if(todo & TDF_SETPOS)
                ImGui::SetNextWindowPos(ImVec2(posX, posY));
            if(todo & TDF_SETSIZE)
                ImGui::SetNextWindowSize(ImVec2(winW, winH));
            if(todo & TDF_SETDOCK_SPLIT)
                ImGui::SetNextDockSplitRatio(ImVec2(dockSplitRatio, dockSplitRatio));
            if(todo & TDF_SETDOCK_SLOT && dockSlot != ImGuiDockSlot_None)
                ImGui::SetNextDock(ImGuiDockSlot(dockSlot));

            todo = TDF_NONE;
        }
        shown = true;

        glm::vec2 startpos, contentsize;
        bool drawbox = false;
        const bool dock = dockSlot != ImGuiDockSlot_None;
        if(dock
            ? ImGui::BeginDock(_usetitle.c_str(), &uivisible, imguiFlags)
            : ImGui::Begin(_usetitle.c_str(), &uivisible, ImVec2(0.f, 0.f), -1.0f, imguiFlags)
            )
        {
            const ImVec2 cursize = ImGui::GetWindowSize();
            _updateSize(cursize.x, cursize.y);
            startpos = ImGui::GetCursorScreenPos();
            startpos += glm::vec2(ImGui::GetScrollX(), ImGui::GetScrollY());
            contentsize = ImGui::GetContentRegionAvail();
            drawbox = dockSlot != ImGuiDockSlot_Float;
            drawContent();
            if(!uivisible)
            {
                onWinClose();
            }
        }

        bool focus;
        if(dock)
        {
            ImDrawList * const drawlist = ImGui::GetWindowDrawList();
            ImGui::EndDock();
            focus = ImGui::IsLastDockFocused();
            if(focus && drawbox)
            {
                glm::vec2 endpos = startpos + contentsize;
                startpos -= glm::vec2(2.0f, 5.0f);
                endpos   += glm::vec2(4.0f, 4.0f);
                const ImColor col(255, 255, 100, 50);
                drawlist->AddRect(startpos, endpos, col, 5.0f, ~0, 3.0f);
            }
        }
        else
        {
            focus = ImGui::IsRootWindowOrAnyChildFocused();
            ImGui::End();
        }

        if(_outerwin && focus)
            _outerwin->focusedInner = this;
    }
    _drawing = false;

    // handle delayed close
    if(!_outerwin)
        _destroy();
}

void InnerWindow::drawContent()
{
    callOpt("drawUI", 0);
}

void InnerWindow::onUpdate(float dt)
{
    CallEnv env(this, "onUpdate");
    if(env)
    {
        push(dt);
        env.call(0);
    }
}

void InnerWindow::onResize(float w, float h)
{
    CallEnv env(this, "onResize");
    if(env)
    {
        push(w);
        push(h);
        env.call(0);
    }
}

void InnerWindow::destroy()
{
    callOpt("onWinClose", 0);
    _destroy();
}

void InnerWindow::_updateSize(float w, float h)
{
    if(w != winW || h != winH)
    {
        winW = w;
        winH = h;
        onResize(w, h);
    }
}

void InnerWindow::_destroy()
{
    removeFromOuter();
    if(!_drawing)
        unbindScript();
}

void InnerWindow::removeFromOuter()
{
    if(_outerwin)
    {
        _outerwin->_removeInnerWindow(this);
        makeNotPersistent();
        _outerwin = NULL;
    }
}

void InnerWindow::setShowMenuBar(bool on)
{
    if(on)
        imguiFlags |= ImGuiWindowFlags_MenuBar;
    else
        imguiFlags &= ~ImGuiWindowFlags_MenuBar;
}

void InnerWindow::onWinClose()
{
    callOpt("onWinClose", 0);
    if(deleteOnClose)
        _destroy();
}

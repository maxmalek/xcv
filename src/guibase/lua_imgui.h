#pragma once
struct lua_State;
int register_lua_imgui(lua_State *L);
int lua_imgui_getWindowFlags(lua_State *L, int idx); // returns ImGuiWindowFlags

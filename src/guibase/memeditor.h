#include "innerwindow.h"

class MemoryEditor : public ScriptableT<MemoryEditor>
{
public:
    static const ScriptDef scriptdef;

    int typeidx; // Blob3dType
    bool    AsHex;
    bool    AllowEdits;
    int     Endian;
    int     Columns;
    int     DataEditingAddr;
    bool    DataEditingTakeFocus;
    char    DataInput[32];
    char    AddrInput[32];
    const unsigned char *mem_data;
    unsigned char *mem_write;
    size_t  mem_size;
    size_t  base_display_addr;

    MemoryEditor();

    bool drawUI();
    void drawControls();
    void drawMenu();
    void setTypeStr(const char *typestr);
};

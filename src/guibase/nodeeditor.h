#pragma once

#include <string>
#include <vector>
#include <map>
#include "glmx.h"
#include "innerwindow.h"
#include "luaobj.h"

struct ConnectorBase;
struct InputConnector;
struct OutputConnector;
struct ConnectorDesc;
class NodeEditorView;
struct ImDrawList;
struct DragNode;
class NodeView;


struct Connection : public ScriptableT<Connection>
{
    friend class NodeEditorView;
    friend class NodeView;

    static const ScriptDef scriptdef;

    enum Status // bitmask
    {
        NONE = 0,
        ACTIVE = 1, // Connection is established and working. If not set, connection is there, but doesn't count
        ANIMATED = 2,
    };

    Connection(OutputConnector *src, InputConnector *trg);
    void unlink();
    void onUnlink();
    void onDestroy();

    inline bool isSelected() const { return selected == 1; }
    void setSelected(bool on);
    inline bool animated() const { return !!(status & ANIMATED); }
    inline bool active() const { return !!(status & ACTIVE); }
    void setstatus(Status s, bool on);
    NodeView *getSourceNode();
    NodeView *getTargetNode();
    const char *getSourceConnName() const;
    const char *getTargetConnName() const;
    bool check() const; // check if allowed, ie. connectors are compatible.
    bool updateStatus(); // setstatus(ACTIVE, check());
    void updateColor(); // set to color of source connector

    unsigned char status;
    glm::vec4 color, colorOV;
    bool overrideColor;

    // Fixed during object lifetime. Not publicly defined.
    OutputConnector * const source;
    InputConnector * const target;

private:
    unsigned char selected; // 1 = fully selected; 2 = being selected
};

struct ConnectorDesc
{
    std::string name;
    std::string detail;
    glm::vec4 color;
    int type;

    // for sorting
    inline bool operator<(const ConnectorDesc& o) const
    {
        return name < o.name;
    }
};

enum ConnectorKind
{
    CK_INPUT,
    CK_OUTPUT
};

struct ConnectorBase
{
protected:
    ConnectorBase(ConnectorKind k, const ConnectorDesc& d, NodeView *src)
        : desc(d), node(src), kind(k) {}
public:
    virtual ~ConnectorBase() {}

    inline glm::vec2 getPos(glm::vec2 offset = glm::vec2()) const;
    virtual bool isInUse() const { assert(false); return false; }
    virtual void unlinkAll() { assert(false); }
    inline const char *slotName() const { return desc.name.c_str(); } // this == node->{inputs/outputs}[slotName]

    glm::vec2 pos; // relative to node node
    ConnectorDesc desc;

    NodeView * const node; // Node this connector is attached to
    const ConnectorKind kind;
};

struct OutputConnector;

struct InputConnector : public ConnectorBase
{
    InputConnector(const ConnectorDesc& d, NodeView *src)
        : ConnectorBase(CK_INPUT, d, src) {}
    virtual ~InputConnector() { unlinkAll(); }

    virtual bool isInUse() const { return !!connection; }
    virtual void unlinkAll() { unlink(); }

    CountedPtr<Connection> connection; // connection to OutputConnector

    Connection *link(OutputConnector *from); // defined below

    void unlink();
};

struct OutputConnector : public ConnectorBase
{
    OutputConnector(const ConnectorDesc& d, NodeView *src)
        : ConnectorBase(CK_OUTPUT, d, src) {}
    virtual ~OutputConnector() { unlinkAll(); }

    virtual bool isInUse() const { return !connections.empty(); }
    virtual void unlinkAll();

    typedef std::vector<CountedPtr<Connection> > Conns;
    Conns connections;

    Connection *link(InputConnector *to);

    void unlink(InputConnector *to);
};

class NodeView : public ScriptableT<NodeView>
{
    friend class NodeEditorView;

public:
    typedef std::map<std::string, InputConnector*> InputConMap;
    typedef std::map<std::string, OutputConnector*> OutputConMap;

    static const ScriptDef scriptdef;
    static bool IsInFrontOf(const NodeView *a, const NodeView *b);

    static const glm::vec4 DEFAULT_BODY_COLOR;

    NodeView(NodeEditorView *holder);
    void *user;

    std::string name;
    glm::vec4 titleColor, bodyColor, borderColor;
    std::vector<ConnectorDesc> inDesc, outDesc;
    glm::vec2 pos;

    glm::vec2 getSize() const { return size; }

    const InputConMap& getInputConnectors() const { return inputs; }
    const OutputConMap& getOutputConnectors() const { return outputs; }
    Connection *getInputConnection(const char *slot);
    const std::vector<CountedPtr<Connection> > *getOutputConnections(const char *slot); // one output connector can have multiple connections
    NodeEditorView *getHolder() { return _holder; } // NULL when the node was removed from its holder (e.g during deletion)
    const NodeEditorView *getHolder() const { return _holder; }
    bool overlapsWith(glm::vec2 ul, glm::vec2 br);
    void clearAllSelections();
    Connection *connectToTarget(const char *srcslot, NodeView *target, const char *targetslot, bool force);
    Connection *connectToSource(const char *targetslot, NodeView *source, const char * sourceslot, bool force);
    void setSelected(bool sel);
    bool isSelected() const { return selected; }

    void updateContent();
    void moveToFront();

    void destroy();
    inline unsigned getInternalID() const { return _id; }

    // for internal use
    virtual void onLinkToTarget(Connection *con, const char * sourceslot, const char * targetslot);
    virtual void onUnlinkFromTarget(Connection *con, const char * sourceslot, const char * targetslot);

protected:
    virtual void onDestroy() {}
    virtual void drawUI();
    virtual ~NodeView();
    void _unlinkAll();

private:

    int _zorder; // topmost node: 0, increasing is below
    bool selected;
    glm::vec2 size;
    glm::vec2 customContentSize;
    unsigned _id;
    InputConMap inputs; // inputs & outputs must not contain NULLs!
    OutputConMap outputs;
    NodeEditorView * _holder;
};


class NodeEditorView : public ScriptableT<NodeEditorView>
{
public:
    static const ScriptDef scriptdef;

    NodeEditorView();
    ~NodeEditorView();
    void drawPanel();
    void removeNode(NodeView *node);
    NodeView *getNodeByID(unsigned id) { return allnodes[id].content(); }
    const NodeView *getNodeByID(unsigned id) const { return allnodes[id].content(); }
    size_t getMaxNodes() const { return allnodes.size(); }
    size_t getNumNodes() const;
    void sortNodeZ();
    void updateAnimation(float dt);
    void selectNode(NodeView *n);
    void unselectNode(NodeView *n);
    void _unselectCon(Connection *c);
    void clearAllSelections();
    void removeAllNodes();
    const std::vector<NodeView*>& getSelectedNodes() const { return selnodes; }

    glm::vec2 getMousePosInPanel();
    glm::vec2 getMousePosOnScreen();
    glm::vec2 screenPosToPanelPos(glm::vec2 p);
    glm::vec2 panelPosToScreenPos(glm::vec2 p);
    glm::vec2 getLocalCursorPos() const { return _savedCursorPos; }
    glm::vec2 getScrollPos() const { return scrolling; }
    glm::vec2 getPanelSize() const { return _panelSize; }
    void setScrollPos(glm::vec2 p) { scrolling = p; }

    void getPanelSelectionBounds(glm::vec2 *ul, glm::vec2 *br);

    // internal use
    bool _canConnect(ConnectorBase *a, ConnectorBase *b) const;

protected:
    virtual NodeView *createNode();
    virtual void onDeleteNode(NodeView *n) {}
    virtual bool canConnect(const NodeView& source, const ConnectorDesc& sourceCon, const NodeView& target, const ConnectorDesc& targetCon) const;

    bool _addNode(NodeView *node);
    NodeView *_createNode();
    NodeView *_createNodeAt(glm::vec2 pos);
    NodeView *_createNodeAtCursor();

private:
    enum NodeState { N_NOTHING, N_INUSE, N_HOVERED, N_SELECTED, N_DRAGGED }; // order is important!
    NodeState _drawNodes(ImDrawList*, glm::vec2 offset);
    NodeState _drawNode(ImDrawList*, glm::vec2 offset, NodeView *node);
    void _handleScrolling();
    void _handleDragging(glm::vec2 offset);
    void _handleContextMenu();
    void _handleSelectionRect();
    void _drawBG(ImDrawList *drawList);
    void _drawConnectionLines(ImDrawList *drawList, glm::vec2 offset);
    bool _drawConnector(ConnectorBase *con, ImDrawList *drawlist, glm::vec2 node_rect_min, glm::vec2 textoffs);
    void _drawSelectionRect();
    void _removeNodeByID(int id);
    void _deleteSelection();
    void _prepareNodeDeletion(NodeView *n);
    void _callContextMenuFunc(const char *fn);
    ConnectorBase *_getHoverCon(glm::vec2 offset);
    std::vector<CountedPtr<NodeView> > allnodes;
    std::vector<NodeView*> nodeZsort;

    bool active;
    int _dragState;

    struct DragNode
    {
        DragNode() : start(0) {}
        glm::vec2 startpos;
        ConnectorBase *start;
    };

    DragNode _dragNode;
    std::vector<NodeView*> selnodes, beingselected;
    std::vector<Connection*> beingselconns;
    int node_hovered_in_list;
    int node_hovered_in_scene;
    int node_active_used; // ID of the node whose controls are used
    int node_active_context_menu; // ID of node the context menu was opened upon
    int node_top_hovered;
    int node_last_selected;
    int timeAccu;
    float timeRemain;
    bool open_context_menu, is_context_menu_open, _animateLines, selectionActive, mouseInPanelArea;
    glm::vec2 _selectionStart;
    glm::vec2 scrolling;
    glm::vec2 _panelPos, _panelSize;
    glm::vec2 _savedCursorPos; // used for context menu
private:
    NodeEditorView(const NodeEditorView&);
};

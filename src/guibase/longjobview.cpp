#include "longjobview.h"
#include "outerwindow.h"

#include "guibase.h"

LongJobExecutorWindow::LongJobExecutorWindow(OuterWindow *w, const LongJobExecutor & exc)
: InnerWindow(w, "Long jobs")
, _exc(exc), height(0)
{
    imguiFlags = ImGuiWindowFlags_NoTitleBar
               | ImGuiWindowFlags_NoResize
               | ImGuiWindowFlags_NoMove
               | ImGuiWindowFlags_NoSavedSettings
               | ImGuiWindowFlags_AlwaysAutoResize;
    inWindowList = false;
}

LongJobExecutorWindow::~LongJobExecutorWindow()
{
}

void LongJobExecutorWindow::drawUI()
{
    if(!uivisible || _exc.getJobs().empty())
        return;

    const ImVec2 screensize = ImGui::GetIO().DisplaySize;
    ImGui::SetNextWindowPos(ImVec2(0.0f, screensize.y - height - 20));
    if(ImGui::Begin(_usetitle.c_str(), &uivisible, imguiFlags))
    {
        const float y = ImGui::GetCursorPosY();
        drawContent();
        height = ImGui::GetCursorPosY() - y;
    }
    else
        height = 0;
    ImGui::End();
}


void LongJobExecutorWindow::drawContent()
{
    const std::vector<SequentialLongJob*>& jobs = _exc.getJobs();
    if(jobs.size())
    {
        ImGui::PushStyleColor(ImGuiCol_PlotHistogram, ImVec4(0.0f, 0.0f, 0.5f, 0.8f));
        for(size_t i = 0; i < jobs.size(); ++i)
        {
            ImGui::PushID((int)i);
                SequentialLongJob *j = jobs[i];
                const glm::vec4 pd = j->getPercDone();
                if(ImGui::SmallButton("X"))
                    j->cancel();
                ImGui::SameLine();
                if(pd.y < 0 && pd.z < 0 && pd.w < 0)
                {
                    ImGui::ProgressBar(pd.x, ImVec2(130, 0));
                    ImGui::SameLine();
                    ImGui::TextUnformatted(j->getName());
                }
                else
                {
                    ImGui::TextUnformatted(j->getName());
                    for(unsigned i = 0; i < 4; ++i)
                        if(pd[i] >= 0)
                            ImGui::ProgressBar(pd[i], ImVec2(130, 13));
                }
                j->drawUI();
            ImGui::PopID();
        }
        ImGui::PopStyleColor();
    }
}

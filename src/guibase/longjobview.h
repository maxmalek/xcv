#pragma once

#include "longjob.h"
#include "innerwindow.h"

class LongJobExecutorWindow : public InnerWindow
{
public:
    LongJobExecutorWindow(OuterWindow *w, const LongJobExecutor& exc);
    virtual ~LongJobExecutorWindow();
    virtual void drawContent();
    virtual void drawUI();
protected:
    float height;
    const LongJobExecutor& _exc;
};


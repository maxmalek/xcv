#pragma once

#include <imgui/imgui.h>
#include <imgui/imgui_dock.h>
#include "texture.h"

extern IMGUI_API void ImGui_Impl_CacheReference(const Refcounted<REFCOUNT_NORMAL> *obj);

namespace ImGui {


inline static ImTextureID ToTexID(const _TextureBase *tex)
{
    // Keep texture around until the end of the frame. Premature deletion will cause GL errors.
    ImGui_Impl_CacheReference(tex);

    return (ImTextureID)tex;
}

}

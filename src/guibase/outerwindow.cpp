#include "outerwindow.h"
#include "innerwindow.h"
#include <algorithm>
#include <imgui/imgui.h>
#include <imgui/imgui_dock.h>

#include "natsort/strnatcmp.h"

OuterWindow::OuterWindow()
    : focusedInner(NULL)
{
#ifdef _DEBUG
    show_test_window = false;
#endif
}

OuterWindow::~OuterWindow()
{
    _deleteInnerWindows();
}

void OuterWindow::onUpdate(float dt)
{
    for(size_t i = 0; i < _innerwindows.size(); ++i)
        _innerwindows[i]->onUpdate(dt);
}

static bool winLT(const InnerWindow *a, const InnerWindow *b)
{
    return strnatcasecmp(a->getTitle().c_str(), b->getTitle().c_str()) < 0;
}
void OuterWindow::_updateDrawOrder()
{
    _draworder.resize(_innerwindows.size());
    for(size_t i = 0; i < _innerwindows.size(); ++i)
        _draworder[i] = _innerwindows[i].content();
     std::sort(_draworder.begin(), _draworder.end(), winLT);
}

void OuterWindow::drawUI()
{
    if(_innerwindows.size())
        _drawInnerWindows();

#ifdef _DEBUG
    if(show_test_window)
    {
        ImGui::SetNextWindowPos(ImVec2(50, 20), ImGuiSetCond_FirstUseEver);
        ImGui::ShowTestWindow(&show_test_window);
    }
#endif
}

void OuterWindow::_drawInnerWindows()
{
    for(size_t i = 0; i < _innerwindows.size(); ++i)
        _innerwindows[i]->drawUI();
}

InnerWindow *OuterWindow::_drawInnerWindowMenuItems()
{
    InnerWindow *clicked = NULL;
    for(size_t i = 0; i < _draworder.size(); ++i)
    {
        InnerWindow *iw = _draworder[i];
        if(iw->inWindowList)
            if(ImGui::MenuItem(iw->getTitle().c_str(), NULL, &iw->uivisible))
                clicked = iw;
    }
#ifdef _DEBUG
    ImGui::Separator();
    ImGui::MenuItem("Test window", NULL, &show_test_window);
#endif
    return clicked;
}

void OuterWindow::_removeInnerWindow(InnerWindow * w)
{
    for(size_t i = 0; i < _innerwindows.size(); ++i)
        if(w == _innerwindows[i])
        {
            _innerwindows[i] = _innerwindows.back();
            _innerwindows.pop_back();
            _updateDrawOrder();
            if(w == focusedInner)
                focusedInner = NULL;
            break;
        }
}

void OuterWindow::_addInnerWindow(InnerWindow * w)
{
    for(size_t i = 0; i < _innerwindows.size(); ++i)
        if(w == _innerwindows[i])
            return;
    _innerwindows.push_back(w);
    _updateDrawOrder();
}

void OuterWindow::_deleteInnerWindows()
{
    while(_innerwindows.size())
        _innerwindows[0]->destroy();
}

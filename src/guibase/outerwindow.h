#pragma once

#include <vector>
#include "innerwindow.h"

class InnerWindow;

// Holds inner windows
class OuterWindow
{
    friend class InnerWindow;

public:
    void drawUI();
    InnerWindow *_drawInnerWindowMenuItems();
    InnerWindow *getFocusedInnerWindow() { return focusedInner; }

protected:
    OuterWindow();
    virtual ~OuterWindow();

    void _drawInnerWindows();

    void _removeInnerWindow(InnerWindow *w);
    void _addInnerWindow(InnerWindow *w);
    void _deleteInnerWindows();

    virtual void onUpdate(float dt);

    std::vector<CountedPtr<InnerWindow> > _innerwindows;
    
private:
    void _updateDrawOrder();
    std::vector<InnerWindow*> _draworder;
    InnerWindow *focusedInner;

#ifdef _DEBUG
    bool show_test_window;
#endif
};


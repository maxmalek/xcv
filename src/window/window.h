#pragma once

#include <vector>
#include <string>


class Renderer;
class Texture;
class IEventRecv;

typedef union SDL_Event SDL_Event;
struct SDL_KeyboardEvent;
struct SDL_Window;

struct ImGuiContext;


class Window
{
public:
    enum CloseStatus
    {
        IS_OPEN = 0,
        CLOSE_REQUESTED = 1,
        CLOSING = 2,
        CLOSED = 3
    };
    enum KeyState
    {
        KDOWN        = 0x01, // Key is currently down
        KPRESSED     = 0x02, // Key was just pressed (set for 1 frame)
        KREPEAT      = 0x04, // Key is held and presses are repeats
        KRELEASED    = 0x08  // Key was just released (set for 1 frame)
    };

    static bool StaticInit();

    static bool UpdateAll(float dt); // update logic and draw all windows
    static void HandleEvents(); // distribute events to all windows; to process in next update()
    static void RequestCloseAll();
    static void CloseAllNow();
    static void TempCloseAll();
    static unsigned TempReopenAll(); // returns number of inits that failed
    static bool IsAnyOpen();

    static KeyState GetKeyState(unsigned k); // SDL_Scancode k
    static unsigned GetKeyCode(const char *kname);
    static const char *GetKeyName(unsigned k);

    Window();
    virtual ~Window();
    bool open(unsigned w, unsigned h, bool full = false, int screen = 0);
    bool close(); // True when closed and unregsitered

    bool update(float dt);

    bool isKey(int k) const; // SDL_Scancode
    void getSize(int *w, int *h) const;
    void setSize(int w, int h);

    inline Renderer *getRenderer() const { return render; }

    bool isMouseButton(unsigned int btn);
    void placeMouse(int x, int y);

    bool isOpen() const { return !!window; }
    bool isClosing() const { return _closestatus == CLOSE_REQUESTED || _closestatus == CLOSING; }

    typedef void (*DrawHookFn)(Window *, void *);
    void addDrawHook(DrawHookFn fn, void *ud);

    void addEventRecv(IEventRecv *ev) { _ev.push_back(ev); }
    void setTitle(const char *s);

    bool setPosition(unsigned x, unsigned y);
    void detach();

    bool requestClose();

    static void SetStaticRenderer(Renderer *r);
    static Window *GetCurrent();

protected:
    virtual void onOpen() {}
    virtual void onClose();
    virtual void onAfterClose();
    virtual bool onCloseRequest();
    virtual void onUpdate(float dt) {}

    struct DrawHook
    {
        DrawHookFn fn;
        void *data;
    };
    void _forwardEvent(const SDL_Event& ev);
    void _pushPendingEvent(const SDL_Event&);
    void _handleEvent(const SDL_Event&);
    
    int _winID;
    Renderer *render;
    SDL_Window *window;
    ImGuiContext *imctx;
    std::vector<DrawHook> _drawHooks;
    std::vector<IEventRecv*> _ev;
    CloseStatus _closestatus;
    bool _autoclose, _full, _detached, _firstopen;
    int _originalW, _originalH;
    int _originalX, _originalY;

    void *pendingEvents;
    unsigned pendingEventsCapacity;
    unsigned numPendingEvents;

    static Window *GetWindowByID(int id);
    static void RegisterWindow(Window *);
    static void UnregisterWindow(Window *);
    static std::vector<Window*> s_aliveWindows;
    static std::vector<Window*> s_windowStack; // top window is active (GL context bound and ImGui context active)
    static void RemoveWindowFromStack(Window *w);

private:
    static void _HandleKeyEvent(const SDL_KeyboardEvent&);

    struct MakeCurrent
    {
        MakeCurrent(Window *w);
        ~MakeCurrent();
    private:
        MakeCurrent(const MakeCurrent&); // non-copyable
        Window *_w;
    };
    struct Status
    {
        int x,y,w,h;
        int flags;
        std::string title;
    } statusBackup;
    void _makeCurrent(); // should ONLY be called by struct MakeCurrent ctor

    bool _initRenderer();
    void _deleteRenderer();
    void _backupStatusAndDestroy();
    bool _recreateFromBackup();

};

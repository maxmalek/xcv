#include "window.h"
#include "renderer.h"
#include <SDL.h>
#include "guibase.h"
#include "imgui_impl_sdl.h"
#include "event.h"
#include "log.h"
#include "platform.h"
#include "deviceobject.h"
#include "util.h"
#include <algorithm>


static const int WIN_ID_UNREGISTERED = -1;

static Renderer *defaultRenderer = NULL;

std::vector<Window*> Window::s_windowStack;
std::vector<Window*> Window::s_aliveWindows;

#define PENDING ((SDL_Event*)pendingEvents)


Window::MakeCurrent::MakeCurrent(Window * w)
: _w(w)
{
    assert(w);

    // Top window is always the one current
    s_windowStack.push_back(w);

    // Make window current
    w->_makeCurrent();
}

Window::MakeCurrent::~MakeCurrent()
{
    Window *top = s_windowStack.back();
    // Current window must be on top. NULL will be there as sentinel value if window was deleted but is still in the stack
    assert(top == NULL || top == _w);
    s_windowStack.pop_back();
    top = s_windowStack.size() ? s_windowStack.back() : NULL;

    if(top == _w)
    {
        // same window still current, nothing to do
    }
    else if(top)
        // Top window is different, make that one current
        top->_makeCurrent();
    else
    {
        // Window that should be made current is dead, use global state as backup
        if(defaultRenderer)
            defaultRenderer->makeCurrent();
        ImGui::SetCurrentContext(NULL); // restore the global/static default context
    }
}

void Window::SetStaticRenderer(Renderer *r)
{
    defaultRenderer = r;
}

Window * Window::GetCurrent()
{
    return s_windowStack.back();
}

void Window::UnregisterWindow(Window *w)
{
    DEBUG_LOG("Unregister window ID %d", w->_winID);
    assert(w->window);
    assert(w->_winID != WIN_ID_UNREGISTERED);
    assert(w->_winID == SDL_GetWindowID(w->window));

    for(size_t i = 0; i < s_windowStack.size(); ++i)
        if(s_windowStack[i] == w)
            s_windowStack[i] = NULL;

    for(size_t i = 0; i < s_aliveWindows.size(); ++i)
        if(s_aliveWindows[i] == w)
        {
            s_aliveWindows[i] = s_aliveWindows.back();
            s_aliveWindows.pop_back();
            w->_winID = WIN_ID_UNREGISTERED;
            return;
        }

    assert(false); // not found?!
}

void Window::RegisterWindow(Window *w)
{
    assert(w->_winID == WIN_ID_UNREGISTERED);
    assert(w->window);
    assert(std::find(s_aliveWindows.begin(), s_aliveWindows.end(), w) == s_aliveWindows.end());
    w->_winID = SDL_GetWindowID(w->window);
    s_aliveWindows.push_back(w);
    DEBUG_LOG("Registered window ID %d", w->_winID);
}

Window *Window::GetWindowByID(int id)
{
    for(size_t i = 0; i < s_aliveWindows.size(); ++i)
        if(s_aliveWindows[i]->_winID == id)
            return s_aliveWindows[i];
    return NULL;
}

Window::Window()
: _winID(WIN_ID_UNREGISTERED)
, render(NULL)
, window(NULL)
, _closestatus(CLOSED)
, _autoclose(true)
, _full(false)
, _detached(false)
, _firstopen(true)
, _originalW(0), _originalH(0)
, _originalX(SDL_WINDOWPOS_CENTERED), _originalY(SDL_WINDOWPOS_CENTERED)
, pendingEvents(NULL)
, pendingEventsCapacity(0)
, numPendingEvents(0)
{
}

Window::~Window()
{
    close();
    free(pendingEvents);
}

bool Window::close()
{
    if(!window)
    {
        _closestatus = CLOSED;
        return false;
    }

    if(_closestatus != CLOSING && _closestatus != CLOSED)
    {
        _closestatus = CLOSING;

        {
            MakeCurrent c(this);

            onClose();
            ImGui_Impl_GL45_InvalidateContext(imctx);
            _deleteRenderer();
            ImGui::DestroyContext(imctx);
            imctx = NULL;
            UnregisterWindow(this);
            SDL_DestroyWindow(window);
            window = NULL;
            for(size_t i = 0; i < _ev.size(); ++i)
                delete _ev[i];
            _ev.clear();
            _closestatus = CLOSED;
        }
        onAfterClose(); // might delete this (dtor of MakeCurrent c should run before this!)
        return true;
    }

    return false;
}

void Window::detach()
{
    _detached = true;
}

bool Window::requestClose()
{
    bool c = onCloseRequest();
    if(c)
        _closestatus = CLOSE_REQUESTED;
    return c;
}

void Window::onClose()
{
    assert(false && "Window should be closed before deleting it (do not call Window::onClose()!)");
}

void Window::onAfterClose()
{
    if(_detached)
        delete this;
}

bool Window::onCloseRequest()
{
    return true;
}

void Window::_makeCurrent()
{
    render->makeCurrent();
    ImGui::SetCurrentContext(imctx);
}

bool Window::_initRenderer()
{
    assert(!render);
    render = Renderer::Create(window, Renderer::Flags(Renderer::DEBUG | Renderer::VSYNC)); // TODO: make debug context optional
    return !!render;
}

void Window::_deleteRenderer()
{
    Renderer::Delete(render);
    render = NULL;
}

bool Window::open(unsigned w, unsigned h, bool full, int screen)
{
    //printf("Window::open() on display #%u [%s]\n", displayIdx, SDL_GetDisplayName(displayIdx));

    assert(_closestatus != CLOSING);

    if(_firstopen)
    {
        _firstopen = false;
        _originalX = _originalY = SDL_WINDOWPOS_CENTERED_DISPLAY(screen);
    }

    // use screen index where the window actually is if it was already opened
    if(window)
        screen = SDL_GetWindowDisplayIndex(window);


    int x = _originalX;
    int y = _originalY;

    // detect bounds
    SDL_Rect bounds = {0,0,0,0};
    SDL_GetDisplayUsableBounds(screen, &bounds);
    logdebug("Detected bounds: (%d,%d)-(%d,%d) on display #%u [%s]", bounds.x, bounds.y, bounds.w, bounds.h, screen, SDL_GetDisplayName(screen));
    int maxw = bounds.w;
    int maxh = bounds.h;
    if(full)
    {
        SDL_DisplayMode mode;
        SDL_GetDesktopDisplayMode(screen, &mode);
        logdebug("Detected desktop mode: %ux%u on display #%u [%s]", w, h, screen, SDL_GetDisplayName(screen));
        // min already 0
        maxw = mode.w;
        maxh = mode.h;
    }

    const bool maximize = !w && !h;

    if(!w && full)
        w = maxw;

    if(!h && full)
        h = maxh;

    _full = full;

    unsigned flags = SDL_WINDOW_OPENGL;
    if(full)
    {
        // both fullscreen modes cause more trouble than good,
        // since when clicking outside of the window area, the window gets minimized automatically.
        // using a borderless window instead makes it work nicely as if it was fullscreen,
        // but the minimize/maximize rules are those for normal windows.
        if(w == maxw && h == maxh)
            flags |= SDL_WINDOW_BORDERLESS; //SDL_WINDOW_FULLSCREEN_DESKTOP;
        else
            flags |= SDL_WINDOW_FULLSCREEN;
    }
    else
        flags |= SDL_WINDOW_RESIZABLE;

    if(window)
    {
        log("Toggle fullscreen on display #%u", screen);
        flags &= (SDL_WINDOW_FULLSCREEN | SDL_WINDOW_FULLSCREEN_DESKTOP);

        SDL_SetWindowBordered(window, (SDL_bool)!(flags & SDL_WINDOW_BORDERLESS));

        if(full)
        {
            SDL_GetWindowPosition(window, &_originalX, &_originalY);
            SDL_GetWindowSize(window, &_originalW, &_originalH);
            SDL_SetWindowPosition(window, bounds.x, bounds.y);
            SDL_SetWindowSize(window, w, h);
        }

        if(SDL_SetWindowFullscreen(window, flags) < 0)
            return false;

        if(!full)
        {
            logdebug("Restore window to (%u, %u) size (%ux%u)", _originalX, _originalY, _originalW, _originalH);
            SDL_SetWindowSize(window, _originalW, _originalH);
            SDL_SetWindowPosition(window, _originalX, _originalY);

            if(maximize)
                SDL_MaximizeWindow(window);
        }
    }
    else
    {
        if(full)
        {
            x = bounds.x;
            y = bounds.y;
        }
        else if(maximize)
        {
            flags |= SDL_WINDOW_MAXIMIZED;
            int bT, bL, bR, bB;
            SDL_GetWindowBordersSize(window, &bT, &bL, &bR, &bB);
            if(!w)
                w = bounds.w - bL - bR;
            if(!h)
                h = bounds.h - bT - bB;
        }

        window = SDL_CreateWindow("", x, y, w, h, flags);
        if(!window)
        {
            logerror("Failed to create window:\n%s", SDL_GetError());
            return false;
        }

        _originalX = _originalY = SDL_WINDOWPOS_CENTERED_DISPLAY(screen);
        _originalW = w;
        _originalH = h;

        RegisterWindow(this);

        if(!_initRenderer())
        {
            close();
            return false;
        }

        imctx = ImGui::CreateContext();
        ImGui::SetCurrentContext(imctx); // make current before ImGui  init
        ImGui_ImplSdl_Init(window); // doesn't require renderer to be current

        MakeCurrent c(this);

        render->resize();
        onOpen();
        update(0); // draw once to initialize everything
    }

    if(const size_t sz = _ev.size())
    {
        int w, h;
        SDL_GetWindowSize(window, &w, &h);
        for(size_t i = 0; i < sz; ++i)
            _ev[i]->windowResize(w, h);
    }

    _closestatus = IS_OPEN;

    return true;
}

bool Window::setPosition(unsigned x, unsigned y)
{
    _originalX = x;
    _originalY = y;
    if(!window)
        return false;
    SDL_SetWindowPosition(window, x, y);
    return true;
}

void Window::RequestCloseAll()
{
    for(size_t i = 0; i < s_aliveWindows.size(); ++i)
        s_aliveWindows[i]->_closestatus = CLOSE_REQUESTED;
}

void Window::CloseAllNow()
{
    while(s_aliveWindows.size())
        if(Window *w = s_aliveWindows.back())
            w->close();
}

bool Window::IsAnyOpen()
{
    return !s_aliveWindows.empty();
}

bool Window::UpdateAll(float dt)
{
    for(unsigned i = 0; i < s_aliveWindows.size(); ++i)
        if(!s_aliveWindows[i]->update(dt))
            return false;
    return true;
}

unsigned Window::TempReopenAll()
{
    unsigned failed = 0;
    for(unsigned i = 0; i < s_aliveWindows.size(); ++i)
        failed += !s_aliveWindows[i]->_recreateFromBackup();
    return failed;
}

void Window::TempCloseAll()
{
    for(unsigned i = 0; i < s_aliveWindows.size(); ++i)
        s_aliveWindows[i]->_backupStatusAndDestroy();
}

static Window::KeyState s_keyState[SDL_NUM_SCANCODES];

void Window::HandleEvents(void)
{
    for(unsigned i = 0; i < Countof(s_keyState); ++i)
        s_keyState[i] = KeyState(s_keyState[i] & ~(KPRESSED | KRELEASED));

    for(unsigned i = 0; i < s_aliveWindows.size(); ++i)
    {
        Window *w = s_aliveWindows[i];
        if(w->isClosing() && w->_autoclose)
            i -= w->close();
    }

    SDL_Event evt;
    while(SDL_PollEvent(&evt))
    {
        int winID = -1;
        switch(evt.type)
        {
            case SDL_KEYDOWN:
            case SDL_KEYUP:
                winID = evt.key.windowID;
                _HandleKeyEvent(evt.key);
                break;
            case SDL_WINDOWEVENT:
                winID = evt.window.windowID;
                break;
            case SDL_MOUSEMOTION:
                winID = evt.motion.windowID;
                break;
            case SDL_MOUSEBUTTONDOWN:
            case SDL_MOUSEBUTTONUP:
                winID = evt.button.windowID;
                break;
            case SDL_MOUSEWHEEL:
                winID = evt.wheel.windowID;
                break;
            case SDL_TEXTEDITING:
                winID = evt.edit.windowID;
                break;
            case SDL_TEXTINPUT:
                winID = evt.text.windowID;
                break;
            case SDL_DROPFILE:
                winID = evt.drop.windowID;
                break;
            case SDL_QUIT:
                RequestCloseAll();
                break;
        }
        Window *target = winID >= 0 ? GetWindowByID(winID) : NULL;
        if(target)
            target->_forwardEvent(evt);
        else if(winID != -1)
        {
#ifdef _DEBUG
            logerror("SDL event dropped: type %u, window id %d", evt.type, winID);
#endif
        }
    }
}

void Window::_HandleKeyEvent(const SDL_KeyboardEvent& kev)
{
    const unsigned k = kev.keysym.scancode;
    assert(k < Countof(s_keyState));
    unsigned ks = KeyState(0);
    if(kev.state)
        ks |= (KDOWN | KPRESSED);
    else
        ks |= KRELEASED;
    if(kev.repeat)
        ks |= KREPEAT;

    s_keyState[k] = (KeyState)ks;
}

Window::KeyState Window::GetKeyState(unsigned k)
{
    assert(k < Countof(s_keyState));
    return k < Countof(s_keyState) ? s_keyState[k] : KeyState(0);
}

unsigned Window::GetKeyCode(const char *kname)
{
    return kname ? SDL_GetScancodeFromName(kname) : SDL_SCANCODE_UNKNOWN;
}

const char *Window::GetKeyName(unsigned k)
{
    return k < SDL_NUM_SCANCODES ? SDL_GetScancodeName((SDL_Scancode)k) : NULL;
}

void Window::_forwardEvent(const SDL_Event& evt)
{
    MakeCurrent c(this);

    switch(evt.type)
    {
        case SDL_WINDOWEVENT:
            switch(evt.window.event)
            {
                case SDL_WINDOWEVENT_CLOSE:
                    requestClose();
                    break;
            }
        break;

        case SDL_KEYDOWN:
            switch(evt.key.keysym.sym)
            {
                case SDLK_RETURN:
                    if(evt.key.keysym.mod & KMOD_ALT)
                        open(0, 0, !_full);
                    break;
            }
        break;
    }
    _pushPendingEvent(evt);
}

void Window::_pushPendingEvent(const SDL_Event& evt)
{
    const unsigned n = numPendingEvents;
    const unsigned cap = pendingEventsCapacity;
    if(n >= cap)
    {
        void *newp = realloc(pendingEvents, sizeof(SDL_Event) * (cap + 32));
        assert(newp);
        if(!newp)
            return;
        pendingEvents = newp;
        pendingEventsCapacity = cap + 32;
    }
    PENDING[n] = evt;
    numPendingEvents = n+1;
}

void Window::_handleEvent(const SDL_Event &evt)
{
    ImGui_ImplSdl_ProcessEvent(&evt);

    // ------ Only with listener from here --------
    const size_t sz = _ev.size();
    if(!sz)
        return;
    switch(evt.type)
    {
        case SDL_KEYDOWN:
        case SDL_KEYUP:
            for(size_t i = 0; i < sz; ++i)
                _ev[i]->keyEvent(evt.key.keysym.scancode, evt.key.keysym.mod, evt.key.state);
            break;

        case SDL_WINDOWEVENT:
            switch(evt.window.event)
            {
                case SDL_WINDOWEVENT_FOCUS_GAINED:
                    for(size_t i = 0; i < sz; ++i)
                        _ev[i]->windowEvent(WEV_FOCUS);
                    break;
                case SDL_WINDOWEVENT_FOCUS_LOST:
                    for(size_t i = 0; i < sz; ++i)
                        _ev[i]->windowEvent(WEV_LOSTFOCUS);
                    break;
                case SDL_WINDOWEVENT_SIZE_CHANGED:
                    for(size_t i = 0; i < sz; ++i)
                        _ev[i]->windowResize(evt.window.data1, evt.window.data2);
                    break;
            }
            break;

        case SDL_MOUSEMOTION:
            for(size_t i = 0; i < sz; ++i)
                _ev[i]->mouseMoved(evt.motion.x, evt.motion.y, evt.motion.xrel, evt.motion.yrel, evt.motion.state);
            break;

        case SDL_MOUSEBUTTONDOWN:
        case SDL_MOUSEBUTTONUP:
            for(size_t i = 0; i < sz; ++i)
                _ev[i]->mouseClick(evt.button.button, evt.button.state, evt.button.x, evt.button.y);
            break;

        case SDL_MOUSEWHEEL:
            for(size_t i = 0; i < sz; ++i)
                _ev[i]->mouseWheel(evt.wheel.y);
            break;
    }
}


bool Window::isMouseButton(unsigned int btn)
{
    unsigned b = SDL_GetMouseState(NULL, NULL);
    return !!(b & SDL_BUTTON(btn));
}

bool Window::isKey(int k) const
{
    int numkeys = 0;
    const unsigned char *kb = SDL_GetKeyboardState(&numkeys);
    return k < numkeys && !!kb[k];
}

bool Window::update(float dt)
{
    MakeCurrent c(this);

    ImGui_ImplSdl_NewFrame(window);

    onUpdate(dt);

    render->beginFrame();
    render->clear();

    // process events just before we are about to render,
    // since imgui does not cope well with multiple windows,
    // and event handlers may invoke arbitrary functionality, including imgui draws.
    if(const unsigned npend = numPendingEvents)
    {
        //printf("Window[%p] events: %u\n", window, npend);
        numPendingEvents = 0;
        for(unsigned i = 0; i < npend; ++i)
            _handleEvent(PENDING[i]);
    }

    for(size_t i = 0; i < _drawHooks.size(); ++i)
        _drawHooks[i].fn(this, _drawHooks[i].data);

    ImGui::Render();

    if(render->endFrame())
    {
        render->show();
        return true;
    }

    // ---------------------------------------------

    // Renderer encountered a serious error, try to deal with it:
    // First, ditch the context, it might still be in the process of
    // resetting, which may take an undefined amount of time.
    render->killContext();
    return false;
}

void Window::addDrawHook(Window::DrawHookFn fn, void *ud)
{
    DrawHook h = { fn, ud };
    _drawHooks.push_back(h);
}

void Window::getSize(int *w, int *h) const
{
    if(window)
        SDL_GetWindowSize(window, w, h);
    else
    {
        if(w)
            *w = 0;
        if(h)
            *h = 0;
    }
}

void Window::setSize(int w, int h)
{
    if(window)
        SDL_SetWindowSize(window, w, h);
}

void Window::setTitle(const char *s)
{
    if(window)
        SDL_SetWindowTitle(window, s);
}

void Window::placeMouse(int x, int y)
{
    SDL_WarpMouseInWindow(window, x, y);
}

void Window::_backupStatusAndDestroy()
{
    assert(window);
    SDL_GetWindowPosition(window, &statusBackup.x, &statusBackup.y);
    SDL_GetWindowSize(window, &statusBackup.w, &statusBackup.h);
    statusBackup.flags = SDL_GetWindowFlags(window);
    statusBackup.title = SDL_GetWindowTitle(window);
    _deleteRenderer();
    SDL_DestroyWindow(window);
    _winID = WIN_ID_UNREGISTERED;
    window = NULL;
}

bool Window::_recreateFromBackup()
{
    assert(!window && !render);
    window = SDL_CreateWindow(statusBackup.title.c_str(),
        statusBackup.x, statusBackup.y,
        statusBackup.w, statusBackup.h,
        statusBackup.flags);
    if(!window)
        return false;

    _winID = SDL_GetWindowID(window);

    if(!_initRenderer())
        return false;

    render->resize();
    return true;
}

bool Window::StaticInit()
{
    memset(s_keyState, 0, sizeof(s_keyState));

    return true;
}

#include "scriptrender.h"
#include "log.h"
#include <imgui/imgui.h>
#include "framebuffer.h"

#include "luainternal.h"

ScriptedRenderObject::ScriptedRenderObject()
{
}

ScriptedRenderObject::~ScriptedRenderObject()
{
}

void ScriptedRenderObject::onRender(const glm::mat4 & proj) const
{
    LuaStackCheck(_L, 0);
    ScriptedRenderObject *self = const_cast<ScriptedRenderObject*>(this); // HACK
    CallEnv env(self, "onRender");
    if(env)
    {
        //ImGui::PushID(this);
        glm::mat4 m = proj;
        self->pushraw(glm::value_ptr(m));
        if(!env.call(0))
        {
            // Better to warn about this explicitly, so there are no hidden surprises when nothing shows up
            logerror("ScriptedRenderObject::onRender() failed");
        }
        //ImGui::PopID();
    }
}

void ScriptedRenderObject::onDeviceReset(bool init)
{
    CallEnv env(this, "onDeviceReset");
    if(env)
    {
        push(init);
        env.call(0);
    }
}

#include "scene.h"
#include "renderobject.h"
#include "camera.h"

const ScriptDef Scene::scriptdef("scene", ScriptTypes::SCENE);

Scene::Scene()
{
}

Scene::~Scene()
{
}

size_t Scene::render(const glm::mat4& m) const
{
    size_t c = 0;
    for(size_t pass = 0; pass < _objs.size(); ++pass)
    {
         const ObjStore& oo = _objs[pass];
         for(size_t i = 0; i < oo.size(); ++i)
             oo[i]->render(m);
         c += oo.size();
    }
    return c;
}

void Scene::addPass(RenderObject *obj, unsigned pass)
{
    assert(obj);
    if(_objs.size() <= pass)
        _objs.resize(pass+1);
    _objs[pass].push_back(obj);
}

// horribly not efficient at all :<
unsigned Scene::remove(const RenderObject *obj)
{
    unsigned c = 0;
    for(size_t pass = 0; pass < _objs.size(); ++pass)
    {
         ObjStore& oo = _objs[pass];
         for(size_t i = 0; i < oo.size(); ++i)
         {
             const RenderObject *ro = oo[i].content();
             if(ro == obj)
             {
                 ++c;
                 if(ro != oo.back().content())
                     oo[i] = oo.back();
                 oo.pop_back();
             }
         }
    }
    return c;
}

unsigned Scene::removePass(unsigned pass)
{
    unsigned c = 0;
    if(pass < _objs.size())
    {
        ObjStore& oo = _objs[pass];
        c = (unsigned)oo.size();
        oo.clear();
    }
    return c;
}

unsigned Scene::removeAll()
{
    unsigned c = 0;
    for(size_t pass = 0; pass < _objs.size(); ++pass)
    {
         ObjStore& oo = _objs[pass];
         c += (unsigned)oo.size();
         oo.clear();
    }
    return c;
}

bool Scene::contains(RenderObject * obj) const
{
    for(size_t pass = 0; pass < _objs.size(); ++pass)
    {
         const ObjStore& oo = _objs[pass];
         for(size_t i = 0; i < oo.size(); ++i)
         {
             const RenderObject *ro = oo[i].content();
             if(ro == obj)
                 return true;
         }
    }
    return false;
}

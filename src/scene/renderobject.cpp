#include "renderobject.h"
#include "renderer.h"
#include <algorithm>

const ScriptDef RenderObject::scriptdef("renderobject", ScriptTypes::RENDEROBJECT);

RenderObject::RenderObject()
:  DeviceObjectBase(100), visible(true), localVisible(true), blend(BlendMode::DONT_CHANGE), scale(1), localScale(1), rotation(1)
{
}

RenderObject::~RenderObject()
{
}

void RenderObject::onDeviceReset(bool init)
{
    // nothing to do by default
}

glm::mat4 RenderObject::transformMatrix(const glm::mat4 & m) const
{
    return glm::translate(m, position) * glm::scale(rotation, scale);
}

glm::mat4 RenderObject::transformLocal(const glm::mat4 & m) const
{
    return glm::scale(m, localScale);
}

void RenderObject::render(const glm::mat4& m) const
{
    if(visible)
    {
        BlendMode::Scope blm(blend);
        const glm::mat4 proj = transformMatrix(m);
        if(localVisible)
            onRender(transformLocal(proj));
        for(size_t i = 0; i < _children.size(); ++i)
            _children[i]->render(proj);
    }
}

void RenderObject::removeChild(const RenderObject *ro)
{
    Children::const_iterator it = std::remove(_children.begin(), _children.end(), ro);
    _children.resize(it - _children.begin());
}


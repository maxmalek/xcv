#pragma once

#include "shader.h"
#include "glmx.h"
#include <vector>
#include "luaobj.h"
#include "blendmode.h"

class Renderer;


class RenderObject : public ScriptableT<RenderObject>, protected DeviceObjectBase
{
public:
    static const ScriptDef scriptdef;

    void render(const glm::mat4& m) const;
    void addChild(RenderObject *ro) { _children.push_back(ro); }
    void addChild(const CountedPtr<RenderObject>& ro) { _children.push_back(ro); }
    void removeChild(const RenderObject *ro);
    glm::mat4 transformMatrix(const glm::mat4& m) const; // transforms applied to children
    glm::mat4 transformLocal(const glm::mat4& m) const; // only transforms specifically for this applies to matrix

    bool visible;
    bool localVisible;
    BlendMode::Enum blend;

    glm::vec3 position; // in world space
    glm::vec3 scale;    // in object space
    glm::vec3 localScale;   // in object space; does not affect children
    glm::mat4 rotation; // in object space

protected:
    virtual void onRender(const glm::mat4& proj) const = 0;
    virtual void onDeviceReset(bool init);

    RenderObject();
    virtual ~RenderObject();

    typedef std::vector<CountedPtr<RenderObject> > Children;
    Children _children;

};

typedef CountedPtr<RenderObject> RenderObjRef;
typedef CountedPtr<const RenderObject> RenderObjCRef;

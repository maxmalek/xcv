#pragma once

#include "luaobj.h"
#include "view.h"

enum ProjectionMode
{
    PROJ_PERSPECTIVE,
    PROJ_ORTHOGONAL,
};


class Camera : public ScriptableT<Camera>
{
public:
    static const ScriptDef scriptdef;

    Camera();
    void pos(const glm::vec3& pos);
    void lookAt(const glm::vec3& at);
    void setViewport(const glm::uvec2& dim);
    void setProjection(ProjectionMode p);
    ProjectionMode getProjection() const { return projmode; }

    const glm::vec3& getPos() const;
    const glm::mat4& getMatrixCenter() const { return mcenter; }
          glm::mat4& getMatrixCenter()       { return mcenter; }
    const glm::mat4& getMatrixLeft() const { return mleft; }
          glm::mat4& getMatrixLeft()       { return mleft; }
    const glm::mat4& getMatrixRight() const { return mright; }
          glm::mat4& getMatrixRight()       { return mright; }
    float getAspect() const { return aspect; }

    void setFov(float f);
    void setEyeDistance(float d);
    float getFov() const { return fov; }
    float getEyeDistance() const { return eyeDistance; }

    const glm::vec3& getForward() const { return v.getForward(); }
    const glm::vec3& getUp() const { return v.getUp(); }
    const glm::vec3& getRight() const { return v.getRight(); }

    void setUp(const glm::vec3& up);


protected:
    void _updateProjection();
    void _updateMatrix();

    glm::mat4 mcenter, mleft, mright;
    View  v;
    float aspect;
    float fov;
    float eyeDistance;
    ProjectionMode projmode;
    glm::mat4 projection;
    glm::vec3 lookat, up;
};


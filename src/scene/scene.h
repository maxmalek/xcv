#pragma once

#include <vector>
#include "glmx.h"
#include "renderobject.h"
#include "luaobj.h"

class Camera;

class Scene : public ScriptableT<Scene>
{
public:
    static const ScriptDef scriptdef;

    Scene();
    virtual ~Scene();

    size_t render(const glm::mat4& m) const;
    void addPass(RenderObject *obj, unsigned pass);
    unsigned remove(const RenderObject *obj);
    unsigned removePass(unsigned pass);
    unsigned removeAll();
    bool contains(RenderObject *obj) const;

    template<typename F>
    void each(F& f, void *ud = 0)
    {
        for(size_t k = 0; k < _objs.size(); ++k)
        {
            ObjStore& oo = _objs[k];
            for(size_t i = 0; i < oo.size(); ++i)
                f(oo[i].content(), ud);
        }
    }


protected:
    typedef std::vector<RenderObjRef> ObjStore;
    std::vector<ObjStore> _objs;
};

typedef CountedPtr<Scene> SceneRef;

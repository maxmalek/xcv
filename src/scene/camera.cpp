#include "camera.h"
#include <assert.h>

const ScriptDef Camera::scriptdef("camera", ScriptTypes::CAMERA);

Camera::Camera()
: v(glm::vec3(0,0,3), glm::vec3(0,0,0), glm::vec3(0,-1,0))
, aspect(1)
, fov(1)
, eyeDistance(1)
, projmode(PROJ_PERSPECTIVE)
{
    // lookat is (0,0,0)
    up = v.getUp();
    _updateMatrix();
}

void Camera::pos(const glm::vec3& pos)
{
    v.setOrigin(pos);
    _updateMatrix();
}

void Camera::lookAt(const glm::vec3& at)
{
    lookat = at;
    v.setLookAt(at, up);
    _updateMatrix();
}

void Camera::setUp(const glm::vec3 & up)
{
    this->up = glm::normalize(up);
    v.setLookAt(lookat, up);
    _updateMatrix();
}

void Camera::setViewport(const glm::uvec2& dim)
{
    aspect = float(dim.x) / float(dim.y);
    _updateProjection();
}

const glm::vec3& Camera::getPos() const
{
    return v.getOrigin();
}

void Camera::setFov(float f)
{
    fov = f;
    _updateProjection();
}

void Camera::setEyeDistance(float d)
{
    eyeDistance = d;
    _updateMatrix();
}

void Camera::setProjection(ProjectionMode p)
{
    projmode = p;
    _updateProjection();
}

void Camera::_updateProjection()
{
    switch(projmode)
    {
        case PROJ_PERSPECTIVE:
            projection = glm::tweakedInfinitePerspective(fov, aspect, 0.001f);
            break;

        case PROJ_ORTHOGONAL:
            projection = glm::ortho(0.0f, 1.0f, 1.0f, 0.0f);
            break;

        default:
            assert(false);
    }
    _updateMatrix();
}

void Camera::_updateMatrix()
{
    const glm::vec3 o = v.getOrigin();
    const glm::vec3 d = v.getRight() * eyeDistance;
    const glm::vec3 f = v.getForward();
    const glm::vec3 u = v.getUp();
    mleft   = projection * glm::lookAt(o - d, f, u);
    mcenter = projection * glm::lookAt(o    , f, u);
    mright  = projection * glm::lookAt(o + d, f, u);
}

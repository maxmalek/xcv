#pragma once

#include "renderobject.h"

class ScriptedRenderObject : public RenderObject
{
public:
    ScriptedRenderObject();
    virtual ~ScriptedRenderObject();
protected:
    virtual void onRender(const glm::mat4& proj) const;
    virtual void onDeviceReset(bool init);
};

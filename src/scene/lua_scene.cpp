#include "lua_scene.h"
#include "luainternal.h"
#include "scene.h"
#include "scriptrender.h"
#include "camera.h"

#define v3(i) lua_tovec3(L, i)
#define v3o(i) lua_optvec3(L, i)
#define vu2(i) lua_touvec2(L, i)
#define F(i) lua_checkfloat(L, i)

inline static RenderObject *RO(lua_State *L, int idx)
{
    return luaGetObj<RenderObject>(L, idx);
}

struct LuaScene : public LuaClassBind<Scene>
{
    static const luaL_Reg methods[];

    luaFunc(new) { luaReturnObj(new Scene); }
    luaFunc(add) { This(L)->addPass(RO(L, 2), lua_toint(L, 3)); luaReturnSelf(); }
    luaFunc(remove) { luaReturnInt(This(L)->remove(RO(L, 2))); }
    luaFunc(removePass) { luaReturnInt(This(L)->removePass(lua_checkint(L, 2))); }
    luaFunc(removeAll) { luaReturnInt(This(L)->removeAll()); }
    luaFunc(render) { luaReturnInt(This(L)->render(lua_getT<glm::mat4>(L, 2))); }
    luaFunc(contains) { luaReturnBool(This(L)->contains(RO(L, 2))); }
};
const luaL_Reg LuaScene::methods[] =
{
    luaRegisterNewDelete(LuaScene),
    luaRegisterMethod(LuaScene, add),
    luaRegisterMethod(LuaScene, remove),
    luaRegisterMethod(LuaScene, removePass),
    luaRegisterMethod(LuaScene, removeAll),
    luaRegisterMethod(LuaScene, render),
    luaRegisterMethod(LuaScene, contains),
};

struct LuaRenderObject : public LuaClassBind<RenderObject>
{
    static const luaL_Reg methods[];

    luaFunc(new)
    {
        luaReturnObj(new ScriptedRenderObject);
    }

    luaFunc(setRotationMatrix)
    {
        This(L)->rotation = lua_getT<glm::mat4>(L, 2);
        luaReturnSelf();
    }

    luaFunc(setPos) { This(L)->position = lua_getT<glm::vec3>(L, 2); luaReturnSelf(); }
    luaFunc(getPos) { luaReturnT(This(L)->position); }

    luaFunc(setScale) { This(L)->scale = lua_getT<glm::vec3>(L, 2); luaReturnSelf(); }
    luaFunc(getScale) { luaReturnT(This(L)->scale); }

    luaFunc(setLocalScale) { This(L)->localScale = lua_getT<glm::vec3>(L, 2); luaReturnSelf(); }
    luaFunc(getLocalScale) { luaReturnT(This(L)->localScale); }

    luaFunc(setVisible) { This(L)->visible = getBool(L, 2); luaReturnSelf(); }
    luaFunc(getVisible) { luaReturnT(This(L)->visible); }

    luaFunc(setLocalVisible) { This(L)->localVisible = getBool(L, 2); luaReturnSelf(); }
    luaFunc(getLocalVisible) { luaReturnT(This(L)->localVisible); }

    luaFunc(addChild)
    {
        This(L)->addChild(RO(L, 2));
        luaReturnSelf();
    }

    luaFunc(removeChild)
    {
        This(L)->removeChild(RO(L, 2));
        luaReturnSelf();
    }

    luaFunc(setBlend)
    {
        This(L)->blend = (BlendMode::Enum)lua_checkuint(L, 2);
        luaReturnSelf();
    }

    luaFunc(getBlend)
    {
        luaReturnInt(This(L)->blend);
    }

    luaFunc(render)
    {
        This(L)->render(lua_getT<glm::mat4>(L, 2));
        luaReturnSelf();
    }

};
const luaL_Reg LuaRenderObject::methods[] =
{
    luaRegisterNewDelete(LuaRenderObject),
    luaRegisterMethod(LuaRenderObject, setRotationMatrix),
    luaRegisterMethod(LuaRenderObject, setPos),
    luaRegisterMethod(LuaRenderObject, getPos),
    luaRegisterMethod(LuaRenderObject, setScale),
    luaRegisterMethod(LuaRenderObject, getScale),
    luaRegisterMethod(LuaRenderObject, setVisible),
    luaRegisterMethod(LuaRenderObject, getVisible),
    luaRegisterMethod(LuaRenderObject, setLocalScale),
    luaRegisterMethod(LuaRenderObject, getLocalScale),
    luaRegisterMethod(LuaRenderObject, setLocalVisible),
    luaRegisterMethod(LuaRenderObject, getLocalVisible),
    luaRegisterMethod(LuaRenderObject, addChild),
    luaRegisterMethod(LuaRenderObject, removeChild),
    luaRegisterMethod(LuaRenderObject, setBlend),
    luaRegisterMethod(LuaRenderObject, getBlend),
    luaRegisterMethod(LuaRenderObject, render),
};

struct LuaCamera : public LuaClassBind<Camera>
{
    static const luaL_Reg methods[];

    luaFunc(new) { luaReturnObj(new Camera); }
    luaFunc(setpos) { This(L)->pos(v3(2)); luaReturnSelf(); }
    luaFunc(lookat) { This(L)->lookAt(v3(2)); luaReturnSelf(); }
    luaFunc(viewport) { This(L)->setViewport(vu2(2)); luaReturnSelf(); }
    luaFunc(setfov) { This(L)->setFov(F(2)); luaReturnSelf(); }
    luaFunc(setup) { This(L)->setUp(v3(2)); luaReturnSelf(); }
    luaFunc(seteyedist) { This(L)->setEyeDistance(F(2)); luaReturnSelf(); }
    luaFunc(getforward) { luaReturnT(This(L)->getForward()); }
    luaFunc(getup) { luaReturnT(This(L)->getUp()); }
    luaFunc(getright) { luaReturnT(This(L)->getRight()); }
    luaFunc(getpos) { luaReturnT(This(L)->getPos()); }
    luaFunc(getaspect) { luaReturnT(This(L)->getAspect()); }
    luaFunc(geteyedist) { luaReturnT(This(L)->getEyeDistance()); }
    luaFunc(getmatrix) { luaReturnT(This(L)->getMatrixCenter()); }
    luaFunc(getmatrixptr) { lua_pushlightuserdata(L, &This(L)->getMatrixCenter()); return 1; }
    luaFunc(getleftmatrix) { luaReturnT(This(L)->getMatrixLeft()); }
    luaFunc(getleftmatrixptr) { lua_pushlightuserdata(L, &This(L)->getMatrixLeft()); return 1; }
    luaFunc(getrightmatrix) { luaReturnT(This(L)->getMatrixRight()); }
    luaFunc(getrightmatrixptr) { lua_pushlightuserdata(L, &This(L)->getMatrixRight()); return 1; }
};
const luaL_Reg LuaCamera::methods[] =
{
    luaRegisterNewDelete(LuaCamera),
    luaRegisterMethod(LuaCamera, setpos),
    luaRegisterMethod(LuaCamera, lookat),
    luaRegisterMethod(LuaCamera, viewport),
    luaRegisterMethod(LuaCamera, setfov),
    luaRegisterMethod(LuaCamera, setup),
    luaRegisterMethod(LuaCamera, seteyedist),
    luaRegisterMethod(LuaCamera, getforward),
    luaRegisterMethod(LuaCamera, getup),
    luaRegisterMethod(LuaCamera, getright),
    luaRegisterMethod(LuaCamera, getpos),
    luaRegisterMethod(LuaCamera, getaspect),
    luaRegisterMethod(LuaCamera, geteyedist),
    luaRegisterMethod(LuaCamera, getmatrix),
    luaRegisterMethod(LuaCamera, getmatrixptr),
    luaRegisterMethod(LuaCamera, getleftmatrix),
    luaRegisterMethod(LuaCamera, getleftmatrixptr),
    luaRegisterMethod(LuaCamera, getrightmatrix),
    luaRegisterMethod(LuaCamera, getrightmatrixptr)
};


static const luaL_Reg scenelib[] =
{
    {NULL, NULL}
};

int register_lua_scene(lua_State *L)
{
    LuaStackCheck(L, 1);

    luaL_newlib(L, scenelib);
    luaRegisterClassInLib<LuaScene>(L, -1);
    luaRegisterClassInLib<LuaRenderObject>(L, -1);
    luaRegisterClassInLib<LuaCamera>(L, -1);

    return 1;
}

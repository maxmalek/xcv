#include "platform.h"
#include "macros.h"
#include "util.h"
#include <SDL.h>
#include <string.h>
#include "pmalloca.h"
#include "log.h"

#include <sys/stat.h>
#include <sys/types.h>

#ifdef _WIN32
#  include <intrin.h>
#  include <Windows.h>
#  define stat64 _stat64 // Both struct and function
#elif defined(__unix__)
#  include <sys/wait.h>
#  include <unistd.h>
#endif

#include <string>
#include "nfd.h"


#ifdef _WIN32
    #define NEED_NORMALIZE
    const char platform::pathsep = '\\';
#else
    const char platform::pathsep = '/';
#endif

static const SDL_MessageBoxButtonData bYesNo[] =
{
    { SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT, 1, "No" },
    { SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT, 0, "Yes" }
};


void platform::msgbox(const char * msg, const char * title)
{
#ifdef _WIN32
    // Win32 native version is just more comfy
    MessageBox(0, msg, title, MB_OK);
#else
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_WARNING, title, msg, NULL);
#endif

}

bool platform::msgboxYesNo(const char * msg, const char * title)
{
    SDL_MessageBoxData mb;
    mb.flags = SDL_MESSAGEBOX_INFORMATION;
    mb.message = msg;
    mb.title = title ? title : "Question";
    mb.buttons = bYesNo;
    mb.numbuttons = Countof(bYesNo);
    mb.colorScheme = NULL;
    mb.window = NULL;
    int btnid = -1;
    int ret = SDL_ShowMessageBox(&mb, &btnid);
    return btnid == 0;
}

void platform::breakpoint()
{
    SDL_TriggerBreakpoint();
}

unsigned platform::getCPUCount()
{
    return SDL_GetCPUCount();
}

bool platform::fileExists(const char * fn)
{
    struct stat st;
    return !stat(fn, &st);
}

bool platform::fileExists(const char * fn, uint64_t *psize)
{
    struct stat64 st;
    if(!stat64(fn, &st))
    {
        if(psize)
            *psize = st.st_size;
        return true;
    }
    return false;
}

uint64_t platform::fileSize(const char * fn)
{
    struct stat64 st;
    if(stat64(fn, &st))
        return 0;
    return st.st_size;
}

void platform::normalizePath(char * s)
{
    if(!s)
        return;
#ifdef NEED_NORMALIZE
    while(*s)
    {
        if(*s == pathsep)
            *s = '/';
        ++s;
    }
#endif
}


void platform::toOSPath(char * s)
{
    if(!s)
        return;
#ifdef NEED_NORMALIZE
    while(*s)
    {
        if(*s == '/')
            *s = pathsep;
        ++s;
    }
#endif
}

char * platform::openFileDialog(const char *filters, const char *where)
{
    nfdchar_t *outPath = NULL;
    char *tmp = NULL;
    if(where)
    {
        size_t n = strlen(where) + 1;
        tmp = (char*)_malloca(n);
        ASSUME(tmp);
        memcpy(tmp, where, n);
        toOSPath(tmp);
        where = tmp;
    }
    switch(NFD_OpenDialog(filters, where, &outPath))
    {
        case NFD_OKAY:
            normalizePath(outPath);
            break;

        default:
            msgbox(NFD_GetError(), "OpenFileDialog error");
        case NFD_CANCEL:
            ;
    }
    _freea(tmp);
    return outPath;
}

char * platform::saveFileDialog(const char *filters, const char *where)
{
    nfdchar_t *savePath = NULL;
    char *tmp = NULL;
    if(where)
    {
        size_t n = strlen(where) + 1;
        tmp = (char*)_malloca(n);
        ASSUME(tmp);
        memcpy(tmp, where, n);
        toOSPath(tmp);
        where = tmp;
    }
    switch(NFD_SaveDialog(filters, where, &savePath))
    {
        case NFD_OKAY:
            normalizePath(savePath);
            break;

        default:
            msgbox(NFD_GetError(), "SaveFileDialog error");
        case NFD_CANCEL:
            ;
    }
    _freea(tmp);
    return savePath;
}

char * platform::getClipboardText()
{
    char *ret = NULL;
    char *clip = SDL_GetClipboardText();
    if(clip)
    {
        if(size_t sz = strlen(clip))
        {
            ret = (char*)malloc(sz+1);
            memcpy(ret, clip, sz+1);
        }
        SDL_free(clip);
    }
    return ret;
}

bool platform::setClipboardText(const char *str)
{
    return SDL_SetClipboardText(str) != 0;
}

#if defined(__unix__) && !defined(__MACOSX__)
struct PassParam1
{
    const char *cmd;
    const char *param;
};

static void _execCB1(const void *p)
{
    const PassParam1 *pass = (const PassParam1*)p;
    execlp(pass->cmd, pass->cmd, pass->param, NULL);
    // only executed when execlp() fails
    _exit(errno);
}

static void _openUrlCB(const void *p)
{
    PassParam1 pass;
    pass.cmd = "xdg-open";
    pass.param = (const char*) p;
    _execCB1(&pass);
}

static bool execfunc_posix(void (*func)(const void *), const void *param)
{
    const pid_t pid = vfork();
    if(pid == -1)
    {
        logerror("platform::execfunc(): Failed to vfork()");
        return false;
    }
    if(!pid) // are we child?
    {
        func(param);
    }
    else
    {
        int wstatus = 0;
        if(waitpid(pid, &wstatus, 0) < 0)
        {
            logerror("platform::execfunc(): waitpid() failed");
            return false;
        }
        if(WIFEXITED(wstatus))
        {
            const int status = WEXITSTATUS(wstatus);
            if(status)
            {
                logerror("platform::execfunc(): got status %d", status);
                return false;
            }
        }
        else
        {
            logerror("platform::execfunc(): did not exit cleanly");
            return false;
        }
        return true;
    }
    assert(false); // unreachable (or callback didn't _exit())
    return false;
}
#endif


bool platform::openURL(const char *url)
{
#ifdef _WIN32
    HINSTANCE ret = ShellExecute(NULL, "open", url, NULL, NULL, SW_SHOWNORMAL);
    return (int)(uintptr_t)ret > 32;
#elif defined(__MACOSX__)
    CFStringRef str = CFStringCreateWithCString(0, url, 0);
    CFURLRef ref = CFURLCreateWithString(kCFAllocatorDefault, str, NULL);
    int ret = LSOpenCFURLRef(ref, 0);
    CFRelease(ref);
    CFRelease(str);
    return ret == 0; // FIXME: Is this correct?
#elif defined(__unix__)
    return execfunc_posix(_openUrlCB, url);
#else
#error Unknown OS -- implement platform::openURL()
#endif
}

bool platform::openDefaultAssoc(const char * file)
{
    // Absolutely need to use backslashes on windows... sigh
    // Probably safer to do this for any OS we're on.
    const size_t filelen = strlen(file);
    char *filenorm = (char*)_malloca(filelen+1);
    if(!filenorm)
        return false;
    memcpy(filenorm, file, filelen+1); // incl \0
    toOSPath(filenorm);
    bool ok = openURL(filenorm);
    _freea(filenorm);
    return ok;
}

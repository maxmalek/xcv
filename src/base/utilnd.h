#pragma once

#include <stdint.h>
#include <assert.h>

namespace utilND {

template<typename I>
inline uint64_t totalsize(const I *sz, unsigned N)
{
    uint64_t a = 1;
    for(unsigned i = 0; i < N; ++i)
        a *= uint64_t(sz[i]);
    return a;
}

template<typename I, unsigned N>
inline uint64_t totalsize(const I (&sz)[N])
{
    return totalsize(sz, N);
}

// Distance from index p to index p+1 on a given axis
template<typename I>
inline uint64_t distanceToNextOnAxis(unsigned axis, const I *sz)
{
    uint64_t a = 1;
    for(unsigned i = 0; i < axis; ++i)
        a *= sz[i];
    return a;
}

template<typename I>
inline uint64_t index(const I *p, const I *sz, unsigned N)
{
    uint64_t a = 0;
    uint64_t aa = 1;
    for(unsigned i = 0; i < N; ++i)
    {
        /*uint64_t aa = 1;
        for(unsigned k = 0; k < i; ++k) // <-- loop is not entered in first iteration over i
            aa *= sz[k];*/
        //uint64_t aa = distanceToNextOnAxis<I,N>(i, sz);

        a += uint64_t(p[i]) * aa;
        aa *= sz[i]; // incremental distanceToNextOnAxis()
    }
    return a;
}

template<typename I, unsigned N>
inline uint64_t index(const I (&p)[N], const I (&sz)[N])
{
    return index(p, sz, N);
}

template<typename I, unsigned N>
inline uint64_t distanceToNextOnAxis(unsigned axis, const I (&sz)[N])
{
    assert(axis < N);
    return distanceToNextOnAxis(axis, &sz[0]);
}

}

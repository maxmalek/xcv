#pragma once

class RefcountedBase
{
public:
    typedef bool is_refcounted; // tag for some compile-time checks
protected:
    inline RefcountedBase() : _refcount(0) {}
    // refcount starts at 0, even if copy constructed
    inline RefcountedBase(const RefcountedBase&) : _refcount(0) {}
    virtual ~RefcountedBase();
    mutable unsigned _refcount;
};

enum RefcountType
{
    REFCOUNT_FAKE, // No refcounting performed, no automatic deletion, nothing
    REFCOUNT_NORMAL,
    REFCOUNT_ATOMIC // refcount inc/dec are performed atomically
};

// Explicitly specialized in cpp file
template<RefcountType> struct _Refcount;

template<RefcountType RT>
class Refcounted : private RefcountedBase
{
public:
    inline unsigned refcount() const { return _refcount; }
    void incref() const;
    void decref() const;
protected:
    virtual ~Refcounted() {}
};


template<typename T> class CountedPtr
{
public:
    typedef T value_type;

    inline ~CountedPtr()
    {
        if(_p)
            _p->decref();
    }
    inline CountedPtr() : _p(0)
    {}
    inline CountedPtr(T* p) : _p(p)
    {
        if(p)
            p->incref();
    }
    inline CountedPtr(const CountedPtr& ref) : _p(ref._p)
    {
        if (_p)
            _p->incref();
    }
    // intentionally not a reference -- see http://stackoverflow.com/questions/3279543/what-is-the-copy-and-swap-idiom
    CountedPtr& operator=(CountedPtr ref)
    {
        CountedPtr::swap(*this, ref);
        return *this;
    }
    CountedPtr& operator=(T *p)
    {
        if(p != _p)
        {
             if(p)
                p->incref();
             if(_p)
                 _p->decref();
             _p = p;
        }
        return *this;
    }

    T* operator->() const { return  _p; }
    T& operator* () const { return *_p; }

    bool operator!() const { return !_p; }

    // Safe for use in if statements
    operator const void*() const  { return _p; }

    template <typename U>
    const CountedPtr<U>& reinterpret() const { return *reinterpret_cast<const CountedPtr<U>*>(this); }

    template <typename U>
    CountedPtr<U>& reinterpret() { return *reinterpret_cast<CountedPtr<U>*>(this); }

    // if you use these, make sure you also keep a counted reference to the object!
    T* content () const { return _p; }

    bool operator<(const CountedPtr& ref) const { return _p < ref._p; }
    bool operator<=(const CountedPtr& ref) const { return _p <= ref._p; }
    bool operator==(const CountedPtr& ref) const { return _p == ref._p; }
    bool operator!=(const CountedPtr& ref) const { return _p != ref._p; }
    bool operator>=(const CountedPtr& ref) const { return _p >= ref._p; }
    bool operator>(const CountedPtr& ref) const { return _p > ref._p; }

    bool operator<(const T *p) const { return _p < p; }
    bool operator<=(const T *p) const { return _p <= p; }
    bool operator==(const T *p) const { return _p == p; }
    bool operator!=(const T *p) const { return _p != p; }
    bool operator>=(const T *p) const { return _p >= p; }
    bool operator>(const T *p) const { return _p > p; }

    inline static void swap(CountedPtr& a, CountedPtr& b)
    {
        T *tmp = a._p;
        a._p = b._p;
        b._p = tmp;
    }

    inline void swap(CountedPtr& o)
    {
        swap(*this, o);
    }

    typedef bool is_counted_ptr; // tag for some compile-time checks

private:

    T *_p;
};

typedef CountedPtr<Refcounted<REFCOUNT_NORMAL> > CountedPtrAny;
typedef CountedPtr<const Refcounted<REFCOUNT_NORMAL> > CountedCPtrAny;

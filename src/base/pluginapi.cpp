#include "pluginapi.h"

#include <set> // The lazy way

namespace pluginapi {

const PluginApiImportFuncs *exports = NULL; // Assigned externally (by api_export)
lua_State *lua = NULL; // Assigned externally (by Runtime)
FuncList funcs;
std::string fileExtSave;
std::string fileExtLoad;
const char SEP = ',';

static void _split(const std::string &src, std::set<std::string>& set)
{
    std::string s;
    for (size_t i = 0; i < src.length(); ++i)
    {
        const char c = src[i];
        if (c == SEP || c == ' ' || c == '\t' || c == ',' || c == ';' || c == '\n' || c == ':')
        {
            if (s.length())
                set.insert(s);
            s.clear();
        }
        else
            s += c;
    }
    if (s.length())
        set.insert(s);
}


static void _fixup(std::string& s)
{
    std::set<std::string> set;
    _split(s, set);
    s.clear();
    set.insert("b3d"); // Core always reads+writes this format
    for(std::set<std::string>::iterator it = set.begin(); it != set.end(); ++it)
    {
        s += *it;
        s += SEP;
    }
    if(s.length())
        s.pop_back();
}

void _fixupExt()
{
    _fixup(fileExtLoad);
    _fixup(fileExtSave);
}



}

#pragma once

#include <stddef.h>

namespace soa {


// T = underlying basic type
// TUPLE_SIZE = how many Ts are packed together per tuple
// SKIP = additional space between each tuple
template<typename T, size_t TUPLE_SIZE, size_t SKIP>
struct Shuffler
{
    static const size_t skip = SKIP;
    static const size_t tuple_size = TUPLE_SIZE;
    typedef T value_type;

    // (x,y,z)(x,y,z) -> (x,x)(y,y)(z,z)
    static void aos2soa(T *dst, const T *src, size_t nTuples)
    {
        T *pdst[TUPLE_SIZE];
        for(size_t i = 0; i < TUPLE_SIZE; ++i)
            pdst[i] = dst + (i * nTuples);
        for(size_t e = 0; e < nTuples; ++e)
        {
            for(size_t i = 0; i < TUPLE_SIZE; ++i)
                *pdst[i]++ = *src++;
            src += SKIP;
        }
    }

    // (x,x)(y,y)(z,z) -> (x,y,z)(x,y,z) 
    static void soa2aos(T *dst, const T *src, size_t nTuples)
    {
        T *psrc[TUPLE_SIZE];
        for(size_t i = 0; i < TUPLE_SIZE; ++i)
            psrc[i] = src + (i * nTuples);
        for(size_t e = 0; e < nTuples; ++e)
        {
            for(size_t i = 0; i < TUPLE_SIZE; ++i)
                *dst++ = *psrc[i]++;
            dst += SKIP;
        }
    }
};


} // end namespace soa

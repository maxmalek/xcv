#include "meshdata.h"
#include <assert.h>

static float quadVerts[] =
{
    -1, -1,
     1,  -1,
     -1, 1,
     -1, 1,
     1,  -1,
     1,  1,
};

static float quadUVs[] =
{
    0, 0,
    1, 0,
    0, 1,
    0, 1,
    1, 0,
    1, 1
};

 static const float cubeVerts[] =
 {
     -1.0f,-1.0f,-1.0f, // triangle 1 : begin
     -1.0f,-1.0f, 1.0f,
     -1.0f, 1.0f, 1.0f, // triangle 1 : end
     1.0f, 1.0f,-1.0f, // triangle 2 : begin
     -1.0f,-1.0f,-1.0f,
     -1.0f, 1.0f,-1.0f, // triangle 2 : end
     1.0f,-1.0f, 1.0f,
     -1.0f,-1.0f,-1.0f,
     1.0f,-1.0f,-1.0f,
     1.0f, 1.0f,-1.0f,
     1.0f,-1.0f,-1.0f,
     -1.0f,-1.0f,-1.0f,
     -1.0f,-1.0f,-1.0f,
     -1.0f, 1.0f, 1.0f,
     -1.0f, 1.0f,-1.0f,
     1.0f,-1.0f, 1.0f,
     -1.0f,-1.0f, 1.0f,
     -1.0f,-1.0f,-1.0f,
     -1.0f, 1.0f, 1.0f,
     -1.0f,-1.0f, 1.0f,
     1.0f,-1.0f, 1.0f,
     1.0f, 1.0f, 1.0f,
     1.0f,-1.0f,-1.0f,
     1.0f, 1.0f,-1.0f,
     1.0f,-1.0f,-1.0f,
     1.0f, 1.0f, 1.0f,
     1.0f,-1.0f, 1.0f,
     1.0f, 1.0f, 1.0f,
     1.0f, 1.0f,-1.0f,
     -1.0f, 1.0f,-1.0f,
     1.0f, 1.0f, 1.0f,
     -1.0f, 1.0f,-1.0f,
     -1.0f, 1.0f, 1.0f,
     1.0f, 1.0f, 1.0f,
     -1.0f, 1.0f, 1.0f,
     1.0f,-1.0f, 1.0f
 };

static const float crosshairVerts[] =
{
    -1, 0, 0,
    1, 0, 0,
    0, -1, 0,
    0, 1, 0,
    0, 0, -1,
    0, 0, 1
};

MeshData::MeshData()
{
}

MeshData::~MeshData()
{
}

void MeshData::clear()
{
    v.clear();
    f.clear();
    vn.clear();
    uv.clear();
    colors.clear();
    multi.clear();
    userIdx.clear();
}

void MeshData::calcNormals()
{
    vn.resize(v.size());
    //std::vector<int> seen(v.size(), 0);
    for (size_t i = 0; i < f.size(); i += 3)
    {
        unsigned ia = f[i];
        unsigned ib = f[i+1];
        unsigned ic = f[i+2];
        glm::vec3 va = glm::vec3(v[ia]);
        glm::vec3 vb = glm::vec3(v[ib]);
        glm::vec3 vc = glm::vec3(v[ic]);
        glm::vec3 normal = glm::normalize(glm::cross(vb - va, vc - va));
        vn[ia] += normal;
        vn[ib] += normal;
        vn[ic] += normal;
    }

    for (size_t i = 0; i < vn.size(); ++i)
        vn[i] = glm::normalize(vn[i]);
}

MeshData *MeshData::createQuad()
{
    MeshData *m = new MeshData();
    for(unsigned i = 0; i < sizeof(quadVerts) / sizeof(float); i += 2)
        m->v.push_back(glm::vec4(quadVerts[i], quadVerts[i+1], 0, 1));
    for(unsigned i = 0; i < sizeof(quadUVs) / sizeof(float); i += 2)
        m->uv.push_back(glm::vec2(quadUVs[i], quadUVs[i+1]));

    //m->calcNormals();
    return m;
}

MeshData *MeshData::createCube()
{
    MeshData *m = new MeshData();
    for(unsigned i = 0; i < sizeof(cubeVerts) / sizeof(float); i += 3)
        m->v.push_back(glm::vec4(cubeVerts[i], cubeVerts[i+1], cubeVerts[i+2], 1));

    //m->calcNormals();
    return m;
}

MeshData *MeshData::createCrosshair()
{
    MeshData *m = new MeshData();
    for(unsigned i = 0; i < sizeof(crosshairVerts) / sizeof(float); i += 3)
        m->v.push_back(glm::vec4(crosshairVerts[i], crosshairVerts[i+1], crosshairVerts[i+2], 1));

    m->multi.push_back(MultiDraw(0, 2));
    m->multi.push_back(MultiDraw(2, 2));
    m->multi.push_back(MultiDraw(4, 2));

    return m;
}

#include "util.h"
#include "log.h"
#include <string.h>
#include <sstream>
#include <ctype.h>
#include "platform.h"


bool wildcardMatch(const char *str, const char *pattern)
{
    const char *cp = 0, *mp = 0;
    while(*str && *pattern != '*')
    {
        if(*pattern != *str && *pattern != '?')
            return false;
        ++pattern;
        ++str;
    }

    while(*str)
    {
        if(*pattern == '*')
        {
            if(!*++pattern)
                return true;
            mp = pattern;
            cp = str + 1;
        }
        else if(*pattern == *str || *pattern == '?')
        {
            ++pattern;
            ++str;
        }
        else
        {
            pattern = mp;
            str = cp++;
        }
    }

    while(*pattern == '*')
        ++pattern;

    return !*pattern;
}

static bool nocasecmp(char a, char b)
{
    return tolower(a) == tolower(b);
}

bool wildcardMatchNoCase(const char *str, const char *pattern)
{
    const char *cp = 0, *mp = 0;
    while(*str && *pattern != '*')
    {
        if(!nocasecmp(*pattern, *str) && *pattern != '?')
            return false;
        ++pattern;
        ++str;
    }

    while(*str)
    {
        if(*pattern == '*')
        {
            if(!*++pattern)
                return true;
            mp = pattern;
            cp = str + 1;
        }
        else if(nocasecmp(*pattern, *str) || *pattern == '?')
        {
            ++pattern;
            ++str;
        }
        else
        {
            pattern = mp;
            str = cp++;
        }
    }

    while(*pattern == '*')
        ++pattern;

    return !*pattern;
}

const char * findFileExtension(const char * fn)
{
    const char *dot = strrchr(fn, '.');
    if(dot && dot[1])
    {
        const char *slash = strrchr(fn, '/');
        if(!slash || slash < dot)
            return dot + 1;
    }
    return NULL;
}

void NORETURN _releaseAssertFail(const char * file, unsigned line, const char * cond, const char * msg)
{
    std::ostringstream os;
    const char * const fail = "ASSERTION FAILED!";
    os << file << ":" << line << ": " << cond;
    if(msg && *msg)
        os << std::endl << msg;
    std::string s = os.str();
    logerror("%s %s", fail, s.c_str());
    platform::msgbox(s.c_str(), fail);
    abort();
}

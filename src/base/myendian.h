#pragma once

#include <assert.h>

namespace endian {


static inline const unsigned char isLittle()
{
    union { unsigned short s; unsigned char c[2]; } endian;
    endian.s = 1;
    return endian.c[0];
}

template<unsigned B>
struct swapper
{
    static inline void swap(unsigned char *p)
    {
        unsigned char *start = p;
        unsigned char *end = p + B - 1u;

        for ( ; start < end; ++start, --end)
        {
            const unsigned char swap = *start;
            *start = *end;
            *end = swap;
        }
    }
};

template <> struct swapper<1>
{
    static inline void swap(unsigned char *) {};
};

template<typename T>
static inline T swap(T x)
{
    void *p = &x;
    swapper<sizeof(T)>::swap((unsigned char*)p);
    return x;
}

template<size_t N>
static inline void swaploop(unsigned char *p, size_t sz)
{
    for(size_t i = 0; i < sz; i += N, p += N)
        swapper<N>::swap(p);
}


static inline void memswap(unsigned char *p, size_t sz, unsigned elemSize)
{
    assert(sz % elemSize == 0);
    switch(elemSize)
    {
        case 1:
            break;
        case 2:
            swaploop<2>(p, sz);
            break;
        case 4:
            swaploop<4>(p, sz);
            break;
        case 8:
            swaploop<8>(p, sz);
            break;

        default:
            assert(false);
    }
}


} // end namespace endian

#pragma once

#include <stddef.h>
#include "macros.h"
#include <stdarg.h>

/*
#ifdef _MSC_VER
#  define I64FMT "%016I64X"
#  define I64FMTX "%I64u"
#elif defined(__GNUC__) || defined(__clang__)
#  define I64FMT "%016llX"
#  define I64FMTX "%llu"
#else
// Hope for the best...
#  error check this
#  define I64FMT "%ll"
#  define I64FMTX "%llu"
#endif
*/

// C++11 defines these at least
#define I64FMT "%tu"
#define I64FMTX "%tX"


// log level
namespace LL
{
    enum Enum
    {
        ERR_MSGBOX = -2,
        ERR = -1,
        NORMAL = 0,
        INFO = 1,
        DEBUG = 2,
        DEBUG2 = 3,

        _MIN = -2,
        _MAX = 3,
        _TOTAL = _MAX - _MIN + 1
    };
};

typedef void (*log_callback_func)(int level, const char *message, size_t len, void *);

void log_addLogCallback(log_callback_func f, void *user);
void log_removeLogCallback(log_callback_func f);
void vlogx(LL::Enum level, int nl, const char *fmt, va_list va);
void logx(LL::Enum level, int nl, const char *fmt, ...) ATTR_PRINTF(3);
void log(const char *fmt, ...) ATTR_PRINTF(1);
void logerror(const char *fmt, ...) ATTR_PRINTF(1);
void logerrorbox(const char *fmt, ...) ATTR_PRINTF(1);
void loginfo(const char *fmt, ...) ATTR_PRINTF(1);
void logdebug(const char *fmt, ...) ATTR_PRINTF(1);
void logdebug2(const char *fmt, ...) ATTR_PRINTF(1);

#if defined(_DEBUG) || !defined(NDEBUG)
#define DEBUG_LOG(...) do { logdebug2(__VA_ARGS__); } while(0)
#else
#define DEBUG_LOG(...) do {} while(0)
#endif

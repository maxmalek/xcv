// Internal functionality for plugin API handling

#pragma once

#include <vector>
#include <string>
#include "api_plugin.h"

namespace pluginapi {

struct FuncsPrio : public PluginAPIFuncs
{
    int priority;
    std::string pluginfile;

    inline static bool Sort(const FuncsPrio *a, const FuncsPrio *b)
    {
        return b->priority < a->priority; // higher priority wins
    }
};

extern const PluginApiImportFuncs *exports;
extern lua_State *lua;

typedef std::vector<const FuncsPrio*> FuncList;
extern FuncList funcs;

extern std::string fileExtSave;
extern std::string fileExtLoad;
extern const char SEP;

void _fixupExt();


}

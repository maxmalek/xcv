#include "mathfoo.h"
#include <math.h>

void createGaussKernel(float * kernel, int dim, float sigma)
{
    float *kcenter = kernel + dim;
    float sum = kcenter[0] = 1.f;
    const float m = -0.5f / (sigma * sigma);
    for (int x = 1; x <= dim; ++x)
    {
        float k = expf(float(x*x) * m);
        kcenter[-x] = k;
        kcenter[x] = k;
        sum += 2 * k;
    }
    // normalize
    const float n = 1.0f / sum;
    for(int i = 0; i < gaussKernelSize(dim); ++i)
        kernel[i] *= n;
}

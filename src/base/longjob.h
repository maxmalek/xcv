#pragma once

#include <vector>
#include <string>
#include "glmx.h"

enum LongJobResult
{
    LONGJOB_NOT_STARTED = -1,
    LONGJOB_RUNNING = 0,

    LONGJOB_DONE,
    LONGJOB_CANCELLED,
    LONGJOB_FAILED,
};

class SequentialLongJob
{
public:
    SequentialLongJob(const char *name) : _name(name),  _status(LONGJOB_NOT_STARTED) {}
    virtual ~SequentialLongJob() {}
    void cancel();
    LongJobResult run();
    inline const char *getName() const { return _name.c_str(); }
    inline LongJobResult getStatus() const { return _status; }
    virtual glm::vec4 getPercDone() const = 0;
    virtual void drawUI();
    virtual void onBegin(); // called on insert. default impl does nothing
    virtual void onFinish(); // called when nno longer running. default implementation deletes self

private:
    std::string _name;
    LongJobResult _status;

    virtual LongJobResult _run() = 0;
    virtual void _cancel() = 0;
};

class LongJobExecutor
{
public:
    void add(SequentialLongJob *);
    void update();
    const std::vector<SequentialLongJob*>& getJobs() const { return _jobs; }

protected:

    std::vector<SequentialLongJob*> _jobs;
};


extern LongJobExecutor g_mainLongExec;


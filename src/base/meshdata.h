#pragma once

#include <vector>
#include "glmx.h"
#include "refcounted.h"

class MeshData : public Refcounted<REFCOUNT_NORMAL>
{
public:
    static MeshData *createQuad();
    static MeshData *createCube();
    static MeshData *createCrosshair();

    MeshData();
    virtual ~MeshData();

    void calcNormals();
    void clear();

    typedef std::vector<glm::vec4> Vertices;
    typedef std::vector<glm::vec3> Normals;
    typedef std::vector<unsigned> Indices;
    typedef std::vector<glm::vec2> TexCoords;
    typedef std::vector<unsigned> UserIndices;

    Vertices v;
    Indices f;
    Normals vn;
    TexCoords uv;
    Vertices colors;
    UserIndices userIdx;

    struct MultiDraw
    {
        MultiDraw() {}
        MultiDraw(unsigned s, unsigned c) : start(s), count(c) {}
        unsigned start;
        unsigned count;
    };
    
    std::vector<MultiDraw> multi;
};

typedef CountedPtr<MeshData> MeshDataRef;



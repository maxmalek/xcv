#include "log.h"

#include <stddef.h>

#ifdef _WIN32
#include <windows.h>
#endif

#include <vector>
#include <stdarg.h>
#include <stdio.h>

#include "macros.h"
#include "util.h"

enum ConsoleColor
{
    NONE = -1,
    BLACK,
    RED,
    GREEN,
    BROWN,
    BLUE,
    MAGENTA,
    CYAN,
    GREY,
    YELLOW,
    LRED,
    LGREEN,
    LBLUE,
    LMAGENTA,
    LCYAN,
    WHITE,

    MAX_COLORS
};

struct LogCallback
{
    LogCallback(log_callback_func af, void *aud) : f(af), ud(aud) {}
    log_callback_func f;
    void *ud;
};

static std::vector<LogCallback> s_logCallbacks;

static void _log_docallback(const std::vector<LogCallback>& v, LL::Enum level, int nl, va_list va, const char *fmt)
{
    char buf[4*1024];
    int written = vsnprintf(buf, Countof(buf)-2, fmt, va);
    int len = Countof(buf)-2;
    if(written < len)
        len = written;
    if(nl)
        buf[len++] = '\n';

    if(len)
    {
        buf[len] = 0;
        for(size_t i = 0; i < v.size(); ++i)
            v[i].f(level, &buf[0], len, v[i].ud);
    }
}

static void _log_removecallback(std::vector<LogCallback>& v, log_callback_func f)
{
    for(size_t i = 0; i < v.size(); )
        if(v[i].f == f)
            v.erase(v.begin() + i);
        else
            ++i;
}

void log_addLogCallback(log_callback_func f, void *ud)
{
    s_logCallbacks.push_back(LogCallback(f, ud));
}

void log_removeLogCallback(log_callback_func f)
{
    _log_removecallback(s_logCallbacks, f);
}


static void _log_setcolor(bool stdout_stream, ConsoleColor color)
{
#ifdef _WIN32

    static WORD WinColorFG[MAX_COLORS] =
    {
        0,                                                  // BLACK
            FOREGROUND_RED,                                     // RED
            FOREGROUND_GREEN,                                   // GREEN
            FOREGROUND_RED | FOREGROUND_GREEN,                  // BROWN
            FOREGROUND_BLUE,                                    // BLUE
            FOREGROUND_RED |                    FOREGROUND_BLUE,// MAGENTA
            FOREGROUND_GREEN | FOREGROUND_BLUE,                 // CYAN
            FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE,// WHITE
            // YELLOW
            FOREGROUND_RED | FOREGROUND_GREEN |                   FOREGROUND_INTENSITY,
            // RED_BOLD
            FOREGROUND_RED |                                      FOREGROUND_INTENSITY,
            // GREEN_BOLD
            FOREGROUND_GREEN |                   FOREGROUND_INTENSITY,
            FOREGROUND_BLUE | FOREGROUND_INTENSITY,             // BLUE_BOLD
            // MAGENTA_BOLD
            FOREGROUND_RED |                    FOREGROUND_BLUE | FOREGROUND_INTENSITY,
            // CYAN_BOLD
            FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
            // WHITE_BOLD
            FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY
    };

    HANDLE hConsole = GetStdHandle(stdout_stream ? STD_OUTPUT_HANDLE : STD_ERROR_HANDLE );
    SetConsoleTextAttribute(hConsole, WinColorFG[color]);
#else

    enum ANSITextAttr
    {
        TA_NORMAL=0,
        TA_BOLD=1,
        TA_BLINK=5,
        TA_REVERSE=7
    };

    enum ANSIFgTextAttr
    {
        FG_BLACK=30, FG_RED,  FG_GREEN, FG_BROWN, FG_BLUE,
        FG_MAGENTA,  FG_CYAN, FG_WHITE, FG_YELLOW
    };

    enum ANSIBgTextAttr
    {
        BG_BLACK=40, BG_RED,  BG_GREEN, BG_BROWN, BG_BLUE,
        BG_MAGENTA,  BG_CYAN, BG_WHITE
    };

    static int UnixColorFG[MAX_COLORS] =
    {
        FG_BLACK,                                           // BLACK
            FG_RED,                                             // RED
            FG_GREEN,                                           // GREEN
            FG_BROWN,                                           // BROWN
            FG_BLUE,                                            // BLUE
            FG_MAGENTA,                                         // MAGENTA
            FG_CYAN,                                            // CYAN
            FG_WHITE,                                           // WHITE
            FG_YELLOW,                                          // YELLOW
            FG_RED,                                             // LRED
            FG_GREEN,                                           // LGREEN
            FG_BLUE,                                            // LBLUE
            FG_MAGENTA,                                         // LMAGENTA
            FG_CYAN,                                            // LCYAN
            FG_WHITE                                            // LWHITE
    };

    fprintf((stdout_stream? stdout : stderr), "\x1b[%d%sm",UnixColorFG[color],(color>=YELLOW&&color<MAX_COLORS ?";1":""));
#endif
}

static void _log_resetcolor(bool stdout_stream)
{
#ifdef _WIN32
    HANDLE hConsole = GetStdHandle(stdout_stream ? STD_OUTPUT_HANDLE : STD_ERROR_HANDLE );
    SetConsoleTextAttribute(hConsole, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED );
#else
    fprintf(( stdout_stream ? stdout : stderr ), "\x1b[0m");
#endif
}

static const ConsoleColor colorLUT[] =
{
    RED,    // ERR_MSGBOX
    LRED,   // ERR
    NONE,   // NORMAL
    LCYAN,  // INFO
    LBLUE,  // DEBUG
    MAGENTA, // DEBUG2
};

static void _valogcolor(ConsoleColor col, int nl, const char *fmt, va_list va)
{
    if(col != NONE)
        _log_setcolor(true, col);
    vprintf(fmt, va);
    if(nl)
        putchar('\n');
    if(col != NONE)
        _log_resetcolor(true);
}

static ConsoleColor getcolor(int level)
{
    compile_assert(Countof(colorLUT) == LL::_TOTAL);
    return level >= LL::_MIN && level <= LL::_MAX ? colorLUT[level - LL::_MIN] : colorLUT[0];
}

void vlogx(LL::Enum level, int nl, const char *fmt, va_list va)
{
    va_list vax;
    va_copy(vax, va);
    const ConsoleColor color = getcolor(level);
    _valogcolor(color, nl, fmt, va);
    _log_docallback(s_logCallbacks, level, nl, vax, fmt);
}

void logx(LL::Enum level, int nl, const char *fmt, ...)
{
    va_list va;
    va_start(va, fmt);
    vlogx(level, nl, fmt, va);
    va_end(va);
}

void log(const char *fmt, ...)
{
    va_list va;
    va_start(va, fmt);
    vlogx(LL::NORMAL, 1, fmt, va);
    va_end(va);
}

void logerror(const char *fmt, ...)
{
    va_list va;
    va_start(va, fmt);
    vlogx(LL::ERR, 1, fmt, va);
    va_end(va);
}

void logerrorbox(const char *fmt, ...)
{
    va_list va;
    va_start(va, fmt);
    vlogx(LL::ERR_MSGBOX, 1, fmt, va);
    va_end(va);
}

void loginfo(const char *fmt, ...)
{
    va_list va;
    va_start(va, fmt);
    vlogx(LL::INFO, 1, fmt, va);
    va_end(va);
}

void logdebug(const char *fmt, ...)
{
    va_list va;
    va_start(va, fmt);
    vlogx(LL::DEBUG, 1, fmt, va);
    va_end(va);
}

void logdebug2(const char *fmt, ...)
{
    va_list va;
    va_start(va, fmt);
    vlogx(LL::DEBUG2, 1, fmt, va);
    va_end(va);
}


#pragma once

#include <stddef.h>
#include <stdlib.h>
#include "macros.h"

namespace internal
{
    template <typename T, size_t N>
    char (&_ArraySizeHelper( T (&a)[N]))[N];
}
#define Countof(a) (sizeof(internal::_ArraySizeHelper(a)))

template<typename T>
static inline const T& clamp(const T& val, const T& lo, const T& hi)
{
    return lo < val ? (val < hi ? val : hi) : lo;
}

// scale t from [lower, upper] into [rangeMin, rangeMax]
template<typename T>
inline T rescale(const T& t, const T& lower, const T& upper, const T& rangeMin, const T& rangeMax)
{
    return upper != lower
        ? (((t - lower) / (upper - lower)) * (rangeMax - rangeMin)) + rangeMin
        : rangeMin;
}

template<typename T>
inline T rescaleClamp(const T& t, const T& lower, const T& upper, const T& rangeMin, const T& rangeMax)
{
    return clamp(rescale(t, lower, upper, rangeMin, rangeMax), rangeMin, rangeMax);
}

template<typename C>
size_t ByteSize(const C& c)
{
    return c.size() * sizeof(typename C::value_type);
}

inline unsigned calcMipLevel1D(unsigned w)
{
    unsigned levels = 1;
    while (w >> levels)
        ++levels;
    return levels;
}

inline unsigned calcMipLevel2D(unsigned w, unsigned h)
{
    return calcMipLevel1D(w | h);
}

inline unsigned calcMipLevel3D(unsigned w, unsigned h, unsigned d)
{
    return calcMipLevel1D(w | h | d);
}

bool wildcardMatch(const char *str, const char *pattern);
bool wildcardMatchNoCase(const char *str, const char *pattern);

template<typename T> T& deref(T *p)
{
    assert(p);
    return *p;
}

template<typename T> const T& deref(const T *p)
{
    assert(p);
    return *p;
}

// The only safe and sane way to cast unrelated types without strict aliasing possibly breaking things
template<typename TO, typename FROM>
inline TO safe_reinterpret_cast(const FROM& v)
{
    union { FROM from; TO to; } u;
    u.from = v;
    return u.to;
}

const char *findFileExtension(const char *fn);

void NORETURN _releaseAssertFail(const char *file, unsigned line, const char *cond, const char *msg);

#define RELEASE_ASSERT(cond, msg) do { if(!(cond)) _releaseAssertFail(__FILE__, __LINE__, #cond, msg); } while(0)

#pragma once

#include "glmx.h"


/* A helper class holding information about a point of view and its direction */
class View
{
public:
    // This ensures we have a normalized up and right vector, and both are orthogonal to each
    // other and to forward.
    View(const glm::vec3& origin, const glm::vec3& at, const glm::vec3 &u)
    {
        setOrigin(origin);
        setLookAt(at, u);
    }

    void setLookAt(const glm::vec3& at, const glm::vec3 u = glm::vec3(0,1,0))
    {
        forward = glm::normalize(glmx::to(origin, at));
        up = glmx::orthogonalize(forward, u, &right);
    }

    void setOrigin(const glm::vec3& o)
    {
        origin = o;
    }

    const glm::vec3& getOrigin() const { return origin; }
    const glm::vec3& getForward() const { return forward; }
    const glm::vec3& getUp() const { return up; }
    const glm::vec3& getRight() const { return right; }

    inline bool operator==(const View& o) const { return origin == o.origin && forward == o.forward && up == o.up; }
    inline bool operator!=(const View& o) const { return !(*this == o); }

protected:
    glm::vec3 origin;
    glm::vec3 forward;
    glm::vec3 up;
    glm::vec3 right;
};

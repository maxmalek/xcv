#pragma once

#include "pstdint.h"

namespace platform {

void msgbox(const char *msg, const char *title = 0);
bool msgboxYesNo(const char *msg, const char *title = 0);
void breakpoint();

unsigned getCPUCount();

bool fileExists(const char *fn);
bool fileExists(const char *fn, uint64_t *psize);
uint64_t fileSize(const char *fn);
void normalizePath(char *s);
void toOSPath(char *s);

bool openURL(const char *url); // also handles file:// ...
bool openDefaultAssoc(const char *file);

// returned string must be free()'d
char *openFileDialog(const char *filters = 0, const char *where = 0);
char *saveFileDialog(const char *filters = 0, const char *where = 0);
char *getClipboardText(); // NULL if nothing in clipboard. must be free()'d
bool setClipboardText(const char *str); // can be NULL to clear clipboard

extern const char pathsep;

}

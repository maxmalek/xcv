#pragma once

#include <vector>
#include <string>

bool GetFileList(const char *path, std::vector<std::string>& files);

void StripFileExtension(std::string& s);

#include "threading.h"

#include <SDL_thread.h>
#include <SDL_version.h>
#include <assert.h>


// --------- Lockable ----------

Lockable::Lockable()
{
    _mtx = SDL_CreateMutex();
}

Lockable::~Lockable()
{
    SDL_DestroyMutex((SDL_mutex*)_mtx);
}

void Lockable::lock()
{
    SDL_LockMutex((SDL_mutex*)_mtx);
}

void Lockable::unlock()
{
    SDL_UnlockMutex((SDL_mutex*)_mtx);
}

// --------- Waitable ----------

Waitable::Waitable()
{
    _cond = SDL_CreateCond();
}

Waitable::~Waitable()
{
    SDL_DestroyCond((SDL_cond*)_cond);
}

void Waitable::wait()
{
    SDL_CondWait((SDL_cond*)_cond, (SDL_mutex*)_mtx);
}

void Waitable::signal()
{
    SDL_CondSignal((SDL_cond*)_cond);
}

void Waitable::broadcast()
{
    SDL_CondBroadcast((SDL_cond*)_cond);
}

Thread::Thread()
: _th(NULL)
{
}

Thread::~Thread()
{
    join();
}

void Thread::join()
{
    if(_th)
    {
        SDL_WaitThread((SDL_Thread*)_th, NULL);
        _th = NULL;
    }
}

static int _launchThread(void *p)
{
    ((Thread*)p)->run();
    return 0;
}

void Thread::launch()
{
    assert(!_th && "Thread already running");
    _th = SDL_CreateThread(_launchThread, NULL, this);
}


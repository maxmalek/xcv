#pragma once

#include "api_types.h"
#include "pstdint.h"

static unsigned Blob3DSize[] =
{
    1, // B3D_U8
    2, // B3D_U16
    4, // B3D_U32
    1, // B3D_S8
    2, // B3D_S16
    4, // B3D_S32
    4, // B3D_FLOAT
    2, // B3D_HALF
    8, // B3D_S64
    8, // B3D_U64
    8  // B3D_DOUBLE
};

static const char * Blob3DNameSized[] =
{
    "u8", // B3D_U8
    "u16", // B3D_U16
    "u32", // B3D_U32
    "s8", // B3D_S8
    "s16", // B3D_S16
    "s32", // B3D_S32
    "f32", // B3D_FLOAT
    "f16", // B3D_HALF
    "s64", // B3D_S64
    "u64", // B3D_U64
    "f64"  // B3D_DOUBLE
};

static const char * Blob3DNameCommon[] =
{
    "unsigned char", // B3D_U8
    "unsigned short", // B3D_U16
    "unsigned int", // B3D_U32
    "signed char", // B3D_S8
    "short", // B3D_S16
    "int", // B3D_S32
    "float", // B3D_FLOAT
    "half", // B3D_HALF
    "int64", // B3D_S64
    "unsigned int64", // B3D_U64
    "double"  // B3D_DOUBLE
};

Blob3DType b3d_getTypeFromString(const char *typestr);
uint64_t b3d_getNumElements(const Blob3DInfo *info); // W x H x D x Channels
uint64_t b3d_getSizeInBytes(const Blob3DInfo *info);

#include "blob3ddef.h"
#include "macros.h"
#include "util.h"
#include <string.h>

Blob3DType b3d_getTypeFromString(const char *typestr)
{
    compile_assert(Countof(Blob3DSize) == B3D_LAST_VALID_TYPE+1);
    compile_assert(Countof(Blob3DNameSized) == B3D_LAST_VALID_TYPE+1);
    compile_assert(Countof(Blob3DNameCommon) == B3D_LAST_VALID_TYPE+1);

    Blob3DType type = B3D_UNSPECIFIED;

    if(typestr && *typestr)
        for(unsigned i = B3D_FIRST_VALID_TYPE; i <= B3D_LAST_VALID_TYPE; ++i)
            if(!strcmp(typestr, Blob3DNameSized[i]) || !strcmp(typestr, Blob3DNameCommon[i]))
            {
                type = (Blob3DType)i;
                break;
            }
    return type;
}

uint64_t b3d_getSizeInBytes(const Blob3DInfo * info)
{
    const uint64_t esz = Blob3DSize[info->type];
    return b3d_getNumElements(info) * esz;
}

uint64_t b3d_getNumElements(const Blob3DInfo * info)
{
    assert(info);
    const uint64_t w = info->w ? info->w : 1;
    const uint64_t h = info->h ? info->h : 1;
    const uint64_t d = info->d ? info->d : 1;
    const uint64_t ch = info->channels;
    return w * h * d * ch;
}

#pragma once

// Via https://blog.molecular-matters.com/2015/11/09/job-system-2-0-lock-free-work-stealing-part-4-parallel_for/

#include "jobq.h"

namespace para {

class CountSplitter
{
public:
    explicit CountSplitter(size_t count)
    : m_count(count)
    {
    }

    template <typename T>
    inline bool Split(size_t count) const
    {
        return (count > m_count);
    }

private:
    size_t m_count;
};

class DataSizeSplitter
{
public:
    explicit DataSizeSplitter(size_t size)
    : m_size(size)
    {
    }

    template <typename T>
    inline bool Split(size_t count) const
    {
        return (count*sizeof(T) > m_size);
    }

private:
    size_t m_size;
};


template <typename T, typename S>
struct parallel_for_job_data
{
    typedef T DataType;
    typedef S SplitterType;
    typedef void (*Func)(DataType*, size_t, size_t);

    parallel_for_job_data(DataType* p, size_t sz, size_t beg, Func f, const SplitterType& s)
        : data(p)
        , count(sz)
        , begin(beg)
        , function(f)
        , splitter(s)
    {
    }

    inline parallel_for_job_data<DataType,SplitterType> derive(DataType *newp, size_t newsz, size_t newbegin) const
    {
         return parallel_for_job_data<DataType,SplitterType>(newp, newsz, newbegin, function, splitter);
    }

    inline void exec() const
    {
        function(data, count, begin);
    }

    DataType* data;
    size_t count;
    size_t begin;
    Func function;
    SplitterType splitter;
};

template <typename T, typename U, typename S>
struct parallel_for_job_data_with_utility
{
    typedef T DataType;
    typedef S SplitterType;
    typedef U UtilityType;
    typedef  void (*Func)(DataType*, size_t, size_t, const UtilityType&);

    parallel_for_job_data_with_utility(DataType* p, size_t sz, size_t beg, Func f, const UtilityType& u, const SplitterType& s)
        : data(p)
        , count(sz)
        , begin(beg)
        , function(f)
        , util(u)
        , splitter(s)
    {
    }

    inline parallel_for_job_data_with_utility<DataType,UtilityType,SplitterType> derive(DataType *newp, size_t newsz, size_t newbegin) const
    {
         return parallel_for_job_data_with_utility<DataType,UtilityType,SplitterType>(newp, newsz, newbegin, function, util, splitter);
    }

    inline void exec() const
    {
        function(data, count, begin, util);
    }

    DataType * const data;
    size_t count;
    size_t begin;
    Func function;
    SplitterType splitter;
    const UtilityType util; // NOT a reference!
};

template <typename JobData>
void parallel_for_job(jobq::Job job, const void* jobData)
{
    const JobData* const data = static_cast<const JobData*>(jobData);
    const typename JobData::SplitterType& splitter = data->splitter;

    if (splitter.template Split<typename JobData::DataType>(data->count))
    {
        // split in two
        const size_t leftCount = data->count / 2u;
        const JobData leftData = data->derive(data->data, leftCount, data->begin);
        jobq::Job left = jobq::CreateJob(job, parallel_for_job<JobData>, leftData);
        jobq::Run(left);

        const size_t rightCount = data->count - leftCount;
        const JobData rightData = data->derive(data->data + leftCount, rightCount, data->begin + leftCount);
        jobq::Job right = jobq::CreateJob(job, parallel_for_job<JobData>, rightData);
        jobq::Run(right);
    }
    else
    {
        data->exec();
    }
}

size_t autoSplitSize(size_t sz, size_t minsize);

// ------------ PLAIN ---------------------

template<typename T, typename S>
jobq::Job CreateJob(jobq::Job parent, T *p, size_t sz, void (*f)(T*, size_t, size_t), const S& split)
{
    typedef parallel_for_job_data<T, S> JobData;
    const JobData jobData(p, sz, f, split);

    return jobq::CreateJob(parent, parallel_for_job<JobData>, jobData);
}


template<typename T, typename S>
jobq::Job CreateJob(T *p, size_t sz, void (*f)(T*, size_t, size_t), const S& split)
{
    typedef parallel_for_job_data<T, S> JobData;
    const JobData jobData(p, sz, f, split);

    return jobq::CreateJob(parallel_for_job<JobData>, jobData);
}

template<typename T, typename S>
void For(T *p, size_t sz, void (*f)(T*, size_t, size_t), const S& split)
{
    jobq::Job j = CreateJob(p, sz, f, split);
    Run(j);
    Wait(j);
}

template<typename T>
inline void ForEach(T *p, size_t sz, void (*f)(T*, size_t, size_t), size_t elems = 0, size_t minsize = 1)
{
    if(sz <= elems) // Avoid going through the job system if the input size is small
        f(p, sz, 0);
    else
        For(p, sz, f, CountSplitter(elems ? elems : autoSplitSize(sz, minsize)));
}


// ---------------------------- WITH UTILITY --------------

template<typename T, typename U, typename S>
jobq::Job CreateJob(jobq::Job parent, T *p, size_t sz, const U& u, void (*f)(T*, size_t, size_t, const U&), const S& split)
{
    typedef parallel_for_job_data_with_utility<T, U, S> JobData;
    const JobData jobData(p, sz, 0, f, u, split);

    return jobq::CreateJob(parent, parallel_for_job<JobData>, jobData);
}

template<typename T, typename U, typename S>
jobq::Job CreateJob(T *p, size_t sz, const U& u, void (*f)(T*, size_t, size_t, const U&), const S& split)
{
    typedef parallel_for_job_data_with_utility<T, U, S> JobData;
    const JobData jobData(p, sz, 0, f, u, split);

    return jobq::CreateJob(parallel_for_job<JobData>, jobData);
}

template<typename T, typename U, typename S>
void For(T *p, size_t sz, const U& u, void (*f)(T*, size_t, size_t, const U&), const S& split)
{
    jobq::Job j = CreateJob(p, sz, u, f, split);
    Run(j);
    Wait(j);
}

template<typename T, typename U>
inline void ForEach(T *p, size_t sz, const U& u, void (*f)(T*, size_t, size_t, const U&), size_t elems = 0, size_t minsize = 1)
{
    if(sz <= elems) // Avoid going through the job system if the input size is small
        f(p, sz, 0, u);
    else
        For(p, sz, u, f, CountSplitter(elems ? elems : autoSplitSize(sz, minsize)));
}


} // end namespace para



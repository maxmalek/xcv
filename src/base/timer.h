#pragma once

class ScopedTimer
{
public:
    ScopedTimer(const char *s = "ScopedTimer");
    ~ScopedTimer();

private:
    const char *_s;
    unsigned _t;
};

class Timer
{
public:
    Timer();

    unsigned reset();
    unsigned elapsed() const;
private:
    unsigned _t;
};

class FrameTimer
{
public:
    FrameTimer() {}
    float getdt();
private:
    Timer _tt;
};

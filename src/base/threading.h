#ifndef THREADING_H
#define THREADING_H

#include <list>
#include <vector>
#include <stddef.h>


// Threading classes etc.

class Lockable
{
public:
    Lockable();
    virtual ~Lockable();
    void lock();
    void unlock();

protected:
    mutable void *_mtx;
};

class Waitable : public Lockable
{
public:
    Waitable();
    virtual ~Waitable();
    void wait(); // releases the associated lock while waiting
    void signal(); // signal a single waiting thread
    void broadcast(); // signal all waiting threads

protected:
    mutable void *_cond;
};

class MTGuard
{
public:
    MTGuard(Lockable& x) : _obj(&x) { x.lock(); }
    MTGuard(Lockable* x) : _obj(x)  { x->lock(); }
    ~MTGuard() { _obj->unlock(); }
private:
    Lockable *_obj;
};

class Thread
{
protected:
    Thread();
public:
    ~Thread();
    virtual void run() = 0;
    void launch();
    void join();
private:
    void *_th;
};


#endif

#pragma once

// Adapted from https://gist.github.com/md-5/4264443
// Original by byuu

namespace vint {
    
typedef unsigned char byte;

template<typename T>
unsigned encode(byte * const out, T data, unsigned maxlen)
{
    byte * p = out;
    byte * const end = out + maxlen;
    while(p < end)
    {
        byte x = byte(data & 0x7f);
        data >>= 7;
        if(data == 0)
        {
            *p++ = 0x80 | x;
            break;
        }
        *p++ = x;
        data--;
    }
    return unsigned(p - out);
}

template<typename T>
unsigned decode(T *out, const byte * const in, unsigned maxlen)
{
    T data = 0, shift = 1;
    const byte * p = in;
    const byte * const end = in + maxlen;
    while(p < end)
    {
        byte x = *p++;
        data += (x & 0x7f) * shift;
        if(x & 0x80)
            break;
        shift <<= 7;
        data += shift;
    }
    if(out)
        *out = data;
    return unsigned(p - in);
}

} // end namespace vint

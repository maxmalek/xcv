#pragma once

#include "macros.h"
#include <string.h>
#include <SDL_atomic.h>

// Do NOT touch the Job struct in any way!
// Just treat it like an opaque thing that is safe to pass around as a pointer, but nothing else.

/* Short description:
1) Create a job.
2) Create any child jobs, and/or add any continuations to the existing job.
3) After all dependencies are added, Run() each job. Parent job comes last. Do NOT call Run() for continuations.
4) A job can create & run more jobs in its function:
    - It may also create & add new child jobs to itself
    - It may NOT add continuations to itself
5) Wait() for a job to finish. A job is finished when all its children are finished. Continuations are irrelevant.
6) Continuations run in-order after their ancestor job is finshed.
    - They are either pushed to the job queue via Run() (the default),
      or executed directly.
7) To wait for a long chain of continuations to finish, Wait() for the last continuation.
*/

namespace jobq {

struct JobStruct;

typedef void (*Func)(JobStruct *job, const void*);

const unsigned CONTINUATIONS_PER_JOB = 7;
const unsigned CACHE_LINES_PER_JOB = 2;

#ifdef _MSC_VER
typedef long NativeAtomicContainer;
typedef long NativeAtomicValue;
#else
typedef SDL_atomic_t NativeAtomicContainer;
typedef int NativeAtomicValue;
#endif

struct JobStruct
{
    Func f;
    JobStruct* parent;
    NativeAtomicContainer atomic_unfinishedJobs;
    NativeAtomicContainer atomic_nCont;

    template<unsigned N>
    void _copyData(const void *p);
    unsigned short contIdxs[CONTINUATIONS_PER_JOB];
    unsigned char contRunDirect; // bitmask
    unsigned char createdByThread;
};

struct PaddedJob : public JobStruct
{
    enum { PADDING_SIZE = CACHE_LINES_PER_JOB * ARCH_CACHE_LINE_SIZE - sizeof(JobStruct) };
    char padding[PADDING_SIZE];
};

template<unsigned N> void JobStruct::_copyData(const void *p)
{
    compile_assert(N <= PaddedJob::PADDING_SIZE);
    memcpy(this+1, p, N);
}

typedef JobStruct* Job;

Job CreateJob(Func f);
Job CreateJobAsChild(Job parent, Func f);

template<unsigned N>
Job CreateJobWithData(Func f, const void *p)
{
    Job j = CreateJob(f);
    j->_copyData<N>(p);
    return j;
}

template<unsigned N>
Job CreateJobAsChildWithData(Job parent, Func f, const void *p)
{
    Job j = CreateJobAsChild(parent, f);
    j->_copyData<N>(p);
    return j;
}

// Simplified API
inline Job CreateJob(Job parent, Func f) { return CreateJobAsChild(parent, f); }
template<typename T> inline Job CreateJob(Func f, const T& data) { return CreateJobWithData<sizeof(T)>(f, &data); }
template<typename T> inline Job CreateJob(Job parent, Func f, const T& data) { return CreateJobAsChildWithData<sizeof(T)>(parent, f, &data); }

void StartWorkers(unsigned n);
void StopWorkers();
void Run(Job);
void Wait(const Job);
bool HasJobCompleted(const Job); // Non-blocking check
void AddContinuation(Job ancestor, Job continuation, bool runInline = false);
unsigned GetNumBackgroundWorkerThreads();
unsigned GetAvailThreads();


} // end namespace jobq

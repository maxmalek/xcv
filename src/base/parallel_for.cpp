#include "parallel_for.h"
#include <assert.h>

namespace para {

size_t autoSplitSize(size_t sz, size_t minsize)
{
    assert(minsize);
    const size_t th = jobq::GetAvailThreads();
    const size_t n = sz / (th * 8);
    return n < minsize ? minsize : n;
}

} // end namespace para

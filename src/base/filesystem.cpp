#include "filesystem.h"

#if _WIN32
#   define WIN32_LEAN_AND_MEAN
#   include <windows.h>
#else
#   include <sys/dir.h>
#   include <unistd.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>

static void MakeSlashTerminated(std::string& s)
{
    if(s.length() && s[s.length() - 1] != '/')
        s += '/';
}

#if !_WIN32
static bool _IsFile(const char *path, dirent *dp)
{
    switch(dp->d_type)
    {
        case DT_DIR:
            return false;
        case DT_LNK:
        {
            std::string fullname = path;
            fullname += '/';
            fullname += dp->d_name;
            struct stat statbuf;
            if(stat(fullname.c_str(), &statbuf))
                return false; // error
            return !S_ISDIR(statbuf.st_mode);
        }
    }
    return true;
}
#endif

// returns list of *plain* file names in given directory,
// without subdirs, and without anything else
bool GetFileList(const char *path, std::vector<std::string>& files)
{
#if !_WIN32
    DIR *dirp = opendir(path);
    if(!dirp)
        return false;
    dirent *dp;
    while((dp=readdir(dirp)) != NULL)
        if (_IsFile(path, dp)) // only add if it is not a directory
            files.push_back(dp->d_name);
    closedir(dirp);
    return true;

# else

    WIN32_FIND_DATA fil;
    std::string search(path);
    MakeSlashTerminated(search);
    search += "*";
    HANDLE hFil = FindFirstFile(search.c_str(),&fil);
    if(hFil == INVALID_HANDLE_VALUE)
        return false;
    do
    {
        if(!(fil.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
        {
            std::string s(fil.cFileName);
            files.push_back(s);
        }
    }
    while(FindNextFile(hFil, &fil));

    FindClose(hFil);
    return true;

# endif
}

void StripFileExtension(std::string& s)
{
    size_t pos = s.find_last_of('.');
    size_t pos2 = s.find_last_of('/');
    if(pos != std::string::npos && (pos2 < pos || pos2 == std::string::npos))
        s.resize(pos);
}

#pragma once

#include "api_macros.h"
#include <assert.h>

#ifdef _MSC_VER
//#  define THREADLOCAL __declspec(thread)
#  define ALIGN(N) __declspec(align(N))
#  define FORCEINLINE __forceinline // possibly __inline on older msvc
#  define UNUSED __declspec(deprecated)
#  define NORETURN __declspec(noreturn)
#  define ATTR_PRINTF(a)
#  if _MSC_VER >= 1700
#    include <sal.h>
#    define ASSUME(cond) do { __analysis_assume(cond); assert(cond); } while(0)
#    define CHECK_RESULT _Check_return_
#  else
#    define ASSUME(cond) assert(cond)
#    define CHECK_RESULT
#  endif
#else // gcc, clang
#  define ALIGN(n) __attribute__((aligned(n)))
//#  define THREADLOCAL __thread
#  define FORCEINLINE __attribute__((always_inline))
#  define UNUSED __attribute__((unused))
#  define CHECK_RESULT __attribute__((warn_unused_result))
#  define ASSUME(cond) assert(cond) // TODO: What is the proper clang annotation for this?
#  define NORETURN __attribute__((noreturn))
#  define ATTR_PRINTF(a) __attribute__ ((format (printf, a, (a)+1)));
#endif

#define THREADLOCAL thread_local

// For compilers that aren't smart enough, stick this at the end of a NORETURN function
#define END_NORETURN_FUNC do{ assert("Reached end of NORETURN function"); } while(1)

#if defined(__x86_64__) || defined(__amd64) || defined(_M_X64) || defined(__i386) || defined(_M_IX86) || defined(_X86_)
#define ARCH_IS_X86 1
#endif

#if ARCH_IS_X86
#  define ARCH_CACHE_LINE_SIZE 64
#else
#  error define: ARCH_CACHE_LINE_SIZE
#endif

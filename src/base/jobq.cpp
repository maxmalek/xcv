#include "jobq.h"
#include "util.h"
#include <SDL.h>
#include <stdio.h>
#include <new>
#include <limits>
#include "log.h"

#include <emmintrin.h> // for _mm_*()


// Pretty much based on http://blog.molecular-matters.com/2015/09/25/job-system-2-0-lock-free-work-stealing-part-3-going-lock-free/

#define USE_SEMAPHORE

namespace jobq {


static const unsigned MAX_JOBS = 4096; // Must be power of 2

class JobQueue;

// Inc & Dec functions must return NEW value as it is AFTER the operation.
// CAS must return 0 when failed, any other value when successful.
#ifdef _MSC_VER
#define COMPILER_BARRIER _ReadWriteBarrier()
static void _compile_check2() { compile_assert(sizeof(int) == sizeof(long)); }
static FORCEINLINE NativeAtomicValue atomicInc(NativeAtomicContainer *x) { return _InterlockedIncrement(x); }
static FORCEINLINE NativeAtomicValue atomicDec(NativeAtomicContainer *x) { return _InterlockedDecrement(x); }
static FORCEINLINE int atomicCAS(NativeAtomicContainer *x, NativeAtomicValue oldval, NativeAtomicValue newval) { return _InterlockedCompareExchange(x, newval, oldval) == oldval; }
static FORCEINLINE void atomicSet(NativeAtomicContainer *x, NativeAtomicValue newval) { _InterlockedExchange(x, newval); }
static FORCEINLINE NativeAtomicValue nonatomicGet(NativeAtomicContainer *x) { return *x; }
static FORCEINLINE void nonatomicSet(NativeAtomicContainer *x, NativeAtomicValue v) { *x = v; }
#else
#define COMPILER_BARRIER SDL_CompilerBarrier()
static FORCEINLINE NativeAtomicValue atomicInc(NativeAtomicContainer *x) { COMPILER_BARRIER; return SDL_AtomicAdd(x, 1) + 1; }
static FORCEINLINE NativeAtomicValue atomicDec(NativeAtomicContainer *x) { COMPILER_BARRIER; return SDL_AtomicAdd(x, -1) - 1; }
static FORCEINLINE int atomicCAS(NativeAtomicContainer *x, NativeAtomicValue oldval, NativeAtomicValue newval) { COMPILER_BARRIER; return SDL_AtomicCAS(x, oldval, newval); }
static FORCEINLINE void atomicSet(NativeAtomicContainer *x, NativeAtomicValue newval) { COMPILER_BARRIER; SDL_AtomicSet(x, newval); }
static FORCEINLINE NativeAtomicValue nonatomicGet(NativeAtomicContainer *x) { return x->value; }
static FORCEINLINE void nonatomicSet(NativeAtomicContainer *x, NativeAtomicValue v) { x->value = v; }
#endif

static void _compile_check()
{
    compile_assert(CACHE_LINES_PER_JOB * ARCH_CACHE_LINE_SIZE == sizeof(PaddedJob));
    compile_assert(sizeof(NativeAtomicContainer) == sizeof(NativeAtomicContainer)); // This is not actually a hard requirement, but useful for development to prevent typos and such.
}


static FORCEINLINE void thyield() { _mm_pause(); }
static FORCEINLINE void mfence() { COMPILER_BARRIER; _mm_mfence(); }


// Adapted from https://github.com/preshing/cpp11-on-multicore/blob/master/common/sema.h
// As published in http://preshing.com/20150316/semaphores-are-surprisingly-versatile/
class LightweightSemaphore
{
private:
    NativeAtomicContainer _atomic_count;
    SDL_semaphore *_sem;

    void waitWithPartialSpinning()
    {
        NativeAtomicValue oldCount;
        int spin = 10000;
        do
        {
            oldCount = nonatomicGet(&_atomic_count);
            COMPILER_BARRIER;
            if (oldCount > 0 && atomicCAS(&_atomic_count, oldCount, oldCount - 1))
                return;
            // Prevent the compiler from collapsing the loop.
            COMPILER_BARRIER;
            thyield();
        }
        while(spin--);
        oldCount = atomicDec(&_atomic_count) + 1;
        if (oldCount <= 0)
        {
            SDL_SemWait(_sem);
        }
    }

public:

    void init(NativeAtomicValue initialCount = 0)
    {
         assert(initialCount >= 0);
        nonatomicSet(&_atomic_count, initialCount);
        _sem = SDL_CreateSemaphore(0);
    }
    void destroy()
    {
        SDL_DestroySemaphore(_sem);
        _sem = NULL;
    }

    bool tryWait()
    {
        const NativeAtomicValue oldCount = nonatomicGet(&_atomic_count);
        COMPILER_BARRIER;
        return oldCount > 0 && atomicCAS(&_atomic_count, oldCount, oldCount - 1);
    }

    void wait()
    {
        if (!tryWait())
            waitWithPartialSpinning();
    }

    void signal()
    {
        const NativeAtomicValue oldCount = atomicInc(&_atomic_count) - 1;
        const NativeAtomicValue toRelease = -oldCount < 1 ? -oldCount : 1;
        if (toRelease > 0)
        {
            SDL_SemPost(_sem);
        }
    }
};

class ScopedLock
{
public:
    ScopedLock(SDL_mutex *m) : _m(m) { SDL_LockMutex(_m); }
    ~ScopedLock() { SDL_UnlockMutex(_m); }
private:
    SDL_mutex *_m;
};

class JobQueue
{
    enum { MASK = MAX_JOBS - 1u };

public:

    JobQueue()
    {
        nonatomicSet(&_bottom, 0);
        nonatomicSet(&_top, 0);
    }

    // Writes to _bottom (never races with Pop())
    void Push(Job job)
    {
        const NativeAtomicValue b = nonatomicGet(&_bottom);
        _jobs[b & MASK] = job;

        // ensure the job is written before b+1 is published to other threads.
        mfence();

        nonatomicSet(&_bottom, b+1);
    }

    // Writes to _top, reads from _bottom
    Job Steal()
    {
        const NativeAtomicValue t = nonatomicGet(&_top);
        
        // ensure that top is always read before bottom
        mfence();

        const NativeAtomicValue b = nonatomicGet(&_bottom);
        if (t < b)
        {
            // non-empty queue
            Job job = _jobs[t & MASK];

            // the CAS serves as a compiler barrier, and guarantees that the read happens before the CAS.
            if (atomicCAS(&_top, t, t+1))
            {
                // we incremented top, so there was no interfering operation
                return job;
            }

            // A concurrent steal or pop operation removed an element from the deque in the meantime.
        }

        // empty queue
        return NULL;
    }

    // Reads and writes _bottom and _top (never races with Push())
    Job Pop()
    {
        const NativeAtomicValue b = nonatomicGet(&_bottom) - 1;

        atomicSet(&_bottom, b); // acts as memory barrier

        const NativeAtomicValue t = nonatomicGet(&_top);
        if (t <= b)
        {
            // non-empty queue
            Job job = _jobs[b & MASK];
            if (t != b)
            {
                // there's still more than one item left in the queue
                return job;
            }

            // this is the last item in the queue
            if(!atomicCAS(&_top, t, t+1))
            {
                // failed race against steal operation
                job = NULL;
            }

            nonatomicSet(&_bottom, t+1);
            return job;
        }
        assert(nonatomicGet(&_bottom) == b);
        // deque was already empty
        nonatomicSet(&_bottom, t);
        return NULL;
    }


private:
    NativeAtomicContainer _bottom;
    NativeAtomicContainer _top;
    JobStruct *_jobs[MAX_JOBS];
};

static THREADLOCAL unsigned th_myThreadID = 0;
static THREADLOCAL int th_allocatedJobs = 0; // actually used as unsigned
static THREADLOCAL PaddedJob th_jobAlloc[MAX_JOBS];
static PaddedJob **s_threadJobAllocs = NULL; // allocated on init and filled in by threads, since th_jobAlloc is threadlocal

static Job _AllocJob()
{
    const unsigned i = th_allocatedJobs++;
    Job j = &th_jobAlloc[i & (MAX_JOBS-1u)];
    assert(HasJobCompleted(j));
    return j;
}

static Job _NewJob(Func f, Job parent)
{
    Job job = _AllocJob();
    job->f = f;
    job->parent = parent;
    nonatomicSet(&job->atomic_unfinishedJobs, 1);
    nonatomicSet(&job->atomic_nCont, 0);
    job->createdByThread = th_myThreadID;
    job->contRunDirect = 0;
    return job;
}

Job CreateJob(Func function)
{
    return _NewJob(function, NULL);
}

Job CreateJobAsChild(Job parent, Func f)
{
    const NativeAtomicValue newval = atomicInc(&parent->atomic_unfinishedJobs);
    assert(newval >= 1); // parent already finished, or finishing? shouldn't be

    return _NewJob(f, parent);
}

static JobQueue *GetMyQueue();
static JobQueue *GetOtherQueue();

Job GetJob(void)
{
    JobQueue *q = GetMyQueue();

    if(Job job = q->Pop())
        return job;

    // try stealing from some other queue
    JobQueue *stealQ = GetOtherQueue();
    if (stealQ != q) // don't try to steal from ourselves
        if(Job stolen = stealQ->Steal())
            return stolen;

    return NULL;
}

static int s_totalThreads = 0; // total number of worker threads allocated (0 or >0, but never inc/decremented)
#ifdef USE_SEMAPHORE
static LightweightSemaphore s_sem;
#endif

void Run(Job job)
{
    assert(s_totalThreads);
    GetMyQueue()->Push(job);
#ifdef USE_SEMAPHORE
    s_sem.signal();
#endif
}

bool HasJobCompleted(const Job job)
{
    //return atomicCmp(&job->atomic_unfinishedJobs, -1);
    const NativeAtomicValue n = nonatomicGet(&job->atomic_unfinishedJobs);
    assert(n >= 0);
    return n <= 0;
}

static void Finish(Job job);

static void Execute(Job job)
{
    Func f = job->f;
    if(f)
        f(job, job+1); // job data are in the padding area, which starts right after the job struct
    assert(!HasJobCompleted(job));
    Finish(job);
}

void Wait(const Job job)
{
    // wait until the job has completed. in the meantime, work on any other job.
    while (!HasJobCompleted(job))
    {
        Job nextJob = GetJob();
        if (nextJob)
            Execute(nextJob);
        else
            thyield();
    }
}

static void Finish(Job job)
{
    const NativeAtomicValue unfinished = atomicDec(&job->atomic_unfinishedJobs);
    if(unfinished == 0)
    {
        if(job->parent)
            Finish(job->parent);

        // Run continuations
        const unsigned N = nonatomicGet(&job->atomic_nCont);
        if(N)
        {
            PaddedJob *pool = s_threadJobAllocs[job->createdByThread];
            const unsigned runDirect = job->contRunDirect;
            for(unsigned i = 0; i < N; ++i)
            {
                Job next = static_cast<Job>(pool + job->contIdxs[i]);
                if(runDirect & (1u << i))
                    Execute(next);
                else
                    Run(next);
            }
        }
    }
}

void AddContinuation(Job ancestor, Job continuation, bool runInline)
{
    const unsigned c = atomicInc(&ancestor->atomic_nCont) - 1;
    PaddedJob *pool = s_threadJobAllocs[ancestor->createdByThread];
    assert(c < CONTINUATIONS_PER_JOB);
    assert(ancestor->createdByThread == continuation->createdByThread);
    assert(pool <= continuation && continuation < pool + MAX_JOBS); // continuation must be from the same pool as ancestor

    ptrdiff_t offs = static_cast<PaddedJob*>(continuation) - pool;
    
    assert(offs <= 0xffff);
    ancestor->contIdxs[c] = (unsigned short)offs;
    ancestor->contRunDirect |= int(runInline) << c;

    // TODO: assert that ancestor isn't running / queued to run yet
}


// --------------------------------------------------


static JobQueue *s_queues = NULL; // array of s_totalThreads elements
//static THREADLOCAL TinyRNG th_rng;
static THREADLOCAL JobQueue *th_queue = NULL;
static NativeAtomicContainer die = {0}; // 1 if worker threads should die off
static NativeAtomicContainer nth = {0}; // number of worker threads that are currently running
static THREADLOCAL unsigned th_qIdx = 0; // which queue to steal from
static SDL_mutex *s_masterLock = NULL; // Used during init and shutdown
static SDL_cond *s_masterCond = NULL;
static SDL_Thread **s_th = NULL;

JobQueue *GetOtherQueue()
{
    const unsigned idx = th_qIdx++ % (unsigned)s_totalThreads;
    assert(idx < (unsigned)s_totalThreads);
    return &s_queues[idx];
}


JobQueue *GetMyQueue()
{
    return &s_queues[th_myThreadID];
}

static void _thinit(int id)
{
    th_myThreadID = id;
    th_queue = new(&s_queues[id]) JobQueue();
    th_qIdx = 0;
    s_threadJobAllocs[id] = &th_jobAlloc[0];

    atomicInc(&nth);

    // Wait until all other threads have their queues initialized
    {
        ScopedLock lock(s_masterLock);

        while(nonatomicGet(&nth) != s_totalThreads)
            SDL_CondWait(s_masterCond, s_masterLock);

        SDL_CondBroadcast(s_masterCond);
    }
}

static int _thfunc(void *ud)
{
    int id = (int)(intptr_t)ud;
    _thinit(id);
   
    const unsigned N = s_totalThreads;
    while(!nonatomicGet(&die))
    {
        // Try a few times before going sleep -- make sure that all queues are checked before the semaphore is decremented
        for(unsigned i = 0; i < N; ++i)
            while(Job job = GetJob())
                Execute(job);

#ifdef USE_SEMAPHORE
        s_sem.wait();
        th_qIdx = 0; // we got woken up! Probably by the main thread, so make sure we try to steal there first
#else
        thyield();
#endif
    }

    atomicDec(&nth);

    return 0;
}

struct _StaticInit
{
    PaddedJob *pMainThreadJobs;
    _StaticInit()
    {
        pMainThreadJobs = &th_jobAlloc[0];
        s_threadJobAllocs = &pMainThreadJobs;
    }
};
static _StaticInit _s_init;

void StartWorkers(unsigned n)
{
    RELEASE_ASSERT(!s_totalThreads, "Threadpool already started");
    assert(n < (1u << (8*sizeof(JobStruct::createdByThread))) - 1);

    logdebug2("jobq: Using %u KB extra job storage memory per thread. MAX_JOBS = %u", unsigned(sizeof(th_jobAlloc) / 1024), MAX_JOBS);

    nonatomicSet(&die, 0);
    s_totalThreads = n+1;
    s_masterLock = SDL_CreateMutex();
    s_masterCond = SDL_CreateCond();

#ifdef USE_SEMAPHORE
    s_sem.init();
#endif
    s_queues = (JobQueue*)malloc(sizeof(JobQueue) * (n+1));
    s_threadJobAllocs = (PaddedJob**)malloc(sizeof(PaddedJob*) * (n+1));
    RELEASE_ASSERT(s_queues && s_threadJobAllocs, "Failed to allocate thread pool");

    if(n)
    {
        s_th = (SDL_Thread**)malloc(sizeof(SDL_Thread*) * n);
        RELEASE_ASSERT(s_th, "Failed to allocate thread pool storage");
        for(unsigned i = 0; i < n; ++i)
        {
            char buf[32];
            sprintf(buf, "W%u", i+1);
            SDL_Thread *th = SDL_CreateThread(_thfunc, buf, (void*)(intptr_t)(i+1));
            RELEASE_ASSERT(th, "Failed to allocate pool thread");
            s_th[i] = th;
        }
    }

    _thinit(0); // main thread is always 0
    // _thinit() waits until all threads have spun up before returning

    logdebug("jobq: Started %u background workers", n);
}

void StopWorkers()
{
    if(!s_totalThreads)
        return;

    atomicSet(&die, 1);

#ifdef USE_SEMAPHORE
    for(int i = 0; i < s_totalThreads; ++i)
    {
        // Poke threads until everyone has woken up
        s_sem.signal();
    }
#endif

    if(s_th)
    {
        for(int i = 0; i < s_totalThreads-1; ++i)
        {
            SDL_WaitThread(s_th[i], NULL);
            s_th[i] = NULL;
        }
        free(s_th);
        s_th = NULL;
    }

    // Clean up
    for(int i = 0; i < s_totalThreads; ++i)
        s_queues[i].~JobQueue();
    free(s_queues);
    s_queues = NULL;
    
    free(s_threadJobAllocs);

#ifdef USE_SEMAPHORE
    s_sem.destroy();
#endif

    SDL_DestroyMutex(s_masterLock);
    SDL_DestroyCond(s_masterCond);
    s_masterLock = NULL;
    s_masterCond = NULL;

    s_totalThreads = 0;
    nonatomicSet(&die, 0);
}

unsigned GetNumBackgroundWorkerThreads()
{
    return s_totalThreads ? s_totalThreads - 1 : 0;
}

unsigned GetAvailThreads()
{
    return s_totalThreads ? s_totalThreads : 1;
}


} // end namespace jobq

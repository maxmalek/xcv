#pragma once

inline int gaussKernelSize(int dim) { return 2 * dim + 1; }

// kernel must be of size 2*dim+1
void createGaussKernel(float *kernel, int dim, float sigma);

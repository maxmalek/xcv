#include "longjob.h"
#include "log.h"

/* extern */ LongJobExecutor g_mainLongExec;


void LongJobExecutor::add(SequentialLongJob *j)
{
    j->onBegin();
    _jobs.push_back(j);
}

void LongJobExecutor::update()
{
    size_t wpos = 0;
    for(size_t i = 0; i < _jobs.size(); ++i)
        if(_jobs[i]->run() <= LONGJOB_RUNNING)
            _jobs[wpos++] = _jobs[i]; // still running
        else
        {
            logdebug2("onFinish job \"%s\" (%p)", _jobs[i]->getName(), _jobs[i]);
            _jobs[i]->onFinish(); // will be overwritten or dropped off the end
        }
    _jobs.resize(wpos);
}

void SequentialLongJob::cancel()
{
    if(_status != LONGJOB_CANCELLED)
    {
        _status = LONGJOB_CANCELLED;
        _cancel();
    }
}

LongJobResult SequentialLongJob::run()
{
    if(_status > LONGJOB_RUNNING)
        return _status;
    return ((_status = _run()));
}

void SequentialLongJob::drawUI()
{
}

void SequentialLongJob::onBegin()
{
}

void SequentialLongJob::onFinish()
{
    delete this;
}

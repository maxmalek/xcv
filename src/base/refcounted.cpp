#include "refcounted.h"
#include <SDL_atomic.h>
#include <assert.h>
#include "macros.h"

#include "log.h"

template<RefcountType>
struct _Refcount {};

RefcountedBase::~RefcountedBase()
{
    assert(_refcount == 0);
}

template<> struct _Refcount<REFCOUNT_ATOMIC>
{
    static void incr(unsigned *c)
    {
        compile_assert(sizeof(unsigned) == sizeof(SDL_atomic_t));
        SDL_AtomicIncRef((SDL_atomic_t*)c);
    }

    static bool decr(unsigned *c)
    {
        return SDL_AtomicDecRef((SDL_atomic_t*)c);
    }
};

template<> struct _Refcount<REFCOUNT_NORMAL>
{
    static void incr(unsigned *c)
    {
        ++*c;
    }

    static bool decr(unsigned *c)
    {
        return !--*c;
    }
};

template<> struct _Refcount<REFCOUNT_FAKE>
{
    static void incr(unsigned *c)
    {
    }

    static bool decr(unsigned *c)
    {
        return false;
    }
};


template struct _Refcount<REFCOUNT_ATOMIC>;
template struct _Refcount<REFCOUNT_NORMAL>;
template struct _Refcount<REFCOUNT_FAKE>;


template<RefcountType T>
void Refcounted<T>::incref() const
{
    _Refcount<T>::incr(&_refcount);
}

template<RefcountType T>
void Refcounted<T>::decref() const
{
    if (_Refcount<T>::decr(&_refcount)) // is zero?
    {
        DEBUG_LOG("delete refcounted %p", this);
        delete this;
    }
}

template class Refcounted<REFCOUNT_ATOMIC>;
template class Refcounted<REFCOUNT_NORMAL>;
template class Refcounted<REFCOUNT_FAKE>;



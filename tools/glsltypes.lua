-- list copied out of https://www.opengl.org/wiki/GLAPI/glGetActiveUniform

local S = [[
GL_FLOAT 	float
GL_FLOAT_VEC2 	vec2
GL_FLOAT_VEC3 	vec3
GL_FLOAT_VEC4 	vec4
GL_DOUBLE 	double
GL_DOUBLE_VEC2 	dvec2
GL_DOUBLE_VEC3 	dvec3
GL_DOUBLE_VEC4 	dvec4
GL_INT 	int
GL_INT_VEC2 	ivec2
GL_INT_VEC3 	ivec3
GL_INT_VEC4 	ivec4
GL_UNSIGNED_INT 	unsigned
GL_UNSIGNED_INT_VEC2 	uvec2
GL_UNSIGNED_INT_VEC3 	uvec3
GL_UNSIGNED_INT_VEC4 	uvec4
GL_BOOL 	bool
GL_BOOL_VEC2 	bvec2
GL_BOOL_VEC3 	bvec3
GL_BOOL_VEC4 	bvec4
GL_FLOAT_MAT2 	mat2
GL_FLOAT_MAT3 	mat3
GL_FLOAT_MAT4 	mat4
GL_FLOAT_MAT2x3 	mat2x3
GL_FLOAT_MAT2x4 	mat2x4
GL_FLOAT_MAT3x2 	mat3x2
GL_FLOAT_MAT3x4 	mat3x4
GL_FLOAT_MAT4x2 	mat4x2
GL_FLOAT_MAT4x3 	mat4x3
GL_DOUBLE_MAT2 	dmat2
GL_DOUBLE_MAT3 	dmat3
GL_DOUBLE_MAT4 	dmat4
GL_DOUBLE_MAT2x3 	dmat2x3
GL_DOUBLE_MAT2x4 	dmat2x4
GL_DOUBLE_MAT3x2 	dmat3x2
GL_DOUBLE_MAT3x4 	dmat3x4
GL_DOUBLE_MAT4x2 	dmat4x2
GL_DOUBLE_MAT4x3 	dmat4x3
GL_SAMPLER_1D 	sampler1D
GL_SAMPLER_2D 	sampler2D
GL_SAMPLER_3D 	sampler3D
GL_SAMPLER_CUBE 	samplerCube
GL_SAMPLER_1D_SHADOW 	sampler1DShadow
GL_SAMPLER_2D_SHADOW 	sampler2DShadow
GL_SAMPLER_1D_ARRAY 	sampler1DArray
GL_SAMPLER_2D_ARRAY 	sampler2DArray
GL_SAMPLER_1D_ARRAY_SHADOW 	sampler1DArrayShadow
GL_SAMPLER_2D_ARRAY_SHADOW 	sampler2DArrayShadow
GL_SAMPLER_2D_MULTISAMPLE 	sampler2DMS
GL_SAMPLER_2D_MULTISAMPLE_ARRAY 	sampler2DMSArray
GL_SAMPLER_CUBE_SHADOW 	samplerCubeShadow
GL_SAMPLER_BUFFER 	samplerBuffer
GL_SAMPLER_2D_RECT 	sampler2DRect
GL_SAMPLER_2D_RECT_SHADOW 	sampler2DRectShadow
GL_INT_SAMPLER_1D 	isampler1D
GL_INT_SAMPLER_2D 	isampler2D
GL_INT_SAMPLER_3D 	isampler3D
GL_INT_SAMPLER_CUBE 	isamplerCube
GL_INT_SAMPLER_1D_ARRAY 	isampler1DArray
GL_INT_SAMPLER_2D_ARRAY 	isampler2DArray
GL_INT_SAMPLER_2D_MULTISAMPLE 	isampler2DMS
GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY 	isampler2DMSArray
GL_INT_SAMPLER_BUFFER 	isamplerBuffer
GL_INT_SAMPLER_2D_RECT 	isampler2DRect
GL_UNSIGNED_INT_SAMPLER_1D 	usampler1D
GL_UNSIGNED_INT_SAMPLER_2D 	usampler2D
GL_UNSIGNED_INT_SAMPLER_3D 	usampler3D
GL_UNSIGNED_INT_SAMPLER_CUBE 	usamplerCube
GL_UNSIGNED_INT_SAMPLER_1D_ARRAY 	usampler1DArray
GL_UNSIGNED_INT_SAMPLER_2D_ARRAY 	usampler2DArray
GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE 	usampler2DMS
GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY 	usampler2DMSArray
GL_UNSIGNED_INT_SAMPLER_BUFFER 	usamplerBuffer
GL_UNSIGNED_INT_SAMPLER_2D_RECT 	usampler2DRect
GL_IMAGE_1D 	image1D
GL_IMAGE_2D 	image2D
GL_IMAGE_3D 	image3D
GL_IMAGE_CUBE 	imageCube
GL_IMAGE_1D_ARRAY 	image1DArray
GL_IMAGE_2D_ARRAY 	image2DArray
GL_IMAGE_2D_MULTISAMPLE 	image2DMS
GL_IMAGE_2D_MULTISAMPLE_ARRAY 	image2DMSArray
GL_IMAGE_BUFFER 	imageBuffer
GL_IMAGE_2D_RECT 	image2DRect
GL_INT_IMAGE_1D 	iimage1D
GL_INT_IMAGE_2D 	iimage2D
GL_INT_IMAGE_3D 	iimage3D
GL_INT_IMAGE_CUBE 	iimageCube
GL_INT_IMAGE_1D_ARRAY 	iimage1DArray
GL_INT_IMAGE_2D_ARRAY 	iimage2DArray
GL_INT_IMAGE_2D_MULTISAMPLE 	iimage2DMS
GL_INT_IMAGE_2D_MULTISAMPLE_ARRAY 	iimage2DMSArray
GL_INT_IMAGE_BUFFER 	iimageBuffer
GL_INT_IMAGE_2D_RECT 	iimage2DRect
GL_UNSIGNED_INT_IMAGE_1D 	uimage1D
GL_UNSIGNED_INT_IMAGE_2D 	uimage2D
GL_UNSIGNED_INT_IMAGE_3D 	uimage3D
GL_UNSIGNED_INT_IMAGE_CUBE 	uimageCube
GL_UNSIGNED_INT_IMAGE_1D_ARRAY 	uimage1DArray
GL_UNSIGNED_INT_IMAGE_2D_ARRAY 	uimage2DArray
GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE 	uimage2DMS
GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_ARRAY 	uimage2DMSArray
GL_UNSIGNED_INT_IMAGE_BUFFER 	uimageBuffer
GL_UNSIGNED_INT_IMAGE_2D_RECT 	uimage2DRect
GL_INT64_ARB                        int64_t
GL_UNSIGNED_INT64_ARB               uint64_t
GL_INT64_VEC2_ARB                   i64vec2
GL_INT64_VEC3_ARB                   i64vec3
GL_INT64_VEC4_ARB                   i64vec4
GL_UNSIGNED_INT64_VEC2_ARB          u64vec2
GL_UNSIGNED_INT64_VEC3_ARB          u64vec3
GL_UNSIGNED_INT64_VEC4_ARB          u64vec4
]]

local faketypes = {
    "unknown",
    "buffer"
}

local reserved = { float = true, int = true, double = true, unsigned = true, bool = true, int64_t = true, uint64_t = true }

local groups = { PRIMTYPE = {}, GLSLTYPE = {}, PRIMMAP = {}, GLSLMAP = {}, GLSLSAMPLER = {}, GLSLIMAGE = {} }
for s in S:gmatch("([^\n]+)%\n") do
    local a, b = s:match("(%S+)%s+(%S+)")
    assert(a)
    local def = (reserved[b] and "PRIMTYPE") or "GLSLTYPE"
    if reserved[b] then
        local B = b:sub(1,1):upper() .. b:sub(2):lower()
        if B == "Unsigned" then
            B = "Uint"
        end
        table.insert(groups.PRIMTYPE, "PRIMTYPE(" .. b .. ", " .. B .. ")")
        table.insert(groups.PRIMMAP, "PRIMMAP(" .. a .. ", " .. b .. ", " .. B .. ")")
    else
        if b:match("^[a-z]?sampler") then
            table.insert(groups.GLSLSAMPLER, "GLSLSAMPLER(" .. b .. ")")
        elseif b:match("^[a-z]?image") then
            table.insert(groups.GLSLIMAGE, "GLSLIMAGE(" .. b .. ")")
        else
            table.insert(groups.GLSLTYPE, "GLSLTYPE(" .. b .. ")")
        end
        table.insert(groups.GLSLMAP, "GLSLMAP(" .. a .. ", " .. b .. ")")
    end
end

local function tofile(fn, groupnames)
    local out = io.open(fn, "w")
    out:write("// Autogenerated via glsltypes.lua\n\n")
    out:write("#ifdef GLSLFAKETYPE\n")
    for i = 1, #faketypes do
        out:write("GLSLFAKETYPE(" .. faketypes[i] .. ")\n")
    end
    out:write("#undef GLSLFAKETYPE\n")
    out:write("#endif\n\n")
    for i = 1, #groupnames do
        local gname = groupnames[i]
        out:write("#ifdef " .. gname .. "\n")
        out:write(table.concat(groups[gname], "\n"))
        out:write("\n#undef " .. gname .. "\n")
        out:write("#endif\n\n")
    end
    out:close()
end

tofile("glsl_gen.h", { "PRIMTYPE", "GLSLTYPE", "GLSLSAMPLER", "GLSLIMAGE" })
tofile("glslmap.h", { "PRIMMAP", "GLSLMAP" })

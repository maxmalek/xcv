#version 430 core

uniform sampler2D tex;
uniform vec4 colorMult = vec4(1.0);

in vec2 UV;
out vec4 color;

void main()
{
    color = texture(tex, UV) * colorMult;
}


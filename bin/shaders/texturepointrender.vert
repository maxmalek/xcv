#version 430 core

#define USE_TEXEL_FETCH

// no vertex inputs -- all calculated from the input textures

uniform uvec3 resolution;
uniform vec2 wminmax;
uniform sampler3D tex;
uniform mat4 proj;
uniform float useTexFactor; // usually 1.0

out vec4 pass_color;


uvec3 linearIDto3D(uint linear, uvec3 dims)
{
    uint bxy = dims.x * dims.y;
    uvec3 blockIdx;
    blockIdx.z = linear / bxy;
    linear -= blockIdx.z * bxy;
    blockIdx.y = linear / dims.x;
    linear -= blockIdx.y * dims.x;
    blockIdx.x = linear;
    return blockIdx;
}

ivec3 getTexelCoord()
{
    return ivec3(linearIDto3D(gl_VertexID, resolution));
}

vec3 getTexcoord()
{
    return (vec3(getTexelCoord()) + 0.5) / vec3(resolution);
}

vec4 testcolor(in float v)
{
    v = clamp(v, 0.0, 1.0);
    vec3 c = vec3(1.0-v, v, v*0.5);
    float len = length(c.rg); // this intentionally ignores the blue channel
    return vec4(c / len, 0.15 + 0.85*(1.0-v));
}

float rescale(in float v, in float vmin, in float vmax, in float targetMin, in float targetMax)
{
    float d = vmax - vmin;
    return mix(targetMin, targetMax, d != 0.0 ? (v - vmin) / d : 0.0);
}

float rescaleClamp(in float v, in float vmin, in float vmax, in float targetMin, in float targetMax)
{
    v = clamp(v, vmin, vmax);
    return rescale(v, vmin, vmax, targetMin, targetMax);
}

void main()
{
#ifdef USE_TEXEL_FETCH
    vec4 p = texelFetch(tex, getTexelCoord(), 0);
#else
    vec4 p = texture(tex, getTexcoord());
#endif
    vec3 tc = getTexcoord();
    float travelDist = distance(p.xyz, tc);
    p.xyz = mix(tc, p.xyz, useTexFactor);
    
    float colVal = 1.0 - travelDist; //p.w;

    gl_Position = proj * vec4(p.xyz * 2.0 - 1.0, 1.0);
    // Only draw points if score > 0. Use the step function to stay branch-free.
    gl_PointSize = 1.0 * step(-1.0, p.w);
    pass_color = testcolor(rescaleClamp(colVal, wminmax.x, wminmax.y, 0.0, 1.0));
}




#version 430 core

layout(std430, binding = 0) readonly buffer ProjectionBuffer
{
    mat4 mtx[];
} projbuf;

in vec4 in_vertex;
in vec4 in_vertexColor;
in int in_userIdx;
uniform vec2 wminmax;

out vec4 pass_color;

vec4 testcolor(in float v)
{
    v = clamp(v, 0.0, 1.0);
    vec3 c = vec3(1.0-v, v, v*0.5);
    float len = length(c.rg); // this intentionally ignores the blue channel
    return vec4(c / len, 0.15 + 0.85*(1.0-v));
}

float rescale(in float v, in float vmin, in float vmax, in float targetMin, in float targetMax)
{
    float d = vmax - vmin;
    return mix(targetMin, targetMax, d != 0.0 ? (v - vmin) / d : 0.0);
}

float rescaleClamp(in float v, in float vmin, in float vmax, in float targetMin, in float targetMax)
{
    v = clamp(v, vmin, vmax);
    return rescale(v, vmin, vmax, targetMin, targetMax);
}

void main()
{
    gl_Position = projbuf.mtx[in_userIdx] * vec4(in_vertex.xyz * 2.0 - 1.0, 1.0);
    gl_PointSize = 5.0;
    
    pass_color = vec4(0.5, 0.5, 1.0, 1.0);
}

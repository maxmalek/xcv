#version 430 core

uniform mat4 proj;

in vec4 in_vertex;

out vec2 UV;


void main()
{
    vec4 pos = proj * vec4(in_vertex.xyz, 1.0);
    gl_Position = pos;
    UV = pos.xy * 0.5 + 0.5;
#ifdef FLIP_V
    UV.y = 1.0 - UV.y; // make (0, 0) the upper left corner
#endif
}

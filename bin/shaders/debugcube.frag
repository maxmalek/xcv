#version 330 core

in vec4 vertexPosWorld;
out vec4 outvert;

void main()
{
    outvert = vec4(vertexPosWorld.xyz * 0.5 + 0.5, 0.66);
}

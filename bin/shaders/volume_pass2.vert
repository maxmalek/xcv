#version 330 core

uniform mat4 proj;

in vec4 in_vertex;

out vec4 vertexPosTransformed;
out vec4 vertexPosOriginal;

void main()
{
    vec4 v = vec4(in_vertex.xyz, 1.0);
    vertexPosTransformed = proj * v;
    vertexPosOriginal = v;
	gl_Position = vertexPosTransformed;
}

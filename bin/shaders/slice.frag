#version 330 core

uniform sampler3D tex;

in vec2 UV;
uniform float depth;
uniform int axis;
out vec4 color;

vec4 mapColor(float x)
{
    x = pow(x, 1.5);
    return vec4(x, x, x, 1.0);
}

float plain(vec3 tc)
{
    return texture(tex, tc).r;
}

const float doffs = 0.001;
const vec3 dv = vec3(doffs, doffs, doffs);
float deriv(vec3 tc)
{
    return abs(plain(tc) - plain(tc+dv)) * 10.0;
}

vec3 axis2tc()
{
    switch(axis)
    {
        case 0:
            return vec3(depth, UV);
        case 1:
            return vec3(UV.x, depth, UV.y);
        case 2:
            return vec3(UV, depth);
    }
    return vec3(0.0);
}

void main()
{
    vec3 tc = axis2tc();
    float x = plain(tc);
    color = mapColor(x);
    color = mix(color, vec4(0.5, 0.0, 0.5, 1.0), 0.2);
}


#version 430

layout(local_size_x = 16, local_size_y = 16, local_size_z = 4) in;

const uint workGroupSz = gl_WorkGroupSize.x * gl_WorkGroupSize.y * gl_WorkGroupSize.z;
const uint totalWorkGroups = gl_NumWorkGroups.x * gl_NumWorkGroups.y * gl_NumWorkGroups.z;
const uvec3 INC = gl_NumWorkGroups * gl_WorkGroupSize;
const uvec3 IDX = gl_GlobalInvocationID;
const uint TID = gl_LocalInvocationIndex;
const uint GID = gl_WorkGroupID.z * gl_NumWorkGroups.x * gl_NumWorkGroups.y
               + gl_WorkGroupID.y * gl_NumWorkGroups.x
               + gl_WorkGroupID.x;

// as output: counts
// as input: prefix sums
layout(std430, binding = 0) restrict buffer Counts
{
    uint p[];
} counts;

layout(std430, binding = 1) restrict writeonly buffer OutPoints
{
    vec4 p[];
} outpoints;

uniform sampler3D inTex;
uniform float threshold;


bool isInteresting(in vec3 tc)
{
    float v = texture(inTex, tc).x;
    return v > threshold;
}

shared uint sh_counts[workGroupSz];

void count()
{
    ivec3 texSize = textureSize(inTex, 0);
    vec3 tm = 1.0 / vec3(resolution);
    vec3 tmhalf = 0.5 * tm;
    1vec3 tc = vec3(i) * tm + tmhalf; 
    
    sh_counts[TID] = uint(all(lessThan(IDX, texSize)) && isInteresting(tc));
    
    memoryBarrierShared();
    barrier();
    
    for(uint s = workGroupSz >> 1u; s; s >>= 1u)
    {
        if(TID < s)
            sh_counts[TID] += sh_counts[s];
        
        memoryBarrierShared();
        barrier();
    }

    if(TID == 0)
    {
        outcounts.p[GID] = c;
    }
}

shared uint sh_idx;

void fill()
{
    ivec3 texSize = textureSize(inTex, 0);
    vec3 tm = 1.0 / vec3(resolution);
    vec3 tmhalf = 0.5 * tm;
    1vec3 tc = vec3(i) * tm + tmhalf; 
    
    uint offs = counts.p[GID];
    if(TID == 0)
        sh_idx = 0;
    memoryBarrierShared();
    barrier();
    
    if(all(lessThan(IDX, texSize)) && isInteresting(tc))
    {
        uint i = atomicAdd(sh_idx, 1);
        outpoints.p[offs + i] = tc;
    }
}


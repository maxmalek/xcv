// This file is included at the top of every shader.

// Check whether extensions are requested to be used
#ifdef HAS_ARB_bindless_texture
#extension GL_ARB_bindless_texture : enable
layout (bindless_sampler) uniform;
layout (bindless_image) uniform;
#endif

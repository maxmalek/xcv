#version 330 core

uniform mat4 proj;
in vec4 in_vertex;

out vec4 vertexPosWorld;

void main()
{
    vec4 v = vec4(in_vertex.xyz, 1.0);
    vertexPosWorld = v;
	gl_Position = proj * v;
}

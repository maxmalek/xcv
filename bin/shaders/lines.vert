#version 330 core

uniform mat4 proj;

in vec4 in_vertex;

void main()
{
    gl_Position = proj * vec4(in_vertex.xyz, 1.0);
}

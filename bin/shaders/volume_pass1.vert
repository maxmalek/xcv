#version 330 core

uniform mat4 proj;
in vec4 in_vertex;

out vec4 vertexPos;

void main()
{
    vec4 v = vec4(in_vertex.xyz, 1.0);
    vec4 t = proj * v;
    // Keep original vertex and scene-space distance to camera
    vertexPos = vec4(in_vertex.xyz, t.w);
	gl_Position = t;
}

#version 330 core

uniform mat4 proj;

in vec4 in_vertex;
in vec2 in_vertexUV;

out vec2 UV;

void main()
{
    gl_Position = proj * vec4(in_vertex.xyz, 1.0);
    UV = in_vertexUV;
}

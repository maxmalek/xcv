#version 430 core

#ifndef MAX_STEPS
#define MAX_STEPS 10000
#endif

const float ALPHA_LIMIT = 0.99;
const int LOD = 0;

uniform sampler3D in_tex;
uniform sampler3D in_colorTex;
uniform sampler3D in_deformTex;
uniform sampler3D in_alphaTex;
uniform sampler2D rayExitTex;
uniform float samplingDist;
uniform float stepValue;
uniform float intensity;
uniform float alphaMult;
uniform float deformMult;
uniform vec4 borderColor = vec4(1.0, 0.3, 1.0, 0.8);
uniform float borderSize = 0.08;

in vec4 vertexPosTransformed;
in vec4 vertexPosOriginal;
out vec4 color;

float plain(vec3 tc) { return textureLod(in_tex, tc, LOD).r; }

float plain2(vec3 tc)
{
    float v = plain(tc);
    return step(v, stepValue);
}

const float doffs = 0.001;
const vec3 dv = vec3(doffs, doffs, doffs);

float deriv_fast(vec3 tc)
{
    float v = plain(tc);
    float v111 = plain(tc+dv);
    return abs(v - v111) * 7.0;
}

float deriv_fast2(vec3 tc)
{
    float v = plain2(tc);
    float v111 = plain2(tc+dv);
    return abs(v - v111) * 7.0;
}

vec4 getcolor(vec3 tc, float val)
{
    vec3 c;
    float a;
#ifdef SD_COLOR
    c = textureLod(in_colorTex, tc, 0).rgb;
  #ifdef SD_NORMAL_COLORING
    c = c * 0.5 + 0.5;
  #endif
#else
    c = vec3(val * intensity);
#endif
#ifdef SD_ALPHA
    a = textureLod(in_alphaTex, tc, 0).a;
#else
    a = val * alphaMult;
#endif
    return vec4(c, a);
}

vec3 cube2tex(vec3 p)
{
    return (p + vec3(1.0)) * 0.5;
}

// (value, alpha, 0, 0)
vec4 voxel(vec3 p)
{
#ifdef SD_DEFORM
    p += deformMult * textureLod(in_deformTex, p, 0).xyz;
#endif
    //return vec4(p, 1.0);
    
    float v = 0.0;
#ifdef SD_DERIV
    float q = deriv_fast2(p);
    v += q;
    // enhance contrast for rendering
    {
        v = smoothstep(0.0, 1.0, 0.3+v);
        for(int i = 0; i < 5; ++i)
            v = smoothstep(0.0, 1.0, 0.06+v);
        v *= step(0.2, v);
    }
    v *= step(0.6, plain(p)) * 100.0;
#else
    float q = plain(p);
#endif
#ifdef SD_PLAIN  
    v += q;
#endif
    return getcolor(p, v);
}

vec4 raymarch(vec3 start, vec3 dir, float thickness, out float marchperc)
{
    vec4 accu = vec4(0.0, 0.0, 0.0, 0.0);
    float d = 0.0;
    float step = samplingDist;
    
#if MAX_STEPS
    thickness = min(thickness, step * float(MAX_STEPS));
#endif

    for( ; d < thickness; d += step)
    {
        vec3 p = cube2tex(start + dir*d);
        vec4 c = voxel(p);
#ifdef SD_OVER_OP
        c.a *= step;
        accu.rgb += (1.0 - accu.a) * c.a * c.rgb;
        accu.a += c.a;
        if(accu.a > ALPHA_LIMIT)
        {
            //accu *= vec4(1.2, 0.2, 0.2, 1.0); // show where early termination occurs
            accu.a = 1.0;
            break;
        }
#else
        accu += c * step;
#endif
    }
    marchperc = d / thickness;
    return accu;
}

void main()
{
    vec2 tex_coord = ((vertexPosTransformed.xy / vertexPosTransformed.w) + 1.) * .5;
    vec4 lookup = textureLod(rayExitTex, tex_coord, 0);
    
    float entryDepth = vertexPosOriginal.w;
    float exitDepth = lookup.w;
    // Prevent rays from going backwards at the edges of objects.
    // This causes some trouble with the ray direction and thickness,
    // resulting in hangs or driver crashes.
    if(entryDepth >= exitDepth)
    {
        discard;
        //color = vec4(0.0);
        return;
    }

    vec3 rayExit = lookup.xyz;
    vec3 rayEnter = vertexPosOriginal.xyz;

    vec3 dir = rayExit-rayEnter;
    float thickness = length(dir);
    
    float marchperc = 0.0;
    vec4 c = raymarch(rayEnter, normalize(dir), thickness, marchperc);

#ifdef DEBUG_BOUNDARIES
    c += borderColor * (1.0 - step(borderSize, thickness));
#endif

#ifdef DEBUG_THICKNESS
    //c = vec4(vec3(thickness), 1.0); // check how far we marched
    c = vec4(vec3(marchperc), 1.0);
#endif
#ifndef SD_UNCLAMPED
    c = clamp(c, 0.0, 1.0);
#endif
    color = c;
}

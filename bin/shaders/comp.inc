// This file is included before every compute shader.

uint _beginIndex(uint dummy)   { return gl_GlobalInvocationID.x; }
uvec2 _beginIndex(uvec2 dummy) { return gl_GlobalInvocationID.xy; }
uvec3 _beginIndex(uvec3 dummy) { return gl_GlobalInvocationID.xyz; }

void _incIndex(inout uint i, uvec3 inc)  { i += inc.x; }
void _incIndex(inout uvec2 i, uvec3 inc ) { i += inc.xy; }
void _incIndex(inout uvec3 i, uvec3 inc) { i += inc; }

bool _inBounds(uint i, uint sz) { return i < sz; }
bool _inBounds(uvec2 i, uvec2 sz) { return all(lessThan(i, sz)); }
bool _inBounds(uvec3 i, uvec3 sz) { return all(lessThan(i, sz)); }

// Can't use gl_WorkGroupSize in a function before the layout() qualifier is used, so all uses are macro-ized...
#define _mainWorkLoop(decl, i, sz) for(decl = _beginIndex((sz)); _inBounds(i, (sz)); _incIndex(i, (gl_NumWorkGroups * gl_WorkGroupSize)))


/* Usage:
void main()
{
   uvecN size = ...;
   
   mainWorkLoopN(i, size) { ...do whatever at position i... }
   
   -OR-
   
   uvecN i;
   mainWorkLoop(i, size) { ...do whatever at position i... }
   
*/
#define mainWorkLoop(i, sz) _mainWorkLoop(i, i, sz)
#define mainWorkLoop1(i, sz) _mainWorkLoop(uint i, i, sz)
#define mainWorkLoop2(i, sz) _mainWorkLoop(uvec2 i, i, sz)
#define mainWorkLoop3(i, sz) _mainWorkLoop(uvec3 i, i, sz)


// -----------------------------------------------

uniform uvec3 multiDispatchId = uvec3(0,0,0); // used by C++: ComputeShader::dispatchComputeMulti()

uvec3 _multiDispatchOffs(uvec3 launchsize)
{
    return launchsize * multiDispatchId;
}

bool _doWorkBlockMulti(out uint i, uint sz, uvec3 launchsize)
{
    i = gl_GlobalInvocationID.x + _multiDispatchOffs(launchsize).x;
    return _inBounds(i, sz);
}
bool _doWorkBlockMulti(out uvec2 i, uvec2 sz, uvec3 launchsize)
{
    i = gl_GlobalInvocationID.xy + _multiDispatchOffs(launchsize).xy;
    return _inBounds(i, sz);
}
bool _doWorkBlockMulti(out uvec3 i, uvec3 sz, uvec3 launchsize)
{
    i = gl_GlobalInvocationID + _multiDispatchOffs(launchsize);
    return _inBounds(i, sz);
}

/* Usage:
void main()
{
    uvecN i, size = ...;
    if(multiDispatchBlock(i, size))
        // do whatever at position i
}
*/
#define multiDispatchBlock(i, sz) _doWorkBlockMulti(i, sz, gl_NumWorkGroups * gl_WorkGroupSize)


// This is the define you usually want to use
/* Usage:
void main()
{
    uvecN i, size = ...;
    AUTOTILE(i, size)
        // do whatever at position i
}
*/
#define AUTOTILE(i, sz) if(multiDispatchBlock(i, sz))

#define GLOBAL_GROUP_IDX() (gl_WorkGroupID.z * gl_NumWorkGroups.x * gl_NumWorkGroups.y + gl_WorkGroupID.y * gl_NumWorkGroups.x + gl_WorkGroupID.x)

#define LOCAL_SIZE() (gl_WorkGroupSize.x * gl_WorkGroupSize.y * gl_WorkGroupSize.z)


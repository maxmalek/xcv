#version 430 core

in vec4 pass_color;
out vec4 outColor;

void main()
{
    outColor = pass_color;
}


#version 330 core

uniform mat4 proj;

in vec4 in_vertex;
in vec4 in_vertexColor;

out vec4 fragColor;

vec4 testcolor(in float v)
{
    v = clamp(v, 0.0, 1.0);
    vec3 c = vec3(1.0-v, v, v*0.5);
    float len = length(c.rg); // this intentionally ignores the blue channel
    return vec4(c / len, 1.0);
}

void main()
{
    gl_Position = proj * vec4(in_vertex.xyz, 1.0);
    gl_PointSize = in_vertex.w;
    fragColor = in_vertexColor;
    //fragColor = testcolor(in_vertex.w);
}

-- Main file for Lua init. Loads extra Lua-defined classes and extensions for C++-defined classes.
-- This file is fully reloadable.
-- To reload all Lua classes and update existing objects, call reload() (see below).

-- Make the GC very aggressive
-- Prevents large objects (such as GPU buffers) from lingering around for too long.
collectgarbage("setstepmul", 400)
collectgarbage("setpause", 50)

local req = {
    "strict",
    "luacore/_all",
}

-- Run tests at startup?
if _DEBUG and io.exists("test/_all.lua") then
    table.insert(req, "test/_all")
end

debugprint("Lua init...")

local function oops(s)
    local msg = "INIT ERROR:\n" .. s .. "\n\nTry again?"
    if rawget(_G, "errorboxYesNo") then -- use the fancier version if available
        return errorboxYesNo(msg, 1), msg
    end
    return os.msgboxYesNo(msg), msg
end

local function trycall(f)
    while true do
        local ok, tryagain, err = xpcall(f, oops)
        if ok then
            return true
        elseif not tryagain then
            break
        end
    end
end

local export = {}

local function patchobj(k, o, reg)
    assert(o.__class)
    local n = o.__name
    local newclass = reg[n] -- get class name from registry
    if newclass --[[and o.__class ~= newclass]] then
        devprint("patch obj " .. tostring(k) .. " (" .. n .. " " .. tostring(o) .. ") => " .. newclass.__name)
        switchclass(o, newclass)
    else
        errprint("Class not in registry: " .. n)
    end
    if o.onScriptReload then
        o:onScriptReload()
    end
end

local function patchCObjTab(objs, reg)
    for k, o in pairs(objs) do
        if isValidObj(o) then
            patchobj(k, o, reg)
        end
    end
end

local function patchLuaObjTab(objs, reg)
    for o, _ in pairs(objs) do
        if type(o) == "table" then
            patchobj("[Lua]", o, reg)
        end
    end
end


function patchobjs()
    local reg = debug.getregistry()
    devprint("Patching normal objects")
    patchCObjTab(reg[".Objs"], reg)
    devprint("Patching persistent objects")
    patchCObjTab(reg[".PObjs"], reg)
    devprint("Patching plain Lua objects")
    patchLuaObjTab(reg[".LObjs"], reg)
    devprint("Done patching objects")
end

function reload()
    local initf, initerr = loadfile("init.lua")
    if not initf then
        logerror("Reloading init.lua failed:\n" .. initerr)
        return
    end
    for k, _ in pairs(package.loaded) do
        package.loaded[k] = nil
    end
    if not trycall(initf) then
        return
    end
    -- Update class instances
    trycall(patchobjs)
end

local function _init()
    export.reload = reload
    for i = 1, #req do
        require(req[i])
    end
end

if not trycall(_init) then
    os.exit(1)
end

-- C++ API function
local function onError(msg)
    errorbox(msg, 2)
end

-- C++ API function
local function onQuit()
    if G.saveConfig then -- fully started up?
        if not saveConfig() then
            errorbox("Failed to save config")
        end
    end
end

-- FIXME: temporary namespace until it's decided where this should go
G.init = export
G.onQuit = onQuit
G.onError = onError
G.onScriptReload = reload


collectgarbage()

debugprint("Lua init done")

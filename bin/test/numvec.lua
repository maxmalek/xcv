local v41 = numvec.Pack(glsl.vec4, 1,2,3,0)
local vec4 = numvec.new(glsl.vec4, 10)

devprint(v41)
devprint(vec4)

devprint(v41:unpack())

local a = numvec.Pack(glsl.vec4, 1,1,1,20,  1,2,3,0):apply(glsl.ivec4, function(x,y,z,w) return x, y*2, z*3, w-10 end)
devprint(a:unpack(1))
devprint(a:unpack(2))

local mv = numvec.new(glsl.mat4, 100000) -- bunch of matrices
mv:mscale(glsl.vec3, 1.5, 2, 5)
mv:mul(mv)
mv:mul(glsl.float, 10)

devprint(mv:unpack())

local vi = numvec.Pack(glsl.ivec2, 1,1,  2,2,  3,-5 )
local rx, ry

rx, ry = vi:reduce(numvec.Add)
assert(rx == 6 and ry == -2)

rx, ry = vi:reduce(numvec.Mul)
assert(rx == 6 and ry == -10)

rx, ry = vi:reduce(numvec.Min)
assert(rx == 1 and ry == -5)

rx, ry = vi:reduce(numvec.Max)
assert(rx == 3 and ry == 2)

local inf = 1/0
local empty = numvec.Pack(glsl.float)
assert(#empty == 0)
assert(empty:reduce(numvec.Min) == inf)
assert(empty:reduce(numvec.Max) == -inf)

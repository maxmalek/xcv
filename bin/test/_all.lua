local function inc(fn)
    return dofile("test/" .. fn .. ".lua")
end

local failed = {}
local current
local function fail(s)
    errprint("OOPS:", s)
    table.insert(failed, "*** Test [" .. current .. "] failed: " .. s)
end

local function test(which)
    current = which
    if xpcall(inc, fail, which) then
        debugprint("Test [" .. which .. "] passed")
    end
end

-------------------------------------------------------

test "util"
test "gpu"
test "numvec"

if #failed > 0 then
    os.msgbox(table.concat(failed, "\n"), "Self-tests failed")
end

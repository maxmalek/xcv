do
    local a, b, c = "A", "B", "C"
    assert(("$a/$b $c"):expand() == "A/B C")
end

local function fail1()
    local a, b = "A", "B"
    ("$a/$b $c"):expand()
end

assert(not pcall(fail1)) -- must fail to expand c

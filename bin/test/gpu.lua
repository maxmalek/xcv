
local b = gpu.gpubuf.new(1024 * 1024, "u")
b.testval = 123 -- varaible binding test

local src = [[
#version 430
layout(local_size_x = 16, local_size_y = 8, local_size_z = 8) in;
void main() {};
]]
b:uploadBytes(src) -- buffer update test

gpu.computeshader.Create(src):dispatch(1) -- compilation test

gpu.computeshader.Create(src .. [[
#if (defines_work != 123) && !defined(nope)
#error Defines test fail
#endif
]], { defines_work = 123 }):dispatch(1) -- #define passing test


-- actual computation + uniform passing + buffer passing test
local sh1 = gpu.computeshader.Create([[
#version 430
layout(local_size_x = 128) in;
uniform float val;
layout(std430, binding = 3) restrict writeonly buffer OutBuf
{
    float p[];
} outbuf;
void main() { outbuf.p[gl_LocalInvocationIndex] = val + float(gl_LocalInvocationIndex); };
]]):bless()
local x, y, z = sh1:localsize()
local b = gpu.gpubuf.new(x * 4, "r")
sh1.val = 42
sh1.OutBuf = b
sh1:dispatch(1)
assert(sh1:getany("val") == 42)
assert(sh1.val == 42)

local N = shadergraph.node

local tins = table.insert

--[=[

-- Build a command list from a set of sinks (given as {[sink] = true, ...} table)
-- The list contains one table per command, in the format:
-- { "command", param1, ... }
-- Note that is is NOT optimized for parallelism at this point.
-- However, calculations are eliminated, and temporaries are purged when no longer needed.
local function buildPipelineCmds(sinks)
    local seen = {}
    local stack = {}
    local todo = {}
    
    -- table of tables. key is node, value is table of things to insert after the node
    local after = table.makeautoextend()
    
    local function ins(parent, node, connName)
        if not seen[node] then
            if parent then -- only insert non-sinks here
                local ap = after[parent]
                tins(ap, { "clear", node, connName })
                local w = ap.waitfor
                if not w then
                    w = {}
                    ap.waitfor = w
                end
                w[node] = true
            end
            tins(todo, node)
            seen[node] = true
        end
        tins(stack, node)
    end
    
    for n, _ in pairs(sinks) do
        assert(n.isSink)
        ins(nil, n)
    end
    
    while true do
        local n = table.remove(todo)
        if n then
            for ch, connName in pairs(n.childnodes) do
                ins(n, ch, connName)
            end
        else
            break
        end
    end
    debugprint("Need to recalc " .. table.count(seen) .. " nodes")
    
    -- optimize stack and remove redundant calculation
    -- generally we want to run a computation ONCE (the first time it's needed),
    -- and destroy temporaries as soon as they are no longer needed.
    -- It would be best to optimize the life-time of all temporaries globally,
    -- but that isn't done here.
    table.clear(seen)

    local list = {}
    -- Go through stack in reverse order, so that leafs are processed first
    for i = #stack, 1, -1 do
        local node = stack[i]
        if not seen[node] then
            seen[node] = true
            -- keep it
            local pending = rawget(after, node)
            if pending then
                local w = pending.waitfor
                if w then
                    local ww = { "wait" }
                    for nn, _ in pairs(w) do
                        tins(ww, nn)
                    end
                    tins(list, ww)
                end
            end
            tins(list, { "exec", node })
            if pending then
                for j = 1, #pending do
                    tins(list, pending[j])
                end
            end
        end
    end
    
    for i = 1, #list do
        local e = list[i]
        print("PIPELINE", table.unpack(e))
    end 

    return list
end

local pipelineDispatch = {
    exec = function(node)
        node:updateData()
    end,
    wait = function(...) -- list of nodes
        -- waiting for completion is only interesting for multi-gpu, which is NYI
    end,
    clear = function(node, connName)
        --node:clearOutput(connName)
    end,
}

-- TODO: might want to split pipeline building and pipeline execution into 2 parts,
-- and only re-build when necessary
local function doPipeline(sinks)
    if not next(sinks) then
        return
    end
    local cmds = buildPipelineCmds(sinks)
    debugprint("Pipeline start: " .. #cmds .. " entries")
    for i = 1, #cmds do
        local e = cmds[i]
        debugprint("PIPELINE", table.unpack(e))
        --pipelineDispatch[e[1]](table.unpack(e, 2))
    end 
end

]=]


function N:_calculate()
    --print("CALC! " .. tostring(self))
    local inputs = self:_getInputsTable()
    local ret
    if CONFIG.debugmode then
        local gt = self.gputimer
        if not gt then
            gt = gpu.gputimer.new()
            self.gputimer = gt
        end
        if not gt:capturing() then
            ret = gt:call(self.recalc, self, inputs)
        else
            ret = self:recalc(inputs)
        end
    else
        ret = self:recalc(inputs)
    end
    local Cout = assert(self.Cout)
    if ret == nil then
        -- nothing to do
    elseif type(ret) == "table" then
        -- merge returned table with self.RESULTS table
        for k, v in pairs(ret) do
            --print("Assign [" .. k .. "]: " .. tostring(v))
            self.RESULTS[k] = v
            if not Cout[k] then
                errprint("Warning: Node " .. tostring(self) .. " returned [" .. k .. "], but this is not a defined output")
            end
        end
    else
        local k = next(Cout)
        if not k then
            errprint("Warning: Node " .. tostring(self) .. " returned single value, but there is no output")
        elseif next(self.Cout, k) then -- > 1 output?
            error("Node " .. self:getName()
                .. " returned a single value but does not have exactly one output, not sure which one to assign to. Return a table with names instead. (E.g. { outTex = tex, outBuf = buf } ) ")
        else -- exactly one output
            --print("Got single object, assign to [" .. k .. "]: " .. tostring(ret))
            -- single return value is fine, as long as we have only a single output
            self.RESULTS[k] = ret
        end
    end
end


-- to be overriden if necessary, does nothing by default
function N:changed() -- called when a parameter knob is changed
end

-- to be defined -- automatically inferred if possible (if the shader is simple enough)
--function N:recalc() -- invoke shader and return table of output data { varName => object }
--end

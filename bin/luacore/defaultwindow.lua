local BASE = sys.syswindow
local W = newclass("basicwindow", BASE)
G.basicwindow = W

function W:onMouseMove(x, y, rx, ry, buttons)
    if self.backpanel then
        self.backpanel:mouseMove(rx, ry, buttons)
    end
end
function W:onMouseWheel(change)
    if self.backpanel then
        self.backpanel:mouseWheel(change)
    end
end
function W:onMouseClick()
end

function W:onRender()
    if self.backpanel then
        self.backpanel:renderToScreen()
    end
end


------------------------------------

local M = newclass("mainwindow", W)
G.mainwindow = M

function M.Open(...)
    return M.OpenClass(M, ...)
end

function M.new()
    local back = render.simpleobjectpanel.new(basicobjcontrol_bg)
    local self = W.new()
    switchclass(self, M)
    self.backpanel = back
    back.parentWindow = self
    --self.jobw  = self:newJobsView()
    back.ortho = true
    back.dtest = false
    back.dwrite = false
    self.detailpane = ui.detailpaneClass.new(self)
    return self
end

function M:onUpdate(dt)
    self.__baseclass.onUpdate(self, dt)
    
    if not self.hideUI then
        ui.mainbar.drawBar(self, dt)
    end
end

function M:onResize(x, y)
    self.backpanel:setWindowSize(x, y)
    self.backpanel:layout()
end

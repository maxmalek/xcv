-- framebuffer with attached fragment shader that renders to self
-- Somewhat compatible with Shadertoy -- https://www.shadertoy.com/
-- Major difference is that images here are flipped, which involves some parameters to be changed.
-- Also: Sound & cube maps are not supported
-- This difference is completely transparent to the shader though.


local unpack = table.unpack
local max = math.max
local abs = math.abs

local C = newclass("fragrender", assert(gpu.framebuffer))
gpu.fragrender = C

local _ORTHO = table.concat({glm.ortho(0,1,1,0)}, ", ")

local VTXSRC = ([[#version 430 core
const mat4 proj = mat4($_ORTHO);
in vec4 in_vertex;
void main()
{
    gl_Position = proj * in_vertex;
}
]]):expand()

local PREFIX = [[
#version 450 core
uniform vec3      iResolution;           // viewport resolution (in pixels)
uniform float     iTime;                 // shader playback time (in seconds)
uniform float     iTimeDelta;            // render time (in seconds)
uniform int       iFrame;                // shader playback frame
uniform float     iChannelTime[4];       // channel playback time (in seconds)
uniform vec3      iChannelResolution[4]; // channel resolution (in pixels)
uniform vec4      iMouse;                // mouse pixel coords. xy: current (if MLB down), zw: click
uniform sampler2D iChannel0;             // input channel.
uniform sampler2D iChannel1;             // input channel.
uniform sampler2D iChannel2;             // input channel.
uniform sampler2D iChannel3;             // input channel.
uniform vec4      iDate;                 // (year, month, day, time in seconds)
//uniform float     iSampleRate;           // sound sample rate (i.e., 44100)
const float iSampleRate = 44100; // unsupported
#line 1]]

local NEWCODE = [[
float rand01(vec2 n) // uniform distribution, in [0, 1)
{
    return fract(sin(dot(n.xy, vec2(12.9898, 78.233)))*43758.5453);
}

void mainImage(out vec4 fragColor, in vec2 fragCoord)
{
    vec2 uv = fragCoord.xy / iResolution.xy;
    float t = fract(iTime * 0.05);
    float v = rand01(uv + t);
    fragColor = vec4(vec3(v), 1.0);
}
]]

local POSTFIX = [[
out vec4 out_fragColor;
void main()
{
    vec4 fragColor = vec4(vec3(0.0), 1.0);
    mainImage(out_fragColor, vec2(gl_FragCoord.x, iResolution.y - gl_FragCoord.y));
}
]]

local quad = assert(gpu.meshes.quad)
local mat4 = glsl.mat4
local floor = math.floor
local min = math.min
local max = math.max
local toint = math.tointeger

local function int(x)
    return toint(floor(x))
end

function C.new(x, y, flags, code)
    flags = flags or "l"
    local self = gpu.framebuffer.new(x,y, flags)
    
    switchclass(self, C)
    self.time = 0.0
    self.frame = 0
    self.tex = self:texture()
    self.mouse = {0,0,0,0}
    self.dt = 0
    self.wasclicked = false
    self.dirty = true
    self.date = {0,0,0,0}
    self._shader = gpu.simpleshader.new()
    
    assert(self:setCode(code or NEWCODE))

    return self
end

local function R(self)
    local sh = self._shader
    sh:bind()
    quad:render("t", false, false, false)
end

function C:redraw()
    local sh = self._shader
    local rx, ry = self:size()
    local d = os.date("*t")
    local t = self.time
    local mx, my, mz, mw = unpack(self.mouse)
    my = ry-my
    if mw < 0 then
        mw = -abs(ry + mw)
    else
        mw = ry-mw
    end
    local dY, dM, dD, dS = d.year, d.month, d.day, (d.hour * 24 + d.min) * 60 + d.sec + (t % 1.0) -- HACK: There's no Lua API for sub-second current time; approximate by using frame time
    local dd = self.date
    dd[1], dd[2], dd[3], dd[4] = dY, dM, dD, dS 
    sh:setuniform("iTime", t)
    sh:setuniform("iTimeDelta", self.dt)
    sh:setuniform("iFrame", self.frame)
    sh:setuniform("iResolution", rx, ry, 1.0)
    sh:setuniform("iDate", dY, dM, dD, dS)
    sh:setuniform("iMouse", mx, my, mz, mw)
    self:captureBlend(gpu.BLEND_REPLACE, R, self)
    self.dirty = false
end

function C:reset()
    self.frame = 0
    self.time = 0
    self.dt = 0
end

function C:getResultTex()
    if self.dirty then
        self:redraw()
    end
    return self.tex
end

function C:update(dt)
    self.time = self.time + dt
    self.dt = dt
    self.frame = self.frame + 1
    self.dirty = true
end

function C:setCode(code)
    self._code = assert(code)
    local frag = PREFIX .. "\n" .. code .. "\n" .. POSTFIX
    local sh = self._shader
    local ok, err = sh:load(VTXSRC, frag)
    if ok then
        return sh
    end
    return nil, err
end

function C:getCode()
    return self._code
end

function C:updateMouse(mx, my)
    if mx then
        local pm = self.mouse
        if not self.wasclicked then
            pm[3], pm[4] = mx, my
            self.wasclicked = true
        end
        pm[1], pm[2] = mx, my
        self.dirty = true
    elseif self.wasclicked then
        local pm = self.mouse
        pm[3], pm[4] = -pm[3], -pm[4]
        self.dirty = true
        self.wasclicked = false
    end
end

function C:drawUI()
    --imgui.ImageKeepAspectFit(self.tex, 0.0, 1.0, 1.0, 0.0, 1.0,0.0,0.0,1.0)

    local cx, cy = imgui.GetCursorPos()
    local sx, sy = imgui.GetCursorScreenPos()
    local iw, ih = imgui.ImageKeepAspectFit(self:getResultTex(), nil,nil,nil,nil, 1.0,0.0,0.0,1.0)
    imgui.SetCursorPos(cx, cy)
    imgui.InvisibleButton("dragButton", iw, ih) -- captures input on the image
    
    local down = imgui.IsMouseButton()
    if imgui.IsItemActive() then
        local mx, my = imgui.GetMousePos()
        local rx, ry = self:size()
        local fx, fy = rx / iw, ry / ih
        self:updateMouse((mx-sx)*fx, (my-sy)*fy) 
    elseif self.wasclicked then
        self:updateMouse()
    end
end

local Text = assert(imgui.Text)

local function printvar(sh, name, fmt, ...)
    Text(name .. " = " .. fmt:format(...) .. ";")
end

function C:drawInputs()
    local sh = self._shader
    printvar(sh, "float iTime", "%.3f", self.time)
    printvar(sh, "float iTimeDelta", "%.4f", self.dt)
    printvar(sh, "int iFrame", "%d", self.frame)
    printvar(sh, "vec3 iResolution", "(%d, %d, 0)", self:size())
    printvar(sh, "vec4 iDate", "(%d, %d, %d, %.4f)", unpack(self.date))
    printvar(sh, "vec4 iMouse", "(%.1f, %.1f, %.1f, %.1f)", unpack(self.mouse))
    if imgui.SmallButton("Reset") then
        self:reset()
    end
end

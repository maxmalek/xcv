-- various algorithms

local log = math.log
local floor = math.floor
local toint = math.tointeger
local min = math.min
local max = math.max
local tins = table.insert
local unpack = table.unpack
local setmetatable = setmetatable

local algo = {}
G.algo = algo

local function ilog2(x)
    return toint(floor(log(x, 2)))
end

-- Batcher's Merge-Exchange sort
-- Via http://pages.ripco.net/~jgamble/nw.html and https://stackoverflow.com/questions/4402944/batchers-merge-exchange-sort
local function _batcher(n)
    local p0 = 1 << ilog2(n)
    assert(p0 ~= 0, "overflow")
    local p, t, idx = p0, {}, 1
    while p > 0 do
        local q, r, d = p0, 0, p
        while d > 0 do
            for i = 0, n - d - 1 do
                if (i & p) == r then
                    t[idx] = {i, i+d}
                    idx = idx + 1
                end
            end
            d = q - p
            q = q >> 1
            r = p
        end
        p = p >> 1
    end
    return t
end

local autoExtendMeta = { __index = function(t, k) local x = {}; t[k] = x; return x; end }

-- Group independent, parallel operations into consecutive sub-groups
local function _group(t)
    local used = setmetatable({}, autoExtendMeta)
    local res = setmetatable({}, autoExtendMeta)
    for i = 1, #t do
        local col, u = 1
        local e = t[i]
        local a, b = unpack(e)
        while true do
            u = used[col]
            if not (u[a] or u[b]) then
                break
            end
            col = col + 1
        end
        u[a], u[b] = true, true
        tins(res[col], e)
    end
    setmetatable(res, nil)
    return res
end

-------------------------------------------------------------------

function algo.generateSortNetwork(n, luaidx)
    local t = _batcher(n)
    if luaidx then
        for i = 1, #t do
            local e = t[i]
            e[1], e[2] = e[1]+1, e[2]+1
        end
    end
    return _group(t)
end

algo.generateSortNetwork(8)

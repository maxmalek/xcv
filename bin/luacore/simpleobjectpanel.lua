local C = newclass("simpleobjectpanel", render.scenepanel)
render.simpleobjectpanel = C

local floor = math.floor
local sqrt = math.sqrt
local max = math.max

function C.new(CtrlClass, x, y, fbflags) -- frame buffer flags are optional
    local panel = C._new(render.scene.new(), x, y, fbflags)
    local ctrl = CtrlClass.new()
    panel._ctrl = ctrl
    panel._objs = {}
    panel.xspace = 5
    panel.yspace = 5
    panel.singleobj = false
    
    function ctrl.onChange()
        panel.dirty = true
    end
    
    return panel
end

function C:mouseMove(rx, ry, buttons)
    local sw, sh = self:size()
    local ww, wh = self:getWindowSize()
    local w = assert(ww or sw)
    local h = assert(wh or sh)
    if self._ctrl:mouseMoveInViewport(rx, ry, buttons, w, h) then
        self.dirty = true
    end
end

function C:mouseWheel(change)
    if self._ctrl:mouseWheel(change) then -- didn't eat the wheel event? -> ok, we handle it
        local x, y, z = self.cam:getpos()
        z = z * (1.0 - change * 0.1)
        z = max(0.01, z)
        self.cam:setpos(x, y, z)
        self.dirty = true
    end
end

C.getViewport = C.size

function C:add(obj)
    if not self._objs[obj] then
        -- Instead of the actual object, register an alias.
        -- We want to keep the object's transformation local to this panel,
        -- and NOT apply it to the object directly.
        local alias = render.renderobject.new()
        self._objs[obj] = alias
        table.insert(self._objs, alias)
        self._ctrl:addObj(alias)
        self.scene:add(alias)
        alias:addChild(obj)
        self.dirty = true
    end
end

function C:removeAll()
    table.clear(self._objs)
    self._ctrl:removeAll()
    self.scene:removeAll()
    self.dirty = true
end

function C:remove(obj)
    local alias = self._objs[obj]
    if alias then
        self._objs[obj] = nil
        local idx = table.getrindexi(self._objs, alias)
        if idx then
            table.remove(self._objs, idx)
        end
        self._ctrl:removeObj(alias)
        self.scene:remove(alias)
        self.dirty = true
    end
end

-- kinda shabby...
function C:layout()

    local objs = self._objs
    local n = #objs
    if n == 0 then
        return
    end
    local w, h = self:size()
    local aspect = w / h
    local nx = max(1, floor(sqrt(n) * aspect)) -- columns
    local ny = max(1, (n + (nx - 1)) // nx) --- rows
    devprint("Distributing " .. n .. " objects into " .. nx .. " x " .. ny .. " grid")
    local xspace = self.xspace
    local yspace = self.yspace
    
    local i = 0
    local done
    local xstart = xspace * -0.5 * (nx - 1)
    local ystart = yspace * -0.5 * (ny - 1)
    local x, y = xstart, ystart
    while not done do
        for _ = 1, nx do
            i = i + 1
            local obj = objs[i]
            if not obj then
                done = true
                break
            end
            
            obj:setPos(x, y, 0)
            x = x + xspace
        end
        y = y + yspace
        x = xstart
    end
    self.dirty = true
end

function C:drawKnobs()
    if not self.singleobj then
        imgui.Text("Object spacing")
        local c, xspace = imgui.Slider("X", self.xspace, 0, 20)
        if c then
            self.xspace = xspace
            self:layout()
        end
        local c, yspace = imgui.Slider("Y", self.yspace, 0, 20)
        if c then
            self.yspace = yspace
            self:layout()
        end
    end
    self.__baseclass.drawKnobs(self)
end

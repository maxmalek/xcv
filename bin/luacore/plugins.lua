local PLUGINPATH = "./plugins/"
local PLUGINMATCH = "*." .. plugin.ext

if not plugin._loadedSrc then
    plugin._loadedSrc = {}
end

local function loadList(list, wc, cb)
    for _, f in pairs(list) do
        if f:lower():wildcard(wc) then
            local loaded = plugin.isloaded(f)
            if loaded then
                debugprint("Already loaded plugin [" .. f .. "]")
            else
                print("Load plugin [" .. f .. "]")
                if cb then
                    cb(f, nil)
                end
                local ok = plugin.load(f)
                if not ok then
                    errprint("Failed to load plugin [" .. f .. "]")
                end
                if cb then
                    cb(f, ok or false)
                end
            end
        end
    end
end

local function loadDir(s, wc, cb)
    debugprint("Load plugins from [" .. s .. "]")
    local list = io.filesfull(s)
    if list then
        loadList(list, wc, cb)
    else
        debugprint("No such directory")
    end
end

local function _searcher_src(name)
    local fn = PLUGINPATH .. name .. ".lua"
    local f, err = loadfile(fn, "t") -- only allow loading source code, NOT binary
    if f then
        infoprint("Load Lua plugin [" .. fn .. "]")
        plugin._loadedSrc[name] = true
        return f
    end
    return "\n" .. err
end

-- Unload source code plugins (will be re-loaded as required)
for k, _ in pairs(plugin._loadedSrc) do
    debugprint("Unload Lua plugin: " .. k)
    package.loaded[k] = nil
end

-- patch package.loaders to check included plugins before hitting the file system
-- This assumes that the package.preload searcher is first
-- (See http://www.lua.org/manual/5.3/manual.html#pdf-package.searchers)
local S = package.searchers
for i = 1, #S do
    if S[i] == plugin._searcher or S[i] == _searcher_src then
        table.remove(S, i)
        break
    end
end
table.insert(S, 2, plugin._searcher)
table.insert(S, 2, _searcher_src)

local function loadPlugins(show)
    local callback, w
    if show then
        local lines
        callback = function(f, ok)
            if not w then
                w = sys.syswindow:Open(400, 300)
                function w:onDrawUI()
                    for i = 1, #lines do
                        imgui.TextColor(table.unpack(lines[i]))
                    end
                end
                w:setTitle("Loading plugins...")
                lines = {{ "Loading plugins...", 1, 1, 1}}
            end
            if ok == nil then
                local e = { f .. " ... ", 1, 1, 1 }
                table.insert(lines, e)
            else
                local e = lines[#lines]
                e[1] = e[1] .. ((ok and " OK") or " FAIL")
                if ok then
                    e[2] = 0.1
                    e[3] = 1
                    e[4] = 0.2
                else
                    e[2] = 1
                    e[3] = 0.1
                    e[4] = 0
                end
            end
            w:update()
        end
    end

    -------------------------------------------
    
    loadDir(PLUGINPATH, PLUGINMATCH, callback)
    --loadDir(".", "plugin_" .. PLUGINMATCH, callback) -- let's not do this for now.
    
    ------------------------------------
    
    if w then
        w:destroy()
    end
end

loadPlugins(CONFIG.showLoadPlugins)


local C = newclass("scenepanel", render.texquad)
render.scenepanel = C
local parentRender = assert(render.texquad.onRender)
local parentRenderToScreen = assert(render.texquad.renderToScreen)
local parentRenderTexToScreen = assert(render.texquad.renderTexToScreen)
local mat4 = glsl.mat4
local floor = math.floor
local min = math.min
local max = math.max
local toint = math.tointeger

local function int(x)
    return toint(floor(x))
end

function C.new(scn, x, y, flags)
    local self = C._new()
    self._fbflags = flags
    self.fb = gpu.framebuffer.new(64,64) -- HACK: initialize to something; will be properly resized shortly
    self.cam = render.camera.new()
    self.cam:setpos(0, 0, 5+2)
    self.cam:lookat(0, 0, 0)
    self.cam:seteyedist(0.05)
    self.scene = scn
    self.dirty = true
    self._mat = numvec.mat4_identity:clone()
    self.dtest = false
    self.dwrite = false
    self._clearcolor = {0,0,0,0}
    assert(self._tint)
    
    self._ressel = resolutionselector.new(
        function(_, w, h)
            self:_onResize(w, h)
        end
    )
    
    if x then
        self:_resize(x, y)
    end
    
    return self
end

local function rsz(self, m, ...)
    local fb = self[m]
    if fb then
        fb:resize(...)
        return fb:texture()
    end
end

local function del(self, m)
    local o = self[m]
    if o then
        --o:delete()
        self[m] = nil
    end
end

function C:size()
    return self.fb:size()
end

function C:getWindowSize()
    return self._ressel:getWindowSize()
end

function C:setClearColor(r,g,b,a)
    local cc = self._clearcolor
    cc[1] = r or 0
    cc[2] = g or 0
    cc[3] = b or 0
    cc[4] = a or 1
end

function C:getClearColor()
    return table.unpack(self._clearcolor)
end

function C:resize(w, h)
    self._ressel:resize(w, h)
end

function C:setWindowSize(w, h)
    self._ressel:setWindowSize(w, h)
end

function C:_onResize(x, y, force)

    x = int(max(x or 0, 64))
    y = int(max(y or 0, 64))
    
    local oldw, oldh = self:size()
    if force or oldw ~= x or oldh ~= y then
    
        debugprint("scenepanel: Resize to physical (" .. x .. ", " .. y .. ")")
        
        self.cam:viewport(x, y)    
        
        del(self, "tex")
        del(self, "texL")
        del(self, "texR")
        
        self.tex = rsz(self, "fb", x, y, self._fbflags)
        self.texL = rsz(self, "fbL", x, y, self._fbflags)
        self.texR = rsz(self, "fbR", x, y, self._fbflags)

        self.dirty = true
    end
end

function C:useStereo(on)
    if on then
        self.fbL = gpu.framebuffer.new()
        self.fbR = gpu.framebuffer.new()
    else
        del(self, "fbL")
        del(self, "fbR")
    end
    local w, h = self:size()
    self:_onResize(w, h, true)
end

local function R(self, ...)
    return self.scene:render(...)
end

local function BLIT(self, tex, ...)
    -- using ortho matrix, so don't need to pass one
    self:_doRender(nil, false, tex, ...)
end

function C:redraw()
    self.dirty = false
    local r,g,b,a = table.unpack(self._clearcolor)
    if not self.fbL then
        -- "one eye"
        self.fb:clear(r,g,b,a)
        self.fb:captureBlend(gpu.BLEND_NORMAL, R, self, self.cam:getmatrixptr())
    else
        -- "two eyes"
        -- (This could be done with one intermediate framebuffer for both eyes, but since we want to keep
        -- render outputs for both eyes, use one for each eye and keep them until redraw.)
        self.fbL:clear(r,g,b,a)
        self.fbR:clear(r,g,b,a)
        
        self.fbL:captureBlend(gpu.BLEND_NORMAL, R, self, self.cam:getleftmatrixptr())
        self.fbR:captureBlend(gpu.BLEND_NORMAL, R, self, self.cam:getrightmatrixptr())
        
        -- ...apply tint and overwrite output fb...
        self.fb:captureBlend(gpu.BLEND_REPLACE, BLIT, self, self.texL, 1, 0, 0, 1)
        -- ... add the complementarily tinted version to the prev. output
        self.fb:captureBlend(gpu.BLEND_ADD, BLIT, self, self.texR, 0, 1, 1, 1)
    end
    
    self:onRedraw()
end

-- to be overwritten if necessary
function C:onRedraw()
end

function C:redrawIfNeed()
    if self.dirty then
        self:redraw()
        return true
    end
end

function C:getResultTex()
    self:redrawIfNeed()
    return self.tex
end

function C:onRender(...)
    self:redrawIfNeed()
    parentRender(self, ...)
end

local function _createView(self, callf, ...)
    local ch = basicwindow:Open(...) -- w, h, [full, displayID]
    local back = self
    ch.backpanel = back
    function ch:onRender()
        back[callf](back)
    end
    --function ch:onMouseMove(...)
    --    SELF:onMouseMove(...)
    --end
    --ch.parentWindow = self
    return ch
end

function C:openNormalView(...)
    return _createView(self, "renderToScreen", ...)
end
function C:openLeftView(...)
    return _createView(self, "renderLeftEyeToScreen", ...)
end
function C:openRightView(...)
    return _createView(self, "renderRightEyeToScreen", ...)
end

local function _popoutParams(self)
    return self:size()
end
local function _postPopout(self, win, title)
    return win
end

-- (fullscreen, displayID)
function C:popoutNormal(...)
    local win = self:openNormalView(_popoutParams(self))
    return _postPopout(self, win, ...)
end
function C:popoutLeft(...)
    local win = self:openLeftView(_popoutParams(self))
    return _postPopout(self, win, ...)
end
function C:popoutRight(...)
    local win = self:openRightView(_popoutParams(self))
    return _postPopout(self, win, ...)
end



function C:drawKnobs()
    local change
    
    local c, r, g, b, a = imgui.ColorChooser(self:getClearColor())
    if c then
        self:setClearColor(r,g,b,a)
        change = true
        self.dirty = true
    end
    imgui.SameLine()
    imgui.Text("BG color")
    
    change = self._ressel:drawUI() or change
    
    local c, s = imgui.Checkbox("Stereo?", not not self.fbL)
    if c then
        self:useStereo(s)
        change = true
    end
    --if s then
        local c, eyed = imgui.Slider("Stereo eye dist", self.cam:geteyedist(), 0, 0.4)
        if c then
           self.cam:seteyedist(eyed)
           self.dirty = true
           change = true
        end
    --end
    
    imgui.Separator()
    
    imgui.Text("Pop out")
    
    if imgui.Button("This") then
        self:popoutNormal()
    end
    imgui.SameLine()
    if imgui.Button("Left") then
        self:popoutLeft()
    end
    imgui.SameLine()
    if imgui.Button("Right") then
        self:popoutRight()
    end
    
    return change
end

function C:renderToScreen(flip)
    self:redrawIfNeed()
    parentRenderToScreen(self, flip)
end

function C:renderLeftEyeToScreen(flip)
    self:redrawIfNeed()
    parentRenderTexToScreen(self, self.texL or self.tex, flip)
end

function C:renderRightEyeToScreen(flip)
    self:redrawIfNeed()
    parentRenderTexToScreen(self, self.texR or self.tex, flip)
end

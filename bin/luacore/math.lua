local exp = math.exp

-- enter is at t[1]
local function createGaussHalfKernel(dim, sigma)
    local sum = 1.0
    local t = { sum }
    local m = -0.5 / (sigma * sigma)
    for i = 1, dim do
        local k = exp(i*i*m)
        t[i+1] = k
        sum = sum + 2.0 * k
    end
    -- normalize
    for i = 1, dim+1 do
        t[i] = t[i] / sum
    end
    return t
end

-- center is at t[dim+1]
local function createGaussKernel(dim, sigma)
    local t = createGaussHalfKernel(dim, sigma)
    table.move(t, 1, dim+1, dim+1)
    local n = #t
    for i = 1, dim do
        t[i] = t[n-i+1]
    end
    return t
end

math.createGaussHalfKernel = createGaussHalfKernel
math.createGaussKernel = createGaussKernel

-- 1-based modulo for positive numbers
function math.mod1(x, mod)
    return (((x + mod - 1) % mod) + 1)
end


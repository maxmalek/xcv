local next = next
local pairs = pairs
local ipairs = ipairs

local fun = {}

local ops = {
    lt = function(a, b) return a < b end,
    gt = function(a, b) return a > b end,
    le = function(a, b) return a <= b end,
    ge = function(a, b) return a >= b end,
    eq = function(a, b) return a == b end,
    ne = function(a, b) return a ~= b end,
    
    add = function(a, b) return a + b end,
    sub = function(a, b) return a - b end,
    mul = function(a, b) return a * b end,
    div = function(a, b) return a / b end,
    idiv = function(a, b) return a // b end,
    mod = function(a, b) return a % b end,
    pow = function(a, b) return a ^ b end,
    
    ["not"] = function(a) return not a end,
}
fun._ops = ops

function fun.op(n)
    return ops[n]
end

function fun.map(t, f, ...)
    local o = {}
    for i, e in pairs(t) do
        o[i] = f(e, ...)
    end
    return o
end

function fun.mapk(t, f, ...)
    local o = {}
    for i, e in pairs(t) do
        o[f(i, ...)] = e
    end
    return o
end

function fun.mapkv(t, f, ...)
    local o = {}
    for i, e in pairs(t) do
        local k, v = f(i, e, ...)
        o[k] = v
    end
    return o
end

function fun.map_inplace(t, f, ...)
    for i, e in pairs(t) do
        t[i] = f(e, ...)
    end
    return t
end

function fun.filter(t, f, ...)
    local o = {}
    for i, e in pairs(t) do
        if f(e, ...) then
            o[i] = e
        end
    end
    return o
end

function fun.filter_inplace(t, f, ...)
    for i, e in pairs(t) do
        if not f(e, ...) then
            t[i] = nil
        end
    end
    return t
end

function fun.filteri(t, f, ...)
    local o = {}
    local w = 1
    for i, e in ipairs(t) do
        if f(e, ...) then
            o[w] = e
            w = w + 1
        end
    end
    return o
end

function fun.filteri_inplace(t, f, ...)
    local w = 1
    for r, e in ipairs(t) do
        if f(e, ...) then
            t[w] = e
            w = w + 1
        end
    end
    for i = w, #t do
        t[i] = nil
    end
    return t
end






G.fun = fun

-- kitchen sink namespace for UI stuff

local ui = G.ui
if not ui then
    ui = {}
    G.ui = ui
end

local LOGLVLS = { "Error", "Normal", "Info", "Debug", "Developer" }

-- extension for the C++ UIConsole class
function gui.uiconsole:drawUI()
    imgui.Separator()
    if imgui.Button("Clear") then
        self:clear()
    end
    imgui.SameLine()
    imgui.Text("Level:")
    imgui.SameLine()
    local lvl = self:getLogLevel()
    local sel, idx = imgui.ItemWidth(100, imgui.Combo, "", lvl + 2, LOGLVLS, 5) -- first log level is -1 (Error)
    if sel then
        self:setLogLevel(idx - 2)
    end
    imgui.SameLine()
    imgui.Text("Filter:")
    imgui.SameLine()
    local changed, flt = imgui.ItemWidth(150, imgui.InputText, "##filter", self:getFilter() or "", 128)
    if changed then
        self:setFilter(flt)
    end
end

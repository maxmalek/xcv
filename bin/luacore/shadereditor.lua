local C = newclass("shadereditor")
G.shadereditor = C

function C.new()
    return registerObject(setmetatable({
        autorecomp = true,
        _lasterror = false,
        _timer = -1.0,
        _timerok = -1.0,
        _errlinesLUT = {},
        _codescrolly = 0,
    }, C))
end

-- to be overridden. expected to return (ok, error)
function C:onCompile(code)
    return nil, "shadereditor:onCompile() not specialized!"
end

local function compile(self, manual)
    local ok, err = self:onCompile(self._code)
    self._msg = err
    self._err = not ok
    local lut = self._errlinesLUT
    table.clear(lut)
    if ok then
        if manual then
            print("Shader compiled successfully")
        end
    else
        errprint("---- Shader compile error ----")
        errprint(err)
        if err then
            for it in err:gmatch("%((%d+)%)") do -- works with nvidia shader compiler error messages. TODO: AMD?
                lut[math.tointeger(it) or 0] = true
            end
        end
    end
    return ok, err
end

function C:setCode(code)
    self._code = assert(code)
    return compile(self)
end

function C:getCode()
    return self._code
end


local function recompile(self, manual)
    if compile(self, manual) then
        self._timerok = 0.5
    end
    self._timer = -1.
end


function C:update(dt)
    self._timerok = math.max(self._timerok - dt, 0.)
    if self._timer >= 0. then
        self._timer = self._timer - dt
        if self._timer <= 0. then
            recompile(self, false)
        end
    end
end

local function firstline(s)
    local pos = s:find("\n", 1, true)
    if pos then
        return s:sub(1, pos-1)
    end
    return s
end

local function padspace(s, n)
    local len = #s
    if len >= n then
        return s
    end
    return s .. (" "):rep(n-len)
end

local TextColor = imgui.TextColor
local TextDisabled = imgui.TextDisabled

local function TextRed(s)
    return TextColor(s, 1, 0.1, 0.1)
end

local STYLE_SIDETEXT  = { [imgui.Style_ItemSpacing] = {4, 0} }
local function _drawSideText(self, maxh)
    local scrolly = self._codescrolly
    local lineh = imgui.GetTextLineHeight()
    local badlines = self._errlinesLUT
    
    -- HACK: super crappy co-scrolling emulation because of the current limitations of InputTextMultiline().
    local begin, bfrac = math.modf(scrolly / lineh)
    local nlines = math.floor(maxh / lineh)
    
    local cx, cy = imgui.GetCursorPos()
    imgui.SetCursorPos(cx+bfrac, cy+4-(lineh*bfrac)) -- HACK: adjust Y position so it matches nicely
    
    for i = begin+1, begin+nlines do
        local s = tostring(i)
        if badlines[i] then
            TextRed(padspace(s .. ">", 4))
        else
            TextDisabled(padspace(s, 4))
        end
    end
end

function C:drawUI()
    
    local c, recomp = imgui.Checkbox("Auto rebuild", self.autorecomp)
    if c then
        self.autorecomp = recomp
    end
    if imgui.IsItemHovered() then
        imgui.Tooltip("Alternatively, press Ctrl+Space to compile.")
    end
    
    if imgui.IsWindowFocused() then
        local ctrldown = sys.getKeyState(224) or sys.getKeyState(228) -- key:LCtrl, key:RCtrl
        local _, press = sys.getKeyState(44) -- key:Space
        if ctrldown and press then
            recompile(self, true)
        end
    end
    
    local w, h = imgui.GetContentRegionAvail()
    
    if self._err then
        TextColor("ERROR: " .. firstline(self._msg), 1., 0.2, 0.2)
    elseif self._msg then
        TextColor("OK (warnings): " .. firstline(self._msg), 1., 1., 0.2)
    elseif self._timerok > 0. then
        TextColor("OK (Recompiled)", 0.3, 1, 0.3)
    else
        TextColor("OK",  0.3, 1, 0.3)
    end
    if self._msg and imgui.IsItemHovered() then
        imgui.Tooltip(self._msg)
    end
    
    --imgui.Child("codeeditor", 0, 0, true, "", function()
        --local nlines = 1
        --self._code:gsub("\n", function() nlines = nlines + 1 end)
        --local lineh = imgui.GetTextLineHeight()
        local hh = h-40 --math.max(nlines * lineh, h)
        
        imgui.ItemWidth(40, imgui.Group, imgui.ApplyStyle, STYLE_SIDETEXT, _drawSideText, self, hh)
        imgui.SameLine()
        local ch, s, scrollx, scrolly = imgui.InputTextMultiline("", self._code, 1024*1024, "t", w-30, hh)
        self._codescrolly = scrolly
        if ch then
            self._code = s
            if self.autorecomp then
                self._timer = 1 -- recompile after short time
            end
        end
        
    --end)
end

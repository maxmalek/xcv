local CONFIG = G.CONFIG or {}

local DEFAULT_NAME = "config.lua"

local function loadConfig(fn)
    fn = fn or DEFAULT_NAME
    if io.exists(fn) then
        local f, err = loadfile(fn)
        if f then
            local t = f()
            if not t then
                return true
            end
            if type(t) ~= "table" then
                return false, "Expected table, got " .. type(t)
            end
            for k, v in pairs(t) do
                CONFIG[k] = v
            end
            return true
        end
        return false, err
    end
    -- nil: no such file
end

local function loadConfigOrDie(fn)
    local ok, err = loadConfig(fn)
    if ok == false then -- nil: no file (this is fine)
        errorbox("Failed to load config file [" .. (fn or DEFAULT_NAME) .. "].\nFix the error or delete the file to use default settings.\nThe error was:\n\n" .. err)
        os.exit(1)
    end
end

local function saveConfig(fn, tab)
    tab = tab or CONFIG
    fn = fn or DEFAULT_NAME
    local s = serialize.save(tab, true) -- indent
    local f = io.open(fn, "w")
    if f then
        f:write(s)
        f:close()
        return true
    end
end


G.CONFIG = CONFIG
G.loadConfig = loadConfig
G.loadConfigOrDie = loadConfigOrDie
G.saveConfig = saveConfig

local tostring = tostring
local tins = table.insert
local strbyte = string.byte
local strchar = string.char
local pairs = pairs
local type = type
local strfind = string.find
local strsub = string.sub
local strfmt = string.format
local gsub = string.gsub
local fmod = math.fmod
local strmatch = string.match
local tconcat = table.concat
local tonumber = tonumber

-- explode(string, seperator, skipEmpty)
function string.explode(p, d, skip)
  local t, ll, l, nw
  
  ll = 0
  t = {}
  if(#p == 1) then return {p} end
    while true do
      l=strfind(p,d,ll,true) -- find the next d in the string
      if l~=nil then -- if "not not" found then..
        nw = strsub(p,ll,l-1)
        if not skip or #nw > 0 then
            tins(t, nw) -- Save it in our array.
        end
        ll=l+1 -- save just after where we found it for searching next time.
      else
        nw = strsub(p,ll)
        if not skip or #nw > 0 then
            tins(t, nw) -- Save what's left in our array.
        end
        break -- Break at end, as it should be, according to the lua manual.
      end
    end
  return t
end

function string.startsWith(String,Start)
   return strsub(String,1,#Start)==Start
end

function string.endsWith(String,End)
   return End=='' or strsub(String,-#End)==End
end

local function _char2hex(c)
    return strfmt("%02X",strbyte(c))
end

function string:tohex(sep)
    if sep then
        return gsub(self, ".", function(c)
            return strfmt("%02X%s",strbyte(c), sep)
        end)
    else
        return gsub(self, ".", _char2hex)
    end
end

function string:trimlr()
    return strmatch(self, "^%s*(.-)%s*$")
end

local function _makeexpandf(t)
    local ty = type(t)
    if ty == "number" then
        t = debugx.getvars(t + 2)
    elseif ty == "string" then
        return t
    end
    if ty == "table" or ty == "number" then
        return function(w)
            local s = t[w]
            if not s then
                error("Undefined variable in string expansion: '" .. w .. "'")
            end
            return tostring(s)
        end
    else
        return function(w)
            local s = t(w)
            if not s then
                error("Undefined variable in string expansion: '" .. w .. "'")
            end
            return tostring(s)
        end
    end
end

function string:expandcallback(f)
    return (gsub(
        gsub(self, '$([%a_][%a%d_]*)', f),
        '$(%b{})', function(m) return f(m:sub(2, -2)) end))
end

function string:expand(t)
    return self:expandcallback(_makeexpandf(t or 1))
end

do
    local t = { a = "--" }
    local x = ("[$a+${a}]"):expand(t)
    assert(x == "[--+--]")
end

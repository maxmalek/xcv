
local W = newclass("innerwindow_detailpanebase", assert(gui.imwindow))

local function _draw(iw, self, mth)
    if iw then
        iw[mth](iw)
    elseif self.drawDefault then
        self:drawDefault()
    end
end

function W:drawUI()
    local sysw = self:getParent()
    if sysw then
        local mth = self._methodName
        -- keep the last thing that got focus AND has a drawDetailUI() method sticky, until the next such thing is focused
        local iw = sysw:getFocusedInnerWindow()
        if not (iw and iw[mth]) then
            iw = self.lastFocused
            if not (isValidObj(iw) and iw[mth]) then
                iw = nil
            end
        end
        local w, h = imgui.GetContentRegionAvail()
        imgui.TextWrapWidth(w, _draw, iw, self, mth)
        self.lastFocused = iw
    end
end

local DETAIL = newclass("innerwindow_detailpane", W)
DETAIL._methodName = "drawDetailUI"
ui.detailpaneClass = DETAIL
function DETAIL.new(sysw)
    sysw = sysw or assert(sys.syswindow.GetCurrent())
    local self = sysw:newInner("Detail pane", "rootdetailpane", nil, nil, nil, nil, "r", 0.2, "V")
    switchclass(self, DETAIL)
    self:setShowMenuBar(true)
    return self
end
function DETAIL:drawDefault()
    if not self.lastFocused then
        ui.help.graphbuilderControls()
    end
end


local INFO = newclass("innerwindow_infopane", W)
INFO._methodName = "drawInfoUI"
ui.infopaneClass = INFO
function INFO.new(sysw)
    sysw = sysw or assert(sys.syswindow.GetCurrent())
    local self = sysw:newInner("Info pane", "rootinfopane", nil, nil, nil, nil, "b", 0.2, "")
    switchclass(self, INFO)
    return self
end
function INFO:drawDefault()
    if not self.lastFocused then
        ui.help.startupAuthorInfo(true)
    end
end

-- Connection management to other nodes

local N = shadergraph.node
local C = gui.imnodeconnection

function C:refresh()
    self:updateColor()
    if self:updateStatus() then -- good?
        self:setOverrideColor(false)
    else
        self:setOverrideColor(true, 1, 0, 0, 1)
    end
end

function C:onUnlink()
    self.oldsource, self.oldtarget = self:getNodes()
    self:setActive(false)
end

function C:onDestroy()
    local source, target = self.oldsource, self.oldtarget
    if isValidObj(source) then
        source:onConnectionsChanged()
    end
    if isValidObj(target) then
        target:onConnectionsChanged()
        -- doesn't change anything for the source node, but target has one input less now and must be updated
        target:onSomethingChanged()
    end
end

function C:onChangeActive(active)
    local source, target = self:getNodes()
    source:onConnectionsChanged()
    target:onConnectionsChanged()
end
 
 
function N:refreshConnections()
    if self.OUTPUTS then
        for _, t in pairs(self.OUTPUTS) do
            if t then
                for _, e in pairs(t)  do
                    if e then
                        local con = e.con
                        con:refresh()
                    end
                end
            end
        end
    end
    if self.ALLINPUTS then
        for _, e in pairs(self.ALLINPUTS) do
            if e then
                local con = e.con
                con:refresh()
            end
        end
    end
end
 
function N:onConnectionsChanged()

    self.ALLINPUTS = self:getInputSources(true)
    self.INPUTS = self:getInputSources(false) -- get only those inputs that are connected
    self.OUTPUTS = self:getOutputTargets(true) -- all outputs, including empty/unconnected ones
    
    -- kick results that are no longer outputs
    for k, _ in pairs(self.RESULTS) do
        if self.OUTPUTS[k] == nil then
            self.RESULTS[k] = nil
        end
    end

    if self.connchanged then
        self:connchanged()
    end
end

-- C++ API method -- called after the link is established
function N:onLinkToTarget(con, target, myslot, targetslot)
    debugprint(("Connect %s[%s] -> %s[%s]")
        :format(self:getName(), myslot, target:getName(), targetslot))
    
    self:onConnectionsChanged()
    target:onConnectionsChanged()
    
    -- now connected; target is always downstream so it will be updated implicitly
    self:onSomethingChanged()
end

-- C++ API method -- called *just before* the link is broken
function N:onUnlinkFromTarget(con, target, myslot, targetslot)
    debugprint(("Disconnect %s[%s] -/- %s[%s]")
        :format(self:getName(), myslot, target:getName(), targetslot))
      
    -- source/target onConnectionsChanged() is called in Connector:onDestroy() (see above)
end

local _tooltipErrorStyle =
{
    [imgui.Col_Text] = {1.0,0.2,0.1}, -- change color of the progress bars
}
local function _tooltipError(s)
    return imgui.ApplyStyle(_tooltipErrorStyle, imgui.Text, s)
end

-- always self --> target (self upstream, target downstream)
function N:canConnectTo(target, myslot, targetslot)
    --print(self, target)
    local csrc = self.Cout[myslot]
    local ctarget = target.Cin[targetslot]
    if not (csrc and ctarget) then
        return false
    end
    local ok = glsl.compatible(csrc.type, ctarget.type)
    if not ok then
        local UNK = glsl.unknown
        ok   = (csrc.possibletypes    and csrc.type == UNK    and ctarget.type ~= UNK and csrc.possibletypes[ctarget.type])
            or (ctarget.possibletypes and ctarget.type == UNK and csrc.type ~= UNK    and ctarget.possibletypes[csrc.type])
    end
    local err
    if ok then
        if self:findNodeUpstream(target) or target:findNodeDownstream(self) then
            err = "!! Cycle detected !!"
        end
    else
        err = ("!! Types incompatible !!\n(%s -> %s)"):format(glsl.nameof(csrc.type), glsl.nameof(ctarget.type))
    end
    if err then
        ok = false
        imgui.Tooltip(_tooltipError, err)
    end
    return ok
end

function N:onConnectorHovered(where, slotname)
    local obj, desc
    if where == "in" then
        desc = assert(self.Cin[slotname])
        local entry = self.INPUTS[slotname]
        if entry then
            obj = entry.sourcenode:getOutput(entry.sourceslot)
        end
    elseif where == "out" then
        desc = assert(self.Cout[slotname])
        obj = self.RESULTS[slotname]
    else
        error("oops")
    end
    self:drawConnectorTooltip(desc, obj)
end

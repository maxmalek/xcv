local function makeColorLUT()
    local c1d, c2d, c3d, cbuf, cu64 = table.unpack(color.rainbowi(5, 60, 360, 1, 0.8))
    return {
        [glsl.sampler1D] = c1d,
        [glsl.sampler2D] = c2d,
        [glsl.sampler3D] = c3d,
        [glsl.image1D] = c1d,
        [glsl.image2D] = c2d,
        [glsl.image3D] = c3d,
        [glsl.buffer] = cbuf,
        [glsl.uint64_t] = cu64,
    }
end

local _colorLUT = makeColorLUT()


function glsl.color(ty)
    return _colorLUT[ty] or 0xffffffff
end

local OPAQUE_T =
{
    [glsl.unknown] = true,
    [glsl.buffer] = true,
    [glsl.uint64_t] = true, -- can be transparently cast into sampler*D, image*D, and more
}


function glsl.isOpaque(ty)
    return OPAQUE_T[ty]
end

-- safeguard against accidental mis-accesses
setmetatable(glsl, {
    __index = function(t, k)
        error("glsl table does not contain key '" .. tostring(k) .. "'")
    end,
})

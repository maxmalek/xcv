-- Node dataflow handling

local N = shadergraph.node

local yield = coroutine.yield

function N:clearOutputs()
    --print("Clear out: " .. tostring(self))
    local R = self.RESULTS
    if R then
        for k, obj in pairs(R) do
            --print("Drop [" .. k .. "] = " .. tostring(obj))
            R[k] = nil
        end
    end
end

-- generate output `name` prior to shader invocation; so that the shader can write to it
function N:_makeOutput(inputs, name)
    debugprint(tostring(self) .. " make output for " .. name .. " ...")
    
    if self.makeNodeOutput then
        debugprint("--> Has custom function...")
        local obj = self:makeNodeOutput(inputs, name)
        debugprint("--> Got: " .. tostring(obj))
        if obj ~= nil then
            return obj
        end
    end
    
    return self:_autoMakeOutput(inputs, name)
end

local function _getinput(k, entry)
    local n = entry.sourcenode
    return k, n:getOutput(entry.sourceslot) or nil
end
function N:_getInputsTable()
    return fun.mapkv(self.INPUTS, _getinput)
end


----- PULL PIPELINE PART ------------------

function N:getOutput(name)
    assert(type(name) == "string")
    local R = self.RESULTS
    local obj = R[name]
    --print("Pull output " .. name .. " from " .. tostring(self) .. ", is " .. tostring(obj) .. " (total: " .. table.count(R) .. ")" )
    if obj == nil then
        self:processPull()
        obj = self.RESULTS[name]
        --print("--> Got obj: " .. tostring(obj))
    end
    return obj or nil -- never returns a literal false. this clears objects in shaders if nothing is passed.
end

function N:_updateInputs()
    for myname, entry in pairs(self.INPUTS) do
        local obj = entry.sourcenode:getOutput(entry.sourceslot)
        entry.con:setAnimated(not not obj)
        entry.con:refresh()
        --self:setInputConnActive(entry.index, not not obj)
        --print("Bind " .. tostring(entry.node) .. ":" .. entry.name .. " -> " .. tostring(self) .. ":" .. myname .. " = " .. tostring(obj))
    end
end

function N:bindOutputs(shader, outputs)
    if not outputs then
        return
    end
    assert(shader, "no shader")
    assert(outputs, "no outputs")
    
    for name, obj in pairs(outputs) do
        --print("SH[" .. name .. "] -> " .. tostring(obj))
        shader:setany(name, obj or nil)
    end
end

function N:bindInputs(shader, inputs)
    inputs = inputs or self:_getInputsTable()
    for myname, obj in pairs(inputs) do
        --print("SH<-[" .. myname .. "] = " .. tostring(obj))
        shader:setany(myname, obj or nil)
    end
    return inputs
end

function N:autoGenerateOutputs(inputs, outputs)
    debugprint("autoGenerateOutputs for " .. tostring(self))
    self:checkInputs(inputs)
    outputs = outputs or self.RESULTS or {}
    local inputs = self:_getInputsTable()
    for myname, _ in pairs(self.OUTPUTS) do
        if not outputs[myname] then
            outputs[myname] = self:_makeOutput(inputs, myname)
        end
    end
    self:checkOutputs(outputs)
    
    -- assign after everything is good
    self.RESULTS = outputs
    
    return outputs
end

function N:autoGenerateAndBind(shader, outputs, inputs)
    --print("autoGenerateAndBind for " .. tostring(self))
    inputs = inputs or self:_getInputsTable()
    outputs = self:autoGenerateOutputs(inputs, outputs)
    self:bindOutputs(shader, outputs)
    self:bindInputs(shader, inputs)
    return outputs, inputs
end

local function _checkobjs(tab, lut)
    for name, obj in pairs(tab) do
        if obj then
            local lu = lut[name]
            if lu then
                local needtype = lut[name].type
                if not glsl.compatible(obj:glsltype(), needtype) then
                    error("Got incompatible object for [" .. name .. "], should be " .. glsl.nameof(needtype) .. ", but is " .. glsl.nameof(obj:glsltype()))
                end
            else
                errprint("Expected obj with name " .. name)
            end
        end
    end
end
function N:checkInputs(inputs)
    _checkobjs(inputs, self.Cin)
end
function N:checkOutputs(outputs)
    _checkobjs(outputs, self.Cout)
end

function N:processPull()
    --print("PROCESS PULL", self)
    self:setTitleColor(0.3, 0, 0, 1)
    self:_updateInputs()
    self:setTitleColor(0.4, 0.2, 0.1, 1)
    self:_calculate()
    if self.mustyield then
        --yield()
    end
    if self._shader then
        self._shader:clearRefs()
    end
    self:setTitleColor(0.1, 0.5, 0.1, 1.0)
end

function N:processSink()
    --self:clearInputs(true)
    self:processPull()
end

-- assumes this node is done calculating
function N:updateDownstream(async)
    self:_processSinks(async, true) -- not async, skip self
end


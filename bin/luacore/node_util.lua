-- Node utility functions

local tremove = table.remove
local tins = table.insert
local next = next

local N = shadergraph.node

function N:_updateConnectors()
    devprint("updateConnectors: " .. tostring(self))
    local Cin, Cout = assert(self.Cin), assert(self.Cout)
    local d = self._nodedef
    
    -- Allow to modify connectors before they are locked in place
    if d.changeconnectors then
        d.changeconnectors(self, Cin, Cout)
    end
    
    -- This sets the connectors on the C++ side.
    -- May cause connection to become valid or invalid. May trigger callbacks.
    self:setConnectors(Cin, Cout)
    
    self._autoOut = {}
    if self.makeNodeOutput ~= false then
        for k, out in pairs(Cout) do
            local name = out.name
            self._autoOut[name] = self:_autoMakeOutputFunc(name, out.type)
        end
    end
    
    self:refreshConnections()
    
    debugprint(("%s: #Cin = %d, #Cout = %d"):format(self:getName(), table.count(Cin), table.count(Cout)))
end

function N:__tostring()
    return isValidObj(self)
        and ("{" .. self:getInternalID() .. ":" .. self:getName() .. "}")
        or "{DELETED NODE}"
end

function N:onSomethingChanged()
    self:changed() -- provided by node def
    self:_processSinks(true)
end

local function _doSinks(sinks)
    for _, node in pairs(sinks) do
        node:processSink()
    end
    return true -- signal success to BG job
end

function N:_processSinks(async, skipSelf)
    local sinks, others = self:getDownstreamSinks(skipSelf)

    if next(sinks) then
        for _, node in pairs(sinks) do
            --node:clearInputs(true)
            node:clearOutputs(true)
        end
        for _, node in pairs(others) do
            --node:clearInputs(true)
            node:clearOutputs(true)
        end
        --if async then
        --    dispatchJob("Node update", _doSinks, sinks)
        --else
            _doSinks(sinks)
        --end
    end
end


-- Adds terminal sinks found downstream to the t table.
-- A terminal sink is a node from which no other nodes are downstream-reachable
local function _findSinks(t, node)
    if t[node] ~= nil then
        return -- already processed, or still processing
    end
    t[node] = false -- mark as being processed
    
    local hasSuccessors
    for slot, outgoing in pairs(node.OUTPUTS) do
        if outgoing then
            for i = 1, #outgoing do
                hasSuccessors = true
                local entry = outgoing[i]
                _findSinks(t, entry.targetnode)
            end
        end
    end

    if hasSuccessors then
        t[node] = true -- not a terminal sink, treat as regular node on the way
        return true
    else -- no node downstream?
        t[node] = node -- terminal sink
        return true
    end
    -- else keep false & return nothing = no sink downstream
end

function N:getDownstreamSinks(skipSelf)
    local t = {}
    _findSinks(t, self, skipSelf)
    local sinks, med = {}, {}
    for node, sink in pairs(t) do
        if skipSelf and node == self then
            -- ignore!
        else
            if node == sink then
                tins(sinks, node)
            elseif sink then -- not false? -> node on the way
                tins(med, node)
            end
        end
    end
    
    --for i, node in ipairs(sinks) do
    --    print("SINK: " .. tostring(node))
    --end
    
    return sinks, med
end

-- Is 'which' reachable from 'begin' following connections in direction 'tn'?
local function _findNode(begin, which, tn, f, k)
    if which == begin then
        return true
    end
    local seen, stack = {}, {}
    seen[begin] = true
    local node = begin
    local tab = node[tn]
    repeat
        for _, entry in pairs(tab) do
            if f(seen, stack, entry, which, k) then
                return true
            end
        end
        node = tremove(stack)
    until not node
end

local function _handleEntry(seen, stack, entry, which, k)
    local n = entry and entry[k]
    if not n then
        return
    elseif n == which then
        return true
    end
    if not seen[n] then
        tins(stack, n)
        seen[n] = true
    end
end

local function _handleEntryList(seen, stack, list, which, k)
    if list then
        for i = 1, #list do
            if _handleEntry(seen, stack, list[i], which, k) then
                return true
            end
        end
    end
end

function N:findNodeUpstream(which)
    return _findNode(self, which, "ALLINPUTS", _handleEntry, "sourcenode") 
end
function N:findNodeDownstream(which)
    return _findNode(self, which, "OUTPUTS", _handleEntryList, "targetnode") 
end


function N:serialize()
    local x, y = self:getPos()
    local file = assert(self._nodefile)
    local ok, user, uniforms
    if self._userSerialize then
        ok, user = xpcall(self._userSerialize, errorbox, self)
        if not ok then
            user = nil
        end
    end
    if self._shader then
        uniforms = self._shader:serializeUniforms()
    end
    return { x=x, y=y, file = file, user = user, uniforms = uniforms }
end

function N:deserialize(t)
    self:setPos(t.x, t.y)
    self._nodefile = t.file
    if self._userDeserialize and t.user ~= nil then
        self:_userDeserialize(t.user)
    end
    if self._shader and t.uniforms then
        self._shader:deserializeUniforms(t.uniforms)
    end
end

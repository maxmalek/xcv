local abs = math.abs
local min = math.min
local max = math.max
local floor = math.floor
local asint = math.tointeger

local color = {}
G.color = color

local function int(v)
    return asint(floor(v))
end
local function clamp(v, mi, ma)
    return min(ma, max(mi, v))
end
local function ccf(v)
    return clamp(int(v * 255), 0, 255)
end

local function cc(v)
    return clamp(int(v), 0, 255)
end

local function i2i(r, g, b, a)
    a = (a and (cc(a) << 24)) or 0xff000000
    return a | cc(r) | (cc(g) << 8) | (cc(b) << 16)
end
color.i2i = i2i

local function f2i(r, g, b, a)
    r,g,b = ccf(r), ccf(g), ccf(b)
    local c = (a and (ccf(a) << 24)) or 0xff000000
    return c | r | (g << 8) | (b << 16)
end
color.f2i = f2i

local function i2f(c)
    local r = (c & 0xff) / 0xff
    local g = ((c >> 8) & 0xff) / 0xff
    local b = ((c >> 16) & 0xff) / 0xff
    local a = ((c >> 24) & 0xff) / 0xff
    return r,g,b,a
end
color.i2f = i2f

function G.colori(r,g,b,a)
    if type(r) == "table" then
        return f2i(table.unpack(r))
    end
    if r and g and b then
        return f2i(r,g,b,a)
    elseif type(r) == "string" then 
        if r:sub(1,1) == "#" then
            r = r:sub(2)
        end
        local n = tonumber(r, 16)
        if n then
            if #r > 6 then
                return n
            end
            return n | 0xff000000
        end
    end
    error("Unhandled color format")
end


local function hsv2rgb(h, s, v)
    local f = (h / 60.)
    local i = int(f)
    f = f - i
    local p = v * (1. - s)
    local q = v * (1. - (s*f))
    local t = v * (1. - (s * (1.-f)))
    if i == 0 then
        return v, t, p
    elseif i == 1 then
        return q, v, p
    elseif i == 2 then
        return p, v, t
    elseif i == 3 then
        return p, q, v
    elseif i == 4 then
        return t, p, v
    elseif i == 5 then
        return v, p, q
    end
    return 0, 0, 0 -- shouldn't reach this
end
color.hsv2rgb = hsv2rgb

-- h in [0..360); s, v in [0, 1]
-- from http://lolengine.net/blog/2013/01/13/fast-rgb-to-hsv
function color.rgb2hsv(r, g, b)
    local K = 0.0
    if g < b then
        g, b = b, g
        K = -1.0
    end
    local min_gb = b
    if r < g then
        r, g = g, r
        K = -2.0 / 6.0 - K
        min_gb = min(g, b)
    end
    local chroma = r - min_gb
    return
        360.0 * abs(K + (g - b) / (6.0 * chroma + 1e-20)), -- h
        chroma / (r + 1e-20), -- s
        r -- v
end

function color.rainbowi(n, hbegin, hend, s, v, a)
    local step = (hend - hbegin) / n
    local t = {}
    local h = hbegin
    for i = 1, n do
        local r, g, b = hsv2rgb(h, s, v)
        t[i] = f2i(r,g,b,a)
        h = ((h + step) + 360.) % 360.
    end
    return t
end


local debugx = {}

local tins = table.insert
local strrep = string.rep

local function formatVariable(x, depth)
    local recurse
    if depth then
        depth = depth - 1
        recurse = depth >= 0
    end
    local ty = type(x)
    local s = ""
    if ty == "table" then
        local c = 0
        for _, _ in pairs(x) do
            c = c + 1
        end
        if recurse then
            s = s .. "{#" .. c .. " "
            local idx, val = next(x)
            for i = 1, 100 do
                if idx == nil then
                    break
                end
                s = s .. formatVariable(idx) .. "=" .. formatVariable(val, depth) .. ", "
                idx, val = next(x, idx)
            end
            if idx then
                s = s .. "..."
            end
            s = s .. "}"
        else
            s = "{#" .. c .. "}"
        end
    elseif ty == "string" then
        if #x < 20 then
            s = "'" .. x:gsub("\n", "\\n") .. "'"
        else
            s = "'" .. x:sub(1, 20):gsub("\n", "\\n") .. "'..."
        end
    elseif ty == "function" then
        s = "FUNC"
        local info = debug and debug.getinfo(x)
        if info and info.source then
            local file = info.source:gsub("\\\\", "/"):explode("/", true)
            file = file[#file]
            if not file then
                file = info.source
            end
            s = s ..":[file: " .. tostring(file) .. "; def: " .. tostring(info.linedefined) .. "]"
        end
    else
        s = tostring(x)
    end
    return s
end
debugx.formatVariable = formatVariable

local function formatLocals(level)
    if not debug then
        return "[No local variables available]"
    end
    -- go up the stack until we hit the first Lua frame
    local lvl = level or 1
    local lfunc
    while true do
        local info = debug.getinfo(lvl, "fS")
        if not info then
            devprint("formatLocals(): No Lua function found")
            lvl = level -- oops.
            break
        elseif info.what == "Lua" then
            lfunc = info.func
            break
        else
            lvl = lvl + 1
            if lvl > 100 then
                return "TOO DEEP NESTING"
            end
        end
    end
    errprint(tostring(lfunc))
    local t = { false }
    local name, val
    local i = 0
    while true do
        i = i + 1
        name, val = debug.getlocal(lvl, i)
        if name then
            table.insert(t, "#" .. i .. " (" .. type(val) .. "): " .. name .. " = " .. formatVariable(val, 1))
        else
            break
        end
    end
    t[1] = "Local variables (" .. i .. "):"
    
    if lfunc then
        i = 0
        while true do
            i = i + 1
            name, val = debug.getupvalue(lfunc, i)
            if name and (name ~= "_ENV" or val ~= _G) then -- omit _ENV if == _G
                if i == 1 then
                    table.insert(t, "Upvalues:")
                end
                table.insert(t, "#" .. i .. " (" .. type(val) .. "): " .. name .. " = " .. formatVariable(val, 1))
            else
                break
            end
        end
    end
    
    return table.concat(t, "\n")
end
debugx.formatLocals = formatLocals


local function formatStack(lvl)
    if debug then
        if not lvl then lvl = 1 end
        return debug.traceback("", lvl) or "[No traceback available]"
    end
    return "[No debug library available]"
end
debugx.formatStack = formatStack


local function getlocals(func)
  local n = 1
  local locals = {}
  func = (type(func) == "number") and func + 1 or func
  while true do
    local lname, lvalue = debug.getlocal(func, n)
    if lname == nil then break end  -- end of list
    locals[lname] = lvalue
    n = n + 1
  end
  return locals
end
debugx.getlocals = getlocals

local function getupvalues(func)
  local n = 1
  local locals = {}
  if type(func) == "number" then
    local inf = debug.getinfo(func + 1, "f")
    local ff = inf and inf.func
    if not ff then
        return
    end
    func = ff
  end
  while true do
    local lname, lvalue = debug.getupvalue(func, n)
    if lname == nil then break end  -- end of list
    locals[lname] = lvalue
    n = n + 1
  end
  return locals
end
debugx.getupvalues = getupvalues

local function _getfenv(func)
    local i = 1
    while true do
        local n, v = debug.getupvalue(func, i)
        if not n or n == "_ENV" then
            return v
        end
        i = i + 1
    end
end
debugx.getfenv = _getfenv

-- collect locals and visible upvalues (non-iterable)
local function getvars(level)
    level = (level or 0) + 1
    local caller = assert(debug.getinfo(level, "f").func)
    local locals = getlocals(level)
    local upvals = getupvalues(level)
    if upvals then
        local callerENV = upvals._ENV --_getfenv(caller)
        setmetatable(locals, {__index = upvals })
        if callerENV then
            assert(type(callerENV) == "table")
            setmetatable(upvals, {__index = callerENV })
        end
    end
    return locals
end
debugx.getvars = getvars



local function kviter(x) -- returns key value iterator function if iterable
    local tx = type(x)
    if tx == "userdata" then
        if not isValidObj(x) then
            return
        end
        x = debug.getmetatable(x)
        if not x then
            return
        end
        return table.sortediter(x)
    end
    
    if tx == "function" then
        x = getupvalues(x)
        if not x then
            return
        end
        
        if x._ENV == _G then
            x._ENV = nil -- better to eliminate that; clutters things a lot
        end
        if not next(x) then -- and don't use an empty table
            return
        end
        
        return table.sortediter(x)
    end
    
    if tx == "table" then
        return table.sortediter(x)
    end
end
debugx.kviter = kviter

local function walkit(it, f, ...)
    for k, v in it do
        f(k, v, ...)
    end
end
debugx.walkit = walkit

local function walk(x, f, ...) -- f called as f(k, v, ...). for terminal objects (numbers, strings, etc) k == nil.
    local it = kviter(x)
    if it then
        walkit(it, f, ...)
    else
        f(nil, x, ...)
    end
end
debugx.walk = walk

G.debugx = debugx

local pairs = pairs
local type = type
local yield = coroutine.yield

function table.clear(t)
    for k, _ in pairs(t) do
        t[k] = nil
    end
end

function table.shallowcopy(t)
    local tt = {}
    for k, v in pairs(t) do
        tt[k] = v
    end
    return tt
end

function table.cleari(t)
    for i = 1, #t do
        t[i] = nil
    end
end
function table.compact(t, n, begin)
    begin = begin or 1
    local w = begin
    for i = begin, n do
        local e = t[i]
        if e then
            t[w] = e
            w = w + 1
        end
    end
    while w <= n do
        t[w] = nil
        w = w + 1
    end
    return t
end

function table.count(t)
    local c = 0
    for _ in pairs(t) do
        c = c + 1
    end
    return c
end

function table.getindexi(t, x)
    for i = 1, #t do
        if t[i] == x then
            return i
        end
    end
end

function table.getrindexi(t, x)
    for i = #t, 1, -1 do
        if t[i] == x then
            return i
        end
    end
end

function table.getindex(t, x)
    for k, v in pairs(t) do
        if x == v then
            return k
        end
    end
end

function table.appendi(t, src)
    local w = #t
    for i = 1, #src do
        t[w+i] = src[i]
    end
    return t
end

local autoextendmeta = {
    __index = function(t, k)
        local s = {}
        t[k] = s
        return s
    end
}
function table.makeautoextend(f)
    local mt
    if not f then
        mt = autoextendmeta
    else
        mt = { __index = function(t, k)
            local s = f(k)
            t[k] = s
            return s
        end}
    end
    return setmetatable({}, mt)
end

local function _cmpAsString(a, b)
    return tostring(a) < tostring(b)
end

local _cmpByType = setmetatable(
{
    number = function(a, b)
        return a < b
    end,
    string = assert(string.natless), -- function(a, b) return a < b end
    boolean = function(a)
        return a
    end,
    userdata = function(a, b)
        if isValidObj(a) and isValidObj(b) then -- class objects?
            local na = tostring(a.__name)
            local nb = tostring(b.__name)
            if na ~= nb then
                return na < nb
            end
            
            return addressof(a) < addressof(b)
        end
    end,
}, { __index = function() return _cmpAsString end })

local function _genericKeySort(a, b)
    local ta, tb = type(a), type(b)
    if ta ~= tb then
        return ta < tb
    end
    -- ta == tb
    return _cmpByType[ta](a, b)
end

local function _genSortedIndex(t, order)
    order = order or _genericKeySort
    local idx = {}
    local i = 0
    for k in pairs(t) do
        i = i + 1
        idx[i] = k
    end
    if order then
        local oldf = assert(order)
        order = function(...)
            local ok, lt = xpcall(oldf, errorbox, ...)
            return ok and lt
        end
    end
    table.sort(idx, order)
    return idx
end

local function _keypairs(t, idx)
    for i = 1, #idx do
        local k = idx[i]
        yield(k, t[k])
    end
end

local function _sortedpairs_noyield(t, order)
    return _keypairs(t, _genSortedIndex(t, order))
end

local function _sortediter(t,  order)
    yield() -- initial yield to obtain params as locals
    return _sortedpairs_noyield(t, order)
end

local function _keyiter(t, keylist)
    yield() -- initial yield to obtain params as locals
    return _keypairs(t, keylist)
end


---------------------------------------------

function table.sortedkeys(t, order)
    return _genSortedIndex(t, order)
end


function table.sortedpairs(t, order) -- intended for use with pairs() -- takes parameters!
    return coroutine.wrap(_sortedpairs_noyield), t, order
end
function table.sortediter(t, order) -- returns single iterator function that takes no params
    local co = coroutine.wrap(_sortediter)
    co(t, order) -- relies on "initial yield" above so we don't have to pass params anymore
    return co
end

function table.keyiter(t, keylist)
    local co = coroutine.wrap(_keyiter)
    co(t, keylist)
    return co
end


G.unpack = table.unpack

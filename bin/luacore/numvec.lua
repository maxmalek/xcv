
local mat2_identity = numvec.Pack(glsl.mat2, 1, 0,
                                              0, 1)

local mat3_identity = numvec.Pack(glsl.mat3, 1, 0, 0,
                                              0, 1, 0,
                                              0, 0, 1)

local mat4_identity = numvec.Pack(glsl.mat4, 1, 0, 0, 0,
                                              0, 1, 0, 0,
                                              0, 0, 1, 0,
                                              0, 0, 0, 1)

local mat4_fullscreen_ortho = numvec.Pack(glsl.mat4, glm.ortho(0, 1, 1, 0))

numvec.mat2_identity = mat2_identity
numvec.mat3_identity = mat3_identity
numvec.mat4_identity = mat4_identity
numvec.mat4_fullscreen_ortho = mat4_fullscreen_ortho


function numvec:loadmat4i()
    self:load(mat4_identity)
end

function numvec:apply(newtype, func)
    local sz = self:len()
    local newvec = numvec.new(newtype, sz)
    for i = 1, sz do
        newvec:set(i, func(self:unpack(i)))
    end
    return newvec
end

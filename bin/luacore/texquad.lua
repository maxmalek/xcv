local C = newclass("texquad", render.renderobject)
render.texquad = C
local quad = assert(gpu.meshes.quad)
local sh = assert(gpu.simpleshader.File("shaders/texquad"))
local shflip = assert(gpu.simpleshader.File("shaders/texquad", { FLIP_V = 1 }))

function C.new(tex, flip, ortho)
    local self = C._new()
    self.tex = tex
    self.ortho = ortho
    self.flip = flip
    self.cull = false
    if ortho then
        self.dtest = assert(gpu.DEPTH_TEST_IGNORE)
        self.dwrite = assert(gpu.DEPTH_WRITE_OFF)
    else
        self.dtest = assert(gpu.DEPTH_TEST_ON)
        self.dwrite = assert(gpu.DEPTH_WRITE_ON)
    end
    self:setTint(1, 1, 1, 1)
    self.matrix = numvec.Pack(glsl.mat4, glm.ortho(0, 1, 1, 0))
    return self
end

function C:setTint(r, g, b, a)
    self._tint = numvec.Pack(glsl.vec4, r, g, b, a or 1)
end

function C:onRender(projptr)
    if self.ortho then
        projptr = nil
    end
    self:_doRender(projptr, self.flip, self.tex, self._tint:unpack())
end

function C:_doRender(projptr, flip, tex, ...)
    local s = (flip and shflip) or sh
    s:bind()
    projptr = projptr or self.matrix:ptr()
    s:setuniform("proj", projptr)
    s:setsampler("tex", tex)
    s:setuniform("colorMult", ...)
    quad:render("t", self.cull, self.dtest, self.dwrite)
end

-- When rendering to screen, invert the flipping logic, since GL texture coords are upside down
function C:renderToScreen(noflip)
    self:renderTexToScreen(self.tex, noflip)
end

function C:renderTexToScreen(tex, noflip)
    local s = (noflip and sh) or shflip
    s:bind()
    s:setuniform("proj", self.matrix:ptr())
    s:setsampler("tex", tex)
    s:setuniform("colorMult", self._tint:unpack())
    quad:render("t", false, true, false)
end

ui.help = ui.help or {}
local H = ui.help

local Bullet = assert(imgui.Bullet)
local TreeNode = assert(imgui.TreeNode)
local Text = assert(imgui.Text)
local TextColor = assert(imgui.TextColor)
local TextDisabled = assert(imgui.TextDisabled)
local SameLine = assert(imgui.SameLine)
local Dummy = assert(imgui.Dummy)
local Tooltip = assert(imgui.Tooltip)
local IsItemHovered = assert(imgui.IsItemHovered)
local Separator = assert(imgui.Separator)


local TOPICTEXT =
{
tex_swizzle_mask = [[
Exactly 4 characters out of {rgba01}.
This specifies the 4 (RGBA) values sampled from the texture.
'rgba' stands for channels 1-4 respectively, 0 returns zero value, 1 returns maximal value.
Examples:
  rgba: Default
  rrr1: Luminance with full alpha
  rggg: First channel is passed through, second channel for everything else
]],
 
tex_format_str = [[
Combination of channels and data format.
r, rg, rgb, rgba specify 1-4 channels, respectively.
8, 16, 32 specifies the number of bits per channel
i, ui, f specifies integer, unsigned integer, or floating-point data.
Not all combinations are valid (e.g. 8-bit float).
Examples:
  rgba8ui: 4 channels with 8 bits per color (typical for color images with transparency)
  rg16f: 2 channels of 16 bit floats (half precision)
  r16ui: 1 channel of 16 bit unsigned ints (typical for MRT/CT scans)
  rgb32f: 3 channels of 32 bit floats (typical for HDR color images)
]],
}
setmetatable(TOPICTEXT, {__index = function(_, k)
    return "[No help text for topic <" .. tostring(k) .. ">]"
end })

function H.topicText(topic)
    Text(TOPICTEXT[topic])
end

function H.topicTooltip(topic)
    Tooltip(TOPICTEXT[topic])
end
function H.topicTooltipForLastItem(topic)
    if IsItemHovered() then
        H.topicTooltip(topic)
    end
end


----------------------------------

local _glslhelpcache

local _helptypetab =
{
    {"Floating-point textures/images",
        { "image1D", "sampler1D" },
        { "image2D", "sampler2D" },
        { "image3D", "sampler3D" },
    },
    { "Signed integer textures/images",
        { "iimage1D", "isampler1D" },
        { "iimage2D", "isampler2D" },
        { "iimage3D", "isampler3D" },
    },
    { "Unsigned integer textures/images",
        { "uimage1D", "usampler1D" },
        { "uimage2D", "usampler2D" },
        { "uimage3D", "usampler3D" },
    },
    { "Others",
        { "buffer" },
        { "uint64_t" },
    },
}

local function _glsltext(name)
    TextColor(name, unpack(_glslhelpcache[name].color))
end

local function _glslgroup(t)
    for i = 2, #t do
        local e = t[i]
        _glsltext(e[1])
        for j = 2, #e do
            SameLine()
            _glsltext(e[j])
        end
    end
end

local function helpblock(s, ...)
    Bullet()
    if ... then
        TextColor(s, ...)
    else
        Text(s)
    end
    Dummy(0, 4)
end

local function _helptypes(self)
    if not _glslhelpcache then
        _glslhelpcache = {}
        for k, v in pairs(glsl) do
            if type(k) == "string" and type(v) == "number" then
                if glsl.isOpaque(v) or glsl.isImage(v) or glsl.isSampler(v) then
                    _glslhelpcache[k] = { color = {color.i2f(glsl.color(v))} }
                end
            end
        end
    end
    
    for i = 1, #_helptypetab do
        local t = _helptypetab[i]
        TreeNode(t[1], "o", _glslgroup, t)
    end
end

local function _helpcontrols()
    helpblock("Left-click to select & move nodes.\nSelected nodes will display extra information in this panel.")
    helpblock("Right-click into empty space to create new nodes.")
    helpblock("Hold middle mouse button to pan the view.")
    helpblock("Press Delete to remove anything that is selected.")
    helpblock("Right-click on a node to open a node-specific context menu, if the node has one.\n(Indicated by a blue border)")
end

local function _helpnodes()
    helpblock("A node can be a data source, sink, or both.") 
    helpblock("Nodes can perform arbitrary computations and transform data that pass through them.") -- in reality: node create a copy of the input and operate on the copy, but that's an implementation detail
    helpblock("Some nodes provide basic widgets directly on the node. Advanced configuration and outputs/results are displayed in this panel when a node is selected.")
end

local function _helpconns()
    helpblock("Connect nodes by drawing lines between connectors.")
    helpblock("A connection causes data to pass from a node's output to the connected node's input.")
    helpblock("Inputs are on the left, outputs are on the right.")
    helpblock("An output connector can be connected to many inputs.")
    helpblock("An input connector may receive data from zero or one outputs.")
    helpblock("Invalid connections are displayed in red. A connection is invalid if in- and output are not type-compatible. No data pass through invalid connections.")
    --helpblock("To ignore types and force a connection (at your own risk!), hold Ctrl when releasing the mouse button. Data will only pass through valid connections.")
end

local _dragval = 1.0
local function _helpimgui()
    helpblock("Click+Hold the left mouse button on a drag box and move the mouse left or right to change its value.")
    helpblock("CTRL+Click on a slider or drag box to input a value directly.")
    helpblock("You can apply arithmetic operators +,*,/ on numerical values. Use +- to subtract.")
    
    Bullet()
    Text("Test box: ")
    SameLine()
    local _
    _, _dragval = imgui.ItemWidth(100, imgui.DragFloat, "##helptestbox", _dragval)
    --Dummy(0, 4)
end

local function _helphotkeys_global()
    helpblock("F2: Show/Hide UI\n(Useful if something is displayed in background)")
    helpblock("F5: Reload all scripts and shaders")
    helpblock("F7: Run manual GC") -- GC settings agre aggressive enough that this is never necessary
end
local function _helphotkeys_gb()
    helpblock("Ctrl+O: Open\nCtrl+S: Save\nCtrl+Shift+S: Save as")
    helpblock("Ctrl+C/X/V: Copy/Cut/Paste\nCtrl+A: Select all")
end
local function _helphotkeys()
    TreeNode("Global", "o", _helphotkeys_global)
    TreeNode("Graph builder", "o", _helphotkeys_gb)
end
function H.graphbuilderControls()
    Text("Quick help")
    imgui.SeparatorY(5)

    TreeNode("Controls", "fo", _helpcontrols)
    TreeNode("Nodes", "f", _helpnodes)
    TreeNode("Connections", "f", _helpconns)
    TreeNode("Types & Colors", "f", _helptypes)
    TreeNode("UI cheat sheet", "f", _helpimgui)
    TreeNode("Hotkeys", "f", _helphotkeys)
end

local function person(who, what)
    local pad = 25 - #who
    if pad > 0 then
        who = who .. (" "):rep(pad)
    end
    Text("- " .. who)
    SameLine()
    TextDisabled(what)
end



local function _authorinfo(ee)
    Text("xcv v" .. XCV_VERSION)
    SameLine()
    Dummy(20, 0)
    SameLine()
    imgui.TextLink("Source code", "https://bitbucket.org/maxmalek/xcv")
    Separator()
    Text("Developed at the Institute of Computational Biotechnology / Graz University of Technology")
    Separator()
    TextDisabled("Authors:")
    person("Maximilian Malek", "Development")
    person("Christoph Sensen", "Project supervision")
    TextDisabled("Contributors:")
    person("Stefan Grabuschnig", "Moral support")
    if ee and IsItemHovered() then SameLine() TextDisabled("(\"Did it explode yet?\", \"On error goto home\")") end
    Dummy(0, 10)
    TextDisabled("See LICENSE.txt for copyright information.")
end

function H.startupAuthorInfo(...)
    local w, h = imgui.GetContentRegionAvail()
    imgui.TextWrapWidth(w, _authorinfo, ...)
end

local _WINIDX = 0
function H.openAboutWindow(win)
    win = win or sys.syswindow.GetCurrent()
    local iw = win:newInner("About", "ui_help_about" .. _WINIDX, 400, 400)
    _WINIDX = _WINIDX + 1 -- HACK
    function iw:drawUI()
        ui.help.startupAuthorInfo()
    end
    iw:detach()
end

ui.mainbar = ui.mainbar or {}

local floor = math.floor

local function imgui_gpuinfo2(info, txinfo, gputime, frametime, p, pblen, pbstr)

    if CONFIG.debugmode then
        imgui.TextDisabled(info)
        imgui.SameLine()
        imgui.ProgressBar(gputime / frametime, pblen, 0, tostring(gputime) .. "µs")
    else
        imgui.Dummy(pblen+txinfo+8, 0)
    end
    imgui.SameLine()
    if p then
        imgui.ProgressBar(p, pblen, 0, pbstr)
    else
        imgui.Dummy(pblen, 0)
    end
end

local STYLE =
{
    [imgui.Col_PlotHistogram] = {0.2,0.2,0.6}, -- change color of the progress bars
}
local function imgui_gpuinfo(...)
    imgui.ApplyStyle(STYLE, imgui_gpuinfo2, ...)
end

local function _panelSettingsPopup(win)
    return win.backpanel:drawKnobs()
end

local function _panelMenu(win)
    if imgui.MenuItem("Show/Hide UI", false, "F2") then
        win.hideUI = not win.hideUI
    end
    imgui.Separator()
    --[[if imgui.MenuItem("Background settings...") then
        imgui.OpenPopup("panel bg settings")
    end
    imgui.PopupBody("panel bg settings", _panelSettingsPopup, win)]]
    
    imgui.Text("Background settings:")
    win.backpanel:drawKnobs()
end

local function _windowMenu(win)
    win:drawInnerWindowMenuItems()
end

local function _functionMenu(win)
    if imgui.MenuItem("New graph editor") then
        error("TODO: WRITE ME")
    end
end

local function _boolsetting(str, name)
    local on = CONFIG[name]
    if imgui.MenuItem(str, on) then
        CONFIG[name] = not on
    end
end

local function _settingsMenu(win)
    _boolsetting("Debug/Developer mode", "debugmode")
    _boolsetting("Hide contextual help", "hidehelp")
end

local function _helpMenu(win)
    if imgui.MenuItem("About") then
        ui.help.openAboutWindow()
    end
end

function ui.mainbar.drawStatsPart(win, dt)
    local sx, sy = imgui.GetContentRegionAvail()
    local ms = floor(dt*1000.0)
    local frametime = ms * 1000
    local fps = dt ~= 0 and floor(1.0/dt) or 0 -- avoid division by 0
    local info = ("%dms/%dFPS"):format(ms, fps)
    local txinfo, tyinfo = imgui.CalcTextSize(info)
    local pblen = 120
    local barlen = txinfo+(2*pblen)+100 -- extra length of rightmost bit
    if sx < barlen then
        return
    end
    imgui.Dummy(sx-barlen)
    imgui.SameLine()
    
    if win then
        local name, free, total, gputime = win:gpuinfo()
        local pmem, memstr
        if free and total and total > 0 then
            pmem = free/total
            memstr = ("%d/%d MB"):format(free//1024, total//1024)
        elseif (not total or total == 0) and free and free > 0 then
            pmem = 0.
            memstr = ("%d MB free"):format(free//1024)
        elseif CONFIG.debugmode then
            pmem = 0.
            memstr = "no memory info"
        end
        imgui.Group(imgui_gpuinfo, info, txinfo, gputime, frametime, pmem, pblen, memstr)
        if imgui.IsItemHovered() then
            local s = "Device:\n " .. name
            if CONFIG.debugmode then
                s = s .. "\nExtensions:\n " .. gpu.extensions():gsub(" ", "\n ")
            end
            imgui.Tooltip(s)
        end
        imgui.SameLine()
    end
end


function ui.mainbar.drawContent(win, dt)
    assert(win)
    --imgui.Menu("Function", true, _functionMenu, win)
    if CONFIG.debugmode then
        imgui.Menu("Background Panel", true, _panelMenu, win)
    end
    imgui.Menu("Settings", true, _settingsMenu, win)
    
    if CONFIG.debugmode then
        ui.debugmenu.drawMenu(win)
    end
    
    imgui.Menu("Help", true, _helpMenu, win)
    
    ui.mainbar.drawStatsPart(win, dt)
    
    imgui.SameLine()
    imgui.TextDisabled("|")
    imgui.SameLine()
    imgui.Menu("Windows", true, _windowMenu, win)
    
    local wx, wy = imgui.GetWindowSize()
    
    return wx, wy
end

local function _adjustDock(wx, wy, ...)
    local dx, dy = imgui.GetDisplaySize()    
    imgui.RootDock(0, wy, dx, dy-wy)
    return ...
end

function ui.mainbar.drawBar(win, dt)
    return _adjustDock(imgui.MainMenuBar(ui.mainbar.drawContent, win, dt))
end

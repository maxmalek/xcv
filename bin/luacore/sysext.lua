local W = sys.syswindow

function W.OpenClass(cls, w, h, ...) -- w, h, full, displayID
    devprint("Open window class " .. cls.__name)
    local win = assert(cls.new(), "bad ctor")
    if not win:open(w, h, ...) then
        errorbox("Failed to open " .. tostring(w) .. "x" .. tostring(h) .. " window")
        win:destroy()
        win = nil
    end
    return win
end

-- Use like method call on derived class
function W:Open(...)
    return self:OpenClass(...)
end

-- API function. Called after drawing main content.
-- Overload for specialized behavior.
function W:onDrawUI()
    if self.hideUI then
        if imgui.SmallButton("Back") then
            self.hideUI = false
        end
    else
        self:drawInnerWindows()
        return true
    end
end

-- API function. Called by requestClose() C++ function, or when user wants to close the window.
-- Window will be closed if function throws an error, returns nothing, or returns true.
-- Return false to prevent closing the window.
function W:onCloseRequest(dt)
    return true
end

-- API function
function W:onRenderBegin()
end

-- API function
function W:onRenderEnd()
end

-- API function. Called in every frame. dt = difftime to last frame
function W:onUpdate(dt)
end

-- API function. Called on keyboard event
function W:onKeyEvent(keyname, scancode, keymod, down)
    -- Default keypress events
    if down then
        if scancode == 62 then -- F5
            infoprint("Reloading scripts...")
            onScriptReload()
            assert(loadfile("main.lua"))(true) -- HACK: Is this really needed?
            print("Scripts reloaded.")
        elseif scancode == 64 then -- F7
            print(("Lua Memory allocated before GC: %d KB"):format(math.floor(collectgarbage("count"))))
            collectgarbage()
            print(("Lua Memory allocated after GC: %d KB"):format(math.floor(collectgarbage("count"))))
        elseif scancode == 59 then -- F2
            self.hideUI = not self.hideUI
        else
            --print("Unhandled key press: [" .. keyname .. "] = " .. scancode .. " (mod " .. keymod .. ")")
        end
    end
    
    -- forward keypresses to inner windows
    local iw = self:getFocusedInnerWindow()
    if iw and iw.onKeyEvent then
        iw:onKeyEvent(keyname, scancode, keymod, down)
    end
end

function W:onRender()
end

-- API functions for mouse input
function W:onMouseMove(x, y, rx, ry, buttons)
end
function W:onMouseClick(button, state, x, y)
end
function W:onMouseWheel(change)
end

-- API function. Called once when the window is closing.
function W:onClose()
end

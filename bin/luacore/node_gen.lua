-- Generate node from input definition
-- See documentation/howto_nodes.txt for info.

local N = shadergraph.node

local tins = table.insert
local unpack = table.unpack

local function _makeMultiDesc(possible, sep, prefix)
    local t = {}
    prefix = prefix or ""
    for ty in pairs(possible) do
        tins(t, prefix .. glsl.nameof(ty))
    end
    table.sort(t)
    return table.concat(t, sep)
end

local function makeconndesc(name, glsltype, vty) -- vty is optional
    -- record all possible types this variable may use
    local possibletypes = {}
    local multi
    if type(glsltype) == "table" then
        multi = true
        for i = 1, #glsltype do
            possibletypes[glsltype[i]] = true
        end
        glsltype = glsl.unknown
    elseif type(glsltype) == "number" then
        possibletypes[glsltype] = true
    else
        error("Variable def must be GLSL type or list of types")
    end
        
    local glslname = glsl.nameof(glsltype)
    local detail =  glslname .. " " .. name
    if multi then
        detail = detail .. "\nPossible types:\n" ..  _makeMultiDesc(possibletypes, "\n", " * ")
    end
    local color = glsl.color(glsltype)
    
    --print(name, glsltype, vty, glslname)
    return {
        -- used by C++ code
        name = name,
        detail = detail,
        color = color,
        type = glsltype,
        -- extra
        vartype = vty,
        possibletypes = possibletypes,
    }
end


local function conndesc1(t)
    return makeconndesc(table.unpack(t))
end

local function canBeOutput(v)
    local ok = glsl.isOpaque(v.type) or glsl.isImage(v.type)
    if not ok then
        errprint(tostring(glsl.nameof(v.type)) .. " is a bad output type, can't be written to")
    end
    return ok
end

local function canBeInput(v)
    local ok = glsl.isOpaque(v.type) or glsl.isImage(v.type) or glsl.isSampler(v.type)
    if not ok then
        errprint(v.name .. ": " .. tostring(glsl.nameof(v.type)) .. " is a bad input type, can't be read from")
    end
    return ok
end

local function guessIsInput(v)
    return v.name:match("^in_")
end
local function guessIsOutput(v)
    return v.name:match("^out_")
end
local function guessIsKnob(v)
    return v.name:match("^u_")
end
local function isInput(v, ins)
    return ((ins and ins[v.name]) or guessIsInput(v)) and canBeInput(v)
end
local function isOutput(v, outs)
    return ((outs and outs[v.name]) or guessIsOutput(v)) and canBeOutput(v)
end

local function _connsort(a, b)
    return a.name < b.name
end

local function makeprepLUT(x)
    return x and fun.mapkv(x, function(k, v) return v, true end)
end

local function insertconndefs(target, conndefs)
    if not conndefs then
        return
    end
    for name, glsltype in pairs(conndefs) do -- { varname = glsltype, ... }
        target[name] = makeconndesc(name, glsltype)
    end
end

local function insertvars(target, vars)
    if not vars then
        return
    end
    for _, var in pairs(vars) do -- { {name=..., type=..., ...}, ... }
        target[var.name] = makeconndesc(var.name, var.type)
    end
end

-- Add connectors to a node, based on shader and defs
local function prepconn(node, sh, def)
    local Cin, Cout = assert(node.Cin), assert(node.Cout)
    local CKnobsList
    if sh then
        local ins = makeprepLUT(def.inDef)
        local outs = makeprepLUT(def.outDef)
        local C = fun.map(sh:getvars(), conndesc1)
        insertvars(Cin, fun.filteri(C, isInput, ins))
        insertvars(Cout, fun.filteri(C, isOutput, outs))
        CKnobsList = fun.filteri(C, guessIsKnob)
        
        for _, d in pairs(Cin) do
            if d.type ~= glsl.buffer then
                d.detail = "uniform " .. d.detail
            end
        end
    end
    
    insertconndefs(Cin, def.inDef)
    insertconndefs(Cin, def.extraInputs)
    insertconndefs(Cout, def.outDef)
    insertconndefs(Cout, def.extraOutputs)
    
    if def.knobDef then
        CKnobsList = CKnobsList or {}
        insertconndefs(CKnobsList, def.knobDef)
    end
    
    if CKnobsList then table.sort(CKnobsList, _connsort) end
        
    return Cout, Cin, CKnobsList
end

local shmth =
{
    glsl = "Create",
    file = "File",
    template = "new",
}

local function shcons(class, srctype, ...)
    local sh, err = class[shmth[srctype]](...)
    if sh then
        sh:bless()
    end
    return sh, err
end

local classdispatch =
{
    compute = gpu.computeshader,
    render = gpu.simpleshader,
    data = false,
    misc = false,
}

local function mkshader(shadertype, srctype, ...)
    if not srctype then
        return true -- no shader needed, return silently
    end
    shadertype = shadertype or "compute"
    local class = classdispatch[shadertype or "compute"]
    if class then
        return shcons(class, srctype, ...)
    elseif class == nil then
        return false, "Unhandled shader type: " .. tostring(shadertype)
    end
end

local function dummy()
end

local function doDispatch(sh, maxsize, w,h,d)
    if maxsize then
        local mx, my, mz
        if type(maxsize) == "table" then
            mx, my, mz = table.unpack(maxsize)
        end
        mx = mx or maxsize or 0
        my = my or mx
        mz = mz or my
        sh:multiDispatchForSize(w,h,d,  mx,my,mz)
    else
        sh:dispatchForSize(w,h,d)
    end
end

local function m_auto_recalc(self, inputs)
    --print("m_auto_recalc: " .. tostring(self))
    local name = next(self.Cout)
    local outputs = self:autoGenerateAndBind(self._shader)
    local img = outputs[name]
    if img then
        local w,h,d = img:size3()
        debugprint(("%s: Invoke shader for %dx%dx%d %s image"):format(self:getName(), w,h,d, img:formatstr()))
        if self:trySpecializeShader(inputs) then
            img:invalidate()
            doDispatch(self._shader, self._nodedef.maxsize, w,h,d)
        end
    else
        -- not an error, just not connected -> nothing to do
        --errprint(tostring(self) .. ": No image bound to output " .. name)
    end
    return outputs
end

local function checkmth(self, method)
    if not self[method] then
        error("Method missing: " .. method)
    end
end

local function checkMethods(self, def)
    -- Automatic mode -- we can deal with a single output that is an image[1|2|3]D.
    -- Anything that writes to buffers or has multiple outputs needs specialization.
    
    -- FIXME: this should go
    if not self.recalc then
        if self._shader or self._lateBuildShader then -- TODO: only for compute shader
            local k, c = next(self.Cout)
            if k and not next(self.Cout, k) then -- only a single element?
                debugprint(tostring(self) .. " single output " .. c.name .. " is type " .. glsl.nameof(c.type))
                if glsl.isImage(c.type) or c.type == glsl.uint64_t or c.type == glsl.unknown then
                    -- Ok, egligible for auto mode -- add missing methods
                    self.recalc = m_auto_recalc
                    debugprint("Using default recalc() function for [" .. self:getName() .. "] node")
                end
            end
        end
    end
    
    checkmth(self, "recalc")
    
    if not self.onDestroy then
        self.onDestroy = def.destroy
    end
end

-- Connector definitions are in Cin, Cout.

--[[ actual connections:
INPUTS[myName] = { ENTRY, ... }
OUTPUTS[myName] = { ENTRY, ... }
where each ENTRY is {
    (-const-)
    desc = connection desc
    (-depends on connection partner-)
    name = name in node,
    node = NODE ON OTHER END,
}
]]

function N:_setShader(sh, nocopy)
    if not nocopy and self._shader and sh then
        sh:copyvars(self._shader)
    end
    self._shader = sh
end

function N:setShader(sh, nocopy)
    self:_setShader(sh)
    self:postInit()
end

local noLengthMeta = {
    __len = function(_) error("Do not use # on this table") end,
}

function N:importDef(...)
    local ok, err = self:_importDef(...)
    if ok then
        self._loaderror = nil
    else
        self._loaderror = tostring(err)
    end
    return ok, err
end

function N:_importDef(d)
    d = d or self._nodedef
    self._nodedef = assert(d)
    self:setName(d.name)
    
    self._lateBuildShader = nil
    
    local src = d.src
    local srctype
    if src then
        srctype = "glsl"
    elseif not src and d.file then
        src = d.file
        srctype = "file"
    end
    if not src and d.template then
        src = d.template
        srctype = "template"
        if src then
            self._lateBuildShader = src
        end
    end
    
    -- TODO: add support for source templates and #defines in the shader
    -- TODO: add support for vertex and fragment
    local sh, sherr = mkshader(d.shadertype, srctype, src)
    if sh == true then
        -- don't need a shader
        sh = nil
    elseif not sh then
        -- throw error and keep the already loaded shader intact, if any
        return nil, sherr
    end
    if sh then
        self:_setShader(sh)
    end
    self.makeNodeOutput = d.makeOutput
    self.outGen = d.outGen
    self.changed = d.changed
    self.connchanged = d.connchanged
    self.recalc = d.recalc
    self.mustyield = d.yield
    self._drawUI = d.drawUI
    self.drawDetailUI = d.drawDetailUI
    self.drawInfoUI = d.drawInfoUI
    self.drawOutput = d.drawOutput
    self.drawContextMenu = d.drawContextMenu
    self.openContextMenu = d.openContextMenu
    self.onUpdate = d.update
    self.drawKnobs = nil
    self._userSerialize = d.serialize
    self._userDeserialize = d.deserialize
    self.Cin = setmetatable({}, noLengthMeta)
    self.Cout = setmetatable({}, noLengthMeta)
    if not self.INPUTS then
        self.INPUTS = {}
    end
    if not self.ALLINPUTS then
        self.ALLINPUTS = {}
    end
    if not self.OUTPUTS then
        self.OUTPUTS = {}
    end
    if not self.RESULTS then
        self.RESULTS = {}
    end
    
    if self.drawContextMenu then
        self:setBorderColor(0.3, 0.4, 1, 1)
    else
        self:setBorderColor(0.4, 0.4, 0.4, 1)
    end
    
    self:trySpecializeShader() -- fix up 
    
    self._isInInit = true
    
    if d.init then
        d.init(self)
    end
    
    self._isInInit = false
    
    return self:postInit()
end

function N:postInit()
    if self._isInInit then
        return
    end
    self._isInInit = true
    
    local d = self._nodedef
    local Cout, Cin, CKnobsList = prepconn(self, self._shader, d)
    
    self:_updateConnectors()

    if self._shader then
        local knobs = CKnobsList and fun.map(CKnobsList, function(x) return x.name end)
        if knobs then
            function self:drawKnobs() -- overwrite dummy method
                return shadergraph.autoDrawControls(self._shader, table.unpack(knobs))
            end
        end
    end
    
    checkMethods(self, d)
    
    self:onSomethingChanged()
    
    self._isInInit = nil

    return true
end

local function mkerror(node, err)
    assert(err)
    
    if node.makeNodeOutput then
        return function()
            error(err)
        end
    end
    
    -- can error immediately, because this node won't attempt to produce output on its own
    error(err)
end

function N:_autoMakeOutputFunc(outName)
    debugprint(tostring(self) .. " Auto make output for " .. outName)
    local outGen = self.outGen
    local inName, formatstr, flags, px, py, pz, swizzle
    local spec = outGen and outGen[outName]
    if spec then
        local t = type(spec)
        if t == "function" then
            return function(inputs)
                return spec(inputs, outName)
            end
        elseif t == "table" then
            inName, formatstr, flags, px, py, pz, swizzle  = spec.input, spec.format, spec.flags, spec.px, spec.py, spec.pz, spec.swizzle
        elseif t == "string" then
            inName = spec
        else
            error("spec is " .. t)
        end
    end
    if formatstr and not gpu.texture.ParseFormat(formatstr) then
        error(self, "autoMakeOutput[" .. outName .. "]: Invalid format string \"" .. formatstr .. "\"")
    end
        
    -- find a texture template to use
    if not inName then
        local Cin = self.Cin
        if not next(Cin) then
            return mkerror(self, "autoMakeOutput[" .. outName .. "]: Node " .. self:getName() .. " has no output spec and no inputs, no idea what to do")
        else
            for name, inp in pairs(Cin) do
                local ty = inp.type
                if glsl.isSampler(ty) or glsl.isImage(ty) then
                    if inName then
                        return mkerror(self, "autoMakeOutput[" .. outName .. "]: Node " .. self:getName() .. " has no output spec and more than one input, must specify which input to use as template")
                    end
                    inName = name
                    -- but continue iterating to ensure we hit only one usable input texture
                end
            end
        end
    end
    
    if not inName then
        return mkerror(self, "self, autoMakeOutput[" .. outName .. "]: Node " .. self:getName() .. " has no output spec and didn't find a suitable template sampler/image")
    end
    
    --------------------------------
    debugprint("AutoOut: Input [" .. inName .. "] will be used as template for output [" .. outName .. "]: (format: " .. tostring(formatstr) .. ", flags: " .. tostring(flags) .. ")")

    return function(inputs)
        local template = inputs[inName]
        if not template then
            debugprint("_autoOut[" .. outName .. "] - don't have input to use as template!") -- this can happen when something isn't connected (yet), e.g. during load
            return false
        end
        debugprint("Create output from template: " .. tostring(template))
        -- return uninitialized texture with the same properties as the input texture unless specified otherwise
        -- all parameters are optional and override the template texture's values if not nil
        return template:newfrom(formatstr, flags, px, py, pz, swizzle)
    end
end

function N:_autoMakeOutput(inputs, outName)
    local f = self._autoOut[outName]
    if f then
        return f(inputs)
    else
        errprint("_autoOut[" .. outName .. "] - no function registered!")
    end
end

function N:trySpecializeShader(inputs, defs)
    inputs = inputs or self:_getInputsTable()
    if not self._lateBuildShader then
        return true
    end
    if not self:_hasSpecializationReq() then
        debugprint(tostring(self) .. " does not have required inputs for specialization")
        return
    end
    local ok, err = self:specializeComputeShader(self._lateBuildShader, inputs, defs)
    if not ok then
        errorbox(tostring(self) .. ": Failed to specialize shader:\n" .. tostring(err))
    end
    return ok
end

function N:_hasSpecializationReq()
    assert(self._lateBuildShader)
    
    local def = self._nodedef
    local inDef = def.inDef
    if inDef then
        debugprint(tostring(self) .. " checking specialization requirements...")
        for k, tab in pairs(inDef) do
            if tab.req then
                local inp = self.INPUTS[k] -- while initializing a node, self.INPUTS may be empty
                local from = inp and inp.node
                local obj = from and from:getOutput(inp.name)
                debugprint(tostring(self) .. " requires " .. k .. ", provided by " .. tostring(from) .. ":" .. tostring(inp and inp.name) .. ". Got: " .. tostring(obj))
                if not obj then
                    debugprint(tostring(self) .. " needs input " .. k .. " to specialize")
                    return false
                end
            end
        end
    end
    return true
end

function N:specializeComputeShader(code, inputs, defs)
    debugprint(tostring(self) .. ": Specializing compute shader...")
    return self._shader:loadTemplate(code, defs, inputs)
end

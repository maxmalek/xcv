
local T = gpu.texture
local B = gpu.gpubuf

function T.newfrom(tex, formatstr, texflags, px, py, pz, swizzle)
    if tex then
        local w, h, d = tex:size()
        local new = T.new(w + (px or 0), h and (h + (py or 0)), d and (d + (pz or 0)),
            formatstr or tex:formatstr(),
            texflags or tex:texflags(),
            swizzle or tex:swizzle()
        )
        new:setPixelSize(tex:getPixelSize())
        return new
    end
end

function T:saveAs(fn, ...)
    fn = fn or askSaveFile() -- TODO: list of supported formats
    if fn then
        return self:save(fn, ...)
    end
end

function T:drawSaveAsButton(postfix)
    local id = "Save as...##texsaveas_"
    if postfix then
        id = id .. postfix
    end
    if imgui.Button(id) then
        local ok, err = self:saveAs() -- if nil: abort
        if ok == false then
            errorbox(err)
        end
    end
end

function T:drawTooltip()
    imgui.Text(tostring(self))
    local fn = self.filename
    fn = fn and fn:match("([^/]+)$") -- basename
    --local tyname = glsl.nameof(self:glsltype())
    local mb = self:rawsize() / (1024*1024)
    local s = ("%s\nFormat: %s\nSwizzle: %s\nPixel size: %s\nMemory: %.2f MB")
        :format(self:sizestring(), self:formatstr(), self:swizzle(), self:pixelsizestring(), mb)
    if fn then
        s = s .. "\nFile: " .. fn
    end
    if self:dimensions() < 3 then
        imgui.ImageMaxWH(self, 100,  nil, nil, nil, nil,  1,1,1,1)
    else
        -- TODO: render 3D texture in some way that is fast and universal
        imgui.Dummy(20, 20)
    end
    imgui.SameLine()
    imgui.Text(s)
   
end

function T:tooltip()
    imgui.Tooltip(self.drawTooltip, self)
end


local DIMSTR = { "%d", "%dx%d", "%dx%dx%d" }
function T:sizestring(sep)
    if not sep then
        return DIMSTR[self:dimensions()]:format(self:size())
    end
    
    return table.concat({self:size()}, sep)
end

local DIMSTRF = { "(%.3f)", "(%.3f, %.3f)", "(%.3f, %.3f, %.3f)" }
function T:pixelsizestring(sep)
    if not sep then
        return DIMSTRF[self:dimensions()]:format(self:getPixelSize())
    end
    
    return table.concat({self:getPixelSize()}, sep)
end

function T:__tostring()
    local glty = self:glsltype()
    local s = ("Texture(%s, %s, %s, %s)"):format(glsl.nameof(glty), self:sizestring(), self:formatstr(), self:swizzle())
    --[[if self.filename then
        s = s .. "\n [" .. self.filename .. "]"
    end]]
    return s
end

local function _layoutstr(self)
    return "layout(" .. self:formatstr() .. ")"
end

function T:samplertypestring()
    local dims = self:dimensions()
    return ("%s sampler%dD"):format(_layoutstr(self), dims)
end

function T:imagetypestring(...)
    local dims = self:dimensions()
    local modstr = table.concat({...}, " ")
    return ("%s %s image%dD"):format(_layoutstr(self), modstr, dims)
end

-- for creating definitions in shaders
function T:uniform_sampler()
    local dims = self:dimensions()
    return ("uniform sampler%dD"):format(dims)
end
function T:uniform_image(...)
    local dims = self:dimensions()
    local layout = _layoutstr(self)
    local modstr = table.concat({...}, " ")
    return ("%s %s uniform sampler%dD"):format(layoutstr, modstr, dims)
end


function B:__tostring()
    local sz = self:rawsize()
    local rp, wp = self:readptr() or self:writeptr()
    local ra, wa = debug.addressof(rp), debug.addressof(wp)
    local r, w = "", ""
    if rp and rp == wp then
        r = (", rwptr = %x"):format(ra)
    else
        if rp then
            r = (", rptr = %x"):format(ra)
        end
        if wp then
            w = (", wptr = %x"):format(wa)
        end
    end
    return ("gpubuf[%u bytes%s%s]"):format(sz, r, w)
end

function B:saveAs(fn, ...)
    fn = fn or askSaveFile()
    if fn then
        return self:save(fn, ...)
    end
end

function B:drawSaveAsButton(postfix)
    local id = "Save as...##bufsaveas_"
    if postfix then
        id = id .. postfix
    end
    if imgui.Button(id) then
        local ok, err = self:saveAs() -- if nil: abort
        if ok == false then
            errorbox(err)
        end
    end
end

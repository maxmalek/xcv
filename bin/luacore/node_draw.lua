local N = shadergraph.node

function N:drawKnobs()
    -- overwritten in node_gen. Expected to return true if node must recalc.
end

-- C++ API method
function N:drawUI()
    if self._drawUI then -- maybe set in node_gen.lua
        if self:_drawUI() then
            self:onSomethingChanged()
        end
    else
        self:drawUIDefault()
    end
    if self.gputimer and CONFIG.debugmode then
        local ms = self.gputimer:difftimeMS()
        local s = ("%.4f ms"):format(ms)
        imgui.TextDisabled(s)
    end
end

function N:drawUIDefault()
    if self:drawKnobs() then
        self:onSomethingChanged()
    end
end

-- possibly overwritten in node_gen
function N:drawDetailUI(...)
    return self:drawDetailUIDefault(...)
end

local function _drawImage(obj)
    imgui.Text(tostring(obj))
    imgui.ImageWidthFit(obj,    nil, nil, nil, nil,    1,1,1,1)
end

local function _drawUnknown(obj)
    imgui.TextDisabled("Is a " .. tostring(obj.__name) .. " object:")
    imgui.Text(tostring(obj))
end

local DRAW_DISPATCH =
{
    [glsl.sampler1D] = _drawImage,
    [glsl.sampler2D] = _drawImage,
    [glsl.image1D] = _drawImage,
    [glsl.image2D] = _drawImage,
    
    -- FIXME: not sure if this is correct for iimage/isampler
    [glsl.isampler1D] = _drawImage,
    [glsl.isampler2D] = _drawImage,
    [glsl.iimage1D] = _drawImage,
    [glsl.iimage2D] = _drawImage,
    
}

local function _getDrawFunc(obj)
    return (obj.glsltype and DRAW_DISPATCH[obj:glsltype()]) or _drawUnknown
end

-- can be overridden
function N:drawOutput(name, obj)
    return self:drawOutputDefault(name, obj)
end

function N:drawOutputDefault(name, obj)
    local obj = obj or self.RESULTS[name]
    if not obj then
        return
    end
    local f = _getDrawFunc(obj)
    local ch = f(obj) -- trigger update if this returns true value
    if obj.drawSaveAsButton then
        obj:drawSaveAsButton()
    end
    return ch
end

local function nodedebug(self)
    if CONFIG.debugmode then
        imgui.Separator()
        imgui.TextDisabled("Node debug:")
        debugui.walknode(self)
    end
end

function N:drawDetailUIDefault()
    if self._loaderror then
        imgui.TextDisabled("Load error:")
        imgui.Text(self._loaderror)
        return
    end
    
    local R = self.RESULTS
    if not R or not next(R) then
        imgui.TextDisabled("No results.")
        nodedebug(self)
        return
    end
    
    local outputs = {}
    local n = 0
    for k, obj in pairs(R) do
        n = n + 1
        outputs[n] = k
    end
    table.sort(outputs)
    
    local ch
    --imgui.TextDisabled("Got " .. n .. " results:")
    for i = 1, n do
        local name = outputs[i]
        local obj = R[name]
        local _, changed = imgui.TreeNode(name, "of", self.drawOutput, self, name, obj)
        ch = ch or changed
    end
    
    nodedebug(self)
    
    if ch then
        self:onSomethingChanged()
    end
end

local function _drawConnTooltip(desc, obj)
    local detail = desc.detail
    if detail and detail ~= "" then
        imgui.Text(detail)
    end
    if obj then
        if obj.drawTooltip then
            obj:drawTooltip()
        elseif obj.__tostring then
            imgui.Text(tostring(obj))
        else
            imgui.Text("OBJECT[" .. obj.__name .. "]: " .. tostring(obj))
        end
    end
end

function N:drawConnectorTooltip(desc, obj)
    imgui.Tooltip(_drawConnTooltip, desc, obj)
end

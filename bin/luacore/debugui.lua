local tostring = tostring
local isValidObj = isValidObj
local imgui = assert(imgui)

G.debugui = G.debugui or {}

local W = newclass("debugui_syswindow", assert(sys.syswindow))

function W:onClose()
    if debugui._window == self then
        debugui._window = nil
    end
end

local function newWin(x, y)
    local win = debugui._window
    if not win then
        win = W:Open(x or 800, y or 600)
        debugui._window = win
    end
    return win
end

local function getwin(new)
    local win
    if not new then
        win = sys.syswindow.GetCurrent()
    end
    if not win then
        win = newWin()
    end
    return win
end

function debugui.showBuffer(name, obj)
    local addr = debug.addressof(obj)
    local me = gui.memoryeditor.new()
    me:set(obj)
    
    local win = getwin()
    local iw = win:newInner("[" .. name .. "] memory view: " .. tostring(obj) .. " [" .. ("%X"):format(addr) .. "]", tostring(addr), 800, 600)
    iw:setShowMenuBar(true)
    
    function iw:drawUI()
        me:drawDefault()
    end
    iw:detach()
end

function debugui.showText(name, str)
    str = tostring(str)
    local win = getwin()
    local iw = win:newInner(name, "", 400, 400)
    function iw:drawUI()
        imgui.Text(str)
    end
    iw:detach()
end

local function rawtostring(x)
    local a = debug.topointer(x)
    local as = tostring(a):match("(%S+)$")
    return ("%s: %s"):format(type(x), as)
end

local function toprettystring(x)
    local tx = type(x)
    if tx == "function" then
        local d = debug.getinfo(x, "nS")
        return ("%s function (%s:%d)"):format(d.what, d.short_src, d.linedefined)
    end
    if tx == "string" then
        return '"' .. x .. '"'
    end

    -- HACK: do not use __tostring() when trying to display metatables
    -- tostring() would see __tostring metamethod exists (since metatables of objects
    -- have their class table set as metatable), and would thus try to call x:__tostring()
    -- ... which goes boom when x is not an actual object.
    if tx == "table" then
        if rawget(x, "__class") and x.__tostring then
            return tostring(x.__name) .. " " .. rawtostring(x)
        end
    end
    
    -- This is not so nice when the label is also used as the imgui ID...
    -- (causes nodes to close when the returned string is different, etc)
    --[[if tx == "table" then
        local s = tostring(x)
        if not next(x) then
            return "empty " .. s
        end
        return s .. " (#=" .. rawlen(x) .. ")"
    end]]
    
    return tostring(x)
end
local function tostringkv(k, v)
    return "[" .. toprettystring(k) .. "] = " .. toprettystring(v)
end
local function TextError(s)
    return imgui.TextColor(s, 1, 0.2, 0.2)
end

local function _getDrawFunc(v)
    if not isValidObj(v) then
        return
    end
    local drawtt = v.drawTooltip
    if drawtt then
        return drawtt
    end
end

local kviter = assert(debugx.kviter)
local walkit = assert(debugx.walkit)
local TreeNode = assert(imgui.TreeNode)
local Group = assert(imgui.Group)
local _walk, _drawkv
local _WININC = 0 -- MEGA HACK: to make win id unique. FIXME: if window exists, just bring to front
local inspectwindow = newclass("debugui_inspect_innerwindow", assert(gui.imwindow))

function inspectwindow:drawUI()
    local o = self.container.obj
    if not o or isDeadObj(o) or (self.closewhenempty and not next(o)) then
        self:destroy()
        return
    end
    _walk(o)
end

local function _watch(title, obj, autoclose, win)
    local container = { obj = obj }
    if autoclose == true then
        setmetatable(container, { __mode = "kv" })
    end
    if win == true then
        win = getwin(true)
    end
    win = win or getwin()
    assert(win, "failed to get window")
    local id = "debugui:" .. _WININC .. ":" .. (tostring(debug.topointer(obj)) or (type(obj) .. "-" .. tostring(obj)))
    _WININC = _WININC + 1
    local iw = win:newInner(title, id, 400, 400)
    switchclass(iw, inspectwindow)
    iw.container = container
    iw.closewhenempty = autoclose == "empty" and type(obj) == "table"
    iw:detach()
end
local function _walkNodePopup(v)
    local sv = toprettystring(v)
    imgui.TextDisabled(sv)
    imgui.Separator()
    if imgui.MenuItem("Watch (keep open)") then
        _watch(sv, v)
    end
    if imgui.MenuItem("Watch (autoclose when GC'd)") then
        _watch(sv, v, true)
    end
    return true -- returns ok for caller
end
local function _checkFocused(h, v)
    if imgui.IsItemHovered() then
        if isValidObj(v) and v.tooltip then
            v:tooltip()
        end
        if imgui.IsMouseClicked(1) then
            imgui.OpenPopup(h)
        end
    end
    imgui.PopupBody(h, _walkNodePopup, v)
end
-- inject popup right after the source node, then expand the node
local function _ttwrap(label, v, treefunc, ...)
    _checkFocused(label, v)
    if treefunc then
        return treefunc(...)
    end
end
local function _walkkv(k, v, vit)
    local f = _getDrawFunc(v)
    if f then
        --TreeNode("PREVIEW", "", f, v)
        f(v)
    end
        
    local kit = kviter(k)
    if kit then
        TreeNode("<KEY> ", "", _ttwrap, toprettystring(k), k, walkit, kit, _drawkv)
    end
    return walkit(vit, _drawkv)
end
local function _drawkv_unsafe(k, v)
    local h
    if k then
        local s = tostringkv(k, v)
        local vit = kviter(v)
        if vit then
            h = not TreeNode(s, "", _ttwrap, s, v, _walkkv, k, v, vit) and s
        else
            TreeNode(s, "l")
            h = s
        end
    else
        h = toprettystring(v)
        local f = _getDrawFunc(v)
        if f then
            h = TreeNode(h, "", _ttwrap, h, v, f, v) and h
        else
            TreeNode(h, "l")
        end
    end
    if h then -- didn't recurse into a tree node
        _checkFocused(h, v)
    end
end
_drawkv = function(...)
    return _drawkv_unsafe(...)
    --[[local ok, err = pcall(_drawkv_unsafe, ...)
    if not ok then
        local k, v = ...
        local ok2, ret = pcall(tostringkv, k, v)
        if ok2 then
            TreeNode(ret, "", TextError, err)
        else
            local okk, sk = toprettystring(k)
            local okv, sv = toprettystring(v)
            ((okk and imgui.Text) or TextError)("KEY = " .. sk)
            ((okv and imgui.Text) or TextError)("VAL = " .. sv)
        end
    end]]
end
_walk = function(obj)
    debugx.walk(obj, _drawkv)
end
debugui.walk = _walk
function debugui.walknode(obj, flags)
    local s = toprettystring(obj)
    if not TreeNode(s, flags, _ttwrap, s, obj, _walk, obj) then
        _checkFocused(s, obj)
    end
end
function debugui.watchwindow(obj, autoclose, srcwin)
    return _watch(toprettystring(obj), obj, autoclose, srcwin)
end

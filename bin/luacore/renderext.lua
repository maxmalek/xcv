local RO = render.renderobject

function RO.newGroup()
    local self = RO.new()
    self:setLocalVisible(false)
    return self
end


ui.debugmenu = ui.debugmenu or {}

local floor = math.floor

--[=[
local function addTestWindow1(win)
    -- actual computation + uniform passing + buffer passing test
    local sh1 = gpu.computeshader.Create([[
    #version 430
    layout(local_size_x = 512) in;
    uniform float val;
    layout(std430, binding = 3) restrict writeonly buffer OutBuf
    {
        float p[];
    } outbuf;
    void main() { outbuf.p[gl_LocalInvocationIndex] = val + float(gl_LocalInvocationIndex); };
    ]]):bless()
    local x, y, z = sh1:localsize()
    local b = gpu.gpubuf.new(x * 4, "rwup")
    sh1.val = 42
    sh1.OutBuf = b
    sh1:dispatch(1)

    local m = win:newMemoryEditor("GPU buf test")
    function m:drawUI()
        sh1.val = math.random()
        sh1:dispatch(1)
    end
    m:set(b)
    m:detach()
end

local function addTestWindow2(win)
    --[[local mv = numvec.new(glsl.mat4, 1000) -- bunch of matrices
    mv:mscale(glsl.vec3, 1.5, 2, 5)
    mv:mul(mv)
    mv:mul(glsl.float, 10)]]
    local mv = numvec.new(glsl.int, 256)
    for i = 1, mv:len() do
        mv:set(i, i-1)
    end
    local me = win:newMemoryEditor("CPU buf test")
    me:set(mv)
    me:detach()
end
]=]

-- Crash the GPU. 'hard' additionally causes an endless loop.
local function gpusegfault(hard)
    local b = gpu.gpubuf.new(1024, "u")
    local sh = gpu.computeshader.Create([[
    #version 430
    uniform bool hard;
    layout(local_size_x = 1024) in;
    layout(std430) restrict writeonly buffer OutBuf
    {
        float p[];
    } outbuf;
    void main() {
        for(uint i = 0; hard || i < 128; ++i)
            outbuf.p[gl_LocalInvocationIndex * 1024 + i] = gl_LocalInvocationIndex * (i+1);
    }
    ]]):bless()
    sh.OutBuf = b
    sh.hard = not not hard
    sh:dispatch(32)
    print("GPU Segfault test done")
end

local SPACE = "        "
local function addKeyboardWindow(win)
    local w = win:newInner("Keyboard Input", "", 500, 350)
    w.lines = {}
    function w:onKeyEvent(keyname, scancode, keymod, down)
        local s = '"' .. keyname .. '"' .. SPACE .. scancode .. SPACE .. keymod .. SPACE .. tostring(down)
        table.insert(self.lines, s)
        if #self.lines > 10 then
            table.remove(self.lines, 1)
        end
    end
    function w:drawUI()
        imgui.TextDisabled("keyname | scancode | keymod | down")
        local L = self.lines
        for i = 1, #L do
            imgui.Text(L[i])
        end
    end
    w:detach()
end

local function getLoadedPluginsSortedByName()
    local t = plugin.listloaded()
    table.sort(t, function(a, b) return a:name() < b:name() end)
    return t
end

local function _drawPlugins(win)
    local list = getLoadedPluginsSortedByName()
    if imgui.MenuItem("(Unload all " .. #list .. ")") then
        for _, plugin in pairs(list) do
            debugprint("Unload " .. plugin:name())
            plugin:unload()
        end
        return
    end
    imgui.Separator()
    imgui.TextDisabled("Click to unload:")
    for _, plugin  in ipairs(list) do
        if imgui.MenuItem(plugin:name()) then
            plugin:unload()
        end
    end
    imgui.Separator()
    imgui.TextDisabled("package.loaded:")
    for k in pairs(package.loaded) do
        imgui.TextDisabled("  " .. k)
    end
end

local function countobj(t, objs)
    for refid, o in pairs(objs) do
        if isValidObj(o) then
            local n = o.__name or "<missing type>"
            t[n] = (t[n] or 0) + 1
        end
    end
end

local function imgui_objinfo()
    local t = {}
    local reg = debug.getregistry()
    countobj(t, reg[".Objs"])
    countobj(t, reg[".PObjs"])
    countobj(t, reg[".LObjs"])
    local tt = { "Objects allocated:" }
    local i = 1
    for k, v in pairs(t) do
        i = i + 1
        tt[i] = k .. ": " .. v
    end
    table.sort(tt)
    imgui.Tooltip(table.concat(tt, "\n"))
end

function ui.debugmenu.drawMenuItems(win)
    win = win or assert(sys.syswindow.GetCurrent(), "No current window")
    
    if imgui.MenuItem("Keyboard input") then
        addKeyboardWindow(win)
    end
    
    --[[if imgui.MenuItem("CPU buf test") then
        addTestWindow2(win)
    end
    if imgui.MenuItem("GPU Buf+Hex test") then
        addTestWindow1(win)
    end]]
    
    local gcon = collectgarbage("isrunning")
    if imgui.MenuItem("Lua GC enabled", gcon) then
        if gcon then
            collectgarbage("stop")
        else
            collectgarbage("restart")
        end
    end
    
    if imgui.MenuItem("Run GC Now (In use: " .. floor(collectgarbage("count")) .. " KB)") then
        collectgarbage()
    end
    if(imgui.IsItemHovered()) then
        imgui_objinfo()
    end
    
    if imgui.MenuItem("Debug current window...") then
        debugui.watchwindow(sys.syswindow.GetCurrent())
    end
    
    imgui.Menu("Plugins", true, _drawPlugins)

    
    imgui.Separator()
    imgui.TextDisabled("Danger Zone")
    imgui.Separator()
    
    if imgui.MenuItem("GPU buffer OOB test") then
        gpusegfault()
    end
    if imgui.MenuItem("GPU segfault & endless loop") then
        gpusegfault(true)
    end
end

function ui.debugmenu.drawMenu(win)
    imgui.Menu("Debug", true, ui.debugmenu.drawMenuItems, win)
end


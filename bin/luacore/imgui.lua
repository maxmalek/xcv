-- Additional functions for imgui

local imgui = assert(imgui)
local min = math.min
local max = math.max

local TextColor = assert(imgui.TextColor)
local IsItemClicked = assert(imgui.IsItemClicked)
local GetCursorPos = assert(imgui.GetCursorPos)
local SetCursorPos = assert(imgui.SetCursorPos)

-- Aspect-ratio-aware scaling to be exactly width pixels wide 
function imgui.ImageWidth(tex, width,  ...)
    local height = 10
    if tex then
        local w, h = tex:displaysize()
        height = width * (h / w)
    end
    imgui.Image(tex, width, height, ...)
    return width, height
end

function imgui.ImageWidthFit(tex, ...)
    local w, h = imgui.GetContentRegionAvail()
    return imgui.ImageWidth(tex, w, ...)
end

function imgui.ImageHeight(tex, height,  ...)
    if  tex then
        local w, h = tex:displaysize()
        width = height * (h / w)
    end
    imgui.Image(tex, width, height, ...)
    return width, height
end

-- Make sure the final image is no more than maxwh pixels wide or high while preserving aspect
function imgui.ImageMaxWH(tex, maxwh,  ...)
    local width, height
    if tex then
        local w, h = tex:displaysize()
        if w > h then
            width = maxwh
            height = maxwh * (h / w)
        else
            width = maxwh * (w / h)
            height = maxwh
        end
    else
        width = maxwh
        height = maxwh
    end
    imgui.Image(tex, width, height, ...)
    return width, height
end

-- w, h is avail. space -> fit in, keeping aspect ratio
function imgui.ImageKeepAspect(tex, maxw, maxh,  ...)
    local w, h
    if tex then
        w, h = tex:displaysize()
        local ratio = min(maxw / w, maxh / h)
        w, h = w*ratio, h*ratio
    else
        w, h = maxw, maxh
    end
    imgui.Image(tex, w, h, ...)
    return w, h
end

function imgui.ImageKeepAspectFit(tex, ...)
    local w, h = imgui.GetContentRegionAvail()
    return imgui.ImageKeepAspect(tex, w, h, ...)
end

local function _colorPopup(r,g,b,a)
    imgui.Text("Edit color")
    local ch
    if a then
        ch,r,g,b,a = imgui.ColorEdit4("##edit", r,g,b,a)
    else
        ch,r,g,b = imgui.ColorEdit4("##edit", r,g,b)
    end
    if imgui.Button("Close") then
        imgui.CloseCurrentPopup()
    end
    if a then
        return ch,r,g,b,a
    end
    return ch,r,g,b
end


function imgui.ColorChooser(r, g, b, a)
    local button = 1
    local ch, r2, g2, b2, a2
    if imgui.ColorButton(r,g,b,a or 1, false, true) then
        if imgui.OpenPopup("colorchooser") then
        end
        ch, r2, g2, b2, a2 = imgui.PopupBody("colorchooser", _colorPopup, r,g,b,a)
    else
        ch, r2, g2, b2, a2 = imgui.PopupContextItem("colorchooser", 1, _colorPopup, r,g,b,a)
    end
    if ch then
        r,g,b,a = r2,g2,b2,a2
    end
    return ch, r,g,b,a
end

local GoodInputTextBAD =
{
    [imgui.Col_FrameBg]            = {0.3,0.0,0.0},
    --[imgui.Col_FrameBgHovered]     = {0.3,0.0,0.0},
    --[imgui.Col_FrameBgActive]      = {0.4,0.0,0.0},
}
function imgui.InputTextGood(ok, ...)
    local f = imgui.InputText
    if ok then
        return f(...)
    else
        return imgui.ApplyStyle(GoodInputTextBAD, f, ...)
    end
end

function imgui.IsBackgroundHovered()
    return not imgui.IsAnyItemActive()
      and (not imgui.IsMouseHoveringAnyWindow()
           or  imgui.IsMouseHoveringRootWindow())
end

local isep = imgui.Separator
local idummy = imgui.Dummy

function imgui.SeparatorY(y)
    idummy(0, y)
    isep()
    idummy(0, y)
end

local function _textlink(s)
    local x, y = GetCursorPos()
    SetCursorPos(x, y+1)
    TextColor(("_"):rep(#s), 0.4, 0.4, 1)
    SetCursorPos(x, y)
    TextColor(s, 0.4, 0.4, 1)
end
function imgui.TextLink(s, url)
    _textlink(s)
    if IsItemClicked() then
        os.openurl(url)
        return true
    end
end

local gb = newclass("graphbuilder", assert(shadergraph.editor))

local W = newclass("innerwindow_graphbuilder", assert(gui.imwindow))

function W:drawUI()
    self.gb:drawMenuBar()
    imgui.Dummy(0) -- fixes alignment issues...?
    self.gb:drawPanel() -- C++ function
end
function W:drawDetailUI()
    return self.gb:drawDetailUI(self)
end
function W:drawInfoUI()
    return self.gb:drawInfoUI(self)
end
function W:onUpdate(dt)
    self.gb:updateAnimation(dt * 5)
    self.gb:onUpdate(dt)
end
function W:onKeyEvent(...)
    return self.gb:onKeyEvent(...)
end

----------------------------------------------

function gb:onUpdate(dt)
    for i = 1, self:getMaxNodes() do
        local n = self:getNode(i)
        if n and n.onUpdate then
            n:onUpdate(dt)
        end
    end
end

local function _nodesMenu(self)
    local c = 0
    local t = {}
    for i = 1, self:getMaxNodes() do
        local n = self:getNode(i)
        if n then
            c = c + 1
            t[c] = n
        end
    end
    table.sort(t, function(a, b) return a:getName() < b:getName() end)
    for i = 1, c do
        local n = t[i]
        if imgui.MenuItem(n:getName() .. " [" .. n:getInternalID() .. "]") then
            self:centerOnNode(n)
        end
    end
    return c
end

function gb.NewInner(win, ...)
    local b = graphbuilder.new()
    local ww = win:newInner("Graph builder", ...)
    switchclass(ww, W)
    ww.gb = b
    ww:setShowMenuBar(true)
    return ww, gb
end

function gb.new(dir)
    local g = gb.__baseclass.new()
    
    g._searchList = {}
    
    -- convert to new class type
    switchclass(g, gb)
    
    g:loadnodes(dir or "nodes")
    
    return g
end

local function checkdef(d)
    if type(d) ~= "table" then
        return
    end
    
    if not not d.serialize ~= not not d.deserialize then
        if d.serialize then
            errorbox("Warning: serialize() is present, but deserialize() is not")
        else
            errorbox("Warning: deserialize() is present, but serialize() is not")
        end
    end
    
    if not d.name then
        d.name = "[file: " .. d._srcfile .. "]"
    end
    if d.category then
        d._listname = d.category:sub(1,1):upper() .. d.category:sub(2) .. ": " .. d.name
    else
        d._listname = d.name
    end
    
    return true
end

local function errdef(fn, err)
    local def = { name = "File:" .. fn .. " (load error)", _err = err, _srcfile = fn }
    checkdef(def)
    return def
end

local function makedef(fn)
    local f, err = sandbox.loadfile(fn)
    if f then
        local ok, def = pcall(f)
        if ok then
            if def then
                if def.category == "DEBUG" then
                    def.debugmode = true
                end
                def._srcfile = fn
            end
            if checkdef(def) then
                return def
            else
                err = "No info table returned"
            end
        else
            err = def
        end
    end
    return errdef(fn, err)
end

local function getname(def)
    return def.name
end

local _less = string.natless
local function defsort(a, b)
    return _less(a._listname, b._listname)
end

local function node_onScriptReload(self)
    assert(self)
    assert(isValidObj(self))
    local h = self:getHolder()
    if not h then
        errprint("node_onScriptReload: No holder for " .. tostring(self))
        return
    end
    local k = self._nodefile
    local def = h._defs[k]
    if not def then
        errorbox("No def for " .. tostring(k))
    end
    self:importDef(def)
end

-- given a note def, return a generator function that inits a node based on the def.
local function def2gen(d, g)
    local g = g
    if d._err then
        local msg = "Error loading node [" .. d._srcfile .. "]:\n" .. d._err
        return function(n)
            n._loaderror = msg
            errprint(msg)
        end
    end
    
    return function(n)
        local ok, err = n:importDef(d)
        if not ok then
            errorbox(err)
        end
    end
end

function gb:loadnodes(dir)
    self._filesdir = dir
    local defs = fun.filteri(
        fun.map(
            fun.filteri(
                io.files(dir),
                function(s) return s:wildcard("*.lua") end
            ),
            function(f) return makedef(dir .. "/" .. f) end
        ),
        function(d) return d end
    )
    table.sort(defs, defsort)
    local g = fun.map(defs, def2gen, self)
    for i = 1, #g do
        g[defs[i].name] = g[i] -- also allow lookup by name and def
        g[defs[i]] = g[i]
    end
    self._generators = g
    
    -- also support lookup by file name (this is for node_onScriptReload()) and node name
    local tmp = {} -- avoid modification while iterating
    for _, d in pairs(defs) do
        tmp[d._srcfile] = d
        tmp[d.name] = d
    end
    for k, v in pairs(tmp) do
        defs[k] = v
    end
    self._defs = defs
end

local function _setupNode(self, def)
    assert(def and def.name)
    local node = self:newNode()
    node._nodefile = def._srcfile
    node._nodedef = def
    node.onScriptReload = node_onScriptReload
    node:setName(def.name)
    node:setPos(self:getLocalCursorPos())
    self._generators[def](node)
    return node
end

-- C++ API method
function gb:canConnect(srcnode, srcslot, targetnode, targetslot)
    return srcnode:canConnectTo(targetnode, srcslot, targetslot)
end

function gb:onScriptReload()
    self:loadnodes(self._filesdir)
end 

function gb:loadFile(fn)
    if type(fn) == "string" then
        -- all good
    elseif not fn then
        fn = askOpenFile()
    end
    
    if fn then
        local t, err = serialize.restorefile(fn)
        if t then
            self:deserialize(t)
        else
            errorbox("Failed to load graph: " .. err)
        end
    end
end

function gb:save(fn)
    if fn == true then
        fn = askSaveFile()
    elseif type(fn) == "string" then
        -- all good
    else
        fn = self._lastSaveAsFn or askSaveFile()
    end
    if fn then
        self:_saveAs(fn)
    end
end

function gb:_saveAs(fn)
    print("Saving to [" .. fn .. "] ...")
    local fh = io.open(fn, "w")
    fh:write(self:serializeStr())
    fh:close()
    self._lastSaveAsFn = fn
end

local function _serializeNodeListToTab(nodes)
    local N, C, indexes = {}, {}, {}
    
    local w = 0
    for i = 1, #nodes do
        local node = nodes[i]
        w = w + 1
        indexes[node] = w
        N[w] = node:serialize()
    end
    
    w = 0
    for node, nodeidx in pairs(indexes) do
        for inName, t in pairs(node:getInputSources()) do
            w = w + 1
            local srcnode, srcslotname = t.sourcenode, t.sourceslot
            if indexes[srcnode] then -- only save connected nodes that are in the set of nodes to save
                -- { fromNode, toNode, fromSlot, toSlot }
                C[w] = { indexes[srcnode], nodeidx, srcslotname, inName }
            end
        end
    end
    
    return { nodes = N, connections = C, version = 1 }
end

local function _serializeNodeListToStr(nodes, pretty)
    local t = _serializeNodeListToTab(nodes)
    return serialize.save(t, pretty)
end

local function _deserializeTab(self, t)
    local N = t.nodes
    local C = t.connections
    local nn = {}
    local defs = self._defs
    for i = 1, #N do
        local tn = N[i]
        local def = defs[tn.file]
        if def then
            devprint("Add node [" .. tn.file .. "], def = " .. tostring(def))
            local n = _setupNode(self, def)
            n:deserialize(tn)
            nn[i] = n
        else -- FIXME: create empty placeholder node?
            errprint("Failed to deserialize node [" .. tn.file .. "] -- script not loaded")
        end
    end
    local conns, k = {}, 1
    for i = 1, #C do
        local a, b, aconn, bconn = table.unpack(C[i])
        local from, to = nn[a], nn[b]
        if from and to then
            local con = from:connectToTarget(aconn, to, bconn, true) -- force create connections
            conns[k] = con
            k = k + 1
        end
    end
    return nn, conns
end

function gb:serialize()
    return _serializeNodeListToTab(self:getAllNodes())
end
function gb:serializeStr(pretty)
    return _serializeNodeListToStr(self:getAllNodes(), pretty)
end


function gb:deserialize(t)
    assert(t.version == 1)
    self:removeAllNodes()
    _deserializeTab(self, t)
    self:centerOnCenterOfMass()
    self:doGlobalRefresh()
end

--[[
function gb:deserializeStr(inp)
    self:deserialize(serialize.restore(inp))
end
]]

function gb:doGlobalRefresh()
    devprint("GraphBuilder:doGlobalRefresh()")

    local nodes = self:getAllNodes()
    -- update all connections and find nodes marked as sink (just need inputs; each output is an input somewhere)
    local lut = {}
    for i = 1, #nodes do
        lut[nodes[i]] = true
    end
    
    -- make sure to only carry over terminal sinks, not intermediate sinks
    local sinks = {}
    for n, _ in pairs(lut) do
        lut[n] = nil
        local nsinks, mid = n:getDownstreamSinks()
        for _, v in pairs(nsinks) do
            lut[v] = nil
            sinks[v] = true
        end
        for _, v in pairs(mid) do
            lut[v] = nil
        end
    end
    devprint("GraphBuilder:doGlobalRefresh(), " .. table.count(sinks) .. " sinks...")
    for node, _ in pairs(sinks) do
        devprint("GraphBuilder:doGlobalRefresh(), Update sink " .. tostring(node))
        node:processSink()
    end
    
    devprint("GraphBuilder:doGlobalRefresh() done")
end


------------------------------------------------------------------------------------------------------



local function _addAsTags(t, s, f)
    if s then
        for part in s:gmatch("[^%s,%.;:%|]+") do
            part = part:lower()
            t[(f and f(part)) or part] = true
        end
    end
    return t
end

local function _buildAllDefTags(defs)
    local soup = {}
    for i = 1, #defs do
        local def = defs[i]
        local t = {}
        _addAsTags(t, def._listname)
        _addAsTags(t, def.desc)
        _addAsTags(t, def.author)
        _addAsTags(t, def.tags)
        soup[def] = t
    end
    return soup
end

local function _hasKeyPart(deftags, part)
    for k in pairs(deftags) do
        if k:wildcard(part) then
            return true
        end
    end
end
local function _defMatchesSearch(def, deftags, searchtags)
    for part in pairs(searchtags) do
        if not (deftags[part] or _hasKeyPart(deftags, part)) then
            --print(def.name, "tag not found:", part)
            return
        end
    end
    return true
end


local function _updateSearchList(list, defs, search)
    local tagsoup = _buildAllDefTags(defs)
    local searchtags = _addAsTags({}, search, function(s) return "*" .. s .. "*" end)
    
    --local tt = {} for ss in pairs(searchtags) do table.insert(tt, ss) end
    --print("search for tags [" .. table.concat(tt, " ") .. "]")
    
    local idx = 1
    for i = 1, #defs do
        local def = defs[i]
        if not def.debugmode or CONFIG.debugmode then -- exclude debug nodes
            if _defMatchesSearch(def, tagsoup[def], searchtags) then
                list[idx] = def
                idx = idx + 1
            end
        end
    end
    for i = idx, #list do
        list[i] = nil
    end
    table.sort(list, defsort)
end

--------------------

local function _testCanPasteClipboard()
    local str = gui.getClipboardText()
    return str and not not serialize.tofunction(str) -- returns a function if valid, but we don't need to store that
end

local function _updateCanPaste(self)
    self._canPaste = _testCanPasteClipboard()
    --infoprint("_updateCanPaste:", self._canPaste)
end

-- C++ API function
function gb:onOpenContextMenu(node)
    if node then
        if node.openContextMenu then
            node:openContextMenu()
        end
    else
        self.searchtext = self.searchtext or ""
        self._focusPopup = true
        _updateSearchList(self._searchList, self._defs, self.searchtext)
    end
    
    _updateCanPaste(self)
end


function gb:selectAll()
    local nodes = self:getAllNodes()
    local cons = {}
    for _, node in pairs(nodes) do
        local cs = node:getAllConnections() -- ignore outgoing
        for _, c in pairs(cs) do
            cons[c] = true
        end
        node:setSelected(true)
    end
    for c in pairs(cons) do
        c:setSelected(true)
    end
end

function gb:pasteFromClipboard(px, py)
    local str = gui.getClipboardText()
    if str then
        return self:pasteFromStr(str, px, py)
    else
        errprint("Clipboard is empty!")
    end
end

function gb:pasteFromStr(str, px, py)
    local t, err = serialize.restore(str)
    if t then
        local newnodes = _deserializeTab(self, t)
        gb:adjustPastePosition(newnodes, px, py)
    elseif err then
        errorbox("Failed to deserialize graph: " .. err)
    end
end

local function _getSelOrOne(self, node)
    local sel = self:getSelectedNodes()
    if #sel == 0 and node then
        sel[1] = node
    end
    return sel
end
local function _serializeSel(self, node, pretty)
    local sel = _getSelOrOne(self, node)
    if #sel > 0 then
        return _serializeNodeListToStr(sel, pretty)
    end
end
local function _actionEditCopy(self, node)
    local str = _serializeSel(self, node)
    if str then
        gui.setClipboardText(str)
    end
end
local function _actionEditDelete(self, node)
    for _, node in pairs(_getSelOrOne(self, node)) do
        node:destroy()
    end
end
local function _actionEditSelectAll(self)
    self:selectAll()
end

local CONTEXT_MENU_ACTIONS = { "Select all", "Copy (to clipboard)", "Delete" }
local CONTEXT_MENU_ACTIONS2 = { "Ctrl+A", "Ctrl+C", "Del" }
local CONTEXT_MENU_ACTIONFUNCS = { _actionEditSelectAll, _actionEditCopy, _actionEditDelete }

local function _editNodeDefScript(def)
    local fn = def._srcfile
    -- FIXME: ideally we'd rather use a user-specified editor
    if not os.opendefault(fn) then
        errprint("Failed to open [" .. tostring(fn) .. "]")
    end
end

function gb:drawDefaultNodeContextMenu(node) -- optional node
    for i = 1, #CONTEXT_MENU_ACTIONS do
        if imgui.MenuItem(CONTEXT_MENU_ACTIONS[i], false, CONTEXT_MENU_ACTIONS2[i]) then
            CONTEXT_MENU_ACTIONFUNCS[i](self, node)
        end
    end
    
    if CONFIG.debugmode then
        if node and imgui.MenuItem("DEBUG: Edit script file [" .. tostring(node._nodedef._srcfile) .. "]") then
            _editNodeDefScript(node._nodedef)
        end
    
        if imgui.MenuItem("DEBUG: Reload scripts for selection") then
            for _, n in pairs(_getSelOrOne(self, node)) do
                n:onScriptReload()
            end
        end
        if imgui.MenuItem("DEBUG: Dump selection (Display serialization)") then
            local s = _serializeSel(self, node, true)
            os.msgbox(s or "nil")
        end
        if imgui.MenuItem("DEBUG: Show object variables") then
            local t = _getSelOrOne(self, node)
            if #t == 1 then
                debugui.watchwindow(t[1], true)
            else
                setmetatable(t, {__mode = "kv" })
                debugui.watchwindow(t, "empty")
            end
        end
    end
end

function gb:drawDefaultBgContextMenu(showAlways, px, py)
    local canpaste = self._canPaste
    if canpaste or showAlways then
        if imgui.MenuItem("Paste", false, "Ctrl+V", canpaste) then
            self:pasteFromClipboard(px, py)
        end
        return true
    end
end

local function _fileMenu(self)
    if imgui.MenuItem("Load", nil, "Ctrl+O") then
        self:loadFile()
    end
    if imgui.MenuItem("Save", nil, "Ctrl+S") then
        self:save()
    end
    if imgui.MenuItem("Save as...", nil, "Ctrl+Shift+S") then
        self:save(true)
    end
end

local function _editMenu(self)
    if not self._editOpen then
        _updateCanPaste(self)
    end
    self:drawDefaultNodeContextMenu()
    self:drawDefaultBgContextMenu(true, self:getViewCenterPos())
end

local function _menuBar(self)
    local nnodes = self:getNumNodes()
    imgui.Menu("File", true, _fileMenu, self)
    self._editOpen = imgui.Menu("Edit", true, _editMenu, self)
    imgui.Menu("Nodes", nnodes > 0, _nodesMenu, self)
    
    local x, y = self:getScrollPos()
    local info = ("(%.0f, %.0f) [%d nodes]"):format(x, y, nnodes)
    local txinfo, tyinfo = imgui.CalcTextSize(info)
    local sx, sy = imgui.GetContentRegionAvail()
    imgui.Dummy(sx-txinfo-20)
    imgui.SameLine()
    imgui.TextDisabled(info)
end

function gb:drawMenuBar()
    imgui.MenuBar(_menuBar, self)
end

function gb:onKeyEvent(keyname, scancode, keymod, down)
    if down then
        if (keymod & 192) ~= 0 then -- LCtrl or RCtrl
            if keyname == "S" then
                self:save((keymod & 3) ~= 0) -- LShift or RShift
            elseif keyname == "O" then
                self:loadFile()
            elseif keyname == "C" then
                _actionEditCopy(self)
            elseif keyname == "A" then
                _actionEditSelectAll(self)
            elseif keyname == "V" then
                self:pasteFromClipboard(self:getViewCenterPos())
            end
        end
    end
end

local function _selectedNodeDef(self, def)
    local ctrldown = sys.getKeyState(224) or sys.getKeyState(228) -- key:LCtrl, key:RCtrl
    if ctrldown then
        _editNodeDefScript(def)
    else
        _setupNode(self, def)
    end
end

-- C++ API function
function gb:onDrawContextMenu(node)
    if node then
        self:drawDefaultNodeContextMenu(node)
        if node.drawContextMenu then
            imgui.Separator()
            node:drawContextMenu()
        end
    else
        if self:drawDefaultBgContextMenu(false, self:getLocalCursorPos()) then
            imgui.Separator()
        end
        
        -- Search
        imgui.TextDisabled("Search (? and * are wildcards) ")
        if self._focusPopup then
            self._focusPopup = false
            imgui.SetKeyboardFocusHere()
        end
        local ch, inp = imgui.InputText("Search", self.searchtext, nil, "s")
        if ch then
            self.searchtext = inp
            _updateSearchList(self._searchList, self._defs, inp)
        end
        imgui.Separator()
        
        local _, kret = sys.getKeyState(40) -- key:Return
        if kret then
            if #self._searchList == 1 then
                local def = self._searchList[1]
                imgui.CloseCurrentPopup()
                _selectedNodeDef(self, def)
                return
            else
                self._focusPopup = true -- Return takes away focus, but we're not done here yet, so set it again
            end
        end
        
        local _, kesc =  sys.getKeyState(41) -- key:Escape
        if kesc then
            imgui.CloseCurrentPopup()
        end
        
        local L = self._searchList
        for i = 1, #L do
            local def = L[i]
            if imgui.MenuItem(def._listname) then
                _selectedNodeDef(self, def)
            end
            if imgui.IsItemHovered() then
                self._overrideInfoNodeDef = def
            end
        end
    end
end

-- C++ API function
function gb:onCloseContextMenu()
    self._overrideInfoNodeDef = nil
end

local dummy = imgui.Dummy
local function _drawDetailSep(node, win)
    node:drawDetailUI(win)
end

function gb:drawDetailUI(win)
    local selnodes = self:getSelectedNodes()
    local N = #selnodes
    if N == 0 then
        if not CONFIG.hidehelp then
            self:drawHelp()
        end
        return
    end
    local flags
    if N == 1 then
        selnodes[1]:drawDetailUI(win)
    else
        for i = 1, #selnodes do
            local n = selnodes[i]
            local vis = imgui.TreeNode(tostring(n), flags, _drawDetailSep, n, win)
            dummy(5)
            imgui.Separator()
            dummy(5)
        end
    end
end

local function _drawDefInfo(def)
    if def.name then
        imgui.TextDisabled("Name: ")
        imgui.SameLine()
        imgui.Text(def.name)
        if def.category then
            imgui.SameLine()
            imgui.TextDisabled(" [" .. def.category .. "]")
        end
    end
    if def.desc then
        imgui.TextDisabled("Description: ")
        imgui.SameLine()
        imgui.Text(def.desc)
    end
    if def.author then
        imgui.TextDisabled("Authors: ")
        imgui.SameLine()
        imgui.Text(def.author)
    end
    local ref = def.references
    if ref then
        if type(ref) == "string" then
            imgui.TextDisabled("Reference: ")
            imgui.SameLine()
            imgui.TextLink(ref, ref)
        else
            imgui.TextDisabled("References:")
            for i = 1, #ref do
                imgui.Text("[" .. i .. "] ")
                imgui.SameLine()
                imgui.TextLink(ref[i], ref[i])
            end
        end
    end
end

function gb:drawInfoUI(win)
    local def = self._overrideInfoNodeDef
    local node
    if not def then
        local selnodes = self:getSelectedNodes()
        if #selnodes == 1 then
            local node = selnodes[1]
            def = node._nodedef
        end
    end
    if def then
        _drawDefInfo(def)
    end
    if node and node.drawInfoUI then
        node:drawInfoUI(win)
    end
end

function gb:getViewCenterPos()
    local pw, ph = self:getPanelSize()
    local sx, sy = self:getScrollPos()
    return -sx + pw/2, -sy + ph/2
end

function gb:centerOnPos(x, y)
    local pw, ph = self:getPanelSize()
    self:setScrollPos(-x + pw/2, -y + ph/2)
end

function gb:centerOnNode(node)
    local x, y = node:getPos()
    local w, h = node:getSize()
    self:centerOnPos(x + w/2, y + h/2)
end

local function _getCenterOfMass(nodes)
    local xs, ys = 0, 0
    local c = #nodes
    if c == 0 then
        return 0, 0
    end
    for i = 1, c do
        local x, y = nodes[i]:getPos()
        xs, ys = xs+x, ys+y
    end
    return xs/c, ys/c
end

function gb:centerOnCenterOfMass()
    self:centerOnPos(_getCenterOfMass(self:getAllNodes()))
end

function gb:adjustPastePosition(nodes, px, py)
    local cx, cy  = _getCenterOfMass(nodes)
    for i = 1, #nodes do
        local node = nodes[i]
        local x, y = node:getPos()
        x = px + (x - cx)
        y = py + (y - cy)
        node:setPos(x, y)
    end
end


function gb:drawHelp()
    ui.help.graphbuilderControls()
end

G.graphbuilder = gb

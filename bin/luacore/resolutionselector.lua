local C = newclass("resolutionselector")
G.resolutionselector = C

local MAXRESLEN = 4 -- It's the year 2017. Resolutions > 9999 are unthinkable.
local INPUTWIDTH = 50

local resmeta = { __tostring = function(t)
    local w, h, e = table.unpack(t)
    local s = ("%dx%d"):format(w, h)
    if e then
        s = s .. " " .. e
    end
    return s
end }
local function RES(...)
    return setmetatable({ fixed = true, ...}, resmeta)
end
local winmeta = { __tostring = function(t)
    return "Window x" .. t.desc
end }
local function WIN(mul, desc)
    return setmetatable({ mul = mul, desc = desc or tostring(mul)}, winmeta)
end

local CUSTOMRES = "Custom Res"

local RESLIST =
{
    RES(0, 0), -- placeholder
    CUSTOMRES,
    WIN(0.25, "1/4"),
    WIN(0.5, "1/2"),
    WIN(1),
    WIN(2),
    WIN(4),
    RES(200, 200),
    RES(320, 240),
    RES(640, 480),
    RES(800, 600),
    RES(1024, 768),
    RES(1280, 800),
    RES(1920, 1080, "(Full HD)"),
    RES(3840, 2160, "(4K)"),
    RES(7680, 4320, "(8K)"),
}

local floor = math.floor
local min = math.min
local max = math.max
local toint = math.tointeger

local function int(x)
    return toint(floor(x))
end

function C.new(callback)
    local self = registerObject(setmetatable({
        _reslistIdx = 5,
        winSizeMult = 1, -- if nil, use fixed size
        _textResX = "",
        _textResY = "",
        resizeCB = assert(callback, "callback missing"),
        _cursizestr = "<error>",
    }, C))
    return self
end

function C:_resize(x,y) -- innternal use only
    x,y = max(1, int(x)), max(1, int(y))
    self._textResX = tostring(x)
    self._textResY = tostring(y)
    self._x, self._y = x, y
    self._cursizestr = ("%dx%d (Keep current)"):format(x,y)
    self:resizeCB(x, y)
end

-- absolute resize, ignores window size -- for external use
function C:resize(x, y)
    assert(x and y and x > 0 and y > 0)
    local found
    for i = 2, #RESLIST do
        local res = RESLIST[i]
        if res[1] == x and res[2] == y then
            self._reslistIdx = i
            found = true
            break
        end
    end
    if not found then
        self._reslistIdx = 1
    end
    self.winSizeMult = nil
    self:_resize(x,y)
end

function C:getWindowSize()
    return self._winW, self._winH
end

-- resize according to window size and multiplier
function C:setWindowSize(w, h)
    self._winW, self._winH = w, h
    local m = self.winSizeMult
    if m then
        self:_resize(w*m, h*m)
    end
end

function C:setWindowSizeMult(m)
    self.winSizeMult = m
    if m and self._winW and self._winH then
        self:setWindowSize(self._winW, self._winH)
    end
end

function C:drawUI()
    local change
    RESLIST[1] = self._cursizestr
    local rsz, idx, elem = imgui.Combo("", self._reslistIdx, RESLIST)
    if idx == 1 then
        rsz = false
    end
    local w, h
    local wmul = elem.mul
    local sx, sy
    if elem == CUSTOMRES then
        local c, s = imgui.ItemWidth(INPUTWIDTH, imgui.InputText, "##i1", self._textResX, MAXRESLEN, "d")
        if c then
            self._textResX = s
            sx = tonumber(s)
            sy = tonumber(self._textResY)
            rsz = true
        end
        imgui.SameLine()
        imgui.Text("x")
        imgui.SameLine()
        c, s = imgui.ItemWidth(INPUTWIDTH, imgui.InputText, "##i2", self._textResY, MAXRESLEN, "d")
        if c then
            self._textResY = s
            sx = tonumber(self._textResX)
            sy = tonumber(s)
            rsz = true
        end
    end
    if wmul then
        local _, s, s2
        local pw = self.parentWindow
        if pw then
            w, h = pw:size()
        else
            w, h = imgui.GetContentRegionAvail()
        end
        local usew, useh = w * wmul, h * wmul
        --[[_, s = imgui.ItemWidth(INPUTWIDTH, imgui.InputText, "##i1", string.format("%d", int(usew)), MAXRESLEN, "dr")
        imgui.SameLine()
        imgui.Text("x")
        imgui.SameLine()
        _, s2 = imgui.ItemWidth(INPUTWIDTH, imgui.InputText, "##i2", string.format("%d", int(useh)), MAXRESLEN, "dr")]]
        s = tostring(usew)
        s2 = tostring(useh)
        if s ~= self._textResX then
            self._textResX = s
            sx = usew
            sy = useh
            change = true
        end
        if s2 ~= self._textResY then
            self._textResY = s2
            sx = usew
            sy = useh
            change = true
        end
    end
    
    if rsz then
        self._reslistIdx = idx
        if elem.fixed then
            sx, sy = table.unpack(elem)
            self._textResX, self._textResY = tostring(sx), tostring(sy)
        end
    end
    if rsz and sx and sy then
        if wmul then
            self:setWindowSizeMult(wmul)
        else
            self:resize(sx, sy)
        end
        change = true
    end
    return change
end

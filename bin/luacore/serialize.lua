-- recursive serializer that ignores userdata and errors on functions/coroutines/other things
-- Lua 5.3 version

local strmatch = string.match
local tins = table.insert
local type = type
local pairs = pairs
local tostring = tostring
local strfmt = string.format
local strrep = string.rep

local isSafeTableKey

do
    local lua_reserved_keywords = {
        'and',    'break',  'do',
        'else',   'elseif', 'end',
        'false',  'for',    'function',
        'if',     'in',     'local',
        'nil',    'not',    'or',
        'repeat', 'return', 'then',
        'true',   'until',  'while',
        'goto'
    }

    local keywords = {}
    for _, w in pairs(lua_reserved_keywords) do
        keywords[w] = true
    end

    isSafeTableKey = function(s)
        return type(s) == "string" and not (s == "" or keywords[s] or strmatch(s, "[^a-zA-Z_]"))
    end
end

local function doindent(tab, n)
    return n and n > 0 and tins(tab, strrep("    ", n))
end

local dump_simple_i

local function dump_simple_table(t, buf, indent)
    tins(buf, "{")
    local idd = indent and (indent+1)
    local usedkeys = {}
    if rawget(t, 1) ~= nil then -- check that this table is really numerically indexed
        for i, val in ipairs(t) do
            usedkeys[i] = true
            dump_simple_i(val, buf, indent)
            tins(buf, ", ")
        end
    end
    
    local big
    for key, val in pairs(t) do
        if not usedkeys[key] then
            local lastlen = #buf
            local revert
            if indent then
                tins(buf, "\n")
                doindent(buf, idd)
            end
            if isSafeTableKey(key) then
                tins(buf, key .. "=")
            else
                tins(buf, "[")
                revert = not dump_simple_i(key, buf)
                tins(buf, "]=")
            end
            if not revert then
                revert = not dump_simple_i(val, buf, idd)
                if not revert then
                    tins(buf, ",")
                    big = true
                end
            end
            if revert then
                for i = lastlen+1, #buf do
                    buf[i] = nil
                end
            end
        end
    end
    if big and indent then
        tins(buf, "\n")
        doindent(buf, indent)
    end
    tins(buf, "}")
    return true
end

local function dump_simple_string(t, buf, indent)
    tins(buf, strfmt("%q", t))
    return true
end

local function dump_simple_value(t, buf, indent)
    tins(buf, tostring(t))
    return true
end

local function dump_ignore(t, buf)
    tins(buf, "nil")
end

local function _serialize_userdata(ud)
    -- can't check for ud.serialize first because the lookup might fail...
    return ud:serialize() -- ... instead, just try and let the caller handle failure
end

local function dump_userdata_safe(t, buf)
    local ok, ret = pcall(_serialize_userdata, t)
    if not ok then
        ret = "nil"
    end
    tins(buf, ret)
end

local function dump_debug(t, buf)
    tins(buf, debugx.formatVariable(t, 8))
end

local dumpfunc = {
    table = dump_simple_table,
    string = dump_simple_string,
    number = dump_simple_value,
    boolean = dump_simple_value,
    userdata = dump_userdata_safe,
}
local function dump_error(t, buf)
    error("serialize: Cannot dump type " .. type(t) .. " (\"" .. tostring(t) .. "\")")
end
local DUMP_FALLBACK = dump_error
setmetatable(dumpfunc, {
    __index = function(t, k)
        return DUMP_FALLBACK
    end
})

dump_simple_i = function(t, buf, indent)
    return dumpfunc[type(t)](t, buf, indent)
end


local function dump_simple(t, indent, unsafe)
    local buf = { "return " }
    dump_simple_i(t, buf, indent and 0)
    return table.concat(buf)
end

local function tofunction(s)
    return load(s, "", "t", false)
end

local function restore(s)
    -- load minimally sandboxed: don't allow any function calls / global lookups / precompiled binary / etc
    return assert(tofunction(s))()
end

local function restore_file(fn)
    -- load minimally sandboxed: don't allow any function calls / global lookups / precompiled binary / etc
    return assert(loadfile(fn, "t", false))()
end

local function serialize_save(tab, indent)
    DUMP_FALLBACK = dump_error
    return tostring(dump_simple(tab, indent))
end

local function serialize_save_debug(tab, indent)
    DUMP_FALLBACK = dump_debug
    return tostring(dump_simple(tab, indent))
end

local function serialize_restore(s)
    local r
    local ok, ret = pcall(restore, s)
    if not ok then
        return nil, ret
    end
    return ret
end

local function serialize_restore_file(s)
    local r
    local ok, ret = pcall(restore_file, s)
    if not ok then
        return nil, ret
    end
    return ret
end

G.serialize = {
    restore = serialize_restore,
    restorefile = serialize_restore_file,
    save = serialize_save,
    save_debug = serialize_save_debug,
    tofunction = tofunction,
}

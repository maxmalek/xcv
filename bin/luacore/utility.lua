local min = math.min
local max = math.max
local asint = math.tointeger
local floor = math.floor
local LUAOBJS = debug.getregistry()[".LObjs"]

local hadError

function G.errorbox(s, lvl)
    lvl = (lvl or 0) + 3 -- want to get info for calling function
    local stack = debugx.formatStack(lvl)
    local loc = debugx.formatLocals(lvl)
    local msg = tostring(s) .. "\n" .. tostring(stack) .. "\n" .. tostring(loc)
    errprint(msg)
    if not hadError then -- Script reload will clear error status and show messages again.
        os.msgbox(msg)
        hadError = true
    end
    return msg
end

function G.errorboxYesNo(s, lvl)
   lvl = (lvl or 0) + 3 -- want to get info for calling function
   local stack = debugx.formatStack(lvl)
   local loc = debugx.formatLocals(lvl)
   local msg = s .. "\n" .. stack .. "\n" .. loc
   errprint(msg)
   return os.msgboxYesNo(msg), msg
end

function G.registerObject(obj)
    if isValidObj(obj) then
        errprint("No need to register objects of class " .. tostring(obj.__name))
        return
    end
    debugprint("Register obj of type " .. obj.__name .. ": " .. tostring(obj))
    assert(type(obj) == "table")
    LUAOBJS[obj] = true
    return obj
end

function G.switchclass(obj, newclass)
    assert(newclass.__name) -- should be set by Lua/C++ bindings
    local base = newclass.__baseclass
    if base then
        assert(base.__name)
        assert(newclass.__name ~= base.__name, "forgot to set custom __name for class")
    end
    
    -- objects derived from scriptable use their metatable as variable storage.
    -- -> patch the metatable to forward to the new class
    if isValidObj(obj) then
        local objvars = getmetatable(obj)
        setmetatable(objvars, newclass)
        objvars.__name = newclass.__name  
    else -- for table-based objects, just change the metatable
        assert(type(obj) == "table")
        setmetatable(obj, newclass)
    end
    return obj
end

function G.newclass(name, base)
    local c = {}
    c.__index = c
    c.__class = c 
    c.__name = name
    c.__iscustomclass = true -- marker for classes that exist purely on the Lua side
    if base then
        c.__baseclass = base
        setmetatable(c, base)
        devprint("New class: " .. name .. " : extend " .. tostring(base.__name))
    else
        devprint("New class: " .. name)
    end
    local reg = debug.getregistry()
    local old = reg[name]
    if old then
        assert(old.__iscustomclass, "Attempt to override internal class?")
    end
    reg[name] = c

    if base and base.new then
        local function new(...)
            local obj = base.new(...)
            -- convert to new class type
            return switchclass(obj, c)
        end
        -- can call _new() in new() to get an object of the correct class, and then do whatever init needs to be done.
        c.new = new 
        c._new = new
    elseif not base then
        local function new(...) -- some default ctor that captures all values passed. Will probably want to replace this
            return registerObject(setmetatable(table.pack(...), c))
        end
        c.new = new
        c._new = new
    end
    
    return c
end

function io.filesfull(path)
    local list = io.files(path)
    if list then
        local n = #path
        local prefix = path
        if path:sub(n, n) ~= "/" then
            prefix = prefix .. "/"
        end
    
        for i = 1, #list do
            list[i] = prefix .. list[i]
        end
    end
    return list
end

-- returns terminated path and file name
function io.splitpath(s)
    local path, fn = s:match"^(.+[/\\])([^/\\]*)$"
    if not path then
        path = nil
        fn = s
    end
    return path, fn
end

local function _incref(self)
    assert(isValidObj(self), "obj not valid")
    self._Refcount = self._Refcount + 1
end

local function _decref(self)
    assert(isValidObj(self), "obj not valid")
    local c = self._Refcount - 1
    if c <= 0 then
        self:delete()
        return
    end
    self._Refcount = c
    return self
end

function G.makeRefCounted(self)
    assert(self.delete, "obj must have delete() method")
    if not self._Refcount then
        self.incref = _incref
        self.decref = _decref
        self._Refcount = 0
    end
    return self
end

-- to be preferred instead of os.savedialog()
function G.askSaveFile(filters)
    local prevpath = CONFIG.lastSavePath
    debugprint("askSaveFile prevpath = [" .. tostring(prevpath) .. "] ...")
    local out = os.savedialog(filters, prevpath)
    if out then
        CONFIG.lastSavePath = io.splitpath(out)
    end
    debugprint("Chose [" .. tostring(out) .. "]")
    return out
end

function G.askOpenFile(filters)
    local prevpath = CONFIG.lastOpenPath
    debugprint("askOpenFile prevpath = [" .. tostring(prevpath) .. "] ...")
    local out = os.opendialog(filters, prevpath)
    if out then
        CONFIG.lastOpenPath = io.splitpath(out)
    end
    debugprint("Chose [" .. tostring(out) .. "]")
    return out
end

-- Modifies shaders before they are loaded.
-- Depending on enabled extensions, some modifiers must be inserted to guarantee smooth operation.
-- E.g. https://www.khronos.org/registry/OpenGL/extensions/ARB/ARB_bindless_texture.txt,
-- "(19) How does ARB_bindless_texture differ from NV_bindless_texture?"

-- convert table of { k = v, ... } to '#define k v'
local function def2str(defs)
    if not defs then
        return ""
    end
    assert(type(defs) == "table", "defs must be table")
    local s = ""
    for k, v in pairs(defs) do
        s = s .. "#define " .. k .. " " .. v .. "\n" -- error propagation on conversion fail is intentional
    end
    return s
end

-- put a HAS_* define for each GL_* extension that is enabled and known to be supported
local function ext2str()
    local ext = gpu.extensions():explode(" ", true)
    for i, s in pairs(ext) do
        ext[i] = "#define HAS_" .. s:match("^[^_]+_(.+)$")
    end
    return table.concat(ext, "\n")
end

-- *.inc files for shader types can supply extra code shared by all shaders
local function readinc(typename)
    local fn = "shaders/" .. typename .. ".inc"
    local fh = io.open(fn, "r")
    if not fh then
        return ""
    end
    local s = fh:read("*a")
    fh:close()
    return s
end

-- *.inc snippet cache
local SNIPS = setmetatable({}, {
    __index = function(t, k)
        local s = readinc(k)
        t[k] = s
        return s
    end
})

-- C++ API (callback for pre-compilation code modification)
function G.onCompileShader(code, typename, defs, ...)
    local defstr = def2str(defs)
    local version, profile, endpos = code:match("^%s*#version[% %\t]+(%d+)[% %\t]*(.-)%\r?%\n()") -- careful to skip whitespace, but not \n
    devprint("onCompileShader() version = [" .. tostring(version) .. "], profile = [" .. tostring(profile) .. "]")
    
    -- skip over any #extension directive -- #extension must come directly after #version
    while true do
        local extstr, extpos = code:match("(#extension%s+.-)%\r?%\n()", endpos)
        if not extpos then
            break
        end
        devprint("Found [" .. extstr .. "] at pos " .. extpos)
        endpos = extpos
    end
    
    local start, rest, prn
    if version then
        start = code:sub(1, endpos - 1)
        rest = code:sub(endpos)
    else
        errprint("Shader #version tag missing! Inserting \"#version 450 core\" and attempting to compile the shader anyway")
        start = "#version 450 core"
        rest = code
        prn = infoprint
    end


    code = start .. "\n" .. ext2str() .. "\n" .. defstr .. "\n" .. SNIPS.all .. "\n" .. SNIPS[typename] .. "\n#line 2\n" .. rest .. "\n" .. SNIPS.all_post
    
    if prn then
        prn(code)
    end
    return code
end

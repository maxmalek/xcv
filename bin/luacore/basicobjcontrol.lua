-- helper class that receives mouse input and controls the rotation of registered objects

local C = newclass("basicobjcontrol")
G.basicobjcontrol = C

function C.new()
    return registerObject(setmetatable({
        _objs = {},
        _rx = 0,
        _ry = 0,
        _rotmat = numvec.mat4_identity:clone(),
        rotspeed = 10.0, -- *10 to speed up things a bit
    }, C))
end

function C:addObj(obj)
    self._objs[obj] = true
end

function C:removeObj(obj)
    self._objs[obj] = nil
end

function C:removeAll()
    table.clear(self._objs)
end

function C:mouseMoveInViewport(rx, ry, buttons, vx, vy)
    local s = self.rotspeed
    return self:mouseMoveRel(rx / vx * s, ry / vy * s, buttons)
end

-- to be overridden if necessary
function C:isEnabled()
    return true
end

function C:mouseMoveRel(x, y, buttons)
    if buttons & 1 ~= 0 and self:isEnabled() then
        self._rx = self._rx - x
        self._ry = self._ry + y
        self:updateRotation()
        return true
    end
end

function C:mouseWheel(change)
    return self:isEnabled()
end

function C:updateRotation()
    local m = self._rotmat
    m:loadmat4i()
    m:mrotate(glsl.vec4, 1.0, 0.0, 0.0, self._ry)
    m:mrotate(glsl.vec4, 0.0, 1.0, 0.0, self._rx)
    
    local pm = m:ptr()
    for obj in pairs(self._objs) do
        obj:setRotationMatrix(pm)
    end
    self:onChange()
end

-- to be overridden
function C:onChange()
end



-------------------------------

-- Same class but for outside-of-any-window control

local Cbg = newclass("basicobjcontrol_bg", C)
G.basicobjcontrol_bg = Cbg
function Cbg:isEnabled()
    return imgui.IsBackgroundHovered()
end

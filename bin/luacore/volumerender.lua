local max = math.max
local sqrt = math.sqrt

local VR = newclass("volumerender", render.renderobject)
render.volumerender = VR

local preshader = assert(gpu.simpleshader.File("shaders/volume_pass1"))
local CULL_FRONT = assert(gpu.CULL_FRONT)
local DEPTH_TEST_ON = assert(gpu.DEPTH_TEST_ON)
local DEPTH_TEST_IGNORE = assert(gpu.DEPTH_TEST_IGNORE)
local DEPTH_WRITE_OFF = assert(gpu.DEPTH_WRITE_OFF)
local DEPTH_WRITE_ON = assert(gpu.DEPTH_WRITE_ON)
local CULL_FRONT = assert(gpu.CULL_FRONT)
local CULL_BACK = assert(gpu.CULL_BACK)
local cube = assert(gpu.meshes.cube)
local BLEND_REPLACE = assert(gpu.BLEND_REPLACE)
local BLEND_NORMAL = assert(gpu.BLEND_NORMAL)

local SER_VARS = { "samplingDist", "stepValue", "intensity", "alphaMult", "deformMult" }



-- framebuffer for the first pass, shared between all invocations
-- blending is disabled when rendering to this framebuffer, so clearing it after a pass is never necessary:
-- The affected pixels will simply be overwritten, and those that are not are also not part of the final render.
local _fb, _fbtex
local function _getFB()
    if not _fb then
        _fb = gpu.framebuffer.new(1024, 1024) -- should be detailed enough to get the back face coords
        _fb:clear()
        _fbtex = _fb:texture()
        VR._FB = _fb
        VR._FBTEX = _fbtex
    end
    return _fb, _fbtex
end

function VR.new(...)
    _getFB()
    local self = VR._new()
    self.shdef = { SD_OVER_OP = "", SD_PLAIN = "" }
    self:init(...)
    return self
end

function VR:init(tex) -- tex optional
    --print("VR init: " .. tostring(self))
    
    self.samplingDist = 0.01
    self.stepValue = 0.1
    self.intensity = 4.0
    self.alphaMult = 50.0
    self.deformMult = 1.0
    self:_getshader()
    
    self:setTex(tex) -- updates pixel scale
end

function VR:setTex(tex)
    self.in_tex = tex
    self:updatePixelScale()
    self:_getshader():setsampler("in_tex", tex)
end

function VR:setColorTex(tex)
    local refresh = not self.in_colorTex ~= not tex
    --print("VR: set color tex: " .. tostring(tex))
    if refresh then
        if tex then
            self.shdef.SD_COLOR = ""
        else
            self.shdef.SD_COLOR = nil
        end
        self:_reloadShader()
    end
    self.in_colorTex = tex
    self:_getshader():setsampler("in_colorTex", tex)
end

function VR:setDeformTex(tex)
    local refresh = not self.in_deformTex ~= not tex
    --print("VR: set deform tex: " .. tostring(tex))
    if refresh then
        if tex then
            self.shdef.SD_DEFORM = ""
        else
            self.shdef.SD_DEFORM = nil
        end
        self:_reloadShader()
    end
    self.in_deformTex = tex
    self:_getshader():setsampler("in_deformTex", tex)
end

function VR:setAlphaTex(tex)
    local refresh = not self.in_alphaTex ~= not tex
    --print("VR: set alpha tex: " .. tostring(tex))
    if refresh then
        if tex then
            self.shdef.SD_ALPHA = ""
        else
            self.shdef.SD_ALPHA = nil
        end
        self:_reloadShader()
    end
    self.in_alphaTex = tex
    self:_getshader():setsampler("in_alphaTex", tex)
end

function VR:getTex()
    return self.in_tex
end

local function _getPixelScaleFactor(tex)
    if not tex then
        return 1, 1, 1
    end
    local w, h, d = glm.normalize(tex:displaysize())
    local m = 5
    return w*m, h*m, d*m
end

function VR:updatePixelScale()
    self:setLocalScale(_getPixelScaleFactor(self.in_tex))
end


function VR:_getshader()
    local sh = self.sh
    if not sh then
        devprint("VR: Recompile shader!")
        for k, v in pairs(self.shdef) do devprint("#define " .. k .. " " .. tostring(v)) end
        local err
        sh, err = gpu.simpleshader.File("shaders/volume_pass2", assert(self.shdef))
        if sh then
            self.sh = sh
            self:_updateShaderUniforms()
        else
            errorbox(err)
        end
        devprint("assign shader " .. tostring(sh))
    end
    return sh
end

function VR:_updateShaderUniforms()
    local sh = self.sh
    sh:setuniform("samplingDist", self.samplingDist)
    sh:setuniform("stepValue", self.stepValue)
    sh:setuniform("intensity", self.intensity)
    sh:setuniform("alphaMult", self.alphaMult)
    sh:setuniform("deformMult", self.deformMult)
    sh:setsampler("in_tex", self.in_tex)
    sh:setsampler("in_colorTex", self.in_colorTex)
    sh:setsampler("in_deformTex", self.in_deformTex)
end

function VR:_reloadShader()
    local oldsh = self.sh
    self.sh = nil
    if not self:_getshader() then -- keep old shader in case new one failed
        errorbox("oops")
        self.sh = oldsh
    end
end

local function SLF(self, k, ...)
    local c, val
    c, val = imgui.Slider(k, self[k], ...)
    self[k] = val
    self.sh:setuniform(k, val)
    return c, val
end

local function SD(self, k)
    local c, on
    local def = self.shdef
    c, on = imgui.Checkbox(k, not not def[k])
    def[k] = (on and "") or nil
    return c, on
end

function VR:drawKnobs()
    local c = SLF(self, "samplingDist", 0.001, 0.1, nil, 1.5)
    c = SLF(self, "stepValue", 0, 1) or c
    c = SLF(self, "intensity", 0, 10) or c
    c = SLF(self, "alphaMult", 0, 100) or c
    c = SLF(self, "deformMult", -1, 1) or c
    local r = SD(self, "SD_OVER_OP")
    r = SD(self, "SD_PLAIN") or r
    r = SD(self, "SD_DERIV") or r
    r = SD(self, "SD_UNCLAMPED") or r
    r = SD(self, "DEBUG_THICKNESS") or r
    r = SD(self, "DEBUG_BOUNDARIES") or r
    if r then
        self:_reloadShader()
    end
    return c or r
end

local function R()
    cube:render("t", CULL_FRONT, DEPTH_TEST_IGNORE, DEPTH_WRITE_OFF)
end

function VR:onRender(projptr)
    preshader:setuniform("proj", projptr)
    preshader:bind()
    if self.in_tex then
        local sh = self:_getshader()
        local fb, fbtex = _getFB()
        fb:captureBlend(BLEND_REPLACE, R) -- first pass: record ray exit positions
        sh:setuniform("proj", projptr)
        sh:setsampler("rayExitTex", fbtex)
        sh:bind()
    end
    cube:render("t", CULL_BACK, DEPTH_TEST_ON, DEPTH_WRITE_OFF)
end

function VR:onDeviceReset(init)
    if init then
        self:_reloadShader()
    else
        self.sh = nil
    end
end

function VR:serialize()
    local t = {}
    for _, k in pairs(SER_VARS) do
        t[k] = self[k]
    end
    t.shdef = self.shdef
    return t
end

function VR:deserialize(t)
    if type(t.shdef) == "table" then
        self.shdef = t.shdef
        self:_reloadShader()
    end
    local sh = self:_getshader()
    for _, k in pairs(SER_VARS) do
        local v = t[k]
        self[k] = v
        sh:setuniform(k, v)
    end
end


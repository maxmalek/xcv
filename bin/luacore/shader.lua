-- Extension functions for gpu.computeshader class

local C = gpu.computeshader
local S = gpu.simpleshader

local rawset = rawset
local rawget= rawget
local unpack = table.unpack

local function _concat(x)
    if type(x) ~= "string" then
        local parts = {}
        while true do
            local s = x()
            if not x then
                break
            end
            table.insert(parts, s)
        end
        x = table.concat(parts)
    end
    return x
end

-- Can unfortunately not be called variadically, since it's used as a metamethod.
-- Means only scalar values can be set with this.
local function _setanyblessed(self, k, v)
    local glty = self._varnames[k]
    if glty then
        if not self:setany(k, v) then
            error("shader variable '" .. k .. "' (" .. glty .. ") does not exist?")
        end
    else -- pass-through Lua table writes
        rawset(self._luavars, k, v)
    end
end
-- Same as above: Can only get scalar values, so effectively only the first element of a vec4 is returned, etc
local function _getanyblessed(self, k, LV)
    -- Warning: This function MUST NOT index self in any way. Doing so will cause infinite __index loop and end up with a C stack overflow error being thrown
    -- (Instead, we use the previously captured Lua varaibles table LV to index, that's safe)
    -- Check members and methods first, to ensure a uniform in the shader can't shadow a class method!
    local val = LV[k] -- <-- this is likely to __index into class table
    if val then
        return val
    end
    -- No member present, NOW we can go and check the uniforms
    return LV.getany(self, k)
end

local function _retbless(self, ok, ...)
    if ok then
        self:bless()
    end
    return ok, ...
end

local function _loadstringbless(self, ...)
    return _retbless(self, self:_loadstring(...))
end

local function _loadfilebless(self, ...)
    return _retbless(self, self:_loadfile(...))
end

-- after calling this on a shader object, simple uniforms and buffers can be set and read directly,
-- like 'uniform float scale;' in the shader can be set via 'self.scale = 42' and read via 'x = self.scale'  in Lua
local function bless(self)
    local LV = getmetatable(self) -- contains all Lua variables
    local V = self:getvars()
    local vn = {}
    for i = 1, #V do
        local v = V[i]
        local n = v[1]
        if self[n] ~= nil then
            error("variable collision: '" .. n .. "' is already defined in Lua")
        end
        vn[n] = glsl.nameof(v[2]) or "<missing type name>" -- record type
    end
    self._varnames = vn
    
    -- patch functions that change the shader to re-bless itself after loading
    if not self._loadfile then
        self._loadfile = self.loadfile
    end
    if not self._loadstring then
        self._loadstring = self.loadstring
    end
    self.loadstring = _loadstringbless
    self.loadfile = _loadfilebless
    self._luavars = LV
    
    -- capture reads & writes
    rawset(LV, "__index", function(self_, k) return _getanyblessed(self_, k, LV) end) -- must capture LV table else this would go into infite __index calls trying to look up self._luavars
    rawset(LV, "__newindex", _setanyblessed)
    return self
end

-- return ok, err
local function _loadTemplate(sh, template, defs, vars, level, ...)
    local code, err = shadertemplate.generate(template, vars, level and (level+2), ...)
    if not code then
        return false, err
    end
    return sh:loadstring(code, defs)
end

local function _serializeUniforms(sh)
    local t = {}
    for _, v in ipairs(sh:getvars()) do
        local name, glsltype, ty = unpack(v)
        if ty == "uniform" then
            t[name] = { type = glsl.nameof(glsltype), sh:getuniform(name) }
        end
    end
    return t
end

local function _deserializeUniforms(sh, t)
    for name, val in pairs(t) do
        sh:setuniform(name, unpack(val))
    end
end

---------------------------------------------

C.bless = bless
C.loadTemplate = _loadTemplate
C.serializeUniforms = _serializeUniforms
C.deserializeUniforms = _deserializeUniforms

-- computeshader:load() with Lua's load() semantics:
-- takes a string or function.
-- if it is not a string, create a string by calling the passed object/function repeatedly until nil is returned,
-- then concatenate the results to the source code string.
-- Given a string, load that directly.
-- Returns bool success, and optionally a string with warnings or errors.
function C:load(x, ...)
    return self:loadstring(_concat(x), ...)
end

-- same parameters as :load().
-- Retuns the newly created shader on success.
-- Throws error on failure.
function C.Create(...)
    local sh = C.new()
    assert(sh:load(...))
    return sh
end

-- Loads a compute shader from a file. Throws on failure.
-- ".comp" is automatically appended to the name passed in.
-- computeshader.File(name, [defs, entrypoint])
function C.File(...)
    local sh = C.new()
    assert(sh:loadfile(...))
    return sh
end

function C.CreateFromTemplate(...)
    local sh = C.new()
    assert(sh:loadTemplate(...))
    return sh
end

----------------------------------------------

S.bless = bless
S.serializeUniforms = _serializeUniforms

function S:load(vert, frag, ...)
    return self:loadstring(_concat(vert), _concat(frag), ...)
end

-- same parameters as :load().
-- Retuns the newly created shader on success.
-- Throws error on failure.
function S.Create(...)
    local sh = S.new()
    assert(sh:load(...))
    return sh
end

-- Loads a compute shader from a file. Throws on failure.
-- ".vert" and ".frag" are automatically appended to the name passed in.
-- simpleshader.File(name, [defs, entrypoint])
function S.File(...)
    local sh = S.new()
    assert(sh:loadfile(...))
    return sh
end

function S.CreateFromTemplate(...)
    local sh = S.new()
    assert(sh:loadTemplate(...))
    return sh
end


function S:loadTemplate(template1, template2, defs, vars, level, ...)
    local ok, err = _loadTemplate(self, template1, defs, vars, level, ...)
    if not ok then
        return false, err
    end
    return _loadTemplate(self, template2, defs, vars, level, ...)
end

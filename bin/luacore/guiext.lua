

function gui.memoryeditor:drawBody()
    if self:drawUI() then
        imgui.Separator()
        self:drawControls()
    end
end

function gui.memoryeditor:drawDefault()
    imgui.MenuBar(imgui.Menu, "Layout", true, self.drawMenu, self)
    self:drawBody()
end
function gui.memoryeditor:set(x, bytes)
    if not x then
        self:unsafeptr(nil, 0)
        self._lock = nil
    elseif x.rawsize then
        local rp, wp
        if x.ptr then
            wp = x:ptr()
            rp = wp
        else
            if x.readptr then
                rp = x:readptr()
            end
            if x.writeptr then
                wp = x:writeptr()
            end
        end
        self:unsafeptr(rp, x:rawsize(), wp)
        self._lock = x -- keep a reference to the passed object; else the GC might collect it while it's ptr() is still in use
    else
        error("Object (" .. tostring(x.__name) .. ") must provide ptr() and rawsize() methods")
    end
end


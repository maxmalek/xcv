-- Simple GLSL templating engine; expands placeholders in a string
-- $X is replaced as variable
-- ${X} is replaced as Lua statement ("return " .. X)
-- interprets lines starting with ! as lua statements, running in their own sandbox

local tins = table.insert

-- returns writable table that looks up in vars tab, visible locals and upvalues, and globals, in that order
local function makelut(vars, level)
    local env = level and debugx.getvars(level + 2)
    local tt = {}
    if vars then tins(tt, vars) end
    if env  then tins(tt, env)  end
    tins(tt, _G)
    local N = #tt
    local function f(_, k)
        for i = 1, N do
            local v = tt[i][k]
            if v then
                return v
            end
        end
        error("Undefined variable: " .. tostring(k))
    end
    return setmetatable({}, { __index = f } )
end
local function makefunc(expr, env)
    local f = load("return " .. expr, "", "t", env)
    if not f then
        error("Error compiling epxression:\n" .. expr)
    end
    return f
end
local function eval(s, env)
    return makefunc(s, env)()
end
local function fexpand(s, env)
    return (s:gsub('$(%b{})', function(x)
        return eval(x:sub(2, -2), env)
    end))
end
local function interp(s, env)
    return fexpand(s:expand(env), env)
end
local function makeluacode(bang, x)
    local esc = string.format("%q", x)
    if bang and bang ~= "" then
        return x .. "; _EMITLUA(" .. esc .. ")"
    else
        return "_EMIT(" .. esc .. ")"
    end
end
local function tolua(code)
    -- caller passes both functions. Wrap body into a function so that the shader template can use ... as expected
    local t = { "local _EMIT, _EMITLUA = ...; return (function(...)" }
    for s in code:gmatch"(.-)%\n" do
        s = s:gsub("^(!?)(.*)$", makeluacode)
        tins(t, s)
    end
    tins(t, "end)(select(3, ...))")
    return table.concat(t, "\n")
end

local function buildcoderet(lines, ...)
    local s = table.concat(lines, "\n")
    devprint("--[ Generated shader: ]--\n" .. s)
    return s, ...
end
local function buildcode(s, vars, level, ...)
    s = tolua(s)
    devprint(s)
    local env = makelut(vars, level)
    local luaf, err = load(s, "", "t", env)
    if not luaf then
        return false, err
    end
    local lines = {}
    local function _EMIT(x)
        tins(lines, interp(x, env))
    end
    local function _EMITLUA(x)
        tins(lines, "//[Lua] " .. x) -- keep the Lua code intact so that the reported line numbers stay correct
    end
    return buildcoderet(lines, luaf(_EMIT, _EMITLUA, ...))
end


------------------ TEST CODE ---------------

--[=[
G = setmetatable({}, { __index = function(t, k) local v = _G[k]; return assert(v, k) end, __newindex = _G })
dofile"debugx.lua"
dofile"string.lua"

local TEST = 42;

local code = [[
!N = in_Tex:dimensions()
!FMT = in_Tex:formatstr()
#define ivecN ivec$N
#define TEST $TEST
#define FMT2 ${in_Tex:formatstr()}
uniform sampler${N}D in_Tex;
layout($FMT) writeonly restrict uniform image${N}D outTex;

void main()
{
    ivec3 sz = imageSize(outTex);
    for(uvec3 i = IDX; all(lessThan(i, uvec3(sz))); i += INC)
    {
        vec4 p = rsmooth(ivec3(i), sz);
        imageStore(outTex, ivec3(i), p);
    }
}
]]
local V = { in_Tex = {
    dimensions = function() return 3 end,
    formatstr = function() return "rgba32f" end,
}}

print(buildcode(code, V, 1, "blarb"))
]=]


G.shadertemplate =
{
    generate = buildcode,
}

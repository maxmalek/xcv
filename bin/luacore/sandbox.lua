local rawset = rawset

local sandbox = {}


local function emptyEnv()
    local g = {}
    g._G = g
    return g
end
sandbox.emptyEnv = emptyEnv

local function _where()
    local di = debug.getinfo(3, "nSl")
    local where = di.short_src or "?"
    if di.currentline then
        where = where .. ":" .. di.currentline
    end
    return where
end

local function writeenv(env, k, v)
    errprint(("[%s] Warning: Write global '%s' = %s"):format(_where(), tostring(k), tostring(v)))
    rawset(env, k, v)
end

-- redirects global writes to the sandbox.
-- does not prevent sub-table writes, such as math.pi = nil
local function shadowEnv(g)
    g = g or _ENV
    local env = {}
    env._G = env
    
    --[[local function readenv(env, k)
        local v = g[k]
        if v == nil then
            errprint(("[%s] Warning: Read non-existing global '%s'"):format(_where(), tostring(k)))
        end
        return v
    end]]

    local meta = { __index = g, __newindex = writeenv, __metatable = false }
    setmetatable(env, meta)
    return env
end
sandbox.shadowEnv = shadowEnv

-- Lua 5.2+
function sandbox.load(s, env)
    return load(s, "", "t", env or shadowEnv())
end

function sandbox.loadfile(fn, env)
    return loadfile(fn, "t", env or shadowEnv())
end

function sandbox.dofile(fn, env)
    return assert(sandbox.loadfile(fn, env or shadowEnv()))()
end


G.sandbox = sandbox

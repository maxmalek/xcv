local M = require "bsplinecore"

------ GLSL CODE GENERATOR ------

-- This generates GLSL code for BSpline calculation on the GPU.

local function buildInfoLine(e)
    return "{" .. e.numControlPoints .. ","
               .. e.numSplinePoints  .. ","
               .. e.knotStartIdx .. ","
               .. e.basisStartIdx .. ","
               .. e.pointDimSkip .. ","
               .. e.outputDimAccu .. "}"
end

local function genSharedHeader(DEG, NDIM, info)
    return ([[
#version 430 core
layout(local_size_x = 1024, local_size_y = 1, local_size_z = 1) in;
const uint DEG = $DEG;
const uint NDIM = $NDIM;
const uint K = DEG + 1;
struct NonzeroBasis {
  uint begin;
  float basis[K];
};
struct DimInfo {
  uint numControlPoints;
  uint numSplinePoints;
  uint knotStartIdx;
  uint basisStartIdx;
  uint pointDimSkip;
  uint outputDimAccu;
};
const DimInfo info[NDIM] = {
    ]]):expand()
    .. table.concat(fun.map(info, buildInfoLine), ",\n")
    .. [[ };
uint numKnots(uint dim) {
  return info[dim].numControlPoints + K;
}
int array2ivec(uint a[1]) { return int(a[0]); }
ivec2 array2ivec(uint a[2]) { return ivec2(int(a[0]), int(a[1])); }
ivec3 array2ivec(uint a[3]) { return ivec3(int(a[0]), int(a[1]), int(a[2])); }
ivec4 array2ivec(uint a[4]) { return ivec4(int(a[0]), int(a[1]), int(a[2]), int(a[3])); }
vec4 vec4zeropad(float f) { return vec4(f, 0.0, 0.0, 0.0); }
vec4 vec4zeropad(vec2 v) { return vec4(v.xy, 0.0, 0.0); }
vec4 vec4zeropad(vec3 v) { return vec4(v.xyz, 0.0); }
vec4 vec4zeropad(vec4 v) { return v; }
]]
end

local HEADER_BASIS_GEN = [[
#ifdef FROM_BUFFER
layout(std430, binding = 0) restrict readonly buffer TArray {
  float data[]; // size: Product(numSplinePoints) for each dimension
} tArray;
#endif
layout(std430) restrict readonly buffer KnotArray {
  float data[];
} knots;
layout(std430) restrict writeonly buffer BasisArray {
  NonzeroBasis data[];
} basisArray;
]]

local HEADER_POINT_EVAL = [[
layout(std430) restrict readonly buffer BasisArray {
  NonzeroBasis data[];
} basisArray;
layout(std430) restrict readonly buffer InputPoints {
  VECTYPE data[];
} inputPoints;
void idx2coord(uint idx, out uint coords[NDIM])
{
  for(uint i = 0; i < NDIM; ++i)
  {
    uint o = NDIM-1-i;
    uint a = info[o].outputDimAccu;
    uint t = idx / a;
    coords[o] = t;
    idx -= (t * a);
  }
}
]]

local CODE_FIND_KNOT_INDEX = [[
uint findKnotIndex(uint offset, uint nKnots, float t)
{
  if(t > 0.0)
  {
    uint c = nKnots;
    uint good = 0;
    while(c != 0)
    {
      uint step = c / 2;
      uint i = good + step;
      if(knots.data[offset+i] < t)
      {
        good = i + 1;
        c -= step + 1;
      }
      else
        c = step;
    }
    return good - 1;
  }
  return DEG;
}
]]

local function KNOTI(offs)
    return ("knots.data[idx%+d]"):format(offs)
end

local function genCalcBasis(DEG)
    local K = DEG + 1
    local o0 = KNOTI(0)
    local o1 = KNOTI(1)
    local ss = ([[
void calcBasis(out float basis_out[$K], float t, uint idx)
{
  float k1 = $o1;
  float k2 = k1 - t;
  float k3 = t - $o0;
]]):expand()
    for i = 0, DEG-1 do
        ss = ss .. "  float basis" .. i .. ";\n"
    end
    ss = ss .. "  float basis" .. DEG .. " = 1.0;\n"
    for d = 1, K-1 do
        local a, b, c = KNOTI(0-d+1), (K-d-1), (K-d)
        ss = ss .. ([[  {
            float x = k1 - $a;
            float m = k2 / x;
            basis$b = m * basis$c;
        ]]):expand()

        for j = 1, d-1 do
            local ci = DEG - d + j
            local io = 0 - d + j
            local aa, bb, cc = KNOTI(io+d+1), KNOTI(io+1), KNOTI(io)
            local ci1 = ci + 1
            ss = ss .. ([[
                {float k1 = $aa;
                float y = k1 - $bb;
                float a = (t - $cc) / x;
                float b = (k1 - t) / y;
                x = y;
                basis$ci = a * basis$ci + b * basis$ci1;}
            ]]):expand()
        end
        ss = ss .. "    basis" .. DEG .. " *= (k3 / x);\n"
                .. "  }\n";
    end

    for i = 0, K-1 do
        ss = ss .. ("  basis_out[%d] = basis%d;\n"):format(i, i)
    end
    
    ss = ss .. "}\n\n"
    return ss
end


local CODE_GENERATE_BASIS_ARRAY = [[
void generateBasisArray(uint dim)
{
  uint i, n = info[dim].numSplinePoints;
  uint knotstart = info[dim].knotStartIdx;
  uint basisstart = info[dim].basisStartIdx;
  uint nKnots = numKnots(dim);
  float tmul = 1.0 / float(int(n-1));
  AUTOTILE(i, n)
  {
  #ifdef FROM_BUFFER
      float t = clamp(tArray.data[i], 0.0, 1.0);
  #else
      float t = float(int(i)) * tmul;
  #endif
    uint idx = findKnotIndex(knotstart, nKnots, t);
    basisArray.data[basisstart+i].begin = idx - DEG;
    calcBasis(basisArray.data[basisstart+i].basis, t, idx + knotstart);
  }
}
]]


local CODE_CALC_POINT_FROM_BASIS_1D = [=[
#ifndef VECTYPE
#error Must define VECTYPE
#endif
VECTYPE calcPoint_0(uint pstart, uint b[NDIM])
{
  NonzeroBasis basis = basisArray.data[b[0]];
  uint pos = pstart + basis.begin;
  VECTYPE p = VECTYPE(0.0);
  for(uint i = 0; i < K; ++i)
    p += inputPoints.data[pos+i] * basis.basis[i];
  return p;
}
]=]

local function calcPointRec(D)
    if D == 0 then
        return ""
    end
    local D1 = D - 1

    return calcPointRec(D1) .. ([=[
    
VECTYPE calcPoint_$D(uint pstart, uint b[NDIM])
{
  const uint D = $D;
  const uint dskip = info[D].pointDimSkip;
  const NonzeroBasis basis = basisArray.data[info[D].basisStartIdx + b[D]];
  uint skipAccu = dskip * basis.begin;
  VECTYPE p = VECTYPE(0.0);
  for(uint i = 0; i < K; ++i, skipAccu += dskip)
    p += basis.basis[i] * calcPoint_$D1(pstart + skipAccu, b);
  return p;
}
]=]):expand()
end

local function calcPoint(NDIM)
    local D = NDIM - 1
    return calcPointRec(NDIM - 1) .. ([[
    
VECTYPE calcPoint(uint b[NDIM])
{
  return calcPoint_$D(0, b);
}
]]):expand()
end

local CODE_MAIN_GENERATE_BASIS = [[
void main()
{
  for(uint dim = 0; dim < NDIM; ++dim)
    generateBasisArray(dim);
}
]]


local function main_calcPoints(NDIM)

    local ss = ([[
#if !(defined(TO_BUFFER) || defined(TO_TEXTURE))
#error #define TO_BUFFER, TO_TEXTURE, or both
#endif
#ifdef TO_BUFFER
layout(std430) restrict writeonly buffer OutputPoints {
  VECTYPE data[];
} outputPoints;
#endif

#ifdef TO_TEXTURE
restrict writeonly uniform image${NDIM}D outputTex;
#endif

const uint NP = info[0].numSplinePoints]]):expand()

    for i = 1, NDIM-1 do
        ss = ss .. " * info[" .. i .. "].numSplinePoints"
    end
    
    return ss .. [[;
        
void main()
{
    uint i, coord[NDIM];
    AUTOTILE(i, NP)
    {
        idx2coord(i, coord);
        VECTYPE p = calcPoint(coord);
        #ifdef TO_BUFFER
            outputPoints.data[i] = p;
        #endif
        #ifdef TO_TEXTURE
            imageStore(outputTex, array2ivec(coord), vec4zeropad(p));
        #endif
    }
}
]]
end

local function allocDimInfo(DEG, NDIM, pNumControlPoints, pNumSplinePoints)
    local info = {}
    local basisStartIdxAccu = 0
    local knotStartIdxAccu = 0
    local pointDimSkipAccu = 1
    local outputDimAccu = 1
    for i = 1, NDIM do
        local e = {}
        info[i] = e
        local ncp = pNumControlPoints[i]
        local nsp = pNumSplinePoints[i]
        local nKnots = ncp + DEG + 1
        e.numControlPoints = ncp
        e.numSplinePoints = nsp
        e.knotStartIdx = knotStartIdxAccu
        e.basisStartIdx = basisStartIdxAccu
        e.pointDimSkip = pointDimSkipAccu
        e.outputDimAccu = outputDimAccu

        basisStartIdxAccu = basisStartIdxAccu + nsp
        outputDimAccu = outputDimAccu * nsp
        knotStartIdxAccu = knotStartIdxAccu + nKnots
        pointDimSkipAccu = pointDimSkipAccu * ncp
    end
    return info
end

local function checkNP(cp, sp)
    local NDIM = #cp
    assert(NDIM == #sp, "Dimensionality of control points and spline points must be equal")
    return NDIM
end


local function generateBasisGLSL(DEG, pNumControlPoints, pNumSplinePoints)
    local NDIM = checkNP(pNumControlPoints, pNumSplinePoints)
    local info = allocDimInfo(DEG, NDIM, pNumControlPoints, pNumSplinePoints)
    return table.concat({
        genSharedHeader(DEG, NDIM, info),
        HEADER_BASIS_GEN,
        CODE_FIND_KNOT_INDEX,
        genCalcBasis(DEG),
        CODE_GENERATE_BASIS_ARRAY,
        CODE_MAIN_GENERATE_BASIS,
    }, "\n")
end
M.generateBasisGLSL = generateBasisGLSL

local function generateEvalGLSL(DEG, pNumControlPoints, pNumSplinePoints)
    local NDIM = checkNP(pNumControlPoints, pNumSplinePoints)
    local info = allocDimInfo(DEG, NDIM, pNumControlPoints, pNumSplinePoints)
    return table.concat({
        genSharedHeader(DEG, NDIM, info),
        HEADER_POINT_EVAL,
        CODE_CALC_POINT_FROM_BASIS_1D,
        calcPoint(NDIM),
        main_calcPoints(NDIM),
    }, "\n")
end
M.generateEvalGLSL = generateEvalGLSL

-- When the shader is generated:
-- [mandatory] #define VECTYPE (float, vec2, or vec4. Do NOT use vec3!)
-- [optional]  #define FROM_BUFFER If coords are to be pulled from a buffer instead of generated on the fly
-- [optional]  #define TO_BUFFER to write results to buffer
-- [optional]  #define TO_TEXTURE to write results to a textureND, where N == NDIM

local GOOD_TYPES = { float = true, vec2 = true, vec4 = true }
-- why not vec3? Because the memory layout is the same as vec4, and that causes a hell lot of trouble

local function makeShaders(DEG, ncp, nsp, glsltype, toBuf, toTex, useCustomT)
    local typestr
    if type(glsltype) == "string" then
        typestr = glsltype
    else
        typestr = glsl.nameof(glsltype)
    end
    if not GOOD_TYPES[typestr] then
        error("Unsupported type: " .. tostring(typestr))
    end
    
    local srcBasis = generateBasisGLSL(DEG, ncp, nsp)
    local srcEval = generateEvalGLSL(DEG, ncp, nsp)
    local def = { VECTYPE = typestr }
    if toBuf then def.TO_BUFFER = "" end
    if toTex then def.TO_TEXTURE = "" end
    if useCustomT then def.FROM_BUFFER = "" end
    
    local shBasis = gpu.computeshader.Create(srcBasis, def)
    local shEval = gpu.computeshader.Create(srcEval, def)
   
   return shBasis, shEval
end
M.makeShaders = makeShaders

return M

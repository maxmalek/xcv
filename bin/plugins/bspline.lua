local codegen = require "bspline/codegen"
local core = require "bsplinecore" -- This is the dynamic library

local unpack = table.unpack

local function setupInstance(deg, ctrlp, splinep, mode, glsltype, toBuf, toTex, useCustomT)
    if type(ctrlp) == "number" then ctrlp = {ctrlp} end
    if type(splinep) == "number" then splinep = {splinep} end
    
    local sh1, sh2 = codegen.makeShaders(deg, ctrlp, splinep, glsltype, toBuf, toTex, useCustomT)
    
    local ndim = #ctrlp
    local cp, sp, bas = 1, 1, 0
    for i = 1, ndim do
        cp = cp * ctrlp[i]
        sp = sp * splinep[i]
        bas = bas + splinep[i]
    end
    
    -- sizes in bytes
    local floatsz = glsl.sizeof(glsl.float)
    local tysz = glsl.sizeof(glsltype)
    
    local cpsz = cp * tysz -- control point buffer
    local spsz = sp * tysz -- spline point buffer
    local basz = bas * (glsl.sizeof(glsl.unsigned) + (deg+1) * floatsz) -- struct NonzeroBasis, see codegen.lua
    local usz = bas * floatsz
    
    -- buffers with above sizes
    local uknots = core.createKnots(deg, mode, unpack(splinep))
    local bknots = gpu.gpubuf.new(uknots, "rp")
    
    
    return { shPre = sh1, shEval = sh2, bknots = bknots }
end


local B = {
    codegen = codegen,
    core = core,
    setupInstance = setupInstance,
}



return B

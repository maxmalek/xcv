-- This file hardens the global Lua enviroment against typos and variable naming errors.
-- Attempting to read or write undefined globals causes an error.
-- If you really want to set a global, use G.varname = value.

local rawget = rawget
local rawset = rawset
local tostring = tostring
local error = error


setmetatable(_G, {
    __index = function(_, k)
        error("Attempt to read undefined global '" .. tostring(k) .. "'", 2)
    end,
    __newindex = function(_, k, v)
        error("Attempt to set global " .. type(v) .. " '" .. tostring(k) .. "'", 2)
    end,
})

-- _G emulation without warnings
local G = setmetatable({}, {
    __index = function(_, k)
        return rawget(_G, k)
    end,
    __newindex = function(_, k, v)
        rawset(_G, k, v)
    end,
})
G.G = G

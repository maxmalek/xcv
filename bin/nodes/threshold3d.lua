local code = [[
#version 430

layout(local_size_x = 8, local_size_y = 8, local_size_z = 8) in;

uniform sampler3D in_Tex;

writeonly restrict uniform image3D out_Tex;
uniform float u_th = 1.0;
uniform bool u_ignoreAlpha = true;

void main()
{
   uvec3 i, sz = imageSize(out_Tex);
   AUTOTILE(i, sz)
   {
        vec4 a = texelFetch(in_Tex, ivec3(i), 0);
        vec4 v = step(vec4(u_th), a);
        if(u_ignoreAlpha)
            v.a = a.a;
        imageStore(out_Tex, ivec3(i), v);
   }
};
]]

return { 
    category = "compute",
    name = "3D Threshold",
    desc = "Below the threshold becomes 0.0, otherwise 1.0",
    src = code,
}

local min = math.min
local max = math.max
local asint = math.tointeger
local floor = math.floor


local W = newclass("innerwindow_show2d", assert(gui.imwindow))

function W.new(node)
    local sysw = assert(sys.syswindow.GetCurrent())
    local self = sysw:newInner("<Empty image>", tostring(node), 320, 240)
    switchclass(self, W)
    
    self.node = node

    return self
end

function W:drawUI()
    local w, h = imgui.GetContentRegionAvail()
    local tex = self.node.tex
    imgui.ImageKeepAspect(tex, w, h,   nil, nil, nil, nil,    1,1,1,1)
    tex:drawSaveAsButton()
end

--------------------------------

local P = newclass("show2d_popout_window", basicwindow)

function P:onDrawUI()
    imgui.ImageKeepAspectFit(self.tex)
end

function P:onClose()
    if self.node then
        self.node.syswindows[self] = nil
    end
end

local function popout(node, ...)
    local w = P:Open(...) -- w, h, [full, displayID]
    w.node = node
    w.tex = node.tex
    node.syswindows[w] = true
    return w
end

local function drawback(win, img)
    imgui.ImageKeepAspectFit(img)
end

local function showinback(node, add)
    local win = sys.syswindow.GetCurrent()
    local back = win.backpanel
    if add then
        back:add(node.q)
    else
        back:remove(node.q)
    end
end

local function updateback(node)
    if node.inback then
        local win = sys.syswindow.GetCurrent()
        local back = win.backpanel
        back.dirty = true
    end
end

--------------------------------

local function updatetitle(self)
    if self.win then
        local tex = self.tex
        if tex then
            local fn = tex.filename
            self.win:setTitle(fn or tostring(tex))
        else
            self.win:setTitle("<No input>")
        end
    end
end

local function init(self)
    self.syswindows = self.syswindows or {}
    self.q = self.q or render.texquad.new(nil, false, true)
end

local function destroy(self)
    if self.win then
        self.win:destroy()
        self.win = nil
    end
    for w in pairs(self.syswindows) do
        w.node = nil -- keep window open + keep displaying whatever it's currently displaying
    end
end

local function recalc(self, inputs)
    local tex = inputs.in2D
    self.tex = tex
    self.q.tex = tex
    --print(self.tex)
    updatetitle(self)
    
    updateback(self)
    for w in pairs(self.syswindows) do
        w.tex = tex
    end
    
    return tex
end

local function _viewMenu(self)
    local winvis = self.win and self.win:visible()
    if imgui.MenuItem("Extra window", winvis) then
        if not winvis and not self.win then
            self.win = assert(W.new(self))
            updatetitle(self)
        end
        self.win:setVisible(not winvis)
    end
    if imgui.MenuItem("Pop out") then
        popout(self, 640, 480)
    end
    if imgui.MenuItem("Background", self.inback) then
        self.inback = not self.inback
        showinback(self, self.inback)
    end
end

local function _menuBar(self)
    imgui.Menu("View", true, _viewMenu, self)
end

local function drawDetailUI(self)
    imgui.MenuBar(_menuBar, self)
    return self:drawDetailUIDefault()
end

local function drawUI(self)
    imgui.ImageKeepAspect(self.tex, 120, 70,    nil,nil, nil,nil,     1,1,1,1)
end

return {
    name = "Show 2D image",
    desc = "Display a 2D texture/image",
    category = "output",
    recalc = recalc,
    drawUI = drawUI,
    drawDetailUI = drawDetailUI,
    init = init,
    destroy = destroy,
    makeOutput = false,
    
    extraInputs = {
        in2D = glsl.sampler2D,
    },
    extraOutputs = {
        out2D = glsl.sampler2D,
    },
}

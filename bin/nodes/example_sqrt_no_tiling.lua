local code = [[#version 430
layout(local_size_x = 32, local_size_y = 32) in;
uniform sampler2D in_Tex;
writeonly restrict uniform image2D out_Tex;
void main()
{
    uvec2 i, sz = imageSize(out_Tex);
    mainWorkLoop(i, sz)
    {
        vec4 v = texelFetch(in_Tex, ivec2(i), 0);
        imageStore(out_Tex, ivec2(i), sqrt(v));
    }
};
]]
return { 
    category = "example",
    name = "2D square root",
    desc = "Minimal example. sqrt() for every pixel (all channels) -- No automatic tiling",
    src = code,
}

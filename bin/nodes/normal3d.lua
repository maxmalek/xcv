local code = [[#version 430

layout(local_size_x = 16, local_size_y = 8, local_size_z = 8) in; // 16*8*8 == 1024 (don't go higher)

uniform sampler3D in_Tex;
writeonly restrict uniform image3D out_Tex;

const int NORMAL_BOX_SIZE = 2;

float plain(in vec3 tc)
{
    return texture(in_Tex, tc).x;
}

vec4 normalDir(in vec3 tc, in vec3 offs)
{
    float c = plain(tc);
    vec3 n = vec3(0.0);
    for(int z = -NORMAL_BOX_SIZE; z <= NORMAL_BOX_SIZE; ++z)
    for(int y = -NORMAL_BOX_SIZE; y <= NORMAL_BOX_SIZE; ++y)
    for(int x = -NORMAL_BOX_SIZE; x <= NORMAL_BOX_SIZE; ++x)
    {
        vec3 d = vec3(x,y,z);
        if(length(d) > 0.0)
        {
            vec3 p = tc + offs * d;
            float v = plain(p);
            n += (v-c) * normalize(d);
        }
    }
    float L = length(n);
    return vec4(L > 0.0 ? normalize(n) : vec3(0.0), L);
}

void main()
{
   ivec3 td = imageSize(out_Tex);
   uvec3 i, sz = uvec3(td);
   vec3 tm = 1.0 / vec3(td);
   AUTOTILE(i, sz)
   {
       vec3 tc = (vec3(i) + 0.5) * tm;
       vec4 c = normalDir(tc, tm);
       imageStore(out_Tex, ivec3(i), c);
   }
};
]]

return {
    category = "compute",
    name = "3D Normal",
    desc = "Calculates normal vectors for a 3D volume",
    src = code,
    outGen = {
        out_Tex = { input = "in_Tex", format = "rgba16f", swizzle = "rgba" }
    },
    maxsize = 256,
}

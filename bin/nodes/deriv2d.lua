local code = [[
#version 430

layout(local_size_x = 32, local_size_y = 32) in; // 32*32 == 1024 (don't go higher)

uniform sampler2D in_Tex;
uniform bool u_keepAlpha = true;
// FIXME: make possible to adjust type to match input
writeonly restrict uniform image2D out_Tex;

uniform float u_mult = 1.0;

void main()
{
    uvec2 i, sz = imageSize(out_Tex);
    AUTOTILE(i, sz)
    {
        vec4 a = texelFetch(in_Tex, ivec2(i), 0);
        vec4 b = texelFetch(in_Tex, ivec2(i+1), 0);
        vec4 diff = abs(a - b) * u_mult;
        if(u_keepAlpha)
            diff.a = a.a;
        imageStore(out_Tex, ivec2(i), diff);
    }
};
]]


return { 
    category = "compute",
    name = "2D edge detect (simple)",
    desc = "A simple edge detector",
    src = code,
}

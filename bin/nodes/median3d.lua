-- supports only 3x3x3 box (center pixel plus neighbors) due to size-limited sorting network generation

local KERNEL_SIZE = 3*3*3

local template = [[#version 450
#line 6
layout(local_size_x = 4, local_size_y = 4, local_size_z = 4) in;

uniform sampler3D in_Tex;
writeonly restrict uniform image3D out_Tex;
uniform uint u_elem = 0;

// Pre-4.5
//#define SWAP(a,b) { vec4 t1 = v[a], t2 = v[b]; v[a] = min(t1, t2); v[b] = max(t1, t2); }
// 4.5 supports mix(_, _, bvec) -- about 5x as fast as the above
#define SWAP(a,b) { bvec4 cmp = lessThan(v[a], v[b]); vec4 mins = mix(v[b], v[a], cmp); v[b] =  mix(v[a], v[b], cmp); v[a] = mins; }
#define SYNC /* nothing */
void sort(inout vec4 v[$KERNEL_SIZE]) { $sortcode }
#undef SWAP
#undef SYNC

vec4 medianvalue(uvec3 p, uint which)
{
    #define T(a,b,c) texelFetch(in_Tex, ivec3(p) + ivec3(a,b,c), 0)
    vec4 v[$KERNEL_SIZE] = { $boxcode };
    #undef T
    sort(v);
    return v[which];
}

void main()
{
    uvec3 i, sz = imageSize(out_Tex);
    AUTOTILE(i, sz)
    {
        vec4 a = medianvalue(i, u_elem);
        imageStore(out_Tex, ivec3(i), a);
    }
}
]]


local function _buildshader(node)
    local sz = 1
    
    local sortcode = {}
    for _, g in ipairs(algo.generateSortNetwork(KERNEL_SIZE)) do
        for _, e in ipairs(g) do
            table.insert(sortcode, ("SWAP(%d, %d)"):format(table.unpack(e)))
        end
        table.insert(sortcode, "SYNC")
    end
    sortcode = table.concat(sortcode, " ")

    local boxcode = {}
    for z = -sz, sz do
        for y = -sz, sz do
            for x = -sz, sz do
                table.insert(boxcode, ("T(%d,%d,%d)"):format(x,y,z))
            end
        end
    end
    boxcode = table.concat(boxcode, ", ")

    local code = template:expand()
    local sh = gpu.computeshader.Create(code)
    node:setShader(sh)
end

local function init(node)
    _buildshader(node)
end

local function drawUI(node)
    local sh = node._shader
    if sh then
        local ec, elem = imgui.SliderInt("Index", sh:getuniform("u_elem"), 0, KERNEL_SIZE-1)
        if ec then
            assert(sh:setuniform("u_elem", elem))
        end
        return ec
    end
end

return {
    name = "3D Median box filter (3x3)",
    desc = "Calculates median around a center pixel",
    category = "compute",
    tags = "sort",
    init = init,
    drawUI = drawUI,
    maxsize = 128,
}

-- NONTE: This does not consider non-uniform pixel sizes

local CODE_PREP = [[
#version 430 core

layout(local_size_x = 32, local_size_y = 32, local_size_z = 1) in;

const float INF = 1E20;

layout(r32f) writeonly restrict uniform image3D out_Tex;
uniform sampler3D in_Tex;

uniform float threshold = 0.5;

float value(ivec3 pos)
{
    float v = texelFetch(in_Tex, pos, 0).x;
    // < thresh becomes INF, >= thresh becomes 0
    return (1.0 - step(threshold, v)) * INF; // This is not actual infinity. Otherwise this would end up as NaN
}

void main()
{
    uvec3 i, sz = imageSize(out_Tex);
    AUTOTILE(i, sz)
    {
        float v = value(ivec3(i));
        imageStore(out_Tex, ivec3(i), vec4(v));
    }
}
]]


local CODE_TRANSFORM = [[
#version 430 core

// Based on the distance transform described in http://cs.brown.edu/~pff/dt/

layout(local_size_x = 32, local_size_y = 32, local_size_z = 1) in;

#if   defined(AXIS_X)
#define AXIS x
#define OAX yz
#elif defined(AXIS_Y)
#define AXIS y
#define OAX zx
#elif defined(AXIS_Z)
#define AXIS z
#define OAX xy
#else
#error what
#endif

#line 63

const float INF = 1E20;

layout(r32f) restrict writeonly uniform image3D out_Tex;
//layout(r32f) restrict readonly uniform image3D in_Tex;
uniform sampler3D in_Tex;

layout(std430) restrict buffer VBuf
{
    int v[];
} vbuf;

layout(std430) restrict buffer ZBuf
{
    float z[];
} zbuf;

ivec3 imgsz = imageSize(out_Tex);

float f(ivec2 start, int i)
{
    ivec3 p;
    p.AXIS = i;
    p.OAX = start;
    return texelFetch(in_Tex, p, 0).x;
    //return imageLoad(in_Tex, p).x;
}

void result(ivec2 start, int q, float d)
{
    ivec3 p;
    p.AXIS = q;
    p.OAX = start;
    imageStore(out_Tex, p, vec4(d));
}

int bufstart(ivec2 start, int offset)
{
    ivec3 p = ivec3(0, start);
    ivec3 sz = ivec3(imgsz.AXIS, imgsz.OAX);
    sz.x += offset;
    return p.z * sz.x * sz.y
         + p.y * sz.x
         + p.x;
}

// Have each thread operate on its own slice of memory
#define V(_i) vbuf.v[B+(_i)]
#define Z(_i) zbuf.z[BZ+(_i)]
#define F(_i) f(start,_i)

void dt1d(ivec2 start)
{
    int B = bufstart(start, 0); // FIXME: should be 64 bit safe
    int BZ = bufstart(start, 1); // the memory block for z is one larger than the rest
    int n = imgsz.AXIS;
    
    V(0) = 0;
    Z(0) = -INF;
    Z(1) = +INF;
    
    {
        int k = 0;
        for(int q = 1; q < n; ++q)
        {
            float tmp = F(q) + float(q*q);
            float s;
            while(true)
            {
                int vk = V(k);
                s = (tmp - (F(vk) + float(vk*vk))) / float(2*q - 2*vk);
                if(s > Z(k))
                    break;
                --k;
            }
            ++k;
            V(k) = q;
            Z(k) = s;
            Z(k+1) = INF;
        }
    }

    {
        int k = 0;
        #ifdef FINAL_PASS
        float div = length(vec3(imgsz));
        #endif
        for(int q = 0; q < n; ++q)
        {
            while(Z(k+1) < q)
                ++k;
            int vk = V(k);
            int tmp = q - vk;
            float d = float(tmp*tmp) + F(vk);
        #ifdef FINAL_PASS
            d = sqrt(d);
          #ifdef NORMALIZE
            d /= div; // normalize to texcoords
          #endif
          #ifdef INVERT
            d = 1.0 - d;
          #endif
        #endif
            result(start, q, d);
        }
    }
}

void main()
{
    uvec2 i, sz = uvec2(imgsz.OAX);
    AUTOTILE(i, sz)
    {
        dt1d(ivec2(i));
    }
}
]]

local function setupShaders(node)
    local inv = (node.invert and "1") or nil
    local norm = (node.normalize and "1") or nil
    local oldpre = node._shpre
    node._shpre = gpu.computeshader.Create(CODE_PREP)
    node._sh =
    {   -- shader dispatch order is backwards, so X axis is always last
        gpu.computeshader.Create(CODE_TRANSFORM, { AXIS_X = 1, FINAL_PASS = 1, INVERT = inv, NORMALIZE = norm }),
        gpu.computeshader.Create(CODE_TRANSFORM, { AXIS_Y = 1 }),
        gpu.computeshader.Create(CODE_TRANSFORM, { AXIS_Z = 1 }),
    }
    
    if oldpre then
        node._shpre:copyvars(oldpre)
    end
end

local function init(node)
    if node.normalize == nil then
        node.normalize = true
    end
    setupShaders(node)
end

local mod1 = math.mod1

-- Gives same thing as OAX in the shader
local function shuf(axis, ...)
    local x = select(mod1(axis+1, 3), ...)
    local y = select(mod1(axis+2, 3), ...)
    return x, y 
end

local function pass(node, k, in_Tex, out_Tex, vbuf, zbuf)
    --print("DT pass " .. k .. " begin")
    --vbuf:invalidate()
    --zbuf:invalidate()
    
    local sh = node._sh[k]
    ---local locx, locy = sh:localsize() -- 32, 32
     -- One dt1d kernel walks over one dimension, so we need this many runs in the other two dimensions.
     --> Makes this a 2D-dispatch.
    local tdx, tdy = shuf(k, out_Tex:internalsize())
    -- Need the right number of blocks for this many dispatches
    ---local bx = (tdx + (locx - 1)) // locx
    ---local by = (tdy + (locy - 1)) // locy
    
    assert(in_Tex ~= out_Tex)
    assert(vbuf ~= zbuf)
    --assert(sh:setimage("in_Tex", in_Tex, "r"))
    assert(sh:setsampler("in_Tex", in_Tex))
    assert(sh:setimage("out_Tex", out_Tex, "w"))
    assert(sh:setbuffer("VBuf", vbuf))
    assert(sh:setbuffer("ZBuf", zbuf))
    --sh:dispatch(bx, by)
    sh:multiDispatchForSize(tdx, tdy, 0, 256,256,0)
    sh:clearRefs() 
    --print("DT pass " .. k .. " done")
end

local function prep(node, in_Tex)
    --print("DT prep begin")
    local sh = node._shpre
    local out_Tex = in_Tex:newfrom("r32f")
    sh:setsampler("in_Tex", in_Tex)
    sh:setimage("out_Tex", out_Tex, "w")
    local w,h,d = in_Tex:internalsize()
    sh:multiDispatchForSize(w,h,d,512,512,512)
    sh:clearRefs()
    --print("DT prep done")
    return out_Tex
end

local FLOAT_SIZE = glsl.sizeof(glsl.float)
local INT_SIZE = glsl.sizeof(glsl.int)

local function allocbufs(tex)
    local w,h,d = tex:internalsize()
    local vbuf = gpu.gpubuf.new(INT_SIZE * w*h*d)
    local zbuf = gpu.gpubuf.new(FLOAT_SIZE * (w+1)*(h+1)*(d+1))
    return vbuf, zbuf
end

local function recalc(node, inputs)
    local original = inputs.in_Tex
    if not original then
        return false
    end
    
    -- temporary work memory
    local vbuf, zbuf = allocbufs(original)
    
    -- initially swapped in/out
    local out_Tex = prep(node, original)
    local in_Tex = out_Tex:newfrom("r32f")
    
    for i = 3, 1, -1 do
        -- ping-pong between textures, but don't touch the original
        in_Tex, out_Tex = out_Tex, in_Tex
        pass(node, i, in_Tex, out_Tex, vbuf, zbuf)
    end
    
    -- don't wait for GC to clean up
    in_Tex:delete()
    vbuf:delete()
    zbuf:delete()
    
    return out_Tex
end

local function drawDetailUI(node)
    local ch, inv = imgui.Checkbox("Invert", node.invert)
    local ch2, norm = imgui.Checkbox("Normalize", node.normalize)
    if ch or ch2 then
        node.invert = inv
        node.normalize = norm
        setupShaders(node)
        node:onSomethingChanged()
    end
    return node:drawDetailUIDefault()
end

local function drawUI(node)
    if shadergraph.autoDrawControls(node._shpre, "threshold") then
        node:onSomethingChanged()
    end
end

return {
    name = "3D distance transform",
    category = "compute",
    references = "http://cs.brown.edu/~pff/dt/",
    
    init = init,
    recalc = recalc,
    drawUI = drawUI,
    drawDetailUI = drawDetailUI,
    makeOutput = false, -- this node manages outputs on its own
    
    extraInputs = {
        in_Tex = glsl.sampler3D,
    },
    extraOutputs = {
        out_Tex = glsl.sampler3D,
    }
}

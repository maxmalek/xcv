local code = [[#version 450
layout(local_size_x=8, local_size_y=8, local_size_z=8) in;
writeonly restrict uniform image3D out_Tex;
uniform float u_mul = 1.0, u_scale = 10.0;
uniform float u_time = 0.0;
uniform uint noiseType = 0;
uniform uint channels = 1;
uniform float u_offset = 1.0;

// by Brian Sharpe
float Perlin4D( vec4 P )
{
    //  https://github.com/BrianSharpe/Wombat/blob/master/Perlin4D.glsl

    // establish our grid cell and unit position
    vec4 Pi = floor(P);
    vec4 Pf = P - Pi;
    vec4 Pf_min1 = Pf - 1.0;

    // clamp the domain
    Pi = Pi - floor(Pi * ( 1.0 / 69.0 )) * 69.0;
    vec4 Pi_inc1 = step( Pi, vec4( 69.0 - 1.5 ) ) * ( Pi + 1.0 );

    // calculate the hash.
    const vec4 OFFSET = vec4( 16.841230, 18.774548, 16.873274, 13.664607 );
    const vec4 SCALE = vec4( 0.102007, 0.114473, 0.139651, 0.084550 );
    Pi = ( Pi * SCALE ) + OFFSET;
    Pi_inc1 = ( Pi_inc1 * SCALE ) + OFFSET;
    Pi *= Pi;
    Pi_inc1 *= Pi_inc1;
    vec4 x0y0_x1y0_x0y1_x1y1 = vec4( Pi.x, Pi_inc1.x, Pi.x, Pi_inc1.x ) * vec4( Pi.yy, Pi_inc1.yy );
    vec4 z0w0_z1w0_z0w1_z1w1 = vec4( Pi.z, Pi_inc1.z, Pi.z, Pi_inc1.z ) * vec4( Pi.ww, Pi_inc1.ww );
    const vec4 SOMELARGEFLOATS = vec4( 56974.746094, 47165.636719, 55049.667969, 49901.273438 );
    vec4 hashval = x0y0_x1y0_x0y1_x1y1 * z0w0_z1w0_z0w1_z1w1.xxxx;
    vec4 lowz_loww_hash_0 = fract( hashval * ( 1.0 / SOMELARGEFLOATS.x ) );
    vec4 lowz_loww_hash_1 = fract( hashval * ( 1.0 / SOMELARGEFLOATS.y ) );
    vec4 lowz_loww_hash_2 = fract( hashval * ( 1.0 / SOMELARGEFLOATS.z ) );
    vec4 lowz_loww_hash_3 = fract( hashval * ( 1.0 / SOMELARGEFLOATS.w ) );
    hashval = x0y0_x1y0_x0y1_x1y1 * z0w0_z1w0_z0w1_z1w1.yyyy;
    vec4 highz_loww_hash_0 = fract( hashval * ( 1.0 / SOMELARGEFLOATS.x ) );
    vec4 highz_loww_hash_1 = fract( hashval * ( 1.0 / SOMELARGEFLOATS.y ) );
    vec4 highz_loww_hash_2 = fract( hashval * ( 1.0 / SOMELARGEFLOATS.z ) );
    vec4 highz_loww_hash_3 = fract( hashval * ( 1.0 / SOMELARGEFLOATS.w ) );
    hashval = x0y0_x1y0_x0y1_x1y1 * z0w0_z1w0_z0w1_z1w1.zzzz;
    vec4 lowz_highw_hash_0 = fract( hashval * ( 1.0 / SOMELARGEFLOATS.x ) );
    vec4 lowz_highw_hash_1 = fract( hashval * ( 1.0 / SOMELARGEFLOATS.y ) );
    vec4 lowz_highw_hash_2 = fract( hashval * ( 1.0 / SOMELARGEFLOATS.z ) );
    vec4 lowz_highw_hash_3 = fract( hashval * ( 1.0 / SOMELARGEFLOATS.w ) );
    hashval = x0y0_x1y0_x0y1_x1y1 * z0w0_z1w0_z0w1_z1w1.wwww;
    vec4 highz_highw_hash_0 = fract( hashval * ( 1.0 / SOMELARGEFLOATS.x ) );
    vec4 highz_highw_hash_1 = fract( hashval * ( 1.0 / SOMELARGEFLOATS.y ) );
    vec4 highz_highw_hash_2 = fract( hashval * ( 1.0 / SOMELARGEFLOATS.z ) );
    vec4 highz_highw_hash_3 = fract( hashval * ( 1.0 / SOMELARGEFLOATS.w ) );

    // calculate the gradients
    lowz_loww_hash_0 -= 0.49999;
    lowz_loww_hash_1 -= 0.49999;
    lowz_loww_hash_2 -= 0.49999;
    lowz_loww_hash_3 -= 0.49999;
    highz_loww_hash_0 -= 0.49999;
    highz_loww_hash_1 -= 0.49999;
    highz_loww_hash_2 -= 0.49999;
    highz_loww_hash_3 -= 0.49999;
    lowz_highw_hash_0 -= 0.49999;
    lowz_highw_hash_1 -= 0.49999;
    lowz_highw_hash_2 -= 0.49999;
    lowz_highw_hash_3 -= 0.49999;
    highz_highw_hash_0 -= 0.49999;
    highz_highw_hash_1 -= 0.49999;
    highz_highw_hash_2 -= 0.49999;
    highz_highw_hash_3 -= 0.49999;

    vec4 grad_results_lowz_loww = inversesqrt( lowz_loww_hash_0 * lowz_loww_hash_0 + lowz_loww_hash_1 * lowz_loww_hash_1 + lowz_loww_hash_2 * lowz_loww_hash_2 + lowz_loww_hash_3 * lowz_loww_hash_3 );
    grad_results_lowz_loww *= ( vec2( Pf.x, Pf_min1.x ).xyxy * lowz_loww_hash_0 + vec2( Pf.y, Pf_min1.y ).xxyy * lowz_loww_hash_1 + Pf.zzzz * lowz_loww_hash_2 + Pf.wwww * lowz_loww_hash_3 );

    vec4 grad_results_highz_loww = inversesqrt( highz_loww_hash_0 * highz_loww_hash_0 + highz_loww_hash_1 * highz_loww_hash_1 + highz_loww_hash_2 * highz_loww_hash_2 + highz_loww_hash_3 * highz_loww_hash_3 );
    grad_results_highz_loww *= ( vec2( Pf.x, Pf_min1.x ).xyxy * highz_loww_hash_0 + vec2( Pf.y, Pf_min1.y ).xxyy * highz_loww_hash_1 + Pf_min1.zzzz * highz_loww_hash_2 + Pf.wwww * highz_loww_hash_3 );

    vec4 grad_results_lowz_highw = inversesqrt( lowz_highw_hash_0 * lowz_highw_hash_0 + lowz_highw_hash_1 * lowz_highw_hash_1 + lowz_highw_hash_2 * lowz_highw_hash_2 + lowz_highw_hash_3 * lowz_highw_hash_3 );
    grad_results_lowz_highw *= ( vec2( Pf.x, Pf_min1.x ).xyxy * lowz_highw_hash_0 + vec2( Pf.y, Pf_min1.y ).xxyy * lowz_highw_hash_1 + Pf.zzzz * lowz_highw_hash_2 + Pf_min1.wwww * lowz_highw_hash_3 );

    vec4 grad_results_highz_highw = inversesqrt( highz_highw_hash_0 * highz_highw_hash_0 + highz_highw_hash_1 * highz_highw_hash_1 + highz_highw_hash_2 * highz_highw_hash_2 + highz_highw_hash_3 * highz_highw_hash_3 );
    grad_results_highz_highw *= ( vec2( Pf.x, Pf_min1.x ).xyxy * highz_highw_hash_0 + vec2( Pf.y, Pf_min1.y ).xxyy * highz_highw_hash_1 + Pf_min1.zzzz * highz_highw_hash_2 + Pf_min1.wwww * highz_highw_hash_3 );

    // Classic Perlin Interpolation
    vec4 blend = Pf * Pf * Pf * (Pf * (Pf * 6.0 - 15.0) + 10.0);
    vec4 res0 = grad_results_lowz_loww + ( grad_results_lowz_highw - grad_results_lowz_loww ) * blend.wwww;
    vec4 res1 = grad_results_highz_loww + ( grad_results_highz_highw - grad_results_highz_loww ) * blend.wwww;
    res0 = res0 + ( res1 - res0 ) * blend.zzzz;
    blend.zw = vec2( 1.0 ) - blend.xy;
    return dot( res0, blend.zxzx * blend.wwyy );
}

float cheapnoise(vec4 q)
{
    const vec4 SOMELARGEFLOATS = vec4( 16974.746094, 7165.636719, 15049.667969, 9901.273438 );
    const float f = fract(sin(dot(q, SOMELARGEFLOATS)) * 43758.5453);
    return (f - 0.5) * 2.0;
}

float singlenoise(vec3 uv, float w)
{
    vec4 q = vec4(uv * u_scale, w);
    float ret = 0.0;
    switch(noiseType)
    {
        case 0:
            ret = cheapnoise(q);
            break;
        case 1:
            ret = Perlin4D(q);
            break;
    }
    return ret;
}

vec4 mynoise(vec3 uv)
{
    vec4 ret = vec4(0.0);
    switch(channels)
    {
        case 1: ret.xyzw = vec4(singlenoise(uv, u_time));
        break;
        
        case 2: ret.xz = vec2(singlenoise(uv, u_time));
                ret.yw = vec2(singlenoise(uv, u_time + u_offset));
        break;
                
        case 3: ret.xw = vec2(singlenoise(uv, u_time));
                ret.y = singlenoise(uv, u_time + u_offset);
                ret.z = singlenoise(uv, u_time + u_offset * 2.0);
        break;
                
        case 4:
            for(uint i = 0; i < 4; ++i)
                ret[i] = singlenoise(uv, u_time + u_offset * float(i));
        break;
    }
    
    return ret;
}

void main()
{
  uvec3 i, sz = imageSize(out_Tex);
  AUTOTILE(i, sz)
  {
    vec3 uv = (vec3(i) + 0.5) / vec3(sz);
    vec4 f = mynoise(uv) * u_mul;
    imageStore(out_Tex, ivec3(i), f);
  }
};
]]

local NoiseTypes =
{
    "Salt+Pepper",
    "Perlin",
}

local TexSizes = { 64, 128, 256, 512 }

local MAXNOISE = #NoiseTypes - 1 -- inclusive

local function _updatefmt(node)
    local fmt = gpu.formatstr(node.channels, glsl.float, 16)
    if fmt ~= node.texfmt then
        node.texfmt = fmt
        return true
    end
end

local function _updatechannels(node, n)
    n = n or 1
    node.channels = n
    local sh = node._shader
    sh:setuniform("channels", n)
    _updatefmt(node)
end

local function _updatetexsize(node, idx)
    idx = idx or 1
    node.texSizeIdx = idx or 1
    node.texSize = TexSizes[node.texSizeIdx] or 64
end

local function init(node)
    _updatechannels(node, node.channels)
    _updatetexsize(node, node.texSizeIdx)
    node.noiseType = node.noiseType or 0
    _updatefmt(node)
end
local function makeOutput(node, name, inputs)
    local sz = node.texSize
    return gpu.texture.new(sz,sz,sz, node.texfmt, "l") -- auto swizzle does the right thing
end

local function drawUI(node)
    node:drawUIDefault()
    local changed, ret, val
    local sh = node._shader
    
    changed, val = imgui.Combo("Noise", node.noiseType, NoiseTypes)
    if changed then
        node.noiseType = val
        sh:setuniform("noiseType", val - 1) -- Lua->GLSL index
        ret = true
    end
    
    changed, val = imgui.Combo("Tex size", node.texSizeIdx, TexSizes)
    if changed then
        _updatetexsize(node, val)
        ret = true
    end
    
    -- FIXME: using a slider with explicit min/max values since the current uniform auto-handling doesn't enforce any boundaries
    changed, val = imgui.SliderInt("channels", node.channels, 1, 4)
    if changed then
        _updatechannels(node, val)
        ret = true
    end
    
    changed, node.anim = imgui.Checkbox("Animate", node.anim)
    
    return ret
end
local function update(node, dt)
    local sh = node._shader
    if node.anim and sh then
        -- can skip init() when doing it like this,
        -- plus we get serialization for free
        local t = sh:getuniform("u_time")
        sh:setuniform("u_time", t + dt)
        node:onSomethingChanged() -- recompute
    end
end

local function serialize(node)
    return { anim = node.anim, channels = node.channels, noiseType = node.noiseType, texSizeIdx = node.texSizeIdx }
end

local function deserialize(node, t)
    node.anim = not not t.anim
    _updatechannels(node, t.channels)
    _updatetexsize(node, t.texSizeIdx)
    node.noiseType = t.noiseType
end

return {
    name = "3D Noise",
    category = "compute",
    desc = "Produces 3D noise of various flavors.",
    tags = "perlin",
    references = { "https://github.com/BrianSharpe/Wombat", "http://doi.acm.org/10.1145/325165.325247" },
    init = init,
    drawUI = drawUI,
    update = update,
    makeOutput = makeOutput,
    serialize = serialize,
    deserialize = deserialize,
    src = code,
    maxsize = 256,
}

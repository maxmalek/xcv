local code = [[
#version 430 core

layout(local_size_x = 32, local_size_y = 32) in;

#ifndef AXIS
#error Must define axis (x or y)
#endif

uniform int kernelSize; // extent of kernel to the left and right (around the central value)
uniform float sigma;
uniform sampler2D inTex;
layout(rgba32f) writeonly restrict uniform image2D outTex;


float gaussv(float x)
{
    return exp(-0.5 * (x*x) / (sigma * sigma) );
}

const float pi = 3.14159265;

vec4 rsmooth(ivec2 p, ivec2 sz)
{
    // Incremental Gaussian Coefficent Calculation (See GPU Gems 3 pp. 877 - 889)
    vec3 g;
    g.x = 1.0 / (sqrt(2.0 * pi) * sigma);
    g.y = exp(-0.5 / (sigma * sigma));
    g.z = g.y * g.y;
    
    vec4 accu = vec4(0.0);
    float sum = 0.0;
    
    // center sample
    vec4 c = texelFetch(inTex, p, 0);
    accu += c * g.x;
    sum += g.x;
    g.xy *= g.yz;
    
    ivec2 offs = ivec2(0);
    offs.AXIS = 1;
    
    for(int i = 0; i < kernelSize; ++i)
    {
        accu += texelFetch(inTex, max(ivec2(0), p - i*offs), 0) * g.x;
        accu += texelFetch(inTex, min(sz, p + i*offs), 0) * g.x;
        sum += 2.0 * g.x;
        g.xy *= g.yz;
    }
    
    return accu / sum;
}


void main()
{
    uvec2 i, sz = imageSize(outTex);
    AUTOTILE(i, sz)
    {
        vec4 p = rsmooth(ivec2(i), ivec2(sz));
        imageStore(outTex, ivec2(i), p);
    }
}

]]

local function _updatehisto(node)
    node._histo = math.createGaussKernel(node._ksz, node._sigma)
end

local AXISNAME = { "x", "y" }
local function compile(node, axis)
    local sh = gpu.computeshader.Create(code, { AXIS = AXISNAME[axis] })
    node._sh[axis] = sh
end

local function _setall(node, vn, val)
    for i = 1, 2 do
        node._sh[i]:setany(vn, val)
    end
    return val
end

local function init(node)
    if not node._sh then node._sh = {} end
    if not node._shon then node._shon = {true,true} end -- all 2 axes enabled by default
    if not node._sigma then node._sigma = 1 end
    if not node._ksz then node._ksz = 2 end
    _updatehisto(node)
    node._activesh = {}
    for i = 1, 2 do
        compile(node, i)
        if node._shon[i] then
            table.insert(node._activesh, node._sh[i])
        end
    end
    _setall(node, "sigma", node._sigma)
    _setall(node, "kernelSize", node._ksz)
end

local function _cb(node, axis, sameline)
    local changed, on = imgui.Checkbox(AXISNAME[axis], node._shon[axis])
    if changed then
        node._shon[axis] = on
        table.clear(node._activesh)
        for i = 1, 2 do
            if node._shon[i] then
                table.insert(node._activesh, node._sh[i])
            end
        end
    end
    if sameline then
        imgui.SameLine()
    end
    return changed
end

local function _drawUI(node, detail)
    local c, c2, val
    
    if detail then
        imgui.Text("Apply on axis:")
        for i = 1, 2 do
            c = _cb(node, i, i ~= 2) or c
        end
    end
    
    c2, val = imgui.Slider("sigma", node._sigma, 0.0001, 16) -- prevent divide by zero
    if c2 then c = true; node._sigma = _setall(node, "sigma", val) end
    c2, val = imgui.SliderInt("size", node._ksz, 1, 32)
    if c2 then c = true; node._ksz = _setall(node, "kernelSize", val) end
    
    if c then
        _updatehisto(node)
        node:onSomethingChanged()
    end
    
    if detail then
        local x, y = imgui.GetContentRegionAvail()
        imgui.PlotHistogram("##histo", node._histo, 0, nil, nil, nil, x, 100)
    end
end

local function drawUI(node)
    _drawUI(node, false)
end

local function drawDetailUI(node)
    _drawUI(node, true)
    node:drawDetailUIDefault()
end


local function _makeOutput(node, inputs)
    local tex = inputs.in2D
    local fmt = gpu.formatstr(tex:channels(), glsl.float) 
    return tex:newfrom(fmt)
end

local function recalc(node, inputs)
    local texIn, texOut
    local ash = node._activesh
    local texOrig = inputs.in2D
    texOut = texOrig
    if not texOut then
        return
    end
    for i = 1, #ash do
        local last = i == #ash
        if texIn and texIn ~= texOrig then
            texIn:delete() -- conserve memory
        end
        texIn = texOut
        texOut = _makeOutput(node, inputs, "out2D")
        assert(texOut)
        local sh = ash[i]
        sh:setany("inTex", texIn)
        sh:setany("outTex", texOut)
        sh:dispatchForSize(texOut:internalsize())
    end
    
    return texOut
end

local function serialize(node)
    return { sigma = node._sigma, size = node._ksz, on = node._shon }
end

local function deserialize(node, t)
    if t.sigma then node._sigma = t.sigma end
    if t.size then node._ksz = t.size end
    if t.on then node._shon = t.on end
end

return {
    name = "2D smooth",
    desc = "Apply Gaussian Blur to a 2D image",
    category = "compute",
    
    init = init,
    recalc = recalc,
    drawUI = drawUI,
    drawDetailUI = drawDetailUI,
    makeOutput = false,
    serialize = serialize,
    deserialize = deserialize,
    maxsize = 2048,
    
    extraInputs = {
        in2D = glsl.sampler2D,
    },
    extraOutputs = {
        out2D = glsl.sampler2D,
    }
}

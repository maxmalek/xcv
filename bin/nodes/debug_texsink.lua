

local function recalc(node, inputs)
    print("SINK[#" .. #inputs .. "]: " .. tostring(inputs.intex))
end

local function drawUI(node)
    if imgui.Button("Recalc") then
        node:onSomethingChanged()
    end
end

return {
    name = "Sink",
    category = "DEBUG",
    extraInputs = {
        intex = glsl.sampler3D,
        mask = glsl.sampler2D,
    },
    recalc = recalc,
    drawUI = drawUI
}

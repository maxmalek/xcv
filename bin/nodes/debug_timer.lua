
local function init(node)
    node.timer = nil
end

local function timeupdate(node)
    if not node.timer then
        node.timer = gpu.gputimer.new()
    end
    node.timer:call(node.onSomethingChanged, node)
end

local function recalc(node, inputs)
    local x = inputs["in"]
    if x ~= node.input then
        node.input = x
        node:postInit()
        timeupdate(node)
    end
    return x
end

local function changeconnectors(node)
    local inputs = node:_getInputsTable()
    local x = inputs["in"]
    local ty = x and x:glsltype() or glsl.unknown
    --infoprint("debug_timer: Conn type is now " .. glsl.nameof(ty) .. " for obj " .. tostring(x))
    local _, c = next(node.Cout)
    c.type = ty
    c.color = glsl.color(ty)
    if ty ~= glsl.unknown then
        c.detail = ""
    end
    _, c = next(node.Cin)
    c.color = glsl.color(ty)
end

local function drawUI(node)
    local press = imgui.Button("Update")
    
    -- do this before the timer call so it has time to process before the value is retrieved
    local s
    if node.timer then
        local ms = node.timer:difftimeMS()
        s = ("%.4f ms"):format(ms)
    else
        s = "<none yet>"
    end
    imgui.Text("Time taken:")
    imgui.Text(s)
        
    if press then
        -- TODO: This will need a fix once async calls / background update is implemented
        timeupdate(node)
    end
end

local alltypes = {}
for k, v in pairs(glsl) do
    if type(k) == "string" and type(v) == "number" then
        table.insert(alltypes, v)
    end
end

return {
    name = "GPU Timer",
    category = "DEBUG",
    extraInputs = {
        ["in"] = alltypes,
    },
    extraOutputs = {
        out = glsl.unknown,
    },
    init = init,
    recalc = recalc,
    drawUI = drawUI,
    changeconnectors = changeconnectors,
    connchanged = changeconnectors,
    makeOutput = false,
}

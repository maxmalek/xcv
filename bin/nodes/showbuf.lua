-- FIXME: re-do hex editor API for this -- should no longer spawn new window

local EMPTY_TITLE = "No buffer to display"

local W = newclass("memoryeditor_debug_showbuf", assert(gui.imwindow))

function W.new(node)
    --local sysw = node:getHolder():getParent()
    local sysw = assert(sys.syswindow.GetCurrent())
    local self = sysw:newInner(EMPTY_TITLE, tostring(node), 640, 480)
    switchclass(self, W)

    self.node = node
    self.memedit = gui.memoryeditor.new()
    return self
end

function W:drawUI()
    self.memedit:drawDefault()
end


local function init(node)
    if not node.memedit then
        node.memedit = gui.memoryeditor.new()
        node.memedit:setColumns(8)
    end
    if not node.win then
        node.win = assert(W.new(node))
        node.win:hide()
    end
    node.windows = node.windows or {}
end

local drawDetailUI

local function _viewMenu(node)
    local buf = node.buf
    local winvis = node.win:visible()
    if imgui.MenuItem("Show window", winvis) then
        node.win:setVisible(not winvis)
    end
    if imgui.MenuItem("Pop out") then
        local me = gui.memoryeditor.new()
        me:set(buf)
        local w = sys.syswindow:Open(800, 600)
        w.node = node
        w.memedit = me
        node.windows[w] = true
        function w:onDrawUI()
            imgui.MainMenuBar(imgui.Menu, "Layout", true, me.drawMenu, me)
            imgui.Dummy(0, 10)
            me:drawBody()
        end
        function w:onClose()
            if w.node then
                w.node.windows[self] = nil
            end
        end
    end
end
--[[
local function _exportMenu(node)
    if imgui.MenuItem("Binary file...") then
        
    end
end
]]
local function _layoutMenu(node)
    node.memedit:drawMenu()
end
local function _menuBar(node)
    --imgui.Menu("Export", true, _exportMenu, node)
    imgui.Menu("View", true, _viewMenu, node)
end

drawDetailUI = function(node)
    node:drawDetailUIDefault()
    node.memedit:drawDefault()
    imgui.MenuBar(_menuBar, node)
end


local function recalc(node, inputs)
    local buf = inputs.in_Buf
    local title = EMPTY_TITLE
    if buf then
        local addr = debug.addressof(buf:readptr() or buf:writeptr())
        local sz = buf:rawsize()
        title = ("Buffer view (0x%016X-0x%016X, %u bytes)"):format(addr, addr+sz, sz)
    end
    if node.win then
        node.win:setTitle(title)
    end
    
    for w in pairs(node.windows) do
        w.memedit:set(buf)
        w:setTitle(title)
    end

    node.buf = buf
    node.memedit:set(buf)
    if node.win then
        node.win.memedit:set(buf)
    end
    return buf
end

local function destroy(node)
    if node.win then
        node.win:destroy()
        node.win = nil
    end
    
    for w in pairs(node.windows) do
        w.node = nil
        w:destroy()
    end
end

return {
    name = "Show Buffer (Hex-editor)",
    desc = "Allows to display and edit a memory buffer on the fly",
    category = "output",
    
    init = init,
    recalc = recalc,
    destroy = destroy,
    makeOutput = false, -- this node manages outputs on its own
    drawDetailUI = drawDetailUI,
    
    extraInputs = {
        in_Buf = glsl.buffer,
    },
    extraOutputs = {
        out_Buf = glsl.buffer,
    }
}

local code = [[#version 430
layout(local_size_x = 32, local_size_y = 32) in;
uniform sampler2D in_Tex;
writeonly restrict uniform image2D out_Tex;
uniform float u_exp = 1.0; // default values
uniform float u_add = 0.0;
void main()
{
   uvec2 i, sz = imageSize(out_Tex);
   AUTOTILE(i, sz)
   {
        vec4 v = texelFetch(in_Tex, ivec2(i), 0); // get pixel
        v = pow(v + u_add, vec4(u_exp));          // calculate
        imageStore(out_Tex, ivec2(i), v);         // write result
   }
};
]]
return {
    category = "example",
    name = "2D pow",
    desc = "Calculates pow(p, u_exp) + u_add for each pixel p",
    src = code,
}

-- takes custom code

local TEMPLATE_PRE = [[
#version 450
layout(local_size_x = 32, local_size_y = 32) in; // works for 2D and 3D
#line 1
]]


local TEMPLATE_POST = [[
ivec3 makev3(float x) { return ivec3(x, 1.0, 1.0); }
ivec3 makev3(ivec2 xy) { return ivec3(xy, 1.0); }
ivec3 makev3(ivec3 xyz) { return xyz; }
void main()
{
    uvec3 i, sz = makev3(imageSize(out_Tex));
    AUTOTILE(i, sz)
    {
        func(ivec3(i));
    }
}
]]

local INITCODE = [[
// --mandatory--
writeonly uniform image3D out_Tex;
uniform sampler3D in_Tex;

// --inputs--

uniform float u_f = 1.0;

// --per-pixel worker--
void func(ivec3 p)
{
    vec4 v = texelFetch(in_Tex, p, 0);
    v.xyz *= u_f;
    imageStore(out_Tex, p, v);
}
]]

local SHED = newclass("shadereditor_node_custom_compmute_simple", shadereditor)

function SHED.new(node, code)
    local self = shadereditor.new()
    switchclass(self, SHED)
    self.node = assert(node)
    return self
end
function SHED:onCompile(code)
    code = TEMPLATE_PRE .. code .. TEMPLATE_POST
    local sh = gpu.computeshader.new()
    local ok, err = sh:load(code)
    if ok then
        self.node:setShader(sh)
    end
    return ok, err
end

------------------------------------------

local function _checkfmt(node, fmt)
    local ok = fmt == "" or not not gpu.texture.ParseFormat(fmt)
    node.formatok = ok
    return ok
end

local function _checkswizzle(node, swizzle)
    local ok = gpu.texture.CheckSwizzle(swizzle)
    node.swizzleok = ok
    return ok
end


local function _drawtexsettings(node)
    local ch, s = imgui.ItemWidth(70, imgui.InputTextGood, node.formatok, "Output format", node.outfmt, 16)
    ui.help.topicTooltipForLastItem("tex_format_str")
    if ch then
        node.outfmt = s
        if _checkfmt(node, s) then
            node:onSomethingChanged()
        end
    end

    local ch, s =  imgui.ItemWidth(70, imgui.InputTextGood, node.swizzleok, "Swizzle mask", node.swizzle, 16)
    ui.help.topicTooltipForLastItem("tex_swizzle_mask")
    if ch then
        node.swizzle = s
        if _checkswizzle(node, s) then
            node:onSomethingChanged()
        end
    end
end

local function drawDetailUI(node)
    
    imgui.TreeNode("out_Tex", "of", node.drawOutput, node, "out_Tex") -- FIXME: this could iterate over all outputs...?
    
    imgui.TreeNode("Output settings", "f", _drawtexsettings, node)
    imgui.Separator()
    node.editor:drawUI()
end

------------------------------------------

local function makeOutput(node, inputs, name)
    local tex = inputs.in_Tex
    local swizzle = node.swizzle
    local fmt = node.outfmt
    if fmt == "" then fmt = "rgba32f" end
    if swizzle == "" then swizzle = nil end
    if not node.formatok then
        return false
    end
    return tex and tex:newfrom(fmt, nil, 0,0,0, swizzle)
end

local function init(node)

    if node._shader == nil then
        node._shader = gpu.computeshader.new()
    end
    
    if not node.editor then
        node.editor = SHED.new(node)
        node.editor:setCode(INITCODE) -- compiles and inits shader
    end

    if node.outfmt == nil then
        node.outfmt = "rgba32f"
    end
    if node.swizzle == nil then
        node.swizzle = "rgba"
    end
    _checkfmt(node, node.outfmt)
    _checkswizzle(node, node.swizzle)
end

local function serialize(node)
    return { outfmt = node.outfmt, swizzle = node.swizzle, autorecomp = node.editor.autorecomp, code = node.editor:getCode() }
end

local function deserialize(node, t)
    node.outfmt = t.outfmt
    _checkfmt(node, node.outfmt)

    node.swizzle = t.swizzle
    _checkswizzle(node, node.swizzle)
    
    node.editor.autorecomp = t.autorecomp
    node.editor:setCode(t.code)
end

local function update(node, dt) 
    node.editor:update(dt)
end



return {
    name = "Custom code (simple)",
    desc = "Run custom GLSL code, simplified",
    category = "compute",
    drawDetailUI = drawDetailUI,
    init = init,
    makeOutput = makeOutput,
    update = update,
    serialize = serialize,
    deserialize = deserialize,
    maxsize = 256, -- guess?
    
    outGen = {
        out_Tex = "in_Tex",
    },
}

local min = math.min
local max = math.max


local function _panelMenu(self)
    return self.renderpanel:drawKnobs()
end

local function _menuBar(self)
    local mopen, ch = imgui.Menu("Settings", true, _panelMenu, self)
    local w, h = self.renderpanel:size()
    local info = ("(%.0fx%.0f)"):format(w, h)
    local txinfo, tyinfo = imgui.CalcTextSize(info)
    local sx, sy = imgui.GetContentRegionAvail()
    imgui.Dummy(sx-txinfo-20)
    imgui.SameLine()
    imgui.TextDisabled(info)
    if imgui.IsItemHovered() then
        local str = tostring(self.renderpanel.tex)
        imgui.Tooltip(str)
    end
    return ch
end


local function _drawVolume(self)
    imgui.MenuBar(_menuBar, self)
    
    local w, h = imgui.GetContentRegionAvail()
    if w >= 10 or h >= 10 then
        self.renderpanel:setWindowSize(w, h)
        
        imgui.Text("- Drag to rotate: -")
        
        local cx, cy = imgui.GetCursorPos()
        local iw, ih = imgui.ImageKeepAspect(self.renderpanel:getResultTex(), w, h,    nil,nil, nil,nil,   1,0,0,1)
        imgui.SetCursorPos(cx, cy)
        imgui.InvisibleButton("dragButton", iw, ih) -- captures input on the image
        
        local imghover = imgui.IsItemHovered()
        
        if imghover then
            if imgui.IsMouseButton(0) then
                self._interacting = true
                self._mstartX, self._mstartY = imgui.GetMousePos()
            end
            local wd = imgui.GetMouseWheelDelta()
            if wd ~= 0.0 then
                self.renderpanel:mouseWheel(wd)
            end
        end
        
        if self._interacting then
            if imgui.IsMouseButton(0) then
                local dx, dy = imgui.GetMouseMoveDelta()
                imgui.ResetMouseDragDelta(0)
                if dx ~= 0 or dy ~= 0 then
                    self.renderpanel:mouseMove(dx, dy, 1)
                end
            else
                self._interacting = false
            end
        end
        
        self._washover = imghover
        
        imgui.Dummy(0, 20)
    end
    
    self.renderpanel:redrawIfNeed()
end


local function _setObj(self, obj)
    if self._obj ~= obj then
        self._obj = obj
        self.renderpanel:removeAll()
        self.renderpanel:add(obj)
    end
end

--------------------------------

local function setShowInBack(self, on)
    debugprint("Show [" .. tostring(self.obj) .. " in back = " .. tostring(on))
    local wasinback = self.showinback
    self.showinback = on
    local back = self.parentBackpanel
    if not back then
        if not on then
            return
        end
        back = sys.syswindow.GetCurrent().backpanel
        self.parentBackpanel = back
    end

    local obj = assert(self.obj)
    if wasinback then
        back:remove(obj)
    end
    if on then
        back:add(obj, true)
    end
    back:layout()
end

local function init(node)
    if not node.obj then
        node.obj = render.volumerender.new()
    end
    if not node.renderpanel then
        node.renderpanel = render.simpleobjectpanel.new(basicobjcontrol, nil, nil, "l") -- linear interpolation, no depth buffer
        node.renderpanel.singleobj = true
        node.renderpanel:resize(800, 600)
    end
    function node.renderpanel:onRedraw()
        -- make sure the output in the node is always current
        --print("VR redraw")
        local R = node.RESULTS
        local tex = self.tex
        R.out = tex -- renderpanel's FB texture
        R.outL = self.texL or tex
        R.outR = self.texR or tex
        -- propagate updated texture to downstream nodes
        node:onSomethingChanged()
    end
end

local function destroy(self)
    --print("VR destroy: " .. tostring(self))
    if self.parentBackpanel then
        setShowInBack(self, false)
    end
end

local function _redraw(self)
    if not self._redrawing then
        self._redrawing = true
        if self.showinback then
            local back = self.parentBackpanel
            if back then
                --back:redraw()
                back.dirty = true
            end
        end
        self.renderpanel:redraw()
        self._redrawing = nil
    end
end

local function _refreshObj(self)
    _setObj(self, self.obj, self.filename)
    _redraw(self)
end

local function _refreshtex(self)  
    --devprint("vol tex: " .. tostring(self.tex) .. ", alpha: " .. tostring(self.alphatex) .. ", color: " .. tostring(self.colortex) .. ", deform: " .. tostring(self.deformtex))
    self.obj:setTex(self.tex)
    self.obj:setColorTex(self.colortex)
    self.obj:setAlphaTex(self.alphatex)
    self.obj:setDeformTex(self.deformtex)
    _refreshObj(self)
end

local function recalc(self, inputs)
    local tex = inputs.in3D
    if tex then
        self.filename = tex and tex.filename or "<Temporary>"
    else
        self.filename = "<No object>"
    end
    self.tex = tex
    self.colortex = inputs.in3DColor
    self.alphatex = inputs.in3DAlpha
    self.deformtex = inputs.in3DDeform
    _refreshtex(self)
    
    local rp = self.renderpanel
    local tex = rp:getResultTex()
    local R = self.RESULTS
    R.out = tex
    R.outL = rp.texL or tex
    R.outR = rp.texR or tex
end

local function ui_showBack(self, label)
    local cback, showback = imgui.Checkbox(label, self.showinback)
    if cback then
        setShowInBack(self, showback)
    end
end

local function sep(y)
    imgui.Dummy(0, y)
    imgui.Separator()
    imgui.Dummy(0, y)
end

local function drawDetailUI(self, win)
    --imgui.TreeNode("Render target", "s", _panelMenu, self)
    
    if CONFIG.debugmode then
        ui_showBack(self, "Add to back?")
        sep(5)
    end
    
    
    imgui.Text("Volume render:")
    
    if self.obj:drawKnobs() then
        _redraw(self)
    end
    
    _drawVolume(self)
    
    --local backbuf = self.obj._FBTEX
    --local iw, ih = imgui.ImageWidthFit(backbuf, nil,nil, nil,nil,   1,0,0,1)
end

local function makeOutput(self, inputs, name)
    local rp = node.renderpanel
    if name == "out" then
        return rp.tex
    end
    if name == "outL" then
        return rp.texL or rp.tex
    end
    if name == "outR" then
        return rp.texR or rp.tex
    end
end

local function serialize(node)
    return { obj = node.obj:serialize() }
end

local function deserialize(node, t)
    if t.obj then
        node.obj:deserialize(t.obj)
    end
end


return {
    name = "3D Volume",
    desc = "Render a 3D volume image into a 2D image",
    category = "render",
    recalc = recalc,
    drawDetailUI = drawDetailUI,
    init = init,
    destroy = destroy,
    makeOutput = makeOutput,
    serialize = serialize,
    deserialize = deserialize,
    
    extraInputs = {
        in3D = glsl.sampler3D,
        in3DColor = glsl.sampler3D,
        in3DAlpha = glsl.sampler3D,
        in3DDeform = glsl.sampler3D,
        --colorRamp = glsl.sampler1D,
        --alphaRamp = glsl.sampler1D,
    },
    extraOutputs = {
        out = glsl.sampler2D,
        outL = glsl.sampler2D,
        outR = glsl.sampler2D,
    },
}

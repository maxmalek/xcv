NOT READY YET

local code = [[
#version 430
layout(local_size_x = 32, local_size_y = 32) in;
uvec3 INC = gl_NumWorkGroups * gl_WorkGroupSize;
uvec3 IDX = gl_GlobalInvocationID;
uint TID = gl_LocalInvocationIndex;

!N = in_Tex:dimensions()
!FMT = out_Tex:formatstr()
uniform sampler${N}D in_Tex;
layout($FMT) writeonly restrict uniform image${N}D out_Tex;
#define ivecN ivec$N
#define vecN vec$N


vec4 getpix(vecN uv)
{
    return texture(in_Tex, uv);
}

void main()
{
    ivec3 sz = ivec3(imageSize(out_Tex));
    vecN tm = 1.0 / vecN(sz);
    for(uvec3 i = IDX; all(lessThan(i, uvec3(sz))); i += INC)
    {
        vecN uv = tm + (vecN(i) / vecN(sz));
        vec4 p = getpix(uv);
        imageStore(out_Tex, ivec3(i), p);
    }
}
]]

--local function init(node)
--end

--local function recalc(node, inputs)
    --node:specializeShader(code, inputs)
--end

local function drawContextMenu(node)
    if imgui.MenuItem("Internal source") then -- FIXME
        local s = "<no shader>"
        if node._shader then
            s = node._shader:getInternalSource()
        end
        debugui.showText(tostring(node), s)
    end
end

return {
    type = "compute",
    name = "Rescale",
    template = code,
    --init = init,
    --recalc = recalc,
    drawContextMenu = drawContextMenu,
    
    inDef = {
        in_Tex = { req = true, glsl.sampler1D, glsl.sampler2D, glsl.sampler3D },
    },
    outDef = {
        out_Tex = { glsl.image1D, glsl.image2D, glsl.image3D },
    },
    outGen = {
        --out_Tex = "in_Tex"
        out_Tex = { input = "in_Tex", format = "rgba16f" }
    },
}


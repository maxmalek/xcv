local code = [[#version 450
#extension GL_ARB_bindless_texture : enable
#extension GL_ARB_gpu_shader_int64 : enable

layout(local_size_x = 32, local_size_y = 32) in; // 32*32 == 1024 (don't go higher)

uniform uint64_t in_Tex;
uniform uint64_t out_Tex;

// FIXME: make possible to adjust type to match input
//layout(rgba32f, binding = 0) writeonly restrict uniform image2D out_Tex;

uvec2 INC = (gl_NumWorkGroups * gl_WorkGroupSize).xy;
uvec2 IDX = gl_GlobalInvocationID.xy;

void main()
{
    sampler2D itex = sampler2D(in_Tex);
    layout(rgba32f) image2D otex = layout(rgba32f) image2D(out_Tex);
    
    uvec2 td = imageSize(otex).xy;
    for(uvec2 i = IDX; all(lessThan(i, td)); i += INC)
    {
        vec4 a = texelFetch(itex, ivec2(i), 0);
        imageStore(otex, ivec2(i), a);
    }    
};
]]


return {
    category = "compute",
    name = "_Multi-D test",
    src = code,
}


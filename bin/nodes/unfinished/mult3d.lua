local code = [[
#version 430

layout(local_size_x = 16, local_size_y = 8, local_size_z = 8) in; // 16*8*8 == 1024 (don't go higher)

uniform float u_mult = 1.0;
uniform sampler3D in_Tex;
// FIXME: make possible to adjust type to match input
layout(rgba32f, binding = 0) writeonly restrict uniform image3D out_Tex;

uvec3 INC = gl_NumWorkGroups * gl_WorkGroupSize;
uvec3 IDX = gl_GlobalInvocationID;

void main()
{
   uvec3 td = imageSize(out_Tex);
   for(uvec3 i = IDX; all(lessThan(i, td)); i += INC)
   {
       vec4 c = texelFetch(in_Tex, ivec3(i), 0);
       imageStore(out_Tex, ivec3(i), c * u_mult);
   }
};
]]


return {
    type = "compute",
    name = "3D multiply",
    desc = "Multiplies all channels of an image with a constant",
    src = code,
}

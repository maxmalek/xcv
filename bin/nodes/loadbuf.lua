
local function shorten(s)
    return s:match"/([^/]+)$" or s
end

local function loadbuf(node, fn)
    local recalc
    if fn then
        local buf = gpu.gpubuf.FromFile(fn, "rpB") -- all access except write, plus backup
        if buf then
            fn = shorten(fn)
            node.buf = buf
            node.curfn = fn
            buf.filename = fn
        else
            errprint("Failed to load file: " .. fn)
        end
        recalc = true
    else
        fn = node.curfn or "(none)"
    end
    if recalc then
        node:postInit() -- Re-init connectors and fix types. Invokes changeconnectors().
        node:onSomethingChanged()
    end
    return fn
end

local function drawUI(node)
    local fn
    if imgui.Button("Load...") then
        fn = askOpenFile()
    end
    fn = (fn and loadbuf(node, fn)) or node.curfn or "(none)"
    imgui.Text(fn)
end

local function openContextMenu(node)
    node.filelist = io.filesfull("userdata")
end

local function drawContextMenu(node)
    local m = imgui.MenuItem
    local list = node.filelist
    imgui.TextDisabled("Quick access (userdata/ directory)")
    for i = 1, #list do
        if m(list[i]) then
            loadbuf(node, list[i])
        end
    end
end

local function recalc(node)
    return node.buf
end

local function makeOutput(node, inputs, name)
    return node.buf
end

local function serialize(node)
    return { file = node.fullfn }
end

local function deserialize(node, dat)
    if dat.file then
        loadbuf(node, dat.file)
    end
end



return {
    name = "Raw buffer",
    desc = "Load a file as raw memory buffer. No file format detection is performed -- the file is loaded as-is.",
    category = "input",
    recalc = recalc,
    drawUI = drawUI,
    drawContextMenu = drawContextMenu,
    openContextMenu = openContextMenu,
    makeOutput = makeOutput,
    serialize = serialize,
    deserialize = deserialize,
    extraOutputs = {
        buf = glsl.buffer,
    },
}

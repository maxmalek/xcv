local code = [[
#version 430

layout(local_size_x = 16, local_size_y = 8, local_size_z = 8) in;

uniform sampler3D in_Tex;
uniform bool u_keepAlpha = true;
// FIXME: make possible to adjust type to match input
writeonly restrict uniform image3D out_Tex;

uniform float u_mult = 1.0;

void main()
{
   uvec3 i, sz = imageSize(out_Tex);
   AUTOTILE(i, sz)
   {
        vec4 a = texelFetch(in_Tex, ivec3(i), 0);
        vec4 b = texelFetch(in_Tex, ivec3(i+1), 0);
        vec4 diff = abs(a - b) * u_mult;
        if(u_keepAlpha)
            diff.a = a.a;
        imageStore(out_Tex, ivec3(i), diff);
   }
};
]]


return {
    category = "compute",
    name = "3D edge detect (simple)",
    desc = "A simple edge detector",
    src = code,
}

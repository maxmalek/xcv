-- FIXME: This filter is currently separable, but not implemented that way

local code = [[#version 430
layout(local_size_x = 16, local_size_y = 8, local_size_z = 8) in;

uniform sampler3D in_Tex;
uniform uint u_boxsize = 1;
writeonly restrict uniform image3D out_Tex;

bool area_nop(ivec3 pp, ivec3 offs, uvec3 imgsz) { return true; }
bool area_box(ivec3 pp, ivec3 offs, uvec3 imgsz)
{
    return all(greaterThanEqual(pp, ivec3(0))) && all(lessThan(pp, ivec3(imgsz)));
}
bool area_circle(ivec3 pp, ivec3 offs, uvec3 imgsz) { return length(vec3(offs)) <= float(u_boxsize); }

void func_max(inout vec4 result, vec4 center, vec4 c) { result = max(result, c); }
void func_min(inout vec4 result, vec4 center, vec4 c) { result = min(result, c); }
void func_sum(inout vec4 result, vec4 center, vec4 c) { result += c; }

void init_zero(out vec4 result, vec4 center) { result = vec4(0.0); }
void init_center(out vec4 result, vec4 center) { result = center; }

void fin_nop      (inout vec4 result, vec4 center, uint n) {}
void fin_normalize(inout vec4 result, vec4 center, uint n) { result /= vec4(n); }


vec4 dobox(uvec3 imgsz, uvec3 p)
{
    uint s = u_boxsize;
    ivec3 pmin = ivec3(-s), pmax = ivec3(s);
    
    vec4 center = texelFetch(in_Tex, ivec3(p), 0);
    vec4 result;
    INITFUNC(result, center);
    uint n = 0;
    for(int z = pmin.z; z <= pmax.z; ++z)
    for(int y = pmin.y; y <= pmax.y; ++y)
    for(int x = pmin.x; x <= pmax.x; ++x)
    {
        ivec3 offs = ivec3(x,y,z);
        ivec3 pp = ivec3(p) + offs;
        if(AREACHECK(pp, offs, imgsz))
        {
            ++n;
            vec4 c = texelFetch(in_Tex, pp, 0);
            FUNC(result, center, c);
        }
    }
    FINALFUNC(result, center, n);
    return result;
}

void main()
{
   uvec3 i, sz = imageSize(out_Tex);
   AUTOTILE(i, sz)
   {
       vec4 c = dobox(sz, i);
       imageStore(out_Tex, ivec3(i), c);
   }
};
]]

local DEF_FUNC = {
    min = "func_min",
    max = "func_max",
    avg = "func_sum",
}
local DEF_INITFUNC = {
    min = "init_center",
    max = "init_center",
    avg = "init_zero",
}
local DEF_FINALFUNC = {
    min = "fin_nop",
    max = "fin_nop",
    avg = "fin_normalize",
}
local DEF_AREACHECK = {
    box = "area_box",
    fbox = "area_nop",
    circle = "area_circle",
}

local comboFunc  = { "Min", "Max", "Average" }
local comboFuncI = { "min", "max", "avg" }
local comboArea  = { "FastBox", "Box", "Circle" }
local comboAreaI = { "fbox",    "box", "circle" }

local function buildshader(node)
    local func, area = comboFuncI[node.funcidx], comboAreaI[node.areaidx]
    local defs = {
        FUNC = assert(DEF_FUNC[func]),
        INITFUNC = assert(DEF_INITFUNC[func]),
        FINALFUNC = assert(DEF_FINALFUNC[func]),
        AREACHECK = assert(DEF_AREACHECK[area]),
    }
    local sh = gpu.computeshader.Create(code, defs)
    assert(sh:setuniform("u_boxsize", node.boxsize))
    node:setShader(sh)
end

local function init(node)
    node.funcidx = node.funcidx or 1
    node.areaidx = node.areaidx or 1
    node.boxsize = node.boxsize or 1
    if not node.shader then
        buildshader(node)
    end
end

local function drawUI(node)
    local fc, fi = imgui.Combo("##func", node.funcidx, comboFunc)
    local ac, ai = imgui.Combo("##area", node.areaidx, comboArea)
    local bc, bs = imgui.SliderInt("Size", node.boxsize, 1, 4) -- for any higher value you'll have to Ctrl+Click and enter manually...
    
    if fc or ac then
        node.funcidx = fi
        node.areaidx = ai
        buildshader(node)
    end
    if bc then
        node.boxsize = bs
        assert(node._shader:setuniform("u_boxsize", bs))
    end
    
    return fc or ac or bc
end

return {
    name = "3D Neighbor filter",
    category = "compute",
    tags = "min max avg average box fastbox filter circle",
    init = init,
    drawUI = drawUI,
    maxsize = 128,
}

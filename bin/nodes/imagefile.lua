
local function changeconnectors(node)
    local Cout = assert(node.Cout)
    local ty = node.tex and node.tex:glsltype() or glsl.unknown
    
    local _, c = next(Cout)
    c.type = ty
    c.color = glsl.color(ty)
    if ty ~= glsl.unknown then
        c.detail = ""
    end
end

local function shorten(s)
    return s:match"/([^/]+)$" or s
end

local function loadimg(node, fn)
    local recalc
    if fn then
        local tex, err = gpu.texture.new(fn, nil, "B") -- B = keep backup in host memory
        if tex then
            infoprint("Loaded texture file: " .. fn)
            node.fullfn = fn
            fn = shorten(fn)
            node.curfn = fn
            node.tex = tex
            recalc = true
            tex.filename = fn
        else
            errprint(err)
        end
    else
        fn = node.curfn or "(none)"
    end
    if recalc then
        node:postInit() -- Re-init connectors and fix types. Invokes changeconnectors().
        node:onSomethingChanged()
    end
    return fn
end

local function drawUI(node)
    local fn
    if imgui.Button("Load...") then
        local filter = getSupportedImageFormats()
        fn = askOpenFile(filter)
    end
    fn = (fn and loadimg(node, fn)) or node.curfn or "(none)"
    imgui.Text(fn)
end

local function openContextMenu(node)
    node.filelist = io.filesfull("userdata")
end

local function drawContextMenu(node)
    local m = imgui.MenuItem
    local list = node.filelist
    imgui.TextDisabled("Quick access (userdata/ directory)")
    for i = 1, #list do
        if m(list[i]) then
            loadimg(node, list[i])
        end
    end
end

local function recalc(node)
    return node.tex
end

local function makeOutput(node, inputs, name)
    return node.tex
end

local function serialize(node)
    return { file = node.fullfn }
end

local function deserialize(node, dat)
    if dat.file then
        loadimg(node, dat.file)
    end
end

return {
    name = "Image file",
    desc = "Load an image from a file. The image file format is detected automatically.",
    category = "input",
    recalc = recalc,
    drawUI = drawUI,
    drawContextMenu = drawContextMenu,
    openContextMenu = openContextMenu,
    changeconnectors = changeconnectors,
    makeOutput = makeOutput,
    serialize = serialize,
    deserialize = deserialize,
    extraOutputs = {
        tex = { glsl.image1D, glsl.image2D, glsl.image3D },
    },
}

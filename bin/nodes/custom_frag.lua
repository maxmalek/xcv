local SHED = newclass("shadereditor_node_custom_frag", shadereditor)

function SHED.new(node, code)
    local self = shadereditor.new()
    switchclass(self, SHED)
    self.node = assert(node)
    if code then
        self:setCode(code)
    end
    return self
end
function SHED:onCompile(code)
    local sh, err = self.node.frag:setCode(code)
    if sh then
        self.node:setShader(sh)
    end
    return sh, err
end

---------------------------

local function update(node, dt) 
    node.editor:update(dt)
    node.frag:update(dt)
    node:onSomethingChanged()
end

local function drawDetailUI(node)
    local frag = node.frag
    imgui.TreeNode("out_Tex", "of", frag.drawUI, frag)
    imgui.TreeNode("Inputs", "f", frag.drawInputs, frag)
    imgui.Separator()
    node.editor:drawUI()
end

local function init(node)
    node.frag = node.frag or gpu.fragrender.new(800, 600)
    node.editor = node.editor or SHED.new(node, node.frag:getCode())
end

local function recalc(node)
    return node.frag:getResultTex()
end

local function serialize(node)
    return { autorecomp = node.editor.autorecomp, code = node.editor:getCode() }
end

local function deserialize(node, t)
    node.editor.autorecomp = t.autorecomp
    node.editor:setCode(t.code)
end


return {
    name = "Custom fragment shader",
    desc = "Run custom GLSL fragment shader. Somewhat compatible to Shadertoy. Produces the shader result as a 2D float texture.",
    category = "output",
    drawDetailUI = drawDetailUI,
    init = init,
    makeOutput = false,
    update = update,
    serialize = serialize,
    deserialize = deserialize,
    recalc = recalc,
    
    extraOutputs = {
        out_Tex = glsl.sampler2D,
    },
}

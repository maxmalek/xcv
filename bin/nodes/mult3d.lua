local code = [[
#version 430

layout(local_size_x = 16, local_size_y = 8, local_size_z = 8) in; // 16*8*8 == 1024 (don't go higher)

uniform float u_multiplier = 1.0;
uniform sampler3D in_Tex;
// FIXME: make possible to adjust type to match input
writeonly restrict uniform image3D out_Tex;

void main()
{
    uvec3 i, sz = imageSize(out_Tex);
    AUTOTILE(i, sz)
    {
        vec4 c = texelFetch(in_Tex, ivec3(i), 0);
        imageStore(out_Tex, ivec3(i), c * u_multiplier);
    }
};
]]


return {
    category = "compute",
    name = "3D multiply",
    src = code,
    maxsize = 1024,
}

local template = [[#version 450
layout(local_size_x = 4, local_size_y = 4) in;

uniform sampler2D in_Tex;
writeonly restrict uniform image2D out_Tex;
uniform uint u_elem = 0;

// Pre-4.5
//#define SWAP(a,b) { vec4 t1 = v[a], t2 = v[b]; v[a] = min(t1, t2); v[b] = max(t1, t2); }
// 4.5 supports mix(_, _, bvec) -- about 5x as fast as the above
#define SWAP(a,b) { bvec4 cmp = lessThan(v[a], v[b]); vec4 mins = mix(v[b], v[a], cmp); v[b] =  mix(v[a], v[b], cmp); v[a] = mins; }
#define SYNC /* nothing */
void sort(inout vec4 v[KERNEL_SIZE]) { $sortcode }
#undef SWAP
#undef SYNC

vec4 medianvalue(uvec2 p, uint which)
{
    #define T(a,b) texelFetch(in_Tex, ivec2(p) + ivec2(a,b), 0)
    vec4 v[KERNEL_SIZE] = { $boxcode };
    #undef T
    sort(v);
    return v[which];
}

void main()
{
    uvec2 i, sz = imageSize(out_Tex);
    AUTOTILE(i, sz)
    {
        vec4 a = medianvalue(i, u_elem);
        imageStore(out_Tex, ivec2(i), a);
    }
}
]]


local function npix(s)
    s = 2*s+1
    return s*s
end

local function _buildshader(node)
    local sz = node.size
    local ksz = npix(sz)
    
    local sortcode = {}
    for _, g in ipairs(algo.generateSortNetwork(ksz)) do
        for _, e in ipairs(g) do
            table.insert(sortcode, ("SWAP(%d, %d)"):format(table.unpack(e)))
        end
        table.insert(sortcode, "SYNC")
    end
    sortcode = table.concat(sortcode, " ")

    local boxcode = {}
    for y = -sz, sz do
        for x = -sz, sz do
            table.insert(boxcode, ("T(%d, %d)"):format(x,y))
        end
    end
    boxcode = table.concat(boxcode, ", ")

    local code = template:expand()
    local sh = gpu.computeshader.Create(code, { KERNEL_SIZE = ksz })
    node:setShader(sh)
end

local function init(node)
    node.size = node.size or 1
    _buildshader(node)
end

local function drawUI(node)
    
    local sc, size = imgui.SliderInt("Size", node.size, 1, 3) -- can't go > 3 because the sort network generation would overflow
    if sc then
        node.size = size
        _buildshader(node)
    end
    local sh = node._shader
    if sh then
        local m = npix(size) - 1
        local ec, elem = imgui.SliderInt("Index", sh:getuniform("u_elem"), 0, m)
        if elem > m then -- make sure not to read out of bounds when box is made smaller
            elem = m
            ec = true
        end
        if ec then
            assert(node._shader:setuniform("u_elem", elem))
        end
        return ec or sc
    end
end

local function serialize(node)
    return { size = node.size }
end

local function deserialize(node, t)
    node.size = t.size
end


return {
    name = "2D Median box filter (small)",
    desc = "Calculates median in a small box around a center pixel",
    category = "compute",
    tags = "sort",
    init = init,
    drawUI = drawUI,
    serialize = serialize,
    deserialize = deserialize,
    maxsize = 512,
}

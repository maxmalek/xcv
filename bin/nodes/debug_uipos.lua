
local function init(node)
    -- do init stuff
end

local function recalc(node)
    -- do compute stuff
    node.num = node.num or 0
end

local function drawUI(node)
    imgui.Text(("(%.3f, %.3f"):format(node:getPos()))
end

local function drawDetailUI(node)
    drawUI(node)
end



return {
    name = "UI pos",
    category = "DEBUG",
    init = init,
    recalc = recalc,
    drawUI = drawUI,
    drawDetailUI = drawDetailUI
}

local code = [[
#version 430

layout(local_size_x = 32, local_size_y = 32) in; // 32*32 == 1024 (don't go higher)

uniform sampler2D in_Tex;

// FIXME: make possible to adjust type to match input
layout(rgba32f, binding = 0) writeonly restrict uniform image2D out_Tex;

uniform float u_th = 1.0;
uniform bool u_ignoreAlpha = true;

void main()
{
   uvec2 i, sz = imageSize(out_Tex);
   AUTOTILE(i, sz)
   {
        vec4 a = texelFetch(in_Tex, ivec2(i), 0);
        vec4 v = step(vec4(u_th), a);
        if(u_ignoreAlpha)
            v.a = a.a;
        imageStore(out_Tex, ivec2(i), v);
   }
};
]]


return { 
    category = "compute",
    name = "2D Threshold",
    desc = "Below the threshold becomes 0.0, otherwise 1.0",
    src = code,
}

local template = [[#version 450
layout(local_size_x = $LSX, local_size_y = $LSY, local_size_z = $LSZ ) in;
uniform sampler${DIM}D in_X, in_X2;
writeonly restrict uniform image${DIM}D out_Y;
uniform float u_P = 0.0;
uniform float u_P2 = 1.0;

#define ivec1 int
#define uvec1 uint
#define ivecn ivec${DIM}
#define uvecn uvec${DIM}

// missing built-in variants to take scalars instead of vec4
vec4 pow_(vec4 v, float p)
{
    return pow(v, vec4(p));
}
vec4 clamp_(vec4 v, float a, float b)
{
    return clamp(v, vec4(a), vec4(b));
}
vec4 cross4(vec3 a, vec3 b)
{
    return vec4(cross(a, b), 0.0);
}

// Force using custom variants above
#define pow pow_
#define clamp clamp_
#define cross cross4

vec4 scalar(float v)
{
    return vec4(v, vec3(0.0));
}


void main()
{
    float P = u_P;
    float P2 = u_P2;
    uvecn i, sz = imageSize(out_Y);
    AUTOTILE(i, sz)
    {
        vec4 X = texelFetch(in_X, ivecn(i), 0);
        vec4 X2 = texelFetch(in_X2, ivecn(i), 0);
        vec4 v = $fcall;
        imageStore(out_Y, ivecn(i), v);
    }
};
]]

local LOCALSIZES =
{
    { 512, 1, 1 },
    { 32, 32, 1 },
    { 8, 8, 8 },
}

local DIMSELECT =
{
    "1D", "2D", "3D"
}

-- call, inputs, params
local FCLASSDESC =
{
    a = {"$F($X)",                1, 0 },
    b = {"$F($X, $X2)",           2, 0 },
    c = {"$F($X, $P)",            1, 1 },
    d = {"$F($X, $X2, $P)",       2, 1 },
    e = {"$F($X, $P, $P2)",       1, 2 },
    f = {"$F($P, $X)",            1, 1 },
    g = {"$F($P, $P2, $X)",       1, 2 },
    sa= {"scalar($F($X))",        1, 0, out = "r32f" },
    sb= {"scalar($F($X, $X2))",   2, 0, out = "r32f" },
    
    x2= {"vec4($F($X.xy), $X.zw)", 1, 0},
    x3= {"vec4($F($X.xyz), $X.w)", 1, 0},
    cx= {"$F($X.xyz, $X2.xyz)",    1, 0, out = "rgba32f"},
}

local FCLASS =
{
    sqrt = "a",
    inversesqrt = "a",
    abs = "a",
    sign = "a",
    floor = "a",
    trunc = "a",
    roundEven = "a",
    ceil = "a",
    fract = "a",
    exp = "a", 
    exp2 = "a",
    log = "a",
    log2 = "a",
    
    sin = "a",
    asin = "a",
    cos = "a",
    acos = "a",
    tan = "a",
    atan = "a",
    
    sinh = "a",
    asinh = "a",
    cosh = "a",
    acosh = "a",
    tanh = "a",
    atanh = "a",
    
    min = "b",
    max = "b",
    
    pow = "c",
    mod = "c",
    
    mix = "d",
    clamp = "e",
    step = "f",
    smoothstep = "g",
    
    -- vector functions
    reflect = "b",
    refract = "d",
    normalize = "a",
    normalize2 = "x2",
    normalize3 = "x3",
    length = "sa",
    distance = "sb",
    dot = "sb",
    cross = "cx",
}

-- custom functions don't exist in the docs so just use the underlying function
local ALIAS =
{
    normalize2 = "normalize",
    normalize3 = "normalize",
}
    

-- prepare code and UI things
local FLIST = {}
local FCALLS = {}
for name, cls in pairs(FCLASS) do
    local calltemplate, ninp, np = table.unpack(FCLASSDESC[cls])
    local F, P, P2, X, X2 = ALIAS[name] or name, "P", "P2", "X", "X2"
    local glsl = calltemplate:expand()
    
    -- collect names and call info
    table.insert(FLIST, name)
    FCALLS[name] = glsl
end
-- sort by name for displaying in the UI
table.sort(FLIST)



-- uses node._funcid & node.dimensions, sets node.funcname
local function _buildshader(node)
    local fname = FLIST[assert(node._funcid)]
    
    -- local vars for string expansion
    local DIM = assert(node.dimensions)
    local LSX, LSY, LSZ = table.unpack(LOCALSIZES[DIM])
    local fcall = FCALLS[fname]
    fname = ALIAS[fname] or fname

    -- build actual shader code
    local code = template:expand()
    
    -- the GLSL compiler will kick out any unused variables,
    -- so only the actually used ones will appear in the UI
    local sh = gpu.computeshader.Create(code)
    node:setShader(sh)
end


local function init(node)
    node._funcid = node._funcid or 1
    node.dimensions = node.dimensions or 3
    _buildshader(node)
end

local function drawUI(node)
    local sd, indexd = imgui.Combo("Input", node.dimensions, DIMSELECT)
    local sf, indexf, fname = imgui.Combo("Function", node._funcid, FLIST)
    imgui.Text("y = " .. FCALLS[fname])
    imgui.SameLine()
    if imgui.SmallButton" ? " then
        local hn = ALIAS[fname] or fname
        os.openurl("http://docs.gl/sl4/" .. hn)
    end
    if imgui.IsItemHovered() then
        imgui.Tooltip("Open documentation for " .. fname .. "() in web browser")
    end
    
    local changed = node:drawKnobs()
    
    if sd or sf then
        node._funcid = indexf
        node.dimensions = indexd
        node._outtype = FCLASSDESC[FCLASS[fname]].out -- optional
        _buildshader(node)
        changed = true
    end
    
    return changed
end

local function makeOutput(node, inputs, name)
    if name == "out_Y" then
        local fmt = node._outtype
        local tex = inputs.in_X
        if fmt and tex then
            return tex:newfrom(fmt)
        end
    end
    -- do whatever default the node does otherwise
end

local function serialize(node)
    return {
        funcname = FLIST[node._funcid],
        dimensions = FLIST[node.dimensions],
    }
end

local function deserialize(node, t)
    if t.funcname then
        local idx = table.getindexi(FLIST, t.funcname)
        if idx then
            node._funcid = idx
        end
    end
    local dim = t.dimensions
    if type(dim) == "number" and dim >= 1 and dim <= 3 then
        node.dimensions = dim
    end
end

local tags = table.concat(FLIST, " ")

return {
    category = "compute",
    name = "GLSL function",
    desc = "Selection of pointwise functions in 1D, 2D or 3D. Standard GLSL built-in functions:\n" .. tags,
    references = {
        "http://docs.gl/",
        "https://www.khronos.org/registry/OpenGL/specs/gl/GLSLangSpec.4.50.pdf#%5B%7B%22num%22%3A442%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C92.1%2C676.8%2C0%5D",
    },
    tags = tags,
    
    init = init,
    drawUI = drawUI,
    serialize = serialize,
    deserialize = deserialize,
    maxsize = 4096,
    makeOutput = makeOutput,
    
    outGen = {
        out_Y = "in_X",
    },
}

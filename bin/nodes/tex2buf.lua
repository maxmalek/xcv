
local function changeconnectors(node, Cin, Cout)
    local c = assert(Cout.out_Tex)
    local inp = node.INPUTS.in_Tex
    local other = inp and inp.node
    if other then
        local ty = other.Cout[inp.name].type
        c.type = ty
        c.detail = glsl.nameof(ty) .. " out_Tex"
        c.color = glsl.color(ty)
    else
        c.type = glsl.uint64_t
        c.detail = "(No input!)"
        c.color = glsl.color(c.type)
    end
end

local function recalc(node, inputs)
    local tex = inputs.in_Tex
    local b

    node.texformat = tex and tex:formatstr() -- or nil
    
    local b = tex and gpu.gpubuf.FromTexture(tex, "rwup") -- set all access flags
    return { out_Buf = b, out_Tex = tex }
end

local function drawUI(node)
    if node.texformat then
        imgui.Text("Format:" .. node.texformat)
    end
end


return {
    name = "Image->Buffer",
    desc = "Convert an image to a memory buffer. The internal format is preserved.",
    category = "convert",
    
    recalc = recalc,
    drawUI = drawUI,
    changeconnectors = changeconnectors,
    makeOutput = false, -- this node manages outputs on its own
    
    extraInputs = {
        in_Tex = glsl.uint64_t, -- accept anything
    },
    extraOutputs = {
        out_Buf = glsl.buffer,
        out_Tex = glsl.uint64_t,
    }
}

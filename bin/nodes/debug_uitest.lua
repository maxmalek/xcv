
local function init(node)
    -- do init stuff
end

local function recalc(node)
    -- do compute stuff
    node.x = node.x or 0
    node.y = node.y or 0
end

local function drawUI(node)
    if imgui.Button("  +  ") then node.x = node.x + 1 end
    imgui.SameLine()
    if imgui.Button("  -  ") then node.x = node.x - 1 end
    imgui.Text("x: " .. node.x .. ", y: " .. node.y)
end

local function drawDetailUI(node)
    drawUI(node)
    imgui.Separator()
    local changed, newy = imgui.SliderInt("Param y", node.y, 0, 100)
    if changed then
        node.y = newy
        node:onSomethingChanged() -- update and propagate downstream
    end
end



return {
    name = "UI Test",
    category = "DEBUG",
    init = init,
    recalc = recalc,
    drawUI = drawUI,
    drawDetailUI = drawDetailUI
}

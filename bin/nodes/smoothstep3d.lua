local code = [[
#version 430

layout(local_size_x = 8, local_size_y = 8, local_size_z = 8) in;

uniform sampler3D in_Tex;

writeonly restrict uniform image3D out_Tex;
uniform float u_center = 0.5;
uniform float u_area = 0.1;
uniform bool u_ignoreAlpha = true;

void main()
{
    uvec3 i, sz = imageSize(out_Tex);
    float edge0 = u_center - u_area;
    float edge1 = u_center + u_area;
   
    AUTOTILE(i, sz)
    {
        vec4 a = texelFetch(in_Tex, ivec3(i), 0);
        vec4 v = smoothstep(edge0, edge1, a);
        if(u_ignoreAlpha)
            v.a = a.a;
        imageStore(out_Tex, ivec3(i), v);
    }
};
]]

return { 
    category = "compute",
    name = "3D Smoothstep",
    desc = "Below (threshold - area) becomes 0.0, above (threshold + area) becomes 1.0, and interpolated between 0..1 in between.",
    src = code,
}

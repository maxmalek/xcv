local code = [[#version 450
layout(local_size_x = 8, local_size_y = 8, local_size_z = 8) in;

uniform sampler3D in_Tex;
writeonly restrict uniform image3D out_Tex;
uniform float u_imin = 0.1;
uniform float u_imax = 0.9;
uniform float u_omin = 0.0;
uniform float u_omax = 1.0;
uniform bool u_clamp = true;
uniform bool u_ignoreAlpha = true;


vec4 _rescale(vec4 t, vec4 lower, vec4 upper, vec4 rangeMin, vec4 rangeMax)
{
    bvec4 eq = equal(upper, lower);
    vec4 scaled = (((t - lower) / (upper - lower)) * (rangeMax - rangeMin)) + rangeMin;
    return mix(scaled, rangeMin, eq);
}

vec4 rescale(vec4 v)
{
    vec4 s = _rescale(v, vec4(u_imin), vec4(u_imax), vec4(u_omin), vec4(u_omax));
    if(u_clamp)
        s = clamp(s, vec4(u_omin), vec4(u_omax));
    if(u_ignoreAlpha)
        s.a = v.a;
    return s;
}

void main()
{
    uvec3 i, sz = imageSize(out_Tex);
    AUTOTILE(i, sz)
    {
        vec4 a = texelFetch(in_Tex, ivec3(i), 0);
        vec4 v = rescale(a);
        imageStore(out_Tex, ivec3(i), v);
    }
}
]]


return {
    category = "compute",
    name = "Rescale",
    desc = "Rescales input in [imin..imax] to [omin..omax] with optional clamping to that range.",
    src = code,
}


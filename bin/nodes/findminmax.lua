local code = [[#version 450
layout(local_size_x = 8, local_size_y = 8, local_size_z = 8) in;

layout(std430) restrict writeonly buffer MinBufOut
{
    vec4 val[];
} minbufout;
layout(std430) restrict writeonly buffer MaxBufOut
{
    vec4 val[];
} maxbufout;

uniform sampler3D in_Tex;

const uint localsize = LOCAL_SIZE();

shared vec4 shmin[localsize];
shared vec4 shmax[localsize];

const vec4 startmin = vec4(1e20);
const vec4 startmax = vec4(-1e20);

void main()
{
    uint tid = gl_LocalInvocationIndex;
    vec4 amin = startmin;
    vec4 amax = startmax;
    
    uvec3 i, sz = uvec3(textureSize(in_Tex, 0));
    mainWorkLoop(i, sz)
    {
        vec4 a = texelFetch(in_Tex, ivec3(i), 0);
        amin = min(a, amin);
        amax = max(a, amax);
    }
    
    shmin[tid] = amin;
    shmax[tid] = amax;
    
    memoryBarrierShared();
    barrier();
    
    for(uint s = localsize >> 1; s != 0; s >>= 1)
    {
        if (tid < s)
        {
            vec4 mi = shmin[tid + s];
            vec4 ma = shmax[tid + s];
            amin = min(mi, amin);
            amax = max(ma, amax);
            shmin[tid] = amin;
            shmax[tid] = amax;
        }

        memoryBarrierShared();
        barrier();
    }
    
    if(tid == 0)
    {
        uint gid = GLOBAL_GROUP_IDX();
        minbufout.val[gid] = amin;
        maxbufout.val[gid] = amax;
    }
}
]]

-- final reduction code
local code2 = [[#version 450
layout(local_size_x = 128) in;

layout(std430) restrict writeonly buffer MinBufOut
{
    vec4 val[];
} minbufout;
layout(std430) restrict writeonly buffer MaxBufOut
{
    vec4 val[];
} maxbufout;

layout(std430) restrict readonly buffer MinBufIn
{
    vec4 val[];
} minbufin;
layout(std430) restrict readonly buffer MaxBufIn
{
    vec4 val[];
} maxbufin;

uniform uint nelements;

const uint localsize = LOCAL_SIZE();

shared vec4 shmin[localsize];
shared vec4 shmax[localsize];

const vec4 startmin = vec4(1e20);
const vec4 startmax = vec4(-1e20);

void main()
{
    uint tid = gl_LocalInvocationIndex;
    vec4 amin = startmin;
    vec4 amax = startmax;
    
    mainWorkLoop1(i, nelements)
    {
        amin = min(amin, minbufin.val[i]);
        amax = max(amax, maxbufin.val[i]);
    }
    
    shmin[tid] = amin;
    shmax[tid] = amax;
    
    memoryBarrierShared();
    barrier();
    
    for(uint s = localsize >> 1; s != 0; s >>= 1)
    {
        if (tid < s)
        {
            vec4 mi = shmin[tid + s];
            vec4 ma = shmax[tid + s];
            amin = min(mi, amin);
            amax = max(ma, amax);
            shmin[tid] = amin;
            shmax[tid] = amax;
        }

        memoryBarrierShared();
        barrier();
    }
    
    if(tid == 0)
    {
        uint gid = GLOBAL_GROUP_IDX();
        minbufout.val[gid] = amin;
        maxbufout.val[gid] = amax;
    }
}
]]

local function init(node)
    node._sh2 = assert(gpu.computeshader.Create(code2))
    node.channels = node.channels or 3 -- 3 is actually the most compatible setting with the usual input
end

local function drawUI(node)
    local s, changed, channels
    channels = node.channels
    if channels == 0 then
        s = "Auto, " .. tostring(node.usedchannels)
    else
        s = tostring(channels)
    end
    imgui.Text("Channels: " .. s)
    changed, channels = imgui.SliderInt("##channels", channels, 0, 4)
    node.channels = channels
    imgui.Text("Min: " .. tostring(node.minval))
    imgui.Text("Max: " .. tostring(node.maxval))
    return changed
end

local function recalc(node, inputs)
    node.minval = nil
    node.maxval = nil
    node.usedchannels = nil
    local tex = inputs.in_Tex
    if not tex then
        return
    end
    local sh = node._shader
    local gx, gy, gz = sh:groupsForSize(tex:size3())
    
    local T = glsl.vec4
    local esz = glsl.sizeof(T)
    local nelem = gx*gy*gz
    local bufsize = nelem * esz
    local minbuf = gpu.gpubuf.new(bufsize, "rp")
    local maxbuf = gpu.gpubuf.new(bufsize, "rp")
    
    --print("FIRST SIZE = " .. nelem .. ", bufsize: " .. bufsize)
    
    assert(sh:setbuffer("MinBufOut", minbuf))
    assert(sh:setbuffer("MaxBufOut", maxbuf))
    assert(sh:setsampler("in_Tex", tex))
    sh:dispatch(gx, gy, gz)
    ------------------------------------------
    
    local sh2 = node._sh2
    local g = sh2:groupsForSize(nelem)
    bufsize = g * esz
    local ominbuf = gpu.gpubuf.new(bufsize, "rp")
    local omaxbuf = gpu.gpubuf.new(bufsize, "rp")
    assert(sh2:setuniform("nelements", nelem))
    assert(sh2:setbuffer("MinBufIn", minbuf))
    assert(sh2:setbuffer("MaxBufIn", maxbuf))
    assert(sh2:setbuffer("MinBufOut", ominbuf))
    assert(sh2:setbuffer("MaxBufOut", omaxbuf))
    sh2:dispatch(g)
    
    ------------------------------------------

    -- insert fence after the dispatch...
    ominbuf:insertFence()
    
    -- do some stuff while the GPU is busy finishing up...
    sh:clearRefs()
    sh2:clearRefs()
    minbuf:delete()
    maxbuf:delete()
    
    -- wait until GPU has finished the dispatch
    -- ideally we'd not do this at all, but those values are needed on the CPU side
    ominbuf:waitFences()
    
    local minv = numvec.newRawView(T, ominbuf:readptr(), ominbuf:rawsize())
    local maxv = numvec.newRawView(T, omaxbuf:readptr(), omaxbuf:rawsize())
    --print("REDUCE SIZE = " .. #minv .. ", bufsize: " .. bufsize)
    local minvals = { minv:reduce(numvec.Min) }
    local maxvals = { maxv:reduce(numvec.Max) }
    
    ominbuf:delete()
    omaxbuf:delete()
    
    local ch = node.channels
    if ch == 0 then
        ch = tex:channels()
    end
    node.usedchannels = ch
    local minval = math.min(table.unpack(minvals, 1, ch))
    local maxval = math.min(table.unpack(maxvals, 1, ch))
    
    node.minval = minval
    node.maxval = maxval
    -- TODO: actually return minval/maxval (-> dev branch)
end



return {
    category = "compute",
    name = "Find min/max",
    desc = "Finds min/max values in input",
    src = code,
    init = init,
    recalc = recalc,
    drawUI = drawUI,
    
    makeOutput = false, -- this node manages outputs on its own
}


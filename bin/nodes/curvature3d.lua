local code = [[#version 430

layout(local_size_x = 16, local_size_y = 8, local_size_z = 8) in; // 16*8*8 == 1024 (don't go higher)

uniform sampler3D in_Tex; // texture with calculated edges
uniform sampler3D in_TexNormal; // calculated normals
writeonly restrict uniform image3D out_Tex;

const int BOX_SIZE = 2;
const int AXIS_PROBES = 2*BOX_SIZE + 1;
const float MULT = 1.0 / float(AXIS_PROBES * AXIS_PROBES * AXIS_PROBES);

float value(in vec3 tc)
{
    return texture(in_Tex, tc).x;
}

vec3 getnormal(in vec3 tc)
{
    return texture(in_TexNormal, tc).xyz;
}

vec3 normalize0(in vec3 v)
{
    return length(v) > 0.0 ? normalize(v) : vec3(0.0);
}


vec4 curv(in vec3 tc, in vec3 offs)
{
    vec3 accu = vec3(0.0);
    vec3 accuN = vec3(0.0);
    float corr = 0.0;
    vec3 n = getnormal(tc);
    for(int z = -BOX_SIZE; z <= BOX_SIZE; ++z)
       for(int y = -BOX_SIZE; y <= BOX_SIZE; ++y)
           for(int x = -BOX_SIZE; x <= BOX_SIZE; ++x)
           {
               vec3 d = vec3(x,y,z);
               vec3 p = tc + offs * d;
               float v = value(p);
               vec3 nn = getnormal(p);
               accu += v * nn;
               accuN += (1.0 - v) * nn;
               corr += (1.0 + dot(n, nn)) * (0.5 * v);
           }
    vec3 avgNormal = normalize0(accu);
    vec3 avgNormalN = normalize0(accuN);
    float avgCorr = corr * MULT;
    vec3 deviation = avgNormal - n;
    vec3 deviationN = avgNormalN - n;
    //return vec4(avgNormal, avgCorr);
    //return vec4(normalize(deviation), length(deviation));
    //return vec4(deviation, avgCorr);
    //return vec4((1.0 - avgCorr) * 2.0);
    //return vec4(vec3(length(deviation)), 1.0);   
    //float vv = length(deviation) + (1.0 - avgCorr);
    //return vec4(vv);
    return vec4(length(deviation), pow(1.0 - avgCorr, 5.0), length(deviationN), 1.0);
}

void main()
{
   uvec3 i, sz = imageSize(out_Tex);
   vec3 tm = 1.0 / vec3(sz);
   vec3 tmhalf = 0.5 * tm;
   AUTOTILE(i, sz)
   {
       vec3 tc = (vec3(i) * tm) + tmhalf; // always use voxel centers
       vec4 c = curv(tc, tm);
       //c = mix(c, vec4(1.0), 0.5);
       imageStore(out_Tex, ivec3(i), c);
   }
};

]]


return {
    category = "compute",
    name = "3D curvature (various)",
    desc = "Calculates various curvatures for a 3D volume",
    src = code,
    outGen = {
        out_Tex = { input = "in_Tex", format = "rgba16f", swizzle = "rgba" }
    },
}

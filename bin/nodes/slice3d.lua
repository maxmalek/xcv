local CODE = [[
#version 430

layout(local_size_x = 64, local_size_y = 16) in;

uvec2 INC = gl_NumWorkGroups.xy * gl_WorkGroupSize.xy;
uvec2 IDX = gl_GlobalInvocationID.xy;
uniform float depth;
uniform sampler3D in_Tex;
layout(FMT) writeonly restrict uniform image2D out_Tex;

ivec3 SZ = textureSize(in_Tex, 0);

#if defined(AXIS_X)
vec3 getcoord(vec2 tc) { return vec3(depth, 1.0-tc.y, tc.x); }
#elif defined(AXIS_Y)
vec3 getcoord(vec2 tc) { return vec3(tc.y, 1.0-depth, tc.x); }
#elif defined(AXIS_Z)
vec3 getcoord(vec2 tc) { return vec3(tc.x, 1.0-tc.y, depth); }
#else
#error what
#endif


void main()
{
   ivec2 td = imageSize(out_Tex);
   vec2 tdf = vec2(td);
   for(uvec2 i = IDX; all(lessThan(i, td)); i += INC)
   {
       vec3 tc = getcoord((vec2(i) + 0.5) / tdf);
       vec4 c = texture(in_Tex, tc);
       imageStore(out_Tex, ivec2(i), c);
   }
}
]]

local unpack = table.unpack
local floor = math.floor
local tointeger = math.tointeger
local max = math.max

local function cf(r,g,b,a)
    return { r/255., g/255., b/255., a/255. }
end

local function int(x)
    return assert(tointeger(floor(x)))
end

local ColorHover = 
{
    color.i2i(255, 140, 140, 140),
    color.i2i(140, 255, 140, 140),
    color.i2i(140, 140, 255, 140),
}

local ColorHoverFloat = 
{
    cf(255, 140, 140, 190),
    cf(140, 255, 140, 190),
    cf(140, 140, 255, 190),
}

local AXISNAMES = { "X", "Y", "Z", X = 1, Y = 2, Z = 3 }

local function shuf(axis, x,y,z)
    if axis == 1 then
        return z, y
    elseif axis == 2 then
        return z, x
    elseif axis == 3 then
        return x, y
    end
end

local function lerp(a, b, t)
    return a + (b - a) * t
end

local STYLE = {}

local function img(node, axis)
    local ch
    local tex = node.texs[axis]
    local D = node.depths
    
    local W, H = imgui.GetContentRegionAvail()
    local w, h = tex:displaysize()
    H = W * (h / w)
    
    --imgui.ImageWidthFit(tex, nil, nil, nil, nil,  unpack(ColorHoverFloat[axis]))
    -- Using an ImageButton captures a mouse drag action so that it's ignored by the underlying window;
    -- Otherwise the window would move when we drag, which is not what we want here.
    local border = ColorHoverFloat[axis]
    STYLE[imgui.Col_Button] = border
    STYLE[imgui.Col_ButtonHovered] = border
    STYLE[imgui.Col_ButtonActive] = border
        
    imgui.ApplyStyle(STYLE, imgui.ImageButton, tex, W, H, nil,nil, nil,nil, 2)
    local x1, y1, x2, y2 = imgui.GetItemRect() 
    
    if imgui.IsItemHovered() then
        local Mx, My = imgui.GetMousePos() -- absolute mouse pos
        local sx, sy = x2 - x1, y2 - y1    -- size of image
        local mx, my = Mx - x1, My - y1    -- mouse relative to image upper left corner
        local u, v = mx / sx, my / sy
        --imgui.Text(("UV: %.3f, %.3f"):format(u, v))
        
        if imgui.IsMouseButton(0) then
            local a1, a2 = shuf(axis, 1, 2, 3)
            ch = D[a1] ~= u or D[a2] ~= v
            D[a1] = u
            D[a2] = v
        end
        
    end

    local dx, dy = shuf(axis, unpack(D))
    local x = lerp(x1, x2, dx)
    local y = lerp(y1, y2, dy)
    local c1, c2 = shuf(axis, unpack(ColorHover))
    
    imgui.DrawLine(x, y1, x, y2, c1, 1)
    imgui.DrawLine(x1, y, x2, y, c2, 1)
    
    tex:drawSaveAsButton()
    
    return ch
end

local function drawOutput(node, name)
    return img(node, AXISNAMES[name])
end


local function allocslice(tex, axis, fmt, flags, fixaspect)
    local w, h
    if fixaspect then
        w, h = shuf(axis, tex:displaysizeScaled())
        w = int(max(w, 1))
        h = int(max(h, 1))
    else
        w, h = shuf(axis, tex:internalsize())
    end
    
    local new = gpu.texture.new(w, h, 0, fmt, flags, tex:swizzle())

    if not fixaspect then
        local px, py = shuf(axis, tex:getPixelSize())
        new:setPixelSize(px, py)
    end
    
    return new
end

local function allocxyz(tex, ...)
    return allocslice(tex, 1, ...),
           allocslice(tex, 2, ...),
           allocslice(tex, 3, ...)
end

local function setupShaders(node, fmt)
    node._sh =
    {
        gpu.computeshader.Create(CODE, { AXIS_X = 1, FMT = fmt }),
        gpu.computeshader.Create(CODE, { AXIS_Y = 1, FMT = fmt }),
        gpu.computeshader.Create(CODE, { AXIS_Z = 1, FMT = fmt }),
    }
end

local function init(node)
    --setupShaders(node, "rgba32f")
    if not node.depths then
        node.depths = { 0.5, 0.5, 0.5 }
    end
    if node.interp == nil then
        node.interp = true
    end
    if node.fixaspect == nil then
        node.fixaspect = false
    end
    node.fmt = nil -- recreate shaders
    node._sh = nil
end

local function drawUI(node)
    for i = 1, 3 do
        local d = node.depths[i]
        local ch, nd = imgui.Slider(AXISNAMES[i], d, 0.0, 1.0)
        if ch then
            node.depths[i] = nd
            node:onSomethingChanged()
        end
    end
end

local function drawDetailUI(node)
    local changed, on = imgui.Checkbox("Interpolate", node.interp)
    if changed then
        node.interp = on
        node:onSomethingChanged()
    end
    changed, on = imgui.Checkbox("Square pixels", node.fixaspect)
    if imgui.IsItemHovered() then
        imgui.Tooltip("Correct pixel aspect ratio so that pixels are of square size.\nUseful if you want to save the image in a format\nthat does not account for non-square pixels, such as JPEG or PNG.\nUncheck to keep pixel sizes as they are.")
    end
    if changed then
        node.fixaspect = on
        node:onSomethingChanged()
    end
    node:drawDetailUIDefault()
end

local function recalc(node, inputs)
    local src = inputs.in_Tex
    if not src then
        return
    end
    local fmt = src:formatstr()
    if fmt ~= node.fmt then
        setupShaders(node, fmt)
        node.fmt = fmt
    end
    local flags
    if node.interp then
        flags = "l" -- linear interpolation
    else
        flags = "" -- uses linear interpolation if nil -> empty string cancels that
    end
    node.texs = { allocxyz(src, fmt, flags, node.fixaspect) }
    local out = {}
    for i = 1, 3 do
        local sh = node._sh[i]
        local slice = node.texs[i]
        slice:invalidate()
        assert(sh:setsampler("in_Tex", src))
        --assert(sh:setimage("in_Tex", src))
        assert(sh:setimage("out_Tex", slice))
        sh:setuniform("depth", node.depths[i])
        sh:dispatchForSize(slice:size())
        out[AXISNAMES[i]] = slice:genMipmaps()
        sh:clearRefs()
    end
    
    return out
end

local function serialize(node)
    return { depths = node.depths, interp = node.interp, fixaspect = node.fixaspect }
end

local function deserialize(node, t)
    node.fixaspect = t.fixaspect
    node.interp = t.interp
    if t.depths then
        assert(#(t.depths) == 3)
        node.depths = t.depths
    end
end

return {
    name = "3D extract slices",
    desc = "Extracts 2D X/Y/Z slices from a 3D volume image",
    category = "compute",
    
    init = init,
    recalc = recalc,
    drawUI = drawUI,
    drawDetailUI = drawDetailUI,
    drawOutput = drawOutput,
    makeOutput = false, -- this node manages outputs on its own
    serialize = serialize,
    deserialize = deserialize,
    
    extraInputs = {
        in_Tex = glsl.sampler3D,
    },
    extraOutputs = {
        X = glsl.sampler2D,
        Y = glsl.sampler2D,
        Z = glsl.sampler2D,
    }
}

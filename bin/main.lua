local reloading = ...

local function init()
    local sw = mainwindow.Open(0, 0, false, 0) -- w, h, fullscreen, displayIndex
    sw:setTitle("XCV")
    graphbuilder.NewInner(sw, "rootgraphbuilder", 400, 400, nil, nil, "l", 0.8)
    sw.infopane = ui.infopaneClass.new(sw)
    sw:newConsole("Log", "main_global_log", nil, nil, 0, 400, "r", 0.5):setLogLevel(1):useGlobalLog(true, true)
end

if not reloading then
    loadConfigOrDie()
    xpcall(init, errorbox)
end

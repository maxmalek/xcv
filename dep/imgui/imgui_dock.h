// based on https://github.com/nem0/LumixEngine/blob/master/external/imgui/imgui_dock.h
// modified from https://bitbucket.org/duangle/liminal/src/tip/src/liminal/imgui_dock.h
// modified from https://github.com/slages/love-imgui/blob/master/src/libimgui/imgui_dock.h
// merged with https://github.com/vassvik/imgui_docking_minimal/blob/master/imgui/imgui_dock.h

#pragma once

#include "imgui.h"

// Forward declarations
typedef int ImGuiWindowFlags;
    
enum ImGuiDockSlot
{
    ImGuiDockSlot_Left,
    ImGuiDockSlot_Right,
    ImGuiDockSlot_Top,
    ImGuiDockSlot_Bottom,
    ImGuiDockSlot_Tab,

    ImGuiDockSlot_Float,
    ImGuiDockSlot_None
};

namespace ImGui{

IMGUI_API void RootDock(const ImVec2& pos, const ImVec2& size);
IMGUI_API void ShutdownDock();
IMGUI_API void SetNextDock(ImGuiDockSlot slot);
IMGUI_API bool BeginDock(const char* label, bool* opened = NULL, ImGuiWindowFlags extra_flags = 0);
IMGUI_API void SetNextDockSplitRatio(const ImVec2& split_ratio);
IMGUI_API void SetNextDockFloatingSize(const ImVec2& floating_size);
IMGUI_API void SetNextDockFloatingPos(const ImVec2& floating_pos);
IMGUI_API void EndDock();
IMGUI_API void SetDockActive();
IMGUI_API bool IsLastDockFocused();

};


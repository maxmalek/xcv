#pragma once

#define GLM_META_PROG_HELPERS

#include <glm/fwd.hpp>

namespace glmx {

template<typename T> struct Info {};

#define X(T, bty, elem) \
    template<> struct Info<T> { typedef bty scalar_type; enum { scalar_components = elem }; };
#define XB(T) \
    X(T, T, 1)
#define XV(T, bty) \
    X(T ## 2, bty, 2) \
    X(T ## 3, bty, 3) \
    X(T ## 4, bty, 4)
#define X2(T, bty, A, B) \
    X(T ## A ## x ## B, bty, (A*B))
#define XM2(T, bty) \
    X(T ## 2, bty, 2*2) \
    X(T ## 3, bty, 3*3) \
    X(T ## 4, bty, 4*4)
#define XM(T, bty) \
    XM2(T, bty) \
    X2(T, bty, 2, 3) \
    X2(T, bty, 2, 4) \
    X2(T, bty, 3, 2) \
    X2(T, bty, 3, 4) \
    X2(T, bty, 4, 2) \
    X2(T, bty, 4, 3)

XB(float)
XB(int)
XB(unsigned)
XB(int64_t)
XB(uint64_t)
XB(double)
XB(bool)
XV(glm::vec, float)
XV(glm::dvec, double)
XV(glm::ivec, int)
XV(glm::uvec, unsigned)
XV(glm::i64vec, int64_t)
XV(glm::u64vec, uint64_t)
XV(glm::bvec, bool)
XM(glm::mat, float)
XM(glm::dmat, double)

#undef X
#undef X2
#undef XB
#undef XV
#undef XM
#undef XM2


} // end namespace glmx

